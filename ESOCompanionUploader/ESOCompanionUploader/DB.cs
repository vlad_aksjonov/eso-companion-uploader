﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using SwissKnife.Extenders;

namespace ESOCompanionUploader
{
    class DB
    {
        private static MySqlConnection con;

        private static string server = "s18.tuthost.com";
        private static string database = "blackswa_neso";
        private static string uid = database;
        private static string password = "13481348";

        public static void Connect()
        {
            con = new MySqlConnection("SERVER=" + server + ";DATABASE=" + database + ";UID=" + uid + ";PASSWORD=" + password);
            con.Open();
        }

        public static void Close()
        {
            con.Close();
            con = null;
        }

        public static void DELETE(string table, Dictionary<string, string> where)
        {
            if (con == null) Connect();
            new MySqlCommand($"DELETE FROM {table} WHERE {DoWhere(where)}", con).ExecuteNonQuery();
        }

        public static void DELETE(string table, string where)
        {
            if (con == null) Connect();
            new MySqlCommand($"DELETE FROM {table} WHERE {where}", con).ExecuteNonQuery();
        }

        public static long INSERT(string table, List<string> columns, List<string> values)
        {
            if (con == null) Connect();
            var cmd = new MySqlCommand($"INSERT INTO {table} ({DoColumns(columns)}) VALUES({DoValues(values)}) ON DUPLICATE KEY UPDATE `id` = `id`;", con);
            cmd.ExecuteNonQuery();
            return cmd.LastInsertedId;
        }

        public static long INSERT(string table, Dictionary<string,string> set)
        {
            if (con == null) Connect();
            var cmd = new MySqlCommand($"INSERT INTO {table} SET {DoSet(set)}  ON DUPLICATE KEY UPDATE `id` = `id`", con);
            cmd.ExecuteNonQuery();
            return cmd.LastInsertedId;
        }

        public static long INSERT(string table, string columns, string values)
        {
            if (con == null) Connect();
            var cmd = new MySqlCommand($"INSERT INTO {table} ({columns}) VALUES({values}) ON DUPLICATE KEY UPDATE `id` = `id`;", con);
            cmd.ExecuteNonQuery();
            return cmd.LastInsertedId;
        }

        /*public static string INSERT(string table, Dictionary<string, string> set, Dictionary<string, string> where)
        {
            if (con == null) Connect();
            INSERT(table, set);
            return con.l//SELECT_SCALAR(new List<string> { "id" }, table, where);
        }

        public static string INSERT(string table, List<string> columns, List<string> values, Dictionary<string,string> where)
        {
            if (con == null) Connect();
            INSERT(table, columns, values);
            return SELECT_SCALAR(new List<string>{"id"}, table, where);
        }

        public static string INSERT(string table, string columns, string values, string where)
        {
            if (con == null) Connect();
            INSERT(table, columns, values);
            return SELECT_SCALAR("`id`", table, where);
        }*/

        public static string SELECT_SCALAR(List<string> select, string table, Dictionary<string,string> where)
        {
            if (con == null) Connect();
            return (string)new MySqlCommand($"SELECT {DoSelect(select)} FROM {table} WHERE {DoWhere(where)}", con).ExecuteScalar()?.ToString();
        }

        public static string SELECT_SCALAR(string select, string table, string where)
        {
            if (con == null) Connect();
            return (string)new MySqlCommand($"SELECT {select} FROM {table} WHERE {where}",con).ExecuteScalar()?.ToString();
        }

        public static void UPDATE(string table, Dictionary<string,string> set, Dictionary<string,string> where)
        {
            if (con == null) Connect();
            new MySqlCommand($"UPDATE {table} SET {DoSet(set)} WHERE {DoWhere(where)}", con).ExecuteNonQuery();
        }

        public static void UPDATE(string table, string set, string where)
        {
            if (con == null) Connect();
            new MySqlCommand($"UPDATE {table} SET {set} WHERE {where}", con).ExecuteNonQuery();
        }

        private static string DoWhere(Dictionary<string, string> where)
        {
            var wheres = new List<string>();
            foreach (var pair in where)
            {
                wheres.Add(pair.Key + "=\"" + pair.Value+"\"");
            }
            return wheres.Implode(" AND ");
        }

        private static string DoSet(Dictionary<string, string> set)
        {
            var sets = new List<string>();
            foreach (var pair in set)
            {
                sets.Add(pair.Key + "=\"" + pair.Value+"\"");
            }
            return sets.Implode(", ");
        }

        private static string DoColumns(List<string> columns)
        {            
            return columns.Implode(", ");
        }

        private static string DoValues(List<string> values)
        {
            for (int i = 0; i < values.Count; i++)
            {
                values[i] = $"\"{values[i]}\"";
            }
            return values.Implode(", ");
        }

        private static string DoSelect(List<string> select)
        {
            return select.Implode(", ");
        }




    }
}
