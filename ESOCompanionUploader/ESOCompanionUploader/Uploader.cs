﻿using System;
using System.IO;
using System.Security.Permissions;
using SwissKnife.Parsers.Lua;
using System.Collections.Generic;
using SwissKnife.Extenders;

namespace ESOCompanionUploader
{
    class Uploader
    {
        string AddonName = "ESOCompanionGatherer";
        private string[] Servers = new string[] { "live", "liveeu", "pts" };
        private FileSystemWatcher[] Watchers = new FileSystemWatcher[3];

        private int[] levels = new int[] { 1, 16, 26, 36, 46, 50, 53, 56, 58, 64 };

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public Uploader()
        {
            for (var i = 0; i < Servers.Length; i++)
            {
                try
                {
                    Watchers[i] = new FileSystemWatcher(Environment.ExpandEnvironmentVariables("%userprofile%\\Documents\\Elder Scrolls Online\\" + Servers[i] + "\\SavedVariables\\"));
                    Watchers[i].EnableRaisingEvents = true;
                    Watchers[i].IncludeSubdirectories = true;
                    Watchers[i].NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.Size | NotifyFilters.CreationTime | NotifyFilters.Attributes | NotifyFilters.DirectoryName | NotifyFilters.FileName | NotifyFilters.Security;
                    Watchers[i].Changed += Uploader_Changed;
                    Watchers[i].Created += Uploader_Changed;
                    Watchers[i].Deleted += Uploader_Changed;
                }
                catch (Exception)
                {

                }
            }
        }

        private bool isKnownUID(string userID)
        {
            var count = DB.SELECT_SCALAR("count(*)", "`eso_users`", $"`id`= '{userID}'");
            return count == "1";
        }

        public void ParseFile(string FilePath)
        {
            var server = "";
            if (FilePath.Contains("\\live\\")) { server = "live"; }
            if (FilePath.Contains("\\liveeu\\")) { server = "liveeu"; }
            if (FilePath.Contains("\\pts\\")) { server = "pts"; }
            string[] lines = File.ReadAllLines(FilePath);
            int i = 2;
            var result = LuaParser.Parse(lines, ref i);
            result = result["Default"];
            var accountName = result.FirstChildName();
            result = result[accountName];
            var userID = "";
            if (result["$AccountWide"]["UserID"].Value!=null)
            {

                userID = result["$AccountWide"]["UserID"].Value;
                DB.Connect();
                if(isKnownUID(userID))
                {
                    foreach(var pair in result)
                    {
                        if(pair.Key.StartsWith("$"))
                        {
                            HandleBank(result[pair.Key], accountName, userID);
                        }
                        else
                        {
                            HandleUser(result[pair.Key], pair.Key, userID);
                        }
                    }
                }
                else
                {
                    var errorFile = File.ReadAllText(Environment.ExpandEnvironmentVariables($"%userprofile%\\Documents\\Elder Scrolls Online\\{server}\\AddOns\\{AddonName}\\wrongUser.tpl"));
                    errorFile = errorFile.Replace("{USERNAME}", accountName).Replace("{WRONGID}", "UserUnreg");
                    File.WriteAllText(Environment.ExpandEnvironmentVariables($"%userprofile%\\Documents\\Elder Scrolls Online\\{server}\\SavedVariables\\{AddonName}.lua"),errorFile);
                }
                DB.Close();
            }
        }

        private void HandleUser(LuaParser.LuaEntry entry, string charName, string userID)
        {
            string charID = HandleCharacter(entry, charName, userID);
            HandleInventory(entry["Inventory"], 1, userID, charID);
            HandleInventory(entry["Worn"], 0, userID, charID);
            HandleStyles(entry["Styles"], charID);
            HandleSkills(entry["Skills"], charID);
            HandleTraits(entry["Traits"], charID);
            HandleBuffs(entry["Buffs"], charID);
        }

        private void HandleBuffs(LuaParser.LuaEntry entry, string charID)
        {
            DB.DELETE("`eso_activebuffs`", $"`char_id`={charID}");
            foreach (var pair in entry)
            {
                var buff = pair.Value;
                var buffID = DB.INSERT("`eso_buffs`",
                        new Dictionary<string, string>
                        {
                            ["`Name`"] = buff["Name"].Value,
                            ["`AbilityType`"] = buff["AbilityType"].Value,
                            ["`StatusEffectType`"] = buff["StatusEffectType"].Value,
                            ["`Type`"] = buff["Type"].Value,
                            ["`Slot`"] = buff["Slot"].Value,
                            ["`Icon`"] = buff["Icon"].Value,
                            ["`CanClickOff`"] = buff["CanClickOff"].Value
                        }).ToString();
                DB.INSERT("`eso_activebuffs`",
                        new Dictionary<string, string>
                        {
                            ["`char_id`"] = charID,
                            ["`buff_id`"] = buffID,
                            ["`started`"] = buff["Started"].Value,
                            ["`ending`"] = buff["Ending"].Value
                        });
            }
        }

        private void HandleTraits(LuaParser.LuaEntry entry, string charID)
        {
            DB.DELETE("`eso_chartrais`", $"`char_id`={charID}");
            DB.DELETE("`eso_chartimers`", $"`char_id`={charID} AND `timerType`=1");
            foreach (var p1 in entry)
            {
                var craftName = p1.Key;
                foreach (var p2 in p1.Value)
                {
                    var part = p2.Value;
                    var partName = p2.Key;
                    var partID = DB.INSERT("`eso_part`",
                        new Dictionary<string, string>
                        {
                            ["`craft`"] = craftName,
                            ["`name`"] = partName,
                            ["`texture`"] = part["texture"].Value
                        }).ToString();
                    var charTraits = new Dictionary<string, string>();
                    charTraits.Add("char_id", charID);
                    charTraits.Add("part_id", partID);
                    foreach (var p3 in part)
                    {
                        if (p3.Value.Value == null)
                        {
                            var trait = p3.Value;
                            var traitIndex = p3.Key.Parse<int>().ToString();
                            if (traitIndex == "0") break;

                            var traitID = DB.INSERT("`eso_traits`",
                                new Dictionary<string, string>
                                {
                                    ["`material`"] = trait["Material"].Value,
                                    ["`part_id`"] = partID,
                                    ["`icon`"] = trait["Icon"].Value,
                                    ["`description`"] = trait["Description"].Value,
                                    ["`traitIndex`"] = traitIndex,
                                }).ToString();
                            charTraits.Add($"trait{traitIndex}", (trait["Known"]=="true")?"1":"0");
                            if(trait["Duration"].Value!=null)
                            {
                                DB.INSERT("`eso_chartimers`",
                                new Dictionary<string, string>
                                {
                                    ["`char_id`"] = charID,
                                    ["`timestamp`"] = trait["Timestamp"].Value,
                                    ["`duration`"] = trait["Duration"].Value,
                                    ["`remaning`"] = trait["Remaning"].Value,
                                    ["`timerType`"] = "1",
                                    ["`parameter1`"] = partID,
                                    ["`parameter2`"] = traitIndex,
                                });
                            }
                        }
                    }
                    DB.INSERT("`eso_chartrais`", charTraits);
                }
            }
        }

        private void HandleSkills(LuaParser.LuaEntry entry, string charID)
        {
            DB.DELETE("`eso_charabilities`", $"`char_id`={charID}");
            DB.DELETE("`eso_charskills`", $"`char_id`={charID}");
            foreach (var p1 in entry)
            {
                var className = p1.Key;
                foreach (var p2 in p1.Value)
                {
                    var skill = p2.Value;
                    var skillID = DB.INSERT("`eso_skills`",
                        new Dictionary<string, string>
                        {
                            ["`Сlass`"] = className,
                            ["`Name`"] = skill["Name"].Value
                        }).ToString();
                    DB.INSERT("`eso_charskills`", 
                        new Dictionary<string, string>
                        {
                            ["`char_id`"] = charID,
                            ["`skill_id`"] = skillID,
                            ["rank"] = skill["Rank"].Value
                        });
                    foreach(var p3 in skill["Abilities"])
                    {
                        var ability = p3.Value;
                        var abilityID = DB.INSERT("`eso_abilities`", 
                            new Dictionary<string, string>
                            {
                                ["`skill_ID`"] = skillID,
                                ["`name`"] = ability["Name"].Value,
                                ["`texture`"] = ability["Texture"].Value,
                                ["`ultimate`"] = ability["Ultimate"].Value,
                                ["`nexttexture`"] = ability["nextTexture"].Value,
                                ["`nextname`"] = ability["nextName"].Value,
                                ["`passive`"] = ability["Passive"].Value
                            }).ToString();
                        DB.INSERT("`eso_charabilities`",
                            new Dictionary<string, string>
                            {
                                ["`char_id`"] = charID,
                                ["`ability_id`"] = abilityID,
                                ["`purchased`"] = ability["Purchased"].Value,
                                ["`rank`"] = ability["Rank"].Value
                            });
                    }
                }
            }
        }

        private void HandleStyles(LuaParser.LuaEntry entry, string charID)
        {
            var table = "`eso_styles`";
            DB.DELETE(table, $"`char_id`={charID}");
            foreach (var pair in entry)
            {
                var style = pair.Value;
                if(style.Value!=null)
                {
                    style.Value = (style.Value == "true") ? "1" : "0";
                    DB.INSERT(
                        table, 
                        new Dictionary<string, string> 
                        {
                            ["`char_id`"] = charID,
                            ["`name`"] = pair.Key,
                            ["`shoulders`"] = style.Value,
                            ["`maces`"] = style.Value,
                            ["`gloves`"] = style.Value,
                            ["`chests`"] = style.Value,
                            ["`belts`"] = style.Value,
                            ["`swords`"] = style.Value,
                            ["`shields`"] = style.Value,
                            ["`helmets`"] = style.Value,
                            ["`daggers`"] = style.Value,
                            ["`legs`"] = style.Value,
                            ["`boots`"] = style.Value,
                            ["`axes`"] = style.Value,
                            ["`bows`"] = style.Value,
                            ["`staves`"] = style.Value,
                        });
                }
                else
                {
                    DB.INSERT(
                        table,
                        new Dictionary<string, string>
                        {
                            ["`char_id`"] = charID,
                            ["`name`"] = pair.Key,
                            ["`shoulders`"] = (style["Shoulders"].Value == "true") ? "1" : "0",
                            ["`maces`"] = (style["Maces"].Value == "true") ? "1" : "0",
                            ["`gloves`"] = (style["Gloves"].Value == "true") ? "1" : "0",
                            ["`chests`"] = (style["Chests"].Value == "true") ? "1" : "0",
                            ["`belts`"] = (style["Belts"].Value == "true") ? "1" : "0",
                            ["`swords`"] = (style["Swords"].Value == "true") ? "1" : "0",
                            ["`shields`"] = (style["Shields"].Value == "true") ? "1" : "0",
                            ["`helmets`"] = (style["Helmets"].Value == "true") ? "1" : "0",
                            ["`daggers`"] = (style["Daggers"].Value == "true") ? "1" : "0",
                            ["`legs`"] = (style["Legs"].Value == "true") ? "1" : "0",
                            ["`boots`"] = (style["Boots"].Value == "true") ? "1" : "0",
                            ["`axes`"] = (style["Axes"].Value == "true") ? "1" : "0",
                            ["`bows`"] = (style["Bows"].Value == "true") ? "1" : "0",
                            ["`staves`"] = (style["Staves"].Value == "true") ? "1" : "0",
                        });
                }
            }
        }

        private string HandleCharacter(LuaParser.LuaEntry entry, string charName, string userID)
        {
            return DB.INSERT("`eso_characters`", 
                new Dictionary<string, string>()
                {
                    ["`user_id`"]= userID,
                    ["`Riding_Inventory`"]= entry["Riding"]["Inventory"].Value,
                    ["`Riding_Stamina`"]= entry["Riding"]["Stamina"].Value,
                    ["`Riding_Speed`"]= entry["Riding"]["Speed"].Value,
                    ["`AvARankPoints`"] = entry["AvARankPoints"].Value,
                    ["`XP`"] = entry["XP"].Value,
                    ["`RaceID`"]= entry["RaceID"].Value,
                    ["`Type`"]= entry["Type"].Value,
                    ["`Level`"]= entry["Level"].Value,
                    ["`ZoneIndex`"]= entry["ZoneIndex"].Value,
                    ["`Gender`"]= entry["Gender"].Value,
                    ["`Race`"]= entry["Race"].Value,
                    ["`Alliance`"]= entry["Alliance"].Value,
                    ["`AvARank`"]= entry["AvARank"].Value,
                    ["`AvARankPointsNeed`"]= entry["AvARankPointsNeed"].Value,
                    ["`Currency_AlliancePoints`"]= entry["Currency"]["AlliancePoints"].Value,
                    ["`Currency_Gold`"]= entry["Currency"]["Gold"].Value,
                    ["`Currency_TelVarStones`"]= entry["Currency"]["TelVarStones"].Value,
                    ["`Power_Health`"]= entry["Powers"]["Health"].Value,
                    ["`Power_Magica`"]= entry["Powers"]["Magica"].Value,
                    ["`Power_Stamina`"]= entry["Powers"]["Stamina"].Value,
                    ["`Power_Ultimate`"]= entry["Powers"]["Ultimate"].Value,
                    ["`Zone`"]= entry["Zone"].Value,
                    ["`VeteranRank`"]= entry["VeteranRank"].Value,
                    ["`Class`"]= entry["Class"].Value,
                    ["`XPMax`"]= entry["XPMax"].Value,
                    ["`Name`"]= entry["Name"].Value
                }).ToString();
        }

        private void HandleBank(LuaParser.LuaEntry entry, string accountName, string userID)
        {
            UpdateAccount(entry, accountName, userID);
            HandleInventory(entry["Inventory"], 3, userID, "BANK");
        }

        private void HandleInventory(LuaParser.LuaEntry entry, int bagID, string userID, string charID)
        {
            DB.DELETE(
                "`eso_inventory`",
                new Dictionary<string, string>
                {
                    ["`userID`"] = userID,
                    ["`bagID`"] = bagID.ToString(),
                    ["`char_id`"] = charID
                });

            foreach (var pair in entry)
            {
                var item = pair.Value;
                var link = item["link"].Value.Split(':');
                var quality = link[3];
                var level = link[4].Parse<int>();
                var worn = link[21];
                link[3] = "0";
                for (var i = levels.Length; i > 0; i--)
                {
                    if (levels[i - 1] <= level)
                    {
                        link[4] = levels[i - 1].ToString();
                        break;
                    }
                }
                link[21] = "0";
                item["link"].Value = string.Join(":", link);

                DB.INSERT(
                    "eso_items",
                    new List<string>
                    {
                        "link", "Name", "ItemType", "Icon"
                    },
                    new List<string>
                    {
                        item["link"].Value,
                        item["name"].Value,
                        item["itemType"].Value,
                        item["icon"].Value
                    });

                var itemID = DB.SELECT_SCALAR("`id`", "`eso_items`", $"`link`='{item["link"]}'");

                DB.INSERT(
                    "eso_inventory",
                    new List<string>
                    {
                        "item_id","char_id","stack","bagID","userID","worn","quality","level"
                    },
                    new List<string>() {
                        itemID,
                        charID,
                        item["stack"].Value,
                        bagID.ToString(),
                        userID,
                        worn,
                        quality,
                        level.ToString()
                    });
            }
        }

        private void UpdateAccount(LuaParser.LuaEntry entry, string accountName, string userID)
        {
            DB.UPDATE(
                "`eso_users`",
                new Dictionary<string, string>()
                {
                    ["`account`"] = accountName,
                    ["`Gold`"] = entry["Currency"]["Gold"].Value,
                    ["`TelVarStones`"] = entry["Currency"]["TelVarStones"].Value,
                    ["`AlliancePoints`"] = entry["Currency"]["AlliancePoints"].Value
                },
                new Dictionary<string, string>()
                {
                    ["`id`"] = userID
                });
        }

        private void Uploader_Changed(object sender, FileSystemEventArgs e)
        {
            if (e.Name == AddonName + ".lua")
            {
                ParseFile(e.FullPath);
            }
        }
    }
}
