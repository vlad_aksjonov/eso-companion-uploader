## Title: Votan's Achievements Overview
## Author: votan
## Description: Build-In achievements overview replacement.
## Version: 1.2.5
## APIVersion: 100014 100015
## SavedVariables: VotansAchievementsOvw_Data

## This Add-On is not created by, affiliated with or sponsored by ZeniMax Media Inc. or its affiliates. The Elder Scrolls® and related logos are registered trademarks or trademarks of ZeniMax Media Inc. in the United States and/or other countries. All rights reserved.

StatusBar.lua
VotansAchievementsOvw.xml
VotansAchievementsOvw.lua