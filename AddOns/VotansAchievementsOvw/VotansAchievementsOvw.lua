local data = {
	name = "VotansAchievementsOvw",
	localTimeShift = 0
}

local em = GetEventManager()
local ROW_TYPE_ID = 1
local selectedColor = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_TEXT_COLORS, INTERFACE_TEXT_COLOR_SELECTED))
local disabledColor = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_TEXT_COLORS, INTERFACE_TEXT_COLOR_DISABLED))

local function GoToAchievement(rowControl)
	if not rowControl then return end
	local rowData = ZO_ScrollList_GetData(rowControl)
	if not rowData then return end
	local achievements = SYSTEMS:GetObject("achievements")

	local achievementId = rowData.achievementId
	local categoryIndex, subCategoryIndex, _, idOffset = GetCategoryInfoFromAchievementId(achievementId)
	if achievements:OpenCategory(categoryIndex, subCategoryIndex) then
		if not achievements.achievements then return end
		local parentAchievementIndex = achievementId - idOffset
		if not achievements.achievements[parentAchievementIndex] then
			achievements:ResetFilters()
		end
		if not achievements.achievements[parentAchievementIndex] then
			for id, row in pairs(achievements.achievements) do
				if row.achievementId == achievementId then
					parentAchievementIndex = id
					break
				end
			end
		end
		if achievements.achievements[parentAchievementIndex] then
			achievements.achievements[parentAchievementIndex]:Expand()
			zo_callLater( function()
				if achievements.achievements and achievements.achievements[parentAchievementIndex] then
					ZO_Scroll_ScrollControlIntoCentralView(achievements.contentList, achievements.achievements[parentAchievementIndex]:GetControl())
				end
			end , 250)
		end
	end
end

local function HideRowHighlight(rowControl, hidden)
	if not rowControl then return end
	if not ZO_ScrollList_GetData(rowControl) then return end

	local highlight = rowControl:GetNamedChild("Highlight")

	if highlight then
		if not highlight.animation then
			highlight.animation = ANIMATION_MANAGER:CreateTimelineFromVirtual("ShowOnMouseOverLabelAnimation", highlight)
		end

		if highlight.animation:IsPlaying() then
			highlight.animation:Stop()
		end
		if hidden then
			highlight.animation:PlayBackward()
			ClearTooltip(AchievementTooltip)
		else
			highlight.animation:PlayForward()

			InitializeTooltip(AchievementTooltip, rowControl, TOPRIGHT, 0, -104, TOPLEFT)
			local rowData = ZO_ScrollList_GetData(rowControl)
			data:ShowTooltip(rowData.achievementId)
		end
	end
end

function data:ShowTooltip(achievementId)
	AchievementTooltip:VotanClearStatusBars()
	AchievementTooltip:SetAchievement(achievementId)

	local numCriteria = GetAchievementNumCriteria(achievementId)
	if numCriteria == 0 then return end

	AchievementTooltip:AddVerticalPadding(8)

	local hasMultipleCriteria = numCriteria > 1
	local showProgressBarDescriptions = hasMultipleCriteria
	local lines = nil
	for i = 1, numCriteria do
		local description, numCompleted, numRequired = GetAchievementCriterion(achievementId, i)
		if numRequired > 1 then
			local statusBar = AchievementTooltip:VotanAddStatusBar()
			statusBar:SetMinMax(0, numRequired)
			statusBar:SetValue(numCompleted)
			statusBar.progress:SetText(zo_strjoin(nil, numCompleted, "/", numRequired))
			if hasMultipleCriteria then
				statusBar.label:SetText(description)
				statusBar.label:SetColor(((numRequired == numCompleted) and selectedColor or disabledColor):UnpackRGB())
			else
				statusBar.label:SetText("")
			end
		elseif hasMultipleCriteria and(numRequired == 1) then
			if not lines then lines = { } end
			lines[#lines + 1] =(numCompleted == 1 and selectedColor or disabledColor):Colorize(description)
		end
	end
	if lines and #lines > 0 then
		AchievementTooltip:AddLine(table.concat(lines, "\n"), "ZoFontWinH4", 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, TEXT_ALIGN_CENTER, true)
	end
end

function data:InitScrollList()
	local function onMouseEnter(rowControl)
		HideRowHighlight(rowControl, false)
	end
	local function onMouseExit(rowControl)
		HideRowHighlight(rowControl, true)
	end
	local function onMouseDoubleClick(rowControl)
		GoToAchievement(rowControl)
	end

	local function setupDataRow(rowControl, rowData, scrollList)
		local icon = rowControl:GetNamedChild("Texture")
		local nameLabel = rowControl:GetNamedChild("Name")
		local recentLabel = rowControl:GetNamedChild("Recent")
		local originLabel = rowControl:GetNamedChild("Origin")

		icon:SetTexture(rowData.icon)
		nameLabel:SetText(zo_strformat(rowData.name))
		nameLabel:SetColor((rowData.completed and selectedColor or disabledColor):UnpackRGB())

		local timeStamp = rowData.lastUpdate
		local timeStr = ZO_FormatTime(timeStamp, TIME_FORMAT_STYLE_CLOCK_TIME, self.ClockFormat)
		recentLabel:SetText(zo_strjoin(nil, timeStr, "\n", GetDateStringFromTimestamp(timeStamp - data.localTimeShift)))

		local topLevelIndex, categoryIndex = GetCategoryInfoFromAchievementId(rowData.achievementId)
		if topLevelIndex then
			local topName = GetAchievementCategoryInfo(topLevelIndex)
			local subName
			if categoryIndex then
				subName = GetAchievementSubCategoryInfo(topLevelIndex, categoryIndex)
			else
				local _, numSubCatgories = GetAchievementCategoryInfo(topLevelIndex)
				subName = numSubCatgories > 0 and GetString(SI_JOURNAL_PROGRESS_CATEGORY_GENERAL) or ""
			end
			topName = topName:gsub("-", "")
			originLabel:SetText(subName ~= "" and zo_strjoin(" - ", topName, subName) or topName)
		else
			originLabel:SetText("")
		end

		rowControl:SetHandler("OnMouseEnter", onMouseEnter)
		rowControl:SetHandler("OnMouseExit", onMouseExit)
		rowControl:SetHandler("OnMouseDoubleClick", onMouseDoubleClick)
	end
	ZO_ScrollList_AddDataType(self.RecentScrollList, ROW_TYPE_ID, "VotansAchievementRow", 60, setupDataRow)
end

function data:UpdateScrollList()
	local scrollList = self.RecentScrollList
	local dataList = ZO_ScrollList_GetDataList(scrollList)

	ZO_ScrollList_Clear(scrollList)

	local progress = self.player.progress
	local achievementIds = { }
	for id in pairs(progress) do
		achievementIds[#achievementIds + 1] = id
	end

	table.sort(achievementIds, function(a, b)
		return(progress[a] == progress[b]) and(a < b) or progress[a] > progress[b]
	end )

	local maxEntries = 15
	local top = { }
	for i = 1, #achievementIds do
		local achievementId = achievementIds[i]
		local name, description, _, icon, completed = GetAchievementInfo(achievementId)
		if not completed then
			local topLevelIndex, categoryIndex, achievementIndex = GetCategoryInfoFromAchievementId(achievementId)
			categoryIndex = categoryIndex or 0
			local sub = top[topLevelIndex] or { }
			top[topLevelIndex] = sub
			local ach = sub[categoryIndex] or { }
			sub[categoryIndex] = ach
			if not ach[achievementIndex] then
				ach[achievementIndex] = achievementId
				local rowData = { name = name, icon = icon, achievementId = achievementId, lastUpdate = progress[achievementId] or 0 }
				dataList[#dataList + 1] = ZO_ScrollList_CreateDataEntry(ROW_TYPE_ID, rowData, 1)
				if #dataList >= maxEntries then break end
			end
		else
			progress[achievementId] = nil
		end
	end

	ZO_ScrollList_Commit(scrollList)
end

function data:SetupControls()
	local function AchievementsStateChange(oldState, newState)
		if newState == SCENE_SHOWING then
			self:UpdateScrollList()
		end
	end

	self.SummaryInset = ZO_AchievementsContents:GetNamedChild("SummaryInset")
	self.SummaryInsetTotal = self.SummaryInset:GetNamedChild("Total")
	self.SummaryInsetRecent = self.SummaryInset:GetNamedChild("Recent")
	self.RecentScrollList = WINDOW_MANAGER:CreateControlFromVirtual("$(parent)VotansAchievementsList", self.SummaryInset, "ZO_ScrollList")
	self.ProgressContainer = WINDOW_MANAGER:CreateControl("$(parent)VotansAchievementsProgress", self.SummaryInset, CT_CONTROL)
	self.ProgressContainer:SetHidden(true)

	self.StatusBars = { }
	local i = 0
	while true do
		i = i + 1
		local statusBar = self.SummaryInset:GetNamedChild("ZO_JournalProgressStatusBar" .. i)
		if statusBar == nil then break end
		self.StatusBars[#self.StatusBars + 1] = statusBar
		statusBar:SetParent(self.ProgressContainer)
	end

	self.ProgressContainer:SetAnchor(TOPLEFT, self.SummaryInsetTotal, BOTTOMLEFT, 0, 10)
	self.ProgressContainer:SetAnchor(BOTTOMRIGHT, self.SummaryInsetRecent, TOPRIGHT, 0, -10)
	self.RecentScrollList:SetWidth(542)
	self.RecentScrollList:SetAnchor(TOPLEFT, self.SummaryInsetTotal, BOTTOMLEFT, 0, 10)
	self.RecentScrollList:SetAnchor(BOTTOMLEFT, self.SummaryInsetRecent, TOPLEFT, 0, -10)

	self:InitScrollList()

	self.achievementsScene = SCENE_MANAGER:GetScene("achievements")
	self.achievementsScene:RegisterCallback("StateChange", AchievementsStateChange)
end

function data:HookAchievementRow()
	local function MouseEnter(achievement)
		if achievement.collapsed then
			InitializeTooltip(AchievementTooltip, achievement.control, TOPRIGHT, 0, -104, TOPLEFT)
			data:ShowTooltip(achievement.achievementId)
		end
	end
	local function MouseExit(control)
		ClearTooltip(AchievementTooltip)
	end

	local orgFactory = ACHIEVEMENTS.achievementPool.m_Factory
	ACHIEVEMENTS.achievementPool.m_Factory = function(...)
		local achievement = orgFactory(...)
		ZO_PreHook(achievement, "OnMouseEnter", MouseEnter)
		ZO_PreHook(achievement, "OnMouseExit", MouseExit)
		return achievement
	end
end

function data:RegisterAchievementEvents()
	local function AchievementsUpdated(eventCode)
		if not SCENE_MANAGER:IsShowing("achievements") then return end
		self:UpdateScrollList()
	end
	local function AchievementUpdated(eventCode, id)
		self.player.progress[id] = GetTimeStamp() + self.localTimeShift
	end
	local function AchievementAwarded(eventCode, name, points, id)
		if self.player.progress[id] then self.player.progress[id] = nil end
	end

	em:RegisterForEvent(data.name, EVENT_ACHIEVEMENTS_UPDATED, AchievementsUpdated)
	em:RegisterForEvent(data.name, EVENT_ACHIEVEMENT_UPDATED, AchievementUpdated)
	em:RegisterForEvent(data.name, EVENT_ACHIEVEMENT_AWARDED, AchievementAwarded)
end

function data:InitialAchievements()
	local now = GetTimeStamp() + self.localTimeShift
	local progress = self.player.progress
	local function GetAchievements(topLevelIndex, categoryIndex, numAchievements)
		for achievementIndex = 1, numAchievements do
			local achievementId = GetAchievementId(topLevelIndex, categoryIndex, achievementIndex)
			local _, _, _, _, completed = GetAchievementInfo(achievementId)
			if not completed then
				local numCriteria = GetAchievementNumCriteria(achievementId)
				for i = 1, numCriteria do
					local _, numCompleted = GetAchievementCriterion(achievementId, i)
					if numCompleted > 0 and not progress[achievementId] then
						progress[achievementId] = now
					end
				end
			end
		end
	end

	local numCategories = GetNumAchievementCategories()
	for topLevelIndex = 1, numCategories do
		local _, numSubCatgories, numAchievements = GetAchievementCategoryInfo(topLevelIndex)
		-- General
		GetAchievements(topLevelIndex, nil, numAchievements)
		for subCategoryIndex = 1, numSubCatgories do
			local _, numAchievements = GetAchievementSubCategoryInfo(topLevelIndex, subCategoryIndex)
			GetAchievements(topLevelIndex, subCategoryIndex, numAchievements)
		end
	end
end

function data:InitSettings()
	local localTimeShift = GetSecondsSinceMidnight() -(GetTimeStamp() % 86400)
	if localTimeShift < -12 * 60 * 60 then localTimeShift = localTimeShift + 86400 end
	self.localTimeShift = localTimeShift
	self.ClockFormat =(GetCVar("Language.2") == "en") and TIME_FORMAT_PRECISION_TWELVE_HOUR or TIME_FORMAT_PRECISION_TWENTY_FOUR_HOUR

	local defaults = { progress = { } }
	self.player = ZO_SavedVars:New("VotansAchievementsOvw_Data", 1, nil, defaults)
	if next(self.player.progress) == nil then
		-- Initial
		self:InitialAchievements()
	end
end

function data:Initialize()
	self:SetupControls()
	self:HookAchievementRow()
	self:InitSettings()
	self:RegisterAchievementEvents()
end

local function OnAddOnLoaded(event, addonName)
	if addonName == data.name then
		em:UnregisterForEvent(data.name, EVENT_ADD_ON_LOADED)
		data:Initialize()
	end
end

em:RegisterForEvent(data.name, EVENT_ADD_ON_LOADED, OnAddOnLoaded)