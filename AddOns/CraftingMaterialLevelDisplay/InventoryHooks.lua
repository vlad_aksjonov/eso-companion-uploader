--The allowed item types that will show labels in the inventory/loot window if enabled in the settings
local allowedItemTypesForLabel = {
--[ITEMTYPE_ADDITIVE] = true,
[ITEMTYPE_ARMOR] = true,
--[ITEMTYPE_ARMOR_BOOSTER] = true,
--[ITEMTYPE_ARMOR_TRAIT] = true,
--[ITEMTYPE_AVA_REPAIR] = true,
--[ITEMTYPE_BLACKSMITHING_BOOSTER] = true,
--[ITEMTYPE_BLACKSMITHING_MATERIAL] = true,
--[ITEMTYPE_BLACKSMITHING_RAW_MATERIAL] = true,
--[ITEMTYPE_CLOTHIER_BOOSTER] = true,
--[ITEMTYPE_CLOTHIER_MATERIAL] = true,
--[ITEMTYPE_CLOTHIER_RAW_MATERIAL] = true,
--[ITEMTYPE_COLLECTIBLE] = true,
[ITEMTYPE_CONTAINER] = true,
--[ITEMTYPE_COSTUME] = true,
--[ITEMTYPE_DISGUISE] = true,
[ITEMTYPE_DRINK] = true,
[ITEMTYPE_ENCHANTING_RUNE_ASPECT] = true,
[ITEMTYPE_ENCHANTING_RUNE_ESSENCE] = true,
[ITEMTYPE_ENCHANTING_RUNE_POTENCY] = true,
--[ITEMTYPE_ENCHANTMENT_BOOSTER] = true,
--[ITEMTYPE_FLAVORING] = true,
[ITEMTYPE_FOOD] = true,
[ITEMTYPE_GLYPH_ARMOR] = true,
[ITEMTYPE_GLYPH_JEWELRY] = true,
[ITEMTYPE_GLYPH_WEAPON] = true,
--[ITEMTYPE_INGREDIENT] = true,
--[ITEMTYPE_LOCKPICK] = true,
--[ITEMTYPE_LURE] = true,
--[ITEMTYPE_MOUNT] = true,
--[ITEMTYPE_NONE] = true,
--[ITEMTYPE_PLUG] = true,
[ITEMTYPE_POISON] = true,
[ITEMTYPE_POTION] = true,
--[ITEMTYPE_RACIAL_STYLE_MOTIF] = true,
--[ITEMTYPE_RAW_MATERIAL] = true,
--[ITEMTYPE_REAGENT] = true,
--[ITEMTYPE_RECIPE] = true,
--[ITEMTYPE_SCROLL] = true,
--[ITEMTYPE_SIEGE] = true,
[ITEMTYPE_SOUL_GEM] = true,
--[ITEMTYPE_SPELLCRAFTING_TABLET] = true,
--[ITEMTYPE_SPICE] = true,
--[ITEMTYPE_STYLE_MATERIAL] = true,
--[ITEMTYPE_TABARD] = true,
[ITEMTYPE_TOOL] = true,
--[ITEMTYPE_TRASH] = true,
--[ITEMTYPE_TROPHY] = true,
[ITEMTYPE_WEAPON] = true,
--[ITEMTYPE_WEAPON_BOOSTER] = true,
--[ITEMTYPE_WEAPON_TRAIT] = true,
--[ITEMTYPE_WOODWORKING_BOOSTER] = true,
--[ITEMTYPE_WOODWORKING_MATERIAL] = true,
--[ITEMTYPE_WOODWORKING_RAW_MATERIAL] = true,
}

--Check the games API
local gameAPI = GetAPIVersion()
--Switch variable name of ZOs inventory for API version 100015
if(gameAPI <= 100014) then
	table.insert(allowedItemTypesForLabel, {[ITEMTYPE_ALCHEMY_BASE] = true})
else
	table.insert(allowedItemTypesForLabel, {[ITEMTYPE_POTION_BASE] = true})
	table.insert(allowedItemTypesForLabel, {[ITEMTYPE_POISON_BASE] = true})
end

local veteranLevelToLevelStr = {
	[60] = "CP10",
	[70] = "CP20",
	[80] = "CP30",
	[90] = "CP40",
	[100] = "CP50",
	[110] = "CP60",
	[120] = "CP70",
	[130] = "CP80",
	[140] = "CP90",
	[150] = "CP100",
	[160] = "CP110",
	[170] = "CP120",
	[180] = "CP130",
	[190] = "CP140",
	[200] = "CP150",
	[210] = "CP160",
}

local function getLabelForInventoryRowControl(row, itemId)
    local label = CraftingMaterialLevelDisplay.currentInventoryRows[row:GetName()]
    if not label then
        label = WINDOW_MANAGER:CreateControl(row:GetName() .. "CMLD", row, CT_LABEL)
        CraftingMaterialLevelDisplay.currentInventoryRows[row:GetName()] = label
    end
    label:SetText(itemId)
    --label:SetFont("ZoFontGame")
	CraftingMaterialLevelDisplay.updateLabels(label)
    label:ClearAnchors()
    label:SetHidden(true)
    return label
end

local function IsTheRowRectangular(rowControl)
    return rowControl:GetWidth() / rowControl:GetHeight() > 1.5
end

local function DisplayTheLabel(rowControl, labelControl, offset, text)
    if IsTheRowRectangular(rowControl) then
        labelControl:SetText(text)
        labelControl:SetAnchor(RIGHT, rowControl, RIGHT, offset)
        labelControl:SetHidden(false)
    end
end

local function AddCraftingMaterialLevelToInventoryRow(rowControl, tradeSkillType, itemType, itemId, offset, itemLink, itemLevel)
	local label = getLabelForInventoryRowControl(rowControl, itemId)

    if tradeSkillType == CRAFTING_TYPE_CLOTHIER then
        if CraftingMaterialLevelDisplay.savedVariables.clothingInventoryList then
            if ClothingMaterials[itemId] and ClothingMaterials[itemId].level ~= nil then
                DisplayTheLabel(rowControl, label, offset, "["..ClothingMaterials[itemId].level.."]")
            end
        end

    elseif tradeSkillType == CRAFTING_TYPE_BLACKSMITHING then
        if CraftingMaterialLevelDisplay.savedVariables.blacksmithingInventoryList then
            if BlacksmithingMaterials[itemId] and BlacksmithingMaterials[itemId].level ~= nil then
                DisplayTheLabel(rowControl, label, offset, "["..BlacksmithingMaterials[itemId].level.."]")
            end
        end

    elseif tradeSkillType == CRAFTING_TYPE_WOODWORKING then
        if CraftingMaterialLevelDisplay.savedVariables.woodworkingInventoryList then
            if WoodworkingMaterials[itemId] and WoodworkingMaterials[itemId].level ~= nil then
                DisplayTheLabel(rowControl, label, offset, "["..WoodworkingMaterials[itemId].level.."]")
            end
        end

    elseif tradeSkillType == CRAFTING_TYPE_ENCHANTING
            and itemType ~= ITEMTYPE_GLYPH_ARMOR
            and itemType ~= ITEMTYPE_GLYPH_JEWELRY
            and itemType ~= ITEMTYPE_GLYPH_WEAPON then
        -- Does not need to account for the created Glyphs, just the runes
        if CraftingMaterialLevelDisplay.savedVariables.enchantingInventoryList then
            if EnchantingMaterials[itemId] and EnchantingMaterials[itemId].level ~= nil then
                DisplayTheLabel(rowControl, label, offset, "["..EnchantingMaterials[itemId].level.."]")
            end
        end
	
	--[[
    elseif tradeSkillType == CRAFTING_TYPE_PROVISIONING then
        -- Does not need to account for the created Glyphs, just the runes
        if CraftingMaterialLevelDisplay.savedVariables.provisioningInventoryList then
            if ProvisioningMaterials[itemId] and ProvisioningMaterials[itemId].level ~= nil and ProvisioningMaterials[itemId].level ~= "-1" then
                DisplayTheLabel(rowControl, label, offset, "["..ProvisioningMaterials[itemId].level.."]")
            end
        end
	]]--
	
	--No crafting material but normal
	else
        if CraftingMaterialLevelDisplay.savedVariables.normalInventoryList then
			--Check if item type should show an label
            local itemType = GetItemLinkItemType(itemLink)

--d("[AddCraftingMaterialLevelToInventoryRow]".. GetItemLinkName(itemLink) .. ": Type: " .. itemType .. ", Level: " .. GetItemLinkRequiredLevel(itemLink) .. ", CP-Level: " .. GetItemLinkRequiredVeteranRank(itemLink))

            if allowedItemTypesForLabel[itemType] then
	        	local level = GetItemLinkRequiredLevel(itemLink)
	            if level == 1 and itemLevel ~= -1 then
					level = itemLevel
	            end
				local vetLevel = GetItemLinkRequiredVeteranRank(itemLink)
                if vetLevel > 0 then
					level = level + vetLevel
				end
				--Output the level label
	            local levelStr = ""
				--Veteran ranks?
	            if level > 50 then
	            	levelStr = veteranLevelToLevelStr[level]
	            else
	            	levelStr = tostring(level)
	            end

				--Error prevention
                if levelStr == nil or levelStr == "" then
                	return nil
                end

                DisplayTheLabel(rowControl, label, offset, "["..levelStr.."]")
            end
        end
    end
end

-- See PLAYER_INVENTORY["bagToInventoryType"] for the mappings from bag constants to inventory IDs
local inventoryLists = {
    ["backpackListView"] = PLAYER_INVENTORY.inventories[1].listView,
    ["bankListView"] = PLAYER_INVENTORY.inventories[3].listView,
    ["guildBankListView"] = PLAYER_INVENTORY.inventories[4].listView,
    ["enchantingTableListView"] = ENCHANTING.inventory.list,
	["craftBagListView"] = PLAYER_INVENTORY.inventories[5].listView,
}

local function HookNormalInventoryLists()
    for _,inventoryList in pairs(inventoryLists) do
        if inventoryList and inventoryList.dataTypes and inventoryList.dataTypes[1] then
			--Check if player got craft bag access, or if the inventory hook is not for the craft bag
        	if (inventoryList ~= PLAYER_INVENTORY.inventories[5].listView) or (inventoryList == PLAYER_INVENTORY.inventories[5].listView and HasCraftBagAccess()) then
				--Hook the inventory list
	            local existingCallbackFunction = inventoryList.dataTypes[1].setupCallback
	            inventoryList.dataTypes[1].setupCallback = function(rowControl, slot)
	                existingCallbackFunction(rowControl, slot)
	                local slotIndex = "slotIndex" and slot["slotIndex"] or nil
	                local bagId = slot["bagId"]
	                local itemId = CraftingMaterialLevelDisplay.GetItemIdFromBagAndSlot(bagId, slotIndex)
	                local tradeSkillType, itemType = GetItemCraftingInfo(bagId, slotIndex)
	                local itemLink = GetItemLink(bagId, slotIndex)
	                local itemLevel = GetItemLevel(bagId, slotIndex)
	                AddCraftingMaterialLevelToInventoryRow(rowControl, tradeSkillType, itemType, itemId, CraftingMaterialLevelDisplay.savedVariables.inventoryOffset, itemLink, itemLevel)
	            end
			end
        end
    end
end

local function HookLootWindowInventoryList()
    local existingCallbackFunction = LOOT_WINDOW.list.dataTypes[1].setupCallback
    LOOT_WINDOW.list.dataTypes[1].setupCallback = function(rowControl, slot)
        existingCallbackFunction(rowControl, slot)
        local itemLink = GetLootItemLink(slot.lootId)
        local itemId = CraftingMaterialLevelDisplay.GetItemIdFromLink(itemLink)
        local tradeSkillType = GetItemLinkCraftingSkillType(itemLink)
        AddCraftingMaterialLevelToInventoryRow(rowControl, tradeSkillType, nil, itemId, CraftingMaterialLevelDisplay.savedVariables.lootOffset, itemLink, -1)
    end
end

function CraftingMaterialLevelDisplay.HookInventoryLists()
    if CraftingMaterialLevelDisplay.savedVariables.showLevelsInInventoryLists then
        HookNormalInventoryLists()
        HookLootWindowInventoryList()
    end
end
