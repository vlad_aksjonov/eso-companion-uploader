WoodworkingMaterials = {
    [802] = {level = "1-14"}, -- rough maple
    [803] = {level = "1-14"}, -- sanded maple

    [521] = {level = "16-24"}, -- rough oak
    [533] = {level = "16-24"}, -- sanded oak

    [23117] = {level = "26-34"}, -- rough beech
    [23121] = {level = "26-34"}, -- sanded beech

    [23118] = {level = "36-44"}, -- rough hickory
    [23122] = {level = "36-44"}, -- sanded hickory

    [23119] = {level = "46-50"}, -- rough yew
    [23123] = {level = "46-50"}, -- sanded yew

    [818] = {level = "CP10-CP30"}, -- rough birch
    [46139] = {level = "CP10-CP30"}, -- sanded birch

    [4439] = {level = "CP40-CP60"}, -- rough ash
    [46140] = {level = "CP40-CP60"}, -- sanded ash

    [23137] = {level = "CP70-CP80"}, -- rough mahogany
    [46141] = {level = "CP70-CP80"}, -- sanded mahogany

    [23138] = {level = "CP90-CP140"}, -- rough nightwood
    [46142] = {level = "CP90-CP140"}, -- sanded nightwood

    [71199] = {level = "CP150-CP160"}, -- rough ruby ash
    [64502] = {level = "CP150-CP160"}, -- sanded ruby ash
}
