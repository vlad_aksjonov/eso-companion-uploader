AlchemyMaterials = {
    -- Credit goes to SilverWF from esoui for adding the Alchemy material information. Thank you!

    -- Reagents
    [30157] = {tooltip = "|c00FF00Restore stamina, Increase weapon power, Speed,|r |cFF0000Ravage health|r", level = "-1"}, -- Blessed Thistle
    [30148] = {tooltip = "|c00FF00Restore health, Invisible,|r |cFF0000Ravage magicka, Lower spell power|r", level = "-1"}, -- Blue Entoloma
    [30160] = {tooltip = "|c00FF00Restore health, Restore magicka, Increase spell resist,|r |cFF0000Lower spell power|r", level = "-1"}, -- Bugloss
    [30164] = {tooltip = "|c00FF00Restore health, Restore magicka, Restore stamina, Unstoppable|r", level = "-1"}, -- Columbine
    [30161] = {tooltip = "|c00FF00Restore magicka, Increase spell power, Detection,|r |cFF0000Ravage health|r", level = "-1"}, -- Corn Flower
    [30162] = {tooltip = "|c00FF00Restore stamina, Increase weapon power, Weapon crit,|r |cFF0000Lower armor|r", level = "-1"}, -- Dragonthorn
    [30151] = {tooltip = "|cFF0000Ravage health, Ravage magicka, Ravage stamina, Stun|r", level = "-1"}, -- Emetic Russula
    [30156] = {tooltip = "|c00FF00Increase armor,|r |cFF0000Ravage stamina, Lower weapon power, Lower weapon crit|r", level = "-1"}, -- Imp Stool
    [30158] = {tooltip = "|c00FF00Restore magicka, Increase spell power, Spell crit,|r |cFF0000Lower spell resist|r", level = "-1"}, -- Lady's Smock
    [30155] = {tooltip = "|c00FF00Restore health,|r |cFF0000Ravage stamina, Lower weapon power, Reduce speed|r", level = "-1"}, -- Luminous Russula
    [30163] = {tooltip = "|c00FF00Resore health, Restore stamina, Increase armor,|r |cFF0000Lower weapon power|r", level = "-1"}, -- Mountain Flower
    [30153] = {tooltip = "|c00FF00Spell crit, Unstoppable, Speed, Invisible|r", level = "-1"}, -- Namira's Rot
    [30165] = {tooltip = "|c00FF00Invisible,|r |cFF0000Ravage health, Lower spell crit, Lower weapon crit|r", level = "-1"}, -- Nirnroot
    [30149] = {tooltip = "|c00FF00Increase weapon power,|r |cFF0000Ravage health, Ravage stamina, Lower armor|r", level = "-1"}, -- Stinkhorn
    [30152] = {tooltip = "|c00FF00Increase spell power,|r |cFF0000Ravage health, Ravage magicka, Lower spell resist|r", level = "-1"}, -- Violet Coprinus
    [30166] = {tooltip = "|c00FF00Restore health, Weapon crit, Spell crit,|r |cFF0000Stun|r", level = "-1"}, -- Water Hyacinth
    [30154] = {tooltip = "|c00FF00Increase spell resist,|r |cFF0000Ravage magicka, Lower spell power, Lower spell crit|r", level = "-1"}, -- White Cap
    [30159] = {tooltip = "|c00FF00Weapon Crit, Detection, Unstoppable,|r |cFF0000Reduce Speed|r", level = "-1"}, -- Wormwood

    -- Solvents
    [883] = {solvent = true, level = "3"}, -- Natural Water
    [1187] = {solvent = true, level = "10"}, -- Clear Water
    [4570] = {solvent = true, level = "20"}, -- Pristine Water
    [23265] = {solvent = true, level = "30"}, -- Cleansed Water
    [23266] = {solvent = true, level = "40"}, -- Filtered Water
    [23267] = {solvent = true, level = "CP10"}, -- Purified Water
    [23268] = {solvent = true, level = "CP50"}, -- Cloud Mist
    [64501] = {solvent = true, level = "CP150"}, -- Lorkhan's tears
}
