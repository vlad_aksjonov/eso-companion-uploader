LMP = LibStub:GetLibrary('LibMediaProvider-1.0')

local inventoryToSortHeader = {
   	[INVENTORY_BACKPACK] 			= ZO_PlayerInventorySortBy,
   	[INVENTORY_BANK] 				= ZO_PlayerBankSortBy,
   	[INVENTORY_GUILD_BANK] 			= ZO_GuildBankSortBy,
   	[INVENTORY_CRAFT_BAG] 			= ZO_CraftBagSortBy,
}

CraftingMaterialLevelDisplay = {
	name			= "CraftingMaterialLevelDisplay",
    versionString 	= "2g",
	fontStyles = {
		"none",
		"outline",
		"thin-outline",
		"thick-outline",
		"shadow",
		"soft-shadow-thin",
		"soft-shadow-thick",
	},
	defaultSavedVariables = {
		--provisioning = true,
		--provisioningFlavor = true,
		alchemy = true,
		enchanting = true,
		showLevelsInInventoryLists = true,
		blacksmithingInventoryList = true,
		clothingInventoryList = true,
		enchantingInventoryList = true,
		woodworkingInventoryList = true,
        --provisioningInventoryList = true,
        font = {
        	family 	= "Univers 55",
            size    = 14,
            style	= "none",
            color	= {
            	["r"] = 1,
                ["g"] = 1,
                ["b"] = 1,
                ["a"] = 1
            }
        },
		levelSortHeader = true,
        normalInventoryList = true,
        inventoryOffset = -100,
        lootOffset = -20,
	},
	currentInventoryRows = {}
}

function CraftingMaterialLevelDisplay.GetItemIdFromLink(itemLink)
    local itemId = select(4, ZO_LinkHandler_ParseLink(itemLink))
    return tonumber(itemId)
end

function CraftingMaterialLevelDisplay.GetItemIdFromBagAndSlot(bagId, slotIndex)
    local itemLink = GetItemLink(bagId, slotIndex)
    return CraftingMaterialLevelDisplay.GetItemIdFromLink(itemLink)
end

local function UpdateSortHeader(headerCtrl)
	if headerCtrl == nil then
--d("[UpdateSortHeader] All inventories")
		--All inventories
        for _, sortParent in pairs(inventoryToSortHeader) do
		    local levelHeader = WINDOW_MANAGER:GetControlByName(sortParent:GetName().."Level", "")
            if levelHeader then
--d(">> Header: " .. levelHeader:GetName())
	            levelHeader:SetHidden(not CraftingMaterialLevelDisplay.savedVariables.levelSortHeader)
				levelHeader:SetMouseEnabled(CraftingMaterialLevelDisplay.savedVariables.levelSortHeader)
            end
        end

    else
--d("[UpdateSortHeader] " .. headerCtrl:GetName())
		--Only a special inventory
		headerCtrl:SetHidden(not CraftingMaterialLevelDisplay.savedVariables.levelSortHeader)
		headerCtrl:SetMouseEnabled(CraftingMaterialLevelDisplay.savedVariables.levelSortHeader)
    end
end

local function GetItemMinMaxLevel(tradeSkillType, itemType, itemId)
	local itemLevelMin = -1
    local itemLevelMax = -1
-- d("[GetItemMinMaxLevel] Start - tradeSkillType: " .. tostring(tradeSkillType) .. ", itemType: " .. tostring(itemType) .. ", itemId: " .. tostring(itemId))
    if   tradeSkillType == nil or itemType == nil or itemId == nil
	  or tradeSkillType == 0 or itemType == 0 or itemId == 0 then
    	return itemLevelMin, itemLevelMax
    end

    local levelStr  = ""

    if tradeSkillType == CRAFTING_TYPE_CLOTHIER then
-- d("[GetItemMinMaxLevel] Clothier")
		if ClothingMaterials[itemId] and ClothingMaterials[itemId].level ~= nil then
			levelStr = ClothingMaterials[itemId].level
		end

    elseif tradeSkillType == CRAFTING_TYPE_BLACKSMITHING then
-- d("[GetItemMinMaxLevel] Blacksmith")
		if BlacksmithingMaterials[itemId] and BlacksmithingMaterials[itemId].level ~= nil then
			levelStr = BlacksmithingMaterials[itemId].level
		end

    elseif tradeSkillType == CRAFTING_TYPE_WOODWORKING then
-- d("[GetItemMinMaxLevel] Woodworking")
		if WoodworkingMaterials[itemId] and WoodworkingMaterials[itemId].level ~= nil then
			levelStr = WoodworkingMaterials[itemId].level
		end

    elseif tradeSkillType == CRAFTING_TYPE_ENCHANTING then
--d("[GetItemMinMaxLevel] Enchanting")
		if EnchantingMaterials[itemId] and EnchantingMaterials[itemId].level ~= nil then
			levelStr = EnchantingMaterials[itemId].level
		end
	--[[
    elseif tradeSkillType == CRAFTING_TYPE_PROVISIONING then
-- d("[GetItemMinMaxLevel] Provision")
		if ProvisioningMaterials[itemId] and ProvisioningMaterials[itemId].level ~= nil and ProvisioningMaterials[itemId].level ~= "-1" then
			levelStr = ProvisioningMaterials[itemId].level
		end
	]]--
    end

-- d("[GetItemMinMaxLevel] LevelStr: " .. levelStr)

	--Split the level string into min and max values
    if levelStr ~= "" and levelStr ~= "-1" then
        --Remove spaces from levelStr
        levelStr = levelStr:gsub("%s+", "")
-- d("[GetItemMinMaxLevel] LevelStr without spaces: " .. levelStr)
		--Set offset for search start
        local untilHereStart
        local untilHereEnd
		local itemLevelMinStr
		local itemLevelMaxStr
		--Find the "-" character
        untilHereStart, untilHereEnd = string.find (levelStr, "-")
        --Was the "-" character found?
        if untilHereStart ~= nil then
--d("[GetItemMinMaxLevel] - found at " .. tostring(untilHereStart))
			--"-" character found -> Level range "from - to"
	    	itemLevelMinStr = string.sub(levelStr, 1, untilHereStart-1)
	        itemLevelMaxStr = string.sub(levelStr, untilHereStart+1)
        else
--d("[GetItemMinMaxLevel] - not found")
			--"-" character not found -> Only one level
	    	itemLevelMinStr = levelStr
	        itemLevelMaxStr = itemLevelMinStr
        end
--d("[GetItemMinMaxLevel] minStr: " .. itemLevelMinStr .. ", maxStr: " .. itemLevelMaxStr)

        --Change string values to numbers (or try it, as levels could be strings like "V2" for veteran ranks)
        itemLevelMin = tonumber(itemLevelMinStr)
        itemLevelMax = tonumber(itemLevelMaxStr)
        local vetRankLevel
		--The maximum player level for normal levels (< Veteran ranks)
        local maxNormalLevel = 50
        --Check if values are numbers
        if itemLevelMin == nil then
			vetRankLevel = 0
			--No number -> Veteran rank
            --Convert veteran rank level to number
            vetRankLevel = tonumber(string.sub(itemLevelMinStr, 3))
--d("[GetItemMinMaxLevel] vetLevelMin: " .. tostring(vetRankLevel))
            itemLevelMin = maxNormalLevel + vetRankLevel
        end
        if itemLevelMax == nil then
			vetRankLevel = 0
			--No number -> Veteran rank
            --Convert veteran rank level to number
            vetRankLevel = tonumber(string.sub(itemLevelMaxStr, 3))
--d("[GetItemMinMaxLevel] vetLevelMax: " .. tostring(vetRankLevel))
            itemLevelMax = maxNormalLevel + vetRankLevel
        end
    end

--d("[GetItemMinMaxLevel] Min: " .. tostring(itemLevelMin) .. ", Max: " .. tostring(itemLevelMax))

	return itemLevelMin, itemLevelMax
end

local function HideCurrentInventoryRows()
    for _,labelControl in pairs(CraftingMaterialLevelDisplay.currentInventoryRows) do
        labelControl:SetHidden(true)
    end
end

function CraftingMaterialLevelDisplay.updateLabels(labelCtrl, isLootWindow)
	isLootWindow = isLootWindow or false
	local offset = 0
    if isLootWindow then
    	offset = CraftingMaterialLevelDisplay.savedVariables.lootOffset
    else
    	offset = CraftingMaterialLevelDisplay.savedVariables.inventoryOffset
    end

    local color = CraftingMaterialLevelDisplay.savedVariables.font.color
    local fontPath = LMP:Fetch('font', CraftingMaterialLevelDisplay.savedVariables.font.family)
	local fontString = string.format('%s|%u|%s', fontPath, CraftingMaterialLevelDisplay.savedVariables.font.size, CraftingMaterialLevelDisplay.savedVariables.font.style)
	--local fontString = "ZoFontGame"
	if labelCtrl == nil then
	    for _,labelControl in pairs(CraftingMaterialLevelDisplay.currentInventoryRows) do
	        labelControl:SetFont(fontString)
	        labelControl:SetColor(color.r, color.g, color.b, color.a)
            labelControl:ClearAnchors()
            labelControl:SetAnchor(RIGHT, labelControl:GetParent(), RIGHT, offset)
	    end
    else
	    labelCtrl:SetFont(fontString)
	    labelCtrl:SetColor(color.r, color.g, color.b, color.a)
        labelCtrl:ClearAnchors()
        labelCtrl:SetAnchor(RIGHT, labelCtrl:GetParent(), RIGHT, offset)
    end

end

local function BuildAddonMenu()

	local LAM = LibStub:GetLibrary("LibAddonMenu-2.0")
	
    CraftingMaterialLevelDisplay.savedVariables = ZO_SavedVars:New("CMLD_SavedVariables", 1, nil, CraftingMaterialLevelDisplay.defaultSavedVariables)
	
	local panelData = {
		type = "panel",
		name = "Crafting Material Level Display",
		displayName = ZO_HIGHLIGHT_TEXT:Colorize("Crafting Material Level Display"),
		author = "Marihk, Ayantir & Baertram",
		version = CraftingMaterialLevelDisplay.versionString,
		slashCommand = "/cmld",
		registerForRefresh = true,
		registerForDefaults = true,
	}
	
   	LAM:RegisterAddonPanel(CraftingMaterialLevelDisplay.name .. "ControlPanel", panelData)
	
	--[[
	local fontPreview = function(panel)
		if panel == CMLDSettingsPanel then
			previewLabel = WINDOW_MANAGER:CreateControl(nil, panel.controlsToRefresh[10], CT_LABEL)
			previewLabel:SetAnchor(RIGHT, panel.controlsToRefresh[10].dropdown:GetControl(), LEFT, -10, 0)
            previewLabel:SetText("[CP100-CP140]")
            previewLabel:SetHidden(false)
            CraftingMaterialLevelDisplay.updateLabels(previewLabel)
			CALLBACK_MANAGER:UnregisterCallback("LAM-RefreshPanel", fontPreview)
		end
	end
	
	CALLBACK_MANAGER:RegisterCallback("LAM-PanelControlsCreated", fontPreview)
	
	]]--
	
	local optionsTable = {
		
		--[[
		
		{
			type = "checkbox",
			name = "Show Provisioning tooltips",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.provisioning end,
			setFunc = function(newValue) CraftingMaterialLevelDisplay.savedVariables.provisioning = newValue end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.provisioning,
		},
		{
			type = "checkbox",
			name = "Use Flavor text instead of Tier 2/3 text",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.provisioningFlavor end,
			setFunc = function(newValue) CraftingMaterialLevelDisplay.savedVariables.provisioningFlavor = newValue end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.provisioningFlavor,
		},
		
		]]--
		
		{
			type = "checkbox",
			name = "Show Alchemy tooltips",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.alchemy end,
			setFunc = function(newValue) CraftingMaterialLevelDisplay.savedVariables.alchemy = newValue end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.alchemy,
		},
		{
			type = "checkbox",
			name = "Show Enchanting tooltips",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.enchanting end,
			setFunc = function(newValue) CraftingMaterialLevelDisplay.savedVariables.enchanting = newValue end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.enchanting,
		},
		{
			type = "checkbox",
			name = "Show levels in inventory lists",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.showLevelsInInventoryLists end,
			setFunc = function(newValue) CraftingMaterialLevelDisplay.savedVariables.showLevelsInInventoryLists = newValue end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.showLevelsInInventoryLists,
			warning = "Requires /reloadui to take effect, prevents the inventory list hook code from even executing, useful if errors happen"
		},
		{
			type = "checkbox",
			name = "Show Enchanting levels in inventory",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.enchantingInventoryList end,
			setFunc = function(newValue)
				CraftingMaterialLevelDisplay.savedVariables.enchantingInventoryList = newValue
				if newValue == false then
					HideCurrentInventoryRows()
				end
			end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.enchantingInventoryList,
		},
		{
			type = "checkbox",
			name = "Show Blacksmithing levels in inventory",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.blacksmithingInventoryList end,
			setFunc = function(newValue)
				CraftingMaterialLevelDisplay.savedVariables.blacksmithingInventoryList = newValue
				if newValue == false then
					HideCurrentInventoryRows()
				end
			end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.blacksmithingInventoryList,
		},
		{
			type = "checkbox",
			name = "Show Clothing levels in inventory",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.clothingInventoryList end,
			setFunc = function(newValue)
				CraftingMaterialLevelDisplay.savedVariables.clothingInventoryList = newValue
				if newValue == false then
					HideCurrentInventoryRows()
				end
			end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.clothingInventoryList,
		},
		{
			type = "checkbox",
			name = "Show Woodworking levels in inventory",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.woodworkingInventoryList end,
			setFunc = function(newValue)
				CraftingMaterialLevelDisplay.savedVariables.woodworkingInventoryList = newValue
				if newValue == false then
					HideCurrentInventoryRows()
				end
			end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.woodworkingInventoryList,
		},
		
		--[[
		
		{
			type = "checkbox",
			name = "Show Provisioning levels in inventory",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.provisioningInventoryList end,
			setFunc = function(newValue)
				CraftingMaterialLevelDisplay.savedVariables.provisioningInventoryList = newValue
				if newValue == false then
					HideCurrentInventoryRows()
				end
			end,
			default = CraftingMaterialLevelDisplay.defaultSavedVariables.provisioningInventoryList,
		},
		
		]]--
		
		{
			type = "checkbox",
			name = "Show normal items levels in inventory",
            tooltip = "Also show item levels for weapons, armor, etc.",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.normalInventoryList end,
			setFunc = function(newValue)
				CraftingMaterialLevelDisplay.savedVariables.normalInventoryList = newValue
				if newValue == false then
					HideCurrentInventoryRows()
				end
			end,
			default = CraftingMaterialLevelDisplay.savedVariables.normalInventoryList,
		},
		{
			type = 'dropdown',
			name = "Font family",
			choices = LMP:List('font'),
            getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.font.family end,
            setFunc = function(value) CraftingMaterialLevelDisplay.savedVariables.font.family = value
				CraftingMaterialLevelDisplay.updateLabels(previewLabel)
            	CraftingMaterialLevelDisplay.updateLabels()
            end,
			default = CraftingMaterialLevelDisplay.savedVariables.font.family,
		},
		{
			type = "slider",
			name = "Font size",
			min = 10,
			max = 48,
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.font.size end,
			setFunc = function(size) CraftingMaterialLevelDisplay.savedVariables.font.size = size
				CraftingMaterialLevelDisplay.updateLabels(previewLabel)
				CraftingMaterialLevelDisplay.updateLabels()
 			end,
			default = CraftingMaterialLevelDisplay.savedVariables.font.size,
		},
		{
			type = 'dropdown',
			name = "Font style",
			choices = CraftingMaterialLevelDisplay.fontStyles,
            getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.font.style end,
            setFunc = function(value) CraftingMaterialLevelDisplay.savedVariables.font.style = value
				CraftingMaterialLevelDisplay.updateLabels(previewLabel)
            	CraftingMaterialLevelDisplay.updateLabels()
            end,
			default = CraftingMaterialLevelDisplay.savedVariables.font.style,
		},
		{
			type = "colorpicker",
			name = "Font color",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.font.color.r, CraftingMaterialLevelDisplay.savedVariables.font.color.g, CraftingMaterialLevelDisplay.savedVariables.font.color.b, CraftingMaterialLevelDisplay.savedVariables.font.color.a end,
            setFunc = function(r,g,b,a) CraftingMaterialLevelDisplay.savedVariables.font.color = {["r"] = r, ["g"] = g, ["b"] = b, ["a"] = a}
				CraftingMaterialLevelDisplay.updateLabels(previewLabel)
	            CraftingMaterialLevelDisplay.updateLabels()
			end,
            default = CraftingMaterialLevelDisplay.savedVariables.font.color,
		},
		{
			type = "checkbox",
			name = "Add 'Level' sort header in inventory",
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.levelSortHeader end,
			setFunc = function(newValue)
				CraftingMaterialLevelDisplay.savedVariables.levelSortHeader = newValue
				UpdateSortHeader()
			end,
			default = CraftingMaterialLevelDisplay.savedVariables.levelSortHeader,
		},
		{
			type = "slider",
			name = "Offset of level in inventory",
			min = -540,
			max = 0,
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.inventoryOffset end,
			setFunc = function(size) CraftingMaterialLevelDisplay.savedVariables.inventoryOffset = size
				CraftingMaterialLevelDisplay.updateLabels()
 			end,
			default = CraftingMaterialLevelDisplay.savedVariables.inventoryOffset,
		},
		{
			type = "slider",
			name = "Offset of level in loot window",
			min = -540,
			max = 0,
			getFunc = function() return CraftingMaterialLevelDisplay.savedVariables.lootOffset end,
			setFunc = function(size) CraftingMaterialLevelDisplay.savedVariables.lootOffset = size
				CraftingMaterialLevelDisplay.updateLabels(nil, true)
 			end,
			default = CraftingMaterialLevelDisplay.savedVariables.lootOffset,
		},
	}
	LAM:RegisterOptionControls(CraftingMaterialLevelDisplay.name .. "ControlPanel", optionsTable)
end

local function InitializeSavedVariables()
    CraftingMaterialLevelDisplay.savedVariables = ZO_SavedVars:New("CMLD_SavedVariables", 1, nil,
        CraftingMaterialLevelDisplay.defaultSavedVariables)
end

local function orderByItemLevel(data1, data2)
    local minLevel1
    local minLevel2
    local maxLevel1
    local maxLevel2
    local vetLevel

	local link   = GetItemLink(data1.bagId, data1.slotIndex)
    local itemId = CraftingMaterialLevelDisplay.GetItemIdFromLink(link)
    local tradeSkillType, itemType = GetItemCraftingInfo(data1.bagId, data1.slotIndex)
	minLevel1, maxLevel1 = GetItemMinMaxLevel(tradeSkillType, itemType, itemId)
	if minLevel1 == -1 and maxLevel1 == -1 then
		minLevel1 = GetItemLinkRequiredLevel(link)
		vetLevel  = GetItemLinkRequiredVeteranRank(link)
		if vetLevel > 0 then
			minLevel1 = minLevel1 + vetLevel
		end
	    if minLevel1 == 1 then
	    	minLevel1 = GetItemLevel(data1.bagId, data1.slotIndex)
	    end
    end

    link = GetItemLink(data2.bagId, data2.slotIndex)
    itemId = CraftingMaterialLevelDisplay.GetItemIdFromLink(link)
    tradeSkillType, itemType = GetItemCraftingInfo(data2.bagId, data2.slotIndex)
	minLevel2, maxLevel2 = GetItemMinMaxLevel(tradeSkillType, itemType, itemId)
	if minLevel2 == -1 and maxLevel2 == -1 then
		minLevel2 = GetItemLinkRequiredLevel(link)
		vetLevel  = GetItemLinkRequiredVeteranRank(link)
		if vetLevel > 0 then
			minLevel2 = minLevel2 + vetLevel
		end
	    if minLevel2 == 1 then
	    	minLevel2 = GetItemLevel(data2.bagId, data2.slotIndex)
	    end
    end

--d("data1: " .. data1.name .. ", minLevel1: " .. minLevel1 .. ", maxLevel1: " .. maxLevel1 .. " / data2: " .. data2.name .. ", minLevel2: " .. minLevel2.. ", maxLevel2: " .. maxLevel2)

	local sortReturn

    if minLevel1 == maxLevel1 and minLevel2 == maxLevel2 then
	    if minLevel1 < minLevel2 then
--d("1")
	    	sortReturn = true
		else
--d("2")
	    	sortReturn = false
        end
    elseif minLevel1 == minLevel2 and maxLevel1 == maxLevel2 then
--d("3")
    	sortReturn = false
        --Insert alphabetical sort here?
    elseif maxLevel1 ~= -1 and maxLevel2 ~= -1 then
	    if minLevel1 < minLevel2 and maxLevel1 < maxLevel2 then
--d("4")
	    	sortReturn = true
	    elseif minLevel1 < minLevel2 and maxLevel1 == maxLevel2 then
--d("5")
	    	sortReturn = true
	    elseif minLevel1 == minLevel2 and maxLevel1 < maxLevel2 then
--d("6")
	    	sortReturn = true
		else
--d("7")
	    	sortReturn = false
        end
    elseif maxLevel1 == -1 and maxLevel2 == -1 then
	    if minLevel1 < minLevel2 then
--d("8")
	    	sortReturn = true
		else
--d("9")
	    	sortReturn = false
        end
    elseif maxLevel1 ~= -1 and maxLevel2 == -1 then
	    if minLevel1 < minLevel2 or minLevel2 < maxLevel1 then
--d("10")
	    	sortReturn = true
		else
--d("11")
	    	sortReturn = false
        end
    elseif maxLevel1 == -1 and maxLevel2 ~= -1 then
	    if minLevel1 < minLevel2 or minLevel1 < maxLevel2 then
--d("13")
	    	sortReturn = true
		else
--d("14")
	    	sortReturn = false
        end
    else
	    if minLevel1 < minLevel2 and maxLevel1 < maxLevel2 then
--d("15")
	    	sortReturn = true
	    elseif minLevel1 < minLevel2 then
--d("16")
	    	sortReturn = true
	    elseif maxLevel1 < maxLevel2 then
--d("17")
	    	sortReturn = true
		else
--d("18")
	    	sortReturn = false
	    end
	end

    return sortReturn
end

local function initializeCustomInventorySortFn(inventory)
    inventory.sortFn = function(entry1, entry2)
        local sortKey = inventory.currentSortKey
        local sortOrder = inventory.currentSortOrder
        local res
        if type(sortKey) == "function" then
            if inventory.currentSortOrder == ZO_SORT_ORDER_UP then
                res = sortKey(entry1.data, entry2.data)
            else
                res = sortKey(entry2.data, entry1.data)
            end
        else
            local sortKeys = ZO_Inventory_GetDefaultHeaderSortKeys()
            res = ZO_TableOrderingFunction(entry1.data, entry2.data, sortKey, sortKeys, sortOrder)
        end
        return res
    end
end

-- call from EVENT_ADD_ON_LOADED
local function HookInventorySortByLevel(inv)
--d("[CMLD] HookInventorySortByLevel, inv: " .. tostring(inv) .. ", parentName: " .. inventoryToSortHeader[inv]:GetName())
    local invSortByParentControl = inventoryToSortHeader[inv]
    local nameHeader = invSortByParentControl:GetNamedChild("Name")
    local levelHeader = CreateControlFromVirtual("$(parent)Level", invSortByParentControl, "ZO_SortHeader")
    levelHeader:SetAnchor(RIGHT, nameHeader, RIGHT, -30, 0)
    levelHeader:SetDimensions(80, 20)
    ZO_SortHeader_Initialize(levelHeader, "Level", orderByItemLevel,
                             ZO_SORT_ORDER_UP, TEXT_ALIGN_RIGHT, "ZoFontHeader")

    local inventory = PLAYER_INVENTORY.inventories[inv]
    initializeCustomInventorySortFn(inventory)
    inventory.sortHeaders:AddHeader(levelHeader)
--d("HookInventorySortByLevel--->")
	UpdateSortHeader(levelHeader)
end

local function onLoad(_, name)
    if name ~= CraftingMaterialLevelDisplay.name then return end
    EVENT_MANAGER:UnregisterForEvent(CraftingMaterialLevelDisplay.name, EVENT_ADD_ON_LOADED);
    InitializeSavedVariables()
    BuildAddonMenu()
    CraftingMaterialLevelDisplay.HookTooltips()
    CraftingMaterialLevelDisplay.HookInventoryLists()
    --Add sort by level to inventory
	HookInventorySortByLevel(INVENTORY_BACKPACK)
	HookInventorySortByLevel(INVENTORY_BANK)
	HookInventorySortByLevel(INVENTORY_GUILD_BANK)
	--Does the player have access to the craft bag?
	if HasCraftBagAccess() then
		HookInventorySortByLevel(INVENTORY_CRAFT_BAG)
    end
end

function CMLD_GetItemLevel(tradeSkillType, itemType, itemId)
-- d("[CMLD] Start - tradeSkillType: " .. tostring(tradeSkillType) .. ", itemType: " .. tostring(itemType) .. ", itemId: " .. tostring(itemId))
	itemLevelMin, itemLevelMax = GetItemMinMaxLevel(tradeSkillType, itemType, itemId)

--d("[CMLD_GetItemLevel] Min: " .. tostring(itemLevelMin) .. ", Max: " .. tostring(itemLevelMax))
	return itemLevelMin, itemLevelMax
end

EVENT_MANAGER:RegisterForEvent(CraftingMaterialLevelDisplay.name, EVENT_ADD_ON_LOADED, onLoad)
