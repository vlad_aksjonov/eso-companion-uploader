ClothingMaterials = {
    [812] = {level = "1-14"}, -- raw jute
    [811] = {level = "1-14"}, -- jute
    [4464] = {level = "16-24"}, -- raw flax
    [4463] = {level = "16-24"}, -- flax
    [23129] = {level = "26-34"}, -- raw cotton
    [23125] = {level = "26-34"}, -- cotton
    [23130] = {level = "36-44"}, -- raw spidersilk
    [23126] = {level = "36-44"}, -- spidersilk
    [23131] = {level = "46-50"}, -- raw ebonthread
    [23127] = {level = "46-50"}, -- ebonthread
    [33217] = {level = "CP10-CP30"}, -- raw kreshweed
    [46131] = {level = "CP10-CP30"}, -- kresh fiber
    [33218] = {level = "CP40-CP60"}, -- raw ironweed
    [46132] = {level = "CP40-CP60"}, -- ironthread
    [33219] = {level = "CP70-CP80"}, -- raw silverweed
    [46133] = {level = "CP70-CP80"}, -- silverweave
    [33220] = {level = "CP90-CP140"}, -- raw void bloom
    [46134] = {level = "CP90-CP140"}, -- void cloth
    [71200] = {level = "CP150-CP160"}, -- raw ancestor silk
    [64504] = {level = "CP150-CP160"}, -- ancestor silk


    [793] = {level = "1-14"}, -- rawhide scraps
    [794] = {level = "1-14"}, -- rawhide
    [4448] = {level = "16-24"}, -- hide scraps
    [4447] = {level = "16-24"}, -- hide
    [23095] = {level = "26-34"}, -- leather scraps
    [23099] = {level = "26-34"}, -- leather
    [6020] = {level = "36-44"}, -- thick leather scraps
    [23100] = {level = "36-44"}, -- thick leather
    [23097] = {level = "46-50"}, -- fell hide scraps
    [23101] = {level = "46-50"}, -- fell hide
    [23142] = {level = "CP10-CP30"}, -- topgrain hide scraps
    [46135] = {level = "CP10-CP30"}, -- topgrain hide
    [23143] = {level = "CP40-CP60"}, -- iron hide scraps
    [46136] = {level = "CP40-CP60"}, -- iron hide
    [800] = {level = "CP70-CP80"}, -- superb hide scraps
    [46137] = {level = "CP70-CP80"}, -- superb hide
    [4478] = {level = "CP90-CP140"}, -- shadowhide scraps
    [46138] = {level = "CP90-CP140"}, -- shadowhide
    [71239] = {level = "CP150-CP160"}, -- rubedo hide scraps
    [64506] = {level = "CP150-CP160"}, -- rubedo leather
}
