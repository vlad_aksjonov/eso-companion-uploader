BlacksmithingMaterials = {
    [808] = {level = "1-14"}, -- iron ore
    [5413] = {level = "1-14"}, -- iron ingot

    [5820] = {level = "16-24"}, -- high iron ore
    [4487] = {level = "16-24"}, -- steel ingot

    [23103] = {level = "26-34"}, -- orichalcum ore
    [23107] = {level = "26-34"}, -- orichalcum ingot

    [23104] = {level = "36-44"}, -- dwarven ore
    [6000] = {level = "36-44"}, -- dwarven ingot

    [23105] = {level = "46-50"}, -- ebony ore
    [6001] = {level = "46-50"}, -- ebony ingot

    [4482] = {level = "CP10-CP30"}, -- calcinium ore
    [46127] = {level = "CP10-CP30"}, -- calcinium ingot

    [23133] = {level = "CP40-CP60"}, -- galatite ore
    [46128] = {level = "CP40-CP60"}, -- galatite ingot

    [23134] = {level = "CP70-CP80"}, -- quicksilver ore
    [46129] = {level = "CP70-CP80"}, -- quicksilver ingot

    [23135] = {level = "CP90-CP140"}, -- voidstone ore
    [46130] = {level = "CP90-CP140"}, -- voidstone ingot

    [71198] = {level = "CP150-CP160"}, -- rubedite ore
    [64489] = {level = "CP150-CP160"}, -- rubedite ingot
}
