; This Add-on is not created by, affiliated with or sponsored by ZeniMax
; Media Inc. or its affiliates. The Elder Scrolls� and related logos are
; registered trademarks or trademarks of ZeniMax Media Inc. in the United
; States and/or other countries. All rights reserved.
; You can read the full terms at https://account.elderscrollsonline.com/add-on-terms

## Title: Crafting Material Level Display
## Description: Displays the level of crafting materials in the item tooltips (as viewed from your bag or bank).
## Version: 2g
## Author: Ayantir, modified by Baertram
## APIVersion: 100015
## SavedVariables: CMLD_SavedVariables
## OptionalDependsOn: LibAddonMenu-2.0 LibMediaProvider-1.0
## FastAPI : 100015

lib/LibStub/LibStub.lua
lib/LibAddonMenu-2.0/LibAddonMenu-2.0.lua
lib/LibAddonMenu-2.0/controls/panel.lua
lib/LibAddonMenu-2.0/controls/checkbox.lua
lib/LibAddonMenu-2.0/controls/colorpicker.lua
lib/LibAddonMenu-2.0/controls/dropdown.lua
lib/LibAddonMenu-2.0/controls/slider.lua
lib/LibMediaProvider-1.0/LibMediaProvider-1.0.lua

CraftingMaterialLevelDisplay.lua
AlchemyMaterials.lua
BlacksmithingMaterials.lua
ClothingMaterials.lua
EnchantingMaterials.lua
WoodworkingMaterials.lua
InventoryHooks.lua
TooltipHooks.lua
