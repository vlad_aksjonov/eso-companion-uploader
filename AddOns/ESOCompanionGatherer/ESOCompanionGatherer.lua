LB_ESOCG = {}
LB_ESOCG.Name = "ESOCompanionGatherer"
LB_ESOCG.Version = "0.03"

function LB_ESOCG.Register(userID)
	LB_ESOCG.B.UserID = userID
  d("Now your game and mobile accounts are connected. Thanks and enjoy your new tool")
end

function LB_ESOCG.OnPlayerActivated(event)
  LB_ESOCG.C = ZO_SavedVars:New("LB_ESOCG_Data", LB_ESOCG.Version, nil, LB_ESOCG.Char)
  LB_ESOCG.B = ZO_SavedVars:NewAccountWide("LB_ESOCG_Data", LB_ESOCG.Version, nil, LB_ESOCG.Bank)
  
  d("ESO Companion Gatherer is here ;)")
  if LB_ESOCG.B.WrongID=="UserUnreg" then
    d("ESO Companion Uploader had problems with your entered uesrID. Check your userID in your ESO Companion mobile application and register again using chat slash command /esocgreg <userID>. This will connect your game account to your mobile application.")
    LB_ESOCG.B.WrongID = nil
  else 
    if LB_ESOCG.B.UserID==0 then
      d("ESO Companion Gatherer not registered, so your data can't be saved to server. Please, launch ESO Companion application on your mobile, get the code and enter here in chat /esocgreg <userID>. This will connect your game account to your mobile application.")
    end
  end
end

function LB_ESOCG.DoSomeDataGathering()
  LB_ESOCG.B.Lang = GetCVar('language.2')
  if LB_ESOCG.DebugMode.Character then
    if LB_ESOCG.DebugMode.Character_Currency then
      LB_ESOCG.C.Currency = LB_ESOCG.GetCurrency(BAG_BACKPACK)
    end
    
    if LB_ESOCG.DebugMode.Character_Inventory then
      LB_ESOCG.C.Inventory = LB_ESOCG.GetInventory(BAG_BACKPACK)
    end
    
    if LB_ESOCG.DebugMode.Character_Worn then
      LB_ESOCG.C.Worn = LB_ESOCG.GetInventory(BAG_WORN)
    end
  
    if LB_ESOCG.DebugMode.Character_Styles then
      LB_ESOCG.C.Styles = LB_ESOCG.GetKnownStyles()
    end
    
    if LB_ESOCG.DebugMode.Character_Personality then
      
      LB_ESOCG.C.Name = GetUnitName("player")      
      LB_ESOCG.C.Gender = GetUnitGender("player")
      LB_ESOCG.C.Race      = GetUnitRace("player")
      LB_ESOCG.C.RaceID    = GetUnitRaceId("player")
      LB_ESOCG.C.Class = GetUnitClass("player")
      
      LB_ESOCG.C.Level = GetUnitLevel("player")
      LB_ESOCG.C.XP        = GetUnitXP("player")
      LB_ESOCG.C.XPMax     = GetUnitXPMax("player")
      
      LB_ESOCG.C.ChampionPoints = GetUnitChampionPoints("player")
      LB_ESOCG.C.CXP       = GetPlayerChampionXP()
      LB_ESOCG.C.CXPMax    = GetNumChampionXPInChampionPoint(LB_ESOCG.C.ChampionPoints+1)
      
      LB_ESOCG.C.Zone      = GetUnitZone("player")
      LB_ESOCG.C.ZoneIndex = GetUnitZoneIndex("player")
    
      LB_ESOCG.C.Alliance  = GetUnitAlliance("player")
    end
    
    if LB_ESOCG.DebugMode.Character_AvA then
      LB_ESOCG.C.AvARank = GetUnitAvARank("player")
      LB_ESOCG.C.AvARankPoints = GetUnitAvARankPoints("player")
      LB_ESOCG.C.AvARankPointsNeed = GetNumPointsNeededForAvARank(LB_ESOCG.C.AvARank+1)
    end
    
    if LB_ESOCG.DebugMode.Character_Powers then
      LB_ESOCG.C.Powers = {}
      LB_ESOCG.C.Powers["Stamina" ] = GetUnitPower("player", POWERTYPE_STAMINA )
      LB_ESOCG.C.Powers["Magica"  ] = GetUnitPower("player", POWERTYPE_MAGICKA )
      LB_ESOCG.C.Powers["Health"  ] = GetUnitPower("player", POWERTYPE_HEALTH  )
      LB_ESOCG.C.Powers["Ultimate"] = GetUnitPower("player", POWERTYPE_ULTIMATE)
    end
    
    if LB_ESOCG.DebugMode.Character_Riding then
      LB_ESOCG.C.Riding = {}
      LB_ESOCG.C.Riding["Inventory"], _, LB_ESOCG.C.Riding["Stamina"], _, LB_ESOCG.C.Riding["Speed"], _ = GetRidingStats()
      LB_ESOCG.C.Riding.timestamp = GetTimeStamp()
      LB_ESOCG.C.Riding.TimeLeft = GetTimeUntilCanBeTrained()
    end
    
    if LB_ESOCG.DebugMode.Character_Skills then
      LB_ESOCG.C.Skills = LB_ESOCG.GetSkills()
    end
    
    if LB_ESOCG.DebugMode.Character_Buffs then
      LB_ESOCG.C.Buffs = LB_ESOCG.GetBuffs()
    end
    
    if LB_ESOCG.DebugMode.Character_Traits then
      LB_ESOCG.C.Traits = LB_ESOCG.GetKnownTraits()
    end
  end
  
  if LB_ESOCG.DebugMode.Bank then
    if LB_ESOCG.DebugMode.Bank_Currency then
      LB_ESOCG.B.Currency = LB_ESOCG.GetCurrency(BAG_BANK)  
    end
    if LB_ESOCG.DebugMode.Bank_Inventory then
      LB_ESOCG.B.Inventory = LB_ESOCG.GetInventory(BAG_BANK)
      LB_ESOCG.B.CraftBag  = LB_ESOCG.GetInventory(BAG_VIRTUAL)
    end
  end
end

function LB_ESOCG.GetKnownStyles()
  local styleList = {}
	for id = 1,GetNumSmithingStyleItems() do
		local _, _, _, _, style = GetSmithingStyleItemInfo(id)
		if style ~= ITEMSTYLE_UNIVERSAL and style ~= ITEMSTYLE_NONE then
      if LB_ESOCG.IsSimpleStyle(id) then
        styleList[LB_ESOCG.Styles[id][4]] = LB_ESOCG.IsStyleKnown(id,1)
      else
        if LB_ESOCG.Styles[id] then
          local chap = {}
          for chapter = 1,14 do          
            chap[LB_ESOCG.Chapters[chapter]] = LB_ESOCG.IsStyleKnown(id,chapter)
          end
          styleList[LB_ESOCG.Styles[id][4]] = chap
        end
      end
		end
	end
  return styleList
end

function LB_ESOCG.GetKnownTraits()
  local trait_list = {}
  for skill = 1,6 do
    local c = {}
    for line = 1, GetNumSmithingResearchLines(skill) do
      local d = {}
      local lineName, traits
      lineName, d.texture, traits, d.nextTime = GetSmithingResearchLineInfo(skill, line)
      for trait = 1, traits do
        local tid, desc, known = GetSmithingResearchLineTraitInfo(skill,line,trait)
        local duration, remaning = GetSmithingResearchLineTraitTimes(skill,line,trait)
        local timestamp = GetTimeStamp()
        local _,traitName,icon = GetSmithingTraitItemInfo(tid + 1)
        local e = 
        {
          Description = desc,
          Known = known,
          Icon = icon,
          Timestamp = timestamp,
          Duration = duration,
          Remaning = remaning,
	  Material = traitName
        }
        d[trait] = e
      end
      c[lineName] = d
    end
    trait_list[LB_ESOCG.CraftName[skill]] = c
  end
  return trait_list
end

function LB_ESOCG.GetSkills()
  local c = {}
  for skillType = 1, GetNumSkillTypes() do
    local numSkillLines = GetNumSkillLines(skillType)
    if numSkillLines > 0 then
      local skillTypeName = LB_ESOCG.SkillTypeNames[skillType]
      c[skillTypeName] = {}
      for skillLineIndex = 1, numSkillLines do
        local d = {}
        d.Name, d.Rank = GetSkillLineInfo(skillType, skillLineIndex)
        d.Abilities = {}
        local numAbilities = GetNumSkillAbilities(skillType, skillLineIndex)
        for abilityIndex = 1, numAbilities do
          local e = {}
          e.Name, e.Texture, e.Rank, e.Passive, e.Ultimate, e.Purchased, e.progressionIndex = GetSkillAbilityInfo(skillType, skillLineIndex, abilityIndex)
          e.CurrentLevel, e.MaxLevel = GetSkillAbilityUpgradeInfo(skillType, skillLineIndex, abilityIndex)
          e.nextName, e. nextTexture, e.nextRank = GetSkillAbilityNextUpgradeInfo(skillType, skillLineIndex, abilityIndex)
          d.Abilities[abilityIndex] = e
        end
        c[skillTypeName][skillLineIndex] = d
      end
    end
  end
  return c
end

function LB_ESOCG.GetBuffs()
  local c = {}
  for buffIndex = 1, GetNumBuffs("player") do
    local d = {}
    d.Name, d.Started, d.Ending, d.Slot, d.StackCount, d.Icon, d.Type, d.EffectType, d.AbilityType, d.StatusEffectType, d.AbilityID, d.CanClickOff = GetUnitBuffInfo("player", buffIndex)
    c[buffIndex] = d
  end
  return c
end

function LB_ESOCG.IsSimpleStyle(style)
		if not LB_ESOCG.Styles[style] then return false end
		if LB_ESOCG.Styles[style][1] == 1 then return true end
		return false
	end

function LB_ESOCG.IsStyleKnown(style,chapter)
		if not LB_ESOCG.Styles[style] then return false end
		if LB_ESOCG.IsSimpleStyle(style) then
			return IsSmithingStyleKnown(style)
		else
			local _, known = GetAchievementCriterion(LB_ESOCG.Styles[style][2],chapter)
			if known == 1 then return true end
		end
		return false
	end
 
function LB_ESOCG.GetCurrency(bagId)
  local c = {}
  if bagId == BAG_BACKPACK then
    c.Gold = GetCarriedCurrencyAmount(1) --CURT_MONEY
    c.AlliancePoints = GetCarriedCurrencyAmount(2) --CURT_ALLIANCE_POINTS
    c.TelVarStones = GetCarriedCurrencyAmount(3) --CURT_TELVAR_STONES
  else
    c.Gold = GetBankedCurrencyAmount(1) --CURT_MONEY
    c.AlliancePoints = GetBankedCurrencyAmount(2) --CURT_ALLIANCE_POINTS
    c.TelVarStones = GetBankedCurrencyAmount(3) --CURT_TELVAR_STONES
  end
  return c
end

 
function LB_ESOCG.GetInventory(bagId)
  local items = {}
  local j = 0
  if bagId ~= BAG_VIRTUAL then
    local numBagSlots = GetBagSize(bagId)
    
    for i = 0, numBagSlots, 1 do
      local item = LB_ESOCG.GetItem(bagId,i)
      if item.name ~="" then
        items[j] = item
        j = j+1
      end
    end
	else
    local lastIndex = nil
    while true do
      lastIndex = GetNextVirtualBagSlotId(lastIndex)
      if lastIndex ~= nil then
        local item = LB_ESOCG.GetItem(bagId,lastIndex)
        if item.name ~="" then
          items[j] = item
          j = j+1
        end
      else
        break
      end
    end
  end
	return items
end

function LB_ESOCG.GetItem(bagId, slotId)
   local i =
   {
    bagId    = bagId,
		slotId   = slotId,
		itemType = GetItemType(bagId, slotId),
		name     = GetItemName(bagId, slotId),
    link     = GetItemLink(bagId, slotId, LINK_STYLE_BRACKETS),
  }
  i.stack, i.icon, i.price, _, i.locked, i.equipType, i.style, i.quality = GetItemInfo(bagId, slotId)
  return i
end

LB_ESOCG.Bank = 
{
  UserID    = 0,
  Currency  = {},
  Inventory = {},
  CraftBag  = {},
}
  
LB_ESOCG.Char = 
{
  Currency  = {},
  Powers    = {},
  Riding    = {},
  Inventory = {},
  Worn      = {}
}

LB_ESOCG.SkillTypeNames =
{
  [SKILL_TYPE_CLASS     ] = "Class",
  [SKILL_TYPE_WEAPON    ] = "Weapon",
  [SKILL_TYPE_ARMOR     ] = "Armor",
  [SKILL_TYPE_WORLD     ] = "World",
  [SKILL_TYPE_GUILD     ] = "Guild",
  [SKILL_TYPE_AVA       ] = "AvA",
  [SKILL_TYPE_RACIAL    ] = "Racial",
  [SKILL_TYPE_TRADESKILL] = "Craft",
}

LB_ESOCG.CraftName =
{
  [CRAFTING_TYPE_BLACKSMITHING] = "Blacksmithing",
  [CRAFTING_TYPE_CLOTHIER     ] = "Clothier",
  [CRAFTING_TYPE_WOODWORKING  ] = "Woodworking",
  [CRAFTING_TYPE_ALCHEMY      ] = "Alchemy",
  [CRAFTING_TYPE_ENCHANTING   ] = "Enchanting",
  [CRAFTING_TYPE_PROVISIONING ] = "Provisioning",
  [CRAFTING_TYPE_INVALID      ] = "Invalid",
}

LB_ESOCG.DebugMode=
{
  Bank = true,
  Bank_Currency = true,
  Bank_Inventory = true,
  
  Character = true,
  Character_Currency = true,
  Character_Inventory = true,
  Character_Worn = true,
  Character_Styles = true,
  Character_AvA = true,
  Character_Personality = true,
  Character_Powers = true,
  Character_Skills = true,
  Character_Riding = true,
  Character_Buffs = true,
  Character_Traits = true,
}

LB_ESOCG.Styles = 
{
	[ITEMSTYLE_RACIAL_ARGONIAN      +1] = {1,1025,27246,"Argonians"},
  [ITEMSTYLE_RACIAL_BRETON        +1] = {1,1025,16425,"Bretons"},
	[ITEMSTYLE_RACIAL_DARK_ELF      +1] = {1,1025,27245,"Dark Elves"},
	[ITEMSTYLE_RACIAL_HIGH_ELF      +1] = {1,1025,16424,"High Elves"},
	[ITEMSTYLE_RACIAL_IMPERIAL      +1] = {1,1025,54868,"Imperial Cyrods"},
	[ITEMSTYLE_RACIAL_KHAJIIT       +1] = {1,1025,44698,"Khajiits"},
	[ITEMSTYLE_RACIAL_NORD          +1] = {1,1025,27244,"Nords"},
	[ITEMSTYLE_RACIAL_ORC           +1] = {1,1025,16426,"Orcs"},
	[ITEMSTYLE_RACIAL_REDGUARD      +1] = {1,1025,16427,"Redguards"},
	[ITEMSTYLE_RACIAL_WOOD_ELF      +1] = {1,1025,16428,"Wood Elves"},
  
	[ITEMSTYLE_DEITY_MALACATH       +1] = {2,1412,71567,"Malacath"},
  [ITEMSTYLE_DEITY_TRINIMAC       +1] = {2,1411,71551,"Trinimac"},
	
	[ITEMSTYLE_AREA_AKAVIRI         +1] = {2,1318,57591,"Akaviri"}, --28
	[ITEMSTYLE_AREA_ANCIENT_ELF     +1] = {1,1025,51638,"Ancient Elves"},
  [ITEMSTYLE_AREA_ANCIENT_ORC     +1] = {2,1341,69528,"Ancient Orc"}, --32
	[ITEMSTYLE_AREA_DWEMER          +1] = {2,1144,57573,"Dwemer"}, --27
	[ITEMSTYLE_AREA_REACH           +1] = {1,1025,51565,"Barbaric"},
	[ITEMSTYLE_AREA_SOUL_SHRIVEN    +1] = {1,1418,71765,"Soul-Shriven Style"},
	[ITEMSTYLE_AREA_XIVKYN          +1] = {2,1181,57835,"Xivkyn"}, --31
	
	[ITEMSTYLE_ENEMY_DAEDRIC        +1] = {1,1025,51688,"Daedric"},
	[ITEMSTYLE_ENEMY_PRIMITIVE      +1] = {1,1025,51345,"Primal"},
	
	[ITEMSTYLE_ALLIANCE_ALDMERI     +1] = {2,1415,71689,"Aldmeri Dominion"},
  [ITEMSTYLE_ALLIANCE_DAGGERFALL  +1] = {2,1416,71705,"Daggerfall Covenant"},
	[ITEMSTYLE_ALLIANCE_EBONHEART   +1] = {2,1414,71721,"Ebonheart Pact"},
	
  [ITEMSTYLE_ORG_ABAHS_WATCH      +1] = {2,1422,74540,"Abah's Watch"},
  [ITEMSTYLE_ORG_ASSASSINS        +1] = {2,1424,76879,"Assasins League"},
  [ITEMSTYLE_ORG_OUTLAW           +1] = {2,1417,71523,"Outlaw"}, --36
  [ITEMSTYLE_ORG_THIEVES_GUILD    +1] = {2,1423,74556,"Thieves Guild"},
  
	[ITEMSTYLE_GLASS                +1] = {2,1319,64670,"Glass"}, --29
  [ITEMSTYLE_UNDAUNTED            +1] = {2,1348,64716,"Mercenary"}, --30
}

LB_ESOCG.Chapters = 
{ 
  [1 ] = "Axes",
	[2 ] = "Belts",
	[3 ] = "Boots",
	[4 ] = "Bows",
	[5 ] = "Chests",
	[6 ] = "Daggers",
	[7 ] = "Gloves",
	[8 ] = "Helmets",
	[9 ] = "Legs",
	[10] = "Maces",
	[11] = "Shields",
	[12] = "Shoulders",
	[13] = "Staves",
	[14] = "Swords"
}
 
EVENT_MANAGER:RegisterForEvent(LB_ESOCG.Name, EVENT_PLAYER_ACTIVATED, LB_ESOCG.OnPlayerActivated)

ZO_PreHook("ReloadUI", LB_ESOCG.DoSomeDataGathering)
ZO_PreHook("Logout", LB_ESOCG.DoSomeDataGathering)
ZO_PreHook("Quit", LB_ESOCG.DoSomeDataGathering)

SLASH_COMMANDS["/esocgreg"] = LB_ESOCG.Register
SLASH_COMMANDS["/rl"] = SLASH_COMMANDS["/reloadui"]