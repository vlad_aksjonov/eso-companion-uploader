-------------------------------------------
-- ru - russian:russian
-- Author: KiriX (from the Esoui.com community)
-------------------------------------------
cl.ln = {
    com = {
        show = "show",
        midnight = "midnight",
        sunrise = "sunrise",
        noon = "noon",
        sunset = "sunset",
        reset = "reset",
        resetTxt = "Ùacÿ càpoúeîÿ.",
        resetui = "resetUI",
        resetUITxt = "Èîòepñeéc ùacoá càpoúeî.",
        resettime = "resetTime",
        resetTimeTxt = "Ápeíü ùacoá càpoúeîo.",
        us = "us",
        usOn = "Ápeíü ïpeäcòaáìeîo á äáeîaäœaòèùacoáoí ñopíaòe (12) c am/pm.",
        usOff = "Ápeíü ïpeäcòaáìeîo á äáaäœaòèùaòÿpexùacoáoí ñopíaòe (24).",
        num = "num",
        numOn = "Îóìè äoàaáìeîÿ.",
        numOff = "Ùacÿ àeç îóìeé.",
        sec = "sec",
        secOn = "Cêóîäÿ ïoêaçaîÿ.",
        secOff = "Ceêóîäÿ cêpÿòÿ.",
        active = "active",
        activeOn = "Ùacÿ - TST áêì.",
        activeOff = "Ùacÿ - TST áÿêì.",
        active = "move",
        activeOn = "Ùacÿ íoæîo ïepeíeûaòö.",
        activeOff = "Ùacÿ çañèêcèpoáaîÿ.",
        moon = "moon",
        moonOn = "Ìóîa ïoêaçaîa.",
        moonOff = "Ìóîa cêpÿòa.",
        ldate = "gdate",
        ldateOn = "Ìopîoe ápeíü ïoêaçaîo.",
        ldateOff = "Ìopîoe ápeíü cêpÿòo.",
        fldate = "fldate",
        fldateOn = "Ècêóccòáeîîoe ìopîoe ápeíü ïoêaçaîo.",
        fldateOff = "Ècêóccòáeîîoe ìopîoe ápeíü cêpÿòo.",
        rt = "rt",
        rtOn = "Peaìöîoe ápeíü ïoêaçaîo.",
        rtOff = "Peaìöîoe ápeíü cêpÿòo.",
        date = "date",
        dateOn = "Äaòa ïoêaçaîa.",
        dateOff = "Äaòa cêpÿòa.",
        text = "Äoàpo ïoæaìoáaòö á íeîô ïoíoûè aääoîa |cFFD700Clock|r - TST by |c5175ea@Tyx|r [RU]\n"
                .. "Ùòoàÿ ïoêaçaòö èâpoáoe ápeíü, îaàepèòe:\n"
                .. "\t\cl show\n"
                .. "Ùòoàÿ çaäaòö òeêóûee ápeíü êaê Òaípèëìöcêèe ïoìîoùö/áocxoä/ïoìäeîö/çaêaò ècïoìöçóéòe cìeäóôûèe êoíaîäÿ:\n"
                .. "\t\cl midnight\n\t\cl sunrise\n\t\cl noon\n\t\cl sunset\n"
                .. "Ùòoàÿ càpocèòö àaçó ê çîaùeîèüí ïo óíoìùaîèô, îaàepèòe:\n"
                .. "\t\cl reset\n\t\cl resetUI\t\cl resetTime\n"
                .. "Áÿ íoæeòe ïepeêìôùèòö ñopíaò ápeíeîè (12ù/24ù) îaàpaá:\n"
                .. "\t\cl us\n"
                .. "Ùòoàÿ óàpaòö îoìö ïepeä ùècìoí ùaca, îaàepèòe:\n"
                .. "\t\cl num\n"
                .. "Áÿ íoæeòe áêìôùèòö/oòêìôùèòö oòoàpaæeîèe ceêóîä êoíaîäoé:\n"
                .. "\t\cl sec\n"
                .. "Ùòoàÿ ïoêaçaòö òeêóûóô ñaçó ìóîÿ, îaàepèòe:\n"
                .. "\t\cl moon\n"
                .. "Ùòoàÿ ïoêaçaòö äaòó (ìopîóô/ñëéêoáóô ìopîóô/peaìöîóô), îaàepèòe:\n"
                .. "\t\cl ldate\n\t\cl fldate\t\cl date\n"
                .. "Ùòoàÿ paçpeúèòö/çaïpeòèòö ïepeíeûeîèe aääoîa, îaàepèòe:\n"
                .. "\t\cl move\n"
                .. "Ùòoàÿ áêìôùèòö/áÿêìôùèòö ùacÿ, îaàepèòe.\n"
                .. "\t\cl active\n\n"
                .. "Ùòoàÿ ïoêaçaòö íeîô îacòpoeê, îaàepèòe:\n"
                .. "\t\clsettings\n",
    },
    gui = {
        com = "/clsettings",
        -- Language
        lang = "Üçÿê",
        langTxt = "Áÿàepèòe üçÿê äìü îacòpoéêè aääoîa è eâo êoíaîä.\n Áîèíaîèe: ëòo ïepeçaâpóçèò UI.",
        -- Toggle
        togOpt = "Áêìôùaeíÿe îacòpoéêè",
        togOptTxt = "Èçíeîüô áîeúîèé áèä ùacoá.",
        sClock = "Ïoêaçaòö ùacÿ",
        sTime = "Ïoêaçaòö ápeíü",
        sMove = "Ïepeíeûeîèe",
        tMove = "Áÿàepèòe, ecìè xoòèòe ïepeíeûaòö ùacÿ.",
        sAHide = "Aáòocêpÿòèe",
        tAHide = "Áÿàepèòe, ecìè ùacÿ îóæîo cêpÿáaòö ïpè oòêpÿòèè íeîô.",
        sFormat = "12ù ñopíaò",
        sAMPM = "Ïoêaçÿáaòö AM/PM",
        sNum = "Äoàaáèòö îóìè",
        tNum = "Äoàaáìüeò îóìè ïepeä ùacaíè: 1:24 -> 01:24",
        sSec = "Ïoêaçÿáaòö ceêóîäÿ",
        sMoon = "Ïoêaçÿáaòö ìóîó",
        tMoon = "Ïoêaçÿáaeò îeàoìöúóô èêoîêó òeêóûeé ñaçÿ ìóîÿ è ápeíü äo îaùaìa cìeäóôûeé ñaçÿ.",
        sLDate = "Ïoêaçÿáaòö ìopîóô äaòó",
        tLDate = "Äoàaáìüeò còpoêó ïoä ùacaíè, oòoàpaæaôûóô äaòó, íecüœ è âoä Òaípèëìü.",
        sFLDate = "Ïoêaçÿáaòö ñëéêoáóô ìopîóô äaòó",
        tFLDate = "Äoàaáìüeò còpoêó ïoä ùacaíè, oòoàpaæaôûóô äaòó, íecüœ è âoä Òaípèëìü (cooòáeòcòáóôûóô peaìöîoé äaòe).",
        sRT = "Ïoêaçÿáaòö peaìöîoe ápeíü",
        tRT = "Äoàaáìüeò còpoêó ïoä ùacaíè, oòoàpaæaôûóô ápeíü oïepaœèoîîoé cècòeíÿ.",
        sDate = "Ïoêaçÿáaòö peaìöîóô äaòó",
        tDate = "Äoàaáìüeò còpoêó ïoä ùacaíè, oòoàpaæaôûóô äaòó, íecüœ è âoä oïepaœèoîîoé cècòeíÿ.",
        nsBg = "Ïoêaçÿáaòö ñoî",
        tsBg = "Äoàaáìüeò ùacaí ñoî.",
        -- Look
        look = "Îacòpoéêè áîeúîeâo áèäa",
        lookTxt = "Paçìèùîÿe îacòpoéêè, èçíeîüôûèe áîeúîèé áèä ùacoá.",
        nColPick = "Œáeò",
        tColPick = "Èçíeîüeò œáeò ùacoá.",
        nFont = "Úpèñò",
        tFont = "Èçíeîüeò úpèñò ùacoá.",
        nStyle = "Còèìö",
        tStyle = "Èçíeîüeò còèìö ùacoá.",
        nSize = "Paçíep",
        tSize = "Èçíeîüeò paçíep ùacoá.",
        --real
        nSepLR = "Paçìèùîÿé áèä",
        tSepLR = "Áÿàepèòe, ecìè xoòèòe, ùòoàÿ peaìöîaü äaòa áÿâìüäeìa èîaùe, ùeí ìopîaü.",
        nRColPick = "Œáeò",
        tRColPick = "Èçíeîüeò œáeò ùacoá.",
        nRFont = "Úpèñò",
        tRFont = "Èçíeîüeò úpèñò ùacoá.",
        nRStyle = "Còèìö",
        tRStyle = "Èçíeîüeò còèìö ùacoá.",
        nRSize = "Paçíep",
        tRSize = "Èçíeîüeò paçíep ùacoá.",
        nBg = "Ñoî",
        tBg = "Áÿàepèòe ñoî ùacoá.",
        sHor = "Âopèçoîòaìöîo",
        tHor = "Ïoêaçÿáaòö ìopîoe è peaìöîoe ápeíü äpóâ ça äpóâoí.",
        descEditLookY = "Èçíeîüeò ñopíaò ùacoá: Ècïoìöçóéòe '_' ïepeä ïepeíeîîoé!\n"
                .. "Âoä/Íecüœ/Äeîö",
        descEditLookYTxt = "Âoä: Y = 582 (14)\tYY = 2Ë 582 (2014)\n"
                .. "Íecüœ: M = 4\tMM = 04\tMMM = Íecüœ Ïepáoâo çepîa (Aïpeìö)\n"
                .. "Äeîö: D = 4\tDD = 04\tDDD = Ñpeäac (Ïüòîèœa)",
        descEditLookD = "Ùacÿ/Íèîóòÿ/Ceêóîäÿ",
        descEditLookDTxt = "Ùacÿ: h = 9\thh = 09\n"
                .. "Íèîóòÿ: m = 9\tmm = 09\n"
                .. "Ceêóîäÿ: s = 9\tss = 09",
        descEditLookE = "Ïpèíep",
        descEditLookETxt = "_DDD, _D ùècìo, _MMM _YY _hh:_mm:_ss\n"
                .. "Ñpeäac, 4 ùècìo, Íecüœ Ïepáoâo çepîa 2Ë 582\n"
                .. "Ïüòîèœa, 4 ùècìo, Aïpeìö 2014",
        nELore = "Ñopíaò ìopîoâo ápeíeîè",
        tELore = "Çaäaéòe ñopíaò ìopîoâo ápeíeîè çäecö.",
        nEReal = "Ñopíaò peaìöîoâo ápeíeîè",
        tEReal = "Çaäaéòe ñopíaò peaìöîoâo ápeíeîè çäecö.",
        -- Data
        day = "Cèîx [Äeîö]",
        dayTxt = "Çaäaéòe äìèòeìöîocòö äîü îa cáoé áêóc.",
        descDB = "Cèîx [Ïoìîoùö/Paccáeò/Ïoìäeîö/Çaêaò]",
        descDBTxt = "Cèîxpoîèçèpóeò ápeíü ùacoá.\nÎaïp. áÿàepèòe ïoìäeîö, êoâäa coìœe ïpüío îaä ôæîoé çaceùêoé êoíïaca.",
        nMid = "Ceéùac ïoìîoùö!",
        tMid = "Áeàepèòe, ecìè ìóîa á çeîèòe. ",
        nRise = "Ceéùac áocxoä!",
        tRise = "Oòêìoîeîo! Áocxoä îeüceî. Paàoòaeí ïo ïoìóäîô!",
        nNoon = "Ceéùac ïoìäeîö!",
        tNoon = "Áeàepèòe, ecìè coìîœe ïpüío îaä ôæîoé çaceùêoé êoíïaca.",
        nSet = "Ceéùac çaêaò!",
        tSet = "Oòêìoîeîo! Çaêaò îeüceî. Paàoòaeí ïo ïoìóäîô!",
        descDS = "Cèîx [Äìèòeìöîocòö äîü]",
        descDSTxt = "Cèîxpoîèçèpóeò äìèòeìöîocòö äîü.",
        nDayH = "Ùacÿ",
        tDayH = "Cêoìöêo äìèòcü oäèî èâpoáoé äeîö á peaìöîÿx ùacax.",
        nDayM = "Íèîóòÿ",
        tDayM = "Cêoìöêo äìèòcü oäèî èâpoáoé äeîö á peaìöîÿx íèîóòax.",
        nDayS = "Ceêóîäÿ",
        tDayS = "Cêoìöêo äìèòcü oäèî èâpoáoé äeîö á peaìöîÿx ceêóîäax.",
        nAplyData = "Cocùèòaòö",
        tAplyData = "Cocùèòaòö òeêóûee Òaípèëìöcêoe ápeíü îa ocîoáe ááeäeîîÿx áaíè äaîîÿx (èìè çîaùeîèüx ïo óíoìùaîèô).",
        -- Moon
        moon = "Cèîx [Ìóîa]",
        moonTxt = "Çaäaéòe ìóîîóô ñaçó è äìèòeìöîocòö ìóîîÿx ñaç îa áaú áêóc.",
        nFull = "Ceéùac ïoìîoìóîèe!",
        tFull = "Áÿàepèòe ëòo çîaùeîèe, ecìè ceéùac ïepáaü ñaça ïoìîoé ìóîÿ.",
        nNew = "Ceéùac îoáoìóîèe!",
        tNew = "Áÿàepèòe ëòo çîaùeîèe, ecìè ceéùac ïepáaü ñaça ïoìîoé ìóîÿ.",
        descMS = "Cèîx [Ìóîa]",
        descMSTxt = "Cèîxpoîèçèpóeò ìóîîóô ñaçó c ùacaíè.\nÄeìaéòe ëòo á caíoí îaùaìe ñaçÿ.",
        descMoon = "Cèîx [Äìèòeìöîocòö ñaçÿ]",
        descMoonTxt = "Cèîxpoîèçaœèô ùacoá îeoàxoäèío äeìaòö òoìöêo á caíoí îaùaìe ïoìîoìóîèü èìè îoáoìóîèü è ìèúö oäîaæäÿ.\n"
                .. "Ïoìîaü ìóîîaü ñaça - 30 èâpoáÿx äîeé. Cìeäóôûèé cìaéäep ïoçáoìüeò çaäaòö ïpoœeîò oò ïoìîoé ñaçÿ.",
        nNightF = "Ïpoœeîò ñaçÿ ïoìîoé ìóîÿ",
        tNightF = "Áÿàepèòe, cêoìöêo äìèòcü ñaça ïoìîoé ìóîÿ.",
        nNightW = "Ñaçÿ óàÿáaôûeé/pacòóûeé ìóîÿ á äîüx",
        tNightW = "Áÿàepèòe, cêoìöêo äìèòcü ñaça óàÿáaôûeé/pacòóûeé ìóîÿ.",
        nNightN = "Ñaça îoáoìóîèü á äîüx",
        tNightN = "Áÿàepèòe, cêoìöêo äìèòcü ñaça îoáoìóîèü.",
        nAplyMoon = "Cocùèòaòö",
        tAplyMoon = "Cocùèòaòö òeêóûóô ñaçó ìóîÿ îa ocîoáe ááeäeîîÿx äaîîÿx èìè äaîîÿx ïo óíoìùaîèô.",
        -- Reset
        reset = "Îacòpoéêè càpoca",
        resetTxt = "Paçìèùîÿe oïœèè càpoca ácex èìè êaêèx-òo êoîêpeòîÿx îacòpoeê ùacoá.",
        nResFull = "Càpocèòö áce",
        tResFull = "Càpocèòö áce ê çîaùeîèüí ïo óíoìùaîèô.",
        wResFull = "Ëòo càpocèòö áce áaúè îacòpoéêè ê çîaùeîèüí ïo óíoìùaîèô!\nÏepeçaâpóçèò UI!",
        nResUI = "Càpocèòö UI",
        tResUI = "Càpocèòö îacòpoéêè áîeúîeâo áèäa è ïoìoæeîèü ê çîaùeîèüí ïo óíoìùaîèô.",
        wResUI = "Ëòo càpocèòö áce áaúè îacòpoéêè áîeúîeâo áèäa UI ê çîaùeîèüí ïo óíoìùaîèô!",
        nResTime = "Càpocèòö ápeíü",
        tResTime = "Càpocèòö áaúè îacòpoéêè cèîxpoîèçaœèè èäìèòeìöîocòè ùacoá ê çîaùeîèüí ïo óíoìùaîèô.",
        wResTime = "Ëòo càpocèò áce áaúè îacòpoéêè ápeíeîè ê çîaùeîèüí ïo óíoìùaîèô!\nÏepeçaâpóçèò UI!",
    },
    vi = {
        dbTS = {
            [1] = "Cìeäóôûaü ïoìîoùö (0:00 TST) á ",
            [2] = "Cìeäóôûèé áocxoä (4:00 TST) á ",
            [3] = "Cìeäóôûèé ïoìäeîö (12:00 TST) á ",
            [4] = "Cìeäóôûèé çaêaò (20:00 TST) á ",
            [5] = "Äeîö äìèòcü: ",
            [6] = "\n",
        },
        date = {
            first = "",
            second = "",
            third = "",
            allNum = "",
            lore = {
                week = {
                    [1] = "Íopîäac",
                    [2] = "Òèpäac",
                    [3] = "Íèääac",
                    [4] = "Òópäac",
                    [5] = "²peäac",
                    [6] = "Ìopäac",
                    [7] = "Caîäac",
                },
                months = {
                    [1] = "Íecüœ Óòpeîîeé çáeçäÿ",
                    [2] = "Íecüœ Áocxoäa coìîœa",
                    [3] = "Íecüœ Ïepáoâo çepîa",
                    [4] = "Íecüœ Póêè äoæäü",
                    [5] = "Íecüœ Áòopoâo çepîa",
                    [6] = "Íecüœ Cepeäèîÿ âoäa",
                    [7] = "Íecüœ Áÿcoêoâo coìîœa",
                    [8] = "Íecüœ Ïocìeäîeâo çepîa",
                    [9] = "Íecüœ Oâîü oùaâa",
                    [10] = "Íecüœ Îaùaìa íopoçoá",
                    [11] = "Íecüœ Çaêaòa coìîœa",
                    [12] = "Íecüœ Áeùepîeé çáeçäÿ",
                },
                year = "2Ë ",
            },
            real = {
                week = {
                    [1] = "Ïoîeäeìöîèê",
                    [2] = "Áòopîèê",
                    [3] = "Cpeäa",
                    [4] = "Ùeòáepâ",
                    [5] = "Ïüòîèœa",
                    [6] = "Cóààoòa",
                    [7] = "Áocêpeceîöe",
                },
                months = {
                    [1] = "Üîáapö",
                    [2] = "Ñeápaìö",
                    [3] = "Íapò",
                    [4] = "Aïpeìö",
                    [5] = "Íaé",
                    [6] = "Èôîö",
                    [7] = "Èôìö",
                    [8] = "Aáâócò",
                    [9] = "Ceîòüàpö",
                    [10] = "Oêòüàpö",
                    [11] = "Îoüàpö",
                    [12] = "Äeêaàpö",
                },
            },
        },
    },
}
