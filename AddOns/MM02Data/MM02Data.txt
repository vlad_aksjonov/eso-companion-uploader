## APIVersion: 100015
## Title: Master Merchant Data File MM02Data
## Description: MM02Data Stores data for Master Merchant
## Version 1.9.0
## License: See license - distribution without license is prohibited!
## LastUpdated: May 2016
## SavedVariables: MM02DataSavedVariables
## DependsOn: MM00Data

MM02Data.lua