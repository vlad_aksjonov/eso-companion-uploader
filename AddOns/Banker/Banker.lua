SI = Banker.SI

Banker.newItem = function(iBagId, iSlotId, iType, iName, iStack, iLink, iIcon)
	local item ={
		bagId    = iBagId,
		slotId   = iSlotId,
		itemType = iType,
		name     = iName,
		stack    = iStack,
		link     = iLink,
		icon     = iIcon,
	}

	return item
end

Banker.initVars = function()
	PACKAGE = "com.github.Nols1000.banker"
	VERSION = 11

	defaults = {}
	defaults.lang = 1
	defaults.iTypes = {}

	for i = 0, #Banker.ItemTypes - 1, 1 do
		defaults.iTypes[Banker.ItemTypes[i]] = true
	end

	defaults.items   = true
	defaults.move    = true
	defaults.money   = true
	defaults.debug   = false
	defaults.msg     = true
	defaults.keymenu = true

	defaults.minMoveStack = 50
	
	defaults.mStep  = 50
	defaults.mMin   = 500

	defaults.updated = false

	Banker.isBankOpen = false
end

Banker.initUI = function()
	local SI = Banker.SI
	local s = Banker.Settings
	
	Banker.KeybindStripDescriptor =
	{
		{ -- I think you can have more than one button in your group if you add more of these sub-groups
			name = SI.get(SI.KB_SYNC_ITEMS),
			keybind = "SYNC_INVENTORY",
			callback = function() Banker.stackItems() end,
			visible = function() return Banker.isBankOpen end,
		},
		{ -- I think you can have more than one button in your group if you add more of these sub-groups
			name = SI.get(SI.KB_SAFE_MONEY),
			keybind = "AUTO_DEPOSIT_MONEY",
			callback = function() Banker.safeMoney() end,
			visible = function() return Banker.isBankOpen end,
		},
		alignment = KEYBIND_STRIP_ALIGN_CENTER,
	}
	
	s.init()
	
	s.add({
		type = "header",
		name = SI.get(SI.COMMON_TITLE),
	});
		
	s.add({
		type  = "description",
		text  = SI.get(SI.COMMON_DESC),
	})
	
	s.add({
		type = "header",
		name = SI.get(SI.HEADER_ITEMS),
	});
	
	s.add({
		type  = "description",
		text  = SI.get(SI.DESC_ITEMS),
	})
	
	s.add({
		type = "checkbox",
		name = SI.get(SI.EITEMS_TITLE),
		tooltip = "",
		getFunc = function()
			return Banker.vars.items
		end,
		setFunc = function(bool)
			Banker.vars.items = bool
		end,
		default = function()
			return defaults.items
		end
	})
	
	for i = 0, #Banker.ItemTypes, 1 do
		
		s.add({
			type = "checkbox",
			name = SI.get(SI.ITEMTYPE[Banker.ItemTypes[i]]),
			tooltip = "",
			getFunc = function()
				return Banker.vars.iTypes[Banker.ItemTypes[i]]
			end,
			setFunc = function(bool)
				Banker.vars.iTypes[Banker.ItemTypes[i]] = bool
			end,
			default = function()
				return defaults.iTypes[Banker.ItemTypes[i]]
			end
		})
	end
	
	s.add({
		type = "header",
		name = SI.get(SI.HEADER_MOVE),
	});
	
	s.add({
		type  = "description",
		text  = SI.get(SI.DESC_MOVE),
	})
	
	s.add({
		type = "checkbox",
		name = SI.get(SI.EMOVE_TITLE),
		tooltip = "",
		getFunc = function()
			return Banker.vars.move
		end,
		setFunc = function(bool)
			Banker.vars.move = bool
		end,
		default = function()
			return defaults.move
		end
	})
	
	s.add({
		type = "slider",
		name = SI.get(SI.MIN_MOVE_TITLE),
		max  = 100,
		min  = 1,
		getFunc =  function()
			return Banker.vars.minMoveStack
		end,
		setFunc = function(arg0)
			Banker.vars.minMoveStack = arg0
		end,
		default = function()
			return defaults.minMoveStack
		end,
	})
	
	s.add({
		type = "header",
		name = SI.get(SI.HEADER_MONEY),
	});
	
	s.add({
		type  = "description",
		text  = SI.get(SI.DESC_MONEY),
	})
	
	s.add({
		type = "checkbox",
		name = SI.get(SI.EMONEY_TITLE),
		tooltip = "",
		getFunc = function()
			return Banker.vars.money
		end,
		setFunc = function(bool)
			Banker.vars.money = bool
		end,
		default = function()
			return defaults.money
		end
	})
	
	s.add({
		type = "slider",
		name = SI.get(SI.STEP_TITLE),
		max  = 1000,
		min  = 5,
		getFunc =  function()
			return Banker.vars.mStep
		end,
		setFunc = function(arg0)
			Banker.vars.mStep = arg0
		end,
		default = function()
			return defaults.mStep
		end,
	})
	
	s.add({
		type = "slider",
		name = SI.get(SI.MIN_TITLE),
		max  = 10000,
		min  = 500,
		getFunc =  function()
			return Banker.vars.mMin
		end,
		setFunc = function(arg0)
			Banker.vars.mMin = arg0
		end,
		default = function()
			return defaults.mMin
		end,
	})
	
	s.add({
		type = "checkbox",
		name = SI.get(SI.KB_TITLE),
		tooltip = "",
		getFunc = function()
			return Banker.vars.keymenu
		end,
		setFunc = function(bool)
			Banker.vars.keymenu = bool
		end,
		default = function()
			return defaults.keymenu
		end
	})
	
	s.add({
		type = "header",
		name = SI.get(SI.HEADER_DEVELOPER),
	})
	
	s.add({
		type  = "description",
		text  = SI.get(SI.WARNING_DEVELOPER),
	})
	
	s.add({
		type = "checkbox",
		name = SI.get(SI.MSG_TITLE),
		warning = SI.get(SI.WARNING_DEVELOPER),
		getFunc = function()
			return Banker.vars.msg
		end,
		setFunc = function(bool)
			Banker.vars.msg = bool
		end,
		default = function()
			return defaults.msg
		end
	})
	
	s.add({
		type = "checkbox",
		name = SI.get(SI.DEBUG_TITLE),
		warning = SI.get(SI.WARNING_DEVELOPER),
		getFunc = function()
			return Banker.vars.debug
		end,
		setFunc = function(bool)
			Banker.vars.debug = bool
		end,
		default = function()
			return defaults.debug
		end
	})
end

Banker.onLoaded = function(event, name)
	if name ~= "Banker" then return end
	
	Banker.initVars()
	Banker.vars = ZO_SavedVars:New("BankerVariables", VERSION, nil, defaults)
	Banker.vars.lang = GetCVar("language.2") or "en"

	if Banker.vars.lang ~= "en" or Banker.vars.lang ~= "de" or Banker.vars.lang ~= "fr" then
		Banker.mDebug(string.format("%s is not supported. Banker will use standard language(en).", Banker.vars.lang))
	end

	Banker.initUI()
end

Banker.onOpenBank = function()
	Banker.toggleSyncBinding()

	if Banker.vars.items then
		Banker.stackItems()
	end
	
	if Banker.vars.move then
		Banker.moveStacks()
		Banker.stackItems()
	end

	if Banker.vars.money then
		Banker.safeMoney()
	end
end

Banker.onCloseBank = function()
	Banker.toggleSyncBinding()
end

Banker.stackItems = function()
	Banker.bankItems = Banker.getItems(BAG_BANK)
	Banker.backpackItems = Banker.getItems(BAG_BACKPACK)

	for i = 1, #Banker.backpackItems, 1 do
		for k = 1, #Banker.bankItems, 1 do
			if Banker.backpackItems[i].name == Banker.bankItems[k].name then
				if Banker.vars.iTypes[Banker.backpackItems[i].itemType] then
					Banker.transferItem(BAG_BACKPACK, BAG_BANK, i, k)
				end
			end
		end
	end
end

Banker.moveStacks = function()
	local backpackItems = Banker.getItems(BAG_BACKPACK)
	local freeSlots = GetNumBagFreeSlots(BAG_BACKPACK)
	
	if freeSlots > 0 then
		for i = 1, #backpackItems, 1 do
			if Banker.vars.iTypes[backpackItems[i].itemType] then
				if backpackItems[i].stack > Banker.vars.minMoveStack then
					Banker.transferItem(BAG_BACKPACK, BAG_BANK, i, FindFirstEmptySlotInBag(BAG_BANK))
				end
			end
		end
	end
end

Banker.safeMoney = function()
	local bankMoney = GetBankedMoney()
	local playerMoney = GetCurrentMoney()

	if playerMoney > Banker.vars.mMin then
		local n = math.floor( (playerMoney - Banker.vars.mMin) / Banker.vars.mStep )

		if n > 0 then
			local tMoney = n * Banker.vars.mStep

			DepositMoneyIntoBank(tMoney)
			Banker.msg(string.format("%s |t16:16:EsoUI/Art/currency/currency_gold.dds|t were transferd to your bank.", tMoney))
		end
	end
end

Banker.getItems = function(bagId)
	local items = {}
	local numBagSlots = GetBagSize(bagId)
	
	local j = 0;
	
	for i = 0, numBagSlots, 1 do
		icon, stack, _, _, _, _ = GetItemInfo(bagId, i)
		itemType = GetItemType(bagId, i)
		name = GetItemName(bagId, i)
		link = GetItemLink(bagId, i, LINK_STYLE_BRACKETS)
		
		if name ~= "" then
			items[j] = Banker.newItem(bagId, i, itemType, name, stack, link, icon)
			j = j+1
		end
	end
	
	Banker.Lib.mergeSort(items);
	
	return items
end

Banker.transferItem = function(fromBag, toBag, fromSlot, toSlot)

	local fStackSize = GetSlotStackSize(fromBag, fromSlot)
	local fName      = GetItemLink(fromBag, fromSlot)
	local tStackSize = GetSlotStackSize(toBag, toSlot)

	local stack      = (fStackSize + tStackSize) - 100

	if stack <= 0 and fStackSize ~= 0 then
		if Banker.Lib.stackItem(fromBag, fromSlot, toBag, toSlot, fStackSize, fName) then
			local texture = GetItemInfo(fromBag, fromSlot)
			Banker.msg(string.format("%s x [%s] was added to bank.", fStackSize, fName))
		else
			Banker.msg(string.format("[%s] wasn't added to bank.", fName))
		end
	elseif fStackSize ~= 0 then
		fStackSize = fStackSize - stack
		if fStackSize > 0 then
			if Banker.Lib.stackItem(fromBag, fromSlot, toBag, toSlot, fStackSize, fName) then
				local texture = GetItemInfo(fromBag, fromSlot)
				Banker.msg(string.format("%s x [%s] was added to bank.", fStackSize, fName))
			else
				Banker.msg(string.format("[%s] wasn't added to bank.", fName))
			end
		end
	end
end

Banker.toggleSyncBinding = function()
	Banker.isBankOpen = not Banker.isBankOpen
	
	if Banker.isBankOpen then
		Banker.mDebug("Banker.isBankOpen: true")
	else
		Banker.mDebug("Banker.isBankOpen: false");
	end
	
	if Banker.vars.keymenu then
		if Banker.isBankOpen then
			Banker.mDebug("show")
			KEYBIND_STRIP:AddKeybindButtonGroup(Banker.KeybindStripDescriptor)
			KEYBIND_STRIP:UpdateKeybindButtonGroup(Banker.KeybindStripDescriptor)
		else
			Banker.mDebug("hide")
			KEYBIND_STRIP:RemoveKeybindButtonGroup(Banker.KeybindStripDescriptor)
		end
	end
end

Banker.mDebug = function(arg0)
	if Banker.vars.debug then
		CHAT_SYSTEM:AddMessage("[BANKER] " .. arg0)
	end
end

Banker.msg = function(arg0)
	if Banker.vars.msg then
		CHAT_SYSTEM:AddMessage(arg0)
	end
end

EVENT_MANAGER:RegisterForEvent("BankerOnBankOpen", EVENT_OPEN_BANK, Banker.onOpenBank)
EVENT_MANAGER:RegisterForEvent("BankerOnBankClose", EVENT_CLOSE_BANK, Banker.onCloseBank)
EVENT_MANAGER:RegisterForEvent("BankerOnLoad", EVENT_ADD_ON_LOADED, Banker.onLoaded)

Banker.Lib = {}

Banker.Lib.stackItem = function(fromBag, fromSlot, toBag, toSlot, quantity, name)
    Banker.mDebug("stacking")
    local result = true
    -- just in case
    ClearCursor()
    -- must call secure protected (pickup the item via cursor)
    result = CallSecureProtected("PickupInventoryItem", fromBag, fromSlot, quantity)
    Banker.mDebug("called secure protected")
    if (result) then
        -- must call secure protected (drop the item on the cursor)
		Banker.mDebug("called secure protected2")
        result = CallSecureProtected("PlaceInInventory", toBag, toSlot)
    end
    -- clear the cursor to avoid issues
    ClearCursor()
    return result
end

Banker.Lib.merge = function(a1, a2) 
   local result = {} 
   local a1_len = #(a1) ; local a2_len = #(a2)
   local i1 = 1 ;         local i2 = 1

   for j = 1, a1_len+a2_len do 
      if i2 > a2_len            -- used up a2? 
         or 
         (i1 <= a1_len          -- a1 still available 
          and a1[i1].stack <= a2[i2].stack) -- and value is small
      then
         result[j] = a1[i1]     -- plug it in
         i1 = i1+1              -- and increment index
      else                      
         result[j] = a2[i2]     -- must use a2 next 
         i2 = i2+1
      end
   end 
   return result 
end 

Banker.Lib.splitArray = function(a) 
   local r1 = {} 
   local r2 = {} 
   local a_len = #a 
   local mid = math.floor( a_len / 2)

   -- first half goes into r1

   for i = 1, mid do 
      r1[i] = a[i]              -- copy to r1
   end 

   for i = mid+1, a_len do 
      r2[i-mid] = a[i]          -- copy to r2, 
      -- but remember to slide the a entries over! 
   end 

   return r1,r2                 -- this works: it returns them both 
end 

Banker.Lib.mergeSort = function(a) 
	if #a <= 1 then 
		return a     -- too small to be unsorted 
	else 
		local a1, a2 = 
		Banker.Lib.splitArray(a)         -- construct the two subproblems 

	return
    Banker.Lib.merge(                    -- merge the results 
		Banker.Lib.mergeSort(a1),        -- of solving them both 
		Banker.Lib.mergeSort(a2))        -- recursively
   end 
end 