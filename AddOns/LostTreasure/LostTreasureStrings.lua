LOST_TREASURE_STRINGS = {
	["en"] = {
		TREASURE_ON_MAP = "Show Treasure on Map",
		TREASURE_ON_MAP_TOOLTIP = "Show treasure map locations on player map?",
		TREASURE_ON_COMPASS = "Show Treasure on Compass",
		TREASURE_ON_COMPASS_TOOLTIP = "Show treasure map locations on compass bar?",
		TREASURE_ICON = "Treasure Pin Texture",
		TREASURE_ICON_TOOLTIP = "Which pin texture to use for treasure map locations",
		TREASURE_MARK_WHICH = "Treasure Map Marking",
		TREASURE_MARK_WHICH_TOOLTIP = "When to mark treasure locations on the player map",
		
		SURVEYS_ON_MAP = "Show Crafting Surveys on Map",
		SURVEYS_ON_MAP_TOOLTIP = "Show crafting survey locations on player map?",
		SURVEYS_ON_COMPASS = "Show Crafting Surveys on Compass",
		SURVEYS_ON_COMPASS_TOOLTIP = "Show crafting survey locations on compass bar?",
		SURVEYS_ICON = "Survey Pin Texture",
		SURVEYS_ICON_TOOLTIP = "Which pin texture to use for crafting survey locations",
		SURVEYS_MARK_WHICH = "Survey Map Marking",
		SURVEYS_MARK_WHICH_TOOLTIP = "When to mark survey locations on the player map",
		
		PIN_SIZE = "Pin Texture Size",
		PIN_SIZE_TOOLTIP = "Size of the pin",
		
		SHOW_MINIMAP = "Show Mini Treasure Map",
		SHOW_MINIMAP_TOOLTIP = "Show the mini treasure map on screen when using map from inventory?",
	},
	["fr"] = {
		TREASURE_ON_MAP = "Show Treasure on Map",
		TREASURE_ON_MAP_TOOLTIP = "Show treasure map locations on player map?",
		TREASURE_ON_COMPASS = "Show Treasure on Compass",
		TREASURE_ON_COMPASS_TOOLTIP = "Show treasure map locations on compass bar?",
		TREASURE_ICON = "Treasure Pin Texture",
		TREASURE_ICON_TOOLTIP = "Which pin texture to use for treasure map locations",
		TREASURE_MARK_WHICH = "Treasure Map Marking",
		TREASURE_MARK_WHICH_TOOLTIP = "When to mark treasure locations on the player map",
		
		SURVEYS_ON_MAP = "Show Crafting Surveys on Map",
		SURVEYS_ON_MAP_TOOLTIP = "Show crafting survey locations on player map?",
		SURVEYS_ON_COMPASS = "Show Crafting Surveys on Compass",
		SURVEYS_ON_COMPASS_TOOLTIP = "Show crafting survey locations on compass bar?",
		SURVEYS_ICON = "Survey Pin Texture",
		SURVEYS_ICON_TOOLTIP = "Which pin texture to use for crafting survey locations",
		SURVEYS_MARK_WHICH = "Survey Map Marking",
		SURVEYS_MARK_WHICH_TOOLTIP = "When to mark survey locations on the player map",
		
		PIN_SIZE = "Pin Texture Size",
		PIN_SIZE_TOOLTIP = "Size of the pin",
		
		SHOW_MINIMAP = "Show Mini Treasure Map",
		SHOW_MINIMAP_TOOLTIP = "Show the mini treasure map on screen when using map from inventory?",
	},
	["de"] = {
		TREASURE_ON_MAP = "Show Treasure on Map",
		TREASURE_ON_MAP_TOOLTIP = "Show treasure map locations on player map?",
		TREASURE_ON_COMPASS = "Show Treasure on Compass",
		TREASURE_ON_COMPASS_TOOLTIP = "Show treasure map locations on compass bar?",
		TREASURE_ICON = "Treasure Pin Texture",
		TREASURE_ICON_TOOLTIP = "Which pin texture to use for treasure map locations",
		TREASURE_MARK_WHICH = "Treasure Map Marking",
		TREASURE_MARK_WHICH_TOOLTIP = "When to mark treasure locations on the player map",
		
		SURVEYS_ON_MAP = "Show Crafting Surveys on Map",
		SURVEYS_ON_MAP_TOOLTIP = "Show crafting survey locations on player map?",
		SURVEYS_ON_COMPASS = "Show Crafting Surveys on Compass",
		SURVEYS_ON_COMPASS_TOOLTIP = "Show crafting survey locations on compass bar?",
		SURVEYS_ICON = "Survey Pin Texture",
		SURVEYS_ICON_TOOLTIP = "Which pin texture to use for crafting survey locations",
		SURVEYS_MARK_WHICH = "Survey Map Marking",
		SURVEYS_MARK_WHICH_TOOLTIP = "When to mark survey locations on the player map",
		
		PIN_SIZE = "Pin Texture Size",
		PIN_SIZE_TOOLTIP = "Size of the pin",
		
		SHOW_MINIMAP = "Show Mini Treasure Map",
		SHOW_MINIMAP_TOOLTIP = "Show the mini treasure map on screen when using map from inventory?",
	},
	["ru"] = {
		TREASURE_ON_MAP = "Coêpoáèûa îa êapòe",
		TREASURE_ON_MAP_TOOLTIP = "Ïoêaçÿáaòö ïoìoæeîèe coêpoáèû îa êapòe äìü èíeôûèxcü êapò coêpoáèû?",
		TREASURE_ON_COMPASS = "Coêpoáèûa îa êoíïace",
		TREASURE_ON_COMPASS_TOOLTIP = "Ïoêaçÿáaòö ïoìoæeîèe coêpoáèû îa êoíïace äìü èíeôûèxcü êapò coêpoáèû?",
		TREASURE_ICON = "Èêoîêa",
		TREASURE_ICON_TOOLTIP = "Áÿàepèòe èêoîêó coêpoáèû äìü oòoàpaæeîèü îa êapòe",
		TREASURE_MARK_WHICH = "Oòíeùaòö",
		TREASURE_MARK_WHICH_TOOLTIP = "Êoâäa oòíeùaòö êapòÿ coêpoáèû îa êapòe",
		
		SURVEYS_ON_MAP = "Peíecìeîîÿe êapòÿ",
		SURVEYS_ON_MAP_TOOLTIP = "Ïoêaçÿáaòö ïoìoæeîèe peíecìeîîÿx êapò îa êapòe äìü èíeôûèxcü peíecìeîîÿx êapò?",
		SURVEYS_ON_COMPASS = "Peíecìeîîÿe êapòÿ îa êoíïace",
		SURVEYS_ON_COMPASS_TOOLTIP = "Ïoêaçÿáaòö ïoìoæeîèe peíecìeîîÿx êapò îa êoíïace äìü èíeôûèxcü peíecìeîîÿx êapò?",
		SURVEYS_ICON = "Èêoîêa",
		SURVEYS_ICON_TOOLTIP = "Áÿàepèòe èêoîêó peíecìeîîÿx êapò äìü oòoàpaæeîèü îa êapòe",
		SURVEYS_MARK_WHICH = "Oòíeùaòö",
		SURVEYS_MARK_WHICH_TOOLTIP = "Êoâäa oòíeùaòö peíecìeîîÿe êapòÿ îa êapòe",
		
		PIN_SIZE = "Paçíep èêoîêè",
		PIN_SIZE_TOOLTIP = "Çaäaeò paçíep èêoîêè coêpoáèû",
		
		SHOW_MINIMAP = "Íèîè-êapòa coêpoáèû",
		SHOW_MINIMAP_TOOLTIP = "Ïoêaçaòö íaìeîöêó êoïèô êapòÿ coêpoáèû îa ëêpaîe, êoâäa ècïoìöçóeòe êapòó á èîáeîòape?",
	},
}
