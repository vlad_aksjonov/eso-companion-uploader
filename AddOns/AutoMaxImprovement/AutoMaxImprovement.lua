AutoMaxImprovement = {}
 
AutoMaxImprovement.name = "AutoMaxImprovement"

function AutoMaxImprovement:Initialize()
    local zorig = SMITHING.improvementPanel.OnSlotChanged
    SMITHING.improvementPanel.OnSlotChanged = function (...)
        local SI = SMITHING.improvementPanel
        zorig(...)
        local hasItem = SI.improvementSlot:HasItem()
        if hasItem then
            local row = SI:GetRowForSelection()
            if row then
                local max = SI:FindMaxBoostersToApply()
                if max then
                    SI.spinner:SetValue(max)
                    -- we don't care about the return value, usually it occurs when upgraded stacked items
                end
            end
        end
    end
end
 
function AutoMaxImprovement.OnAddOnLoaded(event, addonName)
  if addonName == AutoMaxImprovement.name then
    AutoMaxImprovement:Initialize()
  end
end
 
EVENT_MANAGER:RegisterForEvent(AutoMaxImprovement.name, EVENT_ADD_ON_LOADED, AutoMaxImprovement.OnAddOnLoaded)
