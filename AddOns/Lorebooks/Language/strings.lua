local strings = {

	LBOOKS_ACTION_SEARCH				= "Search",
	LBOOKS_LOST							= "Book has been lost",
	LBOOKS_QUEST_BOOK					= "Book available through a quest",
	
	LBOOKS_SPECIAL_ZONE1				= "Solo Dungeon",
	LBOOKS_SPECIAL_ZONE2				= "Group Dungeon",
	LBOOKS_SPECIAL_ZONE3				= "Trial",
	LBOOKS_SPECIAL_ZONE4				= "Main Storyline",
	LBOOKS_SPECIAL_ZONE5				= "Mages Guild Storyline",
	LBOOKS_SPECIAL_ZONE6				= "Fighters Guild Storyline",
	LBOOKS_SPECIAL_ZONE7				= "Thieves Heist",
	
	LBOOKS_QUEST_IN_ZONE				= "Quest in <<1>>",
	LBOOKS_LINK_TO_UNKN_QUEST		= "This book is maybe not here, because it is linked to an unknown quest",
	LBOOKS_BOOK_DISAPEAR				= "Disapear after quest completion",
	
	-- Thank You for datamining
	
	LBOOKS_THANK_YOU1					= "Thank you",
	LBOOKS_THANK_YOU_LONG1			= "You're the first to find this lost book",
	
	LBOOKS_THANK_YOU10				= "Keep it up!",
	LBOOKS_THANK_YOU_LONG10			= "And we'll get all those old books recorded",
	
	LBOOKS_THANK_YOU20				= "Librarian",
	LBOOKS_THANK_YOU_LONG20			= "By the way, did you read those books ? You should",
	
	LBOOKS_THANK_YOU30				= "Keeper of the Elders Scrolls",
	LBOOKS_THANK_YOU_LONG30			= "It could be a nice title for you",
	
	-- Congratulations for collecting
	
	LBOOKS_THANK_YOU2000				= "Already ?",
	LBOOKS_THANK_YOU_LONG2000		= "You found 2000 books over Tamriel",
	
	LBOOKS_THANK_YOU2500				= "Another type of achievement",
	LBOOKS_THANK_YOU_LONG2500		= "You really wanted to collect those 2500 books",
	
	LBOOKS_THANK_YOU2600				= "All those books read",
	LBOOKS_THANK_YOU_LONG2600		= "Consider writing one too",
	
	LBOOKS_THANK_YOU2700				= "Seeker of Knowledge",
	LBOOKS_THANK_YOU_LONG2700		= "You made the longest",
	
	LBOOKS_THANK_YOU2800				= "Path of the wisdom",
	LBOOKS_THANK_YOU_LONG2800		= "Sill a bit more books and you won't be forgotten",
	
	LBOOKS_THANK_YOU2821				= "Congratulations",
	LBOOKS_THANK_YOU_LONG2821		= "There is no one else like you",
	
	LBOOKS_THANK_YOU2900				= "Awakened Mind",
	LBOOKS_THANK_YOU_LONG2900		= "Take your time to enjoy",
	
	LBOOKS_THANK_YOU3000				= "Only a few more",
	LBOOKS_THANK_YOU_LONG3000		= "And you'll achieve something great",
	
	LBOOKS_THANK_YOU3010				= "Congratulations",
	LBOOKS_THANK_YOU_LONG3010		= "There is no one else like you",
	
   --tooltips
   LBOOKS_KNOWN                  = "Collected",

   LBOOKS_MOREINFO1              = "Town",
   LBOOKS_MOREINFO2              = "Delve",
   LBOOKS_MOREINFO3              = "Public Dungeon",
   LBOOKS_MOREINFO4              = "Underground",
   LBOOKS_MOREINFO5              = "Group Instance",

   LBOOKS_SET_WAYPOINT           = "Set waypont to |cFFFFFF<<1>>|r",

   --settings menu header
   LBOOKS_TITLE                  = "LoreBooks",

   --appearance
   LBOOKS_PIN_TEXTURE            = "Select map pin icons",
   LBOOKS_PIN_TEXTURE_DESC       = "Select map pin icons.",
   LBOOKS_PIN_SIZE               = "Pin size",
   LBOOKS_PIN_SIZE_DESC          = "Set the size of the map pins.",
   LBOOKS_PIN_LAYER              = "Pin layer",
   LBOOKS_PIN_LAYER_DESC         = "Set the layer of the map pins",

   --compass
   LBOOKS_COMPASS_UNKNOWN        = "Show lorebooks on the compass.",
   LBOOKS_COMPASS_UNKNOWN_DESC   = "Show/Hide icons for unknown lorebooks on the compass.",
   LBOOKS_COMPASS_DIST           = "Max pin distance",
   LBOOKS_COMPASS_DIST_DESC      = "The maximum distance for pins to appear on the compass.",

   --filters
   LBOOKS_UNKNOWN                = "Show unknown lorebooks",
   LBOOKS_UNKNOWN_DESC           = "Show/Hide icons for unknown lorebooks on the map.",
   LBOOKS_COLLECTED              = "Show already collected lorebooks",
   LBOOKS_COLLECTED_DESC         = "Show/Hide icons for already collected lorebooks on the map.",
   
	LBOOKS_SHARE_DATA					= "Share your discoveries with LoreBooks author",
   LBOOKS_SHARE_DATA_DESC			= "Enabling this option will share your discoveries with LoreBooks author by sending automatically an ingame mail with data collected.\nThis option is only available for EU Users, even if data collected is shared with NA ones\nPlease note that you may encounter a small lag with your skills when the mail is sent. Mail is silently sent every 30 books read.",

	LBOOKS_EIDETIC						= "Show unknown Eidetic Memory",
	LBOOKS_EIDETIC_DESC				= "Show/Hide unknown Eidetic Memory scrolls on map. Those scrolls are lore-related scrolls not involved into Mages Guild Progression, but only informative about Tamriel",
	LBOOKS_COMPASS_EIDETIC			= "Show unknown Eidetic Memory on compass",
	LBOOKS_COMPASS_EIDETIC_DESC	= "Show/Hide unknown Eidetic Memory scrolls on compass. Those scrolls are lore-related scrolls not involved into Mages Guild Progression, but only informative about Tamriel",
	
   --worldmap filters
   LBOOKS_FILTER_UNKNOWN         = "Unknown lorebooks",
   LBOOKS_FILTER_COLLECTED       = "Collected lorebooks",

   --copy button
   LBOOKS_SELECT_ALL             = "Select all",
   LBOOKS_COPY_CLIPBOARD         = "Press CTRL+C to copy to clipboard!",
	
	LBOOKS_SEARCH_LABEL				= "Search in the lore library :",
	LBOOKS_SEARCH_PLACEHOLDER		= "Lorebook Name",
	
	LBOOKS_RANDOM_POSITION			= "[Bookshelves]",
	
}

for stringId, stringValue in pairs(strings) do
   ZO_CreateStringId(stringId, stringValue)
   SafeAddVersion(stringId, 1)
end
