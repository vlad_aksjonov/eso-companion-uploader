----------------------------------------
-- French localization for LoreBooks --
----------------------------------------
--
-- Translated by:
-- Ykses (http://www.esoui.com/forums/member.php?u=1521)
--

SafeAddString(LBOOKS_ACTION_SEARCH,				"Fouiller", 1)
SafeAddString(LBOOKS_LOST,							"Le livre a été perdu", 1)
SafeAddString(LBOOKS_QUEST_BOOK,					"Livre accessible via une quête", 1)

SafeAddString(LBOOKS_SPECIAL_ZONE1,				"Donjon Solo", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE2,				"Donjon de Groupe", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE3,				"Epreuve", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE4,				"Quête Principale", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE5,				"Quête de la Guilde des Mages", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE6,				"Quêtes de la Guilde des Guerriers", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE7,				"Domaine à cambrioler", 1)

SafeAddString(LBOOKS_QUEST_IN_ZONE,				"Quête en <<1>>", 1)
SafeAddString(LBOOKS_LINK_TO_UNKN_QUEST,		"Associé avec une quête inconnue", 1)
SafeAddString(LBOOKS_BOOK_DISAPEAR,				"Disparait après la quête", 1)

SafeAddString(LBOOKS_THANK_YOU1,					"Merci", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG1,			"Vous êtes le premier à retrouver ce livre perdu", 1)

SafeAddString(LBOOKS_THANK_YOU10,				"Continuez comme ça !", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG10,			"Et tous ces vieux livres seront répertoriés", 1)

SafeAddString(LBOOKS_THANK_YOU20,				"Libraire", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG20,			"Est-ce que vous lisez vraiment ces livres ? Vous devriez", 1)

SafeAddString(LBOOKS_THANK_YOU30,				"Gardien des Parchemins des Anciens", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG30,			"Ça pourrait être un joli titre pour vous", 1)

SafeAddString(LBOOKS_THANK_YOU2000,				"Déjà ?", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2000,		"Vous avez retrouvé 2000 livres à travers Tamriel", 1)
	
SafeAddString(LBOOKS_THANK_YOU2500,				"Une autre forme de succès", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2500,		"Vous vouliez vraiment collecter ces 2500 livres", 1)
	
SafeAddString(LBOOKS_THANK_YOU2600,				"Tous ces livres lus", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2600,		"Pensez à en écrire un également", 1)
	
SafeAddString(LBOOKS_THANK_YOU2700,				"Chercheur de savoir", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2700,		"Vous avez fait le plus long", 1)
	
SafeAddString(LBOOKS_THANK_YOU2800,				"Le chemin de la sagesse", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2800,		"Encore quelques livres et vous ne serez pas oublié", 1)
	
SafeAddString(LBOOKS_THANK_YOU2821,				"Félicitations", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2821,		"Il n'y a personne d'autre comme vous", 1)
	
SafeAddString(LBOOKS_THANK_YOU2900,				"Esprit éveillé", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2900,		"Prenez le temps de savourer", 1)
	
SafeAddString(LBOOKS_THANK_YOU3000,				"Encore quelques-uns", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG3000,		"Et vous allez réaliser quelque chose d'important", 1)
	
SafeAddString(LBOOKS_THANK_YOU3010,				"Félicitations", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG3010,		"Il n'y a personne d'autre comme vous", 1)

--tooltips
SafeAddString(LBOOKS_KNOWN,						"collecté", 1)

SafeAddString(LBOOKS_MOREINFO1,					"Ville", 1)
SafeAddString(LBOOKS_MOREINFO2,					"Antres", 1)
SafeAddString(LBOOKS_MOREINFO3,					"Donjon public", 1)
SafeAddString(LBOOKS_MOREINFO4,					"sous la terre", 1)
SafeAddString(LBOOKS_MOREINFO5,					"Instance de groupe", 1)

--settings menu header
--SafeAddString(LBOOKS_TITLE,						"LoreBooks", 1)

--appearance
SafeAddString(LBOOKS_PIN_TEXTURE,				"Choix des icônes", 1)
SafeAddString(LBOOKS_PIN_TEXTURE_DESC,			"Choisir l'apparence des icônes sur la carte.", 1)
SafeAddString(LBOOKS_PIN_SIZE,					"Taille des icônes", 1)
SafeAddString(LBOOKS_PIN_SIZE_DESC,				"Choisir la taille des icônes sur la carte.", 1)
SafeAddString(LBOOKS_PIN_LAYER,					"Épaisseur des icônes", 1)
SafeAddString(LBOOKS_PIN_LAYER_DESC,			"Choisir l'épaisseur des icônes sur la carte.", 1)

--compass
SafeAddString(LBOOKS_COMPASS_UNKNOWN,			"Positions indiquées sur la boussole", 1)
SafeAddString(LBOOKS_COMPASS_UNKNOWN_DESC,	"Afficher/cacher les Livres (de la Guilde des Mages) non collectés sur la boussole.", 1)
SafeAddString(LBOOKS_COMPASS_DIST,				"Distance d'affichage maximum", 1)
SafeAddString(LBOOKS_COMPASS_DIST_DESC,		"Choisir la distance à partir de laquelle les Livres apparaissent sur la boussole.", 1)

--filters
SafeAddString(LBOOKS_UNKNOWN,						"Afficher les Livres inconnus", 1)
SafeAddString(LBOOKS_UNKNOWN_DESC,				"Afficher/cacher les icônes des Livres (de la Guilde des Mages) inconnus sur la carte.", 1)
SafeAddString(LBOOKS_COLLECTED,					"Afficher les Livres collectés", 1)
SafeAddString(LBOOKS_COLLECTED_DESC,			"Afficher/cacher les icônes des Livres (de la Guilde des Mages) déjà collectés sur la carte.", 1)

SafeAddString(LBOOKS_EIDETIC,						"Mémoire Éidétique inconnue sur la carte", 1)
SafeAddString(LBOOKS_EIDETIC_DESC,				"Affiche/Masque les parchemins inconnus présents dans la Mémoire Éidétique sur la carte. Ces parchemins promulguent diverses informations sur le lore de Tamriel et ne sont en rien impliqués dans la progression de la Guilde des Mages", 1)
SafeAddString(LBOOKS_COMPASS_EIDETIC,			"Mémoire Éidétique inconnue sur le compas", 1)
SafeAddString(LBOOKS_COMPASS_EIDETIC_DESC,	"Affiche/Masque les parchemins inconnus présents dans la Mémoire Éidétique sur le compas. Ces parchemins promulguent diverses informations sur le lore de Tamriel et ne sont en rien impliqués dans la progression de la Guilde des Mages", 1)

SafeAddString(LBOOKS_SHARE_DATA,					"Partagez vos découvertes avec l'auteur", 1)
SafeAddString(LBOOKS_SHARE_DATA_DESC,			"Activer cette option partagera vos découvertes avec l'auteur de LoreBooks en lui envoyant automatiquement un mail en jeu avec les données collectées.\nCette option n'est disponible que pour les joueurs du Serveur EU, même si les données collectées sont partagées avec le serveur NA\nVeuillez noter que vous pourrez rencontrer un petit lag de vos compétences lors de l'envoi d'un mail. Le message est silencieusement envoyé tous les 30 livres lus.", 1)

--worldmap filters
SafeAddString(LBOOKS_FILTER_UNKNOWN,			"Livres inconnus", 1)
SafeAddString(LBOOKS_FILTER_COLLECTED,			"Livres collectés", 1)

--copy button
SafeAddString(LBOOKS_SELECT_ALL,					"Sélectionner tout", 1)
SafeAddString(LBOOKS_COPY_CLIPBOARD,			"Appuyez sur CTRL+C pour copier dans le presse papier!", 1)

--research
SafeAddString(LBOOKS_SEARCH_LABEL,				"Recherche dans la bibliothèque :", 1)
SafeAddString(LBOOKS_SEARCH_PLACEHOLDER,		"Nom du livre", 1)

SafeAddString(LBOOKS_RANDOM_POSITION,			"[Étagères]", 1)