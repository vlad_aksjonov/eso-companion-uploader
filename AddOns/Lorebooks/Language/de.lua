---------------------------------------
-- German localization for LoreBooks --
---------------------------------------
--
-- Translated by:
-- Bl4ckSh33p (http://www.esoui.com/forums/member.php?u=683)
--

SafeAddString(LBOOKS_ACTION_SEARCH,				"Durchsuchen", 1)
SafeAddString(LBOOKS_LOST,							"Das Buch ging verloren", 1)

SafeAddString(LBOOKS_SPECIAL_ZONE1,				"Solo Verlies", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE2,				"Gruppen Verlies", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE3,				"Prüfung", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE4,				"Hauptgeschichte", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE5,				"Magiergilden Geschichte", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE6,				"Kriegergilden Geschichte", 1)
SafeAddString(LBOOKS_SPECIAL_ZONE7,				"Diebesnest", 1)

SafeAddString(LBOOKS_THANK_YOU1,					"Vielen Dank", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG1,			"Du findest als Erster dieses verlorene Buch", 1)
  
SafeAddString(LBOOKS_THANK_YOU10,				"Mach weiter so!", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG10,			"So können wir alle alten Bücher a", 1)
  
SafeAddString(LBOOKS_THANK_YOU20,				"Bibliothekar", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG20,			"Hast du per Zufall eines dieser Bücher gelesen? Das solltest du", 1)
  
SafeAddString(LBOOKS_THANK_YOU30,				"Hüter von Elders Scrolls", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG30,			"Das könnte doch ein guter Titel für dich sein", 1)

SafeAddString(LBOOKS_THANK_YOU2000,				"Schon ?", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2000,		"Du hast 2000 Bücher über Tamriel gefunden", 1)

SafeAddString(LBOOKS_THANK_YOU2500,				"Eine weitere Errungenschaft", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2500,		"Du wolltest wirklich diese 2500 Bücher sammeln", 1)

SafeAddString(LBOOKS_THANK_YOU2600,				"Alle diese gelesenen Bücher", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2600,		"Ziehst du es in Betracht selber eines zu schreiben?", 1)

SafeAddString(LBOOKS_THANK_YOU2700,				"Wissenssucher", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2700,		"Du hast richtig Ausdauer", 1)

SafeAddString(LBOOKS_THANK_YOU2800,				"Pfad der Weisheit", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2800,		"Noch ein paar mehr und man wird dich nie vergessen", 1)

SafeAddString(LBOOKS_THANK_YOU2821,				"Herzlichen Glückwunsch", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2821,		"Es gibt niemanden wie dich", 1)

SafeAddString(LBOOKS_THANK_YOU2900,				"Erwachter Geist", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG2900,		"Nimm dir Zeit zum Geniessen", 1)

SafeAddString(LBOOKS_THANK_YOU3000,				"Nur ein paar mehr", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG3000,		"Und du wirst etwas Grosses erreichen", 1)

SafeAddString(LBOOKS_THANK_YOU3010,				"Herzlichen Glückwunsch", 1)
SafeAddString(LBOOKS_THANK_YOU_LONG3010,		"Es gibt niemanden wie dich", 1)

--tooltips
SafeAddString(LBOOKS_KNOWN,						"Eingesammelt", 1)

SafeAddString(LBOOKS_MOREINFO1,					"Stadt", 1)
SafeAddString(LBOOKS_MOREINFO2,					"Verlies", 1)
SafeAddString(LBOOKS_MOREINFO3,					"Offenes Verlies", 1)
SafeAddString(LBOOKS_MOREINFO4,					"unter der Erde", 1)
SafeAddString(LBOOKS_MOREINFO5,					"Gruppeninstanz", 1)

--settings menu header
SafeAddString(LBOOKS_TITLE,						"Bücher", 1)

--appearance
SafeAddString(LBOOKS_PIN_TEXTURE,				"Icon für die Kartenmarkierung", 1)
SafeAddString(LBOOKS_PIN_TEXTURE_DESC,			"Wähle ein Icon für die Kartenmarkierung.", 1)
SafeAddString(LBOOKS_PIN_SIZE,					"Größe der Kartenmarkierung", 1)
SafeAddString(LBOOKS_PIN_SIZE_DESC,				"Bestimmt die Größe der Kartenmarkierung auf der Karte.", 1)
SafeAddString(LBOOKS_PIN_LAYER,					"Ebene der Kartenmarkierung", 1)
SafeAddString(LBOOKS_PIN_LAYER_DESC,			"Bestimmt die Ebene der Kartenmarkierung.", 1)

--compass
SafeAddString(LBOOKS_COMPASS_UNKNOWN,			"Zeige Bücher auf dem Kompass", 1)
SafeAddString(LBOOKS_COMPASS_UNKNOWN_DESC,	"Zeige/Verberge Markierungen für noch nicht gesammelte Bücher auf dem Kompass.", 1)
SafeAddString(LBOOKS_COMPASS_DIST,				"Max. Entfernung der Kartenmarkierung", 1)
SafeAddString(LBOOKS_COMPASS_DIST_DESC,		"Die maximale Entfernung auf die Kartenmarkierungen am Kompass angezeigt werden.", 1)

--filters
SafeAddString(LBOOKS_UNKNOWN,						"Zeige unbekannte Bücher", 1)
SafeAddString(LBOOKS_UNKNOWN_DESC,				"Zeige/Verberge unbekannte Bücher auf der Karte.", 1)
SafeAddString(LBOOKS_COLLECTED,					"Zeige gesammelte Bücher", 1)
SafeAddString(LBOOKS_COLLECTED_DESC,			"Zeige/Verberge bereits eingesammelte Bücher auf der Karte.", 1)

SafeAddString(LBOOKS_SHARE_DATA,					"Teile deine Funde mit LoreBooks Author", 1)
SafeAddString(LBOOKS_SHARE_DATA_DESC,			"Bei der Aktivierung dieser Option wirst du deine Funde direkt mit dem LoreBooks Author teilen können. Es wird automatisch eine Ingame Nachricht mit den erfassten Daten zugeschickt.\nBitte beachte, dass leichte Verzögerungen bei deinen Fertigkeiten auftreten können, solange die Nachricht geschickt wird. Die Nachricht wird alle 30 gelesenen Bücher lautlos verschickt.", 1)

SafeAddString(LBOOKS_EIDETIC,						"Zeige unbekannte Ewige Erinnerung", 1)
SafeAddString(LBOOKS_EIDETIC_DESC,				"Zeige/Verberge unbekannte Schriften auf der Karte. Diese Schriften sind auf das Lore bezogen und nicht aus der Magiergilden Reihe. Sie sind rein informativ über Tamriel.", 1)
SafeAddString(LBOOKS_COMPASS_EIDETIC,			"Zeige unbekannte Ewige Erinnerung im Kompass", 1)
SafeAddString(LBOOKS_COMPASS_EIDETIC_DESC,	"Zeige/Verberge unbekannte Schriften im Kompass. Diese Schriften sind auf das Lore bezogen und nicht aus der Magiergilden Reihe. Sie sind rein informativ über Tamriel.", 1)

--worldmap filters
SafeAddString(LBOOKS_FILTER_UNKNOWN,			"Unbekannte Bücher", 1)
SafeAddString(LBOOKS_FILTER_COLLECTED,			"Eingesammelte Bücher", 1)

--copy button
SafeAddString(LBOOKS_SELECT_ALL,					"Alle auswählen", 1)
SafeAddString(LBOOKS_COPY_CLIPBOARD,			"Drücken Sie STRG+C zum Kopieren in die Zwischenablage!", 1)

--research
SafeAddString(LBOOKS_SEARCH_LABEL,				"Suche in der Bibliothek :", 1)
SafeAddString(LBOOKS_SEARCH_PLACEHOLDER,		"Name des Buches", 1)

--SafeAddString(LBOOKS_RANDOM_POSITION,		"[Bookshelves]", 1)