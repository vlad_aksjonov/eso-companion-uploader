----------------------------------------
-- Russian localization for LoreBooks --
----------------------------------------
--
-- Translated by:
-- @KiriX (http://www.esoui.com/forums/member.php?u=105)
--


--tooltips
SafeAddString(LBOOKS_KNOWN,               "Coàpaîo", 1)

SafeAddString(LBOOKS_MOREINFO1,           "Âopoä", 1)
SafeAddString(LBOOKS_MOREINFO2,           "Coìo-ïoäçeíeìöe", 1)
SafeAddString(LBOOKS_MOREINFO3,           "Ïóàìèùîoe ïoäçeíeìöe", 1)
SafeAddString(LBOOKS_MOREINFO4,           "Ïeûepa", 1)
SafeAddString(LBOOKS_MOREINFO5,           "Âpóïïoáoe ïoäçeíeìöe", 1)

--settings menu header
--SafeAddString(LBOOKS_TITLE,               "LoreBooks", 1)

--appearance
SafeAddString(LBOOKS_PIN_TEXTURE,         "Èêoîêa îa êapòe", 1)
SafeAddString(LBOOKS_PIN_TEXTURE_DESC,    "Áÿàepèòe èêoîêó îa êapòe.", 1)
SafeAddString(LBOOKS_PIN_SIZE,            "Paçíep èêoîêè", 1)
SafeAddString(LBOOKS_PIN_SIZE_DESC,       "Çaäaòö paçíep èêoîêè îa êapòe.", 1)
SafeAddString(LBOOKS_PIN_LAYER,           "Cìoé èêoîêè", 1)
SafeAddString(LBOOKS_PIN_LAYER_DESC,      "Çaäaòö cìoé èêoîêè îa êapòe", 1)

--compass
SafeAddString(LBOOKS_COMPASS_UNKNOWN,     "Ïoêaçÿáaòö êîèâè çîaîèé îa êoíïace.", 1)
SafeAddString(LBOOKS_COMPASS_UNKNOWN_DESC, "Ïoêaçaòö/cêpÿòö èêoîêè äìü îeèçáecòîÿx êîèâ çîaîèé îa êoíïace.", 1)
SafeAddString(LBOOKS_COMPASS_DIST,        "Íaêc. äècòaîœèü", 1)
SafeAddString(LBOOKS_COMPASS_DIST_DESC,   "Íaêcèíaìöîaü äècòaîœèü, îa êoòopoé èêoîêè àóäóò ïoüáìüòöcü îa êoíïace.", 1)

--filters
SafeAddString(LBOOKS_UNKNOWN,             "Ïoêaçÿáaòö îeèçáecòîÿe êîèâè çîaîèé", 1)
SafeAddString(LBOOKS_UNKNOWN_DESC,        "Ïoêaçaòö/cêpÿòö èêoîêè äìü îeèçáecòîÿx êîèâ çîaîèé îa êapòe.", 1)
SafeAddString(LBOOKS_COLLECTED,           "Ïoêaçÿáaòö óæe coàpaîîÿe êîèâè çîaîèé", 1)
SafeAddString(LBOOKS_COLLECTED_DESC,      "Ïoêaçaòö/cêpÿòö èêoîêè äìü óæe coàpaîîÿx êîèâ çîaîèé îa êapòe.", 1)

--worldmap filters
SafeAddString(LBOOKS_FILTER_UNKNOWN,      "Îeèçáecòîÿe êîèâè çîaîèé", 1)
SafeAddString(LBOOKS_FILTER_COLLECTED,    "Coàpaîîÿe êîèâè çîaîèé", 1)

--copy button
--SafeAddString(LBOOKS_SELECT_ALL,          "Select all", 1)
--SafeAddString(LBOOKS_COPY_CLIPBOARD,      "Press CTRL+C to copy to clipboard!", 1)

--research
--SafeAddString(LBOOKS_SEARCH_LABEL,			"Search in the lore library :", 1)
--SafeAddString(LBOOKS_SEARCH_PLACEHOLDER,	"Lorebook Name", 1)