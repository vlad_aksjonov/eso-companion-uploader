-------------------------------------------------------------------------------
-- LoreBooks v2.5.4
-------------------------------------------------------------------------------
--
-- Copyright (c) 2014, 2015 Ales Machat (Garkin)
--
-- Permission is hereby granted, free of charge, to any person
-- obtaining a copy of this software and associated documentation
-- files (the "Software"), to deal in the Software without
-- restriction, including without limitation the rights to use,
-- copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the
-- Software is furnished to do so, subject to the following
-- conditions:
--
-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
-- OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
-- HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
-- WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
-- OTHER DEALINGS IN THE SOFTWARE.
--
-------------------------------------------------------------------------------
--
-- DISCLAIMER:
--
-- This Add-on is not created by, affiliated with or sponsored by ZeniMax
-- Media Inc. or its affiliates. The Elder Scrolls® and related logos are
-- registered trademarks or trademarks of ZeniMax Media Inc. in the United
-- States and/or other countries. All rights reserved.
--
-- You can read the full terms at:
-- https://account.elderscrollsonline.com/add-on-terms
--
-------------------------------------------------------------------------------

--Libraries--------------------------------------------------------------------
local LAM = LibStub("LibAddonMenu-2.0")
local LMP = LibStub("LibMapPins-1.0")
local GPS = LibStub("LibGPS2")
local Postmail = {}
local lastInteractionActionWas
local lastReticleUpdateWas = 0

--Local constants -------------------------------------------------------------
local ADDON_VERSION = "2.5.4"
local PINS_UNKNOWN = "LBooksMapPin_unknown"
local PINS_COLLECTED = "LBooksMapPin_collected"
local PINS_EIDETIC = "LBooksMapPin_eidetic"
--local PINS_EIDETIC_COLLECTED = "LBooksMapPin_eideticCollected"
local PINS_COMPASS = "LBooksCompassPin_unknown"
local PINS_COMPASS_EIDETIC = "LBooksCompassPin_eidetic"

--Local variables -------------------------------------------------------------
local updatePins = {}
local loreBoooksLibrary = {}
local totalCurrentlyCollected = 0
local updating = false
local mapIsShowing
local savedVariables			--user settings
local svData					--collected books
local newBooks = {}			--books which are not listed in LoreBooksData.lua
local defaults = {			--default settings for saved variables
	compassMaxDistance = 0.04,
	pinTexture = {
		type = 1,
		size = 26,
		level = 40,
	},
	filters = {
		[PINS_COMPASS_EIDETIC] = false,
		[PINS_COMPASS] = true,
		[PINS_UNKNOWN] = true,
		[PINS_COLLECTED] = false,
		[PINS_EIDETIC] = false,
--		[PINS_EIDETIC_COLLECTED] = false,
	},
	shareData = false,
	postmailData = "",
	postmailFirstInsert = GetTimeStamp(),
	booksFound = {},
	nbBooksFound = 0,
	booksCollected = {},
}
local dataDefaults = {
	lorebooks = {},
}

local INFORMATION_TOOLTIP

-- Local functions ------------------------------------------------------------
--prints message to chat
local function MyPrint(...)
	CHAT_SYSTEM:AddMessage(...)
end

--save data to the table
local function SetLocalData(dataTable, zone, subzone, localData)
	dataTable = dataTable or {}
	dataTable[zone] = dataTable[zone] or {}
	dataTable[zone][subzone] = dataTable[zone][subzone] or {}

	table.insert(dataTable[zone][subzone], localData)
end

--checks if normalized point is valid
local function InvalidPoint(x, y)
	return (x < 0 or x > 1 or y < 0 or y > 1)
end

local function GetEpsilon(zone, subzone)
	--Epsilon value should be bigger for small maps and smaller for big maps.
	-- As I have to determine epsilon even when it's not possible to check
	-- mapsize directly, I must find some kind of workaround...
	local epsilon = 0.007

	if subzone == "ava_whole" then							--Cyrodiil zone map
		epsilon = 0.003
	elseif zone == "cyrodiil" then							--all other subzones in Cyrodiil
		epsilon = 0.010
	elseif (subzone):find(zone) then							--zones
		epsilon = 0.005
	elseif zone == "guildmaps" or zone == "main" then	--Guild Maps, Main quest maps
		epsilon = 0.012
	end

	return epsilon
end

local function UnknownCoords(dataTable, x, y, epsilon)
	if not dataTable or dataTable == {} then
		return true
	end

	epsilon = epsilon or 0.005

	for i, data in ipairs(dataTable) do
		if zo_floatsAreEqual(data[1], x, epsilon) and zo_floatsAreEqual(data[2], y, epsilon) then
			return false
		end
	end

	return true
end

-- Pins -----------------------------------------------------------------------
local pinTexturesList = {
	[1] = "Shalidor's Library icons",
	[2] = "Book icon set 1",
	[3] = "Book icon set 2",
	[4] = "Esohead's icons (Rushmik)",
}

local pinTextures = {
	--[index] = { known_book_texture, unknown_book_texture },
	[1] = { "EsoUI/Art/Icons/lore_book4_detail4_color5.dds", "EsoUI/Art/Icons/lore_book4_detail4_color5.dds" },
	[2] = { "LoreBooks/Icons/book1.dds", "LoreBooks/Icons/book1-invert.dds" },
	[3] = { "LoreBooks/Icons/book2.dds", "LoreBooks/Icons/book2-invert.dds" },
	[4] = { "LoreBooks/Icons/book3.dds", "LoreBooks/Icons/book3-invert.dds" },
}

local function GetPinTexture(self)
	local _, texture, known = GetLoreBookInfo(1, self.m_PinTag[3], self.m_PinTag[4])
	local textureType = savedVariables.pinTexture.type
	return (pinTexturesList[textureType] == "Shalidor's Library icons") and texture or pinTextures[textureType][known and 1 or 2]
end

local function GetPinTextureEidetic(self)
	local _, texture, known = GetLoreBookInfo(2, self.m_PinTag.c, self.m_PinTag.b)
	local textureType = savedVariables.pinTexture.type
	return (pinTexturesList[textureType] == "Shalidor's Library icons") and texture or pinTextures[textureType][known and 1 or 2]
end

local function IsPinGrayscale()
	return pinTexturesList[savedVariables.pinTexture.type] == "Shalidor's Library icons"
end

--tooltip creator
local pinTooltipCreator = {}
pinTooltipCreator.tooltip = 1 --TOOLTIP_MODE.INFORMATION
pinTooltipCreator.creator = function(pin)
	local pinTag = pin.m_PinTag
	local title, icon, known = GetLoreBookInfo(1, pinTag[3], pinTag[4])
	local collection = GetLoreCollectionInfo(1, pinTag[3])
	local moreinfo = {}

	if pinTag[5] then
		table.insert(moreinfo, "[" .. GetString("LBOOKS_MOREINFO", pinTag[5]) .. "]")
	end
	if known then
		table.insert(moreinfo, "[" .. GetString(LBOOKS_KNOWN) .. "]")
	end
	
	if IsInGamepadPreferredMode() then
		INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, zo_strformat(collection), INFORMATION_TOOLTIP.tooltip:GetStyle("mapTitle"))
		INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, icon, title, {fontSize = 27, fontColorField = GAMEPAD_TOOLTIP_COLOR_GENERAL_COLOR_3})
		if #moreinfo > 0 then
			INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, table.concat(moreinfo, " / "), INFORMATION_TOOLTIP.tooltip:GetStyle("worldMapTooltip"))
		end
	else
		INFORMATION_TOOLTIP:AddLine(zo_strformat(collection), "ZoFontGameOutline", ZO_SELECTED_TEXT:UnpackRGB())
		ZO_Tooltip_AddDivider(INFORMATION_TOOLTIP)
		INFORMATION_TOOLTIP:AddLine(zo_iconTextFormat(icon, 32, 32, title), "", ZO_HIGHLIGHT_TEXT:UnpackRGB())
		if #moreinfo > 0 then
			INFORMATION_TOOLTIP:AddLine(table.concat(moreinfo, " / "), "", ZO_TOOLTIP_DEFAULT_COLOR:UnpackRGB())
		end
	end
	
end

--tooltip creator
local pinTooltipCreatorEidetic = {}
pinTooltipCreatorEidetic.tooltip = 1 --TOOLTIP_MODE.INFORMATION
pinTooltipCreatorEidetic.creator = function(pin)
	local pinTag = pin.m_PinTag
	local title, icon, known = GetLoreBookInfo(2, pinTag.c, pinTag.b)
	local collection = GetLoreCollectionInfo(2, pinTag.c)
	
	if IsInGamepadPreferredMode() then
		
		INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, zo_strformat(collection), INFORMATION_TOOLTIP.tooltip:GetStyle("mapTitle"))
		INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, title, {fontSize = 27, fontColorField = GAMEPAD_TOOLTIP_COLOR_GENERAL_COLOR_3})
		
		if pinTag.q then
			if type(pinTag.q) == "boolean" then
				INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, GetString(LBOOKS_LINK_TO_UNKN_QUEST), {fontSize = 27, fontColorField = GAMEPAD_TOOLTIP_COLOR_GENERAL_COLOR_2})
			elseif type(pinTag.q) == "table" then
				INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, GetString(LBOOKS_QUEST_BOOK), {fontSize = 27, fontColorField = GAMEPAD_TOOLTIP_COLOR_GENERAL_COLOR_2})
				INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, pinTag.q[GetCVar("Language.2")] or bookData.q["en"], {fontSize = 27, fontColorField = GAMEPAD_TOOLTIP_COLOR_GENERAL_COLOR_2})
				INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, zo_strformat(GetString(LBOOKS_QUEST_IN_ZONE), zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetMapNameByIndex(pinTag.qm))), {fontSize = 27, fontColorField = GAMEPAD_TOOLTIP_COLOR_GENERAL_COLOR_2})
			end
			if pinTag.u then
				INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, GetString(LBOOKS_BOOK_DISAPEAR), {fontSize = 27, fontColorField = GAMEPAD_TOOLTIP_COLOR_GENERAL_COLOR_2})
			end
		end
		
		if pinTag.d then
		
			local dungeonDifficulty = ""
			if pinTag.v ~= nil then -- can be false
				if pinTag.v then
					dungeonDifficulty = zo_strformat("(<<1>>)", GetString(SI_DUNGEONDIFFICULTY2))
				else
					dungeonDifficulty = zo_strformat("(<<1>>)", GetString(SI_DUNGEONDIFFICULTY1))
				end
			end
		
			if GetZoneNameByIndex(pinTag.z) == GetMapNameByIndex(pinTag.m) then
				INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, zo_strformat("[<<1>>]", GetString(SI_QUESTTYPE5)), {fontSize = 27, fontColorField = GAMEPAD_TOOLTIP_COLOR_GENERAL_COLOR_2})
			else
				INFORMATION_TOOLTIP:LayoutIconStringLine(INFORMATION_TOOLTIP.tooltip, nil, zo_strformat("[<<1>>] <<2>>", zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetZoneNameByIndex(pinTag.z)), dungeonDifficulty), {fontSize = 27, fontColorField = GAMEPAD_TOOLTIP_COLOR_GENERAL_COLOR_2})
			end
			
		end
		
	else
		
		INFORMATION_TOOLTIP:AddLine(zo_strformat(collection), "ZoFontGameOutline", ZO_SELECTED_TEXT:UnpackRGB())
		ZO_Tooltip_AddDivider(INFORMATION_TOOLTIP)
		
		local bookColor = ZO_HIGHLIGHT_TEXT
		if known then
			bookColor = ZO_SUCCEEDED_TEXT
		end
		
		INFORMATION_TOOLTIP:AddLine(zo_iconTextFormat(icon, 32, 32, title), "", bookColor:UnpackRGB())
		
		if pinTag.q then
			if type(pinTag.q) == "boolean" then
				INFORMATION_TOOLTIP:AddLine(GetString(LBOOKS_LINK_TO_UNKN_QUEST), "", ZO_SELECTED_TEXT:UnpackRGB())
			elseif type(pinTag.q) == "table" then
				INFORMATION_TOOLTIP:AddLine(GetString(LBOOKS_QUEST_BOOK), "", ZO_SELECTED_TEXT:UnpackRGB())
				INFORMATION_TOOLTIP:AddLine(zo_strformat("[<<1>>]", pinTag.q[GetCVar("Language.2")] or bookData.q["en"]), "", ZO_SELECTED_TEXT:UnpackRGB())
				INFORMATION_TOOLTIP:AddLine(zo_strformat(GetString(LBOOKS_QUEST_IN_ZONE), zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetMapNameByIndex(pinTag.qm))), "", ZO_HIGHLIGHT_TEXT:UnpackRGB())
			end
			if pinTag.u then
				INFORMATION_TOOLTIP:AddLine(GetString(LBOOKS_BOOK_DISAPEAR), "", ZO_SELECTED_TEXT:UnpackRGB())
			end
		end
		
		if pinTag.d then
		
			local dungeonDifficulty = ""
			if pinTag.v ~= nil then -- can be false
				if pinTag.v then
					dungeonDifficulty = zo_strformat("(<<1>>)", GetString(SI_DUNGEONDIFFICULTY2))
				else
					dungeonDifficulty = zo_strformat("(<<1>>)", GetString(SI_DUNGEONDIFFICULTY1))
				end
			end
		
			if GetZoneNameByIndex(pinTag.z) == GetMapNameByIndex(pinTag.m) then
				INFORMATION_TOOLTIP:AddLine(zo_strformat("[<<1>>]", GetString(SI_QUESTTYPE5)), "", ZO_SELECTED_TEXT:UnpackRGB())
			else
				INFORMATION_TOOLTIP:AddLine(zo_strformat("[<<1>>] <<2>>",  zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetZoneNameByIndex(pinTag.z)), dungeonDifficulty), "", ZO_SELECTED_TEXT:UnpackRGB())
			end
		end
	end
	
end

local function CreatePins()
	
	if (updatePins[PINS_COLLECTED] and LMP:IsEnabled(PINS_COLLECTED)) or (updatePins[PINS_UNKNOWN] and LMP:IsEnabled(PINS_UNKNOWN)) or (updatePins[PINS_COMPASS] and savedVariables.filters[PINS_COMPASS]) then
		local zone, subzone = LMP:GetZoneAndSubzone()
		local lorebooks = LoreBooks_GetLocalData(zone, subzone)
		if lorebooks then
			for _, pinData in ipairs(lorebooks) do	
				local _, _, known = GetLoreBookInfo(1, pinData[3], pinData[4])
				if known and updatePins[PINS_COLLECTED] and LMP:IsEnabled(PINS_COLLECTED) then
					LMP:CreatePin(PINS_COLLECTED, pinData, pinData[1], pinData[2])
				elseif not known then
					if updatePins[PINS_UNKNOWN] and LMP:IsEnabled(PINS_UNKNOWN) then
						LMP:CreatePin(PINS_UNKNOWN, pinData, pinData[1], pinData[2])
					end
					if updatePins[PINS_COMPASS] and savedVariables.filters[PINS_COMPASS] then
						COMPASS_PINS.pinManager:CreatePin(PINS_COMPASS, pinData, pinData[1], pinData[2])
					end
				end
			end
		end
		
	end
	
	if (updatePins[PINS_EIDETIC] and LMP:IsEnabled(PINS_EIDETIC)) or (updatePins[PINS_COMPASS_EIDETIC] and savedVariables.filters[PINS_COMPASS_EIDETIC]) then
		
		local mapIndex = GetCurrentMapIndex()
		local mapContentType = GetMapContentType()
		local usePrecalculatedCoords = true
		local zoneIndex
		local eideticBooks
		
		if not mapIndex then
		
			usePrecalculatedCoords = false
			if mapContentType == MAP_CONTENT_DUNGEON then
				zoneIndex = GetCurrentMapZoneIndex()
			else
				local measurements = GPS:GetCurrentMapMeasurements()
				mapIndex = measurements.mapIndex
			end
		end
		
		if mapIndex then
			eideticBooks = LoreBooks_GetEideticDataForMap(mapIndex)
		elseif zoneIndex then
			eideticBooks = LoreBooks_GetEideticDataForZone(zoneIndex)
		end
		
		if eideticBooks then
			for _, pinData in ipairs(eideticBooks) do
				local _, _, known = GetLoreBookInfo(2, pinData.c, pinData.b)
				if (not known and LMP:IsEnabled(PINS_EIDETIC)) then
					
					if not pinData.qm or pinData.qm == pinData.m then
					
						if usePrecalculatedCoords and pinData.zx and pinData.zy then
							pinData.xLoc = pinData.zx
							pinData.yLoc = pinData.zy
						else
							pinData.xLoc, pinData.yLoc = GPS:GlobalToLocal(pinData.x, pinData.y)
						end
						
						local CoordsOK = pinData.xLoc and pinData.yLoc
						if CoordsOK then
							if pinData.xLoc > 0 and pinData.yLoc > 0 and pinData.xLoc < 1 and pinData.yLoc < 1 then
								if (mapContentType == MAP_CONTENT_DUNGEON and pinData.d) or mapContentType ~= MAP_CONTENT_DUNGEON then
									if updatePins[PINS_EIDETIC] and LMP:IsEnabled(PINS_EIDETIC) then
										LMP:CreatePin(PINS_EIDETIC, pinData, pinData.xLoc, pinData.yLoc)
									end
								end
							end
							if updatePins[PINS_COMPASS_EIDETIC] and savedVariables.filters[PINS_COMPASS_EIDETIC] and ((mapContentType == MAP_CONTENT_DUNGEON and pinData.d) or (mapContentType ~= MAP_CONTENT_DUNGEON and pinData.d == false)) then
								COMPASS_PINS.pinManager:CreatePin(PINS_COMPASS_EIDETIC, pinData, pinData.xLoc, pinData.yLoc)
							end
						end
					end
				end
			end
			
		end
		
	end
	
	updatePins = {}
	updating = false
	
end

local function QueueCreatePins(pinType)
	updatePins[pinType] = true

	if not updating then
		updating = true
		if IsPlayerActivated() then
			if LMP.AUI.IsMinimapEnabled() then
				zo_callLater(CreatePins, 150) -- See SkyShards
			else
				CreatePins()
			end
		else
			EVENT_MANAGER:RegisterForEvent("LoreBooks_PinUpdate", EVENT_PLAYER_ACTIVATED,
				function(event)
					EVENT_MANAGER:UnregisterForEvent("LoreBooks_PinUpdate", event)
					CreatePins()
				end)
		end
	end
end

local function MapCallback_unknown()
	if not LMP:IsEnabled(PINS_UNKNOWN) or (GetMapType() > MAPTYPE_ZONE) then return end
	if GetMapContentType() == MAP_CONTENT_DUNGEON and IsUnitUsingVeteranDifficulty("player") then return end
	QueueCreatePins(PINS_UNKNOWN)
end

local function MapCallback_collected()
	if not LMP:IsEnabled(PINS_COLLECTED) or (GetMapType() > MAPTYPE_ZONE) then return end
	if GetMapContentType() == MAP_CONTENT_DUNGEON and IsUnitUsingVeteranDifficulty("player") then return end
	QueueCreatePins(PINS_COLLECTED)
end

local function MapCallback_eidetic()
	if not LMP:IsEnabled(PINS_EIDETIC) or (GetMapType() > MAPTYPE_ZONE) then return end
	QueueCreatePins(PINS_EIDETIC)
end

--[[
local function MapCallback_eideticCollected()
	if not LMP:IsEnabled(PINS_EIDETIC_COLLECTED) or (GetMapType() > MAPTYPE_ZONE) then return end
	QueueCreatePins(PINS_EIDETIC_COLLECTED)
end
]]
local function CompassCallback()
	if not savedVariables.filters[PINS_COMPASS] or (GetMapType() > MAPTYPE_ZONE) then return end
	if GetMapContentType() == MAP_CONTENT_DUNGEON and IsUnitUsingVeteranDifficulty("player") then return end
	QueueCreatePins(PINS_COMPASS)
end

local function CompassCallbackEidetic()
	if not savedVariables.filters[PINS_COMPASS_EIDETIC] or (GetMapType() > MAPTYPE_ZONE) then return end
	QueueCreatePins(PINS_COMPASS_EIDETIC)
end

-- Slash commands -------------------------------------------------------------
local function ShowMyPosition()
	if SetMapToPlayerLocation() == SET_MAP_RESULT_MAP_CHANGED then
		CALLBACK_MANAGER:FireCallbacks("OnWorldMapChanged")
	end

	local x, y = GetMapPlayerPosition("player")

	local locX = ("%05.02f"):format(zo_round(x*10000)/100)
	local locY = ("%05.02f"):format(zo_round(y*10000)/100)

	MyPrint(zo_strformat("<<1>>: <<2>>\195\151<<3>> (<<4>>)", GetMapName(), locX, locY, LMP:GetZoneAndSubzone(true)))
end

SLASH_COMMANDS["/mypos"] = ShowMyPosition
SLASH_COMMANDS["/myposition"] = ShowMyPosition
SLASH_COMMANDS["/myloc"] = ShowMyPosition
SLASH_COMMANDS["/mylocation"] = ShowMyPosition

local function Base62(value)
	local r = false
	local state = type( value )
	if state == "number" then
		local k = math.floor( value )
		if k == value and value > 0 then
			local m
			r = ""
			while k > 0 do
				m = k % 62
				k = ( k - m ) / 62
				if m >= 36 then
					m = m + 61
				elseif m >= 10 then
					m = m + 55
				else
					m = m + 48
				end
				r = string.char( m ) .. r
			end
		elseif value == 0 then
			r = "0"
		end
	elseif state == "string" then
		if value:match( "^%w+$" ) then
			local n = #value
			local k = 1
			local c
			r = 0
			for i = n, 1, -1 do
				c = value:byte( i, i )
				if c >= 48 and c <= 57 then
					c = c - 48
				elseif c >= 65 and c <= 90 then
					c = c - 55
				elseif c >= 97 and c <= 122 then
					c = c - 61
				else  -- How comes?
					r = nil
					break  -- for i
				end
				r = r + c * k
				k = k * 62
			end -- for i
		end
	end
	return r
end

-- Dirty trick
local function UnsignedBase62(value)
	local isNegative = value < 0
	local value62 = Base62(math.abs(value))
	if isNegative then return "-" .. value62 end
	return value62
end

local function ConfigureMail(data)

	if data then
		
		Postmail = data
		if (not (Postmail.subject and type(Postmail.subject) == "string" and string.len(Postmail.subject) > 0)) then
			return false
		end
		if (not (Postmail.recipient and type(Postmail.recipient) == "string" and string.len(Postmail.recipient) > 0)) then
			return false
		end
		if (not (Postmail.maxDelay and type(Postmail.maxDelay) == "number" and Postmail.maxDelay >= 0)) then
			return false
		end
		if (not (Postmail.mailMaxSize and type(Postmail.mailMaxSize) == "number" and Postmail.mailMaxSize >= 0 and Postmail.mailMaxSize <= 700)) then
			return false
		end
		
		Postmail.isConfigured = true
		return true
		
	end
	
	return false

end

local function EnableMail()
	if Postmail.isConfigured then
		Postmail.isActive = true
	end
end

local function DisableMail()
	if Postmail.isConfigured and Postmail.isActive then
		Postmail.isActive = false
	end
end

local function SendData(data)

	local function SendMailData(data)
		if Postmail.recipient ~= GetDisplayName() then -- Cannot send to myself
			RequestOpenMailbox()
			SendMail(Postmail.recipient, Postmail.subject, data)
			CloseMailbox()
		else -- Directly add to POSTMAIL_DEAMON
			POSTMAIL_DEAMON[GetDisplayName() .. GetTimeStamp()] = {body = data, sender = Postmail.recipient, received = GetDate()}
		end
	end
	
	local pendingData = savedVariables.postmailData
	if Postmail.recipient == GetDisplayName() then
		d("@Ayantir->Pushed " .. data .. " in POSTMAIL_DEAMON")
		SendMailData(data)
		savedVariables.postmailData = ""
	elseif Postmail.isActive then
		local dataLen = string.len(data)
		local now = GetTimeStamp()
		if pendingData ~= "" then
			if not string.find(pendingData, data) then
				local dataMergedLen = string.len(pendingData) + dataLen + 1 -- 1 is \n
				if now - savedVariables.postmailFirstInsert > Postmail.maxDelay then -- A send must be done
					if dataMergedLen > Postmail.mailMaxSize then
						SendMailData(pendingData) -- too big, send pendingData and save the modulo
						savedVariables.postmailData = data
						savedVariables.postmailFirstInsert = now
					else
						SendMailData(pendingData .. "\n" .. data) -- Send all data
						savedVariables.postmailData = ""
						savedVariables.postmailFirstInsert = now
					end
				else
					-- Send only if data is too big
					if dataMergedLen > Postmail.mailMaxSize then
						SendMailData(pendingData) -- too big, send pendingData and save the modulo
						savedVariables.postmailData = data
						savedVariables.postmailFirstInsert = now
					else
						savedVariables.postmailData = savedVariables.postmailData .. "\n" .. data
					end
				end
			end
		elseif dataLen < Postmail.mailMaxSize then
			savedVariables.postmailData = data
			savedVariables.postmailFirstInsert = now
		end
	end

end

local function BuildLorebooksLoreLibrary()

	for categoryIndex = 1, GetNumLoreCategories() do
		local categoryName, numCollections = GetLoreCategoryInfo(categoryIndex)
		for collectionIndex = 1, numCollections do
			local _, _, _, totalBooks, hidden = GetLoreCollectionInfo(categoryIndex, collectionIndex)
			if not hidden then
				for bookIndex = 1, totalBooks do
					local bookName, _, known = GetLoreBookInfo(categoryIndex, collectionIndex, bookIndex)
					if not loreBoooksLibrary[bookName] then
						loreBoooksLibrary[bookName] = {categoryIndex = categoryIndex, collectionIndex = collectionIndex, bookIndex = bookIndex}
					else
						loreBoooksLibrary[bookName].gotDuplicates = true
					end
					if known then
						totalCurrentlyCollected = totalCurrentlyCollected + 1
					end
				end
			end
		end
	end
	
end

-- Returns false if Book should not be datamined.
-- Not exact func as in PostmailDeamon cause some books still have unknown loot position and need to be datamined for check (Mainly books given by a quest)
local function EideticValidEntry(collectionIndex, bookIndex)
	
	-- Collections where Eidetic Memory mining is disabled
	
	if collectionIndex == 27 -- Dwemer
	or collectionIndex == 28 -- Akaviri
	or collectionIndex == 29 -- Glass
	or collectionIndex == 30 -- Mercenary
	or collectionIndex == 31 -- Xivkyn
	or collectionIndex == 32 -- Anc Orc
	or collectionIndex == 36 -- Outlaw
	or collectionIndex == 37 -- Trinimac
	or collectionIndex == 38 -- Malacath
	or collectionIndex == 39 -- Aldmeri
	or collectionIndex == 40 -- Daggerfall
	or collectionIndex == 41 -- Ebonheart
	or collectionIndex == 42 -- Ra Gada
	or collectionIndex == 43 -- Ebony
	or collectionIndex == 44 -- Dromathra
	or collectionIndex == 45 -- Assassin
	or collectionIndex == 46 -- Thieves Guild
	or collectionIndex == 47 -- Abah's Watch
	then
		return false
	elseif collectionIndex == 1 then -- Base Book styles
		if bookIndex == 3 -- Hight elves
		or bookIndex == 4 -- Dark Elves
		or bookIndex == 5 -- Wood elves
		or bookIndex == 6 -- Nords
		or bookIndex == 7 -- Bretons
		or bookIndex == 8 -- Redguards
		or bookIndex == 9 -- Khajiits
		or bookIndex == 10 -- Orcs
		or bookIndex == 11 -- Argonians
		or bookIndex == 12 -- Imperial
		or bookIndex == 13 -- Anc Elves
		or bookIndex == 14 -- Barbarians
		or bookIndex == 15 -- Primal
		or bookIndex == 16 -- Daedra
		or bookIndex == 23 -- Soul-Shriven
		then
			return false
		end
	end
	
	return true
	
end

local function CoordsNearby(locX, locY, x, y)

	local nearbyIs = 0.0005 --It should be 0.00028, but we add a large diff just in case of. 0.0004 is not enough.
	if math.abs(locX - x) < nearbyIs and math.abs(locY - y) < nearbyIs then
		return true
	end
	return false
	
end

-- This code will be removed at 100015, it's only as is to thank every contributor
local function ShowThankYou(categoryIndex, collectionIndex, bookIndex)

	local icons = {
		[1] = "/esoui/art/icons/scroll_001.dds",
		[10] = "/esoui/art/icons/lore_book2_detail1_color1.dds",
		[20] = "/esoui/art/icons/quest_book_001.dds",
		[30] = "/esoui/art/icons/scroll_005.dds",
	}

	if not savedVariables.booksFound[categoryIndex] then savedVariables.booksFound[categoryIndex] = {} end
	if not savedVariables.booksFound[categoryIndex][collectionIndex] then savedVariables.booksFound[categoryIndex][collectionIndex] = {} end
	if not savedVariables.booksFound[categoryIndex][collectionIndex][bookIndex] then
		savedVariables.booksFound[categoryIndex][collectionIndex][bookIndex] = true
		savedVariables.nbBooksFound = savedVariables.nbBooksFound + 1
		if savedVariables.nbBooksFound == 1 or savedVariables.nbBooksFound == 10 or savedVariables.nbBooksFound == 20 or savedVariables.nbBooksFound == 30 then
			CENTER_SCREEN_ANNOUNCE:AddMessage(0, CSA_EVENT_COMBINED_TEXT, SOUNDS.LEVEL_UP, GetString("LBOOKS_THANK_YOU", savedVariables.nbBooksFound), GetString("LBOOKS_THANK_YOU_LONG", savedVariables.nbBooksFound), icons[savedVariables.nbBooksFound], "EsoUI/Art/Achievements/achievements_iconBG.dds")
		end
	end

end

local function BuildDataToShare(bookName)
	
	if bookName and bookName ~= "" then
		
		local dataToShare
		
		local apiVersion
		if GetAPIVersion() == 100014 then
			apiVersion = 1
		elseif GetAPIVersion() == 100015 then
			apiVersion = 2
		else
			return
		end
		
		if SetMapToPlayerLocation() == SET_MAP_RESULT_MAP_CHANGED then
			CALLBACK_MANAGER:FireCallbacks("OnWorldMapChanged")
		end
		
		-- Will only returns the zone and not the subzone
		local zoneIndex = GetCurrentMapZoneIndex()
		
		-- Avoid nil conversion into base62
		if not zoneIndex then
			zoneIndex = 0
		end
		
		-- mapType of the subzone. Needed when we are elsewhere than zone or subzone.
		local mapContentType = GetMapContentType()
		
		local xGPS, yGPS, zoneGPS = GPS:LocalToGlobal(GetMapPlayerPosition("player"))
		
		if not zoneGPS then
			zoneGPS = 0
		end
		
		local locX = zo_round(xGPS*100000) -- 5 decimals because of Cyrodiil map
		local locY = zo_round(yGPS*100000)
		
		-- v1 = 2.4.0 = locX, locY, zoneIndex, mapType, lastInteractionActionWas
		-- v2 = 2.4.2 = locX, locY, zoneIndex, mapContentType, mapIndex, lastInteractionActionWas
		-- v3 = 2.4.4 = locX, locY, zoneIndex, mapContentType, mapIndex, lastInteractionActionWas, langCode
		-- v4 = 2.4.4 = locX, locY, zoneIndex, mapContentType, mapIndex, lastInteractionActionWas, langCode, apiVersion
		-- v5 = 2.5   = locX, locY, zoneIndex, mapContentType, mapIndex, lastInteractionActionWas, langCode, apiVersion, currentZoneDungeonDifficulty
		-- v6 = 2.5.1 = locX, locY, zoneIndex, mapContentType, mapIndex, lastInteractionActionWas, langCode, apiVersion, currentZoneDungeonDifficulty, reticleAway, associatedQuest
		dataToShare = zo_strformat("<<1>>;<<2>>;<<3>>;<<4>>;<<5>>", UnsignedBase62(locX), UnsignedBase62(locY), UnsignedBase62(zoneIndex), UnsignedBase62(mapContentType), UnsignedBase62(zoneGPS))
		
		-- if true, this is a bookshelve with a "random" book.
		if lastInteractionActionWas == GetString(LBOOKS_ACTION_SEARCH) then
			return
		else
			dataToShare = dataToShare ..";0"
		end
		
		local clang
		local lang = GetCVar("Language.2")
		if lang == "en" then clang = 1 end
		if lang == "fr" then clang = 2 end
		if lang == "de" then clang = 3 end
		dataToShare = dataToShare ..";" .. clang
		
		dataToShare = dataToShare .. ";" .. apiVersion
		
		-- DungeonDifficulty for VetDungeons
		local currentZoneDungeonDifficulty = GetCurrentZoneDungeonDifficulty()
		dataToShare = dataToShare .. ";" .. currentZoneDungeonDifficulty
		
		-- We record time of lastReticle. If timer is too long we consider that the books wasn't hown by a reticle but by another way (-> inventory)
		local reticleAway = 0
		local associatedQuest = 0
		if lastReticleUpdateWas + 1500 < GetFrameTimeMilliseconds() then -- 500 is not enought
			reticleAway = 1
			
			for questIndex, questData in pairs(SHARED_INVENTORY.questCache) do
				for itemIndex, itemData in pairs(questData) do
					if string.lower(itemData.name) == string.lower(bookName) then
						associatedQuest = GetJournalQuestInfo(questIndex)
						break
					end
				end
			end
			
		end
		
		dataToShare = dataToShare .. ";" .. reticleAway .. ";" .. associatedQuest 
		
		if loreBoooksLibrary[bookName] then
			
			local categoryIndex = loreBoooksLibrary[bookName].categoryIndex
			local collectionIndex = loreBoooksLibrary[bookName].collectionIndex
			local bookIndex = loreBoooksLibrary[bookName].bookIndex
			
			if EideticValidEntry(collectionIndex, bookIndex) then
				local bookData = LoreBooks_GetEideticData(categoryIndex, collectionIndex, bookIndex)
				
				if reticleAway == 0 or currentZoneDungeonDifficulty == DUNGEON_DIFFICULTY_NONE then
					if bookData and bookData.c and bookData.e then
						for _, data in ipairs(bookData.e) do
							if data.r == false and data.z == zoneIndex and CoordsNearby(xGPS, yGPS, data.x, data.y) then
								return
							end
						end
					end
				end
				
				local bookRef = zo_strformat("<<1>>;<<2>>;<<3>>", UnsignedBase62(categoryIndex), UnsignedBase62(collectionIndex), UnsignedBase62(bookIndex))
				dataToShare = dataToShare .. "@" .. bookRef
				
				if loreBoooksLibrary[bookName].gotDuplicates then
					dataToShare = dataToShare .. "@1" -- If book got duplicates, we add 1 at the end for a manual check
				end
				
				if not bookData and categoryIndex == 2 then
					ShowThankYou(categoryIndex, collectionIndex, bookIndex)
				end
				
			else
				return
			end
			
		else
			dataToShare = dataToShare .. "@0;" .. bookName
		end
		
		return dataToShare

	end
		
end
		
local function OnShowBook(_, bookName)
	local dataToShare = BuildDataToShare(bookName)
	if dataToShare then
		d("-> " .. dataToShare)
		SendData(dataToShare)
	end
end

local function OnBookLearnedExp(_, categoryIndex, collectionIndex, bookIndex, _, skillType, skillIndex, rank)
	local bookName = GetLoreBookInfo(categoryIndex, collectionIndex, bookIndex)
	local dataToShare = BuildDataToShare(bookName)
	if dataToShare then
		dataToShare = zo_strformat("<<1>@<<2>>;<<3>>;<<4>>", dataToShare, skillType, skillIndex, rank)
		SendData(dataToShare)
	end
end

--Reticle Hook
local function HookTryHandlingInteraction()
	local function GetInteractionInfo(interactionPossible)
		if interactionPossible then
			if savedVariables.shareData then
				local action = GetGameCameraInteractableActionInfo()
				if action then
					lastInteractionActionWas = action
					lastReticleUpdateWas = GetFrameTimeMilliseconds()
				end
			end
		end
	end
	ZO_PreHook(RETICLE, "TryHandlingInteraction", GetInteractionInfo)
end

local function ToggleShareData()

	local PostmailData = {
		subject = "LB_DATA", -- Subject of the mail
		recipient = "@Ayantir", -- Recipient of the mail. The recipient *IS GREATLY ENCOURAGED* to run the Postmail deamon
		maxDelay = 864000, -- 10d
		mailMaxSize = 600, -- Mail limitation is 700 Avoid > 650.
	}
	
	local lang = GetCVar("Language.2")
	if lang == "fr" or lang == "en" or lang == "de" then
		if savedVariables.shareData then
			HookTryHandlingInteraction()
			EVENT_MANAGER:RegisterForEvent("LoreBooks", EVENT_SHOW_BOOK, OnShowBook)
			EVENT_MANAGER:RegisterForEvent("LoreBooks", EVENT_LORE_BOOK_LEARNED_SKILL_EXPERIENCE, OnBookLearnedExp)
			local postmailIsConfigured = ConfigureMail(PostmailData)
			if postmailIsConfigured then
				EnableMail()
			end
		else
			EVENT_MANAGER:UnregisterForEvent("LoreBooks", EVENT_SHOW_BOOK)
			EVENT_MANAGER:UnregisterForEvent("LoreBooks", EVENT_LORE_BOOK_LEARNED_SKILL_EXPERIENCE)
			DisableMail()
		end
	end

end

local function ShowReport(textArray)
	local text = table.concat(textArray, "\n")
	LoreBooks_GUIText:SetText(text)
	LoreBooks_GUI:SetHidden(false)
	SetGameCameraUIMode(true) --release mouse
end

local function BuildReport(data)
	local dataTable = data
	local list = {}

	for zone, zoneData in pairs(dataTable) do
		for subzone, subzoneData in pairs(zoneData) do
			table.insert(list, zone .. "/" .. subzone)
			for _, pinTag in ipairs(subzoneData) do
				local bookname = GetLoreBookInfo(1, pinTag[3], pinTag[4])
				local tag = "{ "
				for i = 1, 3 do tag = tag .. tostring(pinTag[i]) .. ", " end
				tag = tag .. tostring(pinTag[4])
				if (pinTag[6]) then
					tag = tag .. ", " .. tostring(pinTag[6])
				end
				tag = tag .. " }, --" .. bookname
				table.insert(list, tag)
			end
		end
		table.insert(list, "")
	end

	return list
end
SLASH_COMMANDS["/lbreport"] = function() ShowReport(BuildReport(newBooks)) end
SLASH_COMMANDS["/lbcollected"] = function() ShowReport(BuildReport(svData.lorebooks)) end

local function LoreBooksCheck()
	local bookCheck = {}
	local unknownBooks = {}

	--collect unknown books
	local categoryId = 1 --Shalidor's Library
	local _, numCollections = GetLoreCategoryInfo(categoryId)
	for collectionId = 1, numCollections do
		local _, _, numKnownBooks, totalBooks, hidden = GetLoreCollectionInfo(categoryId, collectionId)
		if not hidden and numKnownBooks ~= totalBooks then
			unknownBooks[collectionId] = {}
			for bookId = 1, totalBooks do
				local title, icon, known = GetLoreBookInfo(categoryId, collectionId, bookId)
				if not known then
					unknownBooks[collectionId][bookId] = {}
				end
			end
		end
	end

	--fill locations for unknown books
	local lorebooksData = LoreBooks_GetAllData()
	for zone, zoneData in pairs(lorebooksData) do
		for subzone, subzoneData in pairs(zoneData) do
			for i, pin in ipairs(subzoneData) do
				if unknownBooks[pin[3]] ~= nil and unknownBooks[pin[3]][pin[4]] ~= nil then
					local locationInfo = zo_strformat("<<1>>/<<2>>: <<3>>,<<4>>", zone, subzone, ("%03d"):format(pin[1]*1000), ("%03d"):format(pin[2]*1000))
					table.insert(unknownBooks[pin[3]][pin[4]], locationInfo)
				end
			end
		end
	end

	--build report
	for collectionId, books in pairs(unknownBooks) do
		local collectionName = GetLoreCollectionInfo(categoryId, collectionId)
		table.insert(bookCheck, zo_strformat("|cFFA500<<1>>|r", collectionName))

		for bookId, locations in pairs(books) do
			local title, icon = GetLoreBookInfo(categoryId, collectionId, bookId)
			table.insert(bookCheck, zo_strformat("|cFFFF00<<1>>|r", title))

			if locations == {} then
				table.insert(bookCheck, "no known locations")
			else
				for i, locationInfo in ipairs(locations) do
					table.insert(bookCheck, locationInfo)
				end
			end
		end
		table.insert(bookCheck, "")
	end

	return bookCheck
end
SLASH_COMMANDS["/lbcheck"] = function() ShowReport(LoreBooksCheck()) end

-- Settings menu --------------------------------------------------------------
local function CreateSettingsMenu()
	local panelData = {
		type = "panel",
		name = GetString(LBOOKS_TITLE),
		displayName = ZO_HIGHLIGHT_TEXT:Colorize(GetString(LBOOKS_TITLE)),
		author = "Garkin & Ayantir",
		version = ADDON_VERSION,
		slashCommand = "/lorebooks",
		registerForRefresh = true,
		registerForDefaults = true,
	}
	LAM:RegisterAddonPanel("LoreBooks", panelData)

	local CreateIcons, unknownIcon, collectedIcon
	CreateIcons = function(panel)
		if panel == LoreBooks then
			unknownIcon = WINDOW_MANAGER:CreateControl(nil, panel.controlsToRefresh[1], CT_TEXTURE)
			unknownIcon:SetAnchor(RIGHT, panel.controlsToRefresh[1].combobox, LEFT, -10, 0)
			unknownIcon:SetTexture(pinTextures[savedVariables.pinTexture.type][2])
			unknownIcon:SetDimensions(savedVariables.pinTexture.size, savedVariables.pinTexture.size)
			collectedIcon = WINDOW_MANAGER:CreateControl(nil, panel.controlsToRefresh[1], CT_TEXTURE)
			collectedIcon:SetAnchor(RIGHT, unknownIcon, LEFT, -5, 0)
			collectedIcon:SetTexture(pinTextures[savedVariables.pinTexture.type][1])
			collectedIcon:SetDimensions(savedVariables.pinTexture.size, savedVariables.pinTexture.size)
			collectedIcon:SetDesaturation((pinTexturesList[savedVariables.pinTexture.type] == "Shalidor's Library icons") and 1 or 0)
			CALLBACK_MANAGER:UnregisterCallback("LAM-PanelControlsCreated", CreateIcons)
		end
	end
	CALLBACK_MANAGER:RegisterCallback("LAM-PanelControlsCreated", CreateIcons)

	local optionsTable = {
		{
			type = "dropdown",
			name = GetString(LBOOKS_PIN_TEXTURE),
			tooltip = GetString(LBOOKS_PIN_TEXTURE_DESC),
			choices = pinTexturesList,
			getFunc = function() return pinTexturesList[savedVariables.pinTexture.type] end,
			setFunc = function(selected)
					for index, name in ipairs(pinTexturesList) do
						if name == selected then
							savedVariables.pinTexture.type = index
							unknownIcon:SetTexture(pinTextures[index][2])
							collectedIcon:SetDesaturation((pinTexturesList[index] == "Shalidor's Library icons") and 1 or 0)
							collectedIcon:SetTexture(pinTextures[index][1])
							LMP:RefreshPins(PINS_UNKNOWN)
							LMP:RefreshPins(PINS_COLLECTED)
							LMP:RefreshPins(PINS_EIDETIC)
							COMPASS_PINS.pinLayouts[PINS_COMPASS].texture = pinTextures[index][2]
							COMPASS_PINS:RefreshPins(PINS_COMPASS)
							COMPASS_PINS.pinLayouts[PINS_COMPASS_EIDETIC].texture = pinTextures[index][2]
							COMPASS_PINS:RefreshPins(PINS_COMPASS_EIDETIC)
							break
						end
					end
				end,
			disabled = function() return not (savedVariables.filters[PINS_UNKNOWN] or savedVariables.filters[PINS_COLLECTED] or savedVariables.filters[PINS_EIDETIC]) end,
			default = pinTexturesList[defaults.pinTexture.type],
		},
		{
			type = "slider",
			name = GetString(LBOOKS_PIN_SIZE),
			tooltip = GetString(LBOOKS_PIN_SIZE_DESC),
			min = 20,
			max = 70,
			step = 1,
			getFunc = function() return savedVariables.pinTexture.size end,
			setFunc = function(size)
					savedVariables.pinTexture.size = size
					unknownIcon:SetDimensions(size, size)
					collectedIcon:SetDimensions(size, size)
					LMP:SetLayoutKey(PINS_UNKNOWN, "size", size)
					LMP:SetLayoutKey(PINS_COLLECTED, "size", size)
					LMP:SetLayoutKey(PINS_EIDETIC, "size", size)
					LMP:RefreshPins(PINS_UNKNOWN)
					LMP:RefreshPins(PINS_COLLECTED)
					LMP:RefreshPins(PINS_EIDETIC)
				end,
			disabled = function() return not (savedVariables.filters[PINS_UNKNOWN] or savedVariables.filters[PINS_COLLECTED] or savedVariables.filters[PINS_EIDETIC]) end,
			default = defaults.pinTexture.size
		},
		{
			type = "slider",
			name = GetString(LBOOKS_PIN_LAYER),
			tooltip = GetString(LBOOKS_PIN_LAYER_DESC),
			min = 10,
			max = 200,
			step = 5,
			getFunc = function() return savedVariables.pinTexture.level end,
			setFunc = function(level)
					savedVariables.pinTexture.level = level
					LMP:SetLayoutKey(PINS_UNKNOWN, "level", level)
					LMP:SetLayoutKey(PINS_COLLECTED, "level", level)
					LMP:SetLayoutKey(PINS_EIDETIC, "level", level)
					LMP:RefreshPins(PINS_UNKNOWN)
					LMP:RefreshPins(PINS_COLLECTED)
					LMP:RefreshPins(PINS_EIDETIC)
				end,
			disabled = function() return not (savedVariables.filters[PINS_UNKNOWN] or savedVariables.filters[PINS_COLLECTED] or savedVariables.filters[PINS_EIDETIC]) end,
			default = defaults.pinTexture.level,
		},
		{
			type = "checkbox",
			name = GetString(LBOOKS_UNKNOWN),
			tooltip = GetString(LBOOKS_UNKNOWN_DESC),
			getFunc = function() return savedVariables.filters[PINS_UNKNOWN] end,
			setFunc = function(state)
					savedVariables.filters[PINS_UNKNOWN] = state
					LMP:SetEnabled(PINS_UNKNOWN, state)
				end,
			default = defaults.filters[PINS_UNKNOWN],
		},
		{
			type = "checkbox",
			name = GetString(LBOOKS_COLLECTED),
			tooltip = GetString(LBOOKS_COLLECTED_DESC),
			getFunc = function() return savedVariables.filters[PINS_COLLECTED] end,
			setFunc = function(state)
					savedVariables.filters[PINS_COLLECTED] = state
					LMP:SetEnabled(PINS_COLLECTED, state)
				end,
			default = defaults.filters[PINS_COLLECTED]
		},
		{
			type = "checkbox",
			name = GetString(LBOOKS_EIDETIC),
			tooltip = GetString(LBOOKS_EIDETIC_DESC),
			getFunc = function() return savedVariables.filters[PINS_EIDETIC] end,
			setFunc = function(state)
					savedVariables.filters[PINS_EIDETIC] = state
					LMP:SetEnabled(PINS_EIDETIC, state)
				end,
			default = defaults.filters[PINS_EIDETIC]
		},
		{
			type = "checkbox",
			name = GetString(LBOOKS_COMPASS_UNKNOWN),
			tooltip = GetString(LBOOKS_COMPASS_UNKNOWN_DESC),
			getFunc = function() return savedVariables.filters[PINS_COMPASS] end,
			setFunc = function(state)
					savedVariables.filters[PINS_COMPASS] = state
					COMPASS_PINS:RefreshPins(PINS_COMPASS)
				end,
			default = defaults.filters[PINS_COMPASS],
		},
		{
			type = "checkbox",
			name = GetString(LBOOKS_COMPASS_EIDETIC),
			tooltip = GetString(LBOOKS_COMPASS_EIDETIC_DESC),
			getFunc = function() return savedVariables.filters[PINS_COMPASS_EIDETIC] end,
			setFunc = function(state)
					savedVariables.filters[PINS_COMPASS_EIDETIC] = state
					COMPASS_PINS:RefreshPins(PINS_COMPASS_EIDETIC)
				end,
			default = defaults.filters[PINS_COMPASS_EIDETIC],
		},
		{
			type = "slider",
			name = GetString(LBOOKS_COMPASS_DIST),
			tooltip = GetString(LBOOKS_COMPASS_DIST_DESC),
			min = 1,
			max = 100,
			step = 1,
			getFunc = function() return savedVariables.compassMaxDistance * 1000 end,
			setFunc = function(maxDistance)
					savedVariables.compassMaxDistance = maxDistance / 1000
					COMPASS_PINS.pinLayouts[PINS_COMPASS].maxDistance = maxDistance / 1000
					COMPASS_PINS:RefreshPins(PINS_COMPASS)
					COMPASS_PINS.pinLayouts[PINS_COMPASS_EIDETIC].maxDistance = maxDistance / 1000
					COMPASS_PINS:RefreshPins(PINS_COMPASS_EIDETIC)
				end,
			disabled = function() return not (savedVariables.filters[PINS_COMPASS] or savedVariables.filters[PINS_COMPASS_EIDETIC]) end,
			default = defaults.compassMaxDistance * 1000,
		},
		{
			type = "checkbox",
			name = GetString(LBOOKS_SHARE_DATA),
			tooltip = GetString(LBOOKS_SHARE_DATA_DESC),
			getFunc = function() return savedVariables.shareData end,
			setFunc = function(state)
				savedVariables.shareData = state
				ToggleShareData()
				end,
			default = defaults.shareData,
			disabled = (GetWorldName() ~= "EU Megaserver" or not (GetCVar("Language.2") == "fr" or GetCVar("Language.2") == "en" or GetCVar("Language.2") == "de"))
		},
	}
	LAM:RegisterOptionControls("LoreBooks", optionsTable)
end

local function ShowCongrats(newBook)
	
	local congrats = {
		[2000] = "/esoui/art/icons/quest_book_003.dds",
		[2500] = "/esoui/art/icons/scroll_001.dds",
		[2600] = "/esoui/art/icons/scroll_005.dds",
		[2700] = "/esoui/art/icons/quest_book_001.dds",
		[2800] = "/esoui/art/icons/lore_book2_detail1_color1.dds",
		[2821] = "/esoui/art/icons/ability_mage_064.dds", -- Max TG
		[2900] = "/esoui/art/icons/achievement_thievesguild_024.dds",
		[3000] = "/esoui/art/icons/achievement_wrothgar_027.dds",
		[3010] = "/esoui/art/icons/ability_mage_064.dds", -- Max DB
	}
	
	local function CongratsStuff(collected)
		if not savedVariables.booksCollected[collected] then
			savedVariables.booksCollected[collected] = true
			CENTER_SCREEN_ANNOUNCE:AddMessage(0, CSA_EVENT_COMBINED_TEXT, SOUNDS.LEVEL_UP, GetString("LBOOKS_THANK_YOU", collected), GetString("LBOOKS_THANK_YOU_LONG", collected), congrats[collected], "EsoUI/Art/Achievements/achievements_iconBG.dds")
		end
	end
	
	if newBook then
		
		if congrats[totalCurrentlyCollected] then
			if (totalCurrentlyCollected == 2821 and GetAPIVersion() == 100014) then
				CongratsStuff(totalCurrentlyCollected)
			elseif (totalCurrentlyCollected == 3010 and GetAPIVersion() == 100015) then
				CongratsStuff(totalCurrentlyCollected)
			elseif totalCurrentlyCollected ~= 2821 and totalCurrentlyCollected ~= 3010 then
				CongratsStuff(totalCurrentlyCollected)
			end
		end
		
	else
	
		-- If first activation
		EVENT_MANAGER:UnregisterForEvent("LoreBooks", EVENT_PLAYER_ACTIVATED)
		
		-- It won't show all achiev in a row, I prefer like this.
		if totalCurrentlyCollected == 3010 and GetAPIVersion() == 100015 then
			CongratsStuff(3010)
		elseif totalCurrentlyCollected == 2821 and GetAPIVersion() == 100014 then
			CongratsStuff(2821)
		elseif totalCurrentlyCollected >= 3000 then
			CongratsStuff(3000)
		elseif totalCurrentlyCollected >= 2900 then
			CongratsStuff(2900)
		elseif totalCurrentlyCollected >= 2800 then
			CongratsStuff(2800)
		elseif totalCurrentlyCollected >= 2700 then
			CongratsStuff(2700)
		elseif totalCurrentlyCollected >= 2600 then
			CongratsStuff(2600)
		elseif totalCurrentlyCollected >= 2500 then
			CongratsStuff(2500)
		elseif totalCurrentlyCollected >= 2000 then
			CongratsStuff(2000)
		end
		
	end
	
end

-- Event handlers -------------------------------------------------------------
local function HandleBookLearned(x, y, categoryIndex, collectionIndex, bookIndex)
	
	-- This is now done by Eidetic parser which triggers at every book read. But keep this code here in case of.
	if categoryIndex == 1 then
		--get map information
		local zone, subzone = LMP:GetZoneAndSubzone()
		local lorebooks = LoreBooks_GetLocalData(zone, subzone)
		
		--build pin tag
		local locX = zo_round(x*1000) / 1000
		local locY = zo_round(y*1000) / 1000

		local pinData = {locX, locY, collectionIndex, bookIndex}

		--if location is not known, save data to newBooks and lorebooksData
		--(will be stored just till logout/reload UI)
		local epsilon = GetEpsilon(zone, subzone)
		if UnknownCoords(lorebooks, x, y, epsilon) then
			SetLocalData(newBooks, zone, subzone, pinData)
			LoreBooks_SetLocalData(zone, subzone, pinData)
		end
		--save to saved variables for later use
		if UnknownCoords(svData.lorebooks, x, y, epsilon) then
			SetLocalData(svData.lorebooks, zone, subzone, pinData)
		end
	
	end
	
	-- LORE_LIBRARY need to be refreshed first
	zo_callLater(function() ShowCongrats(true) end, 50)
	
	LMP:RefreshPins(PINS_UNKNOWN)
	LMP:RefreshPins(PINS_COLLECTED)
	LMP:RefreshPins(PINS_EIDETIC)
	COMPASS_PINS:RefreshPins(PINS_COMPASS)
	COMPASS_PINS:RefreshPins(PINS_COMPASS_EIDETIC)
	
end

local function OnBookLearned(eventCode, categoryIndex, collectionIndex, bookIndex)
	
	totalCurrentlyCollected = totalCurrentlyCollected + 1
	
	--Refresh map if needed and get player position
	if SetMapToPlayerLocation() == SET_MAP_RESULT_MAP_CHANGED then
		CALLBACK_MANAGER:FireCallbacks("OnWorldMapChanged")
	end
	local x, y = GetMapPlayerPosition("player")

	if not InvalidPoint(x, y) then -- can be false in some very rare place (mainly fighters/mages/main questline dungeons).
		HandleBookLearned(x, y, categoryIndex, collectionIndex, bookIndex)
	end
	
end

local function OnGamepadPreferredModeChanged()
	if IsInGamepadPreferredMode() then
		INFORMATION_TOOLTIP = ZO_MapLocationTooltip_Gamepad
	else
		INFORMATION_TOOLTIP = InformationTooltip
	end
end

--updates lorebooksData from saved variables, if there is unknown book,
-- it will be added to newBooks table that is used for /lbreport
local function UpdateLorebooksData()
	for zone, zoneData in pairs(svData.lorebooks) do
		if zoneData ~= nil then
			for subzone, subzoneData in pairs(zoneData) do
				if subzoneData ~= nil then
					local epsilon = GetEpsilon(zone, subzone)
					for _, pinData in ipairs(subzoneData) do
						if not InvalidPoint(pinData[1], pinData[2]) then
							local knownBooks = LoreBooks_GetLocalData(zone, subzone)
							if UnknownCoords(knownBooks, pinData[1], pinData[2], epsilon) then
								SetLocalData(newBooks, zone, subzone, pinData)
								LoreBooks_SetLocalData(zone, subzone, pinData)
							end
						end
					end
				end
			end
		end
	end
end

local function OnSearchTextChanged(self)
	
	ZO_EditDefaultText_OnTextChanged(self)
	local search = self:GetText()
	LORE_LIBRARY.search = search
	
	if string.len(search) > 2 or string.len(search) == 0 then
		LORE_LIBRARY.navigationTree:ClearSelectedNode()
		LORE_LIBRARY:BuildCategoryList()
	end
	
end

local function NameSorter(left, right)
	return left.name < right.name
end

local function IsFoundInLoreLibrary(search, data)

	if string.find(string.lower(data.name), search) then
		return true
	else
		
		for bookIndex = 1, data.totalBooks do
			local title = GetLoreBookInfo(data.categoryIndex, data.collectionIndex, bookIndex)
			if string.find(string.lower(title), search) then
				return true
			end
		end
		
	end
	
	return false

end

local function BuildCategoryList(self)
	if self.control:IsControlHidden() then
		self.dirty = true
		return
	end

	self.totalCurrentlyCollected = 0
	self.totalPossibleCollected = 0

	self.navigationTree:Reset()

	local categories = {}
	for categoryIndex = 1, GetNumLoreCategories() do
		local categoryName, numCollections = GetLoreCategoryInfo(categoryIndex)
		for collectionIndex = 1, numCollections do
			local _, _, _, _, hidden = GetLoreCollectionInfo(categoryIndex, collectionIndex)
			if not hidden then
				categories[#categories + 1] = { categoryIndex = categoryIndex, name = categoryName, numCollections = numCollections }
				break
			end
		end
	end

	table.sort(categories, NameSorter)

	for i, categoryData in ipairs(categories) do
		local parent = self.navigationTree:AddNode("ZO_LabelHeader", categoryData, nil, SOUNDS.LORE_BLADE_SELECTED)

		local categoryIndex = categoryData.categoryIndex
		local numCollections = categoryData.numCollections

		local collections = {}

		for collectionIndex = 1, numCollections do
			local collectionName, description, numKnownBooks, totalBooks, hidden = GetLoreCollectionInfo(categoryIndex, collectionIndex)
			if not hidden then
				collections[#collections + 1] = { categoryIndex = categoryIndex, collectionIndex = collectionIndex, name = collectionName, description = description, numKnownBooks = numKnownBooks, totalBooks = totalBooks }

				self.totalCurrentlyCollected = self.totalCurrentlyCollected + numKnownBooks
				self.totalPossibleCollected = self.totalPossibleCollected + totalBooks
			end
		end

		table.sort(collections, NameSorter)
		
		local search = LORE_LIBRARY.search
		for k, collectionData in ipairs(collections) do
			if search and string.len(search) > 2 then
				if IsFoundInLoreLibrary(string.lower(search), collectionData) then
					self.navigationTree:AddNode("ZO_LoreLibraryNavigationEntry", collectionData, parent, SOUNDS.LORE_ITEM_SELECTED)
				end
			else
				self.navigationTree:AddNode("ZO_LoreLibraryNavigationEntry", collectionData, parent, SOUNDS.LORE_ITEM_SELECTED)
			end
		end
		
	end

	self.navigationTree:Commit()
	self:RefreshCollectedInfo()
	
	--Dirty hack to unselect all nodes and select the 1st one.
	
	if self.navigationTree.rootNode.children then
		if self.navigationTree.rootNode.children[1].children then
			self.navigationTree:SelectNode(self.navigationTree.rootNode.children[1].children[1])
		elseif self.navigationTree.rootNode.children[2].children then
			self.navigationTree:SelectNode(self.navigationTree.rootNode.children[2].children[1])
		end
	end
	
	KEYBIND_STRIP:UpdateKeybindButtonGroup(self.keybindStripDescriptor)

	self.dirty = false
	
	return true
	
end

-- "Right clic"
local function OnRowMouseUp(control, button)
	if button == MOUSE_BUTTON_INDEX_RIGHT then
		ClearMenu()
		
		--[[
		This code is meant to prevent scrolling while a right-click is in progress. Unfortunally, impossible to access to "self" because of locales
		SetMenuHiddenCallback(function() self:UnlockSelection() end)
		self:LockSelection()
		--]]
		
		if control.known then
			AddMenuItem(GetString(SI_LORE_LIBRARY_READ), function() ZO_LoreLibrary_ReadBook(control.categoryIndex, control.collectionIndex, control.bookIndex) end)
		end
		
		if IsChatSystemAvailableForCurrentPlatform() then
			AddMenuItem(GetString(SI_ITEM_ACTION_LINK_TO_CHAT), function()
				local link = ZO_LinkHandler_CreateChatLink(GetLoreBookLink, control.categoryIndex, control.collectionIndex, control.bookIndex)
				ZO_LinkHandler_InsertLink(link) 
			end)
		end
		
		if control.categoryIndex == 1 then
			local lorebookInfoOnBook = LoreBooks_GetDataOfBook(control.categoryIndex, control.collectionIndex, control.bookIndex)
			for resultEntry, resultData in ipairs(lorebookInfoOnBook) do
				
				local mapIndex = LoreBooks_GetMapIndexFromMapTile(resultData.zoneName, resultData.subZoneName)
				
				if mapIndex then
					AddMenuItem(zo_strformat("<<1>> : <<2>>x<<3>>", zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetMapNameByIndex(mapIndex)), (resultData.locX * 100), (resultData.locY * 100)),
					function()
						
						ZO_WorldMap_SetMapByIndex(mapIndex)
						PingMap(MAP_PIN_TYPE_RALLY_POINT, MAP_TYPE_LOCATION_CENTERED, resultData.locX, resultData.locY)
						PingMap(MAP_PIN_TYPE_PLAYER_WAYPOINT, MAP_TYPE_LOCATION_CENTERED, resultData.locX, resultData.locY)
						
						if(not ZO_WorldMap_IsWorldMapShowing()) then
							if IsInGamepadPreferredMode() then
								SCENE_MANAGER:Push("gamepad_worldMap")
							else
								MAIN_MENU_KEYBOARD:ShowCategory(MENU_CATEGORY_MAP)
								mapAvailable = false
							end
							mapAvailable = false
							zo_callLater(function() GPS:PanToMapPosition(resultData.locX, resultData.locY) end, 1000)
						end
						
					end)
				end
				
			end
		elseif control.categoryIndex == 2 and EideticValidEntry(control.collectionIndex, control.bookIndex) then
			
			local bookData = LoreBooks_GetEideticData(control.categoryIndex, control.collectionIndex, control.bookIndex)
			
			if bookData and bookData.c and bookData.e then
				
				if not bookData.q then
					for index, data in ipairs(bookData.e) do
					
						if not data.l and data.zx and data.zy then
						
							local xTooltip = ("%0.02f"):format(zo_round(data.zx*10000)/100)
							local yTooltip = ("%0.02f"):format(zo_round(data.zy*10000)/100)
							AddMenuItem(zo_strformat("<<1>> (<<2>>x<<3>>)", zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetMapNameByIndex(data.m)), xTooltip, yTooltip),
							function()
								
								ZO_WorldMap_SetMapByIndex(data.m)
								
								PingMap(MAP_PIN_TYPE_RALLY_POINT, MAP_TYPE_LOCATION_CENTERED, data.zx, data.zy)
								PingMap(MAP_PIN_TYPE_PLAYER_WAYPOINT, MAP_TYPE_LOCATION_CENTERED, data.zx, data.zy)
								
								if(not ZO_WorldMap_IsWorldMapShowing()) then
									if IsInGamepadPreferredMode() then
										SCENE_MANAGER:Push("gamepad_worldMap")
									else
										MAIN_MENU_KEYBOARD:ShowCategory(MENU_CATEGORY_MAP)
									end
									mapIsShowing = true
									zo_callLater(function() mapIsShowing = false end, 500) -- Bit dirty but ZO_WorldMap_IsWorldMapShowing() isn't fast enought
									zo_callLater(function() GPS:PanToMapPosition(data.zx, data.zy) end, 1000)
								end
								
							end)
							
						end
					end
				end
			end
			
		end
		
		ShowMenu(control)
		
	end
end

-- Mouse "hover"
local function OnMouseEnter(self, categoryIndex, collectionIndex, bookIndex)

	local STD_ZONE = 0

	-- No 1 for now.
	if categoryIndex == 2 and EideticValidEntry(collectionIndex, bookIndex) and (not mapIsShowing) then
	
		local bookData = LoreBooks_GetEideticData(categoryIndex, collectionIndex, bookIndex)

		if bookData and bookData.c then
			
			local bookName = GetLoreBookInfo(categoryIndex, collectionIndex, bookIndex) -- Could be retrieved automatically
			InitializeTooltip(InformationTooltip, self, BOTTOMLEFT, 0, 0, TOPRIGHT)
			InformationTooltip:AddLine(bookName, "ZoFontGameOutline", ZO_SELECTED_TEXT:UnpackRGB())
			ZO_Tooltip_AddDivider(InformationTooltip)
			
			local addDivider
			local entryWeight = {}
			
			if bookData.q then
				InformationTooltip:AddLine(GetString(LBOOKS_QUEST_BOOK), "", ZO_HIGHLIGHT_TEXT:UnpackRGB())
				InformationTooltip:AddLine(zo_strformat("[<<1>>]", bookData.q[GetCVar("Language.2")] or bookData.q["en"]), "", ZO_SELECTED_TEXT:UnpackRGB())
				InformationTooltip:AddLine(zo_strformat(GetString(LBOOKS_QUEST_IN_ZONE), zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetMapNameByIndex(bookData.qm))))
			elseif bookData.r then
				
				InformationTooltip:AddLine(GetString(LBOOKS_RANDOM_POSITION), "", ZO_HIGHLIGHT_TEXT:UnpackRGB())
				ZO_Tooltip_AddDivider(InformationTooltip)
				for mapIndex, count in pairs(bookData.m) do
					InformationTooltip:AddLine(zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetMapNameByIndex(mapIndex)), "", ZO_SELECTED_TEXT:UnpackRGB())
				end
				
			else
				for index, data in ipairs(bookData.e) do
					
					local insert = true
					local x = data.x
					local y = data.y
					local mapIndex = data.m
					local zoneIndex = data.z
					local isRandom = data.r
					local inDungeon = data.d
					local isLost = data.l
					local specialZone = data.s
					local linkedToUnknownQuest = data.q
					
					local weight = 0
					if isRandom then
						weight = weight + 1
					end
					if inDungeon then
						weight = weight + 2
					end
					
					local zoneName = zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetZoneNameByIndex(zoneIndex))
					local mapName = zo_strformat(SI_WINDOW_TITLE_WORLD_MAP, GetMapNameByIndex(mapIndex))
					
					local bookPosition
					if zoneName ~= mapName then
						bookPosition = zo_strformat("<<1>> - <<2>>", mapName, zoneName)
						if entryWeight[bookPosition] and entryWeight[bookPosition][weight] then
							insert = false
						end
					else
						bookPosition = mapName
						if entryWeight[bookPosition] and entryWeight[bookPosition][weight] then
							insert = false
						end
					end
					
					if not entryWeight[bookPosition] then entryWeight[bookPosition] = {} end
					entryWeight[bookPosition][weight] = true
					
					if insert then
						if addDivider then
							ZO_Tooltip_AddDivider(InformationTooltip)
						end
						addDivider = true
						
						InformationTooltip:AddLine(bookPosition, "", ZO_HIGHLIGHT_TEXT:UnpackRGB())
						
						if inDungeon then
							InformationTooltip:AddLine(zo_strformat("[<<1>>]", GetString(SI_QUESTTYPE5)), "", ZO_SELECTED_TEXT:UnpackRGB())
						end
						
						if specialZone ~= STD_ZONE then
							InformationTooltip:AddLine(zo_strformat("[<<1>>]", GetString("LBOOKS_SPECIAL_ZONE", specialZone)), "", ZO_SELECTED_TEXT:UnpackRGB())
						elseif isLost then
							InformationTooltip:AddLine(zo_strformat("[<<1>>]", GetString(LBOOKS_LOST)), "", ZO_SELECTED_TEXT:UnpackRGB())
						end
							
					end
					
				end
			end
		end
		
	end
	
	self.owner:EnterRow(self)

end

local function OnMouseExit(self)
	ClearTooltip(InformationTooltip)
	self.owner:ExitRow(self)
end

local function BuildBookListPostHook()
	for controlIndex, controlObject in ipairs(LORE_LIBRARY.list.list.activeControls) do
		controlObject:SetHandler("OnMouseUp", OnRowMouseUp)
		controlObject:SetHandler("OnMouseEnter", function(self) OnMouseEnter(self, controlObject.categoryIndex, controlObject.collectionIndex, controlObject.bookIndex) end)
		controlObject:SetHandler("OnMouseExit", function(self) OnMouseExit(self) end)
	end
end

local function CreateResearch()
	
	local lorebookResearch = WINDOW_MANAGER:CreateControlFromVirtual("Lorebook_Research", ZO_LoreLibrary, "Lorebook_Research_Template")
	lorebookResearch.searchBox = GetControl(lorebookResearch, "Box")
	lorebookResearch.searchBox:SetHandler("OnTextChanged", OnSearchTextChanged)
	
	ZO_PreHook(LORE_LIBRARY, "BuildCategoryList", BuildCategoryList)
	
	local origLoreLibraryBuildBookList = LORE_LIBRARY.BuildBookList
	LORE_LIBRARY.BuildBookList = function(self, ...)
		origLoreLibraryBuildBookList(self, ...)
		BuildBookListPostHook()
	end
	
end

local function OnLoad(eventCode, name)

	if name == "LoreBooks" then
		EVENT_MANAGER:UnregisterForEvent("LoreBooks", EVENT_ADD_ON_LOADED)

		--saved variables for settings
		savedVariables = ZO_SavedVars:New("LBooks_SavedVariables", 2, nil, defaults)

		local pinTextureLevel = savedVariables.pinTexture.level
		local pinTextureSize = savedVariables.pinTexture.size
		local compassMaxDistance = savedVariables.compassMaxDistance

		local mapPinLayout_eidetic = { level = pinTextureLevel, texture = GetPinTextureEidetic, size = pinTextureSize }
		local mapPinLayout_eideticCollected = { level = pinTextureLevel, texture = GetPinTextureEidetic, size = pinTextureSize }
		local mapPinLayout_unknown = { level = pinTextureLevel, texture = GetPinTexture, size = pinTextureSize }
		local mapPinLayout_collected = { level = pinTextureLevel, texture = GetPinTexture, size = pinTextureSize, grayscale = IsPinGrayscale }
		local compassPinLayout = { maxDistance = compassMaxDistance, texture = pinTextures[savedVariables.pinTexture.type][2],
			sizeCallback = function(pin, angle, normalizedAngle, normalizedDistance)
				if zo_abs(normalizedAngle) > 0.25 then
					pin:SetDimensions(54 - 24 * zo_abs(normalizedAngle), 54 - 24 * zo_abs(normalizedAngle))
				else
					pin:SetDimensions(48, 48)
				end
			end,
			additionalLayout = {
				function(pin, angle, normalizedAngle, normalizedDistance)
					if (pinTexturesList[savedVariables.pinTexture.type] == "Shalidor's Library icons") then --replace icon with icon from LoreLibrary
						local _, texture = GetLoreBookInfo(1, pin.pinTag[3], pin.pinTag[4])
						local icon = pin:GetNamedChild("Background")
						icon:SetTexture(texture)
					end
				end,
				function(pin)
					--I do not need to reset anything (texture is changed automatically), so the function is empty
				end
			}
		}
		local compassPinLayoutEidetic = { maxDistance = compassMaxDistance, texture = pinTextures[savedVariables.pinTexture.type][2],
			sizeCallback = function(pin, angle, normalizedAngle, normalizedDistance)
				if zo_abs(normalizedAngle) > 0.25 then
					pin:SetDimensions(54 - 24 * zo_abs(normalizedAngle), 54 - 24 * zo_abs(normalizedAngle))
				else
					pin:SetDimensions(48, 48)
				end
			end,
			additionalLayout = {
				function(pin, angle, normalizedAngle, normalizedDistance)
					if (pinTexturesList[savedVariables.pinTexture.type] == "Shalidor's Library icons") then --replace icon with icon from LoreLibrary
						local _, texture = GetLoreBookInfo(2, pin.pinTag.c, pin.pinTag.b)
						local icon = pin:GetNamedChild("Background")
						icon:SetTexture(texture)
					end
				end,
				function(pin)
					--I do not need to reset anything (texture is changed automatically), so the function is empty
				end
			}
		}

		--global saved variables for collecting lorebook loactions
		svData = ZO_SavedVars:NewAccountWide("LBooksData_SavedVariables", 5, nil, dataDefaults)

		--check if you have saved books that are not in default data
		UpdateLorebooksData()

		--initialize map pins
		LMP:AddPinType(PINS_UNKNOWN, MapCallback_unknown, nil, mapPinLayout_unknown, pinTooltipCreator)
		LMP:AddPinType(PINS_COLLECTED, MapCallback_collected, nil, mapPinLayout_collected, pinTooltipCreator)
		LMP:AddPinType(PINS_EIDETIC, MapCallback_eidetic, nil, mapPinLayout_eidetic, pinTooltipCreatorEidetic)
		--LMP:AddPinType(PINS_EIDETIC_COLLECTED, MapCallback_eideticCollected, nil, mapPinLayout_eideticCollected, pinTooltipCreatorEidetic)

		--add map filters
		LMP:AddPinFilter(PINS_UNKNOWN, GetString(LBOOKS_FILTER_UNKNOWN), nil, savedVariables.filters)
		LMP:AddPinFilter(PINS_COLLECTED, GetString(LBOOKS_FILTER_COLLECTED), nil, savedVariables.filters)
		LMP:AddPinFilter(PINS_EIDETIC, GetLoreCategoryInfo(2), nil, savedVariables.filters)
		--LMP:AddPinFilter(PINS_EIDETIC_COLLECTED, GetLoreCategoryInfo(2) .. " (collecté)", nil, savedVariables.filters)

		--add handler for the left click
		LMP:SetClickHandlers(PINS_UNKNOWN, {
			[1] = {
				name = function(pin) return zo_strformat(LBOOKS_SET_WAYPOINT, GetLoreBookInfo(1, pin.m_PinTag[3], pin.m_PinTag[4])) end,
				show = function(pin) return not select(3, GetLoreBookInfo(1, pin.m_PinTag[3], pin.m_PinTag[4])) end,
				duplicates = function(pin1, pin2) return (pin1.m_PinTag[3] == pin2.m_PinTag[3] and pin1.m_PinTag[4] == pin2.m_PinTag[4]) end,
				callback = function(pin) PingMap(MAP_PIN_TYPE_PLAYER_WAYPOINT, MAP_TYPE_LOCATION_CENTERED, pin.normalizedX, pin.normalizedY) end,
			}
		})

		--initialize compass pins
		COMPASS_PINS:AddCustomPin(PINS_COMPASS, CompassCallback, compassPinLayout)
		COMPASS_PINS:RefreshPins(PINS_COMPASS)

		COMPASS_PINS:AddCustomPin(PINS_COMPASS_EIDETIC, CompassCallbackEidetic, compassPinLayoutEidetic)
		COMPASS_PINS:RefreshPins(PINS_COMPASS_EIDETIC)
		
		-- addon menu
		CreateSettingsMenu()
		
		-- Lorelibrary Research
		CreateResearch()

		-- initialize tooltips
		OnGamepadPreferredModeChanged()
		
		BuildLorebooksLoreLibrary()

		-- Data sniffer
		ToggleShareData()
		
		--events
		EVENT_MANAGER:RegisterForEvent("LoreBooks", EVENT_LORE_BOOK_LEARNED, OnBookLearned)
		EVENT_MANAGER:RegisterForEvent("LoreBooks", EVENT_PLAYER_ACTIVATED, ShowCongrats)
		EVENT_MANAGER:RegisterForEvent("LoreBooks", EVENT_GAMEPAD_PREFERRED_MODE_CHANGED, OnGamepadPreferredModeChanged)
		
	end
	
end

EVENT_MANAGER:RegisterForEvent("LoreBooks", EVENT_ADD_ON_LOADED, OnLoad)