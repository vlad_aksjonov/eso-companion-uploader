-- Undiscovered v0.0.9
if UndiscoveredDataStore == nil then UndiscoveredDataStore = {} end

-- Variables
local savedVariables, exportedData, localLanguage
local libMapPinManager

-- Define Defaults 
local defaults = {
   pinTexture = {
      type = 1,
      size = 35,
      level = 30,
   },
   filters = {
      ["UNDC_PinSet_Unknown"] = true,
      ["UNDC_PinSet_Known"] = true,
   },
}

local function PrintLine(...)
   CHAT_SYSTEM:AddMessage(...)
end

local pinTexturesList = {
   "Default Red X",
   "Asghaard's Croix",
   "Red Dot",
   "Black X",
   "Exclaimation Point",
   "White Box",
   "Asghaard's Aura",
   "Original (Ugly Red X w/ a Box)",
}

local pinTextures = {
   unknown = {
      [1] = "Undiscovered/Icons/Undiscovered-redx.dds",
      [2] = "Undiscovered/Icons/Asghaard-croix.dds",
      [3] = "Undiscovered/Icons/Undiscovered-reddot.dds",
      [4] = "Undiscovered/Icons/Undiscovered-blackx.dds",
      [5] = "Undiscovered/Icons/Undiscovered-exclaim.dds",
      [6] = "Undiscovered/Icons/Undiscovered-found.dds",
      [7] = "Undiscovered/Icons/Asghaard-aura.dds",
      [8] = "Undiscovered/Icons/Undiscovered-defaultx.dds",
   },
   known = {
      [1] = "Undiscovered/Icons/Undiscovered-found.dds",
      [2] = "Undiscovered/Icons/Undiscovered-found.dds",
      [3] = "Undiscovered/Icons/Undiscovered-found.dds",
      [4] = "Undiscovered/Icons/Undiscovered-found.dds",
      [5] = "Undiscovered/Icons/Undiscovered-found.dds",
      [6] = "Undiscovered/Icons/Undiscovered-found.dds",
      [7] = "Undiscovered/Icons/Undiscovered-found.dds",
      [8] = "Undiscovered/Icons/Undiscovered-found.dds",
      
   },
}

  local function GetZoneAndSubzone()
    local textureName = GetMapTileTexture()
    textureName = string.lower(textureName)
    local _,_,_,zone,subzone = string.find(textureName, "(maps/)([%w%-]+)/([%w%-]+_%w+)")
    return zone, subzone
  end

  local function GetUndiscoveredData()
    local zone, subzone = GetZoneAndSubzone()
    if UndiscoveredDataStore.poiData[zone] == nil or UndiscoveredDataStore.poiData[zone][subzone] == nil then
      return nil
    end

    local undiscovered = UndiscoveredDataStore.poiData[zone][subzone]
    return undiscovered
  end

  local function GetPoiTypeName(poiTypeId)
    local poiType = {
          [1] = GetString(UNDISC_POITYPE_WAYSHRINE),
          [2] = GetString(UNDISC_POITYPE_AOI),
          [3] = GetString(UNDISC_POITYPE_PUBLICDUNGEON),
          [4] = GetString(UNDISC_POITYPE_MUNDUS),
          [5] = GetString(UNDISC_POITYPE_DOLMEN),
          [6] = GetString(UNDISC_POITYPE_GROUPBOSS),
          [7] = GetString(UNDISC_POITYPE_GROUPDUNGEON),
          [8] = GetString(UNDISC_POITYPE_QUESTHUB),
          [9] = GetString(UNDISC_POITYPE_CRAFTING), 
          [22] = GetString(UNDISC_POITYPE_UNKNOWN),
       }
     
     if poiType[poiTypeId] == nil then poiTypeId = 22 end
     return poiType[poiTypeId]
  end

  --Iterates POIs from ZO API's, checks 
  local function CreateMapPins(pinManager, pinType, complete)
    if not libMapPinManager:GetPinFilter(pinType) then return end
    if (GetMapType() > MAPTYPE_ZONE) then return end

    local zoneIndex = GetCurrentMapZoneIndex()
    local undiscovered = GetUndiscoveredData()
    
    for i=1,GetNumPOIs(zoneIndex) do
      local objectiveName, objectiveLevel, startDescription, finishedDescription  = GetPOIInfo(zoneIndex, i)
      local normalizedX, normalizedZ, poiType, icon = GetPOIMapInfo(zoneIndex, i)
      local isComplete = (poiType == MAP_PIN_TYPE_POI_COMPLETE)
      local pinTag
      local poiName = zo_strformat('<<1>>', objectiveName)
      local poiTypeId = 22
      local poiTypeName = GetPoiTypeName(poiTypeId)

      if isComplete == false then
        if (complete == 0)then
          if undiscovered ~= nil then
            for _, pinLookup in pairs(undiscovered) do
              if pinLookup[1] == objectiveName then poiTypeId = pinLookup[2] break end
            end
          end
          if poiTypeId ~= 22 then poiTypeName = GetPoiTypeName(poiTypeId) end 
          pinTag = {"|c0099CC" .. poiName .. "|r", "[" .. poiTypeName .. "]"}
          pinManager:CreatePin(_G[pinType], pinTag, normalizedX, normalizedZ)
        end
      else
        if (complete == 1) then
          if undiscovered ~= nil then
            for _, pinLookup in pairs(undiscovered) do
              if pinLookup[1] == objectiveName then poiTypeId = pinLookup[2] break end
            end
          end
          if poiTypeId ~= 22 then poiTypeName = GetPoiTypeName(poiTypeId) end 
          pinTag = {poiName, poiTypeName}
          pinManager:CreatePin(_G[pinType], pinTag, normalizedX, normalizedZ)
        end
      end
    end
  end

  --Invoked on Map update to redraw Unknown Pins
  local function MapCallback_unknown(pinManager)
    CreateMapPins(pinManager, "UNDC_PinSet_Unknown", 0)
  end

  --Invoked on Map update to redraw known Pins
  local function MapCallback_known(pinManager)
    CreateMapPins(pinManager, "UNDC_PinSet_Known", 1)
  end



local function InitSettings()
   local LAM = LibStub:GetLibrary("LibAddonMenu-2.0")

   local panelData = {
       type="panel",
       name=GetString(UNDISC_SETTINGS_TITLE),
       displayName="|c0099CC" .. GetString(UNDISC_SETTINGS_TITLE) .. "|r",
       author="dwot",
       version="0.1.0",
       registerForRefresh=true,
       registerForDefaults=true,
   }

   LAM:RegisterAddonPanel("Undiscovered_OptionsPanel", panelData)

   local optionsTable = {
       {
           --Unknown Pins Check
           type="checkbox",
           name=GetString(UNDISC_SETTINGS_CHECK_UNKNOWN),
           tooltip=GetString(UNDISC_SETTINGS_CHECK_UNKNOWN),
           getFunc=function() return libMapPinManager:GetPinFilter("UNDC_PinSet_Unknown") end,
           setFunc=function(state) libMapPinManager:FilterPinSet("UNDC_PinSet_Unknown", state) end,
           default=defaults.filters["UNDC_PinSet_Unknown"],
       },
       {
           --Known Pins Check
           type="checkbox",
           name=GetString(UNDISC_SETTINGS_CHECK_KNOWN),
           tooltip=GetString(UNDISC_SETTINGS_CHECK_KNOWN),
           getFunc=function() return libMapPinManager:GetPinFilter("UNDC_PinSet_Known") end,
           setFunc=function(state) libMapPinManager:FilterPinSet("UNDC_PinSet_Known", state) end,
           default=defaults.filters["UNDC_PinSet_Known"],
       },
       {
           --Pin Texture Dropdown
           type="dropdown",
           name=GetString(UNDISC_SETTINGS_SELECT_PIN),
           tooltip=GetString(UNDISC_SETTINGS_SELECT_PIN),
           choices=pinTexturesList,
           getFunc=function() return pinTexturesList[savedVariables.pinTexture.type] end,
           setFunc=function(pinTextureListName)
               for index, name in pairs(pinTexturesList) do
                   if name == pinTextureListName then
                       savedVariables.pinTexture.type = index
                       libMapPinManager:SetPinSetTexture("UNDC_PinSet_Unknown", pinTextures.unknown[index])
                       libMapPinManager:SetPinSetTexture("UNDC_PinSet_Known", pinTextures.known[index])
                   end
               end
           end,
       },
       {
           --PinSize Slider
           type="slider",
           name=GetString(UNDISC_SETTINGS_SLIDER_PINSIZE),
           tooltip=GetString(UNDISC_SETTINGS_SLIDER_PINSIZE),
           min=20,
           max=70,
           getFunc=function() return savedVariables.pinTexture.size end,
           setFunc=function(size)
               savedVariables.pinTexture.size = size
               libMapPinManager:SetPinSetSize("UNDC_PinSet_Unknown", size)
               libMapPinManager:SetPinSetSize("UNDC_PinSet_Known", size)
           end,
       },
       {
           --PinLayer Slider
           type="slider",
           name=GetString(UNDISC_SETTINGS_SLIDER_PINLAYER),
           tooltip=GetString(UNDISC_SETTINGS_SLIDER_PINLAYER),
           min=10,
           max=200,
           step=10,
           getFunc=function() return savedVariables.pinTexture.level end,
           setFunc=function(layer)
               savedVariables.pinTexture.level = layer
               libMapPinManager:SetPinSetLayer("UNDC_PinSet_Unknown", layer)
               libMapPinManager:SetPinSetLayer("UNDC_PinSet_Known", layer)
           end,
       },
   }

   LAM:RegisterOptionControls("Undiscovered_OptionsPanel", optionsTable)

end

  --POI Processing
  --On "EVENT_POI_DISCOVERED" redraw map pins
  local function OnPOIUpdate(zoneIndex, poiIndex)
    --PrintLine("Event: POI Updated - " .. zoneIndex .. " " .. poiIndex)
    libMapPinManager:RefreshPinSet("UNDC_PinSet_Unknown")
    libMapPinManager:RefreshPinSet("UNDC_PinSet_Known")
  end

  local function OnPOIDiscovered(zoneIndex, poiIndex)
    --PrintLine("Event: POI Discovered - " .. zoneIndex .. " " .. poiIndex)
    libMapPinManager:RefreshPinSet("UNDC_PinSet_Unknown")
    libMapPinManager:RefreshPinSet("UNDC_PinSet_Known")
  end
  --Initialize the addon
  local function OnLoad(eventCode, name)
    if name ~= "Undiscovered" then return end
  
    libMapPinManager = LibStub("LibMapPinManager-1.0")
    
    savedVariables = ZO_SavedVars:New("Undiscovered_Settings", 1, nil, defaults)
    exportedData = ZO_SavedVars:New("Undiscovered_Settings", 1, "export", nil)
  
    --Check Language, Addon not yet localized
    localLanguage = GetCVar("language.2") or "en"
    if not (localLanguage == "en"  or localLanguage == "de" ) then
      PrintLine("Undiscovered is not properly localized for " .. localLanguage .. ".  English terms will be used and not all POIs may be properly classified.")
    end
  
    --Load Pin Settings
    local pinTextureType = savedVariables.pinTexture.type or defaults.pinTexture.type
    local pinTextureLevel = savedVariables.pinTexture.level or defaults.pinTexture.level
    local pinTextureSize = savedVariables.pinTexture.size or defaults.pinTexture.size
  
    --Establish Pin Configurations
    local pinLayout_unknown = {
      level = pinTextureLevel,
      texture = pinTextures.unknown[pinTextureType],
      size = pinTextureSize,
    }
    local pinLayout_known = {
      level = pinTextureLevel,
      texture = pinTextures.known[pinTextureType],
      size = pinTextureSize,
    }
  
    --Create the Map Pins
    libMapPinManager:RegisterFilterablePinSet("UNDC_PinSet_Unknown", GetString(UNDISC_FILTER_UNKNOWN),  MapCallback_unknown, pinLayout_unknown) 
    libMapPinManager:RegisterFilterablePinSet("UNDC_PinSet_Known", GetString(UNDISC_FILTER_KNOWN),  MapCallback_known, pinLayout_known) 
    
    --Initialize Settings Menu
    InitSettings()
  
    --Register Event Triggers
    EVENT_MANAGER:RegisterForEvent("Undiscovered",  EVENT_POI_DISCOVERED, OnPOIDiscovered)
    EVENT_MANAGER:RegisterForEvent("Undiscovered",  EVENT_POI_UPDATED, OnPOIUpdate)
    EVENT_MANAGER:UnregisterForEvent("Undiscovered", EVENT_ADD_ON_LOADED)
  end

EVENT_MANAGER:RegisterForEvent("Undiscovered", EVENT_ADD_ON_LOADED, OnLoad)
