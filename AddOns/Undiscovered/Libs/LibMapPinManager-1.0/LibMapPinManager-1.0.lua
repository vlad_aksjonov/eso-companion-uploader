-- LibMapPinManager v1.0
local MAJOR, MINOR = "LibMapPinManager-1.0", 1
local libMapPinManager = LibStub:NewLibrary(MAJOR, MINOR)
if not libMapPinManager then return end

local defaultLevel = "50"
local defaultTexture = "LibMapPinManager-1.0/LibMapPinManager-defaultx.dds"
local defaultSize = "30"

local pinSettings 

local filterPanels = {
   pvePanel = {},
   pvpPanel = {},
}

local function createPanelFilter(panel, pinSet, pinSetName)
   local checkBox = panel.checkBoxPool:AcquireObject()
   ZO_CheckButton_SetLabelText(checkBox, pinSetName)
   ZO_CheckButton_SetToggleFunction(checkBox,
      function(control, state)
         pinSettings.filters[pinSet] = state
         ZO_CheckButton_SetCheckState(filterPanels.pvePanel[pinSet], state)
         ZO_CheckButton_SetCheckState(filterPanels.pvpPanel[pinSet], state)
         ZO_WorldMap_RefreshCustomPinsOfType(_G[pinSet])
      end)
   panel:AnchorControl(checkBox)
   return checkBox
end

local defaultLayout = {
      level = defaultLevel,
      texture = defaultTexture,
      size = defaultSize,
    }

local function printLine(...)
   CHAT_SYSTEM:AddMessage(...)
end

local generateTooltip = {
  creator = function(pin)
    local _, pinTag = pin:GetPinTypeAndTag()
    for i,lineData in pairs(pinTag) do
      InformationTooltip:AddLine(lineData)
    end
  end,
  tooltip = InformationTooltip,
}

function libMapPinManager:RegisterPinSet(pinSet, pinSetName,  pinLoadFunction, pinLayout) 
    --printLine("libMapPinManager:RegisterPinSet")
    if pinLayout == nil then pinLayout = defaultLayout end    

    ZO_WorldMap_AddCustomPin(pinSet, pinLoadFunction, nil, pinLayout, generateTooltip)
    ZO_WorldMap_SetCustomPinEnabled(_G[pinSet], true)
    
    libMapPinManager:RefreshPinSet(pinSet)
end

function libMapPinManager:RegisterFilterablePinSet(pinSet, pinSetName, pinLoadFunction, pinLayout)
      --printLine("libMapPinManager:RegisterFilterablePinSet")
      --printLine("REGISTER PIN INVOKED")
      libMapPinManager:RegisterPinSet(pinSet, pinSetName,  pinLoadFunction, pinLayout)
      
      filterPanels.pvePanel[pinSet] = createPanelFilter(WORLD_MAP_FILTERS.pvePanel, pinSet, pinSetName)
      filterPanels.pvpPanel[pinSet] = createPanelFilter(WORLD_MAP_FILTERS.pvpPanel, pinSet, pinSetName)
      
      if libMapPinManager:GetPinFilter(pinSet) == nil then
        --printLine(pinSet .. " filter is nil") 
        libMapPinManager:FilterPinSet(pinSet, true)
      else
        --printLine(pinSet .. " filter looks like " .. tostring(libMapPinManager:GetPinFilter(pinSet)))
        libMapPinManager:FilterPinSet(pinSet, libMapPinManager:GetPinFilter(pinSet)) 
      end
      
end

function libMapPinManager:FilterPinSet(pinSet, filterState)
      --printLine("libMapPinManager:FilterPinSet")
      pinSettings.filters[pinSet] = filterState
      ZO_CheckButton_SetCheckState(filterPanels.pvpPanel[pinSet], pinSettings.filters[pinSet])
      ZO_CheckButton_SetCheckState(filterPanels.pvePanel[pinSet], pinSettings.filters[pinSet]) 
      libMapPinManager:RefreshPinSet(pinSet)
end

function libMapPinManager:GetPinFilter(pinSet)
      --printLine("libMapPinManager:GetPinFilter")
      local defaultPinSettings = { filters = {} }
      pinSettings = ZO_SavedVars:New("LibMPM_Filters", 1, nil, defaultPinSettings, nil) 
      --printLine("GetPinFilter: " .. pinSet)
      if pinSettings.filters[pinSet] == nil then return nil end
      --printLine("FILTER : " .. tostring(pinSettings.filters[pinSet]))
      return pinSettings.filters[pinSet]
end

function libMapPinManager:SetPinSetTexture(pinSet, texture)
      --printLine("libMapPinManager:SetPinSetTexture")
      ZO_MapPin.PIN_DATA[_G[pinSet]].texture = texture
      libMapPinManager:RefreshPinSet(pinSet)
end

function libMapPinManager:SetPinSetSize(pinSet, size)
      --printLine("libMapPinManager:SetPinSetSize")
      ZO_MapPin.PIN_DATA[_G[pinSet]].size = size
      libMapPinManager:RefreshPinSet(pinSet)
end

function libMapPinManager:SetPinSetLayer(pinSet, layer)
      --printLine("libMapPinManager:SetPinSetLayer " .. pinSet .. ":" .. layer)
      ZO_MapPin.PIN_DATA[_G[pinSet]].level = layer
      libMapPinManager:RefreshPinSet(pinSet)
end

function libMapPinManager:RefreshPinSet(pinSet)
      --printLine("libMapPinManager:RefreshPinSet")
      ZO_WorldMap_RefreshCustomPinsOfType(_G[pinSet])
end
