----------------------------------------
-- French localization for Undiscovered --
----------------------------------------
local Add = ZO_CreateStringId

do
  --Map Filter Tags
  Add("UNDISC_FILTER_KNOWN",                   "Known POIs")
  Add("UNDISC_FILTER_UNKNOWN",                 "Unknown POIs")

  --Settings Menu
  Add("UNDISC_SETTINGS_TITLE",                 "Undiscovered")
  Add("UNDISC_SETTINGS_CHECK_UNKNOWN",         "Show unknown POIs")
  Add("UNDISC_SETTINGS_CHECK_KNOWN",           "Show already known POIs")
  Add("UNDISC_SETTINGS_SELECT_PIN",            "Select map pin icons")
  Add("UNDISC_SETTINGS_SLIDER_PINSIZE",        "Pin size")
  Add("UNDISC_SETTINGS_SLIDER_PINLAYER",       "Pin layer")
  Add("UNDISC_SETTINGS_RESET",                 "Reset to default settings")
  
   --POI Types
   Add("UNDISC_POITYPE_WAYSHRINE",                    "Wayshrine")
   Add("UNDISC_POITYPE_AOI",                    "Area of Interest")
   Add("UNDISC_POITYPE_PUBLICDUNGEON",                    "Public Dungeon")
   Add("UNDISC_POITYPE_MUNDUS",                    "Mundus Stone")
   Add("UNDISC_POITYPE_DOLMEN",                    "Dolmen")
   Add("UNDISC_POITYPE_GROUPBOSS",                    "Group Boss")
   Add("UNDISC_POITYPE_GROUPDUNGEON",                    "Group Dungeon")
   Add("UNDISC_POITYPE_QUESTHUB",                    "Quest Hub")
   Add("UNDISC_POITYPE_CRAFTING",                    "Crafting Location") 
   Add("UNDISC_POITYPE_UNKNOWN",                    "Unknown")   
end

