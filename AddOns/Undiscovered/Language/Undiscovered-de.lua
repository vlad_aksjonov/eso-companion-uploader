----------------------------------------
-- German localization for Undiscovered --
-- Contributed by DaBear78 on esoui.com --
-- Thanks!!!! --
----------------------------------------
local Add = ZO_CreateStringId

do
  --Map Filter Tags
  Add("UNDISC_FILTER_KNOWN",                   "Entdeckte POIs")
  Add("UNDISC_FILTER_UNKNOWN",                 "Unentdeckte POIs")
 
  --Settings Menu
  Add("UNDISC_SETTINGS_TITLE",                 "Undiscovered")
  Add("UNDISC_SETTINGS_CHECK_UNKNOWN",         "Zeige unentdeckte POIs")
  Add("UNDISC_SETTINGS_CHECK_KNOWN",           "Zeige bereits entdeckte POIs")
  Add("UNDISC_SETTINGS_SELECT_PIN",            "Kartenpin-Icons auswählen")
  Add("UNDISC_SETTINGS_SLIDER_PINSIZE",        "Pingröße")
  Add("UNDISC_SETTINGS_SLIDER_PINLAYER",       "Pinlayer")
  Add("UNDISC_SETTINGS_RESET",                 "Auf Standard zurücksetzen")
  
   --POI Types
   Add("UNDISC_POITYPE_WAYSHRINE",                    "Wegschrein")
   Add("UNDISC_POITYPE_AOI",                    "interessantes Gebiet")
   Add("UNDISC_POITYPE_PUBLICDUNGEON",                    "Öffentliches Verlies")
   Add("UNDISC_POITYPE_MUNDUS",                    "Mundusstein")
   Add("UNDISC_POITYPE_DOLMEN",                    "Dolmen")
   Add("UNDISC_POITYPE_GROUPBOSS",                    "Gruppen-Boss")
   Add("UNDISC_POITYPE_GROUPDUNGEON",                    "Gruppen-Verlies")
   Add("UNDISC_POITYPE_QUESTHUB",                    "Quest Hub")
   Add("UNDISC_POITYPE_CRAFTING",                    "Handwerksstation") 
   Add("UNDISC_POITYPE_UNKNOWN",                    "Unbekannt")   
end