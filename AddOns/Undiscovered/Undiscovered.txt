## Title: |c0099CCUndiscovered|r 0.1.0|r
## Version: 0.1.0
## Author: dwot
## APIVersion: 100011
## SavedVariables: Undiscovered_Settings LibMPM_Filters
## OptionalDependsOn: LibStub LibAddonMenu-2.0 LibMapPinManager

Libs/LibStub/LibStub.lua
Libs/LibAddonMenu-2.0/LibAddonMenu-2.0.lua
Libs/LibMapPinManager-1.0/LibMapPinManager-1.0.lua

Language/Undiscovered-$(language).lua
Language/UndiscoveredData-$(language).lua

Undiscovered.lua
