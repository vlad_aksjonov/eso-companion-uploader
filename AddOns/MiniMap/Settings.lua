------------------------------------------------------------
-- Settings
-------------------------------------------------------------
function MM_SetPPStyle(value)
	if value == nil then return end
	if value == "Classic" then -- Upgrade from version 2.24 or earlier
		value = GetString(SI_MM_STRING_CLASSIC)
	end
	if value == "Separate Player & Camera" then -- Upgrade from version 2.24 or earlier
		value = GetString(SI_MM_STRING_PLAYERANDCAMERA)
	end
	FyrMM.SV.PPStyle = value
	if value == GetString(SI_MM_STRING_PLAYERANDCAMERA) then
		Fyr_MM_Camera:SetHidden(false)
	else
		Fyr_MM_Camera:SetHidden(true)
	end
end

function MM_Upgrade_MapList()
	if FyrMM.SV.MapSettings == nil then return end
	FyrMM.SV.MapTable = {}
	for i, v in pairs(FyrMM.SV.MapSettings) do
		FyrMM.SV.MapTable[v.FileName] = {ZoomLevel = v.ZoomLevel, MapSize = v.MapSize}
	end
	FyrMM.SV.MapSettings = nil
end

function MM_SetPPTextures(value)
	if value == nil then return end
	FyrMM.SV.PPTextures = value
	if value == "ESO UI Worldmap" then
		Fyr_MM_Player:SetTexture("EsoUI/Art/MapPins/UI-WorldMapPlayerPip.dds")
		Fyr_MM_Camera:SetTexture("/MiniMap/Textures/ui-worldmapplayercamerapip.dds")
		Fyr_MM_Camera:SetBlendMode(TEX_BLEND_MODE_ADD)
		Fyr_MM_Player:SetScale(1)
		Fyr_MM_Camera:SetScale(1)
	else
	if value == "Vixion Regular" then
		Fyr_MM_Player:SetTexture("/MiniMap/Textures/VixionPlayerPointer_arrow.dds")
		Fyr_MM_Camera:SetTexture("/MiniMap/Textures/VixionCameraPointer_glow.dds")
		Fyr_MM_Camera:SetBlendMode(TEX_BLEND_MODE_ADD)
		Fyr_MM_Player:SetScale(1)
		Fyr_MM_Camera:SetScale(1)
	else
		if value == "Vixion Gold" then
			Fyr_MM_Player:SetTexture("/MiniMap/Textures/VixionPlayerPointer_arrow_gold.dds")
			Fyr_MM_Camera:SetTexture("/MiniMap/Textures/VixionCameraPointer_glow.dds")
			Fyr_MM_Camera:SetBlendMode(TEX_BLEND_MODE_ADD)
			Fyr_MM_Player:SetScale(1)
			Fyr_MM_Camera:SetScale(1)
		else
			Fyr_MM_Player:SetTexture("/MiniMap/Textures/PlayerPointer.dds")
			Fyr_MM_Camera:SetTexture("/MiniMap/Textures/CameraPointer.dds")
			Fyr_MM_Camera:SetBlendMode(TEX_BLEND_MODE_ADD)
			local scale = 0.75
		end
	end
	
		if FyrMM.SV.PinScale ~= nil then
			scale = FyrMM.SV.PinScale/100
		end
		Fyr_MM_Player:SetScale(scale)
		Fyr_MM_Camera:SetScale(scale)
	end
end


function MM_SetBorderPins(value)
	if value == nil then return end
	FyrMM.SV.BorderPins = value
	if not FyrMM.Initialized then return end
	Fyr_MM_Axis_Control:SetHidden(not (FyrMM.SV.RotateMap or FyrMM.SV.BorderPins))
	Fyr_MM_Axis_Control:SetTopmost(FyrMM.SV.RotateMap or FyrMM.SV.BorderPins)
	FyrMM.PlaceBorderPins()
end

function MM_SetUseOriginalAPI(value)
	if value == nil then return end
	FyrMM.SV.UseOriginalAPI = value
	FyrMM.API_Check()
end

function MM_SetShowUnexploredPins(value)
	if value == nil then return end
	FyrMM.SV.ShowUnexploredPins = value
	if FyrMM.Initialized then
		FyrMM.Reload()
	end
end

function MM_SetMapRefreshRate(value)
	if value == nil then return end
	FyrMM.SV.MapRefreshRate = value
	if not FyrMM.Initialized then return end
	EVENT_MANAGER:UnregisterForUpdate("OnUpdateFyrMMMapPosition")
	EVENT_MANAGER:RegisterForUpdate("OnUpdateFyrMMMapPosition", FyrMM.SV.MapRefreshRate, FyrMM.PositionUpdate)
end

function MM_SetPinRefreshRate(value)
	if value == nil then return end
	FyrMM.SV.PinRefreshRate = value
	if not FyrMM.Initialized then return end
	EVENT_MANAGER:UnregisterForUpdate("OnUpdateFyrMMMapPins")
	EVENT_MANAGER:RegisterForUpdate("OnUpdateFyrMMMapPins", FyrMM.SV.PinRefreshRate, FyrMM.PinUpdate)
end

function MM_SetViewRefreshRate(value)
	if value == nil then return end
	FyrMM.SV.ViewRefreshRate = value
	if not FyrMM.Initialized then return end
	EVENT_MANAGER:UnregisterForUpdate("OnUpdateFyrMMMapView")
	EVENT_MANAGER:RegisterForUpdate("OnUpdateFyrMMMapView", FyrMM.SV.ViewRefreshRate, FyrMM.UpdateMapTiles)
end

function MM_SetZoneRefreshRate(value)
	if value == nil then return end
	FyrMM.SV.ZoneRefreshRate = value
	if not FyrMM.Initialized then return end
	EVENT_MANAGER:UnregisterForUpdate("OnUpdateFyrMMMapZone")
	EVENT_MANAGER:RegisterForUpdate("OnUpdateFyrMMMapZone", FyrMM.SV.ZoneRefreshRate, FyrMM.ZoneUpdate)
end

function MM_SetKeepNetworkRefreshRate(value)
	if value == nil then return end
	if value < 900 then value = 2000 end
	FyrMM.SV.KeepNetworkRefreshRate = value
	if not FyrMM.Initialized then return end
	EVENT_MANAGER:UnregisterForUpdate("OnUpdateFyrMMMapKeepNetwork")
	EVENT_MANAGER:RegisterForUpdate("OnUpdateFyrMMMapKeepNetwork", FyrMM.SV.KeepNetworkRefreshRate, FyrMM.UpdateKeepNetwork)
end


function MM_GetMapHeight()
	local mheight = Fyr_MM_Scroll:GetHeight()
	return mheight
end

function MM_GetMapWidth()
	local mwidth = Fyr_MM_Scroll:GetWidth()
	return mwidth
end

function MM_SetMapHeight(value)
if value == nil or FyrMM.SV.WheelMap then return end
if FyrMM.SV.LockPosition and FyrMM.Initialized then return end 
	FyrMM.SV.MapHeight = value
	Fyr_MM_Scroll:SetHeight(value)
	Fyr_MM_Border:SetHeight(value+8)
	Fyr_MM:SetHeight(value)
	Fyr_MM_Scroll_Fill:SetHeight(value)
	FyrMM.MapHalfDiagonal()
	MM_RearrangeMenu()
end

function MM_SetMapWidth(value)
if value == nil then return end
	if FyrMM.SV.LockPosition and FyrMM.Initialized then return end 
	FyrMM.SV.MapWidth = value
	Fyr_MM_Scroll:SetWidth(value)
	Fyr_MM_Border:SetWidth(value+8)
	Fyr_MM_Frame_Wheel:SetDimensions(value+8, value+8)
	Fyr_MM_Frame_RoundMenu:SetDimensions(value, value/4)
	Fyr_MM_Frame_RoundMenu:ClearAnchors()
	Fyr_MM_Frame_RoundMenu:SetAnchor(TOPLEFT, Fyr_MM, TOPLEFT, 0, value - value/9)
	Fyr_MM_Frame_SquareMenu:SetDimensions(value, value/4)
	Fyr_MM_Wheel_Background:SetDimensions(value+8, value+8)
	Fyr_MM:SetWidth(value)
	Fyr_MM_Scroll_Fill:SetWidth(value)
	if FyrMM.SV.WheelMap then
		FyrMM.SV.MapHeight = value
		Fyr_MM_Scroll:SetHeight(value)
		Fyr_MM_Border:SetHeight(value+8)
		Fyr_MM:SetHeight(value)
		Fyr_MM_Scroll_Fill:SetHeight(value)
		FyrMM.MapHalfDiagonal()
	end
	MM_RearrangeMenu()
end

function MM_GetPinScale()
	local scale = FyrMM.SV.PinScale
	return scale
end

function MM_SetPinScale(value)
if value == nil then return end
	FyrMM.pScale = value
	FyrMM.pScalePercent = FyrMM.pScale / 100
	MM_SetPPTextures(FyrMM.SV.PPTextures)
	FyrMM.SV.PinScale = value
end

function MM_GetMapAlpha()
	return FyrMM.SV.MapAlpha
end

function MM_SetMapAlpha(value)
if value == nil then return end
	local newAlphaPercent = value / 100
	FyrMM.SV.MapAlpha = value
	Fyr_MM_Wheel_Background:SetAlpha(newAlphaPercent+.10)
	Fyr_MM:SetAlpha(newAlphaPercent)
	Fyr_MM_Scroll_WheelWE:SetAlpha(newAlphaPercent)
	Fyr_MM_Scroll_WheelNS:SetAlpha(newAlphaPercent)
	Fyr_MM_Scroll_WheelCenter:SetAlpha(newAlphaPercent)
end

function MM_GetShowPosition()
	return FyrMM.SV.ShowPosition
end

function MM_SetShowPosition(value)
if value == nil then return end
	FyrMM.SV.ShowPosition = value
	Fyr_MM_Coordinates:SetHidden(not FyrMM.SV.ShowPosition)
end

function MM_GetMapZoom()
	local MM_zoom = FyrMM.SV.MapZoom
	if MM_zoom == nil then
		FyrMM.SV.MapZoom = FyrMM_DEFAULT_ZOOM_LEVEL
		MM_zoom = FyrMM_DEFAULT_ZOOM_LEVEL
	end

	return MM_zoom
end

function MM_GetHideCompass()
	return FyrMM.SV.hideCompass
end

function MM_SetHideCompass(value)
if value == nil then return end
	FyrMM.SV.hideCompass = value
	ZO_CompassFrame:SetHidden(not value)
end

function MM_SetRotateMap(value)
if value == nil then return end
	FyrMM.SV.RotateMap = value
	Fyr_MM_Axis_Control:SetHidden(not FyrMM.SV.RotateMap)
	FyrMM.AxisPins()
	if FyrMM.SV.WheelMap and not FyrMM.SV.RotateMap then
		Fyr_MM_Frame_Wheel:SetTextureRotation(0)
	end
	if not FyrMM.SV.RotateMap then
		FyrMM.UpdateMapTiles()
		FyrMM.Reload()
	end
end

function MM_RearrangeMenu()
	local scale = zo_round(Fyr_MM:GetWidth()/3.41)/100
	Fyr_MM_Time:SetScale(scale)
	Fyr_MM_ZoomIn:SetScale(scale)
	Fyr_MM_ZoomOut:SetScale(scale)
	Fyr_MM_PinDown:SetScale(scale)
	Fyr_MM_PinUp:SetScale(scale)
	Fyr_MM_Reload:SetScale(scale)
	Fyr_MM_Settings:SetScale(scale)
	if FyrMM.SV.WheelMap then
		Fyr_MM_Time:ClearAnchors()
		Fyr_MM_Time:SetDrawLayer(4)
		Fyr_MM_Time:SetAnchor(CENTER, Fyr_MM_Frame_RoundMenu, CENTER, 0, Fyr_MM_Frame_RoundMenu:GetHeight()/5.5)
		Fyr_MM_PinDown:ClearAnchors()
		Fyr_MM_PinDown:SetAnchor(CENTER, Fyr_MM_Frame_RoundMenu, CENTER, -Fyr_MM_Time:GetWidth()/1.4-Fyr_MM_ZoomIn:GetWidth()/1.5, Fyr_MM_Frame_RoundMenu:GetHeight()/11)
		Fyr_MM_PinUp:ClearAnchors()
		Fyr_MM_PinUp:SetAnchor(CENTER, Fyr_MM_Frame_RoundMenu, CENTER, -Fyr_MM_Time:GetWidth()/1.4-Fyr_MM_ZoomIn:GetWidth()/1.5, Fyr_MM_Frame_RoundMenu:GetHeight()/11)
		Fyr_MM_Reload:ClearAnchors()
		Fyr_MM_Reload:SetAnchor(CENTER, Fyr_MM_Frame_RoundMenu, CENTER, Fyr_MM_Time:GetWidth()/1.4+Fyr_MM_ZoomOut:GetWidth()/1.5, Fyr_MM_Frame_RoundMenu:GetHeight()/11)
		Fyr_MM_Settings:ClearAnchors()
		Fyr_MM_Settings:SetAnchor(CENTER, Fyr_MM_Frame_RoundMenu, CENTER, Fyr_MM_Time:GetWidth()/1.4+2*(Fyr_MM_ZoomOut:GetWidth()/1.5), -Fyr_MM_Frame_RoundMenu:GetHeight()/32)
		Fyr_MM_ZoomIn:ClearAnchors()
		Fyr_MM_ZoomIn:SetAnchor(CENTER, Fyr_MM_Frame_RoundMenu, CENTER, -Fyr_MM_Time:GetWidth()/1.4, Fyr_MM_Frame_RoundMenu:GetHeight()/6.5)
		Fyr_MM_ZoomOut:ClearAnchors()
		Fyr_MM_ZoomOut:SetAnchor(CENTER, Fyr_MM_Frame_RoundMenu, CENTER, Fyr_MM_Time:GetWidth()/1.4, Fyr_MM_Frame_RoundMenu:GetHeight()/6.5)
		Fyr_MM_Menu:SetDimensions(Fyr_MM_Frame_RoundMenu:GetDimensions())
		Fyr_MM_Menu:ClearAnchors()
		Fyr_MM_Menu:SetAnchor(TOPLEFT, Fyr_MM, TOPLEFT, 0, Fyr_MM:GetHeight() - Fyr_MM:GetHeight()/9)
	else
		Fyr_MM_Frame_SquareMenu:ClearAnchors()
		Fyr_MM_Frame_SquareMenu:SetAnchor(TOPLEFT, Fyr_MM, TOPLEFT, 0, Fyr_MM:GetHeight() - Fyr_MM_Frame_SquareMenu:GetHeight()/4.5)
		Fyr_MM_Time:ClearAnchors()
		Fyr_MM_Time:SetDrawLayer(4)
		Fyr_MM_Time:SetAnchor(CENTER, Fyr_MM_Frame_SquareMenu, CENTER, 0, Fyr_MM_Frame_SquareMenu:GetHeight()/32)
		Fyr_MM_PinDown:ClearAnchors()
		Fyr_MM_PinDown:SetAnchor(CENTER, Fyr_MM_Frame_SquareMenu, CENTER, -Fyr_MM_Time:GetWidth()/1.4-Fyr_MM_ZoomIn:GetWidth()/1.5, Fyr_MM_Frame_SquareMenu:GetHeight()/48)
		Fyr_MM_PinUp:ClearAnchors()
		Fyr_MM_PinUp:SetAnchor(CENTER, Fyr_MM_Frame_SquareMenu, CENTER, -Fyr_MM_Time:GetWidth()/1.4-Fyr_MM_ZoomIn:GetWidth()/1.5, Fyr_MM_Frame_SquareMenu:GetHeight()/48)
		Fyr_MM_Reload:ClearAnchors()
		Fyr_MM_Reload:SetAnchor(CENTER, Fyr_MM_Frame_SquareMenu, CENTER, Fyr_MM_Time:GetWidth()/1.4+Fyr_MM_ZoomOut:GetWidth()/1.5, Fyr_MM_Frame_SquareMenu:GetHeight()/48)
		Fyr_MM_Settings:ClearAnchors()
		Fyr_MM_Settings:SetAnchor(CENTER, Fyr_MM_Frame_SquareMenu, CENTER, Fyr_MM_Time:GetWidth()/1.4+2*(Fyr_MM_ZoomOut:GetWidth()/1.5), Fyr_MM_Frame_SquareMenu:GetHeight()/48)
		Fyr_MM_ZoomIn:ClearAnchors()
		Fyr_MM_ZoomIn:SetAnchor(CENTER, Fyr_MM_Frame_SquareMenu, CENTER, -Fyr_MM_Time:GetWidth()/1.4, Fyr_MM_Frame_SquareMenu:GetHeight()/48)
		Fyr_MM_ZoomOut:ClearAnchors()
		Fyr_MM_ZoomOut:SetAnchor(CENTER, Fyr_MM_Frame_SquareMenu, CENTER, Fyr_MM_Time:GetWidth()/1.4, Fyr_MM_Frame_SquareMenu:GetHeight()/48)
		Fyr_MM_Menu:SetDimensions(Fyr_MM_Frame_SquareMenu:GetDimensions())
		Fyr_MM_Menu:ClearAnchors()
		Fyr_MM_Menu:SetAnchor(TOPLEFT, Fyr_MM, TOPLEFT, 0, Fyr_MM:GetHeight() - Fyr_MM_Frame_SquareMenu:GetHeight()/4.5)
	end
end

function MM_SetWheelMap(value)
if value == nil then return end
	if value ~= FyrMM.SV.WheelMap and Fyr_MM:GetWidth() ~= Fyr_MM:GetHeight() then
		FyrMM.SV.WheelMap = value
		MM_SetMapWidth(Fyr_MM:GetWidth())
	end
	FyrMM.SV.WheelMap = value
	Fyr_MM_Border:SetHidden(FyrMM.SV.WheelMap)
	Fyr_MM_Frame_Wheel:SetHidden(not FyrMM.SV.WheelMap)
	Fyr_MM_Wheel_Background:SetHidden(not FyrMM.SV.WheelMap)
	Fyr_MM_Frame_RoundMenu:SetHidden(not FyrMM.SV.WheelMap)
	Fyr_MM_Frame_SquareMenu:SetHidden(FyrMM.SV.WheelMap)
	Fyr_MM_Scroll_Fill:SetHidden(FyrMM.SV.WheelMap)
	MM_RearrangeMenu()
	FyrMM.UpdateMapTiles(true)
	FyrMM.Reload()
	Fyr_MM_Frame_Wheel:SetHidden(not FyrMM.SV.WheelMap)
	Fyr_MM_Frame_Control:SetHidden(not FyrMM.SV.WheelMap)
	if FyrMM.SV.WheelMap then
		FyrMM.Show_WheelScrolls()
	end
end

function MM_GetShowBorder()
	return FyrMM.SV.ShowBorder
end

function MM_SetShowBorder(value)
if value == nil then return end
	FyrMM.SV.ShowBorder = value
	if FyrMM.SV.ShowBorder ~= true then Fyr_MM_Border:SetAlpha(0) else Fyr_MM_Border:SetAlpha(100) end
end

function MM_GetHeading()
	return FyrMM.SV.Heading
end

function MM_SetHeading(value)
if value == nil then return end
	FyrMM.SV.Heading = value
end

function MM_GetClampedToScreen()
 return FyrMM.SV.ClampedToScreen
end

function MM_SetClampedToScreen(value)
if value == nil then return end
 FyrMM.SV.ClampedToScreen = value
 Fyr_MM:SetClampedToScreen(value)
end

function MM_GetMapRefreshRate()
 return FyrMM.SV.MapRefreshRate
end

function MM_GetPinRefreshRate()
 return FyrMM.SV.PinRefreshRate
end

function MM_GetViewRefreshRate()
 return FyrMM.SV.ViewRefreshRate
end

function MM_GetZoneRefreshRate()
 return FyrMM.SV.ZoneRefreshRate
end

function MM_GetKeepNetworkRefreshRate()
 return FyrMM.SV.KeepNetworkRefreshRate
end

function MM_GetPinTooltips()
	return FyrMM.SV.PinTooltips
end

function MM_SetBorderPinsOnlyAssisted(value)
	if value == nil then return end
	FyrMM.SV.BorderPinsOnlyAssisted = value
	if not FyrMM.Initialized then return end
	FyrMM.PlaceBorderPins()
end

function MM_SetBorderPinsOnlyLeader(value)
if value == nil then return end
	FyrMM.SV.BorderPinsOnlyLeader = value
	if not FyrMM.Initialized then return end
	FyrMM.PlaceBorderPins()
end

function MM_SetBorderPinsWaypoint(value)
if value == nil then return end
	FyrMM.SV.BorderPinsWaypoint = value
	if not FyrMM.Initialized then return end
	FyrMM.PlaceBorderPins()
end

function MM_SetPinTooltips(value)
if value == nil then return end
	FyrMM.SV.PinTooltips = value
end

function MM_GetMapStepping()
	if FyrMM.SV.MapStepping == nil then
		FyrMM.SV.MapStepping = 2
	end
	return FyrMM.SV.MapStepping
end


function MM_SetMapStepping(value)
if value == nil then return end
	FyrMM.SV.MapStepping = value
end


function MM_GetFastTravelEnabled()
	return FyrMM.SV.FastTravelEnabled
end

function MM_SetFastTravelEnabled(value)
if value == nil then return end
	FyrMM.SV.FastTravelEnabled = value
end

function MM_GetHidePvPPins()
	return FyrMM.SV.HidePvPPins
end

function MM_SetHidePvPPins(value)
if value == nil then return end
	FyrMM.SV.HidePvPPins = value
end

function MM_GetMouseWheel()
	return FyrMM.SV.MouseWheel
end

function MM_SetMouseWheel(value)
if value == nil then return end
	FyrMM.SV.MouseWheel = value
end

function MM_GetHideZoneLabel()
	return FyrMM.SV.HideZoneLabel
end

function MM_SetHideZoneLabel(value)
if value == nil then return end
	FyrMM.SV.HideZoneLabel = value
	Fyr_MM_ZoneFrame:SetHidden(value)
end

function MM_SetInCombatAutoHide(value)
if value == nil then return end
	FyrMM.SV.InCombatAutoHide = value
end

function MM_SetAfterCombatUnhideDelay(value)
if value == nil then return end
	FyrMM.SV.AfterCombatUnhideDelay = value
end


function MM_GetHideZoomLevel()
	return FyrMM.SV.HideZoomLevel
end

function MM_SetHideZoomLevel(value)
if value == nil then return end
	FyrMM.SV.HideZoomLevel = value
	if FyrMM.SV.HideZoomLevel == true then Fyr_MM_ZoomLevel:SetAlpha(0) else Fyr_MM_ZoomLevel:SetAlpha(100) end
end

function  MM_SetShowClock(value)
if value == nil then return end
	FyrMM.SV.ShowClock = value
end

function  MM_SetShowFPS(value)
if value == nil then return end
	FyrMM.SV.ShowFPS = value
end

function  MM_SetLeaderPin(value)
if value == nil then return end
	FyrMM.SV.LeaderPin = value
end

function  MM_SetLeaderPinSize(value)
if value == nil then return end
	FyrMM.SV.LeaderPinSize = value
end

function MM_SetLeaderPinColor(r, g, b, a)
if r == nil then return end
	FyrMM.SV.LeaderPinColor.r = r
	FyrMM.SV.LeaderPinColor.g = g
	FyrMM.SV.LeaderPinColor.b = b
	FyrMM.SV.LeaderPinColor.a = a
end

function MM_SetLeaderDeadPinColor(r, g, b, a)
if r == nil then return end
	FyrMM.SV.LeaderDeadPinColor.r = r
	FyrMM.SV.LeaderDeadPinColor.g = g
	FyrMM.SV.LeaderDeadPinColor.b = b
	FyrMM.SV.LeaderDeadPinColor.a = a
end

function  MM_SetMemberPin(value)
if value == nil then return end
	FyrMM.SV.MemberPin = value
end

function MM_SetLockPosition(value)
	if value == nil then return end
	FyrMM.SV.LockPosition = value
	if FyrMM.SV.LockPosition then
		Fyr_MM_PinDown:SetHidden(true)
		Fyr_MM_PinUp:SetHidden(false)
	else
		Fyr_MM_PinDown:SetHidden(false)
		Fyr_MM_PinUp:SetHidden(true)
	end
	Fyr_MM:SetMovable(not value)
end

function  MM_SetMemberPinSize(value)
if value == nil then return end
	FyrMM.SV.MemberPinSize = value
end

function MM_SetMemberPinColor(r, g, b, a)
if r == nil then return end
	FyrMM.SV.MemberPinColor.r = r
	FyrMM.SV.MemberPinColor.g = g
	FyrMM.SV.MemberPinColor.b = b
	FyrMM.SV.MemberPinColor.a = a
end

function MM_SetMemberDeadPinColor(r, g, b, a)
if r == nil then return end
	FyrMM.SV.MemberDeadPinColor.r = r
	FyrMM.SV.MemberDeadPinColor.g = g
	FyrMM.SV.MemberDeadPinColor.b = b
	FyrMM.SV.MemberDeadPinColor.a = a
end

function MM_SetUndiscoveredPOIPinColor(r, g, b, a)
if r == nil then return end
	FyrMM.SV.UndiscoveredPOIPinColor.r = r
	FyrMM.SV.UndiscoveredPOIPinColor.g = g
	FyrMM.SV.UndiscoveredPOIPinColor.b = b
	FyrMM.SV.UndiscoveredPOIPinColor.a = a
end

function MM_SetMapSettings(Table)
	if Table == nil then return end
	FyrMM.SV.MapSettings = Table
end

function MM_SetZoneNameColor(r, g, b, a)
if r == nil then return end
	FyrMM.SV.ZoneNameColor.r = r
	FyrMM.SV.ZoneNameColor.g = g
	FyrMM.SV.ZoneNameColor.b = b
	FyrMM.SV.ZoneNameColor.a = a
	Fyr_MM_Zone:SetColor(r, g, b, a)
end

function MM_SetPositionColor(r, g, b, a)
if r == nil then return end
	FyrMM.SV.PositionColor.r = r
	FyrMM.SV.PositionColor.g = g
	FyrMM.SV.PositionColor.b = b
	FyrMM.SV.PositionColor.a = a
	Fyr_MM_Position:SetColor(r, g, b, a)
	Fyr_MM_SpeedLabel:SetColor(r, g, b, a)
end

function MM_SetCoordinatesLocation(value)
	if value == nil then return end
	FyrMM.SV.CorrdinatesLocation = value
	if FyrMM.SV.CorrdinatesLocation == "Free" then
			Fyr_MM_Coordinates:SetMovable(true)
			Fyr_MM_Coordinates:SetMouseEnabled(true)
			if FyrMM.SV.CoordinatesAnchor ~= nil then
				MM_SetCoordinatesAnchor(FyrMM.SV.CoordinatesAnchor)
			end
	else
		if FyrMM.SV.CorrdinatesLocation == "Top" then
			Fyr_MM_Coordinates:ClearAnchors()
			Fyr_MM_Coordinates:SetAnchor(TOP, Fyr_MM, TOP, 0, 0)
			Fyr_MM_Position:ClearAnchors()
			Fyr_MM_Position:SetAnchor(CENTER, Fyr_MM_Coordinates, CENTER, 0, 0)
			Fyr_MM_Position_Background:ClearAnchors()
			Fyr_MM_Position_Background:SetAnchor(CENTER, Fyr_MM_Position, CENTER, 0, 0)
			Fyr_MM_Coordinates:SetMovable(false)
		else
			Fyr_MM_Coordinates:ClearAnchors()
			Fyr_MM_Coordinates:SetAnchor(BOTTOM, Fyr_MM, BOTTOM, 0, 0)
			Fyr_MM_Position:ClearAnchors()
			Fyr_MM_Position:SetAnchor(CENTER, Fyr_MM_Coordinates, CENTER, 0, 0)
			Fyr_MM_Position_Background:ClearAnchors()
			Fyr_MM_Position_Background:SetAnchor(CENTER, Fyr_MM_Position, CENTER, 0, 0)
			Fyr_MM_Coordinates:SetMovable(false)
		end
	end
end

function MM_GetCoordinatesAnchor()
	return FyrMM.SV.CoordinatesAnchor[1], GetControl(FyrMM.SV.CoordinatesAnchor[2]), FyrMM.SV.CoordinatesAnchor[3], FyrMM.SV.CoordinatesAnchor[4], FyrMM.SV.CoordinatesAnchor[5]
end

function MM_SetCoordinatesAnchor(anchor)
	if anchor == nil then return end
	if not anchor[2] then anchor[2] = "Fyr_MM" end
	FyrMM.SV.CoordinatesAnchor = anchor
	if GetControl(FyrMM.SV.CoordinatesAnchor[2]) ~= nil and FyrMM.SV.CorrdinatesLocation == "Free" then
		Fyr_MM_Coordinates:ClearAnchors()
		Fyr_MM_Coordinates:SetAnchor(MM_GetCoordinatesAnchor())
	end
end

function MM_SetPositionBackground(value)
	if value == nil then return end
	FyrMM.SV.PositionBackground = value
	Fyr_MM_Position_Background:SetHidden(not FyrMM.SV.PositionBackground)
	Fyr_MM_Speed_Background:SetHidden(not FyrMM.SV.PositionBackground)
end

function MM_GetZoneFrameAnchor()
	return FyrMM.SV.ZoneFrameAnchor[1], GetControl(FyrMM.SV.ZoneFrameAnchor[2]), FyrMM.SV.ZoneFrameAnchor[3], FyrMM.SV.ZoneFrameAnchor[4], FyrMM.SV.ZoneFrameAnchor[5]
end

function MM_SetShowZoneBackground(value)
	if value == nil then return end
	FyrMM.SV.ShowZoneBackground = value
	Fyr_MM_Zone_Background:SetHidden(not FyrMM.SV.ShowZoneBackground)
end

function MM_SetZoneFrameLocationOption(value)
	if value == nil then return end
	FyrMM.SV.ZoneFrameLocationOption = value
	if FyrMM.SV.ZoneFrameLocationOption == "Default" then
		Fyr_MM_ZoneFrame:ClearAnchors()
		if FyrMM.SV.MenuDisabled then
			Fyr_MM_ZoneFrame:SetAnchor(TOP, Fyr_MM_Border, BOTTOM)
		else
			Fyr_MM_ZoneFrame:SetAnchor(TOP, Fyr_MM_Menu, BOTTOM, 0, -Fyr_MM_Menu:GetHeight()/5)
		end
		Fyr_MM_ZoneFrame:SetMovable(false)
	else
		if FyrMM.SV.ZoneFrameAnchor ~= nil then
			Fyr_MM_ZoneFrame:ClearAnchors()
			Fyr_MM_ZoneFrame:SetMovable(true)
			Fyr_MM_ZoneFrame:SetMouseEnabled(true)
			Fyr_MM_ZoneFrame:SetAnchor(MM_GetZoneFrameAnchor())
		end
	end 
end

function MM_SetZoneFrameAnchor(anchor)
	if anchor == nil then return end
	if not anchor[2] then anchor[2] = "Fyr_MM" end
	FyrMM.SV.ZoneFrameAnchor = anchor
	if GetControl(FyrMM.SV.ZoneFrameAnchor[2]) ~= nil and FyrMM.SV.ZoneFrameLocationOption == "Free" then
		Fyr_MM_ZoneFrame:ClearAnchors()
		Fyr_MM_ZoneFrame:SetAnchor(MM_GetZoneFrameAnchor())
	end
end

function MM_SetZoneFont()
	if FyrMM.SV.ZoneFont and FyrMM.SV.ZoneFontHeight and FyrMM.SV.ZoneFontStyle then
		Fyr_MM_Zone:SetFont(FyrMM.Fonts[FyrMM.SV.ZoneFont].."|"..tostring(FyrMM.SV.ZoneFontHeight).."|"..FyrMM.SV.ZoneFontStyle)
	end
end

function MM_SetPositionFont()
	if FyrMM.SV.PositionFont and FyrMM.SV.PositionHeight and FyrMM.SV.PositionFontStyle then
		Fyr_MM_Position:SetFont(FyrMM.Fonts[FyrMM.SV.PositionFont].."|"..tostring(FyrMM.SV.PositionHeight).."|"..FyrMM.SV.PositionFontStyle)
		Fyr_MM_SpeedLabel:SetFont(FyrMM.Fonts[FyrMM.SV.PositionFont].."|"..tostring(FyrMM.SV.PositionHeight).."|"..FyrMM.SV.PositionFontStyle)
	end
end

function MM_SetWheelTexture(value)
	if value == nil then return end
	FyrMM.SV.WheelTexture = value
	if FyrMM.WheelTextures[FyrMM.SV.WheelTexture] then
		Fyr_MM_Frame_Wheel:SetTexture(FyrMM.WheelTextures[FyrMM.SV.WheelTexture])
	end
end

function MM_SetMenuTexture(value)
	if value == nil then return end
	FyrMM.SV.MenuTexture = value
	if FyrMM.MenuTextures[FyrMM.SV.MenuTexture] then
		Fyr_MM_Frame_RoundMenu:SetTexture(FyrMM.MenuTextures[FyrMM.SV.MenuTexture].Round)
		Fyr_MM_Frame_SquareMenu:SetTexture(FyrMM.MenuTextures[FyrMM.SV.MenuTexture].Square)
	end
end

function MM_WheelModeDefaults()
	MM_SetWheelMap(true)
	MM_SetWheelTexture("Moosetrax Astro Wheel")
	MM_SetBorderPins(true)
	MM_SetBorderPinsOnlyAssisted(true)
	MM_SetRotateMap(false)
end

function MM_SquareModeDefaults()
	MM_SetWheelMap(false)
	MM_SetBorderPins(true)
	MM_SetBorderPinsOnlyAssisted(true)
	MM_SetRotateMap(false)
	FyrMM.UpdateMapTiles(true)
end

function MM_CreateDataTables()
FyrMM.Defaults = {
		["HideZoneLabel"] = false,
		["HideZoomLevel"] = false,
		["ShowBorder"] = true,
		["ClampedToScreen"] = true,
		["MapHeight"] = 280,
		["MapWidth"] = 280,
		["position"] = {
			["point"] = TOPLEFT,
			["relativePoint"] = TOPLEFT,
			["offsetX"] = 0,
			["offsetY"] = 0
		},
		["MapAlpha"] = 100,
		["Heading"] = "CAMERA",
		["PinScale"] = 75,
		["PinTooltips"] = true,
		["ShowPosition"] = true,
		["FastTravelEnabled"] = false,
		["HidePvPPins"] = false,
		["MouseWheel"] = true,
		["MapRefreshRate"] = 60,
		["PinRefreshRate"] = 1200,
		["ViewRefreshRate"] = 2500,
		["ZoneRefreshRate"] = 100,
		["KeepNetworkRefreshRate"] = 2000,
		["ShowClock"] = false,
		["ShowFPS"] = false,
		["InCombatState"] =true,
		["LeaderPin"] = "Default",
		["LeaderPinSize"] = 32,
		["ViewRangeFiltering"] = false,	
		["CustomPinViewRange"] = 250,
		["PPStyle"] = "Separate Player & Camera",
		["PPTextures"] = "ESO UI Worldmap",
		["MenuDisabled"] = false,
		["MenuTexture"] = "Default",
		["LeaderPinColor"] = {
			["r"] = 1,
			["g"] = 1,
			["b"] = 1,
			["a"] = 1
		},
		["LeaderDeadPinColor"] = {
			["r"] = 1,
			["g"] = 1,
			["b"] = 1,
			["a"] = 1
		},
		["MemberPin"] = "Default",
		["MemberPinSize"] = 32,
		["MemberPinColor"] = {
			["r"] = 1,
			["g"] = 1,
			["b"] = 1,
			["a"] = 1
		},
		["MemberDeadPinColor"] = {
			["r"] = 1,
			["g"] = 1,
			["b"] = 1,
			["a"] = 1
		},
		["ShowGroupLabels"] = false,
		["PositionColor"] = {
			["r"] = 0.996078431372549,
			["g"] = 0.92,
			["b"] = 0.3137254901960784,
			["a"] = 1
		},
		["ZoneNameColor"] = {
			["r"] = 1,
			["g"] = 1,
			["b"] = 1,
			["a"] = 1
		},
		["UndiscoveredPOIPinColor"] = {
			["r"] = .7,
			["g"] = .7,
			["b"] = .3,
			["a"] = .6
		},
		["MapTable"] = {},
		["MapStepping"] = 2,
		["ZoomIncrement"] = 1,
		["DefaultZoomLevel"] = 10,
		["InCombatAutoHide"] = false,
		["AfterCombatUnhideDelay"] = 5,
		["LockPosition"] = false,
		["UseOriginalAPI"] = true,
		["ShowUnexploredPins"] = true,
		["RotateMap"] = false,
		["BorderPins"] = false,
		["BorderPinsWaypoint"] = false,
		["BorderPinsBank"] = false,
		["BorderPinsStables"] = false,
		["BorderWayshrine"] = false,
		["BorderTreasures"] = false,
		["BorderQuestGivers"] = false,
		["BorderCrafting"] = false,
		["WheelMap"] = false,
		["CorrdinatesLocation"] = "Top",
		["ZoneNameContents"] = "Classic (Map only)",
		["ZoneFontHeight"] = 18,
		["ZoneFont"] = "Univers 57",
		["ZoneFontStyle"] = "soft-shadow-thick",
		["ZoneFrameLocationOption"] = "Default",
		["PositionHeight"] = 18,
		["PositionFont"] = "Univers 57",
		["StartupInfo"] = false,
		["PositionFontStyle"] = "soft-shadow-thick",
		["Siege"] = false,
		["ShowSpeed"] = false,
		["SpeedUnit"] = "ft/s",
		["ChunkSize"] = 200,
		["ChunkDelay"] = 10,
		["WorldMapRefresh"] = true,}

		FyrMM.WheelTextures = {
		["Deathangel RMM Wheel"] = "MiniMap/Textures/wheel.dds",
		["Moosetrax Normal Wheel"] = "MiniMap/Textures/MNormalWheel.dds",
		["Moosetrax Normal Lense Wheel"] = "MiniMap/Textures/MNormalLense1Wheel.dds",
		["Moosetrax Astro Wheel"] = "MiniMap/Textures/MAstroWheel.dds",
		["Moosetrax Astro Lense Wheel"] = "MiniMap/Textures/MAstroLense1Wheel.dds",
		["Vixion Black"] = "MiniMap/Textures/Vixion_Black.dds",
		["Vixion Black Flat"] = "MiniMap/Textures/Vixion_BlackFlat.dds",
		["Vixion Black Gold"] = "MiniMap/Textures/Vixion_BlackGold.dds",
		["Vixion Flat"] = "MiniMap/Textures/Vixion_Flat.dds",
		["Vixion Gold"] = "MiniMap/Textures/Vixion_Gold.dds",
		["Vixion Normal"] = "MiniMap/Textures/Vixion_Normal.dds",
		["Vixion Gloss"] = "MiniMap/Textures/Vixion_Gloss.dds",
		["Vixion Shiny"] = "MiniMap/Textures/Vixion_Shiny.dds",
		["Vixion Shiny Gold"] = "MiniMap/Textures/Vixion_ShinyGold.dds",
		["Vixion Smooth"] = "MiniMap/Textures/Vixion_Smooth.dds",
		["Vixion Simple"] = "MiniMap/Textures/Vixion_Simple.dds",
		["Vixion Optic"] = "MiniMap/Textures/Vixion_Optic.dds",
		["Vixion Noble"] = "MiniMap/Textures/Vixion_Noble.dds",
		["Vixion Noble Gold"] = "MiniMap/Textures/Vixion_Noble_Gold.dds",
		["Rhymer Gold"] = "MiniMap/Textures/Rhymer_Gold.dds",
		["Rhymer Steel"] = "MiniMap/Textures/Rhymer_Steel.dds",
		["Rhymer Wood"] = "MiniMap/Textures/Rhymer_Wood.dds", }

		FyrMM.WheelTextureList = {"Deathangel RMM Wheel", "Moosetrax Normal Wheel", "Moosetrax Normal Lense Wheel", "Moosetrax Astro Wheel", "Moosetrax Astro Lense Wheel", "Vixion Black", "Vixion Black Flat", "Vixion Black Gold", "Vixion Flat", "Vixion Gold", "Vixion Normal", "Vixion Gloss", "Vixion Shiny", "Vixion Shiny Gold", "Vixion Smooth", "Vixion Simple", "Vixion Optic", "Vixion Noble", "Vixion Noble Gold", "Rhymer Gold", "Rhymer Steel", "Rhymer Wood", }

		FyrMM.FontList = {"Arial Narrow","Consolas","ESO Cartographer","Fontin Bold","Fontin Italic","Fontin Regular","Fontin SmallCaps","ProseAntique","Skyrim Handwritten","Trajan Pro","Univers 55","Univers 57","Univers 67",}
		FyrMM.FontStyles = {"normal", "outline", "thick-outline", "shadow", "soft-shadow-thick", "soft-shadow-thin"}
		FyrMM.MenuTextures = {	["Default"] = {["Square"] = "MiniMap/Textures/MiniMap_SquareMenuFrame_Default.dds", ["Round"] = "MiniMap/Textures/MiniMap_RoundMenuFrame_Default.dds",},
								["Black"] = {["Square"] = "MiniMap/Textures/MiniMap_SquareMenuFrame_Black.dds", ["Round"] = "MiniMap/Textures/MiniMap_RoundMenuFrame_Black.dds",},
								["Red"] = {["Square"] = "MiniMap/Textures/MiniMap_SquareMenuFrame_Red.dds", ["Round"] = "MiniMap/Textures/MiniMap_RoundMenuFrame_Red.dds",},
								["Gold"] = {["Square"] = "MiniMap/Textures/MiniMap_SquareMenuFrame_Gold.dds", ["Round"] = "MiniMap/Textures/MiniMap_RoundMenuFrame_Gold.dds",},}
		FyrMM.MenuTextureList = {"Default", "Black", "Red", "Gold",}
		FyrMM.CSProviders = {"Clothing", "Schneidertisch", "couture", "Blacksmithing", "Schmiedestelle", "forge", "Woodworking", "Schreinerbank", "bois", "Alchemy", "Alchemietisch", "d'alchimie", "Enchanting", "Verzauberungstisch", "d'enchantement",}
		FyrMM.SpeedUnits = {"ft/s", "m/s", "%",}
		FyrMM.MapTable = {
						["abagarlas_base_0"] = {["MapId"] = 1, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 454.57, },
						["aba-loria_base_0"] = {["MapId"] = 2, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 368.6, },
						["abamath_base_0"] = {["MapId"] = 3, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 354.00, },
						["abandonedmine_base_0"] = {["MapId"] = 4, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["aetherianarchivebottom_base_0"] = {["MapId"] = 5, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["aetherianarchiveend_base_0"] = {["MapId"] = 6, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["aetherianarchiveislanda_base_0"] = {["MapId"] = 7, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["aetherianarchiveislandb_base_0"] = {["MapId"] = 8, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["aetherianarchiveislandc_base_0"] = {["MapId"] = 9, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["aetherianarchivemiddle_base_0"] = {["MapId"] = 10, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["albeceansea_base_0"] = {["MapId"] = 11, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 209.10, },
						["alcairecastle_base_0"] = {["MapId"] = 12, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 582.61, },
						["aldcroft_base_0"] = {["MapId"] = 13, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 666.22, },
						["aldunz_base_0"] = {["MapId"] = 14, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 495.25, },
						["alikr_base_0"] = {["MapId"] = 15, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 4680.28, },
						["alkiroutlawrefuge_base_0"] = {["MapId"] = 16, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["altencorimont_base_0"] = {["MapId"] = 17, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 812.48, },
						["ancientcarzogsdemise_base_0"] = {["MapId"] = 18, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1370.66, },
						["angofssanctum_base_0"] = {["MapId"] = 19, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 442.76, },
						["aphrenshold_base_0"] = {["MapId"] = 20, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 340.94, },
						["apocryphasgate_base_0"] = {["MapId"] = 21, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["apothacarium_base_0"] = {["MapId"] = 22, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 147.52, },
						["arcwindpoint_base_0"] = {["MapId"] = 23, ["TextureSize"] = 512, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 505.55, },
						["arenthia_base_0"] = {["MapId"] = 24, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 833.38, },
						["arxcorinium_base_0"] = {["MapId"] = 25, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["ashaba_base_0"] = {["MapId"] = 26, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 644.59, },
						["ashmountain_base_0"] = {["MapId"] = 27, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 421.37, },
						["atanazruins_base_0"] = {["MapId"] = 28, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 348.76, },
						["ateliertwicebornstar_base_0"] = {["MapId"] = 29, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 98.02, },
						["auridon_base_0"] = {["MapId"] = 30, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 5350.40, },
						["auridonoutlawrefuge_base_0"] = {["MapId"] = 31, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 206.7, },
						["ava_whole_0"] = {["MapId"] = 32, ["TextureSize"] = 1024, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 13065.38, },
						["avancheznel_base_0"] = {["MapId"] = 33, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 266.65, },
						["baandaritradingpost_base_0"] = {["MapId"] = 34, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 637.37, },
						["badmanscave_base_0"] = {["MapId"] = 35, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 56.13, },
						["badmansend_base_0"] = {["MapId"] = 36, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 29.11, },
						["badmansstart_base_0"] = {["MapId"] = 37, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 29.10, },
						["balamath_base_0"] = {["MapId"] = 38, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 525.7, },
						["balamathairmonarchcham_base_0"] = {["MapId"] = 39, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["balamathlibrary_base_0"] = {["MapId"] = 40, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["balfoyen_base_0"] = {["MapId"] = 41, ["TextureSize"] = 1024, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1696.71, },
						["bangkorai_base_0"] = {["MapId"] = 42, ["TextureSize"] = 1024, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 4344.60, },
						["bangkoraigarrisonl_base_0"] = {["MapId"] = 43, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 211.60, },
						["bangkoraigarrisonsewer_base_0"] = {["MapId"] = 44, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 303.03, },
						["bangkoraioutlawrefuge_base_0"] = {["MapId"] = 45, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 17.87, },
						["barkbitecave_base_0"] = {["MapId"] = 46, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 226.08, },
						["barkbitemine_base_0"] = {["MapId"] = 47, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 181.57, },
						["barrowtrench_base_0"] = {["MapId"] = 48, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 391.2, },
						["bearclawmine_base_0"] = {["MapId"] = 49, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 317.9, },
						["belkarth_base_0"] = {["MapId"] = 50, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 658.62, },
						["bergama_base_0"] = {["MapId"] = 51, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 714.55, },
						["betnihk_base_0"] = {["MapId"] = 52, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 2232.00, },
						["bewan_base_0"] = {["MapId"] = 53, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["bisnensel_base_0"] = {["MapId"] = 54, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 725.63, },
						["blackforge_base_0"] = {["MapId"] = 55, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1081.03, },
						["blackhearthavenarea1_base_0"] = {["MapId"] = 56, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blackhearthavenarea2_base_0"] = {["MapId"] = 57, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blackhearthavenarea3_base_0"] = {["MapId"] = 58, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blackhearthavenarea4_base_0"] = {["MapId"] = 59, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blackvineruins_base_0"] = {["MapId"] = 60, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 394.6, },
						["bleakrock_base_0"] = {["MapId"] = 61, ["TextureSize"] = 512, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1963.40, },
						["bleakrockvillage_base_0"] = {["MapId"] = 62, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 594.08, },
						["blessedcrucible1_base_0"] = {["MapId"] = 63, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blessedcrucible2_base_0"] = {["MapId"] = 64, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blessedcrucible3_base_0"] = {["MapId"] = 65, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blessedcrucible4_base_0"] = {["MapId"] = 66, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blessedcrucible5_base_0"] = {["MapId"] = 67, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blessedcrucible6_base_0"] = {["MapId"] = 68, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blessedcrucible7_base_0"] = {["MapId"] = 69, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["blisslower_base_0"] = {["MapId"] = 70, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 111.60, },
						["blisstop_base_0"] = {["MapId"] = 71, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 279.84, },
						["bloodmaynecave_base_0"] = {["MapId"] = 72, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["bloodmatronscryptgroup_base_0"] = {["MapId"] = 73, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["bloodmatronscryptsingle_base_0"] = {["MapId"] = 74, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["bloodthornlair_base_0"] = {["MapId"] = 75, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 475.62, },
						["boneorchard_base_0"] = {["MapId"] = 76, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["bonesnapruins_base_0"] = {["MapId"] = 77, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 100, },
						["bonesnapruinssecret_base_0"] = {["MapId"] = 78, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 607.36, },
						["bonestrewncrest_base_0"] = {["MapId"] = 79, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 370.91, },
						["brackenleaf_base_0"] = {["MapId"] = 80, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["breaghafinlower_base_0"] = {["MapId"] = 81, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 305.68, },
						["breaghafinupper_base_0"] = {["MapId"] = 82, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 262.58, },
						["breakneckcave_base_0"] = {["MapId"] = 83, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["brokenhelm_base_0"] = {["MapId"] = 84, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 374.3, },
						["brokentuskcave_base_0"] = {["MapId"] = 85, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 270.1, },
						["bthanual_base_0"] = {["MapId"] = 86, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 425.72, },
						["bthzark_base_0"] = {["MapId"] = 87, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 298.17, },
						["buraniim_base_0"] = {["MapId"] = 88, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["burriedsands_base_0"] = {["MapId"] = 89, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 567.44, },
						["burrootkwamamine_base_0"] = {["MapId"] = 90, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 389.28, },
						["campgushnukbur_base_0"] = {["MapId"] = 91, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 186.73, },
						["capstonecave_base_0"] = {["MapId"] = 92, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["caracdena_base_0"] = {["MapId"] = 93, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 333.21, },
						["carzogsdemise_base_0"] = {["MapId"] = 94, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1370.56, },
						["castleoftheworm1_base_0"] = {["MapId"] = 95, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 420.61, },
						["castleoftheworm2_base_0"] = {["MapId"] = 96, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 427.15, },
						["castleoftheworm3_base_0"] = {["MapId"] = 97, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 240.78, },
						["castleoftheworm4_base_0"] = {["MapId"] = 98, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 936.59, },
						["castleoftheworm5_base_0"] = {["MapId"] = 99, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["cathbedraud_base_0"] = {["MapId"] = 100, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 446.74, },
						["catseyequay_base_0"] = {["MapId"] = 101, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["caveofbrokensails_base_0"] = {["MapId"] = 102, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["caveoftrophies_base_0"] = {["MapId"] = 103, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 449.02, },
						["caveoftrophiesupper_base_0"] = {["MapId"] = 104, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["charredridge_base_0"] = {["MapId"] = 105, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 681.72, },
						["chateaumasterbedroom_base_0"] = {["MapId"] = 106, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 163.31, },
						["chateauravenousrodent_base_0"] = {["MapId"] = 107, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 818.81, },
						["cheesemongershollow_base_0"] = {["MapId"] = 108, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["chidmoskaruins_base_0"] = {["MapId"] = 109, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 361.63, },
						["chiselshriek_base_0"] = {["MapId"] = 110, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 423.89, },
						["circusofcheerfulslaughter_base_0"] = {["MapId"] = 111, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 770.57, },
						["cityofashboss_base_0"] = {["MapId"] = 112, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["cityofashmain_base_0"] = {["MapId"] = 113, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["clawsstrike_base_0"] = {["MapId"] = 114, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 430.70, },
						["coldharbour_base_0"] = {["MapId"] = 115, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 5621.54, },
						["coldrockdiggings_base_0"] = {["MapId"] = 116, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 413.40, },
						["coloredrooms_base_0"] = {["MapId"] = 117, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 190.74, },
						["coloviancrossing_base_0"] = {["MapId"] = 118, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["coralheartchamber_base_0"] = {["MapId"] = 119, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 218.19, },
						["cormountprison_base_0"] = {["MapId"] = 120, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 81.01, },
						["coromount_base_0"] = {["MapId"] = 121, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 284.86, },
						["corpsegarden_base_0"] = {["MapId"] = 122, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 294.52, },
						["courtofcontempt_base_0"] = {["MapId"] = 123, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 151.18, },
						["crackedwoodcave_base_0"] = {["MapId"] = 124, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["craglorn_base_0"] = {["MapId"] = 125, ["TextureSize"] = 256, ["TileNum"] = 49, ["Dx"] = 7, ["Dy"] = 7, ["MapSize"] = 4681.72, },
						["craglorn_dragonstar_base_0"] = {["MapId"] = 126, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 458.46, },
						["craglornoutlawrefuge_base_0"] = {["MapId"] = 127, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 194.75, },
						["cragwallow_base_0"] = {["MapId"] = 128, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 326.45, },
						["crestshademine_base_0"] = {["MapId"] = 129, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 359.91, },
						["crgwamasucave_base_0"] = {["MapId"] = 130, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 262.62, },
						["crimsoncove_base_0"] = {["MapId"] = 131, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 572.09, },
						["cryptofhearts_base_0"] = {["MapId"] = 132, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 603.44, },
						["cryptofheartsheroic_base_0"] = {["MapId"] = 133, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 603.41, },
						["cryptoftarishzi_base_0"] = {["MapId"] = 134, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 350.88, },
						["cryptoftarishzi2_base_0"] = {["MapId"] = 135, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["cryptoftarishzizone_base_0"] = {["MapId"] = 136, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 798.71, },
						["cryptoftheexiles_base_0"] = {["MapId"] = 137, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 351.58, },
						["cryptwatchfort_base_0"] = {["MapId"] = 138, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 464.69, },
						["crosswych_base_0"] = {["MapId"] = 139, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 828.17, },
						["crosswychmine_base_0"] = {["MapId"] = 140, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 539.46, },
						["crowswood_base_0"] = {["MapId"] = 141, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 695.00, },
						["crowswooddungeon_base_0"] = {["MapId"] = 142, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 145.18, },
						["daggerfall_base_0"] = {["MapId"] = 143, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1039.50, },
						["darkshadecaverns_base_0"] = {["MapId"] = 144, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 651.57, },
						["darkshadecavernsheroic_base_0"] = {["MapId"] = 145, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["davonswatch_base_0"] = {["MapId"] = 146, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 956.10, },
						["davonswatchcrypt_base_0"] = {["MapId"] = 147, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 305.61, },
						["deadmansdrop_base_0"] = {["MapId"] = 148, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 427.16, },
						["deepcragden_base_0"] = {["MapId"] = 149, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 459.67, },
						["delsclaim_base_0"] = {["MapId"] = 150, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["depravedgrotto_base_0"] = {["MapId"] = 151, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 548.44, },
						["deshaan_base_0"] = {["MapId"] = 152, ["TextureSize"] = 512, ["TileNum"] = 16, ["Dx"] = 4, ["Dy"] = 4, ["MapSize"] = 5347.73, },
						["desolatecave_base_0"] = {["MapId"] = 153, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 304.17, },
						["desolationsend_base_0"] = {["MapId"] = 154, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["despair_base_0"] = {["MapId"] = 155, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 223.69, },
						["dessicatedcave_base_0"] = {["MapId"] = 156, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 400.60, },
						["dhalmora_base_0"] = {["MapId"] = 157, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 402.83, },
						["direfrostkeep_base_0"] = {["MapId"] = 158, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["direfrostkeepheroic_base_0"] = {["MapId"] = 159, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["direfrostkeepsummit_base_0"] = {["MapId"] = 160, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["divadschagrinmine_base_0"] = {["MapId"] = 161, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 396.74, },
						["dokrintemple_base_0"] = {["MapId"] = 162, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 459.82, },
						["doomcragground_base_0"] = {["MapId"] = 163, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 161.98, },
						["doomcragmiddle_base_0"] = {["MapId"] = 164, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 152.82, },
						["doomcragshroudedpass_base_0"] = {["MapId"] = 165, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 414.06, },
						["doomcragtop_base_0"] = {["MapId"] = 166, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 114.97, },
						["dourstonevault_base_0"] = {["MapId"] = 167, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena01_base_0"] = {["MapId"] = 168, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena02_base_0"] = {["MapId"] = 169, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena03_base_0"] = {["MapId"] = 170, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena04_base_0"] = {["MapId"] = 171, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena05_base_0"] = {["MapId"] = 172, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena06_base_0"] = {["MapId"] = 173, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena07_base_0"] = {["MapId"] = 174, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena08_base_0"] = {["MapId"] = 175, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena09_base_0"] = {["MapId"] = 176, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena09crypt_base_0"] = {["MapId"] = 177, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena10_base_0"] = {["MapId"] = 178, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararenavault_base_0"] = {["MapId"] = 179, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dresankeep_base_0"] = {["MapId"] = 180, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 265.13, },
						["dune_base_0"] = {["MapId"] = 181, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 875.18, },
						["east_hut_portal_cave_base_0"] = {["MapId"] = 182, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 168.46, },
						["eastelsweyrgate_base_0"] = {["MapId"] = 183, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 806.67, },
						["eastmarch_base_0"] = {["MapId"] = 184, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 5350.40, },
						["eastmarchrefuge_base_0"] = {["MapId"] = 185, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 247.6, },
						["eboncrypt_base_0"] = {["MapId"] = 186, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 302.43, },
						["ebonheart_base_0"] = {["MapId"] = 187, ["TextureSize"] = 512, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1007.05, },
						["ebonmeretower_base_0"] = {["MapId"] = 188, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 394.75, },
						["echocave_base_0"] = {["MapId"] = 189, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["edraldundercroft_base_0"] = {["MapId"] = 190, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 401.01, },
						["edraldundercroftdomed_base_0"] = {["MapId"] = 191, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 270.43, },
						["eidolonshollow2_base_0"] = {["MapId"] = 192, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 433.75, },
						["eyeschamber_base_0"] = {["MapId"] = 193, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["eyevea_base_0"] = {["MapId"] = 194, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 837.31, },
						["eldenhollow_base_0"] = {["MapId"] = 195, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["eldenhollowheroic1_base_0"] = {["MapId"] = 196, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["eldenhollowheroic2_base_0"] = {["MapId"] = 197, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["eldenrootcrafting_base_0"] = {["MapId"] = 198, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 244.76, },
						["eldenrootfightersguildown_base_0"] = {["MapId"] = 199, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 244.78, },
						["eldenrootfightersguildup_base_0"] = {["MapId"] = 200, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 244.78, },
						["eldenrootgroundfloor_base_0"] = {["MapId"] = 201, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1013.61, },
						["eldenrootmagesguild_base_0"] = {["MapId"] = 202, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 244.71, },
						["eldenrootmagesguilddown_base_0"] = {["MapId"] = 203, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 244.71, },
						["eldenrootservices_base_0"] = {["MapId"] = 204, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 292.61, },
						["eldenrootthroneroom_base_0"] = {["MapId"] = 205, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 244.84, },
						["elinhirmagevision_base_0"] = {["MapId"] = 206, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 332.32, },
						["elinhirsewerworks_base_0"] = {["MapId"] = 207, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["emberflintmine_base_0"] = {["MapId"] = 208, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 504.4, },
						["emericsdquagmireportion_base_0"] = {["MapId"] = 209, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 158.05, },
						["emericsdream_base_0"] = {["MapId"] = 210, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 632.43, },
						["emericsdreampart2_base_0"] = {["MapId"] = 211, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["enduum_base_0"] = {["MapId"] = 212, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 60.29, },
						["entilasfolly_base_0"] = {["MapId"] = 213, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["erokii_base_0"] = {["MapId"] = 214, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 304.92, },
						["evermore_base_0"] = {["MapId"] = 215, ["TextureSize"] = 512, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 749.78, },
						["exarchsstronghold_base_0"] = {["MapId"] = 216, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 564.7, },
						["ezduiin_base_0"] = {["MapId"] = 217, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["falinesticave_base_0"] = {["MapId"] = 218, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 327.96, },
						["fallowstonevault_base_0"] = {["MapId"] = 219, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 278.59, },
						["faltoniasmine_base_0"] = {["MapId"] = 220, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 211.65, },
						["farangelsdelve_base_0"] = {["MapId"] = 221, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 240.10, },
						["fardirsfolly_base_0"] = {["MapId"] = 222, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 338.76, },
						["fearfang_base_0"] = {["MapId"] = 223, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 450.67, },
						["fevered_mews_base_0"] = {["MapId"] = 224, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 310.86, },
						["fevered_mews_subzone_base_0"] = {["MapId"] = 225, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 127.99, },
						["firsthold_base_0"] = {["MapId"] = 226, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 876.52, },
						["flyleafcatacombs_base_0"] = {["MapId"] = 227, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 241.11, },
						["forelhost_base_0"] = {["MapId"] = 228, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 202.52, },
						["forgottencrypts_base_0"] = {["MapId"] = 229, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 444.08, },
						["fortamol_base_0"] = {["MapId"] = 230, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 658.29, },
						["fortarand_base_0"] = {["MapId"] = 231, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 274.37, },
						["fortgreenwall_base_0"] = {["MapId"] = 232, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 243.53, },
						["fortmorvunskar_base_0"] = {["MapId"] = 233, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 444.24, },
						["fortsphinxmoth_base_0"] = {["MapId"] = 234, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 591.73, },
						["fortvirakruin_base_0"] = {["MapId"] = 235, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 212.86, },
						["foundryofwoe_base_0"] = {["MapId"] = 236, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["frostmonarchlair_base_0"] = {["MapId"] = 237, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["fungalgrotto_base_0"] = {["MapId"] = 238, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 653.14, },
						["fungalgrottosecretroom_base_0"] = {["MapId"] = 239, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["gandranen_base_0"] = {["MapId"] = 240, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 375.25, },
						["gladeofthedivineasakala_base_0"] = {["MapId"] = 241, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["gladeofthedivineshivering_base_0"] = {["MapId"] = 242, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["gladeofthedivinevuldngrav_base_0"] = {["MapId"] = 243, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["gladiatorsassembly_base_0"] = {["MapId"] = 244, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["glenumbra_base_0"] = {["MapId"] = 245, ["TextureSize"] = 1024, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 4963.78, },
						["glenumbraoutlawrefuge_base_0"] = {["MapId"] = 246, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["goblinminesend_base_0"] = {["MapId"] = 247, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 339.59, },
						["goblinminesstart_base_0"] = {["MapId"] = 248, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 217.69, },
						["godrunsdream_base_0"] = {["MapId"] = 249, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 31.14, },
						["grahtwood_base_0"] = {["MapId"] = 250, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 4624.12, },
						["grahtwoodoutlawrefuge_base_0"] = {["MapId"] = 251, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 204.40, },
						["greatshackle1_base_0"] = {["MapId"] = 252, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1004.03, },
						["greenhillcatacombs_base_0"] = {["MapId"] = 253, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 351.39, },
						["greenshade_base_0"] = {["MapId"] = 254, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 3916.12, },
						["greymire_base_0"] = {["MapId"] = 255, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["grundasgatehousemain_base_0"] = {["MapId"] = 256, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 457.11, },
						["grundasgatehouseroom_base_0"] = {["MapId"] = 257, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 457.11, },
						["guardiansorbit_base_0"] = {["MapId"] = 258, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["gurzagsmine_base_0"] = {["MapId"] = 259, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 389.25, },
						["haddock_base_0"] = {["MapId"] = 260, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 435.41, },
						["haynotecave_base_0"] = {["MapId"] = 261, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["hajuxith_base_0"] = {["MapId"] = 262, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 407.1, },
						["halcyonlake_base_0"] = {["MapId"] = 263, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["hallinsstand_base_0"] = {["MapId"] = 264, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 943.16, },
						["hallofheroes_base_0"] = {["MapId"] = 265, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 672.22, },
						["hallofthedead_base_0"] = {["MapId"] = 266, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 659.56, },
						["halloftrials_base_0"] = {["MapId"] = 267, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 229.93, },
						["hallsofichor_base_0"] = {["MapId"] = 268, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 540.78, },
						["hallsofsubmission_base_0"] = {["MapId"] = 269, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["hallsoftorment1_base_0"] = {["MapId"] = 270, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 608.71, },
						["harridanslair_base_0"] = {["MapId"] = 271, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 332.3, },
						["haven_base_0"] = {["MapId"] = 272, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 943.10, },
						["havensewers_base_0"] = {["MapId"] = 273, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["hazikslair_base_0"] = {["MapId"] = 274, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["heartsgrief1_base_0"] = {["MapId"] = 275, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 696.21, },
						["heartsgrief2_base_0"] = {["MapId"] = 276, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 502.88, },
						["heartsgrief3_base_0"] = {["MapId"] = 277, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 722.36, },
						["hectahamegrottoarboretum_base_0"] = {["MapId"] = 278, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 880.74, },
						["hectahamegrottoarmory_base_0"] = {["MapId"] = 279, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 880.74, },
						["hectahamegrottomain_base_0"] = {["MapId"] = 280, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 880.74, },
						["hectahamegrottoritual_base_0"] = {["MapId"] = 281, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 880.74, },
						["hectahamegrottovalenheart_base_0"] = {["MapId"] = 282, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 595.83, },
						["heimlynkeepreliquary_base_0"] = {["MapId"] = 283, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 352.66, },
						["helracitadel_base_0"] = {["MapId"] = 284, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1, },
						["helracitadelentry_base_0"] = {["MapId"] = 285, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["helracitadelhallofwarrior_base_0"] = {["MapId"] = 286, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["highrock_base_0"] = {["MapId"] = 287, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["hightidehollow_base_0"] = {["MapId"] = 288, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 361.2, },
						["hildunessecretrefuge_base_0"] = {["MapId"] = 289, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 357.30, },
						["hircineshaunt_base_0"] = {["MapId"] = 290, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 670.09, },
						["hircineswoods_base_0"] = {["MapId"] = 291, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["hoarfrost_base_0"] = {["MapId"] = 292, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 394.53, },
						["hoarvorpit_base_0"] = {["MapId"] = 293, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 434.33, },
						["hollowcity_base_0"] = {["MapId"] = 294, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 658.66, },
						["hollowlair_base_0"] = {["MapId"] = 295, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 511.94, },
						["housedrescrypts_base_0"] = {["MapId"] = 296, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 373.70, },
						["howlingsepulcherscave_base_0"] = {["MapId"] = 297, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 191.62, },
						["howlingsepulchersoverlan_base_0"] = {["MapId"] = 298, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 673.18, },
						["howlingsepulchersoverland_base_0"] = {["MapId"] = 299, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 673.18, },
						["howlingsepulchersscrying_base_0"] = {["MapId"] = 300, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["hozzinsfolley_base_0"] = {["MapId"] = 301, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 352.11, },
						["iccoldharbonobels1_base_0"] = {["MapId"] = 302, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["iccoldharbonobels2_base_0"] = {["MapId"] = 303, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["iccoldharbonobels3_base_0"] = {["MapId"] = 304, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["iccoldharboraboretum1_base_0"] = {["MapId"] = 305, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["iccoldharboraboretum2_base_0"] = {["MapId"] = 306, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["iccoldharboraboretum3_base_0"] = {["MapId"] = 307, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["iccoldharbormarket1_base_0"] = {["MapId"] = 308, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["iccoldharbormarket2_base_0"] = {["MapId"] = 309, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["iccoldharbormarket3_base_0"] = {["MapId"] = 310, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["icehammersvault_base_0"] = {["MapId"] = 311, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 405.7, },
						["ilessantower_base_0"] = {["MapId"] = 312, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 371.59, },
						["iliathtempletunnels_base_0"] = {["MapId"] = 313, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 297.85, },
						["ilmyris_base_0"] = {["MapId"] = 314, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 467.68, },
						["ilthagsundertower_base_0"] = {["MapId"] = 315, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 238.9, },
						["ilthagsundertower02_base_0"] = {["MapId"] = 316, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 247.45, },
						["imperialcity_base_0"] = {["MapId"] = 317, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialprisondistrictdun_base_0"] = {["MapId"] = 318, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialprisondunint01_base_0"] = {["MapId"] = 319, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialprisondunint02_base_0"] = {["MapId"] = 320, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialprisondunint03_base_0"] = {["MapId"] = 321, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialprisondunint04_base_0"] = {["MapId"] = 322, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialsewers_base_0"] = {["MapId"] = 323, ["TextureSize"] = 512, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewersald1_base_0"] = {["MapId"] = 324, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialsewersald2_base_0"] = {["MapId"] = 325, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialsewersdagger1_base_0"] = {["MapId"] = 326, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialsewersdagger2_base_0"] = {["MapId"] = 327, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialsewersebon1_base_0"] = {["MapId"] = 328, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialsewersebon2_base_0"] = {["MapId"] = 329, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialsewershub_base_0"] = {["MapId"] = 330, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialundergroundpart1_base_0"] = {["MapId"] = 331, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 154.07, },
						["imperialundergroundpart2_base_0"] = {["MapId"] = 332, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 375.94, },
						["imperviousvault_base_0"] = {["MapId"] = 333, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 265.16, },
						["innerseaarmature_base_0"] = {["MapId"] = 334, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 482.02, },
						["innertanzelwil_base_0"] = {["MapId"] = 335, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["islesoftorment_base_0"] = {["MapId"] = 336, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 829.9, },
						["yldzuun_base_0"] = {["MapId"] = 337, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 405.33, },
						["yokudanpalace_base_0"] = {["MapId"] = 338, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 209.61, },
						["yokudanpalace02_base_0"] = {["MapId"] = 339, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 207.30, },
						["jaggerjaw_base_0"] = {["MapId"] = 340, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 472.87, },
						["jodeplane_base_0"] = {["MapId"] = 341, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 722.36, },
						["jodeslight_base_0"] = {["MapId"] = 342, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 436.72, },
						["kardala_base_0"] = {["MapId"] = 343, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 451.72, },
						["karthdar_base_0"] = {["MapId"] = 344, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 265.18, },
						["khajrawlith_base_0"] = {["MapId"] = 345, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 278.29, },
						["khenarthisroost_base_0"] = {["MapId"] = 346, ["TextureSize"] = 1024, ["TileNum"] = 5, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 1, },
						["kingscrest_base_0"] = {["MapId"] = 347, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["koeglinmine_base_0"] = {["MapId"] = 348, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 380.7, },
						["koeglinvillage_base_0"] = {["MapId"] = 349, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 551.24, },
						["kozanset_base_0"] = {["MapId"] = 350, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 674.06, },
						["kragenmoor_base_0"] = {["MapId"] = 351, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 799.37, },
						["kulatimines-a_base_0"] = {["MapId"] = 352, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 268.08, },
						["kulatimines-b_base_0"] = {["MapId"] = 353, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 148.14, },
						["kunasdelve_base_0"] = {["MapId"] = 354, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 417.47, },
						["kwamacolony_base_0"] = {["MapId"] = 355, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 304.62, },
						["laeloriaruins_base_0"] = {["MapId"] = 356, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 347.49, },
						["lair_base_0"] = {["MapId"] = 357, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 146.71, },
						["lastresortbarrow_base_0"] = {["MapId"] = 358, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 555.47, },
						["ldtestworld_base_0"] = {["MapId"] = 359, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["libraryofdusk_base_0"] = {["MapId"] = 360, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 330.72, },
						["lightlesscell_base_0"] = {["MapId"] = 361, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 740.25, },
						["lightlessoubliette_base_0"] = {["MapId"] = 362, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 586.39, },
						["lightlessoubliettelava_base_0"] = {["MapId"] = 363, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 586.39, },
						["lionsden_hiddentunnel_base_0"] = {["MapId"] = 364, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 169.6, },
						["lipsandtarn_base_0"] = {["MapId"] = 365, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["loriasel_base_0"] = {["MapId"] = 366, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 668.78, },
						["loriasellowerlevel_base_0"] = {["MapId"] = 367, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["lorkrataruinsa_base_0"] = {["MapId"] = 368, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 329.39, },
						["lorkrataruinsb_base_0"] = {["MapId"] = 369, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 329.39, },
						["lostcity_base_0"] = {["MapId"] = 370, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 939.18, },
						["lostknifecave_base_0"] = {["MapId"] = 371, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 435.04, },
						["lostprospect_base_0"] = {["MapId"] = 372, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 231.25, },
						["lostprospect2_base_0"] = {["MapId"] = 373, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 310.86, },
						["lothna_base_0"] = {["MapId"] = 374, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 488.38, },
						["lowerbthanuel_base_0"] = {["MapId"] = 375, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 337.66, },
						["magistratesbasement_base_0"] = {["MapId"] = 376, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 121.46, },
						["malabaltor_base_0"] = {["MapId"] = 377, ["TextureSize"] = 512, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 4327.08, },
						["malabaltoroutlawrefuge_base_0"] = {["MapId"] = 378, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 258.57, },
						["malsorrastomb_base_0"] = {["MapId"] = 379, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 437.23, },
						["manorofrevelrycave_base_0"] = {["MapId"] = 380, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 286.01, },
						["manorofrevelryint01_base_0"] = {["MapId"] = 381, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 128.29, },
						["manorofrevelryint02_base_0"] = {["MapId"] = 382, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 128.29, },
						["manorofrevelryint03_base_0"] = {["MapId"] = 383, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 128.29, },
						["manorofrevelryint04_base_0"] = {["MapId"] = 384, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 128.29, },
						["manorofrevelryint05_base_0"] = {["MapId"] = 385, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 74.43, },
						["marbruk_base_0"] = {["MapId"] = 386, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 579.90, },
						["marbrukoutlawsrefuge_base_0"] = {["MapId"] = 387, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 270.4, },
						["mehrunesspite_base_0"] = {["MapId"] = 388, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["mephalasnest_base_0"] = {["MapId"] = 389, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 407.09, },
						["minesofkhuras_base_0"] = {["MapId"] = 390, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 45.91, },
						["mistral_base_0"] = {["MapId"] = 391, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 723.42, },
						["mistwatchcrevassecrypt_base_0"] = {["MapId"] = 392, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 257.37, },
						["mistwatchtower_base_0"] = {["MapId"] = 393, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 468.98, },
						["mobarmine_base_0"] = {["MapId"] = 394, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 394.52, },
						["molavar_base_0"] = {["MapId"] = 395, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 381.91, },
						["moonmonttemple_base_0"] = {["MapId"] = 396, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 444.15, },
						["moriseli_base_0"] = {["MapId"] = 397, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 245.51, },
						["mournhold_base_0"] = {["MapId"] = 398, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1105.21, },
						["mournholdoutlawsrefuge_base_0"] = {["MapId"] = 399, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 347.7, },
						["mournholdsewers_base_0"] = {["MapId"] = 400, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 406.22, },
						["mtharnaz_base_0"] = {["MapId"] = 401, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 441.83, },
						["muckvalleycavern_base_0"] = {["MapId"] = 402, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["mudshallowcave_base_0"] = {["MapId"] = 403, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 158.13, },
						["mudtreemine_base_0"] = {["MapId"] = 404, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 286.07, },
						["mundus_base_0"] = {["MapId"] = 405, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 1, },
						["murciensclaim_base_0"] = {["MapId"] = 406, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 387.32, },
						["mzendeldt_base_0"] = {["MapId"] = 407, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["mzithumz_base_0"] = {["MapId"] = 408, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 398.35, },
						["mzulft_base_0"] = {["MapId"] = 409, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 781.22, },
						["narilnagaia_base_0"] = {["MapId"] = 410, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 404.83, },
						["narsis_base_0"] = {["MapId"] = 411, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 636.11, },
						["narsisruins_base_0"] = {["MapId"] = 412, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 274.48, },
						["nchuduabtharthreshold_base_0"] = {["MapId"] = 413, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 310.38, },
						["nereidtemple_base_0"] = {["MapId"] = 414, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 431.04, },
						["nesalas_base_0"] = {["MapId"] = 415, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["newtcave_base_0"] = {["MapId"] = 416, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["nilataruins_base_0"] = {["MapId"] = 417, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 305.68, },
						["nilataruinsboss_base_0"] = {["MapId"] = 418, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 215.46, },
						["nimalten_base_0"] = {["MapId"] = 419, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 940.49, },
						["nimaltenpart1_base_0"] = {["MapId"] = 420, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 341.19, },
						["nimaltenpart2_base_0"] = {["MapId"] = 421, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 165.11, },
						["nisincave_base_0"] = {["MapId"] = 422, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["north_hut_portal_cave_base_0"] = {["MapId"] = 423, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 148.88, },
						["northhighrockgate_base_0"] = {["MapId"] = 424, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1, },
						["northmorrowgate_base_0"] = {["MapId"] = 425, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1, },
						["northpoint_base_0"] = {["MapId"] = 426, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 634.86, },
						["northwindmine_base_0"] = {["MapId"] = 427, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 394.49, },
						["norvulkruins_base_0"] = {["MapId"] = 428, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 328.47, },
						["obsidiangorge_base_0"] = {["MapId"] = 429, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 461.11, },
						["obsidianscar_base_0"] = {["MapId"] = 430, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 380.14, },
						["oldcreepycave_base_0"] = {["MapId"] = 431, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["oldmerchantcaves_base_0"] = {["MapId"] = 432, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 254.66, },
						["oldsordscave_base_0"] = {["MapId"] = 433, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 432.1, },
						["ondil_base_0"] = {["MapId"] = 434, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["onkobrakwamamine_base_0"] = {["MapId"] = 435, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 363.45, },
						["onsisbreathmine_base_0"] = {["MapId"] = 436, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 452.15, },
						["orcsfingerruins_base_0"] = {["MapId"] = 437, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 326.07, },
						["orkeyshollow_base_0"] = {["MapId"] = 438, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 435.21, },
						["orrery_base_0"] = {["MapId"] = 439, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 333.84, },
						["ossuaryoftelacar_base_0"] = {["MapId"] = 440, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 450.66, },
						["ouze_base_0"] = {["MapId"] = 441, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 310.84, },
						["pariahcatacombs_base_0"] = {["MapId"] = 442, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 54.09, },
						["phaercatacombs_base_0"] = {["MapId"] = 443, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["pinepeakcaverns_base_0"] = {["MapId"] = 444, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 493.9, },
						["planeofjodecave_base_0"] = {["MapId"] = 445, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 299.11, },
						["planeofjodedenoflorkhaj_base_0"] = {["MapId"] = 446, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 441.65, },
						["planeofjodehubhillbos_base_0"] = {["MapId"] = 447, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 918.39, },
						["planeofjodetemple_base_0"] = {["MapId"] = 448, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 228.62, },
						["portdunwatch_base_0"] = {["MapId"] = 449, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 255.65, },
						["porthunding_base_0"] = {["MapId"] = 450, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 829.81, },
						["potholecavern_base_0"] = {["MapId"] = 451, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["quarantineserk_base_0"] = {["MapId"] = 452, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 425.97, },
						["quickwatercave_base_0"] = {["MapId"] = 453, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["quickwaterdepths_base_0"] = {["MapId"] = 454, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["rage_base_0"] = {["MapId"] = 455, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 327.23, },
						["ragnthar_base_0"] = {["MapId"] = 456, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 258.78, },
						["raylescatacombs_base_0"] = {["MapId"] = 457, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1, },
						["rajhinsvault_base_0"] = {["MapId"] = 458, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 352.86, },
						["rajhinsvaultsmallroom_base_0"] = {["MapId"] = 459, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 89.80, },
						["rawlkha_base_0"] = {["MapId"] = 460, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 559.07, },
						["rawlkhatemple_base_0"] = {["MapId"] = 461, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 535.42, },
						["razakswheel_base_0"] = {["MapId"] = 462, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 655.74, },
						["reapersmarch_base_0"] = {["MapId"] = 463, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 4439.95, },
						["reapersmarchoutlawrefuge_base_0"] = {["MapId"] = 464, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 266.34, },
						["reavercitadelpyramid_base_0"] = {["MapId"] = 465, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 175.97, },
						["redfurtradingpost_base_0"] = {["MapId"] = 466, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 475.44, },
						["redrubycave_base_0"] = {["MapId"] = 467, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["reinholdsretreatcave_base_0"] = {["MapId"] = 468, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["reliquaryofstars_base_0"] = {["MapId"] = 469, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 536.89, },
						["reliquaryvaultbottom_base_0"] = {["MapId"] = 470, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 272.99, },
						["reliquaryvaulttop_base_0"] = {["MapId"] = 471, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 120.17, },
						["rendrocaverns_base_0"] = {["MapId"] = 472, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 335.69, },
						["riften_base_0"] = {["MapId"] = 473, ["TextureSize"] = 512, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 757.58, },
						["riftoutlaw_base_0"] = {["MapId"] = 474, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 246.31, },
						["rivenspire_base_0"] = {["MapId"] = 475, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 4062.47, },
						["rivenspireoutlaw_base_0"] = {["MapId"] = 476, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["rkulftzel_base_0"] = {["MapId"] = 477, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 164.57, },
						["rkundzelft_base_0"] = {["MapId"] = 478, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 402.31, },
						["rootsofsilvenar_base_0"] = {["MapId"] = 479, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 487.1, },
						["rootsoftreehenge_base_0"] = {["MapId"] = 480, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 344.95, },
						["rootsunder_base_0"] = {["MapId"] = 481, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 484.57, },
						["rubblebutte_base_0"] = {["MapId"] = 482, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 401.00, },
						["rulanyilsfall_base_0"] = {["MapId"] = 483, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 504.19, },
						["sacredleapgrotto_base_0"] = {["MapId"] = 484, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["salasen_base_0"] = {["MapId"] = 485, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 275.18, },
						["saltspray_base_0"] = {["MapId"] = 486, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["sancretor1_base_0"] = {["MapId"] = 487, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 373.04, },
						["sancretor2_base_0"] = {["MapId"] = 488, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 259.97, },
						["sancretor3_base_0"] = {["MapId"] = 489, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 246.88, },
						["sancretor4_base_0"] = {["MapId"] = 490, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 242.98, },
						["sancretor5_base_0"] = {["MapId"] = 491, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 220.76, },
						["sancretor6_base_0"] = {["MapId"] = 492, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 211.57, },
						["sancretor7_base_0"] = {["MapId"] = 493, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 279.52, },
						["sancretor8_base_0"] = {["MapId"] = 494, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 218.11, },
						["sandblownmine_base_0"] = {["MapId"] = 495, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 407.10, },
						["sanguinesdemesne_base_0"] = {["MapId"] = 496, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 570.92, },
						["santaki_base_0"] = {["MapId"] = 497, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 452.10, },
						["scalecourtlaboratory_base_0"] = {["MapId"] = 498, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["scaledcourtlaboratory_base_0"] = {["MapId"] = 499, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["secret_tunnel_base_0"] = {["MapId"] = 500, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["seekersarchivedown_base_0"] = {["MapId"] = 501, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["seekersarchiveup_base_0"] = {["MapId"] = 502, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["selenesweb_base_0"] = {["MapId"] = 503, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["seleneswebfinalbossarea_base_0"] = {["MapId"] = 504, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["senelana_base_0"] = {["MapId"] = 505, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 260.91, },
						["sentinel_base_0"] = {["MapId"] = 506, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1106.39, },
						["serpenthollowcave_base_0"] = {["MapId"] = 507, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["serpentsgrotto_base_0"] = {["MapId"] = 508, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 629.63, },
						["serpentsnest_base_0"] = {["MapId"] = 509, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 556.12, },
						["shadaburialgrounds_base_0"] = {["MapId"] = 510, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["shadacitydistrict_base_0"] = {["MapId"] = 511, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["shadahallofworship_base_0"] = {["MapId"] = 512, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["shadamaincity_base_0"] = {["MapId"] = 513, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["shadastula_base_0"] = {["MapId"] = 514, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 218.09, },
						["shadatemplewing_base_0"] = {["MapId"] = 515, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["shademistenclave_base_0"] = {["MapId"] = 516, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 463.68, },
						["shadowfatecavern_base_0"] = {["MapId"] = 517, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 314.71, },
						["shadowfen_base_0"] = {["MapId"] = 518, ["TextureSize"] = 512, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 4036.65, },
						["shadowfenoutlawrefuge_base_0"] = {["MapId"] = 519, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["shadowscaleenclave_base_0"] = {["MapId"] = 520, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["shaelruins_base_0"] = {["MapId"] = 521, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 372.72, },
						["shatteredshoals_base_0"] = {["MapId"] = 522, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["sheogorathstongue_base_0"] = {["MapId"] = 523, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 494.27, },
						["shorecave_base_0"] = {["MapId"] = 524, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 177.60, },
						["shornhelm_base_0"] = {["MapId"] = 525, ["TextureSize"] = 512, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 629.62, },
						["shorsstone_base_0"] = {["MapId"] = 526, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 752.41, },
						["shorsstonemine_base_0"] = {["MapId"] = 527, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 451.90, },
						["shrineofblackworm_base_0"] = {["MapId"] = 528, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 266.46, },
						["shrineofmauloch_base_0"] = {["MapId"] = 529, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 658.29, },
						["shroudedhollowarea1_base_0"] = {["MapId"] = 530, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 452.10, },
						["shroudedhollowarea2_base_0"] = {["MapId"] = 531, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 452.10, },
						["shroudedhollowcenter_base_0"] = {["MapId"] = 532, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 227.40, },
						["shroudedpass_base_0"] = {["MapId"] = 533, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 243.00, },
						["shroudedpass2_base_0"] = {["MapId"] = 534, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 274.33, },
						["shroudhearth_base_0"] = {["MapId"] = 535, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 296.2, },
						["silatar_base_0"] = {["MapId"] = 536, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 948.66, },
						["silumm_base_0"] = {["MapId"] = 537, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 373.40, },
						["silvenarthroneroom_base_0"] = {["MapId"] = 538, ["TextureSize"] = 256, ["TileNum"] = 18, ["Dx"] = 4, ["Dy"] = 4, ["MapSize"] = 100.46, },
						["skinstealerlair_base_0"] = {["MapId"] = 539, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 235.06, },
						["skyreachcatacombs1_base_0"] = {["MapId"] = 540, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["skyreachcatacombs2_base_0"] = {["MapId"] = 541, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["skyreachcatacombs3_base_0"] = {["MapId"] = 542, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["skyreachcatacombs4_base_0"] = {["MapId"] = 543, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["skyreachcatacombs5_base_0"] = {["MapId"] = 544, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["skyreachhold1_base_0"] = {["MapId"] = 545, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 189.20, },
						["skyreachhold2_base_0"] = {["MapId"] = 546, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 194.77, },
						["skyreachhold3_base_0"] = {["MapId"] = 547, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 187.47, },
						["skyreachhold4_base_0"] = {["MapId"] = 548, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 236.27, },
						["skyreachpinnacle_base_0"] = {["MapId"] = 549, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["skyreachtemple_base_0"] = {["MapId"] = 550, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["skyshroudbarrow_base_0"] = {["MapId"] = 551, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 160.90, },
						["skywatch_base_0"] = {["MapId"] = 552, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 909.16, },
						["smugglerkingtunnel_base_0"] = {["MapId"] = 553, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 287.32, },
						["smugglerstunnel_base_0"] = {["MapId"] = 554, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["smugglertunnel_base_0"] = {["MapId"] = 555, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 144.40, },
						["snaplegcave_base_0"] = {["MapId"] = 556, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 406.5, },
						["softloamcavern_base_0"] = {["MapId"] = 557, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 463.36, },
						["south_hut_portal_cave_base_0"] = {["MapId"] = 558, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 165.87, },
						["southhighrockgate_base_0"] = {["MapId"] = 559, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1, },
						["southmorrowgate_base_0"] = {["MapId"] = 560, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1, },
						["southpoint_base_0"] = {["MapId"] = 561, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["southruins_base_0"] = {["MapId"] = 562, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["spindleclutch_base_0"] = {["MapId"] = 563, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 522.53, },
						["spindleclutchheroic_base_0"] = {["MapId"] = 564, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["sren-ja_base_0"] = {["MapId"] = 565, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 250.78, },
						["sren-ja1_base_0"] = {["MapId"] = 566, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 184.17, },
						["sren-ja2_base_0"] = {["MapId"] = 567, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 160.66, },
						["starvedplanes_base_0"] = {["MapId"] = 568, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["stillrisevillage_base_0"] = {["MapId"] = 569, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 265.00, },
						["stirk_base_0"] = {["MapId"] = 570, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 726.35, },
						["stonefalls_base_0"] = {["MapId"] = 571, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 4770.55, },
						["stonefallsoutlawrefuge_base_0"] = {["MapId"] = 572, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["stonefang_base_0"] = {["MapId"] = 573, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["stonetoothfortress_base_0"] = {["MapId"] = 574, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 744.37, },
						["stormcragcrypt_base_0"] = {["MapId"] = 575, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 334.29, },
						["stormhaven_base_0"] = {["MapId"] = 576, ["TextureSize"] = 256, ["TileNum"] = 49, ["Dx"] = 7, ["Dy"] = 7, ["MapSize"] = 4681.63, },
						["stormhavenoutlawrefuge_base_0"] = {["MapId"] = 577, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["stormhold_base_0"] = {["MapId"] = 578, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 634.77, },
						["stormholdayleidruin_base_0"] = {["MapId"] = 579, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 441.54, },
						["stormlair_base_0"] = {["MapId"] = 580, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["stormwardenundercroft_base_0"] = {["MapId"] = 581, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 461.44, },
						["strosmkai_base_0"] = {["MapId"] = 582, ["TextureSize"] = 512, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1983.31, },
						["sunkenroad_base_0"] = {["MapId"] = 583, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 451.99, },
						["sunscaleenclave_base_0"] = {["MapId"] = 584, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 293.84, },
						["suturahs_crypt_base_0"] = {["MapId"] = 585, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 249.43, },
						["taarengrav_base_0"] = {["MapId"] = 586, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 351.2, },
						["taldeiccrypts_base_0"] = {["MapId"] = 587, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 402.26, },
						["tempestisland_base_0"] = {["MapId"] = 588, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["tempestislandncave_base_0"] = {["MapId"] = 589, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["tempestislandsecave_base_0"] = {["MapId"] = 590, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["tempestislandswcave_base_0"] = {["MapId"] = 591, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["temple_base_0"] = {["MapId"] = 592, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 16.74, },
						["templeofsul_base_0"] = {["MapId"] = 593, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 279.50, },
						["templeofthemourningspring_base_0"] = {["MapId"] = 594, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["tenmaurwolk_base_0"] = {["MapId"] = 595, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 253.40, },
						["thaliasretreat_base_0"] = {["MapId"] = 596, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 440.78, },
						["the_aldmiri_harborage_map_base_0"] = {["MapId"] = 597, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 152.55, },
						["the_ebonheart_harborage_base_0"] = {["MapId"] = 598, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 161.98, },
						["the_guardians_skull_base_0"] = {["MapId"] = 599, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["the_masters_crypt_base_0"] = {["MapId"] = 600, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 92.74, },
						["the_portal_chamber_map_base_0"] = {["MapId"] = 601, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 178.33, },
						["thebanishedcells_base_0"] = {["MapId"] = 602, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 548.61, },
						["thebastardstomb_base_0"] = {["MapId"] = 603, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 431.78, },
						["thechillhollow_base_0"] = {["MapId"] = 604, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 379.07, },
						["theearthforge_base_0"] = {["MapId"] = 605, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 761.50, },
						["theearthforgepublic_base_0"] = {["MapId"] = 606, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["theendlessstair_base_0"] = {["MapId"] = 607, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 549.08, },
						["theeverfullflagon_base_0"] = {["MapId"] = 608, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 160.36, },
						["thefarshores_base_0"] = {["MapId"] = 609, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 486.41, },
						["thefrigidgrotto_base_0"] = {["MapId"] = 610, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 331.77, },
						["thegrave_base_0"] = {["MapId"] = 611, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 231.14, },
						["thehuntinggrounds_base_0"] = {["MapId"] = 612, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 500.29, },
						["thelibrarydusk_base_0"] = {["MapId"] = 613, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 181.38, },
						["thelionsden_base_0"] = {["MapId"] = 614, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1203.36, },
						["thelostfleet_base_0"] = {["MapId"] = 615, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 175.49, },
						["themagesstaff_base_0"] = {["MapId"] = 616, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 763.86, },
						["themangroves_base_0"] = {["MapId"] = 617, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["themanorofrevelry_base_0"] = {["MapId"] = 618, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 677.71, },
						["themiddens_base_0"] = {["MapId"] = 619, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 453.15, },
						["themondmine_base_0"] = {["MapId"] = 620, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 280.84, },
						["themooring_base_0"] = {["MapId"] = 621, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 263.53, },
						["therefugeofdread_base_0"] = {["MapId"] = 622, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["therift_base_0"] = {["MapId"] = 623, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 5350.43, },
						["theshrineofstveloth_base_0"] = {["MapId"] = 624, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 124.10, },
						["theunderroot_base_0"] = {["MapId"] = 625, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 393.6, },
						["thevaultofexile_base_0"] = {["MapId"] = 626, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["thevilelaboratory_base_0"] = {["MapId"] = 627, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 507.95, },
						["thevilemansefirstfloor_base_0"] = {["MapId"] = 628, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 596.45, },
						["thevilemansesecondfloor_base_0"] = {["MapId"] = 629, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 594.4, },
						["thewormsretreat_base_0"] = {["MapId"] = 630, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["thibautscairn_base_0"] = {["MapId"] = 631, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 355.98, },
						["toadstoolhollow_base_0"] = {["MapId"] = 632, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["toadstoolhollowlower_base_0"] = {["MapId"] = 633, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["tombofanahbi_base_0"] = {["MapId"] = 634, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 240.41, },
						["tomboflostkings_base_0"] = {["MapId"] = 635, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 377.51, },
						["tomboftheapostates_base_0"] = {["MapId"] = 636, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 400.66, },
						["toothmaulgully_base_0"] = {["MapId"] = 637, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 730.18, },
						["torinaan1_base_0"] = {["MapId"] = 638, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["torinaan2_base_0"] = {["MapId"] = 639, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["torinaan3_base_0"] = {["MapId"] = 640, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["torinaan4_base_0"] = {["MapId"] = 641, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["torinaan5_base_0"] = {["MapId"] = 642, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["tormented_spire_base_0"] = {["MapId"] = 643, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 642.72, },
						["tormentedspireinstance_base_0"] = {["MapId"] = 644, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 368.9, },
						["toweroflies_base_0"] = {["MapId"] = 645, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 386.42, },
						["tribulationcrypt_base_0"] = {["MapId"] = 646, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 269.83, },
						["tribunaltemple_base_0"] = {["MapId"] = 647, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 305.64, },
						["triplecirclemine_base_0"] = {["MapId"] = 648, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 369.99, },
						["trl_so_map01_base_0"] = {["MapId"] = 649, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["trl_so_map02_base_0"] = {["MapId"] = 650, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["trl_so_map03_base_0"] = {["MapId"] = 651, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["trl_so_map04_base_0"] = {["MapId"] = 652, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["trolhettacave_base_0"] = {["MapId"] = 653, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 508.14, },
						["trolhettasummit_base_0"] = {["MapId"] = 654, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 359.34, },
						["trollstoothpick_base_0"] = {["MapId"] = 655, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 388.00, },
						["tsanji_base_0"] = {["MapId"] = 656, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 141.06, },
						["underpallcave_base_0"] = {["MapId"] = 657, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["unexploredcrag_base_0"] = {["MapId"] = 658, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 374.56, },
						["urcelmosbetrayal_base_0"] = {["MapId"] = 659, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 451.89, },
						["vahtacen_base_0"] = {["MapId"] = 660, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["valeoftheghostsnake_base_0"] = {["MapId"] = 661, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 169.74, },
						["valleyofblades1_base_0"] = {["MapId"] = 662, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 671.39, },
						["valleyofblades2_base_0"] = {["MapId"] = 663, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 288.67, },
						["vaultofhamanforgefire_base_0"] = {["MapId"] = 664, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 327.51, },
						["vaultsofmadness1_base_0"] = {["MapId"] = 665, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 335.68, },
						["vaultsofmadness2_base_0"] = {["MapId"] = 666, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 338.27, },
						["veiledkeepbase_0"] = {["MapId"] = 667, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["velynharbor_base_0"] = {["MapId"] = 668, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 992.91, },
						["vernimwood_base_0"] = {["MapId"] = 669, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 227.26, },
						["vetcirtyash01_base_0"] = {["MapId"] = 670, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["vetcirtyash02_base_0"] = {["MapId"] = 671, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["vetcirtyash03_base_0"] = {["MapId"] = 672, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["vetcirtyash04_base_0"] = {["MapId"] = 673, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["vilemansehouse01_base_0"] = {["MapId"] = 674, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 71.5, },
						["vilemansehouse02_base_0"] = {["MapId"] = 675, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 71.5, },
						["villageofthelost_base_0"] = {["MapId"] = 676, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1503.71, },
						["vindeathcave_base_0"] = {["MapId"] = 677, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 334.40, },
						["vineduckvillage_base_0"] = {["MapId"] = 678, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 151.17, },
						["vineduckvillagecorridor_base_0"] = {["MapId"] = 679, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 293.79, },
						["viridianhideaway_base_0"] = {["MapId"] = 680, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 227.31, },
						["viridianwatch_base_0"] = {["MapId"] = 681, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 263.12, },
						["visionofthecompanions_base_0"] = {["MapId"] = 682, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["visionofthehist_base_0"] = {["MapId"] = 683, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 318.39, },
						["volenfell_base_0"] = {["MapId"] = 684, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["volenfell_pledge_base_0"] = {["MapId"] = 685, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["vulkel_guard_prison_base_0"] = {["MapId"] = 686, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["vulkhelguard_base_0"] = {["MapId"] = 687, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1064.59, },
						["vulkwasten_base_0"] = {["MapId"] = 688, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 715.88, },
						["wailingmaw_base_0"] = {["MapId"] = 689, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 436.23, },
						["wailingprison1_base_0"] = {["MapId"] = 690, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wailingprison2_base_0"] = {["MapId"] = 691, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wailingprison3_base_0"] = {["MapId"] = 692, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wailingprison4_base_0"] = {["MapId"] = 693, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wailingprison5_base_0"] = {["MapId"] = 694, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wailingprison6_base_0"] = {["MapId"] = 695, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wailingprison7_base_0"] = {["MapId"] = 696, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wayrest_base_0"] = {["MapId"] = 697, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 927.44, },
						["wayrestsewers_base_0"] = {["MapId"] = 698, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wansalen_base_0"] = {["MapId"] = 699, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["weepingwindcave_base_0"] = {["MapId"] = 700, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 383.05, },
						["welloflostsouls_base_0"] = {["MapId"] = 701, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 266.46, },
						["west_hut_portal_cave_base_0"] = {["MapId"] = 702, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 201.13, },
						["westelsweyrgate_base_0"] = {["MapId"] = 703, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 600.14, },
						["whitefallmountain_base_0"] = {["MapId"] = 704, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["whiteroseprison_base_0"] = {["MapId"] = 705, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 355.90, },
						["windhelm_base_0"] = {["MapId"] = 706, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 713.14, },
						["windridgecave_base_0"] = {["MapId"] = 707, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 412.76, },
						["wittestadrcrypts_base_0"] = {["MapId"] = 708, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 347.48, },
						["woodhearth_base_0"] = {["MapId"] = 709, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1026.80, },
						["wormrootdepths_base_0"] = {["MapId"] = 710, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 318.76, },
						["wrothgar_base_0"] = {["MapId"] = 711, ["TextureSize"] = 512, ["TileNum"] = 16, ["Dx"] = 4, ["Dy"] = 4, ["MapSize"] = 4687.32, },
						["zehtswatercavelower_base_0"] = {["MapId"] = 712, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["zehtswatercaveupper_base_0"] = {["MapId"] = 713, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["the_daggerfall_harborage_0"] = {["MapId"] = 714, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 181.4, },
						["stormholdmortuary_map_0"] = {["MapId"] = 715, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 127.26, },
						["stormholdguildhall_map_0"] = {["MapId"] = 716, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 191.15, },
						["hissmirruins_map_0"] = {["MapId"] = 717, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 174.47, },
						["sf_percolatingmire_map_0"] = {["MapId"] = 718, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 192.71, },
						["apothacarium_map_0"] = {["MapId"] = 719, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 147.52, },
						["giantsrun_base_0"] = {["MapId"] = 720, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 3117.86, },
						["rkhardahrk_0"] = {["MapId"] = 721, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 388.4, },
						["bangkoraigarrisontop_base_0"] = {["MapId"] = 722, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["bangkoraigarrisonbtm_base_0"] = {["MapId"] = 723, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["hircineswoods_group_base_0"] = {["MapId"] = 724, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["ch_the_everfull_flagon_0"] = {["MapId"] = 725, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["dragonstararena01_0"] = {["MapId"] = 726, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["rahniza_1_0"] = {["MapId"] = 727, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["rahniza_2_0"] = {["MapId"] = 728, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["rahniza_3_0"] = {["MapId"] = 729, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperialsewers_aldmeri1_base_0"] = {["MapId"] = 730, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewers_aldmeri2_base_0"] = {["MapId"] = 731, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewers_aldmeri3_base_0"] = {["MapId"] = 732, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewers_ebon1_base_0"] = {["MapId"] = 733, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewers_ebon2_base_0"] = {["MapId"] = 734, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewer_dagfall3b_base_0"] = {["MapId"] = 735, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewer_daggerfall1_base_0"] = {["MapId"] = 736, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewer_daggerfall2_base_0"] = {["MapId"] = 737, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewer_daggerfall3_base_0"] = {["MapId"] = 738, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewer_ebonheart3b_base_0"] = {["MapId"] = 739, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewer_ebonheart3m_base_0"] = {["MapId"] = 740, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperialsewer_ebonheart3_base_0"] = {["MapId"] = 741, ["TextureSize"] = 256, ["TileNum"] = 36, ["Dx"] = 6, ["Dy"] = 6, ["MapSize"] = 1, },
						["imperial_dragonfire_cath_base_0"] = {["MapId"] = 742, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["imperial_dragonfire_tunne_base_0"] = {["MapId"] = 743, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtbattlemage_base_0"] = {["MapId"] = 744, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtgreenemporerway_base_0"] = {["MapId"] = 745, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtimperialguardquarters_base_0"] = {["MapId"] = 746, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtimperialthroneroom_base_0"] = {["MapId"] = 747, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtlibraryhall_base_0"] = {["MapId"] = 748, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtlibrarymain_base_0"] = {["MapId"] = 749, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtpalacesewers_base_0"] = {["MapId"] = 750, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtpinnacleboss_base_0"] = {["MapId"] = 751, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtpinnacle_base_0"] = {["MapId"] = 752, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtregentsquarters_base_0"] = {["MapId"] = 753, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtvoid1_base_0"] = {["MapId"] = 754, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["wgtvoid2_base_0"] = {["MapId"] = 755, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["glenumbracamlornkeep_base_0"] = {["MapId"] = 756, ["TextureSize"] = 1024, ["TileNum"] = 4, ["Dx"] = 2, ["Dy"] = 2, ["MapSize"] = 1, },
						["gilvardelleabandoncave_0"] = {["MapId"] = 757, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden10_base_0"] = {["MapId"] = 758, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden11f2_base_0"] = {["MapId"] = 759, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden11r1_base_0"] = {["MapId"] = 760, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden11r2_base_0"] = {["MapId"] = 761, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden11r3_base_0"] = {["MapId"] = 762, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden11_base_0"] = {["MapId"] = 763, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden12_base_0"] = {["MapId"] = 764, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden13_base_0"] = {["MapId"] = 765, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden1_base_0"] = {["MapId"] = 766, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden1_main_base_0"] = {["MapId"] = 767, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden1_room1_base_0"] = {["MapId"] = 768, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden1_room2_base_0"] = {["MapId"] = 769, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden1_room3_base_0"] = {["MapId"] = 770, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden2_base_0"] = {["MapId"] = 771, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden2_room1_base_0"] = {["MapId"] = 772, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden2_room2_base_0"] = {["MapId"] = 773, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden2_room3_base_0"] = {["MapId"] = 774, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden2_room4_base_0"] = {["MapId"] = 775, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden3_base_0"] = {["MapId"] = 776, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden3_base_cave_0"] = {["MapId"] = 777, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden3_base_room1_0"] = {["MapId"] = 778, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden3_base_room2_0"] = {["MapId"] = 779, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden3_base_room3_0"] = {["MapId"] = 780, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden4_base_0"] = {["MapId"] = 781, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden4_room1_base_0"] = {["MapId"] = 782, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden4_room2_base_0"] = {["MapId"] = 783, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden4_room3_base_0"] = {["MapId"] = 784, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden4_room4_base_0"] = {["MapId"] = 785, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden4_room5_base_0"] = {["MapId"] = 786, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden4_room6_base_0"] = {["MapId"] = 787, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden5_base_0"] = {["MapId"] = 788, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden5_room1_base_0"] = {["MapId"] = 789, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden5_room2_base_0"] = {["MapId"] = 790, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden5_room3_base_0"] = {["MapId"] = 791, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden5_room4_base_0"] = {["MapId"] = 792, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden6_base_0"] = {["MapId"] = 793, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden6_room1_base_0"] = {["MapId"] = 794, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden6_room2_base_0"] = {["MapId"] = 795, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden6_room3_base_0"] = {["MapId"] = 796, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden6_room4_base_0"] = {["MapId"] = 797, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden6_room5_base_0"] = {["MapId"] = 798, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden7_base_0"] = {["MapId"] = 799, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden8_base_0"] = {["MapId"] = 800, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden8_room1_base_0"] = {["MapId"] = 801, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden8_room2_base_0"] = {["MapId"] = 802, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden8_room3_base_0"] = {["MapId"] = 803, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditden9_base_0"] = {["MapId"] = 804, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["banditdenoneone_base_0"] = {["MapId"] = 805, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["thefivefingerdance_0"] = {["MapId"] = 806, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["cryptofheartsheroicboss_0"] = {["MapId"] = 807, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["arenasclockwork2_base_0"] = {["MapId"] = 808, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["arenasclockworkint_base_0"] = {["MapId"] = 809, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 325.18, },
						["arenaslavacaveinterior_base_0"] = {["MapId"] = 810, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["arenaslobbyexterior_base_0"] = {["MapId"] = 811, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 469.45, },
						["arenasmephalaexterior_base_0"] = {["MapId"] = 812, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["arenasmurkmirecaveint_base_0"] = {["MapId"] = 813, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["arenasmurkmirecaveinter_base_0"] = {["MapId"] = 814, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["arenasmurkmireexterior_base_0"] = {["MapId"] = 815, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["arenasoblivionexterior_base_0"] = {["MapId"] = 816, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["arenasshiveringisles_base_0"] = {["MapId"] = 817, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 628.4, },
						["arenaswrothgarexterior_base_0"] = {["MapId"] = 818, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["argentmine_base_0"] = {["MapId"] = 819, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["argentmine2_base_0"] = {["MapId"] = 820, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 500, },
						["bloodyknoll_base_0"] = {["MapId"] = 821, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["bonegrinder_base_0"] = {["MapId"] = 822, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 533.3, },
						["bonerock_caverns_base_0"] = {["MapId"] = 823, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 745.9, },
						["chambersofloyalty_base_0"] = {["MapId"] = 824, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["coldperchcavern_base_0"] = {["MapId"] = 825, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 488.9, },
						["exilesbarrow_map_base_0"] = {["MapId"] = 826, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 457.8, },
						["fharunprison_base_0"] = {["MapId"] = 827, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["fharunstronghold01_base_0"] = {["MapId"] = 828, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["fharunstronghold02_base_0"] = {["MapId"] = 829, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["fharunstronghold03_base_0"] = {["MapId"] = 830, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["fharunstronghold03b_base_0"] = {["MapId"] = 831, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["frostbreakfortint_map_base_0"] = {["MapId"] = 832, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["graystonequarrybottom_base_0"] = {["MapId"] = 833, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["graystonequarrytop_base_0"] = {["MapId"] = 834, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["honorsrestdesert_base_0"] = {["MapId"] = 835, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["honorsrestfinalc_base_0"] = {["MapId"] = 836, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["honorsrestfinalv_base_0"] = {["MapId"] = 837, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["honorsrestleft_base_0"] = {["MapId"] = 838, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["honorsrestorc_base_0"] = {["MapId"] = 839, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["honorsrestright_base_0"] = {["MapId"] = 840, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["iceheartslair_map_base_0"] = {["MapId"] = 841, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["kennelrun_base_0"] = {["MapId"] = 842, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 756.69, },
						["morkul_base_0"] = {["MapId"] = 843, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 482.14, },
						["morkuldescent_map_base_0"] = {["MapId"] = 844, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["morkuldin_base_0"] = {["MapId"] = 845, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["oldorsiniummap01_base_0"] = {["MapId"] = 846, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 312.8, },
						["oldorsiniummap02_base_0"] = {["MapId"] = 847, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 554.4, },
						["oldorsiniummap03_base_0"] = {["MapId"] = 848, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 262.3, },
						["oldorsiniummap04_base_0"] = {["MapId"] = 849, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 362.7, },
						["oldorsiniummap05_base_0"] = {["MapId"] = 850, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 247.3, },
						["oldorsiniummap06_base_0"] = {["MapId"] = 851, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 398.9, },
						["oldorsiniummap07_base_0"] = {["MapId"] = 852, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 411.2, },
						["orsinium_base_0"] = {["MapId"] = 853, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 893.98, },
						["orsiniumtemplelower_base_0"] = {["MapId"] = 854, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["orsiniumtempleupper_base_0"] = {["MapId"] = 855, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["orsiniumthroneroom_base_0"] = {["MapId"] = 856, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["paragonsrememberance_base_0"] = {["MapId"] = 857, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["pathtothemoo_library_base_0"] = {["MapId"] = 858, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["pathtothemoot_base_0"] = {["MapId"] = 859, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["rectory01_base_0"] = {["MapId"] = 860, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["rkindaleftint01_base_0"] = {["MapId"] = 861, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 467.9, },
						["rkindaleftint02_base_0"] = {["MapId"] = 862, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["rkindaleftint03_base_0"] = {["MapId"] = 863, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["rkindaleftoutside_base_0"] = {["MapId"] = 864, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["sanctumofprowess_base_0"] = {["MapId"] = 865, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["scarpkeeplower_base_0"] = {["MapId"] = 866, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["scarpkeepupper_base_0"] = {["MapId"] = 867, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["sorrowext_base_0"] = {["MapId"] = 868, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["sorrowint01_base_0"] = {["MapId"] = 869, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["sorrowint02_base_0"] = {["MapId"] = 870, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["sorrowint03_base_0"] = {["MapId"] = 871, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 1, },
						["thukozods_base_0"] = {["MapId"] = 872, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 436.75, },
						["watchershold_base_0"] = {["MapId"] = 873, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 596.7, },
						["wrothgaroutlawrefuge_base_0"] = {["MapId"] = 874, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 169.45, },
						["zthenganaz_base_0"] = {["MapId"] = 875, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 475.5, },
						["abahslanding_base_0"] = {["MapId"] = 876, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 1179.25, },
						["hewsbane_base_0"] = {["MapId"] = 877, ["TextureSize"] = 256, ["TileNum"] = 25, ["Dx"] = 5, ["Dy"] = 5, ["MapSize"] = 3330.03, },
						["bahrahasgloom_base_0"] = {["MapId"] = 878, ["TextureSize"] = 256, ["TileNum"] = 9, ["Dx"] = 3, ["Dy"] = 3, ["MapSize"] = 555.26, },
						}


FyrMM.Panel = {
	type = "panel",
	name = "MiniMap",
	displayName = GetString(SI_MM_STRING_SETTINGS),
	author = "|c006600Fyrakin|r",
	version = "2.97",
	slashCommand = "/fyrmmset",
	registerForRefresh = true,
	registerForDefaults = true,
}

	FyrMM.Options = {
	[1] = { type = "checkbox", name = GetString(SI_MM_SETTING_ZYGOR), tooltip = GetString(SI_MM_SETTING_ZYGOR_TOOLTIP),
			getFunc = function() return FyrMM.SV.UseOriginalAPI end,
			setFunc = function(value) MM_SetUseOriginalAPI(value) end,
			width = "full",	default = true,	},
	[2] = { type = "checkbox", name = GetString(SI_MM_SETTING_COMPASS), tooltip = GetString(SI_MM_SETTING_COMPASS_TOOLTIP),
			getFunc = function() return MM_GetHideCompass() end,
			setFunc = function(value) MM_SetHideCompass(value) end,
			width = "full", default = false, },
	[3] = { type = "checkbox", name = GetString(SI_MM_SETTING_STARTUP), tooltip = GetString(SI_MM_SETTING_STARTUP_TOOLTIP),
			getFunc = function() return FyrMM.SV.StartupInfo end,
			setFunc = function(value) FyrMM.SV.StartupInfo = value end,
			width = "full", default = false, },
	[4] = { type = "checkbox", name = GetString(SI_MM_SETTING_MENU), tooltip = GetString(SI_MM_SETTING_MENU_TOOLTIP),
			getFunc = function() return FyrMM.SV.MenuDisabled end,
			setFunc = function(value) FyrMM.SV.MenuDisabled = value if value then Fyr_MM_Menu:SetHidden(true) MM_SetZoneFrameLocationOption(FyrMM.SV.ZoneFrameLocationOption) else Fyr_MM_Menu:SetHidden(false) MM_SetZoneFrameLocationOption(FyrMM.SV.ZoneFrameLocationOption) end end,
			width = "half", default = false, },
	[5] = { type = "checkbox", name = GetString(SI_MM_SETTING_MENUAUTOHIDE), tooltip = GetString(SI_MM_SETTING_MENUAUTOHIDE_TOOLTIP),
			getFunc = function() return FyrMM.SV.MenuAutoHide end,
			setFunc = function(value) FyrMM.SV.MenuAutoHide = value if not value then Fyr_MM_Menu:SetAlpha(1) else FyrMM.MenuFadeOut() end end,
			width = "half", default = false, },
	[6] = { type = "button", name = GetString(SI_MM_SETTING_WHEELDEFAULTS), tooltip = GetString(SI_MM_SETTING_WHEELDEFAULTS_TOOLTIP),
								func = function() MM_WheelModeDefaults() end, width = "half", },
	[7] = { type = "button", name = GetString(SI_MM_SETTING_SQUAREDEFAULTS), tooltip = GetString(SI_MM_SETTING_SQUAREDEFAULTS_TOOLTIP),
								func = function() MM_SquareModeDefaults() end, width = "half", },
	[8] = { type = "submenu", name = GetString(SI_MM_SETTING_SIZEOPTIONS),
			controls = {
						[1] = { type = "slider", name = GetString(SI_MM_SETTING_X), tooltip = GetString(SI_MM_SETTING_X_TOOTLITP),
								min = 0, max = zo_round(GuiRoot:GetWidth()-Fyr_MM:GetWidth()), step = 1,
								getFunc = function() return zo_round(Fyr_MM:GetLeft()) end,
								setFunc = function(value) if FyrMM.SV.LockPosition then return end local pos = {} pos.anchorTo = GetControl(pos.anchorTo) FyrMM.SV.position.offsetX = value
										Fyr_MM:SetAnchor(FyrMM.SV.position.point, pos.anchorTo, FyrMM.SV.position.relativePoint, value, FyrMM.SV.position.offsetY) end,
								width = "half",	default = 0, },
						[2] = {	type = "slider", name = GetString(SI_MM_SETTING_Y), tooltip = GetString(SI_MM_SETTING_Y_TOOLTIP),
								min = 0, max = zo_round(GuiRoot:GetHeight()-Fyr_MM:GetHeight()), step = 1,
								getFunc = function() return zo_round(Fyr_MM:GetTop()) end,
								setFunc = function(value)	if FyrMM.SV.LockPosition then return end local pos = {} pos.anchorTo = GetControl(pos.anchorTo) FyrMM.SV.position.offsetY = value
										Fyr_MM:SetAnchor(FyrMM.SV.position.point, pos.anchorTo, FyrMM.SV.position.relativePoint, FyrMM.SV.position.offsetX, value) end,
								width = "half",	default = 0, },
						[3] = { type = "slider", name = GetString(SI_MM_SETTING_WIDTH), tooltip = GetString(SI_MM_SETTING_WIDTH_TOOLTIP),
								min = 50, max = zo_round(GuiRoot:GetWidth()), step = 1,
								getFunc = function() return MM_GetMapWidth() end,
								setFunc = function(value) MM_SetMapWidth(value) end,
								width = "half",	default = 280, },
						[4] = {	type = "slider", name = GetString(SI_MM_SETTING_HEIGHT), tooltip = GetString(SI_MM_SETTING_HEIGHT_TOOLTIP),
								min = 50, max = zo_round(GuiRoot:GetHeight()), step = 1,
								getFunc = function() return MM_GetMapHeight() end,
								setFunc = function(value) MM_SetMapHeight(value) end,
								width = "half",	default = 280, },
						[5] = { type = "checkbox", name = GetString(SI_MM_SETTING_LOCK), tooltip = GetString(SI_MM_SETTING_LOCK_TOOLTIP),
								getFunc = function() return FyrMM.SV.LockPosition end,
								setFunc = function(value) MM_SetLockPosition(value) end,
								width = "full",	default = false, },
						[6] = { type = "checkbox", name = GetString(SI_MM_SETTING_CLAMP), tooltip = GetString(SI_MM_SETTING_CLAPM_TOOLTIP),
								getFunc = function() return MM_GetClampedToScreen() end,
								setFunc = function(value) MM_SetClampedToScreen(value) end,
								width = "half",	default = true,	}, }, },
	[9] = { type = "submenu", name = GetString(SI_MM_SETTING_MODEOPTIONS),
			controls = {
						[1] = { type = "slider", name = GetString(SI_MM_SETTING_ALPHA), tooltip = GetString(SI_MM_SETTING_ALPHA_TOOLTIP),
								min = 60, max = 100, step = 1,
								getFunc = function() return MM_GetMapAlpha() end,
								setFunc = function(value) MM_SetMapAlpha(value) end,
								width = "half",	default = 100, },
						[2] = {	type = "slider", name = GetString(SI_MM_SETTING_PINSCALE), tooltip = GetString(SI_MM_SETTING_PINSCALE_TOOLTIP),
								min = 25, max = 150, step = 1,
								getFunc = function() return MM_GetPinScale() end,
								setFunc = function(value) MM_SetPinScale(value) end,
								width = "half",	default = 75, },
						[3] = { type = "slider", name = GetString(SI_MM_SETTING_DEFAULTZOOM), tooltip = GetString(SI_MM_SETTING_DEFAULTZOOM_TOOLTIP),
								min = FYRMM_ZOOM_MIN, max = FYRMM_ZOOM_MAX, step = FYRMM_ZOOM_INCREMENT_AMOUNT,
								getFunc = function() return FYRMM_DEFAULT_ZOOM_LEVEL end,
								setFunc = function(value) FYRMM_DEFAULT_ZOOM_LEVEL = value FyrMM.SV.DefaultZoomLevel = value end,
								width = "full",	default = 10, },
						[4] = { type = "slider", name = GetString(SI_MM_SETTING_CURRENTZOOM), tooltip = GetString(SI_MM_SETTING_CURRENTZOOM_TOOLTIP),
								min = FYRMM_ZOOM_MIN, max = FYRMM_ZOOM_MAX, step = FYRMM_ZOOM_INCREMENT_AMOUNT,
								getFunc = function() return FyrMM.currentMap.ZoomLevel end,
								setFunc = function(value) FyrMM.SetCurrentMapZoom(value) end,
								width = "half",	default = FYRMM_DEFAULT_ZOOM_LEVEL, },
						[5] = { type = "slider", name = GetString(SI_MM_SETTING_ZOOMSTEPPING), tooltip = GetString(SI_MM_SETTING_ZOOMSTEPPING_TOOLTIP),
								min = 0.1, max = 2, step = 0.1,
								getFunc = function() return FYRMM_ZOOM_INCREMENT_AMOUNT end,
								setFunc = function(value) value = ("%1.1f"):format(value) FYRMM_ZOOM_INCREMENT_AMOUNT = tonumber(value) FyrMM.SV.ZoomIncrement = tonumber(value) end,
								width = "half",	default = 1, },
						[6] = {	type = "dropdown", name = GetString(SI_MM_SETTING_HEADING), tooltip = GetString(SI_MM_SETTING_HEADING_TOOLTIP),
								choices = {"CAMERA", "PLAYER", "MIXED"},
								getFunc = function() return MM_GetHeading() end,
								setFunc = function(value) MM_SetHeading(value) end,
								width = "half", default = "CAMERA" },
						[7] = { type = "checkbox", name = GetString(SI_MM_SETTING_PVP), tooltip = GetString(SI_MM_SETTING_PVP_TOOLTIP),
								getFunc = function() return MM_GetHidePvPPins() end,
								setFunc = function(value) MM_SetHidePvPPins(value) end,
								width = "half",	default = false, },
						[8] = { type = "checkbox", name = GetString(SI_MM_SETTING_ROTATION), tooltip = GetString(SI_MM_SETTING_ROTATION_TOOLTIP),
								getFunc = function() return FyrMM.SV.RotateMap end,
								setFunc = function(value) MM_SetRotateMap(value) if not value then FyrMM.PendingRotatePins = {} end end,
								width = "half",	default = false,	},
						[9] = { type = "checkbox", name = GetString(SI_MM_SETTING_WHEEL), tooltip = GetString(SI_MM_SETTING_WHEEL_TOOLTIP),
								getFunc = function() return FyrMM.SV.WheelMap end,
								setFunc = function(value) MM_SetWheelMap(value) if not value then FyrMM.PendingWheelPins = {} end end,
								width = "half",	default = false,	},
						[10] = { type = "checkbox", name = GetString(SI_MM_SETTING_VIEWRANGE), tooltip = GetString(SI_MM_SETTING_VIEWRANGE_TOOLTIP),
								getFunc = function() return FyrMM.SV.ViewRangeFiltering end,
								setFunc = function(value) FyrMM.SV.ViewRangeFiltering = value end,
								width = "half",	default = false,	},
						[11] = { type = "slider", name = GetString(SI_MM_SETTING_RANGE), tooltip = GetString(SI_MM_SETTING_RANGE_TOOLTIP),
								min = 50, max = 1000, step = 1,
								getFunc = function() return FyrMM.SV.CustomPinViewRange end,
								setFunc = function(value) FyrMM.SV.CustomPinViewRange = value end,
								width = "half",	default = 250, },
						[12] = {	type = "dropdown", name = GetString(SI_MM_SETTING_WHEELTEXTURE), tooltip = GetString(SI_MM_SETTING_WHEELTEXTURE_TOOLTIP),
								choices = FyrMM.WheelTextureList,
								getFunc = function() return FyrMM.SV.WheelTexture end,
								setFunc = function(value) MM_SetWheelTexture(value) end,
								width = "full", default = "Deathangel RMM Wheel" },
						[13]  = {	type = "dropdown", name = GetString(SI_MM_SETTING_MENUTEXTURE), tooltip = GetString(SI_MM_SETTING_MENUTEXTURE_TOOLTIP),
								choices = FyrMM.MenuTextureList,
								getFunc = function() return FyrMM.SV.MenuTexture end,
								setFunc = function(value) MM_SetMenuTexture(value) end,
								width = "full", default = "Default" },
						[14] = { type = "checkbox", name = GetString(SI_MM_SETTING_UNEXPLORED), tooltip = GetString(SI_MM_SETTING_UNEXPLORED_TOOLTIP),
								getFunc = function() return FyrMM.SV.ShowUnexploredPins end,
								setFunc = function(value) MM_SetShowUnexploredPins(value) end,
								width = "full",	default = true,	},
						[15] = { type = "colorpicker", name = GetString(SI_MM_SETTING_UNEXPLOREDCOLOR), tooltip = GetString(SI_MM_SETTING_UNEXPLOREDCOLOR_TOOLTIP),
								getFunc = function() return FyrMM.SV.UndiscoveredPOIPinColor.r, FyrMM.SV.UndiscoveredPOIPinColor.g, FyrMM.SV.UndiscoveredPOIPinColor.b, FyrMM.SV.UndiscoveredPOIPinColor.a end,
								setFunc = function(r,g,b,a) MM_SetUndiscoveredPOIPinColor(r, g, b, a) end,
								width = "full", },
						[16] = { type = "checkbox", name = GetString(SI_MM_SETTING_TOOLTIPS), tooltip = GetString(SI_MM_SETTING_TOOLTIPS_TOOLTIP),
								getFunc = function() return MM_GetPinTooltips() end,
								setFunc = function(value) MM_SetPinTooltips(value) end,
								width = "half",	default = true,	},
						[17] = { type = "checkbox", name = GetString(SI_MM_SETTING_FASTTRAVEL), tooltip = GetString(SI_MM_SETTING_FASTTRAVEL_TOOLTIP),
								getFunc = function() return MM_GetFastTravelEnabled() end,
								setFunc = function(value) MM_SetFastTravelEnabled(value) end,
								width = "half",	default = true,	},
						[18] = { type = "checkbox", name = GetString(SI_MM_SETTING_MOUSEZOOM), tooltip = GetString(SI_MM_SETTING_MOUSEZOOM_TOOLTIP),
								getFunc = function() return MM_GetMouseWheel() end,
								setFunc = function(value) MM_SetMouseWheel(value) end,
								width = "half",	default = true,	},
						[19] = { type = "checkbox", name = GetString(SI_MM_SETTING_BORDER), tooltip = GetString(SI_MM_SETTING_BORDER_TOOLTIP),
								getFunc = function() return MM_GetShowBorder() end,
								setFunc = function(value) MM_SetShowBorder(value) end,
								width = "half",	default = true,	},
						[20] = { type = "checkbox", name = GetString(SI_MM_SETTING_COMBATHIDE), tooltip = GetString(SI_MM_SETTING_COMBATHIDE_TOOLTIP),
								getFunc = function() return FyrMM.SV.InCombatAutoHide end,
								setFunc = function(value) MM_SetInCombatAutoHide(value) end,
								width = "half",	default = true,	},
						[21] = { type = "slider", name = GetString(SI_MM_SETTING_AUTOSHOW), tooltip = GetString(SI_MM_SETTING_AUTOSHOW_TOOLTIP),
								min = 1, max = 60, step = 1,
								getFunc = function() return FyrMM.SV.AfterCombatUnhideDelay end,
								setFunc = function(value) MM_SetAfterCombatUnhideDelay(value) end,
								width = "half",	default = 5, }, 
						[22] = { type = "checkbox", name = GetString(SI_MM_SETTING_SIEGE), tooltip = GetString(SI_MM_SETTING_SIEGE_TOOLTIP),
								getFunc = function() return FyrMM.SV.Siege end,
								setFunc = function(value) FyrMM.SV.Siege = value end,
								width = "full",	default = true, }, 
						[23] = { type = "checkbox", name = GetString(SI_MM_SETTING_SUBZONE), tooltip = GetString(SI_MM_SETTING_SUBZONE_TOOLTIP),
								getFunc = function() return FyrMM.SV.DisableSubzones end,
								setFunc = function(value) FyrMM.SV.DisableSubzones = value FyrMM.DisableSubzones = value end,
								width = "full",	default = false,	}, }, },
	[10] = { type = "submenu", name = GetString(SI_MM_SETTING_INFOOPTIONS),
			controls = {
						[1] = { type = "checkbox", name = GetString(SI_MM_SETTING_POSITION), tooltip = GetString(SI_MM_SETTING_POSITION_TOOLTIP),
								getFunc = function() return MM_GetShowPosition() end,
								setFunc = function(value) MM_SetShowPosition(value) end,
								width = "full",	default = true,	},
						[2] = { type = "checkbox", name = GetString(SI_MM_SETTING_POSITIONBACKGROUND), tooltip = GetString(SI_MM_SETTING_POSITIONBACKGROUND_TOOLTIP),
								getFunc = function() return FyrMM.SV.PositionBackground end,
								setFunc = function(value) MM_SetPositionBackground(value) end,
								width = "full",	default = true,	},			
						[3] = {	type = "dropdown", name = GetString(SI_MM_SETTING_POSITIONLOCATION), tooltip = GetString(SI_MM_SETTING_POSITIONLOCATION_TOOLTIP),
								choices = {"Top", "Bottom", "Free"},
								getFunc = function() return FyrMM.SV.CorrdinatesLocation end,
								setFunc = function(value) MM_SetCoordinatesLocation(value) end,
								width = "half", default = "Top" },
						[4] = { type = "colorpicker", name = GetString(SI_MM_SETTING_POSITIONCOLOR), tooltip = GetString(SI_MM_SETTING_POSITIONCOLOR_TOOLTIP),
								getFunc = function() return FyrMM.SV.PositionColor.r, FyrMM.SV.PositionColor.g, FyrMM.SV.PositionColor.b, FyrMM.SV.PositionColor.a end,
								setFunc = function(r,g,b,a) MM_SetPositionColor(r, g, b, a) end,
								width = "half", },
						[5] = {	type = "dropdown", name = GetString(SI_MM_SETTING_POSITIONFONT), tooltip = GetString(SI_MM_SETTING_POSITIONFONT_TOOLTIP),
								choices = FyrMM.FontList,
								getFunc = function() return FyrMM.SV.PositionFont end,
								setFunc = function(value) if value ~= nil then FyrMM.SV.PositionFont = value MM_SetPositionFont() end end,
								width = "half", default = "Univers 57" },
						[6] = { type = "slider", name = GetString(SI_MM_SETTING_POSITIONSIZE), tooltip = GetString(SI_MM_SETTING_POSITIONSIZE_TOOLTIP),
								min = 6, max = 50, step = 1,
								getFunc = function() return FyrMM.SV.PositionHeight end,
								setFunc = function(value) if value ~= nil then FyrMM.SV.PositionHeight = value MM_SetPositionFont() end end,
								width = "half",	default = 18, },
						[7] = {type = "dropdown", name = GetString(SI_MM_SETTING_POSITIONSTYLE), tooltip = GetString(SI_MM_SETTING_POSITIONSTYLE_TOOLTIP),
								choices = FyrMM.FontStyles,
								getFunc = function() return FyrMM.SV.PositionFontStyle end,
								setFunc = function(value) if value ~= nil then FyrMM.SV.PositionFontStyle = value MM_SetPositionFont() end end,
								width = "full", default = "normal" },
						[8] = { type = "checkbox", name = GetString(SI_MM_SETTING_MAPNAME), tooltip = GetString(SI_MM_SETTING_MAPNAME_TOOLTIP),
								getFunc = function() return not MM_GetHideZoneLabel() end,
								setFunc = function(value) MM_SetHideZoneLabel(not value) end,
								width = "half", default = true, },
						[9] = {	type = "dropdown", name = GetString(SI_MM_SETTING_MAPNAMESTYLE), tooltip = GetString(SI_MM_SETTING_MAPNAMESTYLE_TOOLTIP),
								choices = {"Classic (Map only)", "Map & Area", "Area only"},
								getFunc = function() return FyrMM.SV.ZoneNameContents end,
								setFunc = function(value) if value ~= nil then FyrMM.SV.ZoneNameContents = value FyrMM.UpdateLabels() end end,
								width = "half", default = "Classic (Map only)" },
						[10] = { type = "checkbox", name = GetString(SI_MM_SETTING_MAPNAMEBACKGROUND), tooltip = GetString(SI_MM_SETTING_MAPNAMEBACKGROUND_TOOLTIP),
								getFunc = function() return FyrMM.SV.ShowZoneBackground end,
								setFunc = function(value) MM_SetShowZoneBackground(value) end,
								width = "full", default = true, },
						[11] = {	type = "dropdown", name = GetString(SI_MM_SETTING_MAPNAMELOCATION), tooltip = GetString(SI_MM_SETTING_MAPNAMELOCATION_TOOLTIP),
								choices = {"Default", "Free"},
								getFunc = function() return FyrMM.SV.ZoneFrameLocationOption end,
								setFunc = function(value) MM_SetZoneFrameLocationOption(value) end,
								width = "half", default = "Top" },
						[12] = { type = "colorpicker", name = GetString(SI_MM_SETTING_MAPNAMECOLOR), tooltip = GetString(SI_MM_SETTING_MAPNAMECOLOR_TOOLTIP),
								getFunc = function() return FyrMM.SV.ZoneNameColor.r, FyrMM.SV.ZoneNameColor.g, FyrMM.SV.ZoneNameColor.b, FyrMM.SV.ZoneNameColor.a end,
								setFunc = function(r,g,b,a) MM_SetZoneNameColor(r, g, b, a) end,
								width = "half", },
						[13] = {	type = "dropdown", name = GetString(SI_MM_SETTING_MAPNAMEFONT), tooltip = GetString(SI_MM_SETTING_MAPNAMEFONT_TOOLTIP),
								choices = FyrMM.FontList,
								getFunc = function() return FyrMM.SV.ZoneFont end,
								setFunc = function(value) if value ~= nil then FyrMM.SV.ZoneFont = value MM_SetZoneFont() end end,
								width = "half", default = "Univers 57" },
						[14] = { type = "slider", name = GetString(SI_MM_SETTING_MAPNAMESIZE), tooltip = GetString(SI_MM_SETTING_MAPNAMESIZE_TOOLTIP),
								min = 6, max = 50, step = 1,
								getFunc = function() return FyrMM.SV.ZoneFontHeight end,
								setFunc = function(value) if value ~= nil then FyrMM.SV.ZoneFontHeight = value MM_SetZoneFont() end end,
								width = "half",	default = 18, },
						[15] = {type = "dropdown", name = GetString(SI_MM_SETTING_MAPNAMESTYLE), tooltip = GetString(SI_MM_SETTING_MAPNAMESTYLE_TOOLTIP),
								choices = FyrMM.FontStyles,
								getFunc = function() return FyrMM.SV.ZoneFontStyle end,
								setFunc = function(value) if value ~= nil then FyrMM.SV.ZoneFontStyle = value MM_SetZoneFont() end end,
								width = "full", default = "normal" },
						[16] = { type = "checkbox", name = GetString(SI_MM_SETTING_ZOOMLABEL), tooltip = GetString(SI_MM_SETTING_ZOOMLABEL_TOOLTIP),
								getFunc = function() return MM_GetHideZoomLevel() end,
								setFunc = function(value) MM_SetHideZoomLevel(value) end,
								width = "half",	default = false, },
						[17] = { type = "checkbox", name = GetString(SI_MM_SETTING_CLOCK), tooltip = GetString(SI_MM_SETTING_CLOCK_TOOLTIP),
								getFunc = function() return FyrMM.SV.ShowClock end,
								setFunc = function(value) FyrMM.SV.ShowClock = value end,
								width = "half",	default = false,	},
						[18] = { type = "checkbox", name = GetString(SI_MM_SETTING_FPS), tooltip = GetString(SI_MM_SETTING_FPS_TOOLTIP),
								getFunc = function() return FyrMM.SV.ShowFPS end,
								setFunc = function(value) FyrMM.SV.ShowFPS = value end,
								width = "half",	default = false, }, 
						[19] = { type = "checkbox", name = GetString(SI_MM_SETTING_SPEED), tooltip = GetString(SI_MM_SETTING_SPEED_TOOLTIP),
								getFunc = function() return FyrMM.SV.ShowSpeed end,
								setFunc = function(value) FyrMM.SV.ShowSpeed = value Fyr_MM_Speed:SetHidden(not value) end,
								width = "full",	default = false, },
						[20] = { type = "dropdown", name = GetString(SI_MM_SETTING_SPEEDSTYLE), tooltip = GetString(SI_MM_SETTING_SPEEDSTYLE_TOOLTIP),
								choices = FyrMM.SpeedUnits,
								getFunc = function() return FyrMM.SV.SpeedUnit end,
								setFunc = function(value) if value ~= nil then FyrMM.SV.SpeedUnit = value end end,
								width = "full", default = "ft/s" }, }, },
	[11] = { type = "submenu", name = GetString(SI_MM_SETTING_GROUPOPTIONS),
			controls = {
						[1] = {	type = "dropdown", name = GetString(SI_MM_SETTING_PLAYERSTYLE), tooltip = GetString(SI_MM_SETTING_PLAYERSTYLE_TOOLTIP),
								choices = {GetString(SI_MM_STRING_CLASSIC), GetString(SI_MM_STRING_PLAYERANDCAMERA)},
								getFunc = function() return FyrMM.SV.PPStyle end,
								setFunc = function(value) MM_SetPPStyle(value) end,
								width = "full", default = GetString(SI_MM_PLAYERANDCAMERA) },
						[2] = {	type = "dropdown", name = GetString(SI_MM_SETTING_PLAYERTEXTURE), tooltip = GetString(SI_MM_SETTING_PLAYERTEXTURE_TOOLTIP),
								choices = {"ESO UI Worldmap", "MiniMap", "Vixion Regular", "Vixion Gold"},
								getFunc = function() return FyrMM.SV.PPTextures end,
								setFunc = function(value) MM_SetPPTextures(value) end,
								width = "full", default = "ESO UI Worldmap" },
						[3] = { type = "checkbox", name = GetString(SI_MM_SETTING_COMBATSTATE), tooltip = GetString(SI_MM_SETTING_COMBATSTATE_TOOLTIP),
								getFunc = function() return FyrMM.SV.InCombatState end,
								setFunc = function(value) FyrMM.SV.InCombatState = value end,
								width = "full",	default = true,	},
						[4] = { type = "dropdown", name = GetString(SI_MM_SETTING_LEADER), tooltip = GetString(SI_MM_SETTING_LEADER_TOLLTIP),
								choices = {"Default", "Role", "Class"},
								getFunc = function() return FyrMM.SV.LeaderPin end,
								setFunc = function(value) MM_SetLeaderPin(value) end,
								width = "half", default = "Default", warning = GetString(SI_MM_SETTING_UIWARNING) },
						[5] = { type = "slider", name = GetString(SI_MM_SETTING_LEADERSIZE), tooltip = GetString(SI_MM_SETTING_LEADERSIZE_TOOLTIP),
								min = 8, max = 32, step = 1,
								getFunc = function() return FyrMM.SV.LeaderPinSize end,
								setFunc = function(value) MM_SetLeaderPinSize(value) end,
								width = "half",	default = 32, },
						[6] = { type = "colorpicker", name = GetString(SI_MM_SETTING_LEADERCOLOR), tooltip = GetString(SI_MM_SETTING_LEADERCOLOR_TOOLTIP),
								getFunc = function() return FyrMM.SV.LeaderPinColor.r, FyrMM.SV.LeaderPinColor.g, FyrMM.SV.LeaderPinColor.b, FyrMM.SV.LeaderPinColor.a end,
								setFunc = function(r,g,b,a) MM_SetLeaderPinColor(r, g, b, a) end,
								width = "full", },
						[7] = { type = "colorpicker", name = GetString(SI_MM_SETTING_LEADERDEADCOLOR), tooltip = GetString(SI_MM_SETTING_LEADERDEADCOLOR_TOOLTIP),
								getFunc = function() return FyrMM.SV.LeaderDeadPinColor.r, FyrMM.SV.LeaderDeadPinColor.g, FyrMM.SV.LeaderDeadPinColor.b, FyrMM.SV.LeaderDeadPinColor.a end,
								setFunc = function(r,g,b,a) MM_SetLeaderDeadPinColor(r, g, b, a) end,
								width = "full", },
						[8] = { type = "dropdown", name = GetString(SI_MM_SETTING_MEMBER), tooltip = GetString(SI_MM_SETTING_MEMBER_TOLLTIP),
								choices = {"Default", "Role", "Class"},
								getFunc = function() return FyrMM.SV.MemberPin end,
								setFunc = function(value) MM_SetMemberPin(value) end,
								width = "half", default = "Default", warning = GetString(SI_MM_SETTING_UIWARNING) },
						[9] = { type = "slider", name = GetString(SI_MM_SETTING_MEMBERSIZE), tooltip = GetString(SI_MM_SETTING_MEMBERSIZE_TOOLTIP),
								min = 8, max = 32, step = 1,
								getFunc = function() return FyrMM.SV.MemberPinSize end,
								setFunc = function(value) MM_SetMemberPinSize(value) end,
								width = "half",	default = 32, },
						[10] = { type = "colorpicker", name = GetString(SI_MM_SETTING_MEMBERCOLOR), tooltip = GetString(SI_MM_SETTING_MEMBERCOLOR_TOOLTIP),
								getFunc = function() return FyrMM.SV.MemberPinColor.r, FyrMM.SV.MemberPinColor.g, FyrMM.SV.MemberPinColor.b, FyrMM.SV.MemberPinColor.a end,
								setFunc = function(r,g,b,a) MM_SetMemberPinColor(r, g, b, a) end,
								width = "full", },
						[11] = { type = "colorpicker", name = GetString(SI_MM_SETTING_MEMBERDEADCOLOR), tooltip = GetString(SI_MM_SETTING_MEMBERDEADCOLOR_TOOLTIP),
								getFunc = function() return FyrMM.SV.MemberDeadPinColor.r, FyrMM.SV.MemberDeadPinColor.g, FyrMM.SV.MemberDeadPinColor.b, FyrMM.SV.MemberDeadPinColor.a end,
								setFunc = function(r,g,b,a) MM_SetMemberDeadPinColor(r, g, b, a) end,
								width = "full", },
						[12] = { type = "checkbox", name = GetString(SI_MM_SETTING_SHOWGROUPLABELS), tooltip = GetString(SI_MM_SETTING_SHOWGROUPLABELS_TOOLTIP),
								getFunc = function() return FyrMM.SV.ShowGroupLabels end,
								setFunc = function(value) FyrMM.SV.ShowGroupLabels = value end,
								width = "full",	default = false,	},
						[13] = { type = "button", name = GetString(SI_MM_SETTING_RELOADUI), tooltip = GetString(SI_MM_SETTING_RELOADUI_TOOLTIP),
								func = function() ReloadUI("ingame") end, width = "full", }, }, },
	[12] = { type = "submenu", name = GetString(SI_MM_SETTING_BORDEROPTIONS),
			controls = {
						[1] = { type = "checkbox", name = GetString(SI_MM_SETTING_BORDERPINS), tooltip = GetString(SI_MM_SETTING_BORDERPINS_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderPins end,
							setFunc = function(value) MM_SetBorderPins(value) end,
							width = "full",	default = true,	},
						[2] = { type = "checkbox", name = GetString(SI_MM_SETTING_ASSISTED), tooltip = GetString(SI_MM_SETTING_ASSISTED_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderPinsOnlyAssisted end,
							setFunc = function(value) MM_SetBorderPinsOnlyAssisted(value) end,
							width = "full",	default = true,	},
						[3] = { type = "checkbox", name = GetString(SI_MM_SETTING_LEADERONLY), tooltip = GetString(SI_MM_SETTING_LEADERONLY_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderPinsOnlyLeader end,
							setFunc = function(value) MM_SetBorderPinsOnlyLeader(value) end,
							width = "full",	default = true,	},
						[4] = { type = "checkbox", name = GetString(SI_MM_SETTING_WAYPOINT), tooltip = GetString(SI_MM_SETTING_WAYPOINT_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderPinsWaypoint end,
							setFunc = function(value) MM_SetBorderPinsWaypoint(value) end,
							width = "full",	default = true,	},
						[5] = { type = "checkbox", name = GetString(SI_MM_SETTING_BANK), tooltip = GetString(SI_MM_SETTING_BANK_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderPinsBank end,
							setFunc = function(value) FyrMM.SV.BorderPinsBank = value end,
							width = "full",	default = true,	},
						[6] = { type = "checkbox", name = GetString(SI_MM_SETTING_STABLES), tooltip = GetString(SI_MM_SETTING_STABLES_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderPinsStables end,
							setFunc = function(value) FyrMM.SV.BorderPinsStables = value end,
							width = "full",	default = true,	},							
						[7] = { type = "checkbox", name = GetString(SI_MM_SETTING_WAYSHRINE), tooltip = GetString(SI_MM_SETTING_WAYSHRINE_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderWayshrine end,
							setFunc = function(value) FyrMM.SV.BorderWayshrine = value end,
							width = "full",	default = true,	},								
						[8] = { type = "checkbox", name = GetString(SI_MM_SETTING_TREASURE), tooltip = GetString(SI_MM_SETTING_TREASURE_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderTreasures end,
							setFunc = function(value) FyrMM.SV.BorderTreasures = value end,
							width = "full",	default = true,	},
						[9] = { type = "checkbox", name = GetString(SI_MM_SETTING_QUESTGIVERS), tooltip = GetString(SI_MM_SETTING_QUESTGIVERS_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderQuestGivers end,
							setFunc = function(value) FyrMM.SV.BorderQuestGivers = value end,
							width = "full",	default = true,	},
						[10] = { type = "checkbox", name = GetString(SI_MM_SETTING_CRAFTING), tooltip = GetString(SI_MM_SETTING_CRAFTING_TOOLTIP),
							getFunc = function() return FyrMM.SV.BorderCrafting end,
							setFunc = function(value) FyrMM.SV.BorderCrafting = value end,
							width = "full",	default = false, }, }, },
	[13] = { type = "submenu", name = GetString(SI_MM_SETTING_PERFORMANCEOPTIONS),
			controls = {
						[1] = { type = "slider", name = GetString(SI_MM_SETTING_POSITIONREFRESH), tooltip = GetString(SI_MM_SETTING_POSITIONREFRESH_TOOLTIP),
								min = 10, max = 250, step = 1,
								getFunc = function() return MM_GetMapRefreshRate() end,
								setFunc = function(value) MM_SetMapRefreshRate(value) end,
								width = "half",	default = 60, },
						[2] = { type = "slider", name = GetString(SI_MM_SETTING_PINREFRESH), tooltip = GetString(SI_MM_SETTING_PINREFRESH_TOOLTIP),
								min = 100, max = 2000, step = 1,
								getFunc = function() return MM_GetPinRefreshRate() end,
								setFunc = function(value) MM_SetPinRefreshRate(value) end,
								width = "half",	default = 600, },
--						[3] = { type = "slider", name = GetString(SI_MM_SETTING_TEXTUREREFRESH), tooltip = GetString(SI_MM_SETTING_TEXTUREREFRESH_TOOLTIP),
--								min = 300, max = 5000, step = 1,
--								getFunc = function() return MM_GetViewRefreshRate() end,
--								setFunc = function(value) MM_SetViewRefreshRate(value) end,
--								width = "half",	default = 2500,	},
						[3] = { type = "slider", name = GetString(SI_MM_SETTING_ZONEREFRESH), tooltip = GetString(SI_MM_SETTING_ZONEREFRESH_TOOLTIP),
								min = 25, max = 600, step = 1,
								getFunc = function() return MM_GetZoneRefreshRate() end,
								setFunc = function(value) MM_SetZoneRefreshRate(value) end,
								width = "half",	default = 100, },
						[4] = { type = "slider", name = GetString(SI_MM_SETTING_AVAREFRESH), tooltip = GetString(SI_MM_SETTING_AVA_REFRESH_TOOLTIP),
								min = 900, max = 8000, step = 1,
								getFunc = function() return MM_GetKeepNetworkRefreshRate() end,
								setFunc = function(value) MM_SetKeepNetworkRefreshRate(value) end,
								width = "half",	default = 2000, },
--						[5] = { type = "slider", name = GetString(SI_MM_SETTING_SCROLLSTEPPING), tooltip = GetString(SI_MM_SETTING_SCROLLSTEPPING_TOOLTIP),
--								min = 0.1, max = 5, step = 0.1,
--								getFunc = function() return MM_GetMapStepping() end,
--								setFunc = function(value) MM_SetMapStepping(value) end,
--								width = "half",	default = 2, },
						[5] = { type = "slider", name = GetString(SI_MM_SETTING_CHUNKDELAY), tooltip = GetString(SI_MM_SETTING_CHUNKDELAY_TOOLTIP),
								min = 10, max = 150, step = 1,
								getFunc = function() return FyrMM.SV.ChunkDelay end,
								setFunc = function(value) FyrMM.SV.ChunkDelay = value end,
								width = "half",	default = 50, },
						[6] = { type = "slider", name = GetString(SI_MM_SETTING_CHUNKSIZE), tooltip = GetString(SI_MM_SETTING_CHUNKSIZE_TOOLTIP),
								min = 10, max = 200, step = 1,
								getFunc = function() return FyrMM.SV.ChunkSize end,
								setFunc = function(value) FyrMM.SV.ChunkSize = value end,
								width = "half",	default = 50, },
   						[7] = { type = "checkbox", name = GetString(SI_MM_SETTING_WORLDMAPREFRESH), tooltip = GetString(SI_MM_SETTING_WORLDMAPREFRESH_TOOLTIP),
								getFunc = function() return FyrMM.SV.WorldMapRefresh end,
								setFunc = function(value) FyrMM.SV.WorldMapRefresh = value end,
								width = "full",	default = true,	}, }, },
	}
end

function MM_ResetToDefaults() -- Hardcoded reset
	MM_SetLockPosition(false)
	FyrMM.SV.HideZoneLabel = false
	FyrMM.SV.HideZoomLevel = false
	FyrMM.SV.ShowBorder = true
	FyrMM.SV.ClampedToScreen = true
	local pos = {}
	pos.anchorTo = GetControl(pos.anchorTo)
	Fyr_MM:SetAnchor(TOPLEFT, pos.anchorTo, TOPLEFT, 0, 0)	FyrMM.SV.MapHeight = 280
	FyrMM.SV.MapWidth = 280
	MM_SetMapHeight(280)
	MM_SetMapWidth(280)
	Fyr_MM_Border:SetWidth(288)
	Fyr_MM_Border:SetHeight(288)
	FyrMM.SV.MapAlpha = 100
	FyrMM.SV.Heading = "CAMERA"
	FyrMM.SV.PinScale = 75
	FyrMM.SV.PinTooltips = true
	FyrMM.SV.ShowPosition = true
	FyrMM.SV.HidePvPPins = false
	FyrMM.SV.MouseWheel = true
	MM_SetMapRefreshRate(40)
	MM_SetPinRefreshRate(600)
	MM_SetViewRefreshRate(1200)
	MM_SetZoneRefreshRate(100)
	MM_SetKeepNetworkRefreshRate(300)
	FyrMM.SV.hideCompass = false
	MM_SetHideCompass(false)
	FyrMM.SV.ShowClock = false
	FyrMM.SV.InCombatState = true
	FyrMM.SV.FastTravelEnabled = false
	FyrMM.SV.LeaderPin = "Default"
	FyrMM.SV.LeaderPinSize = 32
	FyrMM.SV.MemberPin = "Default"
	FyrMM.SV.MemberPinSize = 32
	MM_SetLeaderPinColor(1, 1, 1, 1)
	MM_SetLeaderDeadPinColor(1, 1, 1, 1)
	MM_SetMemberPinColor(1, 1, 1, 1)
	MM_SetMemberDeadPinColor(1, 1, 1, 1)
	FyrMM.SV.MapTable = {}
	FyrMM.SV.MapStepping = 2
	FyrMM_ZOOM_INCREMENT_AMOUNT = 1
	FyrMM.SV.ZoomIncrement = 1
	FyrMM.SV.InCombatAutoHide = false
	FyrMM.SV.AfterCombatUnhideDelay = 5
	FyrMM.SV.UseOriginalAPI = true
	MM_SetShowUnexploredPins(true)
	MM_SetUndiscoveredPOIPinColor(.7,.7,.3,.6)
	MM_SetPositionColor(0.996078431372549, 0.92, 0.3137254901960784, 1)
	FyrMM.SV.RotateMap = false
	MM_SetWheelMap(false)
	FyrMM.SV.BorderPins = false
	FyrMM.SV.BorderPinsWaypoint = false
	MM_SetCoordinatesLocation("Top")
	FyrMM.SV.StartupInfo = false
	FyrMM.SV.ZoneNameContents = "Classic (Map only)"
end

function MM_LoadSavedVars()	
	if FyrMM.SV.position ~= nil then
		local pos = {}
		pos.anchorTo = GetControl(pos.anchorTo)
		Fyr_MM:SetAnchor(FyrMM.SV.position.point, pos.anchorTo, FyrMM.SV.position.relativePoint, FyrMM.SV.position.offsetX, FyrMM.SV.position.offsetY)
		Fyr_MM_Bg:SetAnchorFill(Fyr_MM)
	end
	if FyrMM.SV.ZoneList ~= nil then FyrMM.SV.ZoneList = nil end
	if FyrMM.SV.MapHeight ~= nil then MM_SetMapHeight(FyrMM.SV.MapHeight) end
	if FyrMM.SV.MapWidth ~= nil then MM_SetMapWidth(FyrMM.SV.MapWidth) end
	if FyrMM.SV.MapAlpha ~= nil then MM_SetMapAlpha(FyrMM.SV.MapAlpha) end
	if FyrMM.SV.PinScale ~= nil then MM_SetPinScale(FyrMM.SV.PinScale) end
	if FyrMM.SV.ShowBorder ~= nil then MM_SetShowBorder(FyrMM.SV.ShowBorder) end
	if FyrMM.SV.Heading ~= nil then MM_SetHeading(FyrMM.SV.Heading) end
	if FyrMM.SV.ClampedToScreen ~= nil then MM_SetClampedToScreen(FyrMM.SV.ClampedToScreen) end
	if FyrMM.SV.hideCompass ~= nil then MM_SetHideCompass(FyrMM.SV.hideCompass) end
	if FyrMM.SV.ShowPosition ~= nil then MM_SetShowPosition(FyrMM.SV.ShowPosition) end
	if FyrMM.SV.PositionBackground ~= nil then MM_SetPositionBackground(FyrMM.SV.PositionBackground) end
	if FyrMM.SV.PositionColor ~= nil then MM_SetPositionColor(FyrMM.SV.PositionColor.r, FyrMM.SV.PositionColor.g, FyrMM.SV.PositionColor.b, FyrMM.SV.PositionColor.a) end
	if FyrMM.SV.PinTooltips ~= nil then MM_SetPinTooltips(FyrMM.SV.PinTooltips) end
	if FyrMM.SV.FastTravelEnabled ~= nil then MM_SetFastTravelEnabled(FyrMM.SV.FastTravelEnabled) end
	if FyrMM.SV.HidePvPPins ~= nil then MM_SetHidePvPPins(FyrMM.SV.HidePvPPins) end
	if FyrMM.SV.MouseWheel ~= nil then MM_SetMouseWheel(FyrMM.SV.MouseWheel) end
	if FyrMM.SV.WheelTexture ~= nil then MM_SetWheelTexture(FyrMM.SV.WheelTexture) end
	if FyrMM.SV.MapRefreshRate ~= nil then MM_SetMapRefreshRate(FyrMM.SV.MapRefreshRate) end
	if FyrMM.SV.PinRefreshRate ~= nil then MM_SetPinRefreshRate(FyrMM.SV.PinRefreshRate) end
	if FyrMM.SV.ViewRefreshRate ~= nil then MM_SetViewRefreshRate(FyrMM.SV.ViewRefreshRate) end
	if FyrMM.SV.ZoneRefreshRate ~= nil then MM_SetZoneRefreshRate(FyrMM.SV.ZoneRefreshRate) end
	if FyrMM.SV.KeepNetworkRefreshRate ~= nil then MM_SetKeepNetworkRefreshRate(FyrMM.SV.KeepNetworkRefreshRate) end
	if FyrMM.SV.HideZoneLabel ~= nil then MM_SetHideZoneLabel(FyrMM.SV.HideZoneLabel) end
	if FyrMM.SV.ShowZoneBackground ~= nil then MM_SetShowZoneBackground(FyrMM.SV.ShowZoneBackground) end
	if FyrMM.SV.ZoneFrameLocationOption ~= nil then MM_SetZoneFrameLocationOption(FyrMM.SV.ZoneFrameLocationOption) end
	if FyrMM.SV.ZoneFrameAnchor ~= nil then MM_SetZoneFrameAnchor(FyrMM.SV.ZoneFrameAnchor) end
	if FyrMM.SV.ZoneFontHeight ~= nil then MM_SetZoneFont() end
	if FyrMM.SV.ZoneFont ~= nil then MM_SetZoneFont() end
	if FyrMM.SV.ZoneFontStyle ~= nil then MM_SetZoneFont() end
	if FyrMM.SV.PositionFont ~= nil then MM_SetPositionFont() end
	if FyrMM.SV.PositionHeight ~= nil then MM_SetPositionFont() end
	if FyrMM.SV.PositionFontStyle ~= nil then MM_SetPositionFont() end
	if FyrMM.SV.ZoneNameColor ~= nil then MM_SetZoneNameColor(FyrMM.SV.ZoneNameColor.r, FyrMM.SV.ZoneNameColor.g, FyrMM.SV.ZoneNameColor.b, FyrMM.SV.ZoneNameColor.a) end
	if FyrMM.SV.HideZoomLevel ~= nil then MM_SetHideZoomLevel(FyrMM.SV.HideZoomLevel) end
	if FyrMM.SV.ShowClock ~= nil then MM_SetShowClock(FyrMM.SV.ShowClock) end
	if FyrMM.SV.ShowFPS ~= nil then MM_SetShowFPS(FyrMM.SV.ShowFPS) end
	if FyrMM.SV.LeaderPin ~= nil then MM_SetLeaderPin(FyrMM.SV.LeaderPin) end
	if FyrMM.SV.LeaderPinSize ~= nil then MM_SetLeaderPinSize(FyrMM.SV.LeaderPinSize) end
	if FyrMM.SV.LeaderPinColor ~= nil then MM_SetLeaderPinColor(FyrMM.SV.LeaderPinColor.r, FyrMM.SV.LeaderPinColor.g, FyrMM.SV.LeaderPinColor.b, FyrMM.SV.LeaderPinColor.a) end
	if FyrMM.SV.LeaderDeadPinColor ~= nil then MM_SetLeaderDeadPinColor(FyrMM.SV.LeaderDeadPinColor.r, FyrMM.SV.LeaderDeadPinColor.g, FyrMM.SV.LeaderDeadPinColor.b, FyrMM.SV.LeaderDeadPinColor.a) end
	if FyrMM.SV.MemberPin ~= nil then MM_SetMemberPin(FyrMM.SV.MemberPin) end
	if FyrMM.SV.MemberPinSize ~= nil then MM_SetMemberPinSize(FyrMM.SV.MemberPinSize) end
	if FyrMM.SV.MemberPinColor ~= nil then MM_SetMemberPinColor(FyrMM.SV.MemberPinColor.r, FyrMM.SV.MemberPinColor.g, FyrMM.SV.MemberPinColor.b, FyrMM.SV.MemberPinColor.a) end
	if FyrMM.SV.MemberDeadPinColor ~= nil then MM_SetMemberDeadPinColor(FyrMM.SV.MemberDeadPinColor.r, FyrMM.SV.MemberDeadPinColor.g, FyrMM.SV.MemberDeadPinColor.b, FyrMM.SV.MemberDeadPinColor.a) end
	if FyrMM.SV.MapStepping ~=nil then MM_SetMapStepping(FyrMM.SV.MapStepping) end
	if FyrMM.SV.ZoomIncrement ~=nil then FYRMM_ZOOM_INCREMENT_AMOUNT = tonumber(FyrMM.SV.ZoomIncrement) FyrMM.SV.ZoomIncrement = FYRMM_ZOOM_INCREMENT_AMOUNT end
	if FyrMM.SV.DefaultZoomLevel ~= nil then FYRMM_DEFAULT_ZOOM_LEVEL = tonumber(FyrMM.SV.DefaultZoomLevel) FyrMM.SV.DefaultZoomLevel = FYRMM_DEFAULT_ZOOM_LEVEL end
	if FyrMM.SV.InCombatAutoHide ~=nil then MM_SetInCombatAutoHide(FyrMM.SV.InCombatAutoHide) end
	if FyrMM.SV.AfterCombatUnhideDelay ~=nil then MM_SetAfterCombatUnhideDelay(FyrMM.SV.AfterCombatUnhideDelay) end
	if FyrMM.SV.LockPosition ~=nil then MM_SetLockPosition(FyrMM.SV.LockPosition) end
	if FyrMM.SV.UseOriginalAPI ~= nil then MM_SetUseOriginalAPI(FyrMM.SV.UseOriginalAPI) end
	if FyrMM.SV.ShowUnexploredPins ~= nil then MM_SetShowUnexploredPins(FyrMM.SV.ShowUnexploredPins) end
	if FyrMM.SV.RotateMap ~= nil then MM_SetRotateMap(FyrMM.SV.RotateMap) end
	if FyrMM.SV.WheelMap ~= nil then MM_SetWheelMap(FyrMM.SV.WheelMap) end
	if FyrMM.SV.BorderPins ~= nil then MM_SetBorderPins(FyrMM.SV.BorderPins) end
	if FyrMM.SV.BorderPinsWaypoint ~= nil then MM_SetBorderPinsWaypoint(FyrMM.SV.BorderPinsWaypoint) end
	if FyrMM.SV.CorrdinatesLocation ~= nil then MM_SetCoordinatesLocation(FyrMM.SV.CorrdinatesLocation) end
	if FyrMM.SV.CoordinatesAnchor ~= nil then MM_SetCoordinatesAnchor(FyrMM.SV.CoordinatesAnchor) end
	if FyrMM.SV.PPStyle ~= nil then MM_SetPPStyle(FyrMM.SV.PPStyle) end
	if FyrMM.SV.PPTextures ~= nil then MM_SetPPTextures(FyrMM.SV.PPTextures) end
	if FyrMM.SV.MenuDisabled ~= nil then Fyr_MM_Menu:SetHidden(FyrMM.SV.MenuDisabled) end
	if FyrMM.SV.MenuTexture ~= nil then MM_SetMenuTexture(FyrMM.SV.MenuTexture) end
	if FyrMM.SV.ShowSpeed ~= nil then Fyr_MM_Speed:SetHidden(not FyrMM.SV.ShowSpeed) end
	if FyrMM.SV.MapSettings ~= nil then MM_Upgrade_MapList() end -- Upgrade from version earlier to v2.15
	if FyrMM.SV.DisableSubzones ~= nil then FyrMM.DisableSubzones = FyrMM.SV.DisableSubzones end
	if FyrMM.SV.UndiscoveredPOIPinColor ~= nil then MM_SetUndiscoveredPOIPinColor(FyrMM.SV.UndiscoveredPOIPinColor.r, FyrMM.SV.UndiscoveredPOIPinColor.g, FyrMM.SV.UndiscoveredPOIPinColor.b, FyrMM.SV.UndiscoveredPOIPinColor.a) end
end

function MM_RefreshPanel()
	if not FyrMiniMap:IsHidden() then FyrMiniMap:SetHidden(true) FyrMM.LAM:OpenToPanel(FyrMM.CPL) end -- If Settings are open, have to refresh them
end

function FyrMM_CommandHandler(text)
	if text == "help" or text == "hel" then
		d(GetString(SI_MM_STRING_COMMANDHELP1))
		d("/fyrmm fpstest - "..GetString(SI_MM_STRING_COMMANDHELP2))
		d("/fyrmm help - "..GetString(SI_MM_STRING_COMMANDHELP3))
		d("/fyrmm hide - "..GetString(SI_MM_STRING_COMMANDHELP4))
		d("/fyrmm location - "..GetString(SI_MM_STRING_COMMANDHELP5))
		d("/fyrmm reset - "..GetString(SI_MM_STRING_COMMANDHELP6))
		d("/fyrmm unhide - "..GetString(SI_MM_STRING_COMMANDHELP7))
		d("/fyrmm version - "..GetString(SI_MM_STRING_COMMANDHELP8))
		d("/fyrmm debug - "..GetString(SI_MM_STRING_COMMANDHELP9))
		d("/fyrmmset - "..GetString(SI_MM_STRING_COMMANDHELP10))
	elseif text == "reset" or text == "res" then
		MM_ResetToDefaults()
		MM_RefreshPanel()
		FyrMM.Reload()
	elseif text == "fpstest" or text == "fps" then -- FPS test 
		FyrMM.FpsTest = not  FyrMM.FpsTest
	if  FyrMM.FpsTest then
		FyrMM.Fps = 0
		FyrMM.FpsRaw = 0
		d("["..GetTimeString().."] "..GetString(SI_MM_STRING_FPSRECORDING1))
		d("["..GetTimeString().."] "..GetString(SI_MM_STRING_FPSRECORDING2))
		d("["..GetTimeString().."] "..GetString(SI_MM_STRING_FPSRECORDING3))
		d("["..GetTimeString().."] "..GetString(SI_MM_STRING_FPSRECORDING4))
	else
		d("["..GetTimeString().."] "..GetString(SI_MM_STRING_FPSRECORDING5))
		d("["..GetTimeString().."] "..GetString(SI_MM_STRING_FPSRECORDING6))
		d(string.format(GetString(SI_MM_STRING_FPSRECORDING7).." = %f, "..GetString(SI_MM_STRING_FPSRECORDING8).." = %f", FyrMM.Fps, FyrMM.FpsRaw))
	end
	elseif text == "hide" or text == "hid" then
		FyrMM.Visible = false
	elseif text == "show" or text == "unhide" or text == "sho" or text == "unh" then
		FyrMM.Visible = true
	elseif text == "version" or text == "info" or text == "ver" or text == "inf" then
		d("|ceeeeeeMiniMap by |c006600Fyrakin |ceeeeee v"..FyrMM.Panel.version.."|r")
	elseif text == "loc" or text == "location" then
		local x, y, heading = GetMapPlayerPosition("player")
		d(string.format(GetString(SI_MM_STRING_COORDINATES).." x=%g, y=%g @ %s",("%05.04f"):format(zo_round(x*1000000)/10000),("%05.04f"):format(zo_round(y*1000000)/10000), Fyr_MM_Zone:GetText()))
	elseif text == "debug" or text == "deb" then
		if FyrMM.MovementSpeedMax == nil then FyrMM.MovementSpeedMax = 0 end
			d("Map: "..FyrMM.currentMap.filename..", Stored size: "..string.format("%g",("%05.04f"):format(zo_round(FyrMM.currentMap.TrueMapSize*100)/100))..", suggested size: "..string.format("%g",("%05.04f"):format(zo_round(FyrMM.currentMap.TrueMapSize*100*(9/(FyrMM.MovementSpeedMax*100)))/100)))
		elseif text == "report" or text == "rep" then
			local text = ""
			for i, v in pairs(FyrMM.SV.MapTable) do
				if v.MapSize and FyrMM.MapTable[i] then
					if FyrMM.MapTable[i].MapSize == 0 and v.MapSize > 0 then
						text = text ..i.." = "..string.format("%g",("%05.04f"):format(v.MapSize)).."\n"
					end
				end
			end
	if text == "" then d("Nothing to report.")
	else
  		MAIN_MENU:ShowScene("mailSend")
		local to = GetControl ("ZO_MailSendToField")
		local subject = GetControl ("ZO_MailSendSubjectField")
		local body = GetControl ("ZO_MailSendBodyField")
		if to ~= nil then
			to:SetText("@BigAnvil")
		end
		if subject ~= nil then
			subject:SetText("Map size debug report")
		end
		if body ~= nil then
			body:SetText(text)
		end
	end
  else
	d("/fyrmm help - "..GetString(SI_MM_STRING_COMMANDHELP11))
  end
end

SLASH_COMMANDS["/fyrmm"] = FyrMM_CommandHandler