local addon = {
	name = "VotansImprovedLocations",
	allianceOrder =
	{
		[ALLIANCE_DAGGERFALL_COVENANT] =
		{
			[ALLIANCE_DAGGERFALL_COVENANT] = 1,
			[ALLIANCE_ALDMERI_DOMINION] = 2,
			[ALLIANCE_EBONHEART_PACT] = 3,
			[100] = 100,
			[999] = 999,
		},
		[ALLIANCE_ALDMERI_DOMINION] =
		{
			[ALLIANCE_ALDMERI_DOMINION] = 1,
			[ALLIANCE_EBONHEART_PACT] = 2,
			[ALLIANCE_DAGGERFALL_COVENANT] = 3,
			[100] = 100,
			[999] = 999,
		},
		[ALLIANCE_EBONHEART_PACT] =
		{
			[ALLIANCE_EBONHEART_PACT] = 1,
			[ALLIANCE_DAGGERFALL_COVENANT] = 2,
			[ALLIANCE_ALDMERI_DOMINION] = 3,
			[100] = 100,
			[999] = 999,
		},
	},
	-- Thanks to Enodoc
	levels =
	{
		-- difficulty
		{
			{ 4, 6 },
			{ 3, 15 },
			{ 16, 23 },
			{ 24, 31 },
			{ 31, 37 },
			{ 37, 43 },
		},
		{
			{ 51, 51 },
			{ 51, 51 },
			{ 52, 52 },
			{ 53, 53 },
			{ 54, 54 },
			{ 55, 55 },
		},
		{
			{ 56, 56 },
			{ 56, 56 },
			{ 57, 57 },
			{ 58, 58 },
			{ 59, 59 },
			{ 60, 60 },
		},
	},
	locations =
	{
		-- [1] =
		{
			-- "Tamriel",
			alliance = 999,
			zoneIndex = 0,
		},
		-- [2] =
		{
			-- "Glenumbra",
			alliance = ALLIANCE_DAGGERFALL_COVENANT,
			zoneIndex = 2,
			zoneOrder = 2,
		},
		-- [3] =
		{
			-- "Kluftspitze",
			alliance = ALLIANCE_DAGGERFALL_COVENANT,
			zoneIndex = 5,
			zoneOrder = 4,
		},
		-- [4] =
		{
			-- "Sturmhafen",
			alliance = ALLIANCE_DAGGERFALL_COVENANT,
			zoneIndex = 4,
			zoneOrder = 3,
		},
		-- [5] =
		{
			-- "Alik'r-W�ste",
			alliance = ALLIANCE_DAGGERFALL_COVENANT,
			zoneIndex = 17,
			zoneOrder = 5,
		},
		-- [6] =
		{
			-- "Bangkorai",
			alliance = ALLIANCE_DAGGERFALL_COVENANT,
			zoneIndex = 14,
			zoneOrder = 6,
		},
		-- [7] =
		{
			-- "Grahtwald",
			alliance = ALLIANCE_ALDMERI_DOMINION,
			zoneIndex = 180,
			zoneOrder = 3,
		},
		-- [8] =
		{
			-- "Malabal Tor",
			alliance = ALLIANCE_ALDMERI_DOMINION,
			zoneIndex = 11,
			zoneOrder = 5,
		},
		-- [9] =
		{
			-- "Schattenfenn",
			alliance = ALLIANCE_EBONHEART_PACT,
			zoneIndex = 19,
			zoneOrder = 4,
		},
		-- [10] =
		{
			-- "Deshaan",
			alliance = ALLIANCE_EBONHEART_PACT,
			zoneIndex = 10,
			zoneOrder = 3,
		},
		-- [11] =
		{
			-- "Steinf�lle",
			alliance = ALLIANCE_EBONHEART_PACT,
			zoneIndex = 9,
			zoneOrder = 2,
		},
		-- [12] =
		{
			-- "Rift",
			alliance = ALLIANCE_EBONHEART_PACT,
			zoneIndex = 16,
			zoneOrder = 6,
		},
		-- [13] =
		{
			-- "Ostmarsch",
			alliance = ALLIANCE_EBONHEART_PACT,
			zoneIndex = 15,
			zoneOrder = 5,
		},
		-- [14] =
		{
			-- "Cyrodiil",
			alliance = 100,
			zoneIndex = 37,
		},
		-- [15] =
		{
			-- "Auridon",
			alliance = ALLIANCE_ALDMERI_DOMINION,
			zoneIndex = 178,
			zoneOrder = 2,
		},
		-- [16] =
		{
			-- "Gr�nschatten",
			alliance = ALLIANCE_ALDMERI_DOMINION,
			zoneIndex = 18,
			zoneOrder = 4,
		},
		-- [17] =
		{
			-- "Schnittermark",
			alliance = ALLIANCE_ALDMERI_DOMINION,
			zoneIndex = 179,
			zoneOrder = 6,
		},
		-- [18] =
		{
			-- "Bal Foyen",
			alliance = ALLIANCE_EBONHEART_PACT,
			zoneIndex = 110,
			zoneOrder = 1,
		},
		-- [19] =
		{
			-- "Stros M'Kai",
			alliance = ALLIANCE_DAGGERFALL_COVENANT,
			zoneIndex = 292,
			zoneOrder = 1,
		},
		-- [20] =
		{
			-- "Betnikh",
			alliance = ALLIANCE_DAGGERFALL_COVENANT,
			zoneIndex = 293,
			zoneOrder = 1,
		},
		-- [21] =
		{
			-- "Khenarthis Rast",
			alliance = ALLIANCE_ALDMERI_DOMINION,
			zoneIndex = 294,
			zoneOrder = 1,
		},
		-- [22] =
		{
			-- "�dfels",
			alliance = ALLIANCE_EBONHEART_PACT,
			zoneIndex = 109,
			zoneOrder = 1,
		},
		-- [23] =
		{
			-- "Kalthafen",
			alliance = 100,
			zoneIndex = 154,
			zoneLevel = { 44, 50 },
		},
		-- [24] =
		{
			-- "Aurbis",
			alliance = 999,
			zoneIndex = 0,
		},
		-- [25] =
		{
			-- "Kargstein",
			alliance = 100,
			zoneIndex = 353,
			zoneLevel = { 61, 64 },
		},
		-- [26] =
		{
			-- "Kaiserstadt",
			alliance = 100,
			zoneIndex = 335,
		},
		-- [27] =
		{
			-- "Wrothgar",
			alliance = 100,
			zoneIndex = 396,
			zoneLevel = { 65, 66 },
		},
		-- [28] =
		{
			-- "Abah's Landing",
			alliance = 100,
			zoneIndex = 513,
			zoneLevel = { 65, 66 },
		},
	},
}

local em = GetEventManager()
local LOCATION_TYPE_ID = 2
local ALLIANCE_TYPE_ID = 3
local MAP_TYPE_ID = 4
local RECENT_TYPE_ID = 5

local headerColor = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_TEXT_COLORS, INTERFACE_TEXT_COLOR_SELECTED))
local selectedColor = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_TEXT_COLORS, INTERFACE_TEXT_COLOR_NORMAL))
local disabledColor = ZO_ColorDef:New(GetInterfaceColor(INTERFACE_COLOR_TYPE_TEXT_COLORS, INTERFACE_TEXT_COLOR_DISABLED))

local SORT_LOCATION_NAME = "Name"
local SORT_LOCATION_LEVEL_ASC = "LevelAsc"
local SORT_LOCATION_LEVEL_DSC = "LevelDsc"

local SI_TOOLTIP_ITEM_NAME = GetString(SI_TOOLTIP_ITEM_NAME)

local function HideRowHighlight(rowControl, hidden, unselected)
	if not rowControl then return end
	if not ZO_ScrollList_GetData(rowControl) then return end

	local highlight = rowControl:GetNamedChild("Highlight")

	if highlight then
		if not highlight.animation then
			highlight.animation = ANIMATION_MANAGER:CreateTimelineFromVirtual("ShowOnMouseOverLabelAnimation", highlight)
		end

		if highlight.animation:IsPlaying() then
			highlight.animation:Stop()
		end
		if unselected ~= false then
			highlight:SetTexture("esoui/art/miscellaneous/listitem_selectedhighlight.dds")
		else
			highlight:SetTexture("esoui/art/miscellaneous/listitem_highlight.dds")
		end
		if hidden and unselected ~= false then
			highlight.animation:PlayBackward()
		else
			highlight.animation:PlayForward()
		end
	end
end

function addon:ShowLevel(levelLabel, location)
	local function FormatLevel(min, max)
		if min ~= max then
			return zo_strjoin("-", GetLevelOrVeteranRankString(min > 50 and 50 or min, min > 50 and(min - 50) or 0, 23), max > 50 and(max - 50) or max)
		else
			return GetLevelOrVeteranRankString(min > 50 and 50 or min, min > 50 and(min - 50) or 0, 23)
		end
	end

	if location and self.account.showLevels then
		if location.zoneOrder then
			local difficulty = location.allianceOrder
			levelLabel:SetText(FormatLevel(unpack(self.levels[difficulty][location.zoneOrder])))
		elseif location.zoneLevel then
			levelLabel:SetText(FormatLevel(unpack(location.zoneLevel)))
		elseif location.mapContentType == MAP_CONTENT_AVA then
			levelLabel:SetText("|t23:23:/esoui/art/icons/mapkey/mapkey_keep.dds|t")
		else
			levelLabel:SetText("")
		end
	else
		levelLabel:SetText("")
	end
end

local function onMouseEnter(rowControl)
	HideRowHighlight(rowControl, false)
end

function addon:SetupLocation(rowControl, rowData)
	local function onMouseExit(rowControl)
		local data = ZO_ScrollList_GetData(rowControl)
		HideRowHighlight(rowControl, true, WORLD_MAP_LOCATIONS.selectedMapIndex ~= data.index)
	end
	local function onMouseClick(rowControl)
		local data = ZO_ScrollList_GetData(rowControl)
		ZO_WorldMap_SetMapByIndex(data.index)
		PlaySound(SOUNDS.MAP_LOCATION_CLICKED)
	end

	local listDisabled = WORLD_MAP_LOCATIONS:GetDisabled()
	local locationLabel = rowControl:GetNamedChild("Location")
	locationLabel:SetText(rowData.locationName)
	locationLabel:SetColor((listDisabled and disabledColor or selectedColor):UnpackRGB())

	local levelLabel = rowControl:GetNamedChild("Level")
	self:ShowLevel(levelLabel, self.locations[rowData.index])

	HideRowHighlight(rowControl, true, WORLD_MAP_LOCATIONS.selectedMapIndex ~= rowData.index)
	rowControl:SetMouseEnabled(not listDisabled)
	rowControl:SetHeight(25)
	rowControl:SetHandler("OnMouseEnter", onMouseEnter)
	rowControl:SetHandler("OnMouseExit", onMouseExit)
	rowControl:SetHandler("OnMouseUp", onMouseClick)
end

function addon:SetupRecentLocation(rowControl, rowData)
	local function onMouseExit(rowControl)
		HideRowHighlight(rowControl, true)
	end
	local function onMouseClick(rowControl)
		local rowData = ZO_ScrollList_GetData(rowControl)
		ZO_WorldMap_SetMapByIndex(self.player.recent[rowData.index])
		PlaySound(SOUNDS.MAP_LOCATION_CLICKED)
	end

	local listDisabled = WORLD_MAP_LOCATIONS:GetDisabled()
	local locationLabel = rowControl:GetNamedChild("Location")
	local levelLabel = rowControl:GetNamedChild("Level")

	rowData = self.player.recent[rowData.index] or 0
	if rowData > 0 then
		local location = self.locations[rowData]

		locationLabel:SetText(location.locationName)
		rowControl:SetMouseEnabled(not listDisabled)

		local levelLabel = rowControl:GetNamedChild("Level")
		self:ShowLevel(levelLabel, location)
	else
		locationLabel:SetText("-")
		levelLabel:SetText("")
		rowControl:SetMouseEnabled(false)
	end

	locationLabel:SetColor((listDisabled and disabledColor or selectedColor):UnpackRGB())
	rowControl:SetHeight(25)
	rowControl:SetHandler("OnMouseEnter", onMouseEnter)
	rowControl:SetHandler("OnMouseExit", onMouseExit)
	rowControl:SetHandler("OnMouseUp", onMouseClick)
end

function addon:SetupHeader(rowControl, rowData)
	local locationLabel = rowControl:GetNamedChild("Location")
	local texture = rowControl:GetNamedChild("Texture")

	texture:ClearAnchors()

	if rowData.alliance < 100 then
		locationLabel:SetText(zo_strformat(SI_ALLIANCE_NAME, GetAllianceName(rowData.alliance)))
		texture:SetTexture(ZO_GetAllianceIcon(rowData.alliance))
		texture:SetDimensions(32, 64)
		texture:SetAnchor(TOPLEFT)
		rowControl:SetHeight(50)
	elseif rowData.alliance == 100 then
		locationLabel:SetText(zo_strformat(GetString(SI_MAPTRANSITLINEALLIANCE1)))
		texture:SetTexture("esoui/art/compass/ava_flagneutral.dds")
		texture:SetDimensions(64, 64)
		texture:SetAnchor(TOPLEFT, nil, TOPLEFT, -16)
		rowControl:SetHeight(50)
	end

	locationLabel:SetColor(headerColor:UnpackRGB())
end

function addon:SetupMapType(rowControl, rowData)
	local locationLabel = rowControl:GetNamedChild("Location")
	local texture = rowControl:GetNamedChild("Texture")

	if rowData.alliance == 999 then
		locationLabel:SetText(zo_strformat(GetString(SI_MAP_MENU_WORLD_MAP)))
		texture:ClearAnchors()
		texture:SetTexture("esoui/art/lfg/gamepad/lfg_activityicon_world.dds")
		texture:SetDimensions(32, 32)
		texture:SetAnchor(BOTTOMLEFT)
	elseif rowData.alliance == 1000 then
		locationLabel:SetText("")
		texture:ClearAnchors()
		texture:SetTexture("esoui/art/icons/mapkey/mapkey_elderscroll.dds")
		texture:SetDimensions(48, 48)
		texture:SetAnchor(TOPLEFT)
	end
	rowControl:SetHeight(40)
	locationLabel:SetColor(headerColor:UnpackRGB())
end

local function HookLocations()
	function WORLD_MAP_LOCATIONS:BuildLocationList()
		ZO_ScrollList_Clear(self.list)
		ZO_ScrollList_AddDataType(self.list, LOCATION_TYPE_ID, "VotansLocationRow", 25, function(control, rowData) addon:SetupLocation(control, rowData) end)
		ZO_ScrollList_AddDataType(self.list, ALLIANCE_TYPE_ID, "VotansLocationHeaderRow", 50, function(control, rowData) addon:SetupHeader(control, rowData) end)
		ZO_ScrollList_AddDataType(self.list, MAP_TYPE_ID, "VotansLocationHeaderRow", 40, function(control, rowData) addon:SetupMapType(control, rowData) end)
		ZO_ScrollList_AddDataType(self.list, RECENT_TYPE_ID, "VotansLocationRow", 25, function(control, rowData) addon:SetupRecentLocation(control, rowData) end)

		self.list.mode = 2
		local scrollData = ZO_ScrollList_GetDataList(self.list)
		local playerAlliance = GetUnitAlliance("player")
		local allianceOrder = addon.allianceOrder[playerAlliance]
		allianceOrder[100] = addon.account.allAllianceOnTop and 0 or 100

		local mapData = { }
		local GetMapInfo = GetMapInfo
		local location
		for i = 1, GetNumMaps() do
			local mapName, mapType, mapContentType, zoneIndex = GetMapInfo(i)
			if mapName ~= "" then
				location = addon.locations[i]
				if location == nil then
					location = ZO_DeepTableCopy(addon.locations[27])
					addon.locations[i] = location
				end

				-- assert(location ~= nil, zo_strjoin(" ", "New yet unsupported zone ", i, mapName))
				location.allianceOrder = allianceOrder[location.alliance]
				location.locationName = LocalizeString(SI_TOOLTIP_ITEM_NAME, mapName)
				location.mapType = mapType
				location.mapContentType = mapContentType
				mapData[#mapData + 1] = { locationName = location.locationName, index = i, mapType = mapType, mapContentType = mapContentType, alliance = location.alliance, allianceOrder = location.allianceOrder, zoneOrder = location.zoneOrder }
			end
		end

		local sortLoc
		if addon.account.sortBy == SORT_LOCATION_NAME then
			sortLoc = function(a, b) return a.locationName < b.locationName end
		elseif addon.account.sortBy == SORT_LOCATION_LEVEL_ASC then
			sortLoc = function(a, b)
				if a.zoneOrder ~= b.zoneOrder then
					return a.zoneOrder < b.zoneOrder
				else
					return a.locationName < b.locationName
				end
			end
		else
			sortLoc = function(a, b)
				if a.zoneOrder ~= b.zoneOrder then
					return a.zoneOrder > b.zoneOrder
				else
					return a.locationName > b.locationName
				end
			end
		end

		table.sort(mapData, function(a, b)
			if a.allianceOrder == b.allianceOrder then
				if a.allianceOrder > 0 and a.allianceOrder < 100 then
					return sortLoc(a, b)
				else
					if a.mapContentType == b.mapContentType then
						return a.locationName < b.locationName
					else
						return a.mapContentType < b.mapContentType
					end
				end
			else
				return a.allianceOrder < b.allianceOrder
			end
		end )

		scrollData[#scrollData + 1] = ZO_ScrollList_CreateDataEntry(MAP_TYPE_ID, { alliance = 1000, })
		for i = 1, #addon.player.recent do
			scrollData[#scrollData + 1] = ZO_ScrollList_CreateDataEntry(RECENT_TYPE_ID, { index = i })
		end
		addon.nextRecentIndex = #scrollData

		local lastAlliance = -1
		for i = 1, #mapData do
			local entry = mapData[i]
			if entry.alliance ~= lastAlliance then
				lastAlliance = entry.alliance
				if entry.alliance < 999 then
					scrollData[#scrollData + 1] = ZO_ScrollList_CreateDataEntry(ALLIANCE_TYPE_ID, entry)
				else
					scrollData[#scrollData + 1] = ZO_ScrollList_CreateDataEntry(MAP_TYPE_ID, entry)
				end
			end

			scrollData[#scrollData + 1] = ZO_ScrollList_CreateDataEntry(LOCATION_TYPE_ID, entry)
		end

		ZO_ScrollList_Commit(self.list)
	end

	local orgUpdateSelectedMap = WORLD_MAP_LOCATIONS.UpdateSelectedMap
	function WORLD_MAP_LOCATIONS:UpdateSelectedMap(...)
		local zoneName = GetUnitZone("player")
		local mapIndex = addon.zoneNameToMapIndex[zoneName]
		if mapIndex and addon.player.recent[1] ~= mapIndex then
			local recent = addon.player.recent
			local oldCount = #recent
			for i = oldCount, 1, -1 do
				if recent[i] and recent[i] == mapIndex then table.remove(recent, i) end
			end
			table.insert(recent, 1, mapIndex)

			local count = #recent
			while count > 5 do
				table.remove(recent, count)
				count = count - 1
			end

			if count > oldCount then
				-- can be one new entry, only
				local scrollData = ZO_ScrollList_GetDataList(self.list)
				addon.nextRecentIndex = addon.nextRecentIndex + 1
				table.insert(scrollData, addon.nextRecentIndex, ZO_ScrollList_CreateDataEntry(RECENT_TYPE_ID, { index = count }))
				ZO_ScrollList_Commit(self.list)
			end
		end

		return orgUpdateSelectedMap(self, ...)
	end
end

function addon:SetupControls()
	self.list = WORLD_MAP_LOCATIONS.list
end

function addon:GetZoneMapping()
	local list = { }
	self.zoneNameToMapIndex = list
	local GetZoneNameByIndex = GetZoneNameByIndex
	for mapIndex, entry in pairs(self.locations) do
		list[GetZoneNameByIndex(entry.zoneIndex)] = mapIndex
	end
end

function addon:InitSettings()
	local LAM2 = LibStub("LibAddonMenu-2.0")
	if not LAM2 then return end

	self.player = ZO_SavedVars:New("VotansImprovedLocations_Data", 1, nil, { recent = { }, })

	local defaults = { showLevels = false, sortBy = SORT_LOCATION_NAME, allAllianceOnTop = false, }
	self.account = ZO_SavedVars:NewAccountWide("VotansImprovedLocations_Data", 1, nil, defaults)

	local panelData = {
		type = "panel",
		name = "Improved Locations",
		author = "votan",
		version = "v1.4.3",
		registerForRefresh = false,
		registerForDefaults = true,
	}
	LAM2:RegisterAddonPanel(addon.name, panelData)

	local optionsTable = {
		{
			-- Show Levels
			type = "checkbox",
			name = GetString(SI_VOTANSIMPROVEDLOCATIONS_SHOW_LEVELS),
			tooltip = nil,
			getFunc = function() return self.account.showLevels end,
			setFunc = function(value)
				if self.account.showLevels ~= value then
					self.account.showLevels = value
					ZO_ScrollList_RefreshVisible(self.list)
				end
			end,
			width = "full",
			default = defaults.showLevels,
		},
		{
			-- Sort Order
			type = "dropdown",
			name = GetString(SI_VOTANSIMPROVEDLOCATIONS_SORT),
			tooltip = nil,
			choices = { GetString(SI_VOTANSIMPROVEDLOCATIONS_SORT_NAME), GetString(SI_VOTANSIMPROVEDLOCATIONS_SORT_ASC), GetString(SI_VOTANSIMPROVEDLOCATIONS_SORT_DSC) },
			getFunc = function()
				local value = self.account.sortBy
				if value == SORT_LOCATION_NAME then
					value = GetString(SI_VOTANSIMPROVEDLOCATIONS_SORT_NAME)
				elseif value == SORT_LOCATION_LEVEL_ASC then
					value = GetString(SI_VOTANSIMPROVEDLOCATIONS_SORT_ASC)
				else
					value = GetString(SI_VOTANSIMPROVEDLOCATIONS_SORT_DSC)
				end
				return value
			end,
			setFunc = function(value)
				if value == GetString(SI_VOTANSIMPROVEDLOCATIONS_SORT_NAME) then
					value = SORT_LOCATION_NAME
				elseif value == GetString(SI_VOTANSIMPROVEDLOCATIONS_SORT_ASC) then
					value = SORT_LOCATION_LEVEL_ASC
				else
					value = SORT_LOCATION_LEVEL_DSC
				end
				if self.account.sortBy ~= value then
					self.account.sortBy = value
					WORLD_MAP_LOCATIONS:BuildLocationList()
				end
			end,
			width = "full",

			default = defaults.sortBy,
		},
		{
			-- Show Levels
			type = "checkbox",
			name = GetString(SI_VOTANSIMPROVEDLOCATIONS_SHOW_ALL_ALLIANCE_ON_TOP),
			tooltip = nil,
			getFunc = function() return self.account.allAllianceOnTop end,
			setFunc = function(value)
				if self.account.allAllianceOnTop ~= value then
					self.account.allAllianceOnTop = value

					WORLD_MAP_LOCATIONS:BuildLocationList()
				end
			end,
			width = "full",
			default = defaults.allAllianceOnTop,
		},
	}

	LAM2:RegisterOptionControls(addon.name, optionsTable)
end

function addon:Initialize()
	self:SetupControls()
	self:GetZoneMapping()
	self:InitSettings()
	HookLocations()

	WORLD_MAP_LOCATIONS:BuildLocationList()
end

local function OnAddOnLoaded(event, addonName)
	if addonName == addon.name then
		em:UnregisterForEvent(addon.name, EVENT_ADD_ON_LOADED)
		addon:Initialize()
	end
end

em:RegisterForEvent(addon.name, EVENT_ADD_ON_LOADED, OnAddOnLoaded)