-- TODO French localization is incomplete!
-- russian localization is also not complete anymore
local allLocalizations = {
	["en"] = {
		["pintype1"] = "Blacksmithing pins",
		["pintypetooltip1"] = "Display ore on the map and compass.",

		["pintype2"] = "Clothier pins",
		["pintypetooltip2"] = "Display clothing material on the map and compass.",
		
		["pintype3"] = "Enchantment pins",
		["pintypetooltip3"] = "Display runestones on the map and compass.",
		
		["pintype4"] = "Alchemy pins",
		["pintypetooltip4"] = "Display herbs, flowers and mushrooms on the map and compass.",
		
		["pintype5"] = "Woodworking pins",
		["pintypetooltip5"] = "Display wood on the map and compass.",
		
		["pintype6"] = "Chests pins",
		["pintypetooltip6"] = "Display chests on the map and compass.",
		
		["pintype7"] = "Solvent pins",
		["pintypetooltip7"] = "Display solvents on the map and compass.",
		
		["pintype8"] = "Fishing pins",
		["pintypetooltip8"] = "Display fishing locations on the map and compass.",
		
		["pintype9"] = "Heavy Sack pins",
		["pintypetooltip9"] = "Display heavy sacks on the map and compass.",
		
		["pintype10"] = "Thieves Trove pins",
		["pintypetooltip10"] = "Display Thieves Troves on the map and compass.",
		
		["pintype20"] = "Thieves Trove pins",
		["pintypetooltip20"] = "Display Thieves Troves on the map and compass.",
		
		["pintype21"] = "Justice Container pins",
		["pintypetooltip21"] = "Display Justice Containers like Safeboxes or Heist objectives on the map and compass.",
		
		["savepin"] = "Save <<1>>",
		["savetooltip"] = "Enable to save the locations of this ressource when you discover them.",
		
		["pinsize"] = "Pin size",
		["pinsizetooltip"] = "Set the size of <<1>> on the map.",
		
		["pincolor"] = "Pin color",
		["pincolortooltip"] = "Set the color of the <<1>> on map and compass.",
		
		["pincolor"] = "Pin color",
		["pincolortooltip"] = "Set the color of the <<1>> on map and compass.",
		
		["fov"] = "Field of View",
		["fovtooltip"] = "Sets the field of view on the compass for HarvestMap's pins.\nSetting this value to 360° will display all pins on the compass, 180° will display all pins in front of you on the compass.\nThe default value is 90° which is the field of view for the other compass pins.",
		["distance"] = "Max pin distance",
		["distancetooltip"] = "The maximum distance for pins to appear on the compass.",
		["compass"] = "Enable compass",
		["compasstooltip"] = "Enable the display of close pins on the compass. Can be disabled to increase performance.",
		["minnodedist"] = "Minimum Node Distance",
		["nodedisttooltip"] = "The minimum distance between two ressources.\nIf they are closer than this, only one pin will be created.",
		["apply"] = "Apply Time Setting",
		["applywarning"] = "Once old data has been removed, it can not be restored!",
		["timedifference"] = "Keep only recent data",
		["timedifferencetooltip"] = "HarvestMap will only keep data from the last X days.\nThis prevents displaying old data which may already be outdated.\nSet to 0 to keep any data regardless of its age.",
		["timedifferencewarning"] = "Data from before the Orsinium update doesn't have a timestamp and will thus always be deleted when using a value greater than 0.",
		["compassoptions"] = "Compass Options",
		["account"] = "Account-wide Settings",
		["accounttooltip"] = "All the settings below will be the same for each of your characters.",
		["accountwarning"] = "Changing this setting will reload the UI.",
		["hiddentime"] = "Respawn Timer",
		["hiddentimetooltip"] = "Recently visited pins will be hidden for X minutes.",
		["hiddentimewarning"] = "Setting this to a value greater than zero can decrease the game's performance - especially when using a minimap addon.",
		["hiddenonharvest"] = "Use Respawn Timer only on harvest",
		["hiddenonharvesttooltip"] = "Enable to hide pins only, when you harvested them. When disabled pins will also be hidden if you visit them.",
		["exactitem"] = "Show only discovered items",
		["exactitemtooltip"] = "Enable to display only the discovered ressources in a map pin's tooltip.\nWhen disabled every ressource that can respawn at the pin's location will be displayed in the tooltip.",
		["level"] = "Display ressource pins above POI pins.",
		["leveltooltip"] = "Enable to display the pins of HarvestMap above the POI pins on the map.",
		["deletepinfilter"] = "Delete HarvestMap pins",
		["options"] = "<<1>> Options",
		["debug"] = "Display debug messages",
		["debugtooltip"] = "Enable to display debug messages in the chat.",
		["filterheatmap"] = "Heatmap mode",
		["goldperminute"] = "Gold per minute:",
		["farmresult"] = "HarvestFarm Result",
		["farmnotour"] = "HarvestFarm was not able to calculate a good farming route with the given minimum route length.",
		["farmerror"] = "HarvestFarm Error",
		["farmnoressources"] = "No ressources found.\nThere are no ressources on this map or you don't have any ressource types selected.",
		["farminvalidmap"] = "The farming helper tool can not be used on this map.",
		["farmsuccess"] = "HarvestFarm calculated a farming route with <<1>> nodes per kilometer.",
		["farmdescription"] = "HarvestFarm will calculate a tour with a very high ressource per time ratio.",
		["farmminlength"] = "Minimum route length",
		["farmminlengthtooltip"] = "The minimum length of the tour in kilometers.",
		["farmminlengthdescription"] = "The longer the tour, the higher the chance that the ressources have respawned when you start the next cycle.\nHowever a shorter tour will have a better ressource per time ratio.",
		["calculatetour"] = "Calculate Tour",
		["showtourinterface"] = "Show Tour Interface",
		["canceltour"] = "Cancel Tour",
		["ressourcetypes"] = "Ressource Types",
		["skiptarget"] = "Skip current target",
		["nodesperminute"] = "Nodes per minute",
		["distancetotarget"] = "Distance to the next ressource",
		["showarrow"] = "Display direction",
	},
	["de"] = {
		["pintype1"] = "Schmied Pins",
		["pintypetooltip1"] = "Zeige Erze auf der Karte und dem Kompass.",
		["pintype2"] = "Schneiderei Pins",
		["pintypetooltip2"] = "Zeige Kleidungsmaterial auf der Karte und dem Kompass.",
		["pintype3"] = "Verzauberung Pins",
		["pintypetooltip3"] = "Zeige Runen auf der Karte und dem Kompass.",
		["pintype4"] = "Alchemie Pins",
		["pintypetooltip4"] = "Zeige Kräuter auf der Karte und dem Kompass.",
		["pintype5"] = "Schreinerei Pins",
		["pintypetooltip5"] = "Zeige Holz auf der Karte und dem Kompass.",
		["pintype6"] = "Schatztruhen Pins",
		["pintypetooltip6"] = "Zeige Schatztruhen auf der Karte und dem Kompass.",
		["pintype7"] = "Lösung Pins",
		["pintypetooltip7"] = "Zeige Lösungen (Alchemie Zutat) auf der Karte und dem Kompass.",
		["pintype8"] = "Fischgrund Pins",
		["pintypetooltip8"] = "Zeige Fischgründe auf der Karte und dem Kompass.",
		["pintype9"] = "Schwerer Sack Pins",
		["pintypetooltip9"] = "Zeige Schwere Säcke auf der Karte und dem Kompass.",
		["pintype10"] = "Diebesgut Pins",
		["pintypetooltip10"] = "Zeige Diebesgut auf der Karte und dem Kompass.",
		["pintype20"] = "Diebesgut Pins",
		["pintypetooltip20"] = "Zeige Diebesgut auf der Karte und dem Kompass.",
		["pintype21"] = "Rechtssystem Pins",
		["pintypetooltip21"] = "Zeige Rechtssystem Pins (Beutezugziele und Wertkassetten) auf der Karte und dem Kompass.",
		
		["pinsize"] = "Pingröße",
		["pinsizetooltip"] = "Setze die Größe der <<1>> auf der Karte.",
		
		["pincolor"] = "Pinfarbe",
		["pincolortooltip"] = "Setze die Farbe der <<1>> auf der Karte und dem Kompass.",
		
		["savepin"] = "Speichere <<1>>",
		["savetooltip"] = "Aktiviere diese Einstellung, um die Position dieser Ressource zu speichern, wenn du sie findest.",
		
		["fov"] = "Sichtfeld",
		["fovtooltip"] = "Setze das Sichtfeld für die Pins auf dem Kompass.\nIst dieser Wert auf 360°, so werden alle Pins angezeigt, bei 180° erscheinen nur Pins vor dir auf dem Kompass angezeigt.\nDer Standardwert liegt bei 108°, was dem Sichtfeld der gewöhnlichen Kompasspins entspricht.",
		["distance"] = "Maximale Pinentfernung",
		["distancetooltip"] = "Die maximale Entfernung von Pins, sodass diese noch immer auf dem Kompass erscheinen.",
		["compass"] = "Aktiviere Kompasspins",
		["compasstooltip"] = "Aktiviere diese Einstellung um nahegelegene Pins auf dem Kompass anzeigen zu lassen. Deaktivieren kann zu einer Performanzverbesserung führen.",
		["apply"] = zo_strupper("ü") .. "bernehmen",
		["applywarning"] = "Einmal gelöschte Daten können nicht wiederhergestellt werden!",
		["timedifference"] = "Verwerfe veraltete Daten",
		["timedifferencetooltip"] = "HarvestMap verwirft Daten älter als X Tage.\nSetze diesen Wert auf 0, um alle Daten zu behalten - egal wie alt sie sind.",
		["timedifferencewarning"] = "Daten älter als das Orsiniumupdate besitzen keine Zeitinformationen und werden deshalb bei jedem Wert größer als 0 gelöscht.",
		["compassoptions"] = "Kompass Optionen",
		["account"] = "Accountweite Einstellungen",
		["accounttooltip"] = "Alle Einstellungen unterhalb sind dieselben für alle deine Figuren.",
		["accountwarning"] = "Wenn diese Einstellung geändert wird, wird das Interface neu geladen und es kommt zu einem Ladebildschirm.",
		["hiddentime"] = "Respawn Timer",
		["hiddentimetooltip"] = "Vor kurzem besuche Pins werden für X minuten von der Karte und dem Kompass versteckt. Ist dieser Wert 0, so werden die Pins niemals versteckt.",
		["hiddentimewarning"] = "Setze diesen Wert auf 0 um die Performanz zu verbessern.",
		["hiddenonharvest"] = "Erntespezifischer Respawn Timer",
		["hiddenonharvesttooltip"] = "Aktiviere diese Einstellung um Pins nur zu verstecken, wenn du an ihrer Position etwas geerntet hast.\nWenn deaktiviert werden Pins bereits beim Besuchen versteckt.",
		["exactitem"] = "Zeige nur entdeckte Ressourcen",
		["exactitemtooltip"] = "Aktiviere diese Einstellungen um im Pintooltip nur von dir entdeckte Gegenstände anzuzeigen.\nFalls deaktiviert werden abhängig von der Zone, deiner Stufe und deinem Handwerksrang die möglichen Ressourcen berechnet und angezeigt.",
		["level"] = "Zeige HarvestMaps Pins über den anderen Pins.",
		["leveltooltip"] = "Falls diese Einstellung aktiviert ist, werden HarvestMaps Pins nicht von den anderen Pins auf der Karte verdeckt.",
		["deletepinfilter"] = "Lösche HarvestMap Pins",
		["options"] = "<<1>> Optionen",
		["debug"] = "Zeige Debugnachrichten an",
		["debugtooltip"] = "Falls aktiviert werden Debugnachrichten im Chatfenster angezeigt.",
		["filterheatmap"] = "Heatmap Modus",
		["goldperminute"] = "Gold pro Minute:",
		["farmresult"] = "HarvestFarm Ergebnis",
		["farmnotour"] = "HarvestFarm war nicht in der Lage eine gute Tour mit der gegebenen minimalen Tourlänge zu berechnen.",
		["farmerror"] = "HarvestFarm Fehler",
		["farmnoressources"] = "Keine Ressourcen gefunden.\nEs gibt keine Ressourcen auf dieser Karte oder du hast keine Ressourcen ausgewählt.",
		["farminvalidmap"] = "HarvestFarm kann auf dieser Karte nicht verwendet werden.",
		["farmsuccess"] = "HarvestFarm berechnete eine Tour mit <<1>> Erntepunkten pro Kilometer.",
		["farmdescription"] = "HarvestFarm berechnet eine Tour mit einer sehr hohen Ressourcen-pro-Zeit-Rate.",
		["farmminlength"] = "Minimale Tourlänge",
		["farmminlengthtooltip"] = "Die Länge wird in Kilometern angegeben.",
		["farmminlengthdescription"] = "Je länger die Tour, desto höher die Wahrscheinlichtkeit, dass die Erntepunkte beim nächsten Besuch respawnt sind.\nEine kürzere Tour liefert jedoch eine bessere Ressourcen-pro-Zeit-Rate.",
		["calculatetour"] = "Berechne die Tour",
		["showtourinterface"] = "Zeige Tour Interface",
		["canceltour"] = "Tour Abbrechen",
		["ressourcetypes"] = "Ressourcenarten",
		["skiptarget"] = zo_strupper("ü").."berspringe Ziel",
		["nodesperminute"] = "Erntepunkte pro Minute",
		["distancetotarget"] = "Distanz zum Ziel:",
		["showarrow"] = "Zeige Richtung an",
	},
	["fr"] = { --translation based on wookiefrag's input.
		["pintype1"] = "Gisements Pins",
		["pintypetooltip1"] = "Montre les gisements de minerais sur la carte et la boussole.",
		["pintype2"] = "Matériaux Pins",
		["pintypetooltip2"] = "Montre les matériaux de couture sur la carte et la boussole.",
		["pintype3"] = "Pierres runiques Pins",
		["pintypetooltip3"] = "Montre les pierres runiques sur la carte et la boussole.",
		["pintype4"] = "Plantes Pins",
		["pintypetooltip4"] = "Montre les plantes sur la carte et la boussole.",
		["pintype5"] = "Matériaux Pins",
		["pintypetooltip5"] = "Montre les rondins de bois sur la carte et la boussole.",
		["pintype6"] = "Coffres Pins",
		["pintypetooltip6"] = "Montre les coffres sur la carte et la boussole.",
		["pintype7"] = "Solvants Pins",
		["pintypetooltip7"] = "Montre les solvants sur la carte et la boussole.",
		
		
		["pinsize"] = "Taille des repères",
		["pinsizetooltip"] = "Paramètre la taille des repères des <<1>> de la carte.",
		
		["pincolor"] = "Couleur des repères",
		["pincolortooltip"] = "Définit la couleur des repères des <<1>> de la carte et de la boussole.",
		
		["fov"] = "Champ de vision",
		["distance"] = "Distance maximale d'un repère",
		["distancetooltip"] = "Distance maximale a laquelle les différents repères apparaissent sur la boussole.",
		["compass"] = "Activer la boussole",
		["compasstooltip"] = "Activer l'affichage des repères a proximité sur la boussole.",
	},
	["ru"] = {
		["pintype1"] = "Êóçîeùîoe äeìo",
		["pintypetooltip1"] = "Ïoêaçÿáaòö póäÿ îa êapòe è êoíïace.",
		["gather1"] = "Çaïoíèîaòö póäÿ",
		["gathertooltip1"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe póä îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["pintype2"] = "Ïopòîüæîoe äeìo",
		["pintypetooltip2"] = "Ïoêaçÿáaòö òêaîè îa êapòe è êoíïace.",
		["gather2"] = "Çaïoíèîaòö òêaîè",
		["gathertooltip2"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe òêaîeé îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["pintype3"] = "Çaùapoáaîèe",
		["pintypetooltip3"] = "Ïoêaçÿáaòö póîÿ îa êapòe è êoíïace.",
		["gather3"] = "Çaïoíèîaòö póîÿ",
		["gathertooltip3"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe póî îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["pintype4"] = "Aìxèíèü",
		["pintypetooltip4"] = "Ïoêaçÿáaòö òpaáÿ îa êapòe è êoíïace.",
		["gather4"] = "Çaïoíèîaòö òpaáÿ",
		["gathertooltip4"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe òpaá îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["pintype5"] = "Còoìüpîoe äeìo",
		["pintypetooltip5"] = "Ïoêaçÿáaòö äpeáecèîó îa êapòe è êoíïace.",
		["gather5"] = "Çaïoíèîaòö äpeáecèîó",
		["gathertooltip5"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe äpeáecèîÿ îa êapòe, êoâäa áÿ ee îaéäeòe.",

		["pintype6"] = "Cóîäóêè",
		["pintypetooltip6"] = "Ïoêaçÿáaòö cóîäóêè îa êapòe è êoíïace.",
		["gather6"] = "Çaïoíèîaòö cóîäóêè",
		["gathertooltip6"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe cóîäóêoá îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["pintype7"] = "Pacòáopèòeìè",
		["pintypetooltip7"] = "Ïoêaçÿáaòö pacòáopèòeìè îa êapòe è êoíïace.",
		["gather7"] = "Çaïoíèîaòö pacòáopèòeìè",
		["gathertooltip7"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe pacòoápèòeìeé îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["pintype8"] = "Pÿàîÿe íecòa",
		["pintypetooltip8"] = "Ïoêaçÿáaòö pÿàîÿe íecòa îa êapòe è êoíïace.",
		["gather8"] = "Çaïoíèîaòö pÿàîÿe íecòa",
		["gathertooltip8"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe pÿàîÿx íecò îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["pintype9"] = "Òüæeìÿe íeúêè",
		["pintypetooltip9"] = "Ïoêaçÿáaòö òüæeìÿe íeúêè îa êapòe è êoíïace.",
		["gather9"] = "Çaïoíèîaòö òüæeìÿe íeúêè",
		["gathertooltip9"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe òüæeìÿx íeúêoá îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["pintype10"] = "Áopoácêèe òaéîèêè",
		["pintypetooltip10"] = "Ïoêaçÿáaòö áopoácêèe òaéîèêè îa êapòe è êoíïace.",
		["gather10"] = "Çaïoíèîaòö áopoácêèe òaéîèêè",
		["gathertooltip10"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe áopoácêèx òaéîèêoá îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["pintype20"] = "Áopoácêèe òaéîèêè",
		["pintypetooltip20"] = "Ïoêaçÿáaòö áopoácêèe òaéîèêè îa êapòe è êoíïace.",
		["gather20"] = "Çaïoíèîaòö áopoácêèe òaéîèêè",
		["gathertooltip20"] = "Paçpeúèòö çaïoíèîaòö è coxapîüòö ïoìoæeîèe áopoácêèx òaéîèêoá îa êapòe, êoâäa áÿ èx îaéäeòe.",

		["size1"] = "Paçíep èêoîêè",
		["sizetooltip1"] = "Çaäaeò paçíep èêoîêè póäÿ îa êapòe.",
		["size2"] = "Paçíep èêoîêè",
		["sizetooltip2"] = "Çaäaeò paçíep èêoîêè òêaîeé îa êapòe.",
		["size3"] = "Paçíep èêoîêè",
		["sizetooltip3"] = "Çaäaeò paçíep èêoîêè póî îa êapòe.",
		["size4"] = "Paçíep èêoîêè",
		["sizetooltip4"] = "Çaäaeò paçíep èêoîêè òpaá îa êapòe.",
		["size5"] = "Paçíep èêoîêè",
		["sizetooltip5"] = "Çaäaeò paçíep èêoîêè äpeáecèîÿ îa êapòe.",
		["size6"] = "Paçíep èêoîêè",
		["sizetooltip6"] = "Çaäaeò paçíep èêoîêè cóîäóêoá îa êapòe.",
		["size7"] = "Paçíep èêoîêè",
		["sizetooltip7"] = "Çaäaeò paçíep èêoîêè pacòáopèòeìeé îa êapòe.",
		["size8"] = "Paçíep èêoîêè",
		["sizetooltip8"] = "Çaäaeò paçíep èêoîêè pÿàîÿx íecò îa êapòe.",
		["size9"] = "Paçíep èêoîêè",
		["sizetooltip9"] = "Çaäaeò paçíep èêoîêè òüæeìÿx íeúêoá îa êapòe.",
		["size10"] = "Paçíep èêoîêè",
		["sizetooltip10"] = "Çaäaeò paçíep èêoîêè Áopoácêèx Òaéîèêoá îa êapòe.",
		["size20"] = "Paçíep èêoîêè",
		["sizetooltip20"] = "Çaäaeò paçíep èêoîêè Áopoácêèx Òaéîèêoá îa êapòe.",

		["color1"] = "Œáeò èêoîêè",
		["colortooltip1"] = "Çaäaeò œáeò èêoîêè póäÿ îa êapòe è êoíïace.",
		["color2"] = "Œáeò èêoîêè",
		["colortooltip2"] = "Çaäaeò œáeò èêoîêè òêaîeé îa êapòe è êoíïace.",
		["color3"] = "Œáeò èêoîêè",
		["colortooltip3"] = "Çaäaeò œáeò èêoîêè póî îa êapòe è êoíïace.",
		["color4"] = "Œáeò èêoîêè",
		["colortooltip4"] = "Çaäaeò œáeò èêoîêè òpaá îa êapòe è êoíïace.",
		["color5"] = "Œáeò èêoîêè",
		["colortooltip5"] = "Çaäaeò œáeò èêoîêè äpeáecèîÿ îa êapòe è êoíïace.",
		["color6"] = "Œáeò èêoîêè",
		["colortooltip6"] = "Çaäaeò œáeò èêoîêè cóîäóêoá îa êapòe è êoíïace.",
		["color7"] = "Œáeò èêoîêè",
		["colortooltip7"] = "Çaäaeò œáeò èêoîêè pacòáopèòeìeé îa êapòe è êoíïace.",
		["color8"] = "Œáeò èêoîêè",
		["colortooltip8"] = "Çaäaeò œáeò èêoîêè pÿàîÿx íecò îa êapòe è êoíïace.",
		["color9"] = "Œáeò èêoîêè",
		["colortooltip9"] = "Çaäaeò œáeò èêoîêè òüæeìÿx íeúêoá îa êapòe è êoíïace.",
		["color10"] = "Œáeò èêoîêè",
		["colortooltip10"] = "Çaäaeò œáeò èêoîêè Áopoácêèx Òaéîèêoá îa êapòe è êoíïace.",
		["color20"] = "Œáeò èêoîêè",
		["colortooltip20"] = "Çaäaeò œáeò èêoîêè Áopoácêèx Òaéîèêoá îa êapòe è êoíïace.",

		["fov"] = "Ïoìe áèäèíocòè",
		["fovtooltip"] = "25 ~ Áÿ íoæeòe áèäeòö èêoîêè á êoîóce 90 âpaäócoá ïepeä áaíè.\n(25% = ùeòáepòö êpóâa)\n100 ~ Áÿ íoæeòe áèäeòö áce èêoîêè áoêpóâ áac.\n(100% = ïoìîÿé êpóâ).",
		["distance"] = "Äècòaîœèü èêoîoê",
		["distancetooltip"] = "Íaêcèíaìöîaü äècòaîœèü, îa êoòopoé èêoîêè àóäóò ïoüáìüòöcü îa êapòe è êoíïace.",
		["compass"] = "Oòoàpaæaòö îa êoíïace",
		["compasstooltip"] = "Oòoàpaæaòö àìèæaéúèe èêoîêè îa êoíïace.",
		["minnodedist"] = "Äècò. íeæäó pecópcaíè",
		["nodedisttooltip"] = "Íèîèíaìöîoe paccòoüîèe íeæäó òoùêaíè pecópcoá.\nEcìè oîè àìèæe, ùeí óêaçaîîoe çîaùeîèe, àóäeò coçäaîa òoìöêo oäîa èêoîêa.",
		["apply"] = "Ïpèíeîèòö ápeíeîîÿe îacòpoéêè",
		["applywarning"] = "Êaê òoìöêo còapÿe äaîîÿe àóäóò óäaìeîÿ, èx àóäeò îeáoçíoæîo áoccòaîoáèòö!",
		["timedifference"] = "Òoìöêo aêòóaìöîÿe äaîîÿe",
		["timedifferencetooltip"] = "HarvestMap àóäeò coxpaîüò äaîîÿe òoìöêo ça ïocìäeîèe X äîeé.\nËòo ïoçáoìüeò îe oòpaæaòö còapÿe äaîîÿe, êoòopÿe óæe íoâìè ócòapeòö.\nÓcòaîoáèòe 0, ùòoàÿ coxpaîèòö áce äaîîÿe áîe çaáècèíocòè oò èx áoçpacòa.",
		["timedifferencewarning"] = "Äaîîÿe äo áÿxoäè oàîoáìeîèü Orsinium îe èíeôò ápeíeîîoé ïpèáüçêè è áceâäa àóäeò óäaìüòöcü, ecìè ócòaîoáèòö çîaùeîèe àoìöúe, ùeí 0.",
		["compassoptions"] = "Êoíïac",
		["account"] = "Îacòpoéêè îa aêêaóîò",
		["accounttooltip"] = "Áce îacòpoéêè îèæe àóäóò ïpèíeîeîÿ êo áceí ïepcoîaæaí îa áaúeí aêêaóîòe.",
		["accountwarning"] = "Èçíeîeîèe ëòoé îacòpéoêè ïepeçaâpóçèò UI.",
		["hiddentime"] = "Ápeíü áoçpoæäeîèü",
		["hiddentimetooltip"] = "Òoìöêo ùòo ïoceûeîîÿé pecópc àóäeò cêpÿò îa X íèîóò.",
		["hiddentimewarning"] = "Ócòaîoáêa ëòoâo çîaùeîèü á çîaùeîèe àoìöúe 0 íoæeò cîèçèòö ïpoèçáoäèòeìöîocòö - ocoàeîîo ecìè ècïoìöçóeòcü aääoî íèîèêapòÿ.",
		["exactitem"] = "Òoìöêo îaéäeîîÿe pócópcÿ",
		["exactitemtooltip"] = "Ïpè áêìôùeîèè oòoàpaæaeò á ïoäcêaçêe ê èêoîêe îa êapòe òoìöêo óæe îaéäeîîÿe òaí pócópcÿ.\nEcìè oòêìôùeîo, áce pecópcÿ, êoòopÿe íoâóò ïoüáèòöcü á íecòe èêoîêè, àóäóò ïoêaçaîÿ á ïoäcêaçêe.",
	},
}

-- Set Localization
Harvest.language = GetCVar("language.2")
-- use english localization if the language isn't supported
if not allLocalizations[Harvest.language] then
	Harvest.language = "en"
end
local localization = allLocalizations[Harvest.language]

function Harvest.GetLocalization(tag)
	-- return the localization for the given tag,
	-- if the localization is missing, use the english string instead
	-- if the english string is missing, something went wrong.
	-- return the tag so that at least some string is returned to prevent the addon from crashing
	return (localization[ tag ] or allLocalizations["en"][ tag ]) or tag
end
