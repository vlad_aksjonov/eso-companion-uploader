if not Harvest then
	Harvest = {}
end
local Harvest = _G["Harvest"]

local AS = LibStub("AceSerializer-3.0h")
local LMP = LibStub("LibMapPins-1.0")
local COMPASS_PINS = LibStub("CustomCompassPins")
local GPS = LibStub("LibGPS2")

Harvest.GPS = GPS

-- helper function which i sometimes use to debug stuff, nothing the user needs
--[[
function Harvest.Check()
	for map, _ in pairs(Harvest.savedVars["nodes"].data) do
		for _, pinTypeId in pairs(Harvest.PINTYPES) do
			Harvest.LoadToCache( map, pinTypeId )
		end
	end
	for map, _ in pairs(Harvest.savedVars["ADnodes"].data) do
		for _, pinTypeId in pairs(Harvest.PINTYPES) do
			Harvest.LoadToCache( map, pinTypeId )
		end
	end
	for map, _ in pairs(Harvest.savedVars["DCnodes"].data) do
		for _, pinTypeId in pairs(Harvest.PINTYPES) do
			Harvest.LoadToCache( map, pinTypeId )
		end
	end
	for map, _ in pairs(Harvest.savedVars["EPnodes"].data) do
		for _, pinTypeId in pairs(Harvest.PINTYPES) do
			Harvest.LoadToCache( map, pinTypeId )
		end
	end
	for map, pinTypes in pairs(Harvest.cache) do
		for pinTypeId, nodes in pairs(pinTypes) do
			Harvest.IsNodeAlreadyFound( nodes, 0, 0 )
		end
	end
end
--]]

-- this function returns the current map and the player's x and y coordinate
function Harvest.GetLocation()
	if(SetMapToPlayerLocation() == SET_MAP_RESULT_MAP_CHANGED) then
		CALLBACK_MANAGER:FireCallbacks("OnWorldMapChanged")
	end

	local map = Harvest.GetMap()
	local x, y = GetMapPlayerPosition( "player" )
	return map, x, y
end

-- serialize the given node via the ACE library
-- serializing the data decreases the loadtimes and file size a lot
function Harvest.Serialize(data)
	return AS:Serialize(data)
end

-- register a dialog which is used to display deserialization errors
local deserializeDialog = {
	title = { text = "Deserialization Error" },
	mainText = { text =[[HarvestMap encountered an error:
The following string could not be deserialized:
<<1>>

Please report this error by posting a screenshot of this error message to the comment section of HarvestMap on esoui.com.
Please tell us in the comment as well, if you used the HarvestMerge website.]]},
	buttons = {
		[1] = {
			text = "Close",
		},
	}
}
ZO_Dialogs_RegisterCustomDialog("DESERIALIZE_ERROR", deserializeDialog)

-- this function deserializes the data and displays an error message if debug mode is enabled
function Harvest.Deserialize(data)
	local success, result = AS:Deserialize(data)
	--  it seems some bug in HarvestMerge deleted the x or y coordinates
	if success and Harvest.IsNodeValid(result) then 
		return result
	else
		if Harvest.AreDebugMessagesEnabled() then
			ZO_Dialogs_ShowDialog("DESERIALIZE_ERROR", {}, { mainTextParams = { string.gsub(data,"%^","-") } } )
			d("fatal error while decoding node:")
			d(data)
		end
		Harvest.AddToErrorLog(data)
		Harvest.AddToErrorLog(result)
	end
	return nil
end

function Harvest.IsNodeValid(node)
	if type(node[1]) == "number" and type(node[2]) == "number" then
		-- sometimes encoding the coordinates is wrong and the become ridiculously large numbers or 0
		--if node[1] > 0 and node[1] < 1 and node[2] > 0 and node[2] < 1 then
		-- removed the above condition as there are nodes outside the [0,1] intervall in case of weird underground shenanigans by eso
			return true
		--end
	end
	return false
end

-- helper function to only display debug messages if the debug mode is enabled
function Harvest.Debug( message )
	if Harvest.AreDebugMessagesEnabled() or Harvest.AreVerboseMessagesEnabled() then
		d( message )
	end
end
function Harvest.Verbose( message )
	if Harvest.AreVerboseMessagesEnabled() then
		Harvest.Debug( message )
	end
end

-- this function returns the pinTypeId for the given item id and node name
function Harvest.GetPinTypeId( itemId, nodeName )
	-- get two pinTypes based on the item id and node name
	local itemIdPinType = Harvest.itemId2PinType[ itemId ]
	local nodeNamePinType = Harvest.nodeName2PinType[ zo_strlower( nodeName ) ]
	Harvest.Debug( "Item id " .. tostring(itemId) .. " returns pin type " .. tostring(itemIdPinType))
	Harvest.Debug( "Node name " .. tostring(nodeName) .. " returns pin type " .. tostring(nodeNamePinType))
	-- heavy sacks can contain material for different professions
	-- so don't use the item id to determine the pin type
	if Harvest.IsHeavySack( nodeName ) then
		return Harvest.HEAVYSACK
	end
	if Harvest.IsTrove( nodeName ) then
		return Harvest.TV2
	end
	-- both returned the same pin type (or both are unknown/nil)
	if itemIdPinType == nodeNamePinType then
		return itemIdPinType
	end
	-- we allow this special case because of possible errors in the localization
	if nodeNamePinType == nil then
		return itemIdPinType
	end
	-- the pin types don't match, don't save the node as there is some error
	return nil
end

-- not just called by EVENT_LOOT_RECEIVED but also by Harvest.OnLootUpdated
function Harvest.OnLootReceived( eventCode, receivedBy, objectName, stackCount, soundCategory, lootType, lootedBySelf )
	-- don't touch the save files/tables while they are still being updated/refactored
	if not Harvest.IsUpdateQueueEmpty() then
		Harvest.Debug( "OnLootReceived failed: HarvestMap is updating" )
		return
	end
	
	if not lootedBySelf then
		Harvest.Debug( "OnLootReceived failed: wasn't looted by self" )
		return
	end
	-- in case an addon forgets to call the OnMapChanged callback, we call this callback function now
	-- this way we make sure the correct map is selected
	Harvest.OnMapChanged()
	local map, x, y = Harvest.GetLocation()
	local isHeist = false
	-- only save something if we were harvesting or the target is a heavy sack or thieves trove
	if (not Harvest.wasHarvesting) and (not Harvest.IsHeavySack( Harvest.lastInteractableName )) and (not Harvest.IsTrove( Harvest.lastInteractableName )) then
		-- additional check for heist containers
		if not (lootType == LOOT_TYPE_QUEST_ITEM) or not Harvest.IsHeistMap(map) then
			Harvest.Debug( "OnLootReceived failed: wasn't harvesting" )
			Harvest.Debug( "OnLootReceived failed: wasn't heist quest item" )
			Harvest.Debug( "Interactable name is:" .. tostring(Harvest.lastInteractableName))
			return
		else
			isHeist = true
		end
	end
	-- get the information we want to save
	local itemName, itemId
	local pinTypeId = nil
	if not isHeist then
		itemName, _, _, itemId = ZO_LinkHandler_ParseLink( objectName )
		itemId = tonumber(itemId)
		if itemId == nil then
			-- wait what? does this even happen?! abort mission!
			Harvest.Debug( "OnLootReceived failed: item id is nil" )
			return
		end
		-- get the pintype depending on the item we looted and the name of the harvest node
		-- eg jute will be saved as a clothing pin
		pinTypeId = Harvest.GetPinTypeId(itemId, Harvest.lastInteractableName)
		-- sometimes we can't get the pinType based on the itemId and node name
		-- ie some data in the localization is missing and nirncrux can be found in ore and wood
		-- abort if we couldn't find the correct pinType
		if pinTypeId == nil then
			Harvest.Debug( "OnLootReceived failed: pin type id is nil" )
			return
		end
		-- if this pinType is supposed to be saved
		if not Harvest.IsPinTypeSavedOnGather( pinTypeId ) then
			Harvest.Debug( "OnLootReceived failed: pin type is disabled in the options" )
			return
		end
	else
		pinTypeId = Harvest.JUSTICE
	end
	
	local name = Harvest.lastInteractableName
	if pinTypeId == Harvest.TV2 then
		name = "TV"
	end
	
	Harvest.SaveData( map, x, y, pinTypeId, name, itemId )
	-- refresh pins as a new one was added
	Harvest.needsRefresh = Harvest.needsRefresh or {}
	Harvest.needsRefresh[pinTypeId] = true
	HarvestFarm.FarmedANode(objectName, stackCount)
end

-- neded for those players that play without auto loot
function Harvest.OnLootUpdated()
	-- only save something if we were harvesting or the target is a heavy sack or thieves trove
	if (not Harvest.wasHarvesting) and (not Harvest.IsHeavySack( Harvest.lastInteractableName )) and (not Harvest.IsTrove( Harvest.lastInteractableName )) then
		Harvest.Debug( "OnLootUpdated failed: wasn't harvesting" )
		return
	end

	-- i usually play with auto loot on
	-- everything was programmed with auto loot in mind
	-- if auto loot is disabled (ie OnLootUpdated is called)
	-- let harvestmap believe auto loot is enabled by calling
	-- OnLootReceived for each item in the loot window
	local items = GetNumLootItems()
	Harvest.Debug( "HarvestMap will check " .. tostring(items) .. " items." )
	for lootIndex = 1, items do
		local lootId, _, _, count = GetLootItemInfo( lootIndex )
		Harvest.OnLootReceived( nil, nil, GetLootItemLink( lootId, LINK_STYLE_DEFAULT ), count, nil, nil, true )
	end

	-- when looting something, we have definitely finished the harvesting process
	if Harvest.wasHarvesting then
		Harvest.Debug( "All loot was handled. Set harvesting state to false." )
		Harvest.wasHarvesting = false
	end
end

-- refreshes the pins of the given pinType
-- if no pinType is given, all pins are refreshed
function Harvest.RefreshPins( pinTypeId )
	-- refresh all pins if not pin type was given
	if not pinTypeId then
		for _, pinTypeId in pairs(Harvest.PINTYPES ) do
			LMP:RefreshPins( Harvest.GetPinType( pinTypeId ) )
			COMPASS_PINS:RefreshPins( Harvest.GetPinType( pinTypeId ) )
		end
		return
	end
	-- refresh only the pins of the given pin type
	if Harvest.contains( Harvest.PINTYPES, pinTypeId ) then
		LMP:RefreshPins( Harvest.GetPinType( pinTypeId ) )
		COMPASS_PINS:RefreshPins( Harvest.GetPinType( pinTypeId ) )
	end
end

-- simple helper function which checks if a value is inside the table
-- does lua really not have a default function for this?
function Harvest.contains( table, value)
	for _, element in pairs(table) do
		if element == value then
			return true
		end
	end
	return false
end


function Harvest.OnMapChanged()
	local textureName = GetMapTileTexture()
	textureName = string.lower(textureName)
	textureName = string.gsub(textureName, "^.*maps/", "")
	textureName = string.gsub(textureName, "_%d+%.dds$", "")

	--local mapType = GetMapType()
	--local mapContentType = GetMapContentType()
	--if (mapType == MAPTYPE_SUBZONE) or (mapContentType == MAP_CONTENT_DUNGEON) then
	--    Harvest.minDistanceFactor = 20  -- Larger value for minDist since map is smaller
	--elseif (mapContentType == MAP_CONTENT_AVA) then
	--    Harvest.minDistanceFactor = 4 -- Smaller value for minDist since map is larger
	--else
	--    Harvest.minDistanceFactor = 1 -- This is the default value for minDist
	--end

	if textureName == "eyevea_base" then
		local worldMapName = GetUnitZone("player")
		worldMapName = string.lower(worldMapName)
		textureName = worldMapName .. "/" .. textureName
	else
		local heistMap = Harvest.IsHeistMap( textureName )
		if heistMap then
			Harvest.map = heistMap .. "_base"
			return
		end
	end

	Harvest.map = textureName
end
CALLBACK_MANAGER:RegisterCallback("OnWorldMapChanged", Harvest.OnMapChanged)
-- doing this on callback doesn't work
-- the pins get refreshed BEFORE the new map name is calculated

function Harvest.GetMap()
	if not Harvest.map then Harvest.OnMapChanged() end
	return Harvest.map
end



-- checks if there is a node in the given nodes list which is close to the given coordinates
-- returns the index of the close node if one is found
function Harvest.IsNodeAlreadyFound( nodes, x, y )
	local minDistance = Harvest.GetMinDistanceBetweenPins()
	local dist2 = 0
	for index, node in pairs( nodes ) do
		local dx = node.data[1] - x
		local dy = node.data[2] - y
		-- distance is sqrt(dx * dx + dy * dy) but for performance we compare the squared values
		dist2 = dx * dx + dy * dy
		if dist2 < minDistance then -- the new node is too close to an old one, it's probably a duplicate
			return index
		end
	end
	return nil
end

-- this function tries to save the given data
-- this function is only used by the harvesting part of HarvestMap
-- import and merge features do not use this function
function Harvest.SaveData( map, x, y, pinTypeId, nodeName, itemID )
	-- old data may be mising some information. in that case skip that node data completely
	Harvest.Debug( map )
	if not nodeName then
		Harvest.Debug( "SaveData failed: node name is nil" )
		return
	end
	if not pinTypeId then
		Harvest.Debug( "SaveData failed: pin type id is nil" )
		return
	end
	-- probably a HarvestMerge bug :/
	if type(x) ~= "number" or type(y) ~= "number" then
		Harvest.Debug( "SaveData failed: coordinates aren't numbers" )
		return
	end
	
	-- If the map is on the blacklist then don't save the data
	if Harvest.IsMapBlacklisted( map ) then
		Harvest.Debug( "SaveData failed: map " .. tostring(map) .. " is blacklisted" )
		return
	end
	
	local saveFile = Harvest.GetSaveFile( map )
	if not saveFile then return end
	-- tables may not exist yet
	saveFile.data[ map ] = saveFile.data[ map ] or {}
	saveFile.data[ map ][ pinTypeId ] = saveFile.data[ map ][ pinTypeId ] or {}
	Harvest.LoadToCache( map, pinTypeId )
	local nodes = Harvest.cache[ map ][ pinTypeId ]
	local stamp = Harvest.GetCurrentTimestamp()

	-- If we have found this node already then we don't need to save it again
	local index = Harvest.IsNodeAlreadyFound( nodes, x, y )
	if index then
		-- update the timestamp of the node, to confirm its a recent position (maybe we will delete old data in the future)
		local node = nodes[ index ]
		if Harvest.IsHiddenOnHarvest() then
			local pinType = Harvest.GetPinType(pinTypeId)
			local time = GetFrameTimeSeconds()
			if not node.hidden then
				Harvest.Debug( "respawn timer has hidden a pin of pin type " .. tostring(pinType) )
				LMP:RemoveCustomPin( pinType, node.data )
				COMPASS_PINS.pinManager:RemovePin( node.data, pinType )
				node.hidden = true
			end
			node.time = time
		end
		
		if Harvest.ShouldSaveItemId(pinTypeId) and itemID then
			if not node.data[4] then
				node.data[4] = {}
			end
			node.data[4][itemID] = stamp
		end
		-- keep the new position instead of the old one
		-- the old one could be outdated while the new one was just confirmed
		if (not node.data[5]) or node.data[5] <= stamp then
			node.data[5] = stamp
			node.data[1] = x
			node.data[2] = y
			local globalX, globalY = GPS:LocalToGlobal(x, y)
			node.global = {globalX, globalY}
		end
		Harvest.Debug( "data was merged with a previous node" )
		-- serialize the node for the save file
		saveFile.data[ map ][ pinTypeId ][ index ] = Harvest.Serialize( node )
		return
	end
	if Harvest.ShouldSaveItemId(pinTypeId) then
		itemID = { [itemID] = stamp }
	else
		itemID = nil
	end
	Harvest.Debug( "data was saved and a new pin was created" )
	-- we need to save the data in serialized form, but also as table in the cache for faster access
	index = (#nodes) + 1
	-- the third entry used to be the node name, but that data isn't used anymore. so save nil instead
	saveFile.data[ map ][ pinTypeId ][index] = Harvest.Serialize({ x, y, nil, itemID, stamp })
	local globalX, globalY = GPS:LocalToGlobal(x, y)
	nodes[index] = {data={ x, y, nil, itemID, stamp }, time=GetFrameTimeSeconds(), global = {globalX, globalY} }
end

do
	local GetInteractionType = _G["GetInteractionType"]
	local INTERACTION_HARVEST = _G["INTERACTION_HARVEST"]
	local GetInteractionType = _G["GetInteractionType"]

	function Harvest.OnUpdate(time)
		-- delayed error messages for when a process must not crash (ie deserialization)
		if Harvest.error then
			local e = Harvest.error
			Harvest.error = nil
			error(e)
		end

		-- update the update queue (importing/refactoring data)
		if not Harvest.IsUpdateQueueEmpty() then
			Harvest.UpdateUpdateQueue()
			return
		end

		-- is there a pinType whose pins have to be refreshed? (ie was something harvested)
		if Harvest.needsRefresh then
			-- only refresh the data after the loot window is closed
			-- (AUI prevents refreshing pins while the loot window is open)
			if LOOT_WINDOW.control:IsControlHidden() then
				for pinTypeId,need in pairs(Harvest.needsRefresh) do
					if need then
						Harvest.RefreshPins( pinTypeId )
					end
				end
				HarvestHeat.RefreshHeatmap()
				Harvest.needsRefresh = nil
			end
		end

		local interactionType = GetInteractionType()
		local isHarvesting = (interactionType == INTERACTION_HARVEST)

		-- update the harvesting state. check if the character was harvesting something during the last two seconds
		if not isHarvesting then
			if Harvest.wasHarvesting and time - Harvest.harvestTime > 2000 then
				Harvest.Debug( "Two seconds since last harvesting action passed. Set harvesting state to false." )
				Harvest.wasHarvesting = false
			end
		else
			if not Harvest.wasHarvesting then
				Harvest.Debug( "Started harvesting. Set harvesting state to true." )
			end
			Harvest.wasHarvesting = true
			Harvest.harvestTime = time
		end

		-- the character started a new interaction
		if interactionType ~= Harvest.lastInteractType then
			Harvest.lastInteractType = interactionType
			-- the character started picking a lock
			if interactionType == INTERACTION_LOCKPICK then
				-- if the interactable is owned by an NPC but the action isn't called "Steal From"
				-- then it wasn't a safebox but a simple door. so don't place a chest pin
				if Harvest.lastInteractableOwned and (not (Harvest.lastInteractableAction == GetString(SI_GAMECAMERAACTIONTYPE20))) then
					Harvest.Debug( "not a chest or justice container(?)" )
					return
				end
				local zone, x, y = Harvest.GetLocation()
				-- normal chests aren't owned and their interaction is called "unlock"
				-- other types of chests (ie for heists) aren't owned but their interaction is "search"
				-- safeboxes are owned
				if (not Harvest.lastInteractableOwned) and Harvest.lastInteractableAction == GetString(SI_GAMECAMERAACTIONTYPE12) then
					-- normal chest
					if not Harvest.IsPinTypeSavedOnGather( Harvest.CHESTS ) then
						Harvest.Debug( "chests are disabled" )
						return
					end
					Harvest.SaveData( zone, x, y, Harvest.CHESTS, Harvest.lastInteractableName, nil )
					Harvest.RefreshPins( Harvest.CHEST )
				else
					-- heist chest or safebox
					if not Harvest.IsPinTypeSavedOnGather( Harvest.JUSTICE ) then
						Harvest.Debug( "justice containers are disabled" )
						return
					end
					Harvest.SaveData( zone, x, y, Harvest.JUSTICE, Harvest.lastInteractableName, nil )
					Harvest.RefreshPins( Harvest.JUSTICE )
				end
			end
			-- the character started fishing
			if interactionType == INTERACTION_FISH then
				-- don't create new pin if fishing pins are disabled
				if not Harvest.IsPinTypeSavedOnGather( Harvest.FISHING ) then
					Harvest.Debug( "fishing spots are disabled" )
					return
				end
				local zone, x, y = Harvest.GetLocation()
				Harvest.SaveData( zone, x, y, Harvest.FISHING, "fish", nil )
				Harvest.RefreshPins( Harvest.FISHING )
			end
		end
		-- update the respawn timer feature
		Harvest.UpdateHiddenTime(time / 1000) -- function was written with seconds in mind instead of miliseconds
		--HarvestFarm.UpdatePosition(time)
	end
end

-- harvestmap will hide recently visited pins for a given respawn time (time is set in the options)
-- this function handles this respawn timer feature
-- this function also updates the last visited pin for the farming helper
do
	local GetMapPlayerPosition = _G["GetMapPlayerPosition"]
	local pairs = _G["pairs"]
	local tostring = _G["tostring"]

	function Harvest.UpdateHiddenTime(time)
		local hiddenTime = Harvest.GetHiddenTime()
		-- this function could result in a performance loss
		-- so don't do anything if it isn't needed
		if hiddenTime == 0 then
			return
		end
		hiddenTime = hiddenTime * 60 -- minutes to seconds

		local map = Harvest.GetMap()
		local x, y = GetMapPlayerPosition( "player" )
		local dx, dy
		local minDistance = Harvest.GetMinDistanceBetweenPins()
		local nodes, pinType
		local onHarvest = Harvest.IsHiddenOnHarvest()
		-- iterating over all the pins on the current map
		for _, pinTypeId in pairs(Harvest.PINTYPES) do
			-- if the pins are insivible, there is nothing we need to do...
			if Harvest.IsPinTypeVisible( pinTypeId ) then
				nodes = Harvest.GetNodesOnMap( map, pinTypeId )
				pinType = Harvest.GetPinType( pinTypeId )
				-- check if one of the visible pins needs to be hidden
				for _, node in pairs(nodes) do
					dx = x - node.data[1]
					dy = y - node.data[2]
					if (not onHarvest) and dx * dx + dy * dy < minDistance then
						-- the player is close to the pin
						-- now check if it has a pin
						if not node.hidden then
							Harvest.Debug( "respawn timer has hidden a pin of pin type " .. tostring(pinType) )
							LMP:RemoveCustomPin( pinType, node.data )
							COMPASS_PINS.pinManager:RemovePin( node.data, pinType )
							node.hidden = true
						end
						node.time = time
					else
						-- the player isn't close to the pin, so check if we have to show it again
						if node.hidden and (time - node.time > hiddenTime) then
							--if not ZO_WorldMap_IsWorldMapShowing() then
							--	if(SetMapToPlayerLocation() == SET_MAP_RESULT_MAP_CHANGED) then
							--		CALLBACK_MANAGER:FireCallbacks("OnWorldMapChanged")
							--		return
							--	end
							--end
							Harvest.Debug( "respawn timer displayed pin " .. tostring(node.data) .. " of pin type " .. tostring(pinType) .. " again" )
							LMP:CreatePin( pinType, node.data, node.data[1], node.data[2] )
							COMPASS_PINS.pinManager:CreatePin( pinType, node.data, node.data[1], node.data[2] )
							node.hidden = false
						end
					end
				end
			end
		end
	end
end

-- this hack saves the object's name that was last interacted with
local oldInteract = FISHING_MANAGER.StartInteraction
FISHING_MANAGER.StartInteraction = function(...)
	local action, name, blockedNode, isOwned = GetGameCameraInteractableActionInfo()
	Harvest.lastInteractableAction = action
	Harvest.lastInteractableName = name
	Harvest.lastInteractableOwned = isOwned
	return oldInteract(...)
end

-- some data structures canot be properly saved, this function restores them after the addon is fully loaded
function Harvest.FixSaveFile()
	-- functions can not be saved, so reload them
	for pinTypeId, layout in pairs( Harvest.GetCompassLayouts() ) do
		if pinTypeId == Harvest.TOUR then
			layout.additionalLayout = {HarvestFarm.additionalLayout, HarvestFarm.additionalLayoutReset}
		else
			layout.additionalLayout = {Harvest.additionalLayout, Harvest.additionalLayoutReset}
		end
	end
	-- tints cannot be saved (only as rgba table) so restore these tables to tint objects
	for _, layout in pairs( Harvest.GetMapLayouts() ) do
		if layout.color then
			layout.tint = ZO_ColorDef:New(unpack(layout.color))
			layout.color = nil
		else
			layout.tint = ZO_ColorDef:New(layout.tint)
		end
	end
end

-- returns hours since 1970
function Harvest.GetCurrentTimestamp()
	-- data is saved as string, to prevent the save file from bloating up, reduce the stamp to hours
	return zo_floor(GetTimeStamp() / 3600)
end

-- imports all the nodes on 'map' from the table 'data' into the table 'target'
-- if checkPinType is true, data will be skipped if Harvest.IsPinTypeSavedOnImport(pinTypeId) returns false
function Harvest.ImportFromMap( map, data, target, checkPinType )
	-- nothing to merge, data can simply be copied
	if target.data[ map ] == nil then
		target.data[ map ] = data
		return
	end
	-- the target table contains already a bunch of nodes, so the data has to be merged
	local targetData = nil
	local newNode = nil
	local index = 0
	local oldNode = nil
	local timestamp = Harvest.GetCurrentTimestamp()
	for _, pinTypeId in pairs( Harvest.PINTYPES ) do
		if (not checkPinType) or Harvest.IsPinTypeSavedOnImport( pinTypeId ) then
			if target.data[ map ][ pinTypeId ] == nil then
				-- nothing to merge for this pin type, just copy it
				target.data[ map ][ pinTypeId ] = data[ pinTypeId ]
			else
				-- deserialize target data and clear the serialized target data table (we'll fill it again at the end)
				targetData = {}
				for _, node in pairs( target.data[ map ][ pinTypeId ] ) do
					node = Harvest.Deserialize(node)
					if node then -- check if something went wrong while deserializing the node
						table.insert(targetData, {data=node, time=0})
					end
				end
				target.data[ map ][ pinTypeId ] = {}
				-- deserialize every new node and update the old nodes accordingly
				data[ pinTypeId ] = data[ pinTypeId ] or {}
				for _, entry in pairs( data[ pinTypeId ] ) do
					newNode = Harvest.Deserialize( entry )
					if newNode then -- check if something went wrong while deserializing the node
						-- If the node is new enough to be saved
						if (Harvest.GetMaxTimeDifference() == 0) or ((timestamp - (newNode[5] or 0)) <= Harvest.GetMaxTimeDifference()) then
							-- If we have found this node already then we don't need to save it again
							index = Harvest.IsNodeAlreadyFound( targetData, newNode[1], newNode[2] )
							if index then
								oldNode = targetData[ index ].data
								-- add the node's item ids
								if newNode[4] then
									-- there are reports of field 4 not being a table?! could be a harvest merge bug
									-- or something went wrong in the TG update procedure
									-- anyhow... this code will fix it
									if type(newNode[4] == "number") then
										newNode[4] = {[newNode[4]] = 0}
									end
									-- merge itemid->timestamp tables by saving the higher timestamps of both tables
									if oldNode[4] then
										if type(oldNode[4] == "number") then
											oldNode[4] = {[oldNode[4]] = 0}
										end
										
										for itemId, stamp in pairs( newNode[4] ) do
											oldNode[4][itemId] = zo_max(oldNode[4][itemId] or 0, stamp)
										end
									else
										oldNode[4] = newNode[4]
									end
								end
								-- update the timestamp of the node, to confirm it's a recent position
								-- keep the newer position as that one is less likely to be wrong/outdated
								if oldNode[5] and newNode[5] then
									if oldNode[5] < newNode[5] then
										oldNode[5] = newNode[5]
										oldNode[1] = newNode[1]
										oldNode[2] = newNode[2]
									end
								elseif newNode[5] then
									oldNode[5] = newNode[5]
									oldNode[1] = newNode[1]
									oldNode[2] = newNode[2]
								end
							else
								table.insert(targetData, {data=newNode, time=0})
							end
						end
					end
				end
				-- serialize the new data
				for _, node in pairs( targetData ) do
					table.insert(target.data[ map ][ pinTypeId ], Harvest.Serialize(node.data))
				end
			end
		end
	end
end


-- returns the correct table for the map (HarvestMap, HarvestMapAD/DC/EP save file tables)
-- will return HarvestMap's table if the correct table doesn't currently exist.
-- ie the HarvestMapAD addon isn't currently active
function Harvest.GetSaveFile( map )
	return Harvest.GetSpecialSaveFile( map ) or Harvest.savedVars["nodes"]
end

-- returns the correct (external) table for the map or nil if no such table exists
function Harvest.GetSpecialSaveFile( map )
	local zone = string.gsub( map, "/.*$", "" )
	if HarvestAD then
		if HarvestAD.zones[ zone ] then
			return Harvest.savedVars["ADnodes"]
		end
	end
	if HarvestEP then
		if HarvestEP.zones[ zone ] then
			return Harvest.savedVars["EPnodes"]
		end
	end
	if HarvestDC then
		if HarvestDC.zones[ zone ] then
			return Harvest.savedVars["DCnodes"]
		end
	end
	return nil
end

-- this function moves data from the HarvestMap addon to HarvestMapAD/DC/EP
function Harvest.MoveData()
	for map, data in pairs( Harvest.savedVars["nodes"].data ) do
		local zone = string.gsub( map, "/.*$", "" )
		local file = Harvest.GetSpecialSaveFile( map )
		if file ~= nil then
			Harvest.AddToUpdateQueue(function()
				Harvest.ImportFromMap( map, data, file )
				Harvest.savedVars["nodes"].data[ map ] = nil
				Harvest.Debug("Moving old data to the correct save files. " .. tostring(Harvest.GetQueuePercent()) .. "%")
			end)
		end
	end
end

-- data is stored as ACE strings
-- this functions deserializes the strings and saves the results in the cache
function Harvest.LoadToCache( map, pinTypeId )
	local saveFile = Harvest.GetSaveFile(map)
	local maxIndex = 0
	Harvest.cache[ map ] = Harvest.cache[ map ] or {}
	-- only deserialize/load the data if it hasn't been loaded already
	if Harvest.cache[ map ][ pinTypeId ] == nil then
		local cachedNodes = {}
		local measurement = GPS:GetCurrentMapMeasurements()
		local x1, y1, x2, y2
		-- create table if it doesn't exist yet
		saveFile.data[ map ] = (saveFile.data[ map ]) or {}
		saveFile.data[ map ][ pinTypeId ] = (saveFile.data[ map ][ pinTypeId ]) or {}
		local nodes = saveFile.data[ map ][ pinTypeId ]
		local removedIndices = {}
		local deserializedNode
		local timestamp = Harvest.GetCurrentTimestamp()
		for index, node in pairs( nodes ) do
			deserializedNode = Harvest.Deserialize( node )
			if deserializedNode and ((Harvest.GetMaxTimeDifference() == 0) or ((timestamp - (deserializedNode[5] or 0)) < Harvest.GetMaxTimeDifference())) then
				cachedNodes[ index ] = {data=deserializedNode, time=0}
				x1, y1 = GPS:LocalToGlobal(deserializedNode[1], deserializedNode[2])
				cachedNodes[ index ].global = {x1, y1}
				
				maxIndex = zo_max(maxIndex, index)
			end
		end
		Harvest.cache[ map ][ pinTypeId ] = cachedNodes
		-- merge close nodes based on more accurate map size measurements
		-- no point in doing this, if map couldn't be meassured
		if not measurement then
			return
		end
		-- some maps (ie fulstrom manor) have no width/height
		if measurement.scaleX == 0 or measurement.scaleY == 0 then
			return
		end
		local dx, dy
		local nodeA, nodeB
		local distance = Harvest.GetGlobalMinDistanceBetweenPins()
		for i = 1, maxIndex do if cachedNodes[i] then
			nodeA = cachedNodes[i]
			x1, y1 = unpack(nodeA.global)
			for j = i+1, maxIndex do if cachedNodes[j] then
				nodeB = cachedNodes[j]
				x2, y2 = unpack(nodeB.global)
				
				dx = x1 - x2
				dy = y1 - y2
				if dx * dx + dy * dy < distance then
					if (nodeA.data[5] or 0) > (nodeB.data[5] or 0) then
						if nodeB.data[4] then
							nodeA.data[4] = nodeA.data[4] or {}
							for itemId, stamp in pairs(nodeB.data[4]) do
								nodeA.data[4][itemId] = zo_max(nodeA.data[4][itemId] or 0, stamp)
							end
						end
						cachedNodes[j] = nil
						removedIndices[j] = true
					else
						if nodeA.data[4] then
							nodeB.data[4] = nodeB.data[4] or {}
							for itemId, stamp in pairs(nodeA.data[4]) do
								nodeB.data[4][itemId] = zo_max(nodeB.data[4][itemId] or 0, stamp)
							end
						end
						cachedNodes[i] = nil
						removedIndices[i] = true
						break
					end
				end
			end; end
		end; end
		-- delete removed/filtered nodes from the save file
		for index, _ in pairs(removedIndices) do
			nodes[index] = nil
		end
	end
end

-- Deserializes each node and caches the deserialized nodes
function Harvest.GetNodesOnMap( map, pinTypeId )
	Harvest.LoadToCache( map, pinTypeId )
	return Harvest.cache[ map ][ pinTypeId ]
end

function Harvest.GetErrorLog()
	return Harvest.savedVars["global"].errorlog
end

function Harvest.AddToErrorLog(message)
	local log = Harvest.savedVars["global"].errorlog
	if #log - log.start > Harvest.logSize then
		log[log.start] = nil
		log.start = log.start + 1
	end
	table.insert(Harvest.savedVars["global"].errorlog, message)
end

function Harvest.ClearErrorLog()
	Harvest.savedVars["global"].errorlog = {start = 1}
end

function Harvest.InitializeSavedVariables()
	Harvest.savedVars = {}
	-- global settings that are always account wide
	Harvest.savedVars["global"] = ZO_SavedVars:NewAccountWide("Harvest_SavedVars", 3, "global", Harvest.defaultGlobalSettings)
	if not Harvest.savedVars["global"].maxTimeDifference then --this setting was added in 3.0.8
		Harvest.savedVars["global"].maxTimeDifference = 0
	end
	if not Harvest.savedVars["global"].errorlog then --this log was added in 3.1.11
		Harvest.savedVars["global"].errorlog = {start = 1}
	end
	-- nodes are saved account wide
	Harvest.savedVars["nodes"]  = ZO_SavedVars:NewAccountWide("Harvest_SavedVars", 2, "nodes", Harvest.dataDefault)
	if not Harvest.savedVars["nodes"].firstLoaded then
		Harvest.savedVars["nodes"].firstLoaded = Harvest.GetCurrentTimestamp()
	end
	Harvest.savedVars["nodes"].lastLoaded = Harvest.GetCurrentTimestamp()
	-- load other node addons, if they are activated
	if HarvestAD then
		Harvest.savedVars["ADnodes"]  = HarvestAD.savedVars
	end
	if HarvestEP then
		Harvest.savedVars["EPnodes"]  = HarvestEP.savedVars
	end
	if HarvestDC then
		Harvest.savedVars["DCnodes"]  = HarvestDC.savedVars
	end
	-- depending on the account wide setting, the settings may not be saved per character
	if Harvest.savedVars["global"].accountWideSettings then
		Harvest.savedVars["settings"] = ZO_SavedVars:NewAccountWide("Harvest_SavedVars", 2, "settings", Harvest.defaultSettings )
	else
		Harvest.savedVars["settings"] = ZO_SavedVars:New("Harvest_SavedVars", 2, "settings", Harvest.defaultSettings )
	end
	-- the settings might be from a previous HarvestMap version, in which case there might be missing attributes
	-- initilaize these missing attributes
	for key, value in pairs(Harvest.defaultSettings) do
		if Harvest.savedVars["settings"][key] == nil then
			Harvest.savedVars["settings"][key] = value
		end
	end
	Harvest.savedVars["settings"].mapLayouts[Harvest.JUSTICE].texture = Harvest.defaultSettings.mapLayouts[Harvest.JUSTICE].texture
	Harvest.savedVars["settings"].compassLayouts[Harvest.JUSTICE].texture = Harvest.defaultSettings.compassLayouts[Harvest.JUSTICE].texture
end

function Harvest.OnLoad(eventCode, addOnName)
	if addOnName ~= "HarvestMap" then
		return
	end
	-- initialize temporary variables
	Harvest.wasHarvesting = false
	Harvest.action = nil
	-- cache the ACE deserialized nodes
	-- this way changing maps multiple times will create less lag
	Harvest.cache = {}
	-- mapCounter and compassCounter are used by the delayed pin creation procedure
	-- these procedures are in the HarvestMapMarkers.lua and HarvestMapCompass.lua
	Harvest.mapCounter = {}
	for _, pinTypeId in pairs(Harvest.PINTYPES) do
		Harvest.mapCounter[Harvest.GetPinType( pinTypeId )] = 0
	end
	Harvest.compassCounter = {}
	for _, pinTypeId in pairs(Harvest.PINTYPES) do
		Harvest.compassCounter[Harvest.GetPinType( pinTypeId )] = 0
	end
	-- initialize save variables
	Harvest.InitializeSavedVariables()
	-- check if saved data is from an older version,
	-- update the data if needed
	Harvest.UpdateDataVersion()
	-- move data to correct save files
	-- if AD was disabled while harvesting in AD, everything was saved in ["nodes"]
	-- when ad is enabled, everything needs to be moved to that save file
	-- HOWEVER, only execute this after the save files were updated!
	Harvest.AddToUpdateQueue(Harvest.MoveData)
	-- some data cannot be properly saved, ie functions or tints.
	-- repair this data
	Harvest.FixSaveFile()
	-- initialize pin callback functions
	Harvest.InitializeMapMarkers()
	Harvest.InitializeCompassMarkers()
	-- create addon option panels
	Harvest.InitializeOptions()
	-- initialize bonus features
	HarvestHeat.Initialize()
	HarvestFarm.Initialize()

	EVENT_MANAGER:RegisterForUpdate("HarvestMap", 200, Harvest.OnUpdate)
	-- add these callbacks only after the addon has loaded to fix SnowmanDK's bug (comment section 20.12.15)
	EVENT_MANAGER:RegisterForEvent("HarvestMap", EVENT_LOOT_RECEIVED, Harvest.OnLootReceived)
	EVENT_MANAGER:RegisterForEvent("HarvestMap", EVENT_LOOT_UPDATED, Harvest.OnLootUpdated)
	
end

-- initialization which is dependant on other addons is done on EVENT_PLAYER_ACTIVATED
-- because harvestmap might've been loaded before them
function Harvest.OnActivated()
	HarvestFarm.PostInitialize()
	EVENT_MANAGER:UnregisterForEvent("HarvestMap", EVENT_PLAYER_ACTIVATED, Harvest.OnActivated)
end

EVENT_MANAGER:RegisterForEvent("HarvestMap", EVENT_ADD_ON_LOADED, Harvest.OnLoad)
EVENT_MANAGER:RegisterForEvent("HarvestMap", EVENT_PLAYER_ACTIVATED, Harvest.OnActivated)
