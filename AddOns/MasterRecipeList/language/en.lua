local ESOMRL = _G['ESOMRL']
local L = {}

------------------------------------------------------------------------------------------------------------------
-- English
------------------------------------------------------------------------------------------------------------------

-- General strings
	L.ESOMRL_SEARCHBOX = 'Search for recipe name.'
	L.ESOMRL_WRITQUEST = 'Provisioner Writ'
	L.ESOMRL_WRITFINISH = 'Deliver Goods'
	L.ESOMRL_CRAFT = 'Craft'
	L.ESOMRL_CLEAR = 'Clear active search list.'
	L.ESOMRL_ICLEAR = 'Clear ingredient search list.'
	L.ESOMRL_TRACKING = 'Tracking'
	L.ESOMRL_RECIPES = 'recipes.'
	L.ESOMRL_INGREDIENTS = 'ingredients.'
	L.ESOMRL_CLOSE = 'Close Recipe List'
	L.ESOMRL_SHOWING = 'Show Ingredients List'
	L.ESOMRL_SHOWRECIPE = 'Show Recipe List'
	L.ESOMRL_LTTSHOW = 'Enable Popup Tooltips'
	L.ESOMRL_LTTHIDE = 'Disable Popup Tooltips'
	L.ESOMRL_TTRECIPE = 'Show Recipe in Tooltips'
	L.ESOMRL_TTFOOD = 'Show Food in Tooltips'
	L.ESOMRL_FINDWRIT = 'Find Writ Requirements '
	L.ESOMRL_TRACKRING = 'Mark all tracked recipe ingredients.'
	L.ESOMRL_TRACKALLING = 'Mark all ingredients.'
	L.ESOMRL_JUNKUING = 'Click to automatically set unmarked ingredients you loot as junk.'
	L.ESOMRL_DJUNKUING = 'Click to stop automatically setting unmarked ingredients you loot as junk.'
	L.ESOMRL_DESTROYUING = 'Click to automatically destroy (permanently) unmarked ingredients you loot.'
	L.ESOMRL_DDESTROYUING = 'Click to stop automatically destroying unmarked ingredients you loot.'
	L.ESOMRL_LEVEL = 'Level'
	L.ESOMRL_VETERAN = 'Veteran'
	L.ESOMRL_ANY = 'Any Level'
	L.ESOMRL_H = '(Health)'
	L.ESOMRL_M = '(Magicka)'
	L.ESOMRL_S = '(Stamina)'
	L.ESOMRL_HM = '(Health/Magicka)'
	L.ESOMRL_HS = '(Health/Stamina)'
	L.ESOMRL_MS = '(Magicka/Stamina)'
	L.ESOMRL_HMS = '(Health/Magicka/Stamina)'
	L.ESOMRL_RTRACK = 'ON RECIPE TRACKING LIST'
	L.ESOMRL_ITRACK = 'ON INGREDIENT TRACKING LIST'
	L.ESOMRL_NWRITU = 'UNKNOWN & NEEDED FOR CURRENT WRIT'
	L.ESOMRL_NWRITK = 'KNOWN & NEEDED FOR CURRENT WRIT'
	L.ESOMRL_NWRIT = 'NEEDED FOR CURRENT WRIT'
	L.ESOMRL_MAKE = 'Makes'
	L.ESOMRL_KNOWN = 'KNOWN BY'
	L.ESOMRL_CRAFTABLE = 'CRAFTABLE BY'
	L.ESOMRL_TRACKUNKNOWN = 'Mark all recipes the selected character does not know.'
	L.ESOMRL_TRACKALL = 'Mark all recipes.'
	L.ESOMRL_JUNKURECIPE = 'Click to automatically set unmarked recipes you loot as junk (set quality limit in addon settings).'
	L.ESOMRL_DJUNKURECIPE = 'Click to stop automatically setting unmarked recipes you loot as junk.'
	L.ESOMRL_DESTROYURECIPE = 'Click to automatically destroy (permanently) unmarked recipes you loot (set quality limit in addon settings).'
	L.ESOMRL_DDESTROYURECIPE = 'Click to stop automatically destroying unmarked recipes you loot.'
	L.ESOMRL_SRCONFIGPANEL = 'Show the recipe tracking config panel.'
	L.ESOMRL_HRCONFIGPANEL = 'Hide the recipe tracking config panel.'
	L.ESOMRL_REMOVECHAR = 'Remove selected character from the tracking database (log in as them to re-add if removed accidentally).'
	L.ESOMRL_QUALITY1 = 'Green'
	L.ESOMRL_QUALITY2 = 'Blue'
	L.ESOMRL_QUALITY3 = 'Purple'
	L.ESOMRL_SAVEINGTP = 'Save current tracked ingredient list to the global profile.'
	L.ESOMRL_LOADINGTP = 'Load the global ingredient tracking profile.'
	L.ESOMRL_FINDRECIPES = 'Find recipes containing selected ingredients.'
	L.ESOMRL_INGFOOD = 'INGREDIENTS'
	L.ESOMRL_SEARCHS = 'Search MRL'
	L.ESOMRL_STRACKY = 'Highlight Tracked'
	L.ESOMRL_STRACKN = 'Don\'t Highlight Tracked'

-- Settings panel
	L.ESOMRL_SHOWKNOWN = 'Show \'known by\' in tooltips:'
	L.ESOMRL_SHOWKNOWND = 'When enabled, the \'known by\' and \'craftable by\' sections will show in all recipe and food item tooltips known by any currently tracked characters.'
	L.ESOMRL_SHOWINGFOOD = 'Show ingredients on food items:'
	L.ESOMRL_SHOWINGFOODD = 'When enabled, the list of ingredients required will appear on craftable food items as it does on recipes.'
	L.ESOMRL_ICONPOSI = 'Inventory icon position:'
	L.ESOMRL_ICONPOSID = 'Adjust the horizontal positioning of the inventory tracking icons. (Disabled when \'Use FCO ItemSaver for tracked items\' is enabled in Third-Party Addon Support section.)'
	L.ESOMRL_ICONPOSB = 'Guild store search icon position:'
	L.ESOMRL_ICONPOSBD = 'Adjust the horizontal positioning of the guild store search result tracking icons.'
	L.ESOMRL_ICONPOSL = 'Guild store listing icon position:'
	L.ESOMRL_ICONPOSLD = 'Adjust the horizontal positioning of the guild store sale listing tracking icons.'
	L.ESOMRL_ETRACKING = 'Enable tracking:'
	L.ESOMRL_TRACKDESC = 'Character progress will be tracked for display in the \'known by\' recipe tooltip section.'
	L.ESOMRL_TRACKWARN = 'Requires /reloadui or relog to take effect.'
	L.ESOMRL_INVENTORYI = 'Enable inventory icons:'
	L.ESOMRL_INVENTORYD = 'Show icons in your inventory for tracked recipes and ingredients.'
	L.ESOMRL_IDESTROY = 'Destroy junk ingredients:'
	L.ESOMRL_IDESTDESC = 'When enabled and the junk/destroy unmarked ingredient pin is on, unmarked ingredients are destroyed instead of marked as junk.'
	L.ESOMRL_IDESTROYWARN = 'All newly looted ingredients NOT set to tracking will be PERMANENTLY destroyed!'
	L.ESOMRL_DEBUGMODE = 'Enable debug mode:'
	L.ESOMRL_DEBUGDESC = 'Display information to chat whenever recipes or ingredients are destroyed or marked as junk.'
	L.ESOMRL_STACKSIZE = 'Max amount to destroy:'
	L.ESOMRL_STACKDESC = 'Total carried ingredient stack count greater than this number will not be set to junk or deleted, regardless of other settings.'
	L.ESOMRL_RDESTROY = 'Destroy junk recipes:'
	L.ESOMRL_RDESTDESC = 'When enabled and the junk/destroy unmarked recipe pin is on, unmarked recipes are destroyed instead of marked as junk.'
	L.ESOMRL_RDESTROYWARN = 'All recipes NOT set to tracking and lower than the quality limit set below will be PERMANENTLY destroyed!'
	L.ESOMRL_DQUALITY = 'Color quality protection:'
	L.ESOMRL_DQUALITYDESC = 'Recipes of this color quality level and higher will not be set to junk or deleted, regardless of other settings.'
	L.ESOMRL_GLOBALOPT = 'Global options'
	L.ESOMRL_CHAROPT = 'Character options'
	L.ESOMRL_ADDONS = 'Third-Party Addon Support'
	L.ESOMRL_ADDONSD = 'Options for supported third-party addons.'
	L.ESOMRL_FCOITEM = 'Use FCO ItemSaver for tracked items:'
	L.ESOMRL_FCOITEMD = 'When checked, tracked recipes and ingredients in your inventory or other bags will be marked with your FCO ItemSaver \'locked\' icon instead of the MRL check mark and will be set to locked in FCO ItemSaver. Default MRL marks still appear in the guild store.'
	L.ESOMRL_ISTOLEN = 'Ignore Stolen Items'
	L.ESOMRL_ISTOLEND = 'When enabled and using above features to automatically set to junk or delete unmarked recipes and ingredients, stolen items will be ignored.'
	L.ESOMRL_ISTOLENM = 'ignored by stolen item filter.'
	L.ESOMRL_IDESTROYM = 'was permanently destroyed.'
	L.ESOMRL_IJUNKM = 'was set to junk.'
L.ESOMRL_TCHART = 'Enable tracking character'
L.ESOMRL_TCHARTD = 'Enable below options to show recipe status of selected character.'
	L.ESOMRL_TCHAR = 'Select tracking character'
	L.ESOMRL_TCHARD = 'Recipes NOT known by the selected character will show a green check in the inventories.'
	L.ESOMRL_ALTIU = 'Show known recipe icon'
	L.ESOMRL_ALTIUD = 'Enabling this option shows a grey check for recipes already known by the above selected character.'
	L.ESOMRL_SSTATS = 'Cooking station stat icons'
	L.ESOMRL_SSTATSD = 'Show colored icons for which stats a category\'s food or drink items buff at the cooking station.'
	L.ESOMRL_SSTATICONS = 'Stat icon style'
	L.ESOMRL_SSTATICONSD = 'Style of icons to display when cooking station stat icons are enabled.'
	L.ESOMRL_ICONTT1 = 'Stars'
	L.ESOMRL_ICONTT2 = 'Circles'
	L.ESOMRL_ALTIS = 'Show known for current instead?'
	L.ESOMRL_ALTISD = 'When set to on, grey icons will show for recipes the CURRENT character knows instead of for recipes the above selected character knows. \'Show known recipe icon\' must be on for this to have an effect.'

-- Info tooltip
	L.ESOMRL_IKNOWN = ' = Known recipe.'
	L.ESOMRL_IKNOWNT = ' = Known recipe, tracking.'
	L.ESOMRL_IUNKNOWN = ' = Unknown recipe.'
	L.ESOMRL_IUNKNOWNT = ' = Unknown recipe, tracking.'
	L.ESOMRL_IWRIT = ' = Unknown, needed for current writ.'
	L.ESOMRL_IWTRACK = ' = Unknown, needed for writ, tracking.'
	L.ESOMRL_SHIFTCLICK = 'Shift-click items to link in chat.'
	L.ESOMRL_INGGOLD = 'Tracked ingredients are colored gold.'

-- Food categories
	L.ESOMRL_FOOD = 'Food'
	L.ESOMRL_TFOOD = 'Food Recipes'

-- Drink categories
	L.ESOMRL_DRINK = 'Drink'
	L.ESOMRL_TDRINK = 'Drink Recipes'

------------------------------------------------------------------------------------------------------------------

for i = 1, 14 do
	local recipeListName, numRecipes, upIcon, downIcon, overIcon, disabledIcon, createSound = GetRecipeListInfo(i)
	if i == 1 then L.ESOMRL_FOOD1 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 2 then L.ESOMRL_FOOD2 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 3 then L.ESOMRL_FOOD3 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 4 then L.ESOMRL_FOOD4 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 5 then L.ESOMRL_FOOD5 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 6 then L.ESOMRL_FOOD6 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 7 then L.ESOMRL_FOOD7 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 8 then L.ESOMRL_DRINK1 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 9 then L.ESOMRL_DRINK2 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 10 then L.ESOMRL_DRINK3 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 11 then L.ESOMRL_DRINK4 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 12 then L.ESOMRL_DRINK5 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 13 then L.ESOMRL_DRINK6 = zo_strformat("<<t:1>>",recipeListName)
	elseif i == 14 then L.ESOMRL_DRINK7 = zo_strformat("<<t:1>>",recipeListName) end
end

function ESOMRL:GetLanguage() -- default locale, will be the return unless overwritten
	return L
end
