local ESOMRL = _G['ESOMRL']
local L = {}

------------------------------------------------------------------------------------------------------------------
-- German (Thanks to ESOUI.com user Baertram for the excellent translations!)
------------------------------------------------------------------------------------------------------------------

-- General strings
	L.ESOMRL_SEARCHBOX = 'Nach Rezeptnamen suchen.'
	L.ESOMRL_WRITQUEST = 'Versorgerschrieb'
	L.ESOMRL_WRITFINISH = 'Beliefert'
	L.ESOMRL_CRAFT = 'Stellt'
	L.ESOMRL_CLEAR = 'Leere aktive Suchliste.'
	L.ESOMRL_ICLEAR = 'Leere Zutaten Suchliste.'
	L.ESOMRL_TRACKING = 'Wird beobachtet'
	L.ESOMRL_RECIPES = 'rezepte.'
	L.ESOMRL_INGREDIENTS = 'Zutaten.'
	L.ESOMRL_CLOSE = 'Schließe Rezeptliste'
	L.ESOMRL_SHOWING = 'Zeige Zutatenliste'
	L.ESOMRL_SHOWRECIPE = 'Zeige Rezeptliste'
	L.ESOMRL_LTTSHOW = 'Aktiviere Popup Tooltips'
	L.ESOMRL_LTTHIDE = 'Deaktiviere Popup Tooltips'
	L.ESOMRL_TTRECIPE = 'Zeige Rezept im Tooltip an'
	L.ESOMRL_TTFOOD = 'Zeige Nahrung im Tooltip an'
	L.ESOMRL_FINDWRIT = 'Finde Schrieb Bestandteile '
	L.ESOMRL_TRACKRING = 'Markiere alle Zutaten beobachteter Rezepte.'
	L.ESOMRL_TRACKALLING = 'Markiere alle Zutaten.'
	L.ESOMRL_JUNKUING = 'Setze unmarkierte Zutaten automatisch auf die Abfall Liste.'
	L.ESOMRL_DJUNKUING = 'Beende das automatische auf die Abfall Liste setzen für unmarkierte Zutaten'
	L.ESOMRL_DESTROYUING = 'Zerstöre unmarkierte Zutaten automatisch beim Looten.'
	L.ESOMRL_DDESTROYUING = 'Beende das Zerstören unmarkierte Zutaten automatisch beim Looten.'
	L.ESOMRL_LEVEL = 'Stufe'
	L.ESOMRL_VETERAN = 'Veteranenrang'
	L.ESOMRL_ANY = 'Irgendein Stufe'
	L.ESOMRL_H = '(Leben)'
	L.ESOMRL_M = '(Magicka)'
	L.ESOMRL_S = '(Ausdauer)'
	L.ESOMRL_HM = '(Leben/Magicka)'
	L.ESOMRL_HS = '(Leben/Ausdauer)'
	L.ESOMRL_MS = '(Magicka/Ausdauer)'
	L.ESOMRL_HMS = '(Leben/Magicka/Ausdauer)'
	L.ESOMRL_RTRACK = 'AUF DER REZEPT BEOBACHTUNGSLISTE'
	L.ESOMRL_ITRACK = 'AUF VERFOLGT ZUTATENLISTE'
	L.ESOMRL_NWRITU = 'UNBEKANNT & FÜR AKTUELLEN SCHRIEB BENÖTIGT'
	L.ESOMRL_NWRITK = 'BEKANNT & FÜR AKTUELLEN SCHRIEB BENÖTIGT'
	L.ESOMRL_NWRIT = 'FÜR AKTUELLEN SCHRIEB BENÖTIGT'
	L.ESOMRL_MAKE = 'Ergibt'
	L.ESOMRL_KNOWN = 'BEKANNT DURCH'
	L.ESOMRL_CRAFTABLE = 'HERSTELLBAR DURCH'
	L.ESOMRL_TRACKUNKNOWN = 'Markiere alle für diesen Charakter unbekannte Rezepte.'
	L.ESOMRL_TRACKALL = 'Markiere alle Rezepte.'
	L.ESOMRL_JUNKURECIPE = 'Klicke, um automatisch alle gelooteten, unmarkierten Rezepte zum Trödel zu verschieben (setze Qualitätslimit in den Einstellungen).'
	L.ESOMRL_DJUNKURECIPE = 'Klicke, um die automatische Trödel Verschiebung für gelootete, unmarkierte Rezepte zu beenden.'
	L.ESOMRL_DESTROYURECIPE = 'Klicke, um automatisch alle gelooteten, unmarkierten Rezepte zu zerstören (setze Qualitätslimit in den Einstellungen).'
	L.ESOMRL_DDESTROYURECIPE = 'Klicke, um die automatische Zerstörung für gelootete, unmarkierte Rezepte zu beenden.'
	L.ESOMRL_SRCONFIGPANEL = 'Zeige die Rezept-Beobachtung Einstellungen.'
	L.ESOMRL_HRCONFIGPANEL = 'Verberge die Rezept-Beobachtung Einstellungen.'
	L.ESOMRL_REMOVECHAR = 'Entferne den ausgewählten Charakter von der Beobachtungsdatenbank (log dich mit diesem ein, um ihn erneut hinzuzufügen, falls du ihn aus Versehen gelöscht hast).'
	L.ESOMRL_QUALITY1 = 'Grün'
	L.ESOMRL_QUALITY2 = 'Blau'
	L.ESOMRL_QUALITY3 = 'Lila'
	L.ESOMRL_SAVEINGTP = 'Speichere aktuelle Zutaten Beobachtungsliste im globalen Profil.'
	L.ESOMRL_LOADINGTP = 'Lade das globale Beobachtungs Profil für Zutaten.'
L.ESOMRL_FINDRECIPES = 'Rezepte finden ausgewählten Zutaten enthalten.'
	L.ESOMRL_INGFOOD = 'ZUTATEN'
	L.ESOMRL_SEARCHS = 'Durchsuche MRL'
	L.ESOMRL_STRACKY = 'Beobachtete hervorheben'
	L.ESOMRL_STRACKN = 'Beobachtete nicht hervorheben'

-- Settings panel
	L.ESOMRL_SHOWKNOWN = 'Zeige \'Bekannt durch\' im Tooltip an:'
	L.ESOMRL_SHOWKNOWND = 'Ist diese Option aktiviert werden die \'Bekannt durch\' und \'Herstellbar durch\' Sektionen in allen Tooltips von Rezepten und Zutaten, für alle beobachteten Charaktere, angezeigt.'
	L.ESOMRL_SHOWINGFOOD = 'Zeige Zutaten bei Nahrung an:'
	L.ESOMRL_SHOWINGFOODD = 'Ist diese Option aktiviert, werden die Zutaten für gekochte Nahrung genau wie bei Rezepten auch angezeigt.'
	L.ESOMRL_ICONPOSI = 'Inventar Symbol Position:'
	L.ESOMRL_ICONPOSID = 'Stelle die horizontale Position der Inventar Symbole für die Markierungen ein (Wird deaktiviert, wenn \'Verwende FCO ItemSaver für Markierungen\' in den \'Andere Addons\' Optionen aktiviert ist.)'
	L.ESOMRL_ICONPOSB = 'Gildenladen Suche Symbol Position:'
	L.ESOMRL_ICONPOSBD = 'Verändere die horizontale Position des Beobachten Symbols in der Gildenladen Suche Ergebnisliste.'
	L.ESOMRL_ICONPOSL = 'Gildenladen Verkauf Symbol Position:'
	L.ESOMRL_ICONPOSLD = 'Verändere die horizontale Position des Beobachten Symboles in der Gildenladen Verkaufsliste.'
	L.ESOMRL_ETRACKING = 'Aktivieren Verfolgung:'
	L.ESOMRL_TRACKDESC = 'Verfolgen Charakter Fortschritt weltweit.'
	L.ESOMRL_TRACKWARN = 'Benötigt / reloadui oder relog wirksam werden.'
	L.ESOMRL_INVENTORYI = 'Aktiviere Symbole im Inventar:'
	L.ESOMRL_INVENTORYD = 'Zeige Symbole im Inventar für beobachtete Rezepte und Zutaten an.'
	L.ESOMRL_IDESTROY = 'Zerstöre Trödel Zutaten:'
	L.ESOMRL_IDESTDESC = 'Ist diese Option aktiviert und der Zutaten zum Trödel verschieben/Zerstören Pin ist an, werden unmarkierte Zutaten zerstört, und nicht zum Trödel verschoben.'
	L.ESOMRL_IDESTROYWARN = 'Alle neu gelooteten Zutaten, welche NICHT beobachtet werden, werden DAUERHAFT zerstört!'
	L.ESOMRL_DEBUGMODE = 'Debugging Modus aktivieren:'
	L.ESOMRL_DEBUGDESC = 'Zeige jedes Mal Informationen im Chat an, wenn Rezeote/Zutaten zum Trödel verschobeen/zerstört werden.'
	L.ESOMRL_STACKSIZE = 'Maximale Anzahl zum Zerstören:'
	L.ESOMRL_STACKDESC = 'Stapel von Zutaten mit einer höheren Anzahl, als hier festgelegt wird, werden weder als Trödel markiert, noch zerstört werden. Andere Einstellungen beeinflussen diese überprüfung hier NICHT!'
	L.ESOMRL_RDESTROY = 'Zerstöre Trödel Rezepte:'
	L.ESOMRL_RDESTDESC = 'Ist diese Option aktiviert und der Rezepte zum Trödel verschieben/Zerstören Pin ist an, werden unmarkierte Rezepte zerstört, und nicht zum Trödel verschoben.'
	L.ESOMRL_RDESTROYWARN = 'Alle NICHT beobachteten Rezepte, deren Qualtität geringer als das hier drunter gesetzte Qualitätslimit ist, werden DAUERHAFT zerstört!'
	L.ESOMRL_DQUALITY = 'Farb-Qualität Schutz:'
	L.ESOMRL_DQUALITYDESC = 'Rezepte, deren Qualität gleich oder höher als der hier gesetzte Wert ist, werdne nicht zum Trödel verschoben oder zerstört werden. Andere Einstellungen beeinflussen diese Qualitäts-überprüfung hier NICHT!'
	L.ESOMRL_GLOBALOPT = 'Account Optionen'
	L.ESOMRL_CHAROPT = 'Charakter Optionen'
	L.ESOMRL_ADDONS = 'Andere Addons'
	L.ESOMRL_ADDONSD = 'Optionen für unterstütze Addons.'
	L.ESOMRL_FCOITEM = 'Verwende FCO ItemSaver für Markierungen:'
	L.ESOMRL_FCOITEMD = 'Ist diese Option aktiviert werden beobachtete Rezepte und Zutaten in deinem Inventar, oder anderen Taschen, mit dem \'Schloß\' Symbol von FCO ItemSaver markiert, anstelle des MRL Symbols. Damit sind diese Gegenstände in FCO ItemSaver dann auch filterbar. Standardmäßig erscheinen die MRL Markierungen weitehrin im Gildenladen!'
	L.ESOMRL_ISTOLEN = 'Ignoriere gestohlene Gegenstände'
	L.ESOMRL_ISTOLEND = 'Ist diese Option aktiviert, und die weiteren Features für die automatische Markierung als Müll bzw. Löschung von unmarkierten Rezepten und Zutaten sind ebenfalls aktiviert, werden dabei gestohlene Gegenstände nicht berücksichtigt.'
	L.ESOMRL_ISTOLENM = 'wurde durch gestohlene Gegenstände Filter ignoriert.'
	L.ESOMRL_IDESTROYM = 'wurde dauerhaft gelöscht.'
	L.ESOMRL_IJUNKM = 'wurde als Abfall markiert.'
L.ESOMRL_TCHART = 'Überwachenden Charakter aktivieren'
L.ESOMRL_TCHARTD = 'Aktivieren Sie unter Optionen zur Rezeptstatus von ausgewählten Charakter zu zeigen.'
	L.ESOMRL_TCHAR = 'Wähle den zu überwachenden Charakter'
	L.ESOMRL_TCHARD = 'Für den ausgewählten Charakter unbekannte Rezepte werden im Inventar mit einem grüner Haken markiert.'
	L.ESOMRL_ALTIU = 'Zeige \'Rezept bekannt\' Symbol.'
	L.ESOMRL_ALTIUD = 'Ist diese Option aktiviert, werden für den ausgewählten Charakter bekannte Rezepte im Inventar mit einem grauen Haken markiert.'
	L.ESOMRL_SSTATS = 'Versorger Station Attribute Symbole'
	L.ESOMRL_SSTATSD = 'Zeigt farbige Symbole je Nahrung & Getränk an, welche Attribute diese beeinflussen.'
	L.ESOMRL_SSTATICONS = 'Attribut Symbol Stil'
	L.ESOMRL_SSTATICONSD = 'Wählen Sie hier den Stil aus, welcher für die dargestellten Attribute Symbole verwendet werden soll.'
	L.ESOMRL_ICONTT1 = 'Sterne'
	L.ESOMRL_ICONTT2 = 'Kreise'
	L.ESOMRL_ALTIS = 'Zeige Bekannte für aktuellen Charakter an?'
	L.ESOMRL_ALTISD = 'Ist diese Option aktiviert, werden graue Symbole für die Rezepte angezeigt, welche der AKTUELLE Charakter bereits kennt (anstelle des oben ausgewählten Charakters). \'Zeige bekannte Rezept Symbole\' muss aktiviert sein, damit diese Option Auswirkungen zeigt.'

-- Info tooltip
	L.ESOMRL_IKNOWN = ' = Bekanntes Rezept.'
	L.ESOMRL_IKNOWNT = ' = Bekanntes Rezept, wird beobachtet.'
	L.ESOMRL_IUNKNOWN = ' = Unbekanntes Rezept.'
	L.ESOMRL_IUNKNOWNT = ' = Unbekanntes Rezept, wird beobachtet.'
	L.ESOMRL_IWRIT = ' = Unbekannt, für Schrieb benötigt.'
	L.ESOMRL_IWTRACK = ' = Unbekannt, für Schrieb benötigt, wird beobachtet.'
	L.ESOMRL_SHIFTCLICK = 'Shift-Klicke Gegenstände, um sie im Chat zu verlinken.'
	L.ESOMRL_INGGOLD = 'Beobachtete Zutaten sind Gold eingefärbt.'

-- Food categories
	L.ESOMRL_FOOD = 'Essen'
	L.ESOMRL_TFOOD = 'Essen Rezepte'

-- Drink categories
	L.ESOMRL_DRINK = 'Getränk'
	L.ESOMRL_TDRINK = 'Getränk Rezepte'

------------------------------------------------------------------------------------------------------------------

if (GetCVar('language.2') == 'de') then -- overwrite GetLanguage for new language
	for k,v in pairs(ESOMRL:GetLanguage()) do
		if (not L[k]) then -- no translation for this string, use default
			L[k] = v
		end
	end

	function ESOMRL:GetLanguage() -- set new language return
		return L
	end
end
