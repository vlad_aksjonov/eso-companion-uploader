local ESOMRL = _G['ESOMRL']
local L = {}

------------------------------------------------------------------------------------------------------------------
-- French (Thanks to ESOUI.com users Khrill and Ayantir for the excellent translations!)
------------------------------------------------------------------------------------------------------------------

-- General strings
	L.ESOMRL_SEARCHBOX = 'Chercher le nom d\'une recette.'
	L.ESOMRL_WRITQUEST = 'Commande de cuisine'
	L.ESOMRL_WRITFINISH = 'Livrez'
	L.ESOMRL_CRAFT = 'Préparez'
	L.ESOMRL_CLEAR = 'Effacer la recherche actuelle.'
	L.ESOMRL_ICLEAR = 'Efface la liste des ingrédients recherchés.'
	L.ESOMRL_TRACKING = 'Surveillance'
	L.ESOMRL_RECIPES = 'recettes.'
	L.ESOMRL_INGREDIENTS = 'ingrédients.'
	L.ESOMRL_CLOSE = 'Fermer la Liste des Recettes'
	L.ESOMRL_SHOWING = 'Afficher la Liste des Ingrédients'
	L.ESOMRL_SHOWRECIPE = 'Afficher la Liste des Recettes'
	L.ESOMRL_LTTSHOW = 'Activer les infobulles'
	L.ESOMRL_LTTHIDE = 'Désactiver les infobulles'
	L.ESOMRL_TTRECIPE = 'Afficher la recette dans les infobulles.'
	L.ESOMRL_TTFOOD = 'Afficher la nourriture dans les infobulles.'
	L.ESOMRL_FINDWRIT = 'Trouver le nécessaire pour la quête d\'artisanat '
	L.ESOMRL_TRACKRING = 'Marquer tous les ingrédients de la recette suivie.'
	L.ESOMRL_TRACKALLING = 'Marquer tous les ingrédients.'
	L.ESOMRL_JUNKUING = 'Cliquez pour mettre automatiquement au rebut les ingrédients non marqués que vous ramassez.'
	L.ESOMRL_DJUNKUING = 'Cliquer pour arrêter de mettre au rebut les ingrédients non marqués que vous ramassez.'
	L.ESOMRL_DESTROYUING = 'Cliquer pour détruire automatiquement (et de façon permanente) les ingrédients non marqués que vous ramassez.'
	L.ESOMRL_DDESTROYUING = 'Cliquer pour arrêter de détruire les ingrédients non marqués que vous ramassez.'
	L.ESOMRL_LEVEL = 'Niveau'
	L.ESOMRL_VETERAN = 'Vétéran'
	L.ESOMRL_ANY = 'Quelconque Niveau'
	L.ESOMRL_H = '(Santé)'
	L.ESOMRL_M = '(Magie)'
	L.ESOMRL_S = '(Vigueur)'
	L.ESOMRL_HM = '(Santé/Magie)'
	L.ESOMRL_HS = '(Santé/Vigueur)'
	L.ESOMRL_MS = '(Magie/Vigueur)'
	L.ESOMRL_HMS = '(Santé/Magie/Vigueur)'
	L.ESOMRL_RTRACK = 'Sur la liste de suivi de recette.'
	L.ESOMRL_ITRACK = 'Sur la liste de suivi de ingrédient.'
	L.ESOMRL_NWRITU = 'INCONNU & NECESSAIRE POUR LA QUETE D\'ARTISANAT'
	L.ESOMRL_NWRITK = 'CONNU & NECESSAIRE POUR LA QUETE D\'ARTISANAT'
	L.ESOMRL_NWRIT = 'NECESSAIRE POUR LA QUETE D\'ARTISANAT'
	L.ESOMRL_MAKE = 'Produit'
	L.ESOMRL_KNOWN = 'CONNU PAR'
	L.ESOMRL_CRAFTABLE = 'FABRIQUE PAR'
	L.ESOMRL_TRACKUNKNOWN = 'Marquer toutes les recettes non connues du personnage sélectionné.'
	L.ESOMRL_TRACKALL = 'Marquer toutes les recettes.'
	L.ESOMRL_JUNKURECIPE = 'Cliquez pour mettre automatiquement au rebut les recettes non marquées que vous ramassez (selon la qualité définie dans les réglages).'
	L.ESOMRL_DJUNKURECIPE = 'Cliquer pour arrêter de mettre au rebut les recettes non marquées que vous ramassez.'
	L.ESOMRL_DESTROYURECIPE = 'Cliquer pour détruire automatiquement (et de façon permanente) les recettes non marquées que vous ramassez (selon la qualité définie dans les réglages).'
	L.ESOMRL_DDESTROYURECIPE = 'Cliquer pour arrêter de détruire les recettes non marquées que vous ramassez.'
	L.ESOMRL_SRCONFIGPANEL = 'Afficher le panneau de configuration du suivi de la recette.'
	L.ESOMRL_HRCONFIGPANEL = 'Masquer le panneau de configuration du suivi de la recette.'
	L.ESOMRL_REMOVECHAR = 'Effacer les données du personnage sélectionné de la base de suivi (se reconnecter avec pour le réintégrer).'
	L.ESOMRL_QUALITY1 = 'Verte'
	L.ESOMRL_QUALITY2 = 'Bleue'
	L.ESOMRL_QUALITY3 = 'Violette'
	L.ESOMRL_SAVEINGTP = 'Sauvegarder la liste des ingrédients suivis dans la base globale.'
	L.ESOMRL_LOADINGTP = 'Charger la base globale des ingrédients suivis.'
L.ESOMRL_FINDRECIPES = 'Trouvez des recettes contenant des ingrédients sélectionnés.'
	L.ESOMRL_INGFOOD = 'INGRÉDIENTS'
	L.ESOMRL_SEARCHS = 'Recherche MRL'
	L.ESOMRL_STRACKY = 'Mettre en valeur les suivis'
	L.ESOMRL_STRACKN = 'Ne pas mettre en valeur les suivis'

-- Settings panel
	L.ESOMRL_SHOWKNOWN = 'Afficher \'connu par\' dans l\'infobulle:'
	L.ESOMRL_SHOWKNOWND = 'Quand il est activé, les sections \'connu par\' et \'fabricable par\' seront affichées dans les infobulles des recettes et nourritures connus des personnages'
	L.ESOMRL_SHOWINGFOOD = 'Afficher les ingrédients sur les plats:'
	L.ESOMRL_SHOWINGFOODD = 'La liste des ingrédients nécessaires apparaîtra sur les produits cuisinés comme il est fait sur les recettes.'
	L.ESOMRL_ICONPOSI = 'Position de l\'icône dans l\'inventaire:'
	L.ESOMRL_ICONPOSID = 'Ajuster la position horizontale de l\'icône de marquage dans l\'inventaire. (Désactivé lorsque \'Utiliser FCO ItemSaver pour les éléments suivis\' est activé dans la section Addons tiers)'
--	L.ESOMRL_ICONPOSB = 'Position de l\'icône de boutique de guilde:'
--	L.ESOMRL_ICONPOSBD = 'Ajuster le positionnement horizontal des icônes de suivi de la boutique de guilde.'

L.ESOMRL_ICONPOSB = 'Guild magasin résultat de recherche icône poste:'
L.ESOMRL_ICONPOSBD = 'Ajustez le positionnement horizontal de la recherche de la banque de guilde entraîner icônes de suivi.'
L.ESOMRL_ICONPOSL = 'Magasin de Guild Liste icône poste:'
L.ESOMRL_ICONPOSLD = 'Ajustez le positionnement horizontal de la boutique de la guilde vente Liste des icônes de suivi.'
	
	L.ESOMRL_ETRACKING = 'Activer le suivi:'
	L.ESOMRL_TRACKDESC = 'Les trophèes du personnage seront suivis pour le compte.'
	L.ESOMRL_TRACKWARN = 'Nécessite un rechargement de l\'interface pour prendre effet.'
	L.ESOMRL_INVENTORYI = 'Activer les icônes dans l\'inventaire:'
	L.ESOMRL_INVENTORYD = 'Afficher les icônes dans votre inventaire pour les recettes et les ingrédients marqués.'
	L.ESOMRL_IDESTROY = 'Détruire les ingrédients mis au rebut:'
	L.ESOMRL_IDESTDESC = 'Quand il est activé, et que les options mise au rebut/destruction sont actives, les ingrédients non marqués sont détruits au lieu d\'être mis au rebut.'
	L.ESOMRL_IDESTROYWARN = 'Tous les nouveaux ingrédients que vous ramassez et qui ne sont pas marqués seront détruits de manière PERMANENTE!'
	L.ESOMRL_DEBUGMODE = 'Activer le mode Debug:'
	L.ESOMRL_DEBUGDESC = 'Affiche un message dans le tchat à chaque fois qu\'une rectte ou un ingrédient est détruit ou mis au rebut.'
	L.ESOMRL_STACKSIZE = 'Taille maximale pour la destruction:'
	L.ESOMRL_STACKDESC = 'Les ingrédients dont la quantité totale est supérieure à ce nombre ne seront pas mis au rebut ou supprimés, quelque soit les autres réglages.'
	L.ESOMRL_RDESTROY = 'Détruire les recettes indésirables:'
	L.ESOMRL_RDESTDESC = 'Lorsqu\'elle est activée et que la mise au rebut/destruction des recettes non marquées sont actives, les recettes non marquées sont détruites au lieu d\'être mises au rebut.'
	L.ESOMRL_RDESTROYWARN = 'Toutes les recettes qui NE sont PAS suivies et qui ont une qualité inférieure à la limite ci-dessous seront détruites de manière PERMANENTE!'
	L.ESOMRL_DQUALITY = 'Protection de la qualité (couleur):'
	L.ESOMRL_DQUALITYDESC = 'Les recettes de ce niveau de qualité ou supérieur, ne seront pas mises au rebut ou détruites (quelque soit les autres réglages).'
	L.ESOMRL_GLOBALOPT = 'Options globales'
	L.ESOMRL_CHAROPT = 'Options pour le personnage'
	L.ESOMRL_ADDONS = 'Addons tiers'
	L.ESOMRL_ADDONSD = 'Options pour les Addons-tiers.'
	L.ESOMRL_FCOITEM = 'Utilisez FCO ItemSaver pour les éléments suivis:'
	L.ESOMRL_FCOITEMD = 'Les recettes et ingrédients seront marqués avec l\'icône \'verrouillé\' de FCO ItemSaver au lieu de la coche MRL. Les marques par défaut de MRL apparaitront toujours néanmoins dans la boutique de guilde.'
	L.ESOMRL_ISTOLEN = 'Ignorer les articles volés'
	L.ESOMRL_ISTOLEND = 'Les objets volés ne seront ni mis aux rebuts, ni détruits.'
	L.ESOMRL_ISTOLENM = 'ignoré par le filtre des articles volés.'
	L.ESOMRL_IDESTROYM = 'a été détruit.'
	L.ESOMRL_IJUNKM = 'a été mis aux rebuts.'
L.ESOMRL_TCHART = 'Activer caractère de suivi'
L.ESOMRL_TCHARTD = 'Activer des options ci-dessous pour afficher l\'état de réception de caractère sélectionné.'
	L.ESOMRL_TCHAR = 'Personnage pour le suivi des recettes'
	L.ESOMRL_TCHARD = 'Recettes qui ne sont PAS connues par le personnage sélectionné seront marquées d\'une marque verte dans l\'inventaire.'
	L.ESOMRL_ALTIU = 'Icône pour les recettes connues'
	L.ESOMRL_ALTIUD = 'Activer cette option affichera une marque grise pour les recettes déjà connues par le personnage sélectionné ci-dessus.'
	L.ESOMRL_SSTATS = 'Icônes au feu de camp'
	L.ESOMRL_SSTATSD = 'Afficher des icônes de différentes couleur au feu de camp pour indiquer quel statistique est augmentée par les recettes.'
	L.ESOMRL_SSTATICONS = 'Style des icônes de stat'
	L.ESOMRL_SSTATICONSD = 'Style des icônes de stat au feu de camp.'
	L.ESOMRL_ICONTT1 = 'Etoiles'
	L.ESOMRL_ICONTT2 = 'Cercles'
	L.ESOMRL_ALTIS = 'Recettes connues du personnage connecté?'
	L.ESOMRL_ALTISD = 'Lorsque cette option est activée, les marqueurs gris seront présents pour les recettes connues du personnage actuellement connecté à la plage de celui sélectionné. L\'option \'Icône pour les recettes connues\' doit être active pour que ce paramètre soit effectif'

-- Info tooltip
	L.ESOMRL_IKNOWN = ' = Recette connue.'
	L.ESOMRL_IKNOWNT = ' = Recette connue, en surveillance.'
	L.ESOMRL_IUNKNOWN = ' = Recette inconnue.'
	L.ESOMRL_IUNKNOWNT = ' = Recette inconnue, en surveillance.'
	L.ESOMRL_IWRIT = ' = Inconnu, nécessaire pour commande.'
	L.ESOMRL_IWTRACK = ' = Inconnu, nécessaire pour commande, en surveillance.'
	L.ESOMRL_SHIFTCLICK = 'Shift+Click pour mettre un lien.'
	L.ESOMRL_INGGOLD = 'Les ingrédients suivis sont colorés en or.'

-- Food categories
	L.ESOMRL_FOOD = 'Nourriture'
	L.ESOMRL_TFOOD = 'Nourriture Recettes'

-- Drink categories
	L.ESOMRL_DRINK = 'Boisson'
	L.ESOMRL_TDRINK = 'Boisson Recettes'

------------------------------------------------------------------------------------------------------------------

if (GetCVar('language.2') == 'fr') then -- overwrite GetLanguage for new language
	for k,v in pairs(ESOMRL:GetLanguage()) do
		if (not L[k]) then -- no translation for this string, use default
			L[k] = v
		end
	end

	function ESOMRL:GetLanguage() -- set new language return
		return L
	end
end
