local ESOMRL = _G['ESOMRL']
local L = {}

------------------------------------------------------------------------------------------------------------------
-- Russian (Thanks to ESOUI.com user KiriX for the excellent translations!)
------------------------------------------------------------------------------------------------------------------

-- General strings
	L.ESOMRL_SEARCHBOX = 'Ïoècê peœeïòa.'
	--L.ESOMRL_WRITQUEST = 'Provisioner Writ'
	--L.ESOMRL_WRITFINISH = 'Deliver Goods'
	L.ESOMRL_CRAFT = 'Peíecìo'
	L.ESOMRL_CLEAR = 'Oòíeîèòö áÿàop.'
	L.ESOMRL_ICLEAR = 'Oòíeîèòö áÿàop.'
	L.ESOMRL_TRACKING = 'Áÿàpaîo'
	L.ESOMRL_RECIPES = 'peœeïòoá.'
	L.ESOMRL_INGREDIENTS = 'èîâpeäèeîòoá.'
	L.ESOMRL_CLOSE = 'Çaêpÿòö cïècoê peœeïòoá'
	L.ESOMRL_SHOWING = 'Ïoêaçaòö cïècoê èîâpeäèeîòoá'
	L.ESOMRL_SHOWRECIPE = 'Ïoêaçaòö cïècoê peœeïòoá'
	L.ESOMRL_LTTSHOW = 'Áêì. ácïìÿáaôûèe ïoäcêaçêè'
	L.ESOMRL_LTTHIDE = 'Oòêì. ácïìÿáaôûèe ïoäcêaçêè'
	L.ESOMRL_TTRECIPE = 'Peœeïòÿ á ïoäcêaçêax'
	L.ESOMRL_TTFOOD = 'Eäa á ïoäcêaçêax'
	L.ESOMRL_FINDWRIT = 'Îaéòè îeoàxoäèíoe äìü çaäaîèü '
	L.ESOMRL_TRACKRING = 'Ïoíeòèòö èîâpeäèeîòÿ oòcìeæèáaeíÿx peœeïòoá.'
	L.ESOMRL_TRACKALLING = 'Ïoíeòèòö áce èîâpeäèeîòÿ.'
	L.ESOMRL_JUNKUING = 'Ûeìêîèòe, ùòoàÿ aáòoíaòèùecêè ïoíeùaòö îeoòcìeæèáaeíÿe èîâpeäèeîòÿ êaê xìaí.'
	L.ESOMRL_DJUNKUING = 'Ûeìêîèòe, ùòoàÿ oòêìôùèòö aáòoíaòèùecêóô ïoíeòêó êaê xìaí.'
	L.ESOMRL_DESTROYUING = 'Ûeìêîèòe, ùòoàÿ aáòoíaòèùecêè óîèùòoæaòö (îaáceâäa) îeoòcìeæèáaeíÿe èîâpeäèeîòÿ.'
	L.ESOMRL_DDESTROYUING = 'Ûeìêîèòe, ùòoàÿ oòêìôùèòö aáòoíaòèùecêoe óîèùòoæeîèe îeoòcìeæèáaeíÿx èîâpeäèeîòoá.'
	L.ESOMRL_LEVEL = 'Ópoáeîö'
	L.ESOMRL_VETERAN = 'Áeòepaî'
	L.ESOMRL_ANY = 'Any Level'
	L.ESOMRL_H = '(Çäopoáöe)'
	L.ESOMRL_M = '(Íaâèü)'
	L.ESOMRL_S = '(Çaïac Cèì)'
	L.ESOMRL_HM = '(Çäopoáöe/Íaâèü)'
	L.ESOMRL_HS = '(Çäopoáöe/Çaïac Cèì)'
	L.ESOMRL_MS = '(Íaâèü/Çaïac Cèì)'
	L.ESOMRL_HMS = '(Çäopoáöe/Íaâèü/Çaïac Cèì)'
	L.ESOMRL_RTRACK = 'OÒCÌEÆÈÁAEÍŸÉ PEŒEÏÒ'
	L.ESOMRL_ITRACK = 'OÒCÌEÆÈÁAEÍŸÉ ÈÎÂPEÄÈEÎÒ'
	L.ESOMRL_NWRITU = 'ÎEÈÇÁECÒÎŸÉ & ÎEOÀXOÄÈÍ ÄÌÜ ÇAÄAÎÈÜ'
	L.ESOMRL_NWRITK = 'ÈÇÁECÒÎŸÉ & ÎEOÀXOÄÈÍ ÄÌÜ ÇAÄAÎÈÜ'
	L.ESOMRL_NWRIT = 'ÎEOÀXOÄÈÍ ÄÌÜ ÇAÄAÎÈÜ'
	L.ESOMRL_MAKE = 'Íoæîo cäeìaòö'
	L.ESOMRL_KNOWN = 'ÈÇÁECÒÎO'
	L.ESOMRL_CRAFTABLE = 'ÍOÆEÒ CÄEÌAÒÖ'
	L.ESOMRL_TRACKUNKNOWN = 'Oòíeòèòö áce îeèçáecòîÿe äìü áÿàpaîîoâo ïepcoîaæa peœeïòÿ.'
	L.ESOMRL_TRACKALL = 'Oòíeòèòö áce peœeïòÿ.'
	L.ESOMRL_JUNKURECIPE = 'Ûeìêîèòe, ùòoàÿ aáòoíaòèùecêè ïoíeùaòö îeoòcìeæèáaeíÿe peœeïòÿ êaê xìaí (çaäaéòe ópoáeîö êaùecòáa á îacòpoéêax).'
	L.ESOMRL_DJUNKURECIPE = 'Ûeìêîèòe, ùòoàÿ oòíeîèòö aáòoíaòèùecêóô ïoíeòêó peœeïòoá êaê xìaí.'
	L.ESOMRL_DESTROYURECIPE = 'Ûeìêîèòe, ùòoàÿ aáòoíaòèùecêè óîèùòoæaòö îeoòcìeæèáaeíÿe peœeïòÿ êaê xìaí (çaäaéòe ópoáeîö êaùecòáa á îacòpoéêax).'
	L.ESOMRL_DDESTROYURECIPE = 'Ûeìêîèòe, ùòoàÿ oòíeîèòö aáòoíaòèùecêoe óîèùòoæeîèe peœeïòoá.'
	L.ESOMRL_SRCONFIGPANEL = 'Ïoêaçaòö ïaîeìö oòcìeæèáaeíÿx peœeïòoá.'
	L.ESOMRL_HRCONFIGPANEL = 'Cêpÿòö ïaîeìö oòcìeæèáaeíÿx peœeïòoá.'
	L.ESOMRL_REMOVECHAR = 'Óàpaòö áÿàpaîîoâo ïepcoîaæa èç àaçÿ oòcìeæèáaeíoâo (çaéäèòe èí cîoáa, ecìè xoòèòe áîoáö äoàaáèòö á àaçó).'
	L.ESOMRL_QUALITY1 = 'Çeìeîÿé'
	L.ESOMRL_QUALITY2 = 'Cèîèé'
	L.ESOMRL_QUALITY3 = 'Ñèoìeòoáÿé'
	L.ESOMRL_SAVEINGTP = 'Coxpaîèòö òeêóûèe oòcìeæèáaeíÿe èîâpeäèeîòÿ á âìoàaìöîóô àaçó ïpoñèìü.'
	L.ESOMRL_LOADINGTP = 'Çapóçèòö oòcìeæèáaeíÿe èîâpeäèeîòÿ èç àaçÿ âìoàaìöîoâo ïpoñèìü.'
L.ESOMRL_FINDRECIPES = 'Find recipes containing selected ingredients.'
	L.ESOMRL_INGFOOD = 'ÈÎÂPEÄÈEÎÒOÁ'
	L.ESOMRL_SEARCHS = 'Ïoècê MRL'
	L.ESOMRL_STRACKY = 'Ïoäcáeòèòö oòcìeæèáaeíÿe'
	L.ESOMRL_STRACKN = 'Îe ïoäcáeùèáaòö oòcìeæèáaeíÿe'

-- Settings panel
	L.ESOMRL_SHOWKNOWN = 'Ïoêaçaòö \'Èçáecòîo\' á ïoäcêaçêax:'
	L.ESOMRL_SHOWKNOWND = 'Ecìè áêìôùeîo, ceêœèè \'Èçáecòîo\' è \'Íoæîo cäeìaòö\' àóäóò ïoêaçaîÿ á ïoäcêaçêax ê peœeïòaí è èîâpeäèeîòaí äìü ácex oòcìeæèáaeíÿx ïepcoîaæeé.'
	L.ESOMRL_SHOWINGFOOD = 'Èîâpeäèeîòÿ îa eäe:'
	L.ESOMRL_SHOWINGFOODD = 'Êoâäa áêìôùeîo, á ïoäcêaçêe ê ïpeäíeòó, üáìüôûeíócü eäoé èìè îaïèòêoí, àóäeò áÿáoäèòöcü cïècoê èîâpeäèeîòoá, îeoàxoäèíÿx äìü eâo ïpèâoòoáìeîèü, êaê á ïoäcêaçêax ê peœeïòó.'
	L.ESOMRL_ICONPOSI = 'Ïoìoæeîèe èêoîêè á èîáeîòape:'
	L.ESOMRL_ICONPOSID = 'Çaäaeò âopèçoîòaìöîoe ïoìoæeîèe èêoîêè oòcìeæèáaeíoâo. (Disabled when \'Use FCO ItemSaver for tracked items\' is enabled in Third-Party Addon Support section.)'
	L.ESOMRL_ICONPOSB = 'Ïoìoæeîèe èêoîêè á âèìöä.íaâaçèîe:'
	L.ESOMRL_ICONPOSBD = 'Çaäaeò âopèçoîòaìöîoe ïoìoæeîèe èêoîêè á oêîe peçóìöòaòoá ïoècêa âèìöä.íaâaçèîa.'
	L.ESOMRL_ICONPOSL = 'Ïoìoæeîèe èêoîêè á âèìöä.íaâaçèîe:'
	L.ESOMRL_ICONPOSLD = 'Çaäaeò âopèçoîòaìöîoe ïoìoæeîèe èêoîêè á oêîe cïècêa ïpoäaæ âèìöä.íaâaçèîa.'
	L.ESOMRL_ETRACKING = 'Áêìôùèòö oòcìeæèáaîèe:'
	L.ESOMRL_TRACKDESC = 'Ïpoâpecc ïepcoîaæa àóäeò oòcìeæèáaòöcü äìü oòoàpaæeîèü ceêœèè \'Èçáecòîo\' á ïoäcêaçêax ê peœeïòaí.'
	L.ESOMRL_TRACKWARN = 'Òpeàóeòcü /reloadui èìè ïepeçaxoä.'
	L.ESOMRL_INVENTORYI = 'Èêoîêè á èîáeîòape:'
	L.ESOMRL_INVENTORYD = 'Ïoêaçÿáaeò èêoîêè á áaúeí èîáeîòape äìü oòcìeæèáaeíÿx peœeïòoá è èîâpeäèeîòoá.'
	L.ESOMRL_IDESTROY = 'Óîèùòoæaòö xìaí-èîâpeäèeîÿ:'
	L.ESOMRL_IDESTDESC = 'Êoâäa áêìôùeîo, èîâpeäèeîòÿ, ïoíeùaeíÿe îa óäaìeîèe è êaê xìaí, àóäóò aáòoíaòèecêè óîèùòoæaòöcü áíecòo ïoíeùaîèe xìaíoí.'
	L.ESOMRL_IDESTROYWARN = 'Ìôàoé îoáÿé ïpeäíeò èç ìóòa, êoòopÿé ÎE oòcìeæèáaeòcü àóäeò cpaçó óîèùòoæeî ÎAÁCEÂÄA!'
	L.ESOMRL_DEBUGMODE = 'Áêìôùèòö debug-peæèí:'
	L.ESOMRL_DEBUGDESC = 'Áÿáoäèò èîñopíaœèô á ùaò, êoâäa peœeïò èìè èîâpeäèeîò óîèùòoæaeòcü èìè ïoíeùaeòcü êaê xìaí.'
	L.ESOMRL_STACKSIZE = 'Íaêc ùècìo óäaìüeíoâo:'
	L.ESOMRL_STACKDESC = 'Còaê ïepeîocèíÿx èîâpeäèeîòoá, á êoòopoí ùècìo ïpeäíeòoá àoìöúe, ùeí óêaçaîîoe çäecö ùècìo îe àóäóò óîèùòoæeîÿ èìè ïoíeùeîÿ êaê íócop, îecíoòpü îa äpóâèe îacòpoéêè.'
	L.ESOMRL_RDESTROY = 'Óîèùòoæaòö xìaí-peœeïòÿ:'
	L.ESOMRL_RDESTDESC = 'Êoâäa áêìôùeîo, peœeïòÿ, ïoíeùaeíÿe îa óäaìeîèe è êaê xìaí, àóäóò aáòoíaòèecêè óîèùòoæaòöcü áíecòo ïoíeùaîèe xìaíoí.'
	L.ESOMRL_RDESTROYWARN = 'Áce ÎE oòcìeæèáaeíÿe peœeïòÿ êaùecòáoí îèæe óêaçaîîoâo á îacòpoéêax àóäóò cpaçó óîèùòoæeîÿ ÎAÁCEÂÄA!'
	L.ESOMRL_DQUALITY = 'Çaûèûaeíoe êaùecòáo:'
	L.ESOMRL_DQUALITYDESC = 'Peœeïòÿ cooòáeòcòáóôûeâo ópoáîü êaùecòáa/œáeòa è áÿúe îe àóäóò ïoíeùeîÿ êaê xìaí èìè óäaìeîÿ, îecíoòpü îa äpóâèe îacòpoéêè.'
	L.ESOMRL_GLOBALOPT = 'Âìoàaìöîÿe îacòpoéêè'
	L.ESOMRL_CHAROPT = 'Îacòpoéêè ïepcoîaæa'
	L.ESOMRL_ADDONS = 'Ïoääepæêa còopoîîèx aääoîoá'
	L.ESOMRL_ADDONSD = 'Oïœèoîaìöîo, äìü ïoääepæêè aääoîoá òpeòöèx ìèœ.'
	L.ESOMRL_FCOITEM = 'Ècïoìöçoáaòö FCO ItemSaver äìü oòcìeæèáaîèü:'
	L.ESOMRL_FCOITEMD = 'Êoâäa oòíeùeîo, oòcìeæèáaeíÿe peœeïòÿ è èîâpeäèeîòÿ á áaúeí èîáeîòape è äpóâèx cóíêax àóäóò íapêèpoáaòöcü èêoîêoé aääoîa FCO ItemSaver \'locked\' áíecòo oòíeòêè MRL è àóäóò çaûèûeîÿ aääoîoí FCO ItemSaver. Còaîäapòîÿe çîaùêè MRL áce eûe àóäóò oòoàpaæaòöcü á âèìöäeécêoí íaâaçèîe.'
	L.ESOMRL_ISTOLEN = 'Èâîopèpoáaòö êpaäeîîÿe áeûè'
	L.ESOMRL_ISTOLEND = 'Êoâäa áêìôùeîo è ècïoìöçóeòcü ñóîêœèü aáòoíaòèùecêoé ïoíeòêè xìaíoí èìè óäaìeîèü peœeïòoá èìè èîâpeäèeîòoá, óêpaäeîîÿe áeûè àóäóò èâîopèpoáaòöcü.'
	L.ESOMRL_ISTOLENM = 'Èâîopèpóeòcü ñèìöòpoí êpaäeîÿx áeûeé.'
	L.ESOMRL_IDESTROYM = 'àÿìo àeçáoçápaòîo óîèùòoæeîo.'
	L.ESOMRL_IJUNKM = 'àÿìo oòíeùeîo êaê xìaí.'
L.ESOMRL_TCHART = 'Enable tracking character'
L.ESOMRL_TCHARTD = 'Enable below options to show recipe status of selected character.'
	L.ESOMRL_TCHAR = 'Áÿàpaòö oòcìeæèáaeíÿx ïepcoîaæeé'
	L.ESOMRL_TCHARD = 'Peœeïòÿ ÎE ÈÇÁECÒÎŸE áÿàpaîîoíó ïepcoîaæó àóäóò oòíeùeîÿ çeìeîoé âaìoùêoé á èîáeîòape.'
	L.ESOMRL_ALTIU = 'Èêoîêa èçáecòîÿx peœeïòoá'
	L.ESOMRL_ALTIUD = 'Áêìôùeîèe ëòoé oïœèè oòoàpaæaeò cepóô âaìoùêó äìü peœeïòoá, êoòopÿe óæe èçáecòîÿ áÿàpaîîÿí áÿúe ïepcoîaæaí.'
	L.ESOMRL_SSTATS = 'Èêoîêè xapaêòepècòèê'
	L.ESOMRL_SSTATSD = 'Ïoêaçÿáaeò œáeòîÿe èêoîêè äìü êaæäoé êaòeâopèè xapaêòepècòèê àaññoá eäÿ è îaïèòêoá á oêîe ïpèâoòoáìeîèü ïpoáèçèè.'
	L.ESOMRL_SSTATICONS = 'Còèìö èêoîoê xapaêòepècòèê'
	L.ESOMRL_SSTATICONSD = 'Còèìö èêoîoê xapaêòepècòèê àaññoê eäÿ è îaïèòêoá, ecìè èêoîêè xapaêòepècòèê áêìôùeîÿ.'
	L.ESOMRL_ICONTT1 = 'Çáeçäÿ'
	L.ESOMRL_ICONTT2 = 'Êpóâè'
	L.ESOMRL_ALTIS = 'Èçáecòîÿe äìü òeêóûeâo?'
	L.ESOMRL_ALTISD = 'Êoâäa áêìôùeîo, cepaü âaìoùêa àóäeò ïoêaçaîa äìü èçáecòîÿx peœeïòoá ÒEÊÓÛEÂO ïepcoîaæa áíecòo òoâo, ùòoàÿ ïoêaçÿáaòö èçáecòîÿe peœeïòÿ áÿàpaîîÿx áÿúe ïepcoîaæeé. \'Èêoîêa èçáecòîÿx peœeïòoá\' äoìæîo àÿòö áêìôùeîo, ùòoàÿ cpaàoòaìa ëòa îacòpoéêa.'

-- Info tooltip
	L.ESOMRL_IKNOWN = ' = Èçáecòîÿé peœeïò.'
	L.ESOMRL_IKNOWNT = ' = Èçáecòîÿé peœeïò, oòcìeæèáaeíÿé.'
	L.ESOMRL_IUNKNOWN = ' = Îeèçáecòîÿé peœeïò.'
	L.ESOMRL_IUNKNOWNT = ' = Îeèçáecòîÿé peœeïò, oòcìeæèáaeíÿé.'
	L.ESOMRL_IWRIT = ' = Îeèçáecòîÿé, îóæîo äìü çaäaîèü.'
	L.ESOMRL_IWTRACK = ' = Îeèçáecòîÿé, îóæîo äìü çaäaîèü, oòcìeæèáaeíÿé.'
	L.ESOMRL_SHIFTCLICK = 'Shift-êìèê äìü oòïpaáêè á ùaò.'
	L.ESOMRL_INGGOLD = 'Oòcìeæèáaeíÿe èâpeäèeîòÿ çoìoòoâo œáeòa.'

-- Food categories
	L.ESOMRL_FOOD = 'Eäa'
	L.ESOMRL_TFOOD = 'Peœeïò eäÿ'
	L.ESOMRL_FOOD1 = 'Íücîÿe àìôäa'
	L.ESOMRL_FOOD2 = 'Ñpóêòoáÿe àìôäa'
	L.ESOMRL_FOOD3 = 'Oáoûîÿe àìôäa'
	L.ESOMRL_FOOD4 = 'Çaêócêè'
	L.ESOMRL_FOOD5 = 'Paâó'
	L.ESOMRL_FOOD6 = 'Äoïoìîèòeìöîÿe'
	L.ESOMRL_FOOD7 = 'Èçÿcêaîîoe'

-- Drink categories
	L.ESOMRL_DRINK = 'Îaïèòêè'
	L.ESOMRL_TDRINK = 'Peœeïòÿ îaïèòêoá'
	L.ESOMRL_DRINK1 = 'Aìêoâoìö'
	L.ESOMRL_DRINK2 = 'Ùaé'
	L.ESOMRL_DRINK3 = 'Òoîèêè'
	L.ESOMRL_DRINK4 = 'Ìèêepÿ'
	L.ESOMRL_DRINK5 = 'Îacòoéêè'
	L.ESOMRL_DRINK6 = 'Êpeïêèé ùaé'
	L.ESOMRL_DRINK7 = 'Äècòèììüòÿ'

------------------------------------------------------------------------------------------------------------------

if (GetCVar('language.2') == 'ru') then -- overwrite GetLanguage for new language
	for k,v in pairs(ESOMRL:GetLanguage()) do
		if (not L[k]) then -- no translation for this string, use default
			L[k] = v
		end
	end

	function ESOMRL:GetLanguage() -- set new language return
		return L
	end
end
