-------------------------------------------------------------------------------
-- ESO Master Recipe List v1.42a
-------------------------------------------------------------------------------
--
-- Copyright (c) 2015 James A. Keene (Phinix) All rights reserved.
--
-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation (the "Software"),
-- to operate the Software for personal use only. Permission is NOT granted
-- to modify, merge, publish, distribute, sublicense, re-upload, and/or sell
-- copies of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
-- OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
-- NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
-- HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
-- WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
-- FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
-- OTHER DEALINGS IN THE SOFTWARE.
--
-------------------------------------------------------------------------------
--
-- DISCLAIMER:
--
-- This Add-on is not created by, affiliated with or sponsored by ZeniMax
-- Media Inc. or its affiliates. The Elder Scrolls® and related logos are
-- registered trademarks or trademarks of ZeniMax Media Inc. in the United
-- States and/or other countries. All rights reserved.
--
-- You can read the full terms at:
-- https://account.elderscrollsonline.com/add-on-terms
--
-------------------------------------------------------------------------------
local ESOMRL = _G['ESOMRL']
local L = ESOMRL:GetLanguage()
ESOMRL.Name = 'MasterRecipeList'
ESOMRL.Author = '|c66ccffPhinix|r'
ESOMRL.Version = '1.42a'

local setPage = 1
local editText = 0
local searchSize = 0
local recountTracked = 0
local PlayerLoaded = 0
local stationNode = 0
local stationItem = 0
local stationNav = 0
local stnorecs = 0
local textInput = ""
local searchTable = {}
local qualityCap = {}
local nameList = {}
local openTab
local currentChar
local charnameDropdown
local stationControl
local stationSelection
local RecipeItemTooltipControl
local IngredientItemTooltipControl

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Used for third-party addon support.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
local FCOItemSaverActive

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Database setup.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
local IconStrings = {[1]='|t16:16:/MasterRecipeList/textures/known.dds|t ',[2]='|t16:16:/MasterRecipeList/textures/knownt.dds|t ',[3]='|t16:16:/MasterRecipeList/textures/unknown.dds|t ',[4]='|t16:16:/MasterRecipeList/textures/unknownt.dds|t ',[5]='|t16:16:/MasterRecipeList/textures/writ.dds|t ',[6]='|t16:16:/MasterRecipeList/textures/writt.dds|t '}
local StationIcons = {[1] = '  |t16:16:/MasterRecipeList/textures/stat_hstar.dds|t',[2] = '  |t16:16:/MasterRecipeList/textures/stat_mstar.dds|t',[3] = '  |t16:16:/MasterRecipeList/textures/stat_sstar.dds|t',[4] = '  |t16:16:/MasterRecipeList/textures/stat_hstar.dds|t |t16:16:/MasterRecipeList/textures/stat_mstar.dds|t',[5] = '  |t16:16:/MasterRecipeList/textures/stat_hstar.dds|t |t16:16:/MasterRecipeList/textures/stat_sstar.dds|t',[6] = '  |t16:16:/MasterRecipeList/textures/stat_mstar.dds|t |t16:16:/MasterRecipeList/textures/stat_sstar.dds|t',[7] = '  |t16:16:/MasterRecipeList/textures/stat_hstar.dds|t |t16:16:/MasterRecipeList/textures/stat_mstar.dds|t |t16:16:/MasterRecipeList/textures/stat_sstar.dds|t',[8] = ' |t16:16:/MasterRecipeList/textures/tracking.dds|t',[9] = ' |t16:16:/MasterRecipeList/textures/trackings.dds|t'}
local StatIcons = {[1] = '  |t16:16:/MasterRecipeList/textures/stat_hstar.dds|t',[2] = '  |t16:16:/MasterRecipeList/textures/stat_mstar.dds|t',[3] = '  |t16:16:/MasterRecipeList/textures/stat_sstar.dds|t',[4] = '  |t16:16:/MasterRecipeList/textures/stat_hstar.dds|t |t16:16:/MasterRecipeList/textures/stat_mstar.dds|t',[5] = '  |t16:16:/MasterRecipeList/textures/stat_hstar.dds|t |t16:16:/MasterRecipeList/textures/stat_sstar.dds|t',[6] = '  |t16:16:/MasterRecipeList/textures/stat_mstar.dds|t |t16:16:/MasterRecipeList/textures/stat_sstar.dds|t',[7] = '  |t16:16:/MasterRecipeList/textures/stat_hstar.dds|t |t16:16:/MasterRecipeList/textures/stat_mstar.dds|t |t16:16:/MasterRecipeList/textures/stat_sstar.dds|t',[8] = '  |t16:16:/MasterRecipeList/textures/stat_h.dds|t',[9] = '  |t16:16:/MasterRecipeList/textures/stat_m.dds|t',[10] = '  |t16:16:/MasterRecipeList/textures/stat_s.dds|t',[11] = '  |t16:16:/MasterRecipeList/textures/stat_h.dds|t |t16:16:/MasterRecipeList/textures/stat_m.dds|t',[12] = '  |t16:16:/MasterRecipeList/textures/stat_h.dds|t |t16:16:/MasterRecipeList/textures/stat_s.dds|t',[13] = '  |t16:16:/MasterRecipeList/textures/stat_m.dds|t |t16:16:/MasterRecipeList/textures/stat_s.dds|t',[14] = '  |t16:16:/MasterRecipeList/textures/stat_h.dds|t |t16:16:/MasterRecipeList/textures/stat_m.dds|t |t16:16:/MasterRecipeList/textures/stat_s.dds|t'}
local StatOptions = {[1] = '/MasterRecipeList/textures/opt_stars.dds',[2] = '/MasterRecipeList/textures/opt_circles.dds'}
local StatOptionTooltips = {[1] = L.ESOMRL_ICONTT1,[2] = L.ESOMRL_ICONTT2}
local WritQuest = {reqId1 = 0, reqId2 = 0}
local WritIng = {}
local StationChecking = {[1] = {tr = ESOMRL.ProvisioningLinksFood1, cv = 1},[2] = {tr = ESOMRL.ProvisioningLinksFood2, cv = 1},[3] = {tr = ESOMRL.ProvisioningLinksFood3, cv = 1},[4] = {tr = ESOMRL.ProvisioningLinksFood4, cv = 2},[5] = {tr = ESOMRL.ProvisioningLinksFood5, cv = 2},[6] = {tr = ESOMRL.ProvisioningLinksFood6, cv = 2},[7] = {tr = ESOMRL.ProvisioningLinksFood7, cv = 3},[8] = {tr = ESOMRL.ProvisioningLinksDrinks1, cv = 1},[9] = {tr = ESOMRL.ProvisioningLinksDrinks2, cv = 1},[10] = {tr = ESOMRL.ProvisioningLinksDrinks3, cv = 1},[11] = {tr = ESOMRL.ProvisioningLinksDrinks4, cv = 2},[12] = {tr = ESOMRL.ProvisioningLinksDrinks5, cv = 2},[13] = {tr = ESOMRL.ProvisioningLinksDrinks6, cv = 2},[14] = {tr = ESOMRL.ProvisioningLinksDrinks7, cv = 3},[15] = {tr = ESOMRL.ProvisioningLinksPsijic, cv = 4},[16] = {tr = ESOMRL.ProvisioningLinksOrsinium, cv = 4}}
local StationControls = {[1] = {c = nil, w = 0, t = 0},[2] = {c = nil, w = 0, t = 0},[3] = {c = nil, w = 0, t = 0},[4] = {c = nil, w = 0, t = 0},[5] = {c = nil, w = 0, t = 0},[6] = {c = nil, w = 0, t = 0},[7] = {c = nil, w = 0, t = 0},[8] = {c = nil, w = 0, t = 0},[9] = {c = nil, w = 0, t = 0},[10] = {c = nil, w = 0, t = 0},[11] = {c = nil, w = 0, t = 0},[12] = {c = nil, w = 0, t = 0},[13] = {c = nil, w = 0, t = 0},[14] = {c = nil, w = 0, t = 0},[14] = {c = nil, w = 0, t = 0},[15] = {c = nil, w = 0, t = 0},[16] = {c = nil, w = 0, t = 0}}
local CharacterDefaults = {[1]=0,[2]=0,[3]=0,[4]=0,[5]=0,[6]=0,[7]=0,[8]=0,[9]=0,[10]=0,[11]=0,[12]=0,[13]=0,[14]=0,[15]=0,[16]=0,[17]=0,[18]=0,[19]=0,[20]=0,[21]=0,[22]=0,[23]=0,[24]=0,[25]=0,[26]=0,[27]=0,[28]=0,[29]=0,[30]=0,[31]=0,[32]=0,[33]=0,[34]=0,[35]=0,[36]=0,[37]=0,[38]=0,[39]=0,[40]=0,[41]=0,[42]=0,[43]=0,[44]=0,[45]=0,[46]=0,[47]=0,[48]=0,[49]=0,[50]=0,[51]=0,[52]=0,[53]=0,[54]=0,[55]=0,[56]=0,[57]=0,[58]=0,[59]=0,[60]=0,[61]=0,[62]=0,[63]=0,[64]=0,[65]=0,[66]=0,[67]=0,[68]=0,[69]=0,[70]=0,[71]=0,[72]=0,[73]=0,[74]=0,[75]=0,[76]=0,[77]=0,[78]=0,[79]=0,[80]=0,[81]=0,[82]=0,[83]=0,[84]=0,[85]=0,[86]=0,[87]=0,[88]=0,[89]=0,[90]=0,[91]=0,[92]=0,[93]=0,[94]=0,[95]=0,[96]=0,[97]=0,[98]=0,[99]=0,[100]=0,[101]=0,[102]=0,[103]=0,[104]=0,[105]=0,[106]=0,[107]=0,[108]=0,[109]=0,[110]=0,[111]=0,[112]=0,[113]=0,[114]=0,[115]=0,[116]=0,[117]=0,[118]=0,[119]=0,[120]=0,[121]=0,[122]=0,[123]=0,[124]=0,[125]=0,[126]=0,[127]=0,[128]=0,[129]=0,[130]=0,[131]=0,[132]=0,[133]=0,[134]=0,[135]=0,[136]=0,[137]=0,[138]=0,[139]=0,[140]=0,[141]=0,[142]=0,[143]=0,[144]=0,[145]=0,[146]=0,[147]=0,[148]=0,[149]=0,[150]=0,[151]=0,[152]=0,[153]=0,[154]=0,[155]=0,[156]=0,[157]=0,[158]=0,[159]=0,[160]=0,[161]=0,[162]=0,[163]=0,[164]=0,[165]=0,[166]=0,[167]=0,[168]=0,[169]=0,[170]=0,[171]=0,[172]=0,[173]=0,[174]=0,[175]=0,[176]=0,[177]=0,[178]=0,[179]=0,[180]=0,[181]=0,[182]=0,[183]=0,[184]=0,[185]=0,[186]=0,[187]=0,[188]=0,[189]=0,[190]=0,[191]=0,[192]=0,[193]=0,[194]=0,[195]=0,[196]=0,[197]=0,[198]=0,[199]=0,[200]=0,[201]=0,[202]=0,[203]=0,[204]=0,[205]=0,[206]=0,[207]=0,[208]=0,[209]=0,[210]=0,[211]=0,[212]=0,[213]=0,[214]=0,[215]=0,[216]=0,[217]=0,[218]=0,[219]=0,[220]=0,[221]=0,[222]=0,[223]=0,[224]=0,[225]=0,[226]=0,[227]=0,[228]=0,[229]=0,[230]=0,[231]=0,[232]=0,[233]=0,[234]=0,[235]=0,[236]=0,[237]=0,[238]=0,[239]=0,[240]=0,[241]=0,[242]=0,[243]=0,[244]=0,[245]=0,[246]=0,[247]=0,[248]=0,[249]=0,[250]=0,[251]=0,[252]=0,[253]=0,[254]=0,[255]=0,[256]=0,[257]=0,[258]=0,[259]=0,[260]=0,[261]=0,[262]=0,[263]=0,[264]=0,[265]=0,[266]=0,[267]=0,[268]=0,[269]=0,[270]=0,[271]=0,[272]=0,[273]=0,[274]=0,[275]=0,[276]=0,[277]=0,[278]=0,[279]=0,[280]=0,[281]=0,[282]=0,[283]=0,[284]=0,[285]=0,[286]=0,[287]=0,[288]=0,[289]=0,[290]=0,[291]=0,[292]=0,[293]=0,[294]=0,[295]=0,[296]=0,[297]=0,[298]=0,[299]=0,[300]=0,[301]=0,[302]=0,[303]=0,[304]=0,[305]=0,[306]=0,[307]=0,[308]=0,[309]=0,[310]=0,[311]=0,[312]=0,[313]=0,[314]=0,[315]=0,[316]=0,[317]=0,[318]=0,[319]=0,[320]=0,[321]=0,[322]=0,[323]=0,[324]=0,[325]=0,[326]=0,[327]=0,[328]=0,[329]=0,[330]=0,[331]=0,[332]=0,[333]=0,[334]=0,[335]=0,[336]=0,[337]=0,[338]=0,[339]=0,[340]=0,[341]=0,[342]=0,[343]=0,[344]=0,[345]=0,[346]=0,[347]=0,[348]=0,[349]=0,[350]=0,[351]=0,[352]=0,[353]=0,[354]=0,[355]=0,[356]=0,[357]=0,[358]=0,[359]=0,[360]=0,[361]=0,[362]=0,[363]=0,[364]=0,[365]=0,[366]=0,[367]=0,[368]=0,[369]=0,[370]=0,[371]=0,[372]=0,[373]=0,[374]=0,[375]=0,[376]=0,[377]=0,[378]=0,[379]=0,[380]=0,[381]=0,[382]=0,[383]=0,[384]=0,[385]=0,[386]=0,[387]=0,[388]=0,[389]=0,[390]=0,[391]=0,[392]=0,[393]=0,[394]=0,[395]=0,[396]=0,[397]=0,[398]=0,[399]=0,[400]=0,[401]=0,[402]=0,[403]=0,[404]=0,[405]=0,[406]=0,[407]=0,[408]=0,[409]=0,[410]=0,[411]=0,[412]=0,[413]=0,[414]=0,[415]=0,[416]=0,[417]=0,[418]=0,[419]=0,[420]=0,[421]=0,[422]=0,[423]=0,[424]=0,[425]=0,[426]=0,[427]=0,[428]=0,[429]=0,[430]=0,[431]=0,[432]=0,[433]=0,[434]=0,[435]=0,[436]=0,[437]=0,[438]=0,[439]=0,[440]=0,[441]=0,[442]=0,[443]=0,[444]=0,[445]=0,[446]=0,[447]=0,[448]=0,[449]=0,[450]=0,[451]=0,[452]=0,[453]=0,[454]=0,[455]=0,[456]=0,[457]=0,[458]=0,[459]=0,[460]=0,[461]=0,[462]=0,[463]=0,[464]=0,[465]=0,[466]=0,[467]=0,[468]=0,[469]=0,[470]=0,[471]=0,[472]=0,[473]=0,[474]=0,[475]=0,[476]=0,[477]=0,[478]=0,[479]=0,[480]=0,[481]=0,[482]=0,[483]=0,[484]=0,[485]=0,[486]=0,[487]=0,[488]=0,[489]=0,[490]=0,[491]=0,[492]=0,[493]=0,[494]=0,[495]=0,[496]=0,[497]=0,[498]=0,[499]=0,[500]=0,[501]=0,[502]=0,[503]=0,[504]=0,[505]=0,[506]=0,[507]=0,[508]=0,[509]=0,[510]=0,[511]=0,[512]=0,[513]=0,[514]=0,[515]=0,[516]=0,[517]=0,[518]=0,[519]=0,[520]=0,[521]=0,[522]=0,[523]=0,[524]=0,[525]=0,[526]=0,[527]=0,[528]=0,[529]=0,[530]=0,[531]=0,[532]=0,[533]=0,[534]=0,[535]=0,[601]=0,[602]=0,[603]=0,[604]=0,[605]=0,[606]=0,[607]=0,[608]=0,[609]=0,[610]=0,[611]=0,[612]=0,[613]=0,[614]=0,[615]=0,[616]=0,[617]=0,[618]=0,[619]=0,[620]=0,[621]=0,[622]=0,[623]=0,[624]=0,[625]=0,[626]=0,[627]=0,[628]=0,[629]=0,[630]=0,[631]=0,[632]=0,[633]=0,[634]=0,[635]=0,[636]=0,[637]=0,[638]=0,[639]=0,[640]=0,[641]=0,[642]=0,[643]=0,[644]=0,[645]=0,[646]=0,[647]=0,[648]=0,[649]=0,[650]=0,[800]={xpos=0,ypos=0,recipetracklist=0,ingtracklist=0,trackChar=true,destroyjunkrecipes=false,destroyjunkingredients=false,debugmode=true,ignorestolen=true,fcoitemsaver=false,altknown=false,tchart=false,altknownself=false,trackingchar=1,lttshow=1,sttshow=1,stmarked=1,tooltipstyle=0,recipeconfigpanel=0,junkunmarkedrecipes=0,junkunmarkedingredients=0,destroyunmarkedrecipes=0,destroyunmarkedingredients=0,maxjunkstack=10,maxjunkquality=2}}
local AccountDefaults = {[1]={names={}},[2]={names={}},[3]={names={}},[4]={names={}},[5]={names={}},[6]={names={}},[7]={names={}},[8]={names={}},[9]={names={}},[10]={names={}},[11]={names={}},[12]={names={}},[13]={names={}},[14]={names={}},[15]={names={}},[16]={names={}},[17]={names={}},[18]={names={}},[19]={names={}},[20]={names={}},[21]={names={}},[22]={names={}},[23]={names={}},[24]={names={}},[25]={names={}},[26]={names={}},[27]={names={}},[28]={names={}},[29]={names={}},[30]={names={}},[31]={names={}},[32]={names={}},[33]={names={}},[34]={names={}},[35]={names={}},[36]={names={}},[37]={names={}},[38]={names={}},[39]={names={}},[40]={names={}},[41]={names={}},[42]={names={}},[43]={names={}},[44]={names={}},[45]={names={}},[46]={names={}},[47]={names={}},[48]={names={}},[49]={names={}},[50]={names={}},[51]={names={}},[52]={names={}},[53]={names={}},[54]={names={}},[55]={names={}},[56]={names={}},[57]={names={}},[58]={names={}},[59]={names={}},[60]={names={}},[61]={names={}},[62]={names={}},[63]={names={}},[64]={names={}},[65]={names={}},[66]={names={}},[67]={names={}},[68]={names={}},[69]={names={}},[70]={names={}},[71]={names={}},[72]={names={}},[73]={names={}},[74]={names={}},[75]={names={}},[76]={names={}},[77]={names={}},[78]={names={}},[79]={names={}},[80]={names={}},[81]={names={}},[82]={names={}},[83]={names={}},[84]={names={}},[85]={names={}},[86]={names={}},[87]={names={}},[88]={names={}},[89]={names={}},[90]={names={}},[91]={names={}},[92]={names={}},[93]={names={}},[94]={names={}},[95]={names={}},[96]={names={}},[97]={names={}},[98]={names={}},[99]={names={}},[100]={names={}},[101]={names={}},[102]={names={}},[103]={names={}},[104]={names={}},[105]={names={}},[106]={names={}},[107]={names={}},[108]={names={}},[109]={names={}},[110]={names={}},[111]={names={}},[112]={names={}},[113]={names={}},[114]={names={}},[115]={names={}},[116]={names={}},[117]={names={}},[118]={names={}},[119]={names={}},[120]={names={}},[121]={names={}},[122]={names={}},[123]={names={}},[124]={names={}},[125]={names={}},[126]={names={}},[127]={names={}},[128]={names={}},[129]={names={}},[130]={names={}},[131]={names={}},[132]={names={}},[133]={names={}},[134]={names={}},[135]={names={}},[136]={names={}},[137]={names={}},[138]={names={}},[139]={names={}},[140]={names={}},[141]={names={}},[142]={names={}},[143]={names={}},[144]={names={}},[145]={names={}},[146]={names={}},[147]={names={}},[148]={names={}},[149]={names={}},[150]={names={}},[151]={names={}},[152]={names={}},[153]={names={}},[154]={names={}},[155]={names={}},[156]={names={}},[157]={names={}},[158]={names={}},[159]={names={}},[160]={names={}},[161]={names={}},[162]={names={}},[163]={names={}},[164]={names={}},[165]={names={}},[166]={names={}},[167]={names={}},[168]={names={}},[169]={names={}},[170]={names={}},[171]={names={}},[172]={names={}},[173]={names={}},[174]={names={}},[175]={names={}},[176]={names={}},[177]={names={}},[178]={names={}},[179]={names={}},[180]={names={}},[181]={names={}},[182]={names={}},[183]={names={}},[184]={names={}},[185]={names={}},[186]={names={}},[187]={names={}},[188]={names={}},[189]={names={}},[190]={names={}},[191]={names={}},[192]={names={}},[193]={names={}},[194]={names={}},[195]={names={}},[196]={names={}},[197]={names={}},[198]={names={}},[199]={names={}},[200]={names={}},[201]={names={}},[202]={names={}},[203]={names={}},[204]={names={}},[205]={names={}},[206]={names={}},[207]={names={}},[208]={names={}},[209]={names={}},[210]={names={}},[211]={names={}},[212]={names={}},[213]={names={}},[214]={names={}},[215]={names={}},[216]={names={}},[217]={names={}},[218]={names={}},[219]={names={}},[220]={names={}},[221]={names={}},[222]={names={}},[223]={names={}},[224]={names={}},[225]={names={}},[226]={names={}},[227]={names={}},[228]={names={}},[229]={names={}},[230]={names={}},[231]={names={}},[232]={names={}},[233]={names={}},[234]={names={}},[235]={names={}},[236]={names={}},[237]={names={}},[238]={names={}},[239]={names={}},[240]={names={}},[241]={names={}},[242]={names={}},[243]={names={}},[244]={names={}},[245]={names={}},[246]={names={}},[247]={names={}},[248]={names={}},[249]={names={}},[250]={names={}},[251]={names={}},[252]={names={}},[253]={names={}},[254]={names={}},[255]={names={}},[256]={names={}},[257]={names={}},[258]={names={}},[259]={names={}},[260]={names={}},[261]={names={}},[262]={names={}},[263]={names={}},[264]={names={}},[265]={names={}},[266]={names={}},[267]={names={}},[268]={names={}},[269]={names={}},[270]={names={}},[271]={names={}},[272]={names={}},[273]={names={}},[274]={names={}},[275]={names={}},[276]={names={}},[277]={names={}},[278]={names={}},[279]={names={}},[280]={names={}},[281]={names={}},[282]={names={}},[283]={names={}},[284]={names={}},[285]={names={}},[286]={names={}},[287]={names={}},[288]={names={}},[289]={names={}},[290]={names={}},[291]={names={}},[292]={names={}},[293]={names={}},[294]={names={}},[295]={names={}},[296]={names={}},[297]={names={}},[298]={names={}},[299]={names={}},[300]={names={}},[301]={names={}},[302]={names={}},[303]={names={}},[304]={names={}},[305]={names={}},[306]={names={}},[307]={names={}},[308]={names={}},[309]={names={}},[310]={names={}},[311]={names={}},[312]={names={}},[313]={names={}},[314]={names={}},[315]={names={}},[316]={names={}},[317]={names={}},[318]={names={}},[319]={names={}},[320]={names={}},[321]={names={}},[322]={names={}},[323]={names={}},[324]={names={}},[325]={names={}},[326]={names={}},[327]={names={}},[328]={names={}},[329]={names={}},[330]={names={}},[331]={names={}},[332]={names={}},[333]={names={}},[334]={names={}},[335]={names={}},[336]={names={}},[337]={names={}},[338]={names={}},[339]={names={}},[340]={names={}},[341]={names={}},[342]={names={}},[343]={names={}},[344]={names={}},[345]={names={}},[346]={names={}},[347]={names={}},[348]={names={}},[349]={names={}},[350]={names={}},[351]={names={}},[352]={names={}},[353]={names={}},[354]={names={}},[355]={names={}},[356]={names={}},[357]={names={}},[358]={names={}},[359]={names={}},[360]={names={}},[361]={names={}},[362]={names={}},[363]={names={}},[364]={names={}},[365]={names={}},[366]={names={}},[367]={names={}},[368]={names={}},[369]={names={}},[370]={names={}},[371]={names={}},[372]={names={}},[373]={names={}},[374]={names={}},[375]={names={}},[376]={names={}},[377]={names={}},[378]={names={}},[379]={names={}},[380]={names={}},[381]={names={}},[382]={names={}},[383]={names={}},[384]={names={}},[385]={names={}},[386]={names={}},[387]={names={}},[388]={names={}},[389]={names={}},[390]={names={}},[391]={names={}},[392]={names={}},[393]={names={}},[394]={names={}},[395]={names={}},[396]={names={}},[397]={names={}},[398]={names={}},[399]={names={}},[400]={names={}},[401]={names={}},[402]={names={}},[403]={names={}},[404]={names={}},[405]={names={}},[406]={names={}},[407]={names={}},[408]={names={}},[409]={names={}},[410]={names={}},[411]={names={}},[412]={names={}},[413]={names={}},[414]={names={}},[415]={names={}},[416]={names={}},[417]={names={}},[418]={names={}},[419]={names={}},[420]={names={}},[421]={names={}},[422]={names={}},[423]={names={}},[424]={names={}},[425]={names={}},[426]={names={}},[427]={names={}},[428]={names={}},[429]={names={}},[430]={names={}},[431]={names={}},[432]={names={}},[433]={names={}},[434]={names={}},[435]={names={}},[436]={names={}},[437]={names={}},[438]={names={}},[439]={names={}},[440]={names={}},[441]={names={}},[442]={names={}},[443]={names={}},[444]={names={}},[445]={names={}},[446]={names={}},[447]={names={}},[448]={names={}},[449]={names={}},[450]={names={}},[451]={names={}},[452]={names={}},[453]={names={}},[454]={names={}},[455]={names={}},[456]={names={}},[457]={names={}},[458]={names={}},[459]={names={}},[460]={names={}},[461]={names={}},[462]={names={}},[463]={names={}},[464]={names={}},[465]={names={}},[466]={names={}},[467]={names={}},[468]={names={}},[469]={names={}},[470]={names={}},[471]={names={}},[472]={names={}},[473]={names={}},[474]={names={}},[475]={names={}},[476]={names={}},[477]={names={}},[478]={names={}},[479]={names={}},[480]={names={}},[481]={names={}},[482]={names={}},[483]={names={}},[484]={names={}},[485]={names={}},[486]={names={}},[487]={names={}},[488]={names={}},[489]={names={}},[490]={names={}},[491]={names={}},[492]={names={}},[493]={names={}},[494]={names={}},[495]={names={}},[496]={names={}},[497]={names={}},[498]={names={}},[499]={names={}},[500]={names={}},[501]={names={}},[502]={names={}},[503]={names={}},[504]={names={}},[505]={names={}},[506]={names={}},[507]={names={}},[508]={names={}},[509]={names={}},[510]={names={}},[511]={names={}},[512]={names={}},[513]={names={}},[514]={names={}},[515]={names={}},[516]={names={}},[517]={names={}},[518]={names={}},[519]={names={}},[520]={names={}},[521]={names={}},[522]={names={}},[523]={names={}},[524]={names={}},[525]={names={}},[526]={names={}},[527]={names={}},[528]={names={}},[529]={names={}},[530]={names={}},[531]={names={}},[532]={names={}},[533]={names={}},[534]={names={}},[535]={names={}},[800]={known=true,ingfood=true,inventoryicons=true,stationstats=true,stationicons=1,bagiconoffset=25,gstoreiconoffset=-30,glistingiconoffset=-30},[801]={globalingprofile={[1]=0,[2]=0,[3]=0,[4]=0,[5]=0,[6]=0,[7]=0,[8]=0,[9]=0,[10]=0,[11]=0,[12]=0,[13]=0,[14]=0,[15]=0,[16]=0,[17]=0,[18]=0,[19]=0,[20]=0,[21]=0,[22]=0,[23]=0,[24]=0,[25]=0,[26]=0,[27]=0,[28]=0,[29]=0,[30]=0,[31]=0,[32]=0,[33]=0,[34]=0,[35]=0,[36]=0,[37]=0,[38]=0,[39]=0,[40]=0,[41]=0,[42]=0,[43]=0,[44]=0,[45]=0,[46]=0,[47]=0,[48]=0,[49]=0,[50]=0}}}

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Get and set saved variables.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function TierIndex(index, tier)
	if tier == 2 then
		index = index + 42
	elseif tier == 3 then
		index = index + 84
	elseif tier == 4 then
		index = index + 126
	elseif tier == 5 then
		index = index + 162
	elseif tier == 6 then
		index = index + 198
	elseif tier == 7 then
		index = index + 234
	elseif tier == 8 then
		index = index + 265
	elseif tier == 9 then
		index = index + 307
	elseif tier == 10 then
		index = index + 349
	elseif tier == 11 then
		index = index + 391
	elseif tier == 12 then
		index = index + 427
	elseif tier == 13 then
		index = index + 463
	elseif tier == 14 then
		index = index + 499
	elseif tier == 15 then
		index = 531
	elseif tier == 16 then
		index = index + 531
	end
	return index
end

local function GetRecipeSavedVar(index, tier)
	index = TierIndex(index, tier)
	return ESOMRL.SV[index]
end

local function SetRecipeSavedVar(index, tier, val)
	index = TierIndex(index, tier)
	ESOMRL.SV[index] = val
end

local function GetIngredientSavedVar(index)
	index = index + 600
	return ESOMRL.SV[index]
end

local function SetIngredientSavedVar(index, val)
	index = index + 600
	ESOMRL.SV[index] = val
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Various utility functions used in other areas.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function SetTrackedRecipeCountText() -- Updates the current tracked recipe display text.
	ESOMRL_MainFrameSelectionFrameClearSearchLabel:SetText(L.ESOMRL_CLEAR .. ' |cff9900(' .. L.ESOMRL_TRACKING .. '|r |cffffff' .. ESOMRL.SV[800].recipetracklist .. '|r |cff9900' .. L.ESOMRL_RECIPES .. ')|r')
	if stationControl ~= nil then stationControl:RefreshRecipeList() end
end

local function SetTrackedIngredientCountText() -- Updates the current tracked ingredient display text.
	ESOMRL_MainFrameIngredientsFrameClearIngTrackLabel:SetText(L.ESOMRL_ICLEAR .. ' |cff9900(' .. L.ESOMRL_TRACKING .. '|r |cffffff' .. ESOMRL.SV[800].ingtracklist .. '|r |cff9900' .. L.ESOMRL_INGREDIENTS .. ')|r')
end

local function GetItemIdFromLink(itemLink) -- Parse the link data for base itemId.
	local itemId = select(4, ZO_LinkHandler_ParseLink(itemLink))
	return tonumber(itemId)
end

local function RefreshViews() -- Refresh current views when tracking status updates so icons re-populate in real-time.
	ZO_ScrollList_RefreshVisible(ZO_PlayerInventoryBackpack)
	ZO_ScrollList_RefreshVisible(ZO_PlayerBankBackpack)
	ZO_ScrollList_RefreshVisible(ZO_GuildBankBackpack)
	ZO_ScrollList_RefreshVisible(ZO_TradingHouseItemPaneSearchResults)
	ZO_ScrollList_RefreshVisible(ZO_TradingHousePostedItemsList)
end

local function LinkNameLanguageFormat(langtext)
	return zo_strformat("<<t:1>>",langtext)
end

local function LowerSpaceSpecial(text) -- Remove spaces and change to lower case.
	if GetCVar('Language.2') == 'en' then
		return tostring(text):gsub("%s",''):gsub(":.*",''):gsub("%W",''):lower()
	else
		return zo_strlower(zo_strformat("<<t:1>>",text)):gsub("%s",''):gsub(":.*",''):gsub("%W",'')
	end
end

local function Contains(table, element)	-- Determined if a given element exists in a given table.
	for k,v in pairs(table) do
		if v == element then
			return true
		end
	end
	return false
end

local function FindName(table, element)	-- Returns the table key that contains a given element value.
	for k,v in pairs(table) do	-- Used for determining which key has the name to remove with table.remove.
		if v == element then
			return k
		end
	end
	return 0
end

local function LookupPosition(htable, tval) -- Lookup the itemId's position from a given resource table (fast).
	if htable[tval] ~= nil then
		return tonumber(htable[tval].position)
	end
	return 0
end

local function FindMatch(htable, tval) -- Search for an itemId and get the position if found (slower).
	for k, v in pairs(htable) do
		if htable[k].id == tostring(tval) then
			return tonumber(htable[k].position)
		end
	end
	return 0
end

local function GetTextureId(texturePath)
	for k, v in pairs(StatOptions) do
		if	v == texturePath then
			return k
		end
	end
	return 0
end

local function SwitchStationIcons(textureId)
	if textureId == 1 then
		for i = 1, 7 do StationIcons[i] = StatIcons[i] end
	elseif textureId == 2 then
		for i = 1, 7 do local b = i + 7 StationIcons[i] = StatIcons[b] end
	end
end

local function RecountTracked() -- Recount tracked recipes/ingredients once when you log in.
	if ShowMotifs then -- Offset default position if ShowMotifs enabled to match new columns.
		if ESOMRL.ASV[800].bagiconoffset == 25 then
			ESOMRL.ASV[800].bagiconoffset = 82
		end
		if ESOMRL.ASV[800].gstoreiconoffset == -30 then
			ESOMRL.ASV[800].gstoreiconoffset = 27
		end
		if ESOMRL.ASV[800].glistingiconoffset == -30 then
			ESOMRL.ASV[800].glistingiconoffset = 27
		end
		RefreshViews()
	elseif not ShowMotifs then
		if ESOMRL.ASV[800].bagiconoffset == 82 then
			ESOMRL.ASV[800].bagiconoffset = 25
		end
		if ESOMRL.ASV[800].gstoreiconoffset == 27 then
			ESOMRL.ASV[800].gstoreiconoffset = -30
		end
		if ESOMRL.ASV[800].glistingiconoffset == 27 then
			ESOMRL.ASV[800].glistingiconoffset = -30
		end
		RefreshViews()
	end
	ESOMRL.SV[800].recipetracklist = 0
	ESOMRL.SV[800].ingtracklist = 0
	local trackedrecipes = 0
	local trackedingredients = 0
	local playername = GetUnitName("player")
	for r = 1, 535 do
		local savedvar = GetRecipeSavedVar(r, 1)
		if savedvar == 2 or savedvar == 1 then
			trackedrecipes = trackedrecipes + 1
		end
	end
	for i = 1, 50 do
		local savedvar = GetIngredientSavedVar(i)
		if savedvar == 1 then
			trackedingredients = trackedingredients + 1
		end
	end
	ESOMRL.SV[800].recipetracklist = trackedrecipes
	ESOMRL.SV[800].ingtracklist = trackedingredients
	SetTrackedRecipeCountText()
	SetTrackedIngredientCountText()
	recountTracked = 1
end

local function InitKnown() -- Re-scans for known recipes any time you learn a recipe or open the recipe book.
	local playername = GetUnitName("player")
	for i = 1, 535 do
		local savedvar = GetRecipeSavedVar(i, 1)
		if IsItemLinkRecipeKnown(ESOMRL.ProvisioningLinks[i].link) == true then
			if (savedvar == 2) or (savedvar == 1) then
				SetRecipeSavedVar(i, 1, 2)
			else
				SetRecipeSavedVar(i, 1, 3)
			end
			if ESOMRL.SV[800].trackChar == true then
				if not Contains(ESOMRL.ASV[i].names, playername) then
					table.insert(ESOMRL.ASV[i].names, #ESOMRL.ASV[i].names + 1, playername)
				end
			else
				if Contains(ESOMRL.ASV[i].names, playername) then
					table.remove(ESOMRL.ASV[i].names, FindName(ESOMRL.ASV[i].names, playername))
				end
			end
		else
			if savedvar == 2 then
				SetRecipeSavedVar(i, 1, 1)
			elseif savedvar == 3 then
				SetRecipeSavedVar(i, 1, 0)
			end
		end
	end
	RefreshViews()
	if recountTracked == 0 then
		RecountTracked()
	end
end

local function NoRecipes() -- Checks if any recipes are known when accessing the cooking station.
	for i = 1, 535 do
		if IsItemLinkRecipeKnown(ESOMRL.ProvisioningLinks[i].link) == true then
			return false
		end
	end
	return true
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Functions to show and hide the recipe list, adjust for laguage settings, and remember position.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function RestorePosition() -- Restores previous window position when opened.
	local left = ESOMRL.SV[800].xpos
	local top = ESOMRL.SV[800].ypos
	ESOMRL_MainFrame:ClearAnchors()
	ESOMRL_MainFrame:SetAnchor(TOPLEFT, GuiRoot, TOPLEFT, left, top)
end

local function FormatTooltipText(text) -- Formats tooltip data by language.
	if GetCVar('Language.2') == 'en' then
		return text
	else
		text = text:gsub('Any Level',L.ESOMRL_ANY)
		text = text:gsub('Makes',L.ESOMRL_MAKE)
		text = text:gsub('Level',L.ESOMRL_LEVEL)
		text = text:gsub('Veteran',L.ESOMRL_VETERAN)
		text = text:gsub('Food',L.ESOMRL_FOOD)
		text = text:gsub('Drink',L.ESOMRL_DRINK)
		text = text:gsub('Psijic Ambrosia',zo_strformat("<<t:1>>",GetItemLinkName(GetItemLinkRecipeResultItemLink("|H1:item:64223:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h"))))
		return text
	end
end

local function CloseMain(control, option)
	if option == 1 then
		InitializeTooltip(InformationTooltip, control, TOPLEFT, 8, -44, BOTTOMRIGHT)
		SetTooltipText(InformationTooltip, L.ESOMRL_CLOSE)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		SCENE_MANAGER:ToggleTopLevel(ESOMRL_MainFrame)
	end
end

local function StationButtonTooltip(control, option)
	if option == 1 then
		InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, 0, -11, TOPRIGHT)
		SetTooltipText(InformationTooltip, "Master Recipe List")
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	end
end

local function InfoTooltip(option) -- Show the legend.
	if setPage == 1 then
		if option == 1 then
			ESOMRL_MainFrameInfoBox:SetHidden(false)
		elseif option == 2 then
			ESOMRL_MainFrameInfoBox:SetHidden(true)
		end
	elseif setPage == 0 then
		if option == 1 then
			ESOMRL_MainFrameInfoBoxIng:SetHidden(false)
		elseif option == 2 then
			ESOMRL_MainFrameInfoBoxIng:SetHidden(true)
		end
	end
end
		
local function ToggleTooltips(control, option) -- Handles turning list item tooltips on and off.
	if option == 1 then
		InitializeTooltip(InformationTooltip, control, TOPLEFT, 34, -39, BOTTOMRIGHT)
		if ESOMRL.SV[800].lttshow == 1 then
			SetTooltipText(InformationTooltip, L.ESOMRL_LTTHIDE)
		else
			SetTooltipText(InformationTooltip, L.ESOMRL_LTTSHOW)
		end
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		if ESOMRL.SV[800].lttshow == 1 then
			ESOMRL.SV[800].lttshow = 0
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, control, TOPLEFT, 34, -39, BOTTOMRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_LTTSHOW)
		else
			ESOMRL.SV[800].lttshow = 1
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, control, TOPLEFT, 34, -39, BOTTOMRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_LTTHIDE)
		end
	end
end

local function IngredientPanelSetup() -- Configure the ingredients display when the panel is shown.
	for i = 112, 161 do
		local control = ESOMRL_MainFrameIngredientsFrame:GetChild(i)
		local index = i - 111
		if GetIngredientSavedVar(index) == 1 then
			control:SetText(LinkNameLanguageFormat(GetItemLinkName(ESOMRL.IngredientLinks[index].link)))
			control:SetColor(1,1,0,1)
			control:SetAlpha(1)
		elseif GetIngredientSavedVar(index) == 0 then
			control:SetText(LinkNameLanguageFormat(GetItemLinkName(ESOMRL.IngredientLinks[index].link)))
			control:SetColor(0.7647,0.7647,0.7647,1)
			control:SetAlpha(1)
		end
	end
	if ESOMRL.SV[800].destroyjunkingredients == true then
		if ESOMRL.SV[800].destroyunmarkedingredients == 0 then
			ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton1:SetHidden(false)
			ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton2:SetHidden(true)
		elseif ESOMRL.SV[800].destroyunmarkedingredients == 1 then
			ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton1:SetHidden(true)
			ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton2:SetHidden(false)
		end
	elseif ESOMRL.SV[800].destroyjunkingredients == false then
		if ESOMRL.SV[800].junkunmarkedingredients == 0 then
			ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton1:SetHidden(false)
			ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton2:SetHidden(true)
		elseif ESOMRL.SV[800].junkunmarkedingredients == 1 then
			ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton1:SetHidden(true)
			ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton2:SetHidden(false)
		end
	end
end

local function RecipePanelSetup() -- Configure the recipe display when the panel is shown.
	if ESOMRL.SV[800].destroyjunkrecipes == true then
		if ESOMRL.SV[800].destroyunmarkedrecipes == 0 then
			ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton1:SetHidden(false)
			ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton2:SetHidden(true)
		elseif ESOMRL.SV[800].destroyunmarkedrecipes == 1 then
			ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton1:SetHidden(true)
			ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton2:SetHidden(false)
		end
	elseif ESOMRL.SV[800].destroyjunkrecipes == false then
		if ESOMRL.SV[800].junkunmarkedrecipes == 0 then
			ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton1:SetHidden(false)
			ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton2:SetHidden(true)
		elseif ESOMRL.SV[800].junkunmarkedrecipes == 1 then
			ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton1:SetHidden(true)
			ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton2:SetHidden(false)
		end
	end
end

local function RecipeOptionPanel(option) -- Handles turning recipe config panel on and off.
	if option == 1 then
		InitializeTooltip(InformationTooltip, ESOMRL_MainFrameListFrame, BOTTOMLEFT, 26, 28, BOTTOMRIGHT)
		if ESOMRL.SV[800].recipeconfigpanel == 0 then
			SetTooltipText(InformationTooltip, L.ESOMRL_SRCONFIGPANEL)
		elseif ESOMRL.SV[800].recipeconfigpanel == 1 then
			SetTooltipText(InformationTooltip, L.ESOMRL_HRCONFIGPANEL)
		end
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		if ESOMRL.SV[800].recipeconfigpanel == 0 then
			ESOMRL.SV[800].recipeconfigpanel = 1
			ESOMRL_MainFrameListFrameBatchTracking:SetHidden(false)
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, ESOMRL_MainFrameListFrame, BOTTOMLEFT, 26, 28, BOTTOMRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_HRCONFIGPANEL)
		elseif ESOMRL.SV[800].recipeconfigpanel == 1 then
			ESOMRL.SV[800].recipeconfigpanel = 0
			ESOMRL_MainFrameListFrameBatchTracking:SetHidden(true)
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, ESOMRL_MainFrameListFrame, BOTTOMLEFT, 26, 28, BOTTOMRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_SRCONFIGPANEL)
		end
		RecipePanelSetup()
	end
end

local function RecipePanelRestore() -- Restore previous status of recipe config panel on load.
	if ESOMRL.SV[800].recipeconfigpanel == 0 then
		ESOMRL_MainFrameListFrameBatchTracking:SetHidden(true)
	elseif ESOMRL.SV[800].recipeconfigpanel == 1 then
		ESOMRL_MainFrameListFrameBatchTracking:SetHidden(false)
	end
	RecipePanelSetup()
end

local function OnCharacterSelected(_, charName, choice) -- Dropbox callback.
	currentChar = charName
end

local function SetupCharacterDropbox() -- Initialize dropbox selection of all tracked characters who know any recipes.
	currentChar = GetUnitName("player")
	table.insert(nameList, 1, currentChar)
	for i = 1, 535 do
		if next(ESOMRL.ASV[i].names) ~= nil then
			for n = 1, #ESOMRL.ASV[i].names do
				local name = ESOMRL.ASV[i].names[n]
				if not Contains(nameList, name) then
					table.insert(nameList, #nameList + 1, name)
				end
			end
		end
	end
	charnameDropdown = WINDOW_MANAGER:CreateControlFromVirtual('CharacterDropdownList', ESOMRL_MainFrameListFrameBatchTracking, 'ZO_StatsDropdownRow')
	charnameDropdown:SetWidth(300)
	charnameDropdown:SetAnchor(TOPLEFT, ESOMRL_MainFrameListFrameBatchTracking, TOPLEFT, -4, 2)
	charnameDropdown:GetNamedChild('Dropdown'):SetWidth(295)
	charnameDropdown.dropdown:SetSelectedItem(currentChar)
	for k,v in pairs(nameList) do
		local entry = charnameDropdown.dropdown:CreateItemEntry(v, OnCharacterSelected)
		charnameDropdown.dropdown:AddItem(entry)
	end
end

local function RemoveCharacter(option) -- Handles removing selected character from the tracking database.
	if option == 1 then
		InitializeTooltip(InformationTooltip, ESOMRL_MainFrameCloseButton, TOPLEFT, 8, -44, BOTTOMRIGHT)
		SetTooltipText(InformationTooltip, L.ESOMRL_REMOVECHAR)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		if currentChar ~= GetUnitName("player") then
			table.remove(nameList, FindName(nameList, currentChar)) -- Remove the selected character from the current name table.
			for i = 1, 535 do -- Remove the selected character from the global tracking database.
				if Contains(ESOMRL.ASV[i].names, currentChar) then
					table.remove(ESOMRL.ASV[i].names, FindName(ESOMRL.ASV[i].names, currentChar))
				end
			end
			currentChar = GetUnitName("player") -- Set the addon-global active name to the current character.
			charnameDropdown.dropdown:ClearItems() -- Clear the list of names in the dropdown menu so we can rebuild.
			for k,v in pairs(nameList) do -- Rebuild using the names table we just removed a character from.
				local entry = charnameDropdown.dropdown:CreateItemEntry(v, OnCharacterSelected)
				charnameDropdown.dropdown:AddItem(entry)
			end
			charnameDropdown.dropdown:SetSelectedItem(currentChar) -- Set the dropdown selected item to the current character.
		end
	end
end

local function SwitchPage(control, option) -- Handles switching between recipe and ingredient pages.
	if option == 1 then
		if setPage == 1 then
			InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, -53, -4, TOPRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_SHOWING)
		elseif setPage == 0 then
			InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, -53, -4, TOPRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_SHOWRECIPE)
		end
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		if setPage == 1 then
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, -53, -4, TOPRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_SHOWRECIPE)
			ESOMRL_MainFrameSelectionFrame:SetHidden(true)
			ESOMRL_MainFrameListFrame:SetHidden(true)
			ESOMRL_MainFrameIngredientsFrame:SetHidden(false)
			SetTrackedIngredientCountText()
			IngredientPanelSetup()
			setPage = 0
		elseif setPage == 0 then
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, -53, -4, TOPRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_SHOWING)
			ESOMRL_MainFrameSelectionFrame:SetHidden(false)
			ESOMRL_MainFrameListFrame:SetHidden(false)
			ESOMRL_MainFrameIngredientsFrame:SetHidden(true)
			setPage = 1
		end
	end
end

local function WritButton(control, option) -- Button tooltip for writ lookups.
	if option == 1 then
		InitializeTooltip(InformationTooltip, control, TOPLEFT, 0, 0, BOTTOMRIGHT)
		SetTooltipText(InformationTooltip, L.ESOMRL_FINDWRIT)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	end
end

local function OnMoveStop() -- Saves the current window position when closed.
	ESOMRL.SV[800].xpos = ESOMRL_MainFrame:GetLeft()
	ESOMRL.SV[800].ypos = ESOMRL_MainFrame:GetTop()
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Functions related to the quest-event-based tracking of writ requirements.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function CheckForWritMatch(conditionText)
	local function SetWritIngredients(sm)
		local ingredients = {}
		local linkcheck = ESOMRL.ProvisioningLinks[sm].link
		local numIngredients = GetItemLinkRecipeNumIngredients(linkcheck)
		for i = 1, 50 do
			local namelookup = LowerSpaceSpecial(GetItemLinkName(ESOMRL.IngredientLinks[i].link))
			table.insert(ingredients, #ingredients + 1, namelookup)
		end
		for n = 1, numIngredients do
			local ingredientName,_ = GetItemLinkRecipeIngredientInfo(linkcheck, n)
			ingredientName = LowerSpaceSpecial(ingredientName)
			local ingID = tonumber(ESOMRL.IngredientLinks[tonumber(FindName(ingredients, ingredientName))].id)
			if not Contains(WritIng, ingID) then
				table.insert(WritIng, #WritIng + 1, ingID)
			end
		end
	end
	local mTable = {}
	local wWords = {}
	for w in conditionText:gmatch("%S+") do table.insert(wWords, #wWords + 1, LowerSpaceSpecial(w)) end
	local text = LowerSpaceSpecial(conditionText)
	local mCount = 0
	for i = 1, 535 do
		local search = LowerSpaceSpecial(GetItemLinkName(ESOMRL.ProvisioningLinks[i].item))
		local name = zo_strlower(zo_strformat("<<t:1>>",GetItemLinkName(ESOMRL.ProvisioningLinks[i].item)))
		if (string.find(text,search) ~= nil) then
			table.insert(mTable, #mTable + 1, {search=name, pos=i}) mCount = mCount + 1
		end
	end
	if mCount > 0 then
		if mCount > 1 then
			local smCount = 0
			local sm = 0
			for i = 1, mCount do
				local tsmCount = 0
				local sWords = {}
				local sString = mTable[i].search
				for w in sString:gmatch("%S+") do table.insert(sWords, #sWords + 1, LowerSpaceSpecial(w)) end
				for m = 1, #sWords do
					for w = 1, #wWords do
						if sWords[m] == wWords[w] then tsmCount = tsmCount + 1 end
					end
					if tsmCount > smCount then
						smCount = tsmCount
						sm = mTable[i].pos
					end
				end
			end
			SetWritIngredients(sm)
			return GetItemIdFromLink(ESOMRL.ProvisioningLinks[sm].item)
		elseif mCount == 1 then
			SetWritIngredients(mTable[1].pos)
			return GetItemIdFromLink(ESOMRL.ProvisioningLinks[mTable[1].pos].item)
		end
	else
		return 0
	end
end

local function GetWritID()
	for i = 1, MAX_JOURNAL_QUESTS do
		if IsValidQuestIndex(i) then
			local questType = GetJournalQuestType(i)
			if questType == 4 then
				local QuestName = GetJournalQuestName(i)
				if QuestName == L.ESOMRL_WRITQUEST then
					return i
				end
			end
		end
	end
	return 0
end

function StationWritCheck()
	local writIndex = GetWritID()
	local questTable = {}
	WritQuest.reqId1 = 0
	WritQuest.reqId2 = 0
	WritIng = {}
	local function CheckInfo(q,r)
		local a, b, _, _, _, _ = GetJournalQuestConditionInfo(q,1,r)
		return a, b
	end
	if writIndex == 0 then
		WritQuest.reqId1 = 0
		WritQuest.reqId2 = 0
		return
	end
	for i = 1, GetJournalQuestNumConditions(writIndex) do
		local conditionText, current = CheckInfo(writIndex,i)
		table.insert(questTable, #questTable + 1, {conditionText=conditionText, current=current})
	end
	for r = 1, #questTable do
		if string.find(questTable[r].conditionText, L.ESOMRL_WRITFINISH) then
			WritQuest.reqId1 = 0
			WritQuest.reqId2 = 0
			return
		end
		if questTable[r].conditionText ~= "" and questTable[r].current == 0 then
			if WritQuest.reqId1 == 0 then
				WritQuest.reqId1 = CheckForWritMatch(questTable[r].conditionText)
			elseif WritQuest.reqId2 == 0 then
				WritQuest.reqId2 = CheckForWritMatch(questTable[r].conditionText)
			end
		end
	end
end

local function OnQuestAdded(eventCode, journalIndex, questName, objectiveName)
	if questName == L.ESOMRL_WRITQUEST then
		StationWritCheck()
	end
end

local function OnQuestConditionCounterChanged(eventCode, journalIndex, questName, conditionText, conditionType, currConditionVal, newConditionVal, conditionMax, isFailCondition, stepOverrideText, isPushed, isComplete, isConditionComplete, isStepHidden)
	if questName == L.ESOMRL_WRITQUEST then
		local a, b, _, _, _, _ = GetJournalQuestConditionInfo(journalIndex,1,1)
		local c, d, _, _, _, _ = GetJournalQuestConditionInfo(journalIndex,1,2)
		local e, f, _, _, _, _ = GetJournalQuestConditionInfo(journalIndex,1,2)
		if string.find(a, "L.ESOMRL_WRITFINISH") or string.find(c, "L.ESOMRL_WRITFINISH") or string.find(e, "L.ESOMRL_WRITFINISH") then
			WritQuest.reqId1 = 0
			WritQuest.reqId2 = 0
			return
		elseif a == "" and c == "" then
			WritQuest.reqId1 = 0
			WritQuest.reqId2 = 0
			return
		else
			StationWritCheck()
		end
	end
end

local function OnQuestComplete(eventCode, questName, level, previousExperience, currentExperience, rank, previousPoints, currentPoints)
	if questName == L.ESOMRL_WRITQUEST then
		WritQuest.reqId1 = 0 WritQuest.reqId2 = 0 return
	end
end

local function OnQuestRemoved(eventCode, isCompleted, journalIndex, questName, zoneIndex, poiIndex)
	if questName == L.ESOMRL_WRITQUEST then
		WritQuest.reqId1 = 0 WritQuest.reqId2 = 0 return
	end
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Set up hooks into various tooltips and add appropriate text.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function AddRecipeTooltipLine(control, tooltipLine, itemId, itemLink, option) -- Modifies tooltip based on various options.
	local pos = 0
	if option == 1 then
		pos = LookupPosition(ESOMRL.RecipeMaterials, itemId)
	elseif option == 0 then
		pos = LookupPosition(ESOMRL.ResultMaterials, itemId)
	end
	if option == 1 then
		control:AddVerticalPadding(20)
		control:AddLine(FormatTooltipText(tooltipLine), 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
	end
	if pos ~= 0 then

		if ESOMRL.ASV[800].ingfood == true then -- Show ingredients in food item tooltips (if enabled in global options).
			if option == 0 then
				local inglink = ESOMRL.ProvisioningLinks[tonumber(ESOMRL.ResultMaterials[itemId].position)].link
				local ingnumber = GetItemLinkRecipeNumIngredients(inglink)
				local ingstring = ''
				for i = 1, ingnumber do
					local ing, num = GetItemLinkRecipeIngredientInfo(inglink, i)
					if i == ingnumber then
						if num > 0 then
							ingstring = ingstring .. '|cffffff' .. tostring(LinkNameLanguageFormat(ing)) .. '|r'
						else
							ingstring = ingstring .. '|c656565' .. tostring(LinkNameLanguageFormat(ing)) .. '|r'
						end
					else
						if num > 0 then
							ingstring = ingstring .. '|cffffff' .. tostring(LinkNameLanguageFormat(ing)) .. '|r' .. ', '
						else
							ingstring = ingstring .. '|c656565' .. tostring(LinkNameLanguageFormat(ing)) .. '|r' .. ', '
						end
					end
				end
				control:AddVerticalPadding(20)
				control:AddLine(L.ESOMRL_INGFOOD, 'ZoFontHeader', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
				control:AddVerticalPadding(-10)
				control:AddLine(ingstring, 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
			end
		end

		if ESOMRL.ASV[800].known == true then -- Show list of characters who know/are able to craft the given recipe or food item in tooltips.
			local namestring
			local cstring
			if next(ESOMRL.ASV[pos].names) ~= nil then
				control:AddVerticalPadding(10)
				ZO_Tooltip_AddDivider(control)
				control:AddVerticalPadding(10)
				if option == 1 then
					control:AddLine(L.ESOMRL_KNOWN, 'ZoFontHeader', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
				elseif option == 0 then
					control:AddLine(L.ESOMRL_CRAFTABLE, 'ZoFontHeader', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
				end
				local count = #ESOMRL.ASV[pos].names
				for i = 1, count do
					local name = ESOMRL.ASV[pos].names[i]
					if i == 1 then
						if count == i then
							control:AddVerticalPadding(-10)
							control:AddLine('|c66ccff' .. name .. '|r', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
						else
							if #name > 35 then
								control:AddVerticalPadding(-10)
								control:AddLine('|c66ccff' .. name .. '|r' .. ', ', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
							else
								namestring = '|c66ccff' .. name .. '|r' .. ', '
								cstring = name .. ', '
							end
						end
					else
						if count == i then
							local tempa = namestring .. '|c66ccff' .. name .. '|r'
							local tempb = cstring .. name
							if #tempb > 45 then
								control:AddVerticalPadding(-10)
								control:AddLine(namestring, 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
								control:AddVerticalPadding(-10)
								control:AddLine('|c66ccff' .. name .. '|r', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
							else
								control:AddVerticalPadding(-10)
								control:AddLine(tempa, 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
							end
						else
							if #name > 35 then
								control:AddVerticalPadding(-10)
								control:AddLine('|c66ccff' .. name .. '|r' .. ', ', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
							else
								local tempa = namestring .. '|c66ccff' .. name .. '|r' .. ', '
								local tempb = cstring .. name .. ','
								if #tempb > 45 then
									control:AddVerticalPadding(-10)
									control:AddLine(namestring, 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, LEFT, false)
									namestring = '|c66ccff' .. name .. '|r' .. ', '
									cstring = name .. ', '
								else
									namestring = tempa
									cstring = tempb
								end
							end
						end
					end
				end
			end
		end
	end

	if option == 1 then -- Show if selected recipe/food item is needed for current writ (if any).
		if IsItemLinkRecipeKnown(itemLink) == false then
			local saved = GetRecipeSavedVar(pos, 0)
			if saved == 1 then
				control:AddVerticalPadding(20)
				control:AddLine('|cfee854' .. L.ESOMRL_RTRACK .. '|r', 'ZoFontHeader2', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
				local resLink = ESOMRL.RecipeMaterials[itemId].item
				if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
					control:AddLine('|cffff00' .. L.ESOMRL_NWRITU .. '|r', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
				end
			elseif saved == 0 then
				local resLink = ESOMRL.RecipeMaterials[itemId].item
				if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
					control:AddVerticalPadding(20)
					control:AddLine('|cffff00' .. L.ESOMRL_NWRITU .. '|r', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
				end
			end
		elseif IsItemLinkRecipeKnown(itemLink) == true then
			local pos = tonumber(ESOMRL.RecipeMaterials[itemId].position)
			local saved = GetRecipeSavedVar(pos, 0)
			if saved == 2 then
				control:AddVerticalPadding(20)
				control:AddLine('|cfee854' .. L.ESOMRL_RTRACK .. '|r', 'ZoFontHeader2', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
				local resLink = ESOMRL.RecipeMaterials[itemId].item
				if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
					control:AddLine('|cffff00' .. L.ESOMRL_NWRITK .. '|r', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
				end
			elseif saved == 3 then
				local resLink = ESOMRL.RecipeMaterials[itemId].item
				if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
					control:AddVerticalPadding(20)
					control:AddLine('|cffff00' .. L.ESOMRL_NWRITK .. '|r', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
				end
			end
		end
	elseif option == 0 then
		if pos ~= 0 then
			local saved = GetRecipeSavedVar(pos, 0)
			if saved == 1 or saved == 2 then
				local resLink = ESOMRL.ProvisioningLinks[tonumber(ESOMRL.ResultMaterials[itemId].position)].item
				if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
					control:AddVerticalPadding(20)
					control:AddLine('|cffff00' .. L.ESOMRL_NWRIT .. '|r', 'ZoFontHeader2', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
				end
			elseif saved == 0 or saved == 3 then
				local resLink = ESOMRL.ProvisioningLinks[tonumber(ESOMRL.ResultMaterials[itemId].position)].item
				if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
					control:AddVerticalPadding(20)
					control:AddLine('|cffff00' .. L.ESOMRL_NWRIT .. '|r', 'ZoFontHeader2', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
				end
			end
		end
	end
end

local function AddIngredientTooltipLine(control, itemId) -- Modifies ingredient tooltip with tracked status.
	local pos = LookupPosition(ESOMRL.IngredientMaterials, itemId)
	if pos ~= 0 then
		if GetIngredientSavedVar(pos) == 1 then
			control:AddVerticalPadding(30)
			control:AddLine('|cfee854' .. L.ESOMRL_ITRACK .. '|r', 'ZoFontHeader2', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
			if Contains(WritIng, itemId) then
				control:AddLine('|cffff00' .. L.ESOMRL_NWRIT .. '|r', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
			end
		else
			if Contains(WritIng, itemId) then
				control:AddVerticalPadding(30)
				control:AddLine('|cffff00' .. L.ESOMRL_NWRIT .. '|r', 'ZoFontGame', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
			end
		end
	end
end

local function HookTooltips()
	local ESOMRLSetBagItemTooltip = ItemTooltip.SetBagItem
	ItemTooltip.SetBagItem = function(control, bagId, slotIndex)
		local itemLink = GetItemLink(bagId, slotIndex)
		local itemId = GetItemIdFromLink(itemLink)
		ESOMRLSetBagItemTooltip(control, bagId, slotIndex)
		if ESOMRL.RecipeMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, ESOMRL.RecipeMaterials[itemId].tooltip, itemId, itemLink, 1)
		elseif ESOMRL.IngredientMaterials[itemId] ~= nil then
			AddIngredientTooltipLine(control, itemId)
		elseif ESOMRL.ResultMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, nil, itemId, itemLink, 0)
		end
	end
	local ESOMRLSetLootItemTooltip = ItemTooltip.SetLootItem
	ItemTooltip.SetLootItem = function(control, lootId)
		local itemLink = GetLootItemLink(lootId)
		local itemId = GetItemIdFromLink(itemLink)
		ESOMRLSetLootItemTooltip(control, lootId)
		if ESOMRL.RecipeMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, ESOMRL.RecipeMaterials[itemId].tooltip, itemId, itemLink, 1)
		elseif ESOMRL.IngredientMaterials[itemId] ~= nil then
			AddIngredientTooltipLine(control, itemId)
		elseif ESOMRL.ResultMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, nil, itemId, itemLink, 0)
		end
	end
	local ESOMRLSetTradingHouseItemTooltip = ItemTooltip.SetTradingHouseItem
	ItemTooltip.SetTradingHouseItem = function(control, tradingHouseIndex)
		local itemLink = GetTradingHouseSearchResultItemLink(tradingHouseIndex)
		local itemId = GetItemIdFromLink(itemLink)
		ESOMRLSetTradingHouseItemTooltip(control, tradingHouseIndex)
		if ESOMRL.RecipeMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, ESOMRL.RecipeMaterials[itemId].tooltip, itemId, itemLink, 1)
		elseif ESOMRL.IngredientMaterials[itemId] ~= nil then
			AddIngredientTooltipLine(control, itemId)
		elseif ESOMRL.ResultMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, nil, itemId, itemLink, 0)
		end
	end
	local ESOMRLSetTradingHouseListingTooltip = ItemTooltip.SetTradingHouseListing
	ItemTooltip.SetTradingHouseListing = function(control, tradingHouseListingIndex)
		local itemLink = GetTradingHouseListingItemLink(tradingHouseListingIndex)
		local itemId = GetItemIdFromLink(itemLink)
		ESOMRLSetTradingHouseListingTooltip(control, tradingHouseListingIndex)
		if ESOMRL.RecipeMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, ESOMRL.RecipeMaterials[itemId].tooltip, itemId, itemLink, 1)
		elseif ESOMRL.IngredientMaterials[itemId] ~= nil then
			AddIngredientTooltipLine(control, itemId)
		elseif ESOMRL.ResultMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, nil, itemId, itemLink, 0)
		end
	end
	local ESOMRLSetAttachedMailItemTooltip = ItemTooltip.SetAttachedMailItem
	ItemTooltip.SetAttachedMailItem = function(control, mailId, attachmentIndex)
		local itemLink = GetAttachedItemLink(mailId, attachmentIndex)
		local itemId = GetItemIdFromLink(itemLink)
		ESOMRLSetAttachedMailItemTooltip(control, mailId, attachmentIndex)
		if ESOMRL.RecipeMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, ESOMRL.RecipeMaterials[itemId].tooltip, itemId, itemLink, 1)
		elseif ESOMRL.IngredientMaterials[itemId] ~= nil then
			AddIngredientTooltipLine(control, itemId)
		elseif ESOMRL.ResultMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, nil, itemId, itemLink, 0)
		end
	end
	local ESOMRLSetBuybackTooltip = ItemTooltip.SetBuybackItem
	ItemTooltip.SetBuybackItem = function(control, slotIndex)
		local itemLink = GetBuybackItemLink(slotIndex)
		local itemId = GetItemIdFromLink(itemLink)
		ESOMRLSetBuybackTooltip(control, slotIndex)
		if ESOMRL.RecipeMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, ESOMRL.RecipeMaterials[itemId].tooltip, itemId, itemLink, 1)
		elseif ESOMRL.IngredientMaterials[itemId] ~= nil then
			AddIngredientTooltipLine(control, itemId)
		elseif ESOMRL.ResultMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, nil, itemId, itemLink, 0)
		end
	end
	local ESOMRLSetTradeItemTooltip = ItemTooltip.SetTradeItem
	ItemTooltip.SetTradeItem = function(control, tradeId, slotIndex)
		local itemLink = GetTradeItemLink(slotIndex)
		local itemId = GetItemIdFromLink(itemLink)
		ESOMRLSetTradeItemTooltip(control, tradeId, slotIndex)
		if ESOMRL.RecipeMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, ESOMRL.RecipeMaterials[itemId].tooltip, itemId, itemLink, 1)
		elseif ESOMRL.IngredientMaterials[itemId] ~= nil then
			AddIngredientTooltipLine(control, itemId)
		elseif ESOMRL.ResultMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, nil, itemId, itemLink, 0)
		end
	end
	local ESOMRLSetStoreItemTooltip = ItemTooltip.SetStoreItem
	ItemTooltip.SetStoreItem = function(control, slotIndex)
		local itemLink = GetStoreItemLink(slotIndex)
		local itemId = GetItemIdFromLink(itemLink)
		ESOMRLSetStoreItemTooltip(control, slotIndex)
		if ESOMRL.RecipeMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, ESOMRL.RecipeMaterials[itemId].tooltip, itemId, itemLink, 1)
		elseif ESOMRL.IngredientMaterials[itemId] ~= nil then
			AddIngredientTooltipLine(control, itemId)
		elseif ESOMRL.ResultMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, nil, itemId, itemLink, 0)
		end
	end
	local ESOMRLSetLinkTooltip = PopupTooltip.SetLink
	PopupTooltip.SetLink = function(control, itemLink)
		local itemId = GetItemIdFromLink(itemLink)
		ESOMRLSetLinkTooltip(control, itemLink)
		if ESOMRL.RecipeMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, ESOMRL.RecipeMaterials[itemId].tooltip, itemId, itemLink, 1)
		elseif ESOMRL.IngredientMaterials[itemId] ~= nil then
			AddIngredientTooltipLine(control, itemId)
		elseif ESOMRL.ResultMaterials[itemId] ~= nil then
			AddRecipeTooltipLine(control, nil, itemId, itemLink, 0)
		end
	end
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Set up hooks into various inventories and apply the tracking icon if enabled.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function CreateInventoryTrackingControl(parent)
	local control = WINDOW_MANAGER:CreateControl(parent:GetName() .. 'InventoryTrackingControl', parent, CT_TEXTURE)
	control:SetDrawTier(DT_HIGH)
	control:SetHidden(true)
	return control
end

local function CreateInventoryKnownControl(parent)
	local control = WINDOW_MANAGER:CreateControl(parent:GetName() .. 'InventoryKnownControl', parent, CT_TEXTURE)
	control:SetDrawTier(DT_HIGH)
	control:SetHidden(true)
	return control
end

local function AddTrackingIndicator(control, bagID, slotIndex, itemLink, relativePoint, opt)
	local TrackingControl = control:GetNamedChild('InventoryTrackingControl')
	local KnownControl = control:GetNamedChild('InventoryKnownControl')
	local itemType = GetItemLinkItemType(itemLink)
	local itemId = GetItemIdFromLink(itemLink)
	local playerChar = GetUnitName("player")
	local IsGridViewEnabled
	
	if not ESOMRL.ASV[800].inventoryicons then
		if TrackingControl then TrackingControl:SetHidden(true) end
		if KnownControl then KnownControl:SetHidden(true) end
		return
	end
	
	if not TrackingControl then TrackingControl = CreateInventoryTrackingControl(control) else TrackingControl:SetHidden(true) end
	if not KnownControl then KnownControl = CreateInventoryKnownControl(control) else KnownControl:SetHidden(true) end
	
	if itemType ~= ITEMTYPE_INGREDIENT and itemType ~= ITEMTYPE_RECIPE and itemType ~= ITEMTYPE_FOOD and itemType ~= ITEMTYPE_DRINK then
		TrackingControl:SetHidden(true)
		KnownControl:SetHidden(true)
		return
	end
	if itemType == ITEMTYPE_INGREDIENT and ESOMRL.IngredientMaterials[itemId] == nil then
		TrackingControl:SetHidden(true)
		KnownControl:SetHidden(true)
		return
	end
	if itemType == ITEMTYPE_RECIPE and ESOMRL.RecipeMaterials[itemId] == nil then
		TrackingControl:SetHidden(true)
		KnownControl:SetHidden(true)
		return
	end
	if itemType == ITEMTYPE_FOOD and ESOMRL.ResultMaterials[itemId] == nil then
		TrackingControl:SetHidden(true)
		KnownControl:SetHidden(true)
		return
	end
	if itemType == ITEMTYPE_DRINK and ESOMRL.ResultMaterials[itemId] == nil then
		TrackingControl:SetHidden(true)
		KnownControl:SetHidden(true)
		return
	end

	if ( control.isGrid or ( control:GetWidth() - control:GetHeight() < 5 ) ) then
		IsGridViewEnabled = true else IsGridViewEnabled = false
	end

	local controlName = WINDOW_MANAGER:GetControlByName(control:GetName() .. 'Name')
	TrackingControl:ClearAnchors()

	if opt == 2 then
		TrackingControl:SetAnchor(LEFT, controlName, relativePoint, ESOMRL.ASV[800].gstoreiconoffset, 9)
		KnownControl:SetAnchor(LEFT, controlName, relativePoint, ESOMRL.ASV[800].gstoreiconoffset, 9)
	elseif opt == 3 then
		TrackingControl:SetAnchor(LEFT, controlName, relativePoint, ESOMRL.ASV[800].glistingiconoffset, 0)
		KnownControl:SetAnchor(LEFT, controlName, relativePoint, ESOMRL.ASV[800].glistingiconoffset, 0)
	elseif opt == 1 then
		if IsGridViewEnabled then
			TrackingControl:SetAnchor(LEFT, parent, BOTTOMLEFT, 4, -12)
			KnownControl:SetAnchor(LEFT, parent, BOTTOMLEFT, 4, -12)
		else
			TrackingControl:SetAnchor(LEFT, controlName, relativePoint, ESOMRL.ASV[800].bagiconoffset)
			KnownControl:SetAnchor(LEFT, controlName, relativePoint, ESOMRL.ASV[800].bagiconoffset)
		end
	end

	if itemType == ITEMTYPE_INGREDIENT then
		local saved = GetIngredientSavedVar(tonumber(ESOMRL.IngredientMaterials[itemId].position))
		if KnownControl then KnownControl:SetHidden(true) end
		if saved == 0 then
			if Contains(WritIng, itemId) then
				if IsGridViewEnabled then
					TrackingControl:SetDimensions(14, 14)
					TrackingControl:SetTexture('/MasterRecipeList/textures/trackings_igv.dds')
					TrackingControl:SetHidden(false)
					KnownControl:SetHidden(true)
				else
					TrackingControl:SetDimensions(20, 20)
					TrackingControl:SetTexture('/MasterRecipeList/textures/trackings.dds')
					TrackingControl:SetHidden(false)
					KnownControl:SetHidden(true)
				end
			else			
				if TrackingControl then TrackingControl:SetHidden(true) end
			end
		else
			if IsGridViewEnabled then
				TrackingControl:SetDimensions(14, 14)
				if Contains(WritIng, itemId) then
					TrackingControl:SetTexture('/MasterRecipeList/textures/trackings_igv.dds')
				else
					TrackingControl:SetTexture('/MasterRecipeList/textures/tracking_igv.dds')
				end
				TrackingControl:SetHidden(false)
				KnownControl:SetHidden(true)
			else
				TrackingControl:SetDimensions(20, 20)
				if Contains(WritIng, itemId) then
					TrackingControl:SetTexture('/MasterRecipeList/textures/trackings.dds')
				else
					TrackingControl:SetTexture('/MasterRecipeList/textures/tracking.dds')
				end
				TrackingControl:SetHidden(false)
				KnownControl:SetHidden(true)
			end
		end
	elseif itemType == ITEMTYPE_RECIPE then
		local saved = GetRecipeSavedVar(tonumber(ESOMRL.RecipeMaterials[itemId].position), 1)
		local resLink = ESOMRL.RecipeMaterials[itemId].item
		if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
			if IsGridViewEnabled then
				TrackingControl:SetDimensions(14, 14)
				TrackingControl:SetTexture('/MasterRecipeList/textures/trackings_igv.dds')
				TrackingControl:SetHidden(false)
			else
				TrackingControl:SetDimensions(20, 20)
				TrackingControl:SetTexture('/MasterRecipeList/textures/trackings.dds')
				TrackingControl:SetHidden(false)
			end
		elseif (saved ~= 1) and (saved ~= 2) then
			if TrackingControl then TrackingControl:SetHidden(true) end
			local pos = tonumber(ESOMRL.RecipeMaterials[itemId].position)
			if ESOMRL.SV[800].tchart then
				if not Contains(ESOMRL.ASV[pos].names, nameList[ESOMRL.SV[800].trackingchar]) then
					if IsGridViewEnabled then
						KnownControl:SetDimensions(14, 14)
						KnownControl:SetTexture('/MasterRecipeList/textures/trackcknown_igv.dds')
						KnownControl:SetHidden(false)
					else
						KnownControl:SetDimensions(20, 20)
						KnownControl:SetTexture('/MasterRecipeList/textures/trackcknown.dds')
						KnownControl:SetHidden(false)
					end
				elseif ESOMRL.SV[800].altknown then
					if ESOMRL.SV[800].altknownself then
						if Contains(ESOMRL.ASV[pos].names, playerChar) then
							if IsGridViewEnabled then
								KnownControl:SetDimensions(14, 14)
								KnownControl:SetTexture('/MasterRecipeList/textures/trackaknown_igv.dds')
								KnownControl:SetHidden(false)
							else
								KnownControl:SetDimensions(20, 20)
								KnownControl:SetTexture('/MasterRecipeList/textures/trackaknown.dds')
								KnownControl:SetHidden(false)
							end
						end
					elseif not ESOMRL.SV[800].altknownself then
						if Contains(ESOMRL.ASV[pos].names, nameList[ESOMRL.SV[800].trackingchar]) then
							if IsGridViewEnabled then
								KnownControl:SetDimensions(14, 14)
								KnownControl:SetTexture('/MasterRecipeList/textures/trackaknown_igv.dds')
								KnownControl:SetHidden(false)
							else
								KnownControl:SetDimensions(20, 20)
								KnownControl:SetTexture('/MasterRecipeList/textures/trackaknown.dds')
								KnownControl:SetHidden(false)
							end
						end
					end
				end
			end
		else
			if IsGridViewEnabled then
				if KnownControl then KnownControl:SetHidden(true) end
				TrackingControl:SetDimensions(14, 14)
				TrackingControl:SetTexture('/MasterRecipeList/textures/tracking_igv.dds')
				TrackingControl:SetHidden(false)
			else
				if KnownControl then KnownControl:SetHidden(true) end
				TrackingControl:SetDimensions(20, 20)
				TrackingControl:SetTexture('/MasterRecipeList/textures/tracking.dds')
				TrackingControl:SetHidden(false)
			end
		end
	elseif itemType == ITEMTYPE_FOOD or itemType == ITEMTYPE_DRINK then
		local resLink = ESOMRL.ProvisioningLinks[tonumber(ESOMRL.ResultMaterials[itemId].position)].item
		if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
			if IsGridViewEnabled then
				TrackingControl:SetDimensions(14, 14)
				TrackingControl:SetTexture('/MasterRecipeList/textures/trackings_igv.dds')
				TrackingControl:SetHidden(false)
			else
				TrackingControl:SetDimensions(20, 20)
				TrackingControl:SetTexture('/MasterRecipeList/textures/trackings.dds')
				TrackingControl:SetHidden(false)
			end
		end
	else
		if TrackingControl then TrackingControl:SetHidden(true) end
		if KnownControl then KnownControl:SetHidden(true) end
	end

	if FCOIsMarked and ESOMRL.SV[800].fcoitemsaver == true then -- Compatibility for FCO ItemSaver
		if opt == 1 then
			if itemType == ITEMTYPE_INGREDIENT and ESOMRL.IngredientMaterials[itemId] ~= nil then
				local saved = GetIngredientSavedVar(tonumber(ESOMRL.IngredientMaterials[itemId].position))
				if saved == 0 then
					FCOMarkItem(bagID, slotIndex, 1, false, true)
				else
					if TrackingControl then TrackingControl:SetHidden(true) end
					FCOMarkItem(bagID, slotIndex, 1, true, true)
				end
			elseif itemType == ITEMTYPE_RECIPE and ESOMRL.RecipeMaterials[itemId] ~= nil then
				local saved = GetRecipeSavedVar(tonumber(ESOMRL.RecipeMaterials[itemId].position), 1)
				if (saved ~= 1) and (saved ~= 2) then
					FCOMarkItem(bagID, slotIndex, 1, false, true)
				else
					if TrackingControl then TrackingControl:SetHidden(true) end
					FCOMarkItem(bagID, slotIndex, 1, true, true)
				end
			end
		end
	elseif FCOIsMarked and ESOMRL.SV[800].fcoitemsaver == false then
		if opt == 1 then
			if itemType == ITEMTYPE_INGREDIENT and ESOMRL.IngredientMaterials[itemId] ~= nil then
				FCOMarkItem(bagID, slotIndex, 1, false, true)
			elseif itemType == ITEMTYPE_RECIPE and ESOMRL.RecipeMaterials[itemId] ~= nil then
				FCOMarkItem(bagID, slotIndex, 1, false, true)
			end
		end
	end
end

local function HookBags( ... )
	for k,v in pairs(PLAYER_INVENTORY.inventories) do
		local listView = v.listView
		if ( listView and listView.dataTypes and listView.dataTypes[1] ) then
			local ESOMRLHookBags = listView.dataTypes[1].setupCallback
			listView.dataTypes[1].setupCallback = function(control, slot)
			ESOMRLHookBags(control, slot)
				local itemLink = GetItemLink(control.dataEntry.data.bagId, control.dataEntry.data.slotIndex, LINK_STYLE_BRACKETS)
				AddTrackingIndicator(control, control.dataEntry.data.bagId, control.dataEntry.data.slotIndex, itemLink, RIGHT, 1)
			end				
		end
	end
end

local function HookTradeHouse( ... )
	local ESOMRLHookTradeHouseSearch = TRADING_HOUSE.m_searchResultsList.dataTypes[1].setupCallback
	TRADING_HOUSE.m_searchResultsList.dataTypes[1].setupCallback = function( ... )
	local control, data = ...
	ESOMRLHookTradeHouseSearch( ... )
		if ( control.slotControlType and control.slotControlType == 'listSlot' and control.dataEntry.data.slotIndex ) then
			local itemLink = GetTradingHouseSearchResultItemLink(control.dataEntry.data.slotIndex, LINK_STYLE_BRACKETS)
			AddTrackingIndicator(control, nil, nil, itemLink, RIGHT, 2)
		end
	end
	local ESOMRLHookTradeHouseListing = TRADING_HOUSE.m_postedItemsList.dataTypes[2].setupCallback
	TRADING_HOUSE.m_postedItemsList.dataTypes[2].setupCallback = function( ... )
	local control, data = ...
	ESOMRLHookTradeHouseListing( ... )
		if ( control.slotControlType and control.slotControlType == 'listSlot' and control.dataEntry.data.slotIndex ) then
			local itemLink = GetTradingHouseListingItemLink(control.dataEntry.data.slotIndex, LINK_STYLE_BRACKETS)
			AddTrackingIndicator(control, nil, nil, itemLink, RIGHT, 3)
		end
	end
end

local function HookBuyback( ... )
	local ESOMRLHookBuybackList = ZO_BuyBackList.dataTypes[1].setupCallback
	ZO_BuyBackList.dataTypes[1].setupCallback = function( ... )
	local control, data = ...
	ESOMRLHookBuybackList( ... )
		if ( control.slotControlType and control.slotControlType == 'listSlot' and control.dataEntry.data.slotIndex ) then
			local itemLink = GetBuybackItemLink(control.dataEntry.data.slotIndex, LINK_STYLE_BRACKETS)
			AddTrackingIndicator(control, control.dataEntry.data.bagId, control.dataEntry.data.slotIndex, itemLink, RIGHT, 1)
		end
	end
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Handle coloring the cooking station categories and adding optional stat and writ tracking icons.
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function ClearStationSelection()
	local selectedNode = stationControl.recipeTree.selectedNode
	if selectedNode then selectedNode.selected = false selectedNode = nil stationControl:RefreshRecipeList() end
	stationControl.resultTooltip:SetHidden(true)
	for ingredientIndex, ingredientSlot in ipairs(stationControl.ingredientRows) do
		ingredientSlot:ClearItem()
	end
end

local function ToggleStationTooltips(control, option) -- Handles turning cooking station item tooltips on and off.
	if option == 1 then
		InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, 0, -11, TOPRIGHT)
		if ESOMRL.SV[800].sttshow == 1 then
			SetTooltipText(InformationTooltip, L.ESOMRL_LTTHIDE)
		else
			SetTooltipText(InformationTooltip, L.ESOMRL_LTTSHOW)
		end
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		if NoRecipes() == false then
			if ESOMRL.SV[800].sttshow == 1 then
				stationControl.resultTooltip:SetHidden(true)
				ESOMRL.SV[800].sttshow = 0
				ClearTooltip(InformationTooltip)
				InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, 0, -11, TOPRIGHT)
				SetTooltipText(InformationTooltip, L.ESOMRL_LTTSHOW)
			else
				local selectedNode = stationControl.recipeTree.selectedNode
				if selectedNode and selectedNode.selected then
					stationControl.resultTooltip:SetHidden(false)
				end
				ESOMRL.SV[800].sttshow = 1
				ClearTooltip(InformationTooltip)
				InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, 0, -11, TOPRIGHT)
				SetTooltipText(InformationTooltip, L.ESOMRL_LTTHIDE)
			end
		end
	end
end

local function HighlightTracked(control, option) -- Toggle highlighting tracked items at the cooking station.
	if option == 1 then
		InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, 0, -11, TOPRIGHT)
		if ESOMRL.SV[800].stmarked == 1 then
			SetTooltipText(InformationTooltip, L.ESOMRL_STRACKN)
		else
			SetTooltipText(InformationTooltip, L.ESOMRL_STRACKY)
		end
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		if ESOMRL.SV[800].stmarked == 1 then
			ESOMRL.SV[800].stmarked = 0
			stationControl:RefreshRecipeList()
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, 0, -11, TOPRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_STRACKY)
		else
			ESOMRL.SV[800].stmarked = 1
			stationControl:RefreshRecipeList()
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, 0, -11, TOPRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_STRACKN)
		end
	end
end

local function SetStationIcons(control, list, headerText, hasWrit, hasTrack) -- Add stat indicator if enabled.
	local recipeListName, numRecipes, upIcon, downIcon, overIcon, disabledIcon, createSound = GetRecipeListInfo(list)
	local recipeListName = string.upper(recipeListName)
	local function markTrack() control.text:SetText(control.text:GetText() .. StationIcons[9]) end
	local function markWrit() control.text:SetText(control.text:GetText() .. StationIcons[8]) end
	if list == 1 or list == 8 then
		if recipeListName == headerText then
			if ESOMRL.ASV[800].stationstats then control.text:SetText(headerText .. StationIcons[1]) else control.text:SetText(headerText) end
			if hasTrack then markTrack() end
			if hasWrit then markWrit() end
		end
	elseif list== 2 or list == 9 then
		if recipeListName == headerText then
			if ESOMRL.ASV[800].stationstats then control.text:SetText(headerText .. StationIcons[2]) else control.text:SetText(headerText) end
			if hasTrack then markTrack() end
			if hasWrit then markWrit() end
		end
	elseif list == 3 or list == 10 then
		if recipeListName == headerText then
			if ESOMRL.ASV[800].stationstats then control.text:SetText(headerText .. StationIcons[3]) else control.text:SetText(headerText) end
			if hasTrack then markTrack() end
			if hasWrit then markWrit() end
		end
	elseif list == 4 or list == 11 then
		if recipeListName == headerText then
			if ESOMRL.ASV[800].stationstats then control.text:SetText(headerText .. StationIcons[4]) else control.text:SetText(headerText) end
			if hasTrack then markTrack() end
			if hasWrit then markWrit() end
		end
	elseif list == 5 or list == 12 then
		if recipeListName == headerText then
			if ESOMRL.ASV[800].stationstats then control.text:SetText(headerText .. StationIcons[5]) else control.text:SetText(headerText) end
			if hasTrack then markTrack() end
			if hasWrit then markWrit() end
		end
	elseif list == 6 or list == 13 then
		if recipeListName == headerText then
			if ESOMRL.ASV[800].stationstats then control.text:SetText(headerText .. StationIcons[6]) else control.text:SetText(headerText) end
			if hasTrack then markTrack() end
			if hasWrit then markWrit() end
		end
	elseif list == 7 or list == 14 then
		if recipeListName == headerText then
			if ESOMRL.ASV[800].stationstats then control.text:SetText(headerText .. StationIcons[7]) else control.text:SetText(headerText) end
			if hasTrack then markTrack() end
			if hasWrit then markWrit() end
		end
	elseif list == 15 then
		if recipeListName == headerText then
			control.text:SetText(headerText)
			if hasTrack then markTrack() end
		end
	end
end

local function SetStationColors(control, opt, recipeListIndex) -- Colorize cooking station recipe headers by quality.
	local headerText = control.text:GetText():gsub("  |.*",''):gsub(" |.*",'')
	local function setWritColor(recipeListName, header, cv, hasWrit, hasTrack)
		local r, g, b
		if cv == 1 then
			if recipeListName == headerText then
				if StationControls[header].c == nil then StationControls[header].c = control end
				if opt == 1 then r, g, b = 0.176, 0.773, 0.055 else r, g, b = 0.314, 0.91, 0.192 end
				control.text:SetColor(r,g,b)
				SetStationIcons(control, header, headerText, hasWrit, hasTrack)
			end
		elseif cv == 2 then
			if recipeListName == headerText then
				if StationControls[header].c == nil then StationControls[header].c = control end
				if opt == 1 then r, g, b = 0.227, 0.573, 1 else r, g, b = 0.365, 0.71, 1 end
				control.text:SetColor(r,g,b)
				SetStationIcons(control, header, headerText, hasWrit, hasTrack)
			end
		elseif cv == 3 then
			if recipeListName == headerText then
				if StationControls[header].c == nil then StationControls[header].c = control end
				if opt == 1 then r, g, b = 0.627, 0.18, 0.969 else r, g, b = 0.765, 0.318, 1 end
				control.text:SetColor(r,g,b)
				SetStationIcons(control, header, headerText, hasWrit, hasTrack)
			end
		elseif cv == 4 then
			if recipeListName == headerText then
				if StationControls[header].c == nil then StationControls[header].c = control end
				if opt == 1 then r, g, b = 0.933, 0.792, 0.165 else r, g, b = 1, 0.929, 0.302 end
				control.text:SetColor(r,g,b)
				SetStationIcons(control, header, headerText, hasWrit, hasTrack)
			end
		end
	end
	if recipeListIndex ~= nil then
		local countWrit = false
		local countTrack = false
		local recipeListName, numRecipes, upIcon, downIcon, overIcon, disabledIcon, createSound = GetRecipeListInfo(recipeListIndex)
		recipeListName = string.upper(recipeListName)
		local cv = StationChecking[recipeListIndex].cv
		for i = 1, #StationChecking[recipeListIndex].tr do
			if StationChecking[recipeListIndex].tr[i].writ == 1 then countWrit = true break end
		end
		for i = 1, #StationChecking[recipeListIndex].tr do
			if StationChecking[recipeListIndex].tr[i].track == 1 then countTrack = true break end
		end
		setWritColor(recipeListName, recipeListIndex, cv, countWrit, countTrack)
	else
		for c = 1, 16 do
			local countWrit = false
			local countTrack = false
			local recipeListName, numRecipes, upIcon, downIcon, overIcon, disabledIcon, createSound = GetRecipeListInfo(c)
			recipeListName = string.upper(recipeListName)
			local cv = StationChecking[c].cv
			for i = 1, #StationChecking[c].tr do
				if StationChecking[c].tr[i].writ == 1 then countWrit = true break end
			end
			for i = 1, #StationChecking[c].tr do
				if StationChecking[c].tr[i].track == 1 then countTrack = true break end
			end
			setWritColor(recipeListName, c, cv, countWrit, countTrack)
		end
	end
end

local function SetWritColor(self)
	local recipeListIndex = self.data.recipeListIndex
	local recipeIndex = self.data.recipeIndex
	local itemLink = GetRecipeResultItemLink(recipeListIndex, recipeIndex)
	local itemId = GetItemIdFromLink(itemLink)
	local control = StationControls[recipeListIndex].c
	local saved = GetRecipeSavedVar(LookupPosition(ESOMRL.ResultMaterials, itemId), 1)
	local sub = ESOMRL.ResultMaterials[itemId].sub
	StationChecking[recipeListIndex].tr[sub].writ = 0
	StationChecking[recipeListIndex].tr[sub].track = 0
	if itemId == WritQuest.reqId1 or itemId == WritQuest.reqId2 then
		StationChecking[recipeListIndex].tr[sub].writ = 1
		SetStationColors(control, 1, recipeListIndex)
		if not self.enabled then
			return 0.878,0.878,0.298
		elseif self.mouseover then
			return 1,1,0.394
		else
			return 1,1,0
		end
	elseif ( ( (saved == 1) or (saved == 2) ) and ( ESOMRL.SV[800].stmarked == 1 ) ) then
		StationChecking[recipeListIndex].tr[sub].track = 1
		SetStationColors(control, 1, recipeListIndex)
		if not self.enabled then
			return 0,0.82,0.91
		elseif self.selected or self.mouseover then
			return 0.2,1,1
		else
			return 0,0.91,1
		end
	elseif not self.enabled then
		SetStationColors(control, 1, recipeListIndex)
		return GetInterfaceColor(INTERFACE_COLOR_TYPE_TEXT_COLORS, INTERFACE_TEXT_COLOR_DISABLED)
	elseif self.selected then
		SetStationColors(control, 1, recipeListIndex)
		return GetInterfaceColor(INTERFACE_COLOR_TYPE_TEXT_COLORS, INTERFACE_TEXT_COLOR_SELECTED)
	elseif self.mouseover then
		SetStationColors(control, 1, recipeListIndex)
		return GetInterfaceColor(INTERFACE_COLOR_TYPE_TEXT_COLORS, INTERFACE_TEXT_COLOR_HIGHLIGHT)
	elseif self.meetsLevelReq and self.meetsQualityReq then
		SetStationColors(control, 1, recipeListIndex)
		return GetInterfaceColor(INTERFACE_COLOR_TYPE_TEXT_COLORS, INTERFACE_TEXT_COLOR_NORMAL)
	end
	return ZO_ERROR_COLOR:UnpackRGBA()
end

local function HookStation()
	local ESOMRL_ZO_IconHeader_Setup = ZO_IconHeader_Setup
	ZO_IconHeader_Setup = function(control,...)
		ESOMRL_ZO_IconHeader_Setup(control,...)
		if GetCraftingInteractionType() == 5 then SetStationColors(control, 1) end
	end
	local ESOMRL_ZO_IconHeader_OnMouseEnter = ZO_IconHeader_OnMouseEnter
	ZO_IconHeader_OnMouseEnter = function(control)
		ESOMRL_ZO_IconHeader_OnMouseEnter(control)
		if GetCraftingInteractionType() == 5 then SetStationColors(control, 2) end
	end
	local ESOMRL_ZO_IconHeader_OnMouseExit = ZO_IconHeader_OnMouseExit
	ZO_IconHeader_OnMouseExit = function(control)
		ESOMRL_ZO_IconHeader_OnMouseExit(control)
		if GetCraftingInteractionType() == 5 then SetStationColors(control, 1) end
	end
	local ESOMRL_ZO_ProvisionerRow_GetTextColor = ZO_ProvisionerRow_GetTextColor
	ZO_ProvisionerRow_GetTextColor = function(self)
		return SetWritColor(self)
	end

	local ESOMRL_ZO_ProvisionerRefreshRecipeDetails = ZO_Provisioner.RefreshRecipeDetails
	ZO_Provisioner.RefreshRecipeDetails = function(self,...)
		local recipeListIndex, recipeIndex = self:GetSelectedRecipeListIndex(), self:GetSelectedRecipeIndex()
		local itemLink = GetRecipeResultItemLink(recipeListIndex, recipeIndex)
		local itemId = GetItemIdFromLink(itemLink)
		local name, icon = GetRecipeResultItemInfo(recipeListIndex, recipeIndex)
		stationSelection = LinkNameLanguageFormat(name) -- Store the selected item name for interactive use elsewhere.
		ESOMRL_ZO_ProvisionerRefreshRecipeDetails(self,...)

		if stnorecs == 0 then
			if ESOMRL.SV[800].sttshow == 0 then self.resultTooltip:SetHidden(true) else self.resultTooltip:SetHidden(false) end
		else
			self.resultTooltip:SetHidden(true)
		end

		if itemId == WritQuest.reqId1 or itemId == WritQuest.reqId2 then
			self.resultTooltip:AddVerticalPadding(20)
			self.resultTooltip:AddLine('|cffff00' .. L.ESOMRL_NWRIT .. '|r', 'ZoFontHeader2', 1, 1, 1, CENTER, MODIFY_TEXT_TYPE_NONE, CENTER, false)
		end
	end

	local ESOMRL_ZO_ProvisionerStartInteract = ZO_Provisioner.StartInteract
	ZO_Provisioner.StartInteract = function(self,...)
		ESOMRL_ZO_ProvisionerStartInteract(self,...)
		stationControl = self -- Store the station control for interactive use elsewhere.
		stationControl:RefreshRecipeList()
	end
	local ESOMRL_ZO_ProvisionerOnSceneShowing = ZO_Provisioner.OnSceneShowing
	ZO_Provisioner.OnSceneShowing = function(self)
		ESOMRL_ZO_ProvisionerOnSceneShowing(self)
		if stationItem == 0 then
			stationControl.resultTooltip:SetHidden(true)
			for ingredientIndex, ingredientSlot in ipairs(stationControl.ingredientRows) do ingredientSlot:ClearItem() end
			stationItem = 1
		end
	end
	local ESOMRL_ZO_ProvisionerOnTabFilterChanged = ZO_Provisioner.OnTabFilterChanged
	ZO_Provisioner.OnTabFilterChanged = function(self, filterData)
		ESOMRL_ZO_ProvisionerOnTabFilterChanged(self, filterData)
		ClearStationSelection()
	end

	local ESOMRL_ZO_CheckButton_OnClicked = ZO_CheckButton_OnClicked
	ZO_CheckButton_OnClicked = function(buttonControl, mouseButton)
	ESOMRL_ZO_CheckButton_OnClicked(buttonControl, mouseButton)
		if GetCraftingInteractionType() == 5 then
			ClearStationSelection()
		end
	end

	local ESOMRL_ZO_IconHeader_UpdateSize = ZO_IconHeader_UpdateSize
	ZO_IconHeader_UpdateSize = function(control)
		ESOMRL_ZO_IconHeader_UpdateSize(control)
		if GetCraftingInteractionType() == 5 then
			local _, textHeight = control.text:GetTextDimensions() 
			control.text:SetDimensionConstraints(0, 0, 580, 0)
			control.text:SetDimensions(580, textHeight)
		end
	end

	local ESOMRL_ZO_TreeHeader_OnMouseUp = ZO_TreeHeader_OnMouseUp
	ZO_TreeHeader_OnMouseUp = function(self,...)
		if GetCraftingInteractionType() == 5 then
			local treeNode = self.node.tree
			if treeNode.exclusive or treeNode.open then
				treeNode.exclusive = false
				treeNode.open = false
			end
			stationNav = 1
			stationItem = 1
			ClearStationSelection()
		end
		ESOMRL_ZO_TreeHeader_OnMouseUp(self,...)
	end

	local ESOMRL_ZO_TreeSetNodeOpen = ZO_Tree.SetNodeOpen
	ZO_Tree.SetNodeOpen = function(self, treeNode, open,...)
		if GetCraftingInteractionType() == 5 then
			if stationNode == 0 then open = false stationNode = 1 end
			if stationItem == 0 or stationNav == 0 then open = false end
		end
		ESOMRL_ZO_TreeSetNodeOpen(self, treeNode, open,...)
	end
end

local function OnEndCraftingStationInteract(eventCode)
	ESOMRL:SetHidden(true)
	stationControl = nil
	EVENT_MANAGER:UnregisterForEvent(ESOMRL.Name, EVENT_END_CRAFTING_STATION_INTERACT)
end

local function OnCraftingStationInteract(eventCode, craftSkill)
	if craftSkill == CRAFTING_TYPE_PROVISIONING then
		if NoRecipes() then stnorecs = 1 else stnorecs = 0 end
		local controlName = ZO_ProvisionerTopLevelNavigationContainer
		if ESOMRL:IsHidden() then
			if CS_Character then -- Station navigation offset for CraftStore
				ESOMRL:ClearAnchors() ESOMRL:SetAnchor(TOPLEFT, controlName, TOPLEFT, 0, -186) ESOMRL:SetHidden(false)
			elseif KhrillMasterCook_settings then -- Station navigation offset for Krill Master Cook
				ESOMRL:ClearAnchors() ESOMRL:SetAnchor(TOPLEFT, controlName, TOPLEFT, 58, -89) ESOMRL:SetHidden(false)
			else -- Standard station navigation position
				ESOMRL:ClearAnchors() ESOMRL:SetAnchor(TOPLEFT, controlName, TOPLEFT, 0, -86) ESOMRL:SetHidden(false)
			end
		end
		EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_END_CRAFTING_STATION_INTERACT, OnEndCraftingStationInteract)
	end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Initialize the scrollable list of recipes and handle various category and search functions.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function SetListItem(control,data) -- Hook the ScrollList data table.
	local listitemtext = control:GetNamedChild( 'Name' )
	listitemtext:SetText( data.RecipeName )
end

local function TextSearch(refresh) -- Populate recipe list based on text search.
	local datalist = ZO_ScrollList_GetDataList(ESOMRL_MainFrameListFrameList)
	local searchtext
	local known
	local color
	local row = 0
	if editText ~= 2 then
		openTab = 50
		if editText == 1 or refresh == 1 then
			ZO_ScrollList_Clear(ESOMRL_MainFrameListFrameList) 
			if refresh == 1 then searchtext = LowerSpaceSpecial(LinkNameLanguageFormat(textInput)) else searchtext = ESOMRL_MainFrameSelectionFrameSearchBox:GetText() end
			if searchtext == "" then
				for k,v in pairs(datalist) do datalist[k] = nil end
				ZO_ScrollList_Commit(ESOMRL_MainFrameListFrameList, datalist)
			else
				for k,v in pairs(searchTable) do searchTable[k] = nil end
				if refresh ~= 1 then searchtext = searchtext:gsub("%W",''):lower() end
				for i = 1, 535 do
					local name = LowerSpaceSpecial(LinkNameLanguageFormat(GetItemLinkName(ESOMRL.ProvisioningLinks[i].item)))
					local flevel = ESOMRL.ProvisioningLabels[i].level
					local searchn = searchtext:gsub("%D",'')
					flevel = flevel:gsub("%(Veteran",''):gsub("%(Level",''):gsub("Food%)",''):gsub("Food %*%)",''):gsub("Drink%)",''):gsub("Drink %*%)",''):gsub("%s",'')
					flevel = tonumber(flevel)
					if string.find(ESOMRL.ProvisioningLabels[i].level,"Veteran") ~= nil then
						if flevel == 1 then flevel = 50 else flevel = flevel + 50 end
					end
					searchn = tonumber(searchn)
					if (string.find(name,searchtext) ~= nil) or (flevel == searchn and flevel ~= nil) then
						local ln = tonumber(ESOMRL.ProvisioningLabels[i].position)
						if ln > 0 and ln < 127 then
							color = '|c00ff00'
						elseif ln > 126 and ln < 235 then
							color = '|c3a92ff'
						elseif ln > 234 and ln < 266 then
							color = '|ca02ef7'
						elseif ln > 265 and ln < 392 then
							color = '|c00ff00'
						elseif ln > 391 and ln < 500 then
							color = '|c3a92ff'
						elseif ln > 499 and ln < 531 then
							color = '|ca02ef7'
						elseif ln >= 531 then
							color = '|ceeca2a'
						end
						local level = FormatTooltipText(ESOMRL.ProvisioningLabels[ln].level) 
						local saved = GetRecipeSavedVar(ln, 0)
						if IsItemLinkRecipeKnown(ESOMRL.ProvisioningLinks[ln].link) == false then
							local resLink = ESOMRL.ProvisioningLinks[ln].item
							if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
								if saved == 1 then known = IconStrings[6] elseif saved == 0 then known = IconStrings[5] end
							else
								if saved == 1 then known = IconStrings[4] elseif saved == 0 then known = IconStrings[3] end
							end
						else
							if saved == 2 then known = IconStrings[2] elseif saved == 3 then known = IconStrings[1] end
						end
						row = row + 1
						searchSize = row
						local namestring = LinkNameLanguageFormat(GetItemLinkName(ESOMRL.ProvisioningLinks[ln].item))
						searchTable[row] = { pos = ln, text = known .. color .. namestring .. '|r' .. '|cffffff ' .. level .. '|r' }
						datalist[row] = ZO_ScrollList_CreateDataEntry( 1, 
						{
							RecipeName = known .. color .. namestring .. '|r' .. '|cffffff ' .. level .. '|r',
						}
						)
					end
				end
				ZO_ScrollList_Commit(ESOMRL_MainFrameListFrameList, datalist)
			end
		end
	end
end

local function IngRecipeSearch() -- Show list of recipes containing selected ingredients.
	local datalist = ZO_ScrollList_GetDataList(ESOMRL_MainFrameListFrameList)
	for k,v in pairs(datalist) do datalist[k] = nil end
	local ingredients = {}
	local recipes = {}
	local match = {}
	local known
	local color
	local row = 0
	ESOMRL_MainFrameSelectionFrame:SetHidden(false)
	ESOMRL_MainFrameListFrame:SetHidden(false)
	ESOMRL_MainFrameIngredientsFrame:SetHidden(true)
	setPage = 1
	textInput = ""
	openTab = 52
	for i = 1, 50 do
		local savedvar = GetIngredientSavedVar(i)
		if savedvar == 1 then
			local namelookup = LowerSpaceSpecial(GetItemLinkName(ESOMRL.IngredientLinks[i].link))
			if not Contains(ingredients, namelookup) then
				table.insert(ingredients, #ingredients + 1, namelookup)
			end
		end
	end
	if #ingredients == 0 then
		ZO_ScrollList_Commit(ESOMRL_MainFrameListFrameList, datalist)
	else
		for k,v in pairs(searchTable) do searchTable[k] = nil end
		for r = 1, 535 do
			local tempdb = {}
			local tempcount = 0
			local linkcheck = ESOMRL.ProvisioningLinks[r].link
			local numIngredients = GetItemLinkRecipeNumIngredients(linkcheck)
			for n = 1, numIngredients do
				local ingredientName,_ = GetItemLinkRecipeIngredientInfo(linkcheck, n)
				ingredientName = LowerSpaceSpecial(ingredientName)
				table.insert(tempdb, #tempdb + 1, ingredientName)
			end
			for c = 1, #ingredients do
				if not Contains(tempdb, ingredients[c]) then
					tempcount = tempcount - 1
				end
			end
			if tempcount >= 0 then match[r] = 1 else match[r] = 0 end
		end
		for m = 1, 535 do
			if match[m] == 1 then
				local ln = tonumber(ESOMRL.ProvisioningLabels[m].position)
				if ln > 0 and ln < 127 then
					color = '|c00ff00'
				elseif ln > 126 and ln < 235 then
					color = '|c3a92ff'
				elseif ln > 234 and ln < 266 then
					color = '|ca02ef7'
				elseif ln > 265 and ln < 392 then
					color = '|c00ff00'
				elseif ln > 391 and ln < 500 then
					color = '|c3a92ff'
				elseif ln > 499 and ln < 531 then
					color = '|ca02ef7'
				elseif ln >= 531 then
					color = '|ceeca2a'
				end
				local level = FormatTooltipText(ESOMRL.ProvisioningLabels[ln].level) 
				local saved = GetRecipeSavedVar(ln, 0)
				if IsItemLinkRecipeKnown(ESOMRL.ProvisioningLinks[ln].link) == false then
					local resLink = ESOMRL.ProvisioningLinks[ln].item
					if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
						if saved == 1 then known = IconStrings[6] elseif saved == 0 then known = IconStrings[5] end
					else
						if saved == 1 then known = IconStrings[4] elseif saved == 0 then known = IconStrings[3] end
					end
				else
					if saved == 2 then known = IconStrings[2] elseif saved == 3 then known = IconStrings[1] end
				end
				row = row + 1
				searchSize = row
				local namestring = LinkNameLanguageFormat(GetItemLinkName(ESOMRL.ProvisioningLinks[ln].item))
				searchTable[row] = { pos = ln, text = known .. color .. namestring .. '|r' .. '|cffffff ' .. level .. '|r' }
				datalist[row] = ZO_ScrollList_CreateDataEntry( 1, 
				{
					RecipeName = known .. color .. namestring .. '|r' .. '|cffffff ' .. level .. '|r',
				}
				)
			end
		end
		ZO_ScrollList_Commit(ESOMRL_MainFrameListFrameList, datalist)
	end
end

local function StationSearch(control, option) -- Search MRL for the selected cooking station item.
	if option == 1 then
		InitializeTooltip(InformationTooltip, control, BOTTOMLEFT, 0, -11, TOPRIGHT)
		SetTooltipText(InformationTooltip, L.ESOMRL_SEARCHS)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		local control = GetControl('ESOMRL_MainFrame')
		if control:IsHidden() then
			ESOMRL:SetLanguageFormat()
			control:SetHidden(false)
			RestorePosition()
			openTab = 1
			SetTrackedRecipeCountText()
			RecipePanelRestore()
			textInput = stationSelection
			TextSearch(1)
		else
			textInput = stationSelection
			TextSearch(1)
		end
	end
end

local function GetWrits() -- Populate recipe list based on current active writ requirements.
	if GetWritID() ~= 0 then
		local datalist = ZO_ScrollList_GetDataList(ESOMRL_MainFrameListFrameList)
		local searchtext
		local known
		local color
		local row = 0
		openTab = 51
		textInput = ''
		ZO_ScrollList_Clear(ESOMRL_MainFrameListFrameList)
		for k,v in pairs(datalist) do datalist[k] = nil end
		for k,v in pairs(searchTable) do searchTable[k] = nil end
		for i = 1, 487 do
			local lookup = GetItemIdFromLink(ESOMRL.ProvisioningLinks[i].item)
			if lookup == WritQuest.reqId1 or lookup == WritQuest.reqId2 then
				local ln = tonumber(ESOMRL.ProvisioningLabels[i].position)
				if ln > 0 and ln < 127 then
					color = '|c00ff00'
				elseif ln > 126 and ln < 235 then
					color = '|c3a92ff'
				elseif ln > 234 and ln < 266 then
					color = '|ca02ef7'
				elseif ln > 265 and ln < 392 then
					color = '|c00ff00'
				elseif ln > 391 and ln < 500 then
					color = '|c3a92ff'
				elseif ln > 499 and ln < 531 then
					color = '|ca02ef7'
				elseif ln >= 531 then
					color = '|ceeca2a'
				end
				local level = FormatTooltipText(ESOMRL.ProvisioningLabels[ln].level) 
				local saved = GetRecipeSavedVar(ln, 0)
				if IsItemLinkRecipeKnown(ESOMRL.ProvisioningLinks[ln].link) == false then
					local resLink = ESOMRL.ProvisioningLinks[ln].item
					if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
						if saved == 1 then known = IconStrings[6] elseif saved == 0 then known = IconStrings[5] end
					else
						if saved == 1 then known = IconStrings[4] elseif saved == 0 then known = IconStrings[3] end
					end
				else
					if saved == 2 then known = IconStrings[2] elseif saved == 3 then known = IconStrings[1] end
				end
				row = row + 1
				searchSize = row
				local namestring = LinkNameLanguageFormat(GetItemLinkName(ESOMRL.ProvisioningLinks[ln].item))
				searchTable[row] = { pos = ln, text = known .. color .. namestring .. '|r' .. '|cffffff ' .. level .. '|r' }
				datalist[row] = ZO_ScrollList_CreateDataEntry( 1, 
				{
					RecipeName = known .. color .. namestring .. '|r' .. '|cffffff ' .. level .. '|r',
				}
				)
			end
		end
		ZO_ScrollList_Commit(ESOMRL_MainFrameListFrameList, datalist)
		editText = 2
	end
end

local function TrackNavigationTier(list) -- Marks the entire tier as tracked if holding shift when clicking the button.
	local lower
	local upper
	if list == 1 then
		lower = 1
		upper = 42
	elseif list == 2 then
		lower = 43
		upper = 84
	elseif list == 3 then
		lower = 85
		upper = 126
	elseif list == 4 then
		lower = 127
		upper = 162
	elseif list == 5 then
		lower = 163
		upper = 198
	elseif list == 6 then
		lower = 199
		upper = 234
	elseif list == 7 then
		lower = 235
		upper = 265
	elseif list == 8 then
		lower = 266
		upper = 307
	elseif list == 9 then
		lower = 308
		upper = 349
	elseif list == 10 then
		lower = 350
		upper = 391
	elseif list == 11 then
		lower = 392
		upper = 427
	elseif list == 12 then
		lower = 428
		upper = 463
	elseif list == 13 then
		lower = 464
		upper = 499
	elseif list == 14 then
		lower = 500
		upper = 530
	elseif list == 15 then
		lower = 531
		upper = 531
	elseif list == 16 then
		lower = 532
		upper = 535
	end
	for i = lower, upper do
		local savedvar = GetRecipeSavedVar(i, 1)
		if savedvar == 3 then
			SetRecipeSavedVar(i, 1, 2)
			ESOMRL.SV[800].recipetracklist = ESOMRL.SV[800].recipetracklist + 1
		elseif savedvar == 0 then
			SetRecipeSavedVar(i, 1, 1)
			ESOMRL.SV[800].recipetracklist = ESOMRL.SV[800].recipetracklist + 1
		end
	end
	SetTrackedRecipeCountText()
	RefreshViews()
end

local function NavigateScrollList(list) -- Populate recipe list based on navigation item clicked.
	ZO_ScrollList_Clear(ESOMRL_MainFrameListFrameList)
	local datalist = ZO_ScrollList_GetDataList(ESOMRL_MainFrameListFrameList)
	for k,v in pairs(datalist) do datalist[k] = nil end
	for k,v in pairs(searchTable) do searchTable[k] = nil end	
	local known
	local color
	local row = 0
	local linktable
	textInput = ''
	if list == 1 then
		linktable = ESOMRL.ProvisioningLinksFood1
		color = '|c00ff00'
		openTab = 1
	elseif list == 2 then
		linktable = ESOMRL.ProvisioningLinksFood2
		color = '|c00ff00'
		openTab = 2
	elseif list == 3 then
		linktable = ESOMRL.ProvisioningLinksFood3
		color = '|c00ff00'
		openTab = 3
	elseif list == 4 then
		linktable = ESOMRL.ProvisioningLinksFood4
		color = '|c3a92ff'
		openTab = 4
	elseif list == 5 then
		linktable = ESOMRL.ProvisioningLinksFood5
		color = '|c3a92ff'
		openTab = 5
	elseif list == 6 then
		linktable = ESOMRL.ProvisioningLinksFood6
		color = '|c3a92ff'
		openTab = 6
	elseif list == 7 then
		linktable = ESOMRL.ProvisioningLinksFood7
		color = '|ca02ef7'
		openTab = 7
	elseif list == 8 then
		linktable = ESOMRL.ProvisioningLinksDrinks1
		color = '|c00ff00'
		openTab = 8
	elseif list == 9 then
		linktable = ESOMRL.ProvisioningLinksDrinks2
		color = '|c00ff00'
		openTab = 9
	elseif list == 10 then
		linktable = ESOMRL.ProvisioningLinksDrinks3
		color = '|c00ff00'
		openTab = 10
	elseif list == 11 then
		linktable = ESOMRL.ProvisioningLinksDrinks4
		color = '|c3a92ff'
		openTab = 11
	elseif list == 12 then
		linktable = ESOMRL.ProvisioningLinksDrinks5
		color = '|c3a92ff'
		openTab = 12
	elseif list == 13 then
		linktable = ESOMRL.ProvisioningLinksDrinks6
		color = '|c3a92ff'
		openTab = 13
	elseif list == 14 then
		linktable = ESOMRL.ProvisioningLinksDrinks7
		color = '|ca02ef7'
		openTab = 14
	elseif list == 15 then
		linktable = ESOMRL.ProvisioningLinksPsijic
		color = '|ceeca2a'
		openTab = 15
	elseif list == 16 then
		linktable = ESOMRL.ProvisioningLinksOrsinium
		color = '|ceeca2a'
		openTab = 16
	end
	for i = 1, #linktable do
		local ln = tonumber(linktable[i].position)
		local level = FormatTooltipText(linktable[i].level) 
		local saved = GetRecipeSavedVar(i, list)
		if IsItemLinkRecipeKnown(linktable[i].link) == false then
			local resLink = linktable[i].item
			if GetItemIdFromLink(resLink) == WritQuest.reqId1 or GetItemIdFromLink(resLink) == WritQuest.reqId2 then
				if saved == 1 then known = IconStrings[6] elseif saved == 0 then known = IconStrings[5] end
			else
				if saved == 1 then known = IconStrings[4] elseif saved == 0 then known = IconStrings[3] end
			end
		else
			if saved == 2 then known = IconStrings[2] elseif saved == 3 then known = IconStrings[1] end
		end
		row = row + 1
		searchSize = row
		local namestring = LinkNameLanguageFormat(GetItemLinkName(linktable[i].item))
		searchTable[row] = { pos = ln, text = known .. color .. namestring .. '|r' .. '|cffffff ' .. level .. '|r' }
		datalist[i] = ZO_ScrollList_CreateDataEntry( 1, 
		{
			RecipeName = known .. color .. namestring .. '|r' .. '|cffffff ' .. level .. '|r',
		}
		)
	end
	ZO_ScrollList_Commit(ESOMRL_MainFrameListFrameList, datalist)
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Handle the clicking of items in the active recipe list, setting and clearing tracked items.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function CountRecipeTrackingList(option, tier) -- Counts the current number of recipes you are tracking.
	if option == 0 then
		ESOMRL.SV[800].recipetracklist = ESOMRL.SV[800].recipetracklist - 1
		SetTrackedRecipeCountText()
	elseif option == 1 then
		ESOMRL.SV[800].recipetracklist = ESOMRL.SV[800].recipetracklist + 1
		SetTrackedRecipeCountText()
	end
	if tier == 50 then
		TextSearch(1)
	elseif tier == 51 then
		GetWrits()
	elseif tier == 52 then
		IngRecipeSearch()
	else
		NavigateScrollList(tier)
	end
	RefreshViews()
end

local function TrackAllRecipes(option) -- Set all recipes to tracked.
	if option == 1 then
		InitializeTooltip(InformationTooltip, ESOMRL_MainFrameCloseButton, TOPLEFT, 8, -44, BOTTOMRIGHT)
		SetTooltipText(InformationTooltip, L.ESOMRL_TRACKALL)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		for i = 1, 535 do
			local savedvar = GetRecipeSavedVar(i, 1)
			if savedvar == 3 then
				SetRecipeSavedVar(i, 1, 2)
				ESOMRL.SV[800].recipetracklist = ESOMRL.SV[800].recipetracklist + 1
			elseif savedvar == 0 then
				SetRecipeSavedVar(i, 1, 1)
				ESOMRL.SV[800].recipetracklist = ESOMRL.SV[800].recipetracklist + 1
			end
		end
		if (openTab ~= 50) and (openTab ~= 51) and (openTab ~= 52) then
			NavigateScrollList(openTab)
		else
			openTab = 1
			NavigateScrollList(openTab)
			textInput = ''
		end
		ESOMRL.SV[800].junkunmarkedrecipes = 0
		ESOMRL.SV[800].destroyunmarkedrecipes = 0
		SetTrackedRecipeCountText()
		RecipePanelRestore()
		RefreshViews()
	end
end

local function TrackUnknownRecipes(option) -- Track recipes the current selected character does not know.
	if option == 1 then
		InitializeTooltip(InformationTooltip, ESOMRL_MainFrameCloseButton, TOPLEFT, 8, -44, BOTTOMRIGHT)
		SetTooltipText(InformationTooltip, L.ESOMRL_TRACKUNKNOWN)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		for i = 1, 535 do
			local savedvar = GetRecipeSavedVar(i, 1)
			if not Contains(ESOMRL.ASV[i].names, currentChar) then
				if savedvar == 3 then
					SetRecipeSavedVar(i, 1, 2)
					ESOMRL.SV[800].recipetracklist = ESOMRL.SV[800].recipetracklist + 1
				elseif savedvar == 0 then
					SetRecipeSavedVar(i, 1, 1)
					ESOMRL.SV[800].recipetracklist = ESOMRL.SV[800].recipetracklist + 1
				end
			end
		end
		if (openTab ~= 50) and (openTab ~= 51) and (openTab ~= 52) then
			NavigateScrollList(openTab)
		else
			openTab = 1
			NavigateScrollList(openTab)
			textInput = ''
		end
		ESOMRL.SV[800].junkunmarkedrecipes = 0
		ESOMRL.SV[800].destroyunmarkedrecipes = 0
		SetTrackedRecipeCountText()
		RecipePanelRestore()
		RefreshViews()
	end
end

local function ClearTrackedRecipes() -- Clears all tracked recipes, removing tooltip line.
	ESOMRL.SV[800].recipetracklist = 0
	for i = 1, 535 do
		if IsItemLinkRecipeKnown(ESOMRL.ProvisioningLinks[i].link) == false then
			SetRecipeSavedVar(i, 1, 0)
		else
			if GetRecipeSavedVar(i, 1) == 2 then
				SetRecipeSavedVar(i, 1, 3)
			end
		end
	end
	if (openTab ~= 50) and (openTab ~= 51) and (openTab ~= 52) then
		NavigateScrollList(openTab)
	else
		openTab = 1
		NavigateScrollList(openTab)
		textInput = ''
	end
	ESOMRL.SV[800].junkunmarkedrecipes = 0
	ESOMRL.SV[800].destroyunmarkedrecipes = 0
	SetTrackedRecipeCountText()
	RecipePanelRestore()
	RefreshViews()
end

local function RecipeListClick(clicktext) -- Handles clicking and shift-clicking of list items.
	if openTab < 50 then
		for i = 1, searchSize do
			local name = searchTable[i].text
			if clicktext == name then
				if IsShiftKeyDown() == false then
					local savedval = GetRecipeSavedVar(i, openTab)
					if savedval == 0 then
						SetRecipeSavedVar(i, openTab, 1)
						CountRecipeTrackingList(1, openTab)
					elseif savedval == 1 then
						SetRecipeSavedVar(i, openTab, 0)
						CountRecipeTrackingList(0, openTab)
					elseif savedval == 2 then
						SetRecipeSavedVar(i, openTab, 3)
						CountRecipeTrackingList(0, openTab)
					elseif savedval == 3 then
						SetRecipeSavedVar(i, openTab, 2)
						CountRecipeTrackingList(1, openTab)
					end
					ESOMRL.SV[800].junkunmarkedrecipes = 0
					ESOMRL.SV[800].destroyunmarkedrecipes = 0
					RecipePanelRestore()
				elseif IsShiftKeyDown() == true then
					local index = 0
					if openTab == 1 then
						index = i
					elseif openTab == 2 then
						index = i + 42
					elseif openTab == 3 then
						index = i + 84
					elseif openTab == 4 then
						index = i + 126
					elseif openTab == 5 then
						index = i + 162
					elseif openTab == 6 then
						index = i + 198
					elseif openTab == 7 then
						index = i + 234
					elseif openTab == 8 then
						index = i + 265
					elseif openTab == 9 then
						index = i + 307
					elseif openTab == 10 then
						index = i + 349
					elseif openTab == 11 then
						index = i + 391
					elseif openTab == 12 then
						index = i + 427
					elseif openTab == 13 then
						index = i + 463
					elseif openTab == 14 then
						index = i + 499
					elseif openTab == 15 then
						index = 531
					elseif openTab == 16 then
						index = i + 531
					end
					local ChatEditControl = CHAT_SYSTEM.textEntry.editControl
					if not ChatEditControl:HasFocus() then 
						StartChatInput()
					end
					if ESOMRL.SV[800].tooltipstyle == 0 then
						ChatEditControl:InsertText(ESOMRL.ProvisioningLinks[index].link)
					elseif ESOMRL.SV[800].tooltipstyle == 1 then
						ChatEditControl:InsertText(ESOMRL.ProvisioningLinks[index].item)
					end
				end
			end
		end
	else
		for i = 1, searchSize do
			local row = searchTable[i].pos
			local name = searchTable[i].text
			if clicktext == name then
				if IsShiftKeyDown() == false then
					local savedval = GetRecipeSavedVar(row, openTab)
					if savedval == 0 then
						SetRecipeSavedVar(row, openTab, 1)
						CountRecipeTrackingList(1, openTab)
					elseif savedval == 1 then
						SetRecipeSavedVar(row, openTab, 0)
						CountRecipeTrackingList(0, openTab)
					elseif savedval == 2 then
						SetRecipeSavedVar(row, openTab, 3)
						CountRecipeTrackingList(0, openTab)
					elseif savedval == 3 then
						SetRecipeSavedVar(row, openTab, 2)
						CountRecipeTrackingList(1, openTab)
					end
					ESOMRL.SV[800].junkunmarkedrecipes = 0
					ESOMRL.SV[800].destroyunmarkedrecipes = 0
					RecipePanelRestore()
				elseif IsShiftKeyDown() == true then
					local ChatEditControl = CHAT_SYSTEM.textEntry.editControl
					if not ChatEditControl:HasFocus() then 
						StartChatInput() 
					end
					if ESOMRL.SV[800].tooltipstyle == 0 then
						ChatEditControl:InsertText(ESOMRL.ProvisioningLinks[row].link)
					elseif ESOMRL.SV[800].tooltipstyle == 1 then
						ChatEditControl:InsertText(ESOMRL.ProvisioningLinks[row].item)
					end
				end
			end
		end
	end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Handle the clicking of items in the ingredients panel.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function CountIngredientTrackingList(option) -- Counts the current number of ingredients you are tracking.
	if option == 0 then
		ESOMRL.SV[800].ingtracklist = ESOMRL.SV[800].ingtracklist - 1
		SetTrackedIngredientCountText()
	elseif option == 1 then
		ESOMRL.SV[800].ingtracklist = ESOMRL.SV[800].ingtracklist + 1
		SetTrackedIngredientCountText()
	end
	RefreshViews()
end

local function ClearTrackedIngredients() -- Clears all tracked ingredients, removing tooltip line.
	ESOMRL.SV[800].ingtracklist = 0
	for i = 1, 50 do
		SetIngredientSavedVar(i, 0)
		SetTrackedIngredientCountText()
	end
	ESOMRL.SV[800].junkunmarkedingredients = 0
	ESOMRL.SV[800].destroyunmarkedingredients = 0
	IngredientPanelSetup()
	RefreshViews()
end

local function SelectAllIngredients(control, option) -- Button function to mark all ingredients.
	if option == 1 then
		InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -43, BOTTOMRIGHT)
		SetTooltipText(InformationTooltip, L.ESOMRL_TRACKALLING)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		for i = 1, 50 do
			if GetIngredientSavedVar(i) == 0 then
				SetIngredientSavedVar(i, 1)
				CountIngredientTrackingList(1)
			end
		end
		ESOMRL.SV[800].junkunmarkedingredients = 0
		ESOMRL.SV[800].destroyunmarkedingredients = 0
		SetTrackedIngredientCountText()
		IngredientPanelSetup()
		RefreshViews()
	end
end

local function SaveGlobalIngredientProfile(option) -- Clear the global ingredient tracking profile.
	if option == 1 then
		InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -65, BOTTOMRIGHT)
		SetTooltipText(InformationTooltip, L.ESOMRL_SAVEINGTP)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		for i = 1, 50 do
			ESOMRL.ASV[801].globalingprofile[i] = GetIngredientSavedVar(i)
		end
	end
end

local function LoadGlobalIngredientProfile(option) -- Load the global ingredient tracking profile.
	if option == 1 then
		if GetCVar('Language.2') == 'fr' then
			InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -65, BOTTOMRIGHT)
		elseif GetCVar('Language.2') == 'de' then
			InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -65, BOTTOMRIGHT)
		else
			InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -43, BOTTOMRIGHT)
		end
		SetTooltipText(InformationTooltip, L.ESOMRL_LOADINGTP)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		ESOMRL.SV[800].ingtracklist = 0
		ESOMRL.SV[800].junkunmarkedingredients = 0
		ESOMRL.SV[800].destroyunmarkedingredients = 0
		for i = 1, 50 do
			local global = ESOMRL.ASV[801].globalingprofile[i]
			if global == 1 then ESOMRL.SV[800].ingtracklist = ESOMRL.SV[800].ingtracklist + 1 end
			SetIngredientSavedVar(i, global)
		end
		SetTrackedIngredientCountText()
		IngredientPanelSetup()
		RefreshViews()
	end
end

local function FindIngredientRecipes(option) -- Find recipes containing selected ingredients.
	if option == 1 then
		InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -65, BOTTOMRIGHT)
		SetTooltipText(InformationTooltip, L.ESOMRL_FINDRECIPES)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		IngRecipeSearch()
	end
end

local function IngredientListClick(control, option, index) -- Handles clicking and shift-clicking of ingredients.
	if option == 1 then
		if ESOMRL.SV[800].lttshow == 1 then
			local IngredientItemTooltip = IngredientItemTooltipControl
			local itemLink = ESOMRL.IngredientLinks[index].link
			InitializeTooltip(IngredientItemTooltip, ESOMRL_MainFrame, TOPLEFT, 10, -8, TOPRIGHT)
			PopupTooltip.SetLink(IngredientItemTooltip, itemLink)
		end
		if GetIngredientSavedVar(index) == 1 then
			control:SetColor(1,1,0,1)
			control:SetAlpha(.5)
		elseif GetIngredientSavedVar(index) == 0 then
			control:SetColor(1,1,1,1)
			control:SetAlpha(1)
		end
	elseif option == 2 then
		local IngredientItemTooltip = IngredientItemTooltipControl
		ClearTooltip(IngredientItemTooltip)
		if GetIngredientSavedVar(index) == 1 then
			control:SetColor(1,1,0,1)
			control:SetAlpha(1)
		elseif GetIngredientSavedVar(index) == 0 then
			control:SetColor(0.7647,0.7647,0.7647,1)
			control:SetAlpha(1)
		end
	elseif option == 3 then
		if IsShiftKeyDown() == false then
			if GetIngredientSavedVar(index) == 1 then
				SetIngredientSavedVar(index, 0)
				CountIngredientTrackingList(0)
			elseif GetIngredientSavedVar(index) == 0 then
				SetIngredientSavedVar(index, 1)
				CountIngredientTrackingList(1)
			end
			ESOMRL.SV[800].junkunmarkedingredients = 0
			ESOMRL.SV[800].destroyunmarkedingredients = 0
			IngredientPanelSetup()
		elseif IsShiftKeyDown() == true then
			local ChatEditControl = CHAT_SYSTEM.textEntry.editControl
			if not ChatEditControl:HasFocus() then 
				StartChatInput() 
			end
			ChatEditControl:InsertText(ESOMRL.IngredientLinks[index].link)
		end
	end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Generate the popup list tooltips.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function ListTooltips(control, text, option)
	if option == 1 then
		if ESOMRL.SV[800].lttshow == 1 then
			local RecipeItemTooltip = RecipeItemTooltipControl
			for i = 1, searchSize do
				local row = searchTable[i].pos
				local name = searchTable[i].text
				if text == name then
					local itemlink
					if ESOMRL.SV[800].tooltipstyle == 0 then
						itemLink = ESOMRL.ProvisioningLinks[row].link
					elseif ESOMRL.SV[800].tooltipstyle == 1 then
						itemLink = ESOMRL.ProvisioningLinks[row].item
					end
					InitializeTooltip(RecipeItemTooltip, ESOMRL_MainFrame, TOPLEFT, 10, -8, TOPRIGHT)
					PopupTooltip.SetLink(RecipeItemTooltip, itemLink)
				end
			end
		end
		control:SetAlpha(.5)
	elseif option == 2 then
		local RecipeItemTooltip = RecipeItemTooltipControl
		ClearTooltip(RecipeItemTooltip)
		control:SetAlpha(1)
	end
end

local function SelectTooltipStyle(option)
	if option == 1 then
		InitializeTooltip(InformationTooltip, ESOMRL_MainFrameListFrame, BOTTOMLEFT, 26, 28, BOTTOMRIGHT)
		if ESOMRL.SV[800].tooltipstyle == 0 then
			SetTooltipText(InformationTooltip, L.ESOMRL_TTFOOD)
		elseif ESOMRL.SV[800].tooltipstyle == 1 then
			SetTooltipText(InformationTooltip, L.ESOMRL_TTRECIPE)
		end
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		if ESOMRL.SV[800].tooltipstyle == 0 then
			ESOMRL.SV[800].tooltipstyle = 1
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, ESOMRL_MainFrameListFrame, BOTTOMLEFT, 26, 28, BOTTOMRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_TTRECIPE)
		elseif ESOMRL.SV[800].tooltipstyle == 1 then
			ESOMRL.SV[800].tooltipstyle = 0
			ClearTooltip(InformationTooltip)
			InitializeTooltip(InformationTooltip, ESOMRL_MainFrameListFrame, BOTTOMLEFT, 26, 28, BOTTOMRIGHT)
			SetTooltipText(InformationTooltip, L.ESOMRL_TTFOOD)
		end
	end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Text search mouse events.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function TextBoxEvents(option)
	local control = ESOMRL_MainFrameSelectionFrameSearchBox
	local controlBG = ESOMRL_MainFrameSelectionFrameSearchBG
	if option == 1 then -- On Mouse Enter
		if editText == 0 or editText == 2 then
			controlBG:SetAlpha(0.5)
			if textInput == '' then
				control:SetText(L.ESOMRL_SEARCHBOX)
			else
				control:SetText(textInput)
			end
		end
	elseif option == 2 then -- On Mouse Exit
		if editText == 0 or editText == 2 then
			controlBG:SetAlpha(0)
			if control:GetText() == L.ESOMRL_SEARCHBOX then
				textInput = L.ESOMRL_SEARCHBOX
			end
			control:SetText('')
		end
	elseif option == 3 then -- On Focus Gained
		control:SetText('')
		textInput = ''
		editText = 1
		openTab = 50
		TextSearch()
	elseif option == 4 then -- On Focus Lost
		editText = 0
		textInput = control:GetText()
		control:SetText('')
		controlBG:SetAlpha(0)
		TextSearch(1)
	elseif option == 5 then -- On Text Changed
		if textInput ~= L.ESOMRL_SEARCHBOX and control:GetText() ~= L.ESOMRL_SEARCHBOX then
			TextSearch()
		end
	end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Handles populating the list of tracked ingredients based on those needed by currently tracked recipes.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function TrackedRecipeCheck()
	local ingredients = {}
	for r = 1, 535 do
		local savedvar = GetRecipeSavedVar(r, 1)
		if (savedvar == 1) or (savedvar == 2) then
			local linkcheck = ESOMRL.ProvisioningLinks[r].link
			local numIngredients = GetItemLinkRecipeNumIngredients(linkcheck)
			for i = 1, numIngredients do
				local ingredientName,_ = GetItemLinkRecipeIngredientInfo(linkcheck, i)
				ingredientName = LowerSpaceSpecial(ingredientName)
				if not Contains(ingredients, ingredientName) then
					table.insert(ingredients, #ingredients + 1, ingredientName)
				end
			end
		end
	end
	for a = 1, #ingredients do
		for b = 1, 50 do
			local namelookup = LowerSpaceSpecial(GetItemLinkName(ESOMRL.IngredientLinks[b].link))
			if namelookup == ingredients[a] then
				if GetIngredientSavedVar(b) == 0 then
					SetIngredientSavedVar(b, 1)
					CountIngredientTrackingList(1)
				end
			end
		end
	end
	IngredientPanelSetup()
end

local function TrackedRecipeIngredients(control, option)
	if option == 1 then
		if GetCVar('Language.2') == 'fr' then
			InitializeTooltip(InformationTooltip, control, TOPLEFT, 10, -65, BOTTOMRIGHT)
		elseif GetCVar('Language.2') == 'de' then
			InitializeTooltip(InformationTooltip, control, TOPLEFT, 10, -65, BOTTOMRIGHT)
		else
			InitializeTooltip(InformationTooltip, control, TOPLEFT, 10, -43, BOTTOMRIGHT)
		end
		SetTooltipText(InformationTooltip, L.ESOMRL_TRACKRING)
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		ESOMRL.SV[800].junkunmarkedingredients = 0
		ESOMRL.SV[800].destroyunmarkedingredients = 0
		SetTrackedIngredientCountText()
		RefreshViews()
		TrackedRecipeCheck()
	end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Setup button functions and handle the junking and deleting of unmarked items as per options.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function JunkUnmarkedRecipeFunction(option)
	if option == 1 then
		InitializeTooltip(InformationTooltip, ESOMRL_MainFrameCloseButton, TOPLEFT, 8, -44, BOTTOMRIGHT)
		if ESOMRL.SV[800].destroyjunkrecipes == true then
			if ESOMRL.SV[800].destroyunmarkedrecipes == 0 then
				SetTooltipText(InformationTooltip, L.ESOMRL_DESTROYURECIPE)
			elseif ESOMRL.SV[800].destroyunmarkedrecipes == 1 then
				SetTooltipText(InformationTooltip, L.ESOMRL_DDESTROYURECIPE)
			end
		elseif ESOMRL.SV[800].destroyjunkrecipes == false then
			if ESOMRL.SV[800].junkunmarkedrecipes == 0 then
				SetTooltipText(InformationTooltip, L.ESOMRL_JUNKURECIPE)
			elseif ESOMRL.SV[800].junkunmarkedrecipes == 1 then
				SetTooltipText(InformationTooltip, L.ESOMRL_DJUNKURECIPE)
			end
		end
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		if ESOMRL.SV[800].destroyjunkrecipes == true then
			if ESOMRL.SV[800].destroyunmarkedrecipes == 0 then
				ESOMRL.SV[800].destroyunmarkedrecipes = 1
				ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton1:SetHidden(true)
				ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton2:SetHidden(false)
				ClearTooltip(InformationTooltip)
				InitializeTooltip(InformationTooltip, ESOMRL_MainFrameCloseButton, TOPLEFT, 8, -44, BOTTOMRIGHT)
				SetTooltipText(InformationTooltip, L.ESOMRL_DDESTROYURECIPE)
			elseif ESOMRL.SV[800].destroyunmarkedrecipes == 1 then
				ESOMRL.SV[800].destroyunmarkedrecipes = 0
				ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton1:SetHidden(false)
				ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton2:SetHidden(true)
				ClearTooltip(InformationTooltip)
				InitializeTooltip(InformationTooltip, ESOMRL_MainFrameCloseButton, TOPLEFT, 8, -44, BOTTOMRIGHT)
				SetTooltipText(InformationTooltip, L.ESOMRL_DESTROYURECIPE)
			end
		elseif ESOMRL.SV[800].destroyjunkrecipes == false then
			if ESOMRL.SV[800].junkunmarkedrecipes == 0 then
				ESOMRL.SV[800].junkunmarkedrecipes = 1
				ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton1:SetHidden(true)
				ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton2:SetHidden(false)
				ClearTooltip(InformationTooltip)
				InitializeTooltip(InformationTooltip, ESOMRL_MainFrameCloseButton, TOPLEFT, 8, -44, BOTTOMRIGHT)
				SetTooltipText(InformationTooltip, L.ESOMRL_DJUNKURECIPE)
			elseif ESOMRL.SV[800].junkunmarkedrecipes == 1 then
				ESOMRL.SV[800].junkunmarkedrecipes = 0
				ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton1:SetHidden(false)
				ESOMRL_MainFrameListFrameBatchTrackingTrashUncheckedButton2:SetHidden(true)
				ClearTooltip(InformationTooltip)
				InitializeTooltip(InformationTooltip, ESOMRL_MainFrameCloseButton, TOPLEFT, 8, -44, BOTTOMRIGHT)
				SetTooltipText(InformationTooltip, L.ESOMRL_JUNKURECIPE)
			end
		end
	end
end

local function JunkUnmarkedIngredientFunction(option)
	if option == 1 then
		if GetCVar('Language.2') == 'fr' then
			InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -87, BOTTOMRIGHT)
		else
			InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -65, BOTTOMRIGHT)
		end
		if ESOMRL.SV[800].destroyjunkingredients == true then
			if ESOMRL.SV[800].destroyunmarkedingredients == 0 then
				SetTooltipText(InformationTooltip, L.ESOMRL_DESTROYUING)
			elseif ESOMRL.SV[800].destroyunmarkedingredients == 1 then
				SetTooltipText(InformationTooltip, L.ESOMRL_DDESTROYUING)
			end
		elseif ESOMRL.SV[800].destroyjunkingredients == false then
			if ESOMRL.SV[800].junkunmarkedingredients == 0 then
				SetTooltipText(InformationTooltip, L.ESOMRL_JUNKUING)
			elseif ESOMRL.SV[800].junkunmarkedingredients == 1 then
				SetTooltipText(InformationTooltip, L.ESOMRL_DJUNKUING)
			end
		end
	elseif option == 2 then
		ClearTooltip(InformationTooltip)
	elseif option == 3 then
		if ESOMRL.SV[800].destroyjunkingredients == true then
			if ESOMRL.SV[800].destroyunmarkedingredients == 0 then
				ESOMRL.SV[800].destroyunmarkedingredients = 1
				ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton1:SetHidden(true)
				ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton2:SetHidden(false)
				ClearTooltip(InformationTooltip)
				if GetCVar('Language.2') == 'fr' then
					InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -85, BOTTOMRIGHT)
				else
					InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -63, BOTTOMRIGHT)
				end
				SetTooltipText(InformationTooltip, L.ESOMRL_DDESTROYUING)
			elseif ESOMRL.SV[800].destroyunmarkedingredients == 1 then
				ESOMRL.SV[800].destroyunmarkedingredients = 0
				ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton1:SetHidden(false)
				ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton2:SetHidden(true)
				ClearTooltip(InformationTooltip)
				if GetCVar('Language.2') == 'fr' then
					InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -85, BOTTOMRIGHT)
				else
					InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -63, BOTTOMRIGHT)
				end
				SetTooltipText(InformationTooltip, L.ESOMRL_DESTROYUING)
			end
		elseif ESOMRL.SV[800].destroyjunkingredients == false then
			if ESOMRL.SV[800].junkunmarkedingredients == 0 then
				ESOMRL.SV[800].junkunmarkedingredients = 1
				ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton1:SetHidden(true)
				ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton2:SetHidden(false)
				ClearTooltip(InformationTooltip)
				if GetCVar('Language.2') == 'fr' then
					InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -85, BOTTOMRIGHT)
				else
					InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -63, BOTTOMRIGHT)
				end
				SetTooltipText(InformationTooltip, L.ESOMRL_DJUNKUING)
			elseif ESOMRL.SV[800].junkunmarkedingredients == 1 then
				ESOMRL.SV[800].junkunmarkedingredients = 0
				ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton1:SetHidden(false)
				ESOMRL_MainFrameIngredientsFrameTrashUncheckedButton2:SetHidden(true)
				ClearTooltip(InformationTooltip)
				if GetCVar('Language.2') == 'fr' then
					InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -85, BOTTOMRIGHT)
				else
					InitializeTooltip(InformationTooltip, ESOMRL_MainFrameIngredientsFrameListTooltipButton, TOPLEFT, 10, -63, BOTTOMRIGHT)
				end
				SetTooltipText(InformationTooltip, L.ESOMRL_JUNKUING)
			end
		end
	end
end

local function OnInventorySlotUpdate(eventCode, bagId, slotId, isNewItem, itemSoundCategory, updateReason)
	if PlayerLoaded == 0 then return end
	if isNewItem ~= true then return end
	if bagId ~= BAG_BACKPACK then return end
	if updateReason ~= INVENTORY_UPDATE_REASON_DEFAULT then return end
	if IsUnderArrest() then return end
	if Roomba and Roomba.WorkInProgress and Roomba.WorkInProgress() then return end
	if IsItemJunk(bagId, slotId) then return end
	local icon, stackCount, sellPrice, meetsUsageRequirements, locked, equipType, itemStyle, quality = GetItemInfo(bagId, slotId)
	if stackCount < 1 then return end
	local itemLink = GetItemLink(bagId, slotId)
	local itemType = GetItemLinkItemType(itemLink)
	local itemId = select(4, ZO_LinkHandler_ParseLink(itemLink))
	if ESOMRL.SV[800].ignorestolen and IsItemLinkStolen(itemLink) then
		if ESOMRL.SV[800].debugmode == true then
			if itemType == ITEMTYPE_INGREDIENT then
				d('\'' .. stackCount .. 'x ' .. GetItemLinkName(itemLink) .. '\' ' .. L.ESOMRL_ISTOLENM)
			elseif itemType == ITEMTYPE_RECIPE then
				d('\'' .. GetItemLinkName(itemLink) .. '\' ' .. L.ESOMRL_ISTOLENM)
			end
		end
	return end
	if itemType == ITEMTYPE_INGREDIENT then
		local key = FindMatch(ESOMRL.IngredientMaterials, itemId)
		if key ~= 0 then
			if itemId == ESOMRL.IngredientLinks[key].id then
				if GetIngredientSavedVar(key) == 0 then
					if ESOMRL.SV[800].destroyjunkingredients == true then
						if ESOMRL.SV[800].destroyunmarkedingredients == 1 then
							if stackCount <= ESOMRL.SV[800].maxjunkstack then
								DestroyItem(bagId, slotId)
								if ESOMRL.SV[800].debugmode == true then
									d('\'' .. stackCount .. 'x ' .. GetItemLinkName(itemLink) .. '\' ' .. L.ESOMRL_IDESTROYM)
								end
							end
						end
					elseif ESOMRL.SV[800].destroyjunkingredients == false then
						if ESOMRL.SV[800].junkunmarkedingredients == 1 then
							if not IsItemJunk(bagId, slotId) then
								if stackCount <= ESOMRL.SV[800].maxjunkstack then	
									SetItemIsJunk(bagId, slotId, true)
									if ESOMRL.SV[800].debugmode == true then
										d('\'' .. stackCount .. 'x ' .. GetItemLinkName(itemLink) .. '\' ' .. L.ESOMRL_IJUNKM)
									end
								end
							end
						end
					end
				end
			end
		end
	elseif itemType == ITEMTYPE_RECIPE then
		local key = FindMatch(ESOMRL.RecipeMaterials, itemId)
		if key ~= 0 then
			local quality = GetItemLinkQuality(itemLink)
			local checktracking = GetRecipeSavedVar(key, 1)
			if itemId == ESOMRL.ProvisioningLinks[key].id then
				if checktracking == 0 or checktracking == 3 then
					if ESOMRL.SV[800].destroyjunkrecipes == true then
						if ESOMRL.SV[800].destroyunmarkedrecipes == 1 then
							if quality < ESOMRL.SV[800].maxjunkquality then
								DestroyItem(bagId, slotId)
								if ESOMRL.SV[800].debugmode == true then
									d('\'' .. GetItemLinkName(itemLink) .. '\' ' .. L.ESOMRL_IDESTROYM)
								end
							end
						end
					elseif ESOMRL.SV[800].destroyjunkrecipes == false then
						if ESOMRL.SV[800].junkunmarkedrecipes == 1 then
							if not IsItemJunk(bagId, slotId) then
								if quality < ESOMRL.SV[800].maxjunkquality then
									SetItemIsJunk(bagId, slotId, true)
									if ESOMRL.SV[800].debugmode == true then
										d('\'' .. GetItemLinkName(itemLink) .. '\' ' .. L.ESOMRL_IJUNKM)
									end
								end
							end
						end
					end
				end
			end
		end
	end
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Set up the Addon Settings options panel.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function CreateSettingsWindow(addonName)
	local panelData = {
		type					= 'panel',
		name					= 'ESO Master Recipe List',
		displayName				= '|cFEE854ESO|r |cFF9900Master Recipe List|r',
		author					= ESOMRL.Author,
		version					= ESOMRL.Version,
		registerForRefresh		= true,
		registerForDefaults		= true,
	}

	local optionsData = {
	{
		type = 'header',
		name = ZO_HIGHLIGHT_TEXT:Colorize(L.ESOMRL_CHAROPT),
	},
	{
		type			= 'checkbox',
		name			= L.ESOMRL_ETRACKING,
		tooltip			= L.ESOMRL_TRACKDESC,
		getFunc			= function() return ESOMRL.SV[800].trackChar end,
		setFunc			= function(value)
							ESOMRL.SV[800].trackChar = value
							InitKnown()
						end,
		warning			= L.ESOMRL_TRACKWARN,
		default			= CharacterDefaults[800].trackChar,
	},
	{
		type			= 'checkbox',
		name			= '|cff0000' .. L.ESOMRL_RDESTROY .. '|r',
		tooltip			= L.ESOMRL_RDESTDESC,
		getFunc			= function() return ESOMRL.SV[800].destroyjunkrecipes end,
		setFunc			= function(value)
							ESOMRL.SV[800].junkunmarkedrecipes = 0
							ESOMRL.SV[800].destroyunmarkedrecipes = 0
							ESOMRL.SV[800].destroyjunkrecipes = value
							RecipePanelSetup()
						end,
		warning			= L.ESOMRL_RDESTROYWARN,
		default			= CharacterDefaults[800].destroyjunkrecipes,
	},
	{
		type			= 'checkbox',
		name			= '|cff0000' .. L.ESOMRL_IDESTROY .. '|r',
		tooltip			= L.ESOMRL_IDESTDESC,
		getFunc			= function() return ESOMRL.SV[800].destroyjunkingredients end,
		setFunc			= function(value)
							ESOMRL.SV[800].junkunmarkedingredients = 0
							ESOMRL.SV[800].destroyunmarkedingredients = 0
							ESOMRL.SV[800].destroyjunkingredients = value
							IngredientPanelSetup()
						end,
		warning			= L.ESOMRL_IDESTROYWARN,
		default			= CharacterDefaults[800].destroyjunkingredients,
	},
	{
		type			= 'checkbox',
		name			= '|c00ff00' .. L.ESOMRL_ISTOLEN .. '|r',
		tooltip			= L.ESOMRL_ISTOLEND,
		getFunc			= function() return ESOMRL.SV[800].ignorestolen end,
		setFunc			= function(value) ESOMRL.SV[800].ignorestolen = value end,
		default			= CharacterDefaults[800].ignorestolen,
	},
	{
		type			= 'checkbox',
		name			= '|cffff00' .. L.ESOMRL_DEBUGMODE .. '|r',
		tooltip			= L.ESOMRL_DEBUGDESC,
		getFunc			= function() return ESOMRL.SV[800].debugmode end,
		setFunc			= function(value) ESOMRL.SV[800].debugmode = value end,
		default			= CharacterDefaults[800].debugmode,
	},
	{
		type			= 'dropdown',
		name			= L.ESOMRL_DQUALITY,
		tooltip			= L.ESOMRL_DQUALITYDESC,
		choices			= qualityCap,
		getFunc			= function() return qualityCap[ESOMRL.SV[800].maxjunkquality - 1] end,
		setFunc			= function(selected)
							for k,v in ipairs(qualityCap) do
								if v == selected then
									ESOMRL.SV[800].maxjunkquality = k + 1
									break
								end
							end
						end,
		default			= qualityCap[CharacterDefaults[800].maxjunkquality],
	},
	{
		type			= 'slider',
		name			= L.ESOMRL_STACKSIZE,
		tooltip			= L.ESOMRL_STACKDESC,
		min				= 1,
		max				= 100,
		step			= 1,
		getFunc			= function() return ESOMRL.SV[800].maxjunkstack end,
		setFunc			= function(value) ESOMRL.SV[800].maxjunkstack = value end,
		default			= CharacterDefaults[800].maxjunkstack,
	},
	{
		type			= 'checkbox',
		name			= L.ESOMRL_TCHART,
		tooltip			= L.ESOMRL_TCHARTD,
		getFunc			= function() return ESOMRL.SV[800].tchart end,
		setFunc			= function(value)
							ESOMRL.SV[800].tchart = value
							RefreshViews()
						end,
		default			= CharacterDefaults[800].tchart,
	},
	{
		type			= 'dropdown',
		name			= L.ESOMRL_TCHAR,
		tooltip			= L.ESOMRL_TCHARD,
		choices			= nameList,
		getFunc			= function() return nameList[ESOMRL.SV[800].trackingchar] end,
		setFunc			= function(selected)
							for k,v in ipairs(nameList) do
								if v == selected then
									ESOMRL.SV[800].trackingchar = k
									break
								end
							end
							RefreshViews()
						end,
		default			= CharacterDefaults[800].trackingchar,
		disabled		= function() return not ESOMRL.SV[800].tchart end,
	},
	{
		type			= 'checkbox',
		name			= L.ESOMRL_ALTIU,
		tooltip			= L.ESOMRL_ALTIUD,
		getFunc			= function() return ESOMRL.SV[800].altknown end,
		setFunc			= function(value)
							ESOMRL.SV[800].altknown = value
							RefreshViews()
						end,
		default			= CharacterDefaults[800].altknown,
		disabled		= function() return not ESOMRL.SV[800].tchart end,
	},
	{
		type			= 'checkbox',
		name			= L.ESOMRL_ALTIS,
		tooltip			= L.ESOMRL_ALTISD,
		getFunc			= function() return ESOMRL.SV[800].altknownself end,
		setFunc			= function(value)
							ESOMRL.SV[800].altknownself = value
							RefreshViews()
						end,
		default			= CharacterDefaults[800].altknownself,
		disabled		= function() return not ESOMRL.SV[800].altknown end,
	},
	{
		type			= 'submenu',
		name			= L.ESOMRL_GLOBALOPT,
		tooltip			= L.ESOMRL_GLOBALOPT,
		controls		= {
						[1] = {
							type = 'checkbox',
							name = L.ESOMRL_SHOWKNOWN,
							tooltip = L.ESOMRL_SHOWKNOWND,
							getFunc = function() return ESOMRL.ASV[800].known end,
							setFunc = function(value) ESOMRL.ASV[800].known = value end,
							width = 'full',
							default = AccountDefaults[800].known,
						},
						[2] = {
							type = 'checkbox',
							name = L.ESOMRL_SHOWINGFOOD,
							tooltip = L.ESOMRL_SHOWINGFOODD,
							getFunc = function() return ESOMRL.ASV[800].ingfood end,
							setFunc = function(value) ESOMRL.ASV[800].ingfood = value end,
							width = 'full',
							default = AccountDefaults[800].ingfood,
						},
						[3] = {
							type			= 'checkbox',
							name			= L.ESOMRL_INVENTORYI,
							tooltip			= L.ESOMRL_INVENTORYD,
							getFunc			= function() return ESOMRL.ASV[800].inventoryicons end,
							setFunc			= function(value)
												ESOMRL.ASV[800].inventoryicons = value
												RefreshViews()
											end,
							default			= AccountDefaults[800].inventoryicons,
						},
						[4] = {
							type			= 'slider',
							name			= L.ESOMRL_ICONPOSI,
							tooltip			= L.ESOMRL_ICONPOSID,
							min				= -380,
							max				= 100,
							step			= 1,
							getFunc			= function() return ESOMRL.ASV[800].bagiconoffset end,
							setFunc			= function(value)
												ESOMRL.ASV[800].bagiconoffset = value
												RefreshViews()
											end,
							default			= AccountDefaults[800].bagiconoffset,
							disabled		= function() return ESOMRL.SV[800].fcoitemsaver end,
						},
						[5] =  {
							type			= 'slider',
							name			= L.ESOMRL_ICONPOSB,
							tooltip			= L.ESOMRL_ICONPOSBD,
							min				= -30,
							max				= 145,
							step			= 1,
							getFunc			= function() return ESOMRL.ASV[800].gstoreiconoffset end,
							setFunc			= function(value)
												ESOMRL.ASV[800].gstoreiconoffset = value
												RefreshViews()
											end,
							default			= AccountDefaults[800].gstoreiconoffset,
						},
						[6] =  {
							type			= 'slider',
							name			= L.ESOMRL_ICONPOSL,
							tooltip			= L.ESOMRL_ICONPOSLD,
							min				= -30,
							max				= 145,
							step			= 1,
							getFunc			= function() return ESOMRL.ASV[800].glistingiconoffset end,
							setFunc			= function(value)
												ESOMRL.ASV[800].glistingiconoffset = value
												RefreshViews()
											end,
							default			= AccountDefaults[800].glistingiconoffset,
						},
						[7] = {
							type			= 'checkbox',
							name			= L.ESOMRL_SSTATS,
							tooltip			= L.ESOMRL_SSTATSD,
							getFunc			= function() return ESOMRL.ASV[800].stationstats end,
							setFunc			= function(value)
												ESOMRL.ASV[800].stationstats = value
											end,
							default			= AccountDefaults[800].stationstats,
						},
						[8] = {
							type			= 'iconpicker',
							name			= L.ESOMRL_SSTATICONS,
							tooltip			= L.ESOMRL_SSTATICONSD,
							choices			= StatOptions,
							choicesTooltips	= StatOptionTooltips,
							getFunc			= function() return StatOptions[ESOMRL.ASV[800].stationicons] end,
							setFunc			= function(texturePath)
												local textureId = GetTextureId(texturePath)
												if textureId ~= 0 then
													ESOMRL.ASV[800].stationicons = textureId
													SwitchStationIcons(textureId)
												end
											end,
							maxColumns		= 2,
							visibleRows		= 1,
							iconSize		= 64,
							default			= StatOptions[AccountDefaults[800].stationicons],
							disabled		= function() return not ESOMRL.ASV[800].stationstats end,
						},
						},
	},
	{
		type			= 'submenu',
		name			= L.ESOMRL_ADDONS,
		tooltip			= L.ESOMRL_ADDONSD,
		controls		= {
						[1] = {
							type			= 'checkbox',
							name			= L.ESOMRL_FCOITEM,
							tooltip			= L.ESOMRL_FCOITEMD,
							getFunc			= function() return ESOMRL.SV[800].fcoitemsaver end,
							setFunc			= function(value)
												ESOMRL.SV[800].fcoitemsaver = value
												RefreshViews()
											end,
							default			= CharacterDefaults[800].fcoitemsaver,
							disabled		= function() return not FCOItemSaverActive end,
						},
						}
	}
	}

	local LAM = LibStub('LibAddonMenu-2.0')
	LAM:RegisterAddonPanel('ESOMRL_Panel', panelData)
	LAM:RegisterOptionControls('ESOMRL_Panel', optionsData)
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Initialize the addon and the interactive recipe scroll list, and set up language strings and formatting.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
local function InitMain()
	local control = GetControl('ESOMRL_MainFrameListFrameList')
	ZO_ScrollList_AddDataType(control, 1 , 'ESOMRL_ListItemTemplate', 20,  SetListItem)
end

local function InitRecipeTooltip(control)
	if not control then return end
	RecipeItemTooltipControl = control
	control:SetParent(PopupTooltipTopLevel)
end

local function InitIngredientTooltip(control)
	if not control then return end
	IngredientItemTooltipControl = control
	control:SetParent(PopupTooltipTopLevel)
end

local function InitQualityDatabase()
	qualityCap[1] = '|c00ff00' .. L.ESOMRL_QUALITY1 .. '|r'
	qualityCap[2] = '|c3a92ff' .. L.ESOMRL_QUALITY2 .. '|r'
	qualityCap[3] = '|ca02ef7' .. L.ESOMRL_QUALITY3 .. '|r'
end

local function ShowMain()
	ESOMRL:SetLanguageFormat()
	local control = GetControl('ESOMRL_MainFrame')
	if ( control:IsHidden() ) then
		SCENE_MANAGER:ToggleTopLevel(ESOMRL_MainFrame)
		RestorePosition()
		openTab = 1
		NavigateScrollList(1)
	else
		SCENE_MANAGER:ToggleTopLevel(ESOMRL_MainFrame)
	end
	SetTrackedRecipeCountText()
	RecipePanelRestore()
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Global function to check if MRL is tracking a recipe or ingredient.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ESOMRL.ISMRLTracking(itemLink)
	if itemLink ~= nil then
		local itemIdT = {select(4, ZO_LinkHandler_ParseLink(itemLink))}
		local itemId = tonumber(itemIdT[1])
		if ESOMRL.IngredientMaterials[itemId] ~= nil then
			local key = ESOMRL.IngredientMaterials[itemId].position
			if GetIngredientSavedVar(key) == 1 then
				return true
			end
		elseif ESOMRL.RecipeMaterials[itemId] ~= nil then
			local key = ESOMRL.RecipeMaterials[itemId].position
			local checktracking = GetRecipeSavedVar(key, 1)
			if checktracking == 1 or checktracking == 2 then
				return true
			end
		end
	end
	return false
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Handle function calls from XML.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
function ESOMRL.XMLNavigation(option, control, text, n1, n2)
-- List click and tooltips.
	if option == 001 then
		ListTooltips(control, text, n1)
	elseif option == 002 then
		RecipeListClick(text)
-- Main Frame functions.
	elseif option == 101 then
		OnMoveStop()
	elseif option == 102 then
		InitMain()
	elseif option == 103 then
		InitRecipeTooltip(control)
	elseif option == 104 then
		InitIngredientTooltip(control)
	elseif option == 105 then
		CloseMain(control, n1)
	elseif option == 106 then
		ToggleTooltips(control, n1)
	elseif option == 107 then
		InfoTooltip(n1)
	elseif option == 108 then
		SwitchPage(control, n1)
-- Ingredient Frame functions.
	elseif option == 201 then
		IngredientListClick(control, n1, n2)
	elseif option == 202 then
		ClearTrackedIngredients()
	elseif option == 203 then
		TrackedRecipeIngredients(control, n1)
	elseif option == 204 then
		SelectAllIngredients(control, n1)
	elseif option == 205 then
		JunkUnmarkedIngredientFunction(n1)
	elseif option == 206 then
		SaveGlobalIngredientProfile(n1)
	elseif option == 207 then
		LoadGlobalIngredientProfile(n1)
	elseif option == 208 then
		FindIngredientRecipes(n1)
-- Left side navigation.
	elseif option == 301 then
		ClearTrackedRecipes()
	elseif option == 302 then
		WritButton(control, n1)
	elseif option == 303 then
		GetWrits()
	elseif option == 304 then
		if IsShiftKeyDown() == true then
			TrackNavigationTier(n1)
		end
		NavigateScrollList(n1)
	elseif option == 305 then
		SelectTooltipStyle(n1)
	elseif option == 306 then
		RecipeOptionPanel(n1)
-- Text search options
	elseif option == 401 then
		TextBoxEvents(n1)
-- Open Main Window
	elseif option == 501 then
		ShowMain()
-- Recipe config menu functions.
	elseif option == 601 then
		TrackUnknownRecipes(n1)
	elseif option == 602 then
		TrackAllRecipes(n1)
	elseif option == 603 then
		JunkUnmarkedRecipeFunction(n1)
	elseif option == 604 then
		RemoveCharacter(n1)
-- Cooking station controls.
	elseif option == 701 then
		StationButtonTooltip(control, n1)
	elseif option == 702 then
		HighlightTracked(control, n1)
	elseif option == 703 then
		ToggleStationTooltips(control, n1)
	elseif option == 704 then
		StationSearch(control, n1)
	end
end

local function Init(eventCode) PlayerLoaded = 1 end
local function OnAddonLoaded(event, addonName)
	if addonName ~= ESOMRL.Name then return end
	EVENT_MANAGER:UnregisterForEvent(ESOMRL.Name, EVENT_ADD_ON_LOADED)
	SCENE_MANAGER:RegisterTopLevel(ESOMRL_MainFrame, false)
	ESOMRL.SV = ZO_SavedVars:New(ESOMRL.Name, 1.37, 'CharacterSettings', CharacterDefaults)
	ESOMRL.ASV = ZO_SavedVars:NewAccountWide(ESOMRL.Name, 1.37, 'AccountSettings', AccountDefaults)
	ZO_CreateStringId('SI_BINDING_NAME_TOGGLE_RECIPE_WINDOW', 'Toggle Recipe Window')
	FCOItemSaverActive = (FCOIsMarked ~= nil)
	if not FCOItemSaverActive then ESOMRL.SV[800].fcoitemsaver = false end
	SetupCharacterDropbox()
	InitQualityDatabase()
	HookBags()
	HookTooltips()
	HookStation()
	InitKnown()
	StationWritCheck()
	CreateSettingsWindow(addonName)
end

SLASH_COMMANDS['/mrl'] = ShowMain
SLASH_COMMANDS['/recipes'] = ShowMain
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_PLAYER_ACTIVATED, Init)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_ADD_ON_LOADED, OnAddonLoaded)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_RECIPE_LEARNED, InitKnown)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_INVENTORY_SINGLE_SLOT_UPDATE, OnInventorySlotUpdate)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_TRADING_HOUSE_RESPONSE_RECEIVED, HookTradeHouse)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_OPEN_STORE, HookBuyback)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_QUEST_ADDED, OnQuestAdded)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_QUEST_REMOVED, OnQuestRemoved)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_QUEST_COMPLETE, OnQuestComplete)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_QUEST_CONDITION_COUNTER_CHANGED, OnQuestConditionCounterChanged)
EVENT_MANAGER:RegisterForEvent(ESOMRL.Name, EVENT_CRAFTING_STATION_INTERACT, OnCraftingStationInteract)
