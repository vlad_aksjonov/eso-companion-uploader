local ESOMRL = _G['ESOMRL']

ESOMRL.ProvisioningLinksDrinks2 = {

[1] = {position = "308", level = "(Level 1 Drink)", link = "|H1:item:45996:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28441:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[2] = {position = "309", level = "(Level 1 Drink)", link = "|H1:item:46006:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33648:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[3] = {position = "310", level = "(Level 1 Drink)", link = "|H1:item:46014:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33981:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[4] = {position = "311", level = "(Level 5 Drink)", link = "|H1:item:46007:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33654:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[5] = {position = "312", level = "(Level 5 Drink)", link = "|H1:item:46015:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33987:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[6] = {position = "313", level = "(Level 5 Drink)", link = "|H1:item:45997:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28445:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[7] = {position = "314", level = "(Level 10 Drink)", link = "|H1:item:45998:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28449:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[8] = {position = "315", level = "(Level 10 Drink)", link = "|H1:item:46008:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33660:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[9] = {position = "316", level = "(Level 10 Drink)", link = "|H1:item:46016:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33993:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[10] = {position = "317", level = "(Level 15 Drink)", link = "|H1:item:46017:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33999:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[11] = {position = "318", level = "(Level 15 Drink)", link = "|H1:item:46009:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33666:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[12] = {position = "319", level = "(Level 15 Drink)", link = "|H1:item:45999:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28453:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[13] = {position = "320", level = "(Level 20 Drink)", link = "|H1:item:46000:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28457:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[14] = {position = "321", level = "(Level 20 Drink)", link = "|H1:item:46018:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34005:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[15] = {position = "322", level = "(Level 20 Drink)", link = "|H1:item:46010:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33672:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[16] = {position = "323", level = "(Level 25 Drink)", link = "|H1:item:46011:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33678:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[17] = {position = "324", level = "(Level 25 Drink)", link = "|H1:item:46019:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34011:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[18] = {position = "325", level = "(Level 25 Drink)", link = "|H1:item:46001:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28461:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[19] = {position = "326", level = "(Level 30 Drink)", link = "|H1:item:46002:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28465:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[20] = {position = "327", level = "(Level 30 Drink)", link = "|H1:item:46012:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33684:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[21] = {position = "328", level = "(Level 30 Drink)", link = "|H1:item:46020:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34017:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[22] = {position = "329", level = "(Level 35 Drink)", link = "|H1:item:46021:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34023:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[23] = {position = "330", level = "(Level 35 Drink)", link = "|H1:item:46003:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28469:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[24] = {position = "331", level = "(Level 35 Drink)", link = "|H1:item:46013:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33690:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[25] = {position = "332", level = "(Level 40 Drink)", link = "|H1:item:46004:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28473:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[26] = {position = "333", level = "(Level 40 Drink)", link = "|H1:item:57021:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57159:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[27] = {position = "334", level = "(Level 40 Drink)", link = "|H1:item:57022:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57160:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[28] = {position = "335", level = "(Level 45 Drink)", link = "|H1:item:46005:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28477:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[29] = {position = "336", level = "(Level 45 Drink)", link = "|H1:item:57033:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57171:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[30] = {position = "337", level = "(Level 45 Drink)", link = "|H1:item:57034:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57172:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[31] = {position = "338", level = "(Veteran 1 Drink)", link = "|H1:item:46049:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33602:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[32] = {position = "339", level = "(Veteran 1 Drink)", link = "|H1:item:57045:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57183:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[33] = {position = "340", level = "(Veteran 1 Drink)", link = "|H1:item:57046:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57184:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[34] = {position = "341", level = "(Veteran 5 Drink)", link = "|H1:item:46055:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:46061:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[35] = {position = "342", level = "(Veteran 5 Drink)", link = "|H1:item:46052:1:36:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28482:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[36] = {position = "343", level = "(Veteran 5 Drink)", link = "|H1:item:57057:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57195:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[37] = {position = "344", level = "(Veteran 10 Drink)", link = "|H1:item:57065:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57203:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[38] = {position = "345", level = "(Veteran 10 Drink)", link = "|H1:item:57066:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57204:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[39] = {position = "346", level = "(Veteran 10 Drink)", link = "|H1:item:57067:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57205:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[40] = {position = "347", level = "(Veteran 15 Drink)", link = "|H1:item:68214:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:68258:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[41] = {position = "348", level = "(Veteran 15 Drink)", link = "|H1:item:68215:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:68259:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[42] = {position = "349", level = "(Veteran 15 Drink)", link = "|H1:item:68216:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:68260:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0}
}
