local ESOMRL = _G['ESOMRL']

ESOMRL.ProvisioningLinksFood4 = {

[1] = {position = "127", level = "(Level 10 Food)", link = "|H1:item:45636:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28348:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[2] = {position = "128", level = "(Level 10 Food)", link = "|H1:item:45675:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28373:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[3] = {position = "129", level = "(Level 10 Food)", link = "|H1:item:45654:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28299:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[4] = {position = "130", level = "(Level 15 Food)", link = "|H1:item:45639:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28360:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[5] = {position = "131", level = "(Level 15 Food)", link = "|H1:item:45678:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28385:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[6] = {position = "132", level = "(Level 15 Food)", link = "|H1:item:45657:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28311:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[7] = {position = "133", level = "(Level 20 Food)", link = "|H1:item:45681:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28397:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[8] = {position = "134", level = "(Level 20 Food)", link = "|H1:item:45642:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:42808:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[9] = {position = "135", level = "(Level 20 Food)", link = "|H1:item:45660:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33480:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[10] = {position = "136", level = "(Level 25 Food)", link = "|H1:item:45685:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33567:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[11] = {position = "137", level = "(Level 25 Food)", link = "|H1:item:45646:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:42793:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[12] = {position = "138", level = "(Level 25 Food)", link = "|H1:item:45664:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33505:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[13] = {position = "139", level = "(Level 30 Food)", link = "|H1:item:45688:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33585:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[14] = {position = "140", level = "(Level 30 Food)", link = "|H1:item:45649:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33846:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[15] = {position = "141", level = "(Level 30 Food)", link = "|H1:item:45667:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33523:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[16] = {position = "142", level = "(Level 35 Food)", link = "|H1:item:45672:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33804:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[17] = {position = "143", level = "(Level 35 Food)", link = "|H1:item:45693:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33889:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[18] = {position = "144", level = "(Level 35 Food)", link = "|H1:item:45652:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33864:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[19] = {position = "145", level = "(Level 40 Food)", link = "|H1:item:45699:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:43158:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[20] = {position = "146", level = "(Level 40 Food)", link = "|H1:item:56955:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57092:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[21] = {position = "147", level = "(Level 40 Food)", link = "|H1:item:56956:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57093:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[22] = {position = "148", level = "(Level 45 Food)", link = "|H1:item:45705:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33913:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[23] = {position = "149", level = "(Level 45 Food)", link = "|H1:item:56968:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57105:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[24] = {position = "150", level = "(Level 45 Food)", link = "|H1:item:56969:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57106:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[25] = {position = "151", level = "(Veteran 1 Food)", link = "|H1:item:45711:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33931:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[26] = {position = "152", level = "(Veteran 1 Food)", link = "|H1:item:56981:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57118:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[27] = {position = "153", level = "(Veteran 1 Food)", link = "|H1:item:56982:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57119:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[28] = {position = "154", level = "(Veteran 5 Food)", link = "|H1:item:45717:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:43097:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[29] = {position = "155", level = "(Veteran 5 Food)", link = "|H1:item:56993:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57130:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[30] = {position = "156", level = "(Veteran 5 Food)", link = "|H1:item:56994:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57131:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[31] = {position = "157", level = "(Veteran 10 Food)", link = "|H1:item:57007:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57144:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[32] = {position = "158", level = "(Veteran 10 Food)", link = "|H1:item:57008:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57145:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[33] = {position = "159", level = "(Veteran 10 Food)", link = "|H1:item:57009:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57146:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[34] = {position = "160", level = "(Veteran 15 Food)", link = "|H1:item:68198:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:68242:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[35] = {position = "161", level = "(Veteran 15 Food)", link = "|H1:item:68199:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:68243:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[36] = {position = "162", level = "(Veteran 15 Food)", link = "|H1:item:68200:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:68244:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0}
}
