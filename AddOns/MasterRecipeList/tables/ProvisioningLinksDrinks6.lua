local ESOMRL = _G['ESOMRL']

ESOMRL.ProvisioningLinksDrinks6 = {

[1] = {position = "464", level = "(Level 10 Drink)", link = "|H1:item:45607:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33711:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[2] = {position = "465", level = "(Level 10 Drink)", link = "|H1:item:45614:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34044:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[3] = {position = "466", level = "(Level 10 Drink)", link = "|H1:item:45594:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28492:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[4] = {position = "467", level = "(Level 15 Drink)", link = "|H1:item:45608:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33717:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[5] = {position = "468", level = "(Level 15 Drink)", link = "|H1:item:45615:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34050:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[6] = {position = "469", level = "(Level 15 Drink)", link = "|H1:item:45595:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28496:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[7] = {position = "470", level = "(Level 20 Drink)", link = "|H1:item:45616:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34056:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[8] = {position = "471", level = "(Level 20 Drink)", link = "|H1:item:45609:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33723:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[9] = {position = "472", level = "(Level 20 Drink)", link = "|H1:item:45596:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28500:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[10] = {position = "473", level = "(Level 25 Drink)", link = "|H1:item:45617:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34062:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[11] = {position = "474", level = "(Level 25 Drink)", link = "|H1:item:45610:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33729:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[12] = {position = "475", level = "(Level 25 Drink)", link = "|H1:item:45597:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28504:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[13] = {position = "476", level = "(Level 30 Drink)", link = "|H1:item:45598:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28508:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[14] = {position = "477", level = "(Level 30 Drink)", link = "|H1:item:45618:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34068:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[15] = {position = "478", level = "(Level 30 Drink)", link = "|H1:item:45611:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33735:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[16] = {position = "479", level = "(Level 35 Drink)", link = "|H1:item:45600:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28512:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[17] = {position = "480", level = "(Level 35 Drink)", link = "|H1:item:45612:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33741:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[18] = {position = "481", level = "(Level 35 Drink)", link = "|H1:item:45619:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34074:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[19] = {position = "482", level = "(Level 40 Drink)", link = "|H1:item:45602:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:28516:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[20] = {position = "483", level = "(Level 40 Drink)", link = "|H1:item:57030:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57168:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[21] = {position = "484", level = "(Level 40 Drink)", link = "|H1:item:57031:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57169:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[22] = {position = "485", level = "(Level 45 Drink)", link = "|H1:item:45576:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33458:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[23] = {position = "486", level = "(Level 45 Drink)", link = "|H1:item:57042:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57180:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[24] = {position = "487", level = "(Level 45 Drink)", link = "|H1:item:57043:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57181:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[25] = {position = "488", level = "(Veteran 1 Drink)", link = "|H1:item:45624:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:33650:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[26] = {position = "489", level = "(Veteran 1 Drink)", link = "|H1:item:57054:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57192:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[27] = {position = "490", level = "(Veteran 1 Drink)", link = "|H1:item:57055:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57193:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[28] = {position = "491", level = "(Veteran 5 Drink)", link = "|H1:item:45629:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:34030:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[29] = {position = "492", level = "(Veteran 5 Drink)", link = "|H1:item:45535:1:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:46060:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[30] = {position = "493", level = "(Veteran 5 Drink)", link = "|H1:item:57061:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57199:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[31] = {position = "494", level = "(Veteran 10 Drink)", link = "|H1:item:57077:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57215:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[32] = {position = "495", level = "(Veteran 10 Drink)", link = "|H1:item:57078:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57216:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[33] = {position = "496", level = "(Veteran 10 Drink)", link = "|H1:item:57079:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:57217:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[34] = {position = "497", level = "(Veteran 15 Drink)", link = "|H1:item:68226:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:68270:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[35] = {position = "498", level = "(Veteran 15 Drink)", link = "|H1:item:68227:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:68271:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0},
[36] = {position = "499", level = "(Veteran 15 Drink)", link = "|H1:item:68228:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", item = "|H1:item:68272:3:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", writ = 0, track = 0}
}
