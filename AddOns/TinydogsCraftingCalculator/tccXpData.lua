-- TinydogsCraftingCalculator Crafting XP Data LUA File
-- Last Updated December 2, 2015
-- Created October 2015 by @tinydog - tinydog1234@hotmail.com

local skillType
skillType, TCC_SKILL_INDEX_ALCHEMY = GetCraftingSkillLineIndices(CRAFTING_TYPE_ALCHEMY)
skillType, TCC_SKILL_INDEX_BLACKSMITHING = GetCraftingSkillLineIndices(CRAFTING_TYPE_BLACKSMITHING)
skillType, TCC_SKILL_INDEX_CLOTHING = GetCraftingSkillLineIndices(CRAFTING_TYPE_CLOTHIER)
skillType, TCC_SKILL_INDEX_ENCHANTING = GetCraftingSkillLineIndices(CRAFTING_TYPE_ENCHANTING)
skillType, TCC_SKILL_INDEX_PROVISIONING = GetCraftingSkillLineIndices(CRAFTING_TYPE_PROVISIONING)
skillType, TCC_SKILL_INDEX_WOODWORKING = GetCraftingSkillLineIndices(CRAFTING_TYPE_WOODWORKING)
skillType = nil

function tcc_InitCraftingData()
	tcc.TexturePaths["Create"] = "/esoui/art/crafting/smithing_tabicon_creation_%.dds"
	tcc.TexturePaths["Deconstruct"] = "/esoui/art/crafting/enchantment_tabicon_deconstruction_%.dds"

	-- Same for all crafts
	tcc.CraftingXpNeededForSkillLevel = {
		0, 640, 3995, 6920, 8900, 10560, 13040, 16360, 17960, 21880,                    -- 1 - 10
		24400, 26900, 29400, 30200, 31000, 32600, 34200, 35800, 39610, 43420,           -- 11 - 20
		45750, 48080, 50500, 52920, 55560, 58200, 61280, 64360, 67660, 70960,           -- 21 - 30
		74700, 78440, 82400, 86360, 90980, 95600, 100440, 105280, 116280, 128600,       -- 31 - 40
		135200, 141800, 149280, 156760, 164680, 173040, 181840, 191080, 200760, 211320  -- 41 - 50
	}
	
	-- tcc_CraftObj(craftName, craftType, optimalItemType, skillIndex)
	tcc.Crafts = {
		["Blacksmithing"] = tcc_CraftObj("Blacksmithing", "Equipment", "Dagger", TCC_SKILL_INDEX_BLACKSMITHING),
		["Clothing"] = tcc_CraftObj("Clothing", "Equipment", "Shoes", TCC_SKILL_INDEX_CLOTHING),
		["Leatherworking"] = tcc_CraftObj("Leatherworking", "Equipment", "Boots", TCC_SKILL_INDEX_CLOTHING),
		["Woodworking"] = tcc_CraftObj("Woodworking", "Equipment", "Bow", TCC_SKILL_INDEX_WOODWORKING),
		["Enchanting"] = tcc_CraftObj("Enchanting", "Consumables", "", TCC_SKILL_INDEX_ENCHANTING),
		--["Alchemy"] = tcc_CraftObj("Alchemy", "Consumables", TCC_SKILL_INDEX_ALCHEMY),
		--["Provisioning"] = tcc_CraftObj("Provisioning", "Consumables", TCC_SKILL_INDEX_PROVISIONING),
	}
	
	-- (nameGeneric, name, description, texture, rankAvailableAtSkillLevelArray)
	tcc.Crafts["Blacksmithing"].Passives = {
		tcc_CraftingPassiveObj("Proficiency", "Metalworking", "", "/esoui/art/icons/ability_smith_007.dds", 
			{1, 5, 10, 15, 20, 25, 30, 35, 40, 50}),
		tcc_CraftingPassiveObj("Keen Eye", "Keen Eye: Ore", "", "/esoui/art/icons/ability_smith_002.dds", 
			{2, 9, 30}),
		tcc_CraftingPassiveObj("Hireling", "Miner Hireling", "", "/esoui/art/icons/ability_smith_006.dds", 
			{3, 12, 32}),
		tcc_CraftingPassiveObj("Extraction", "Metal Extraction", "", "/esoui/art/icons/ability_smith_003.dds", 
			{4, 22, 32}),
		tcc_CraftingPassiveObj("Research", "Metallurgy", "", "/esoui/art/icons/crafting_runecrafter_armor_vendor_component_002.dds", 
			{8, 18, 28, 42}), -- Verify 42 vs. 45
		tcc_CraftingPassiveObj("Improvement", "Temper Expertise", "", "/esoui/art/icons/ability_smith_004.dds", 
			{10, 25, 40}),
	}
	tcc.Crafts["Clothing"].Passives = {
		tcc_CraftingPassiveObj("Proficiency", "Tailoring", "", "/esoui/art/icons/ability_tradecraft_008.dds", 
			{1, 5, 10, 15, 20, 25, 30, 35, 40, 50}),
		tcc_CraftingPassiveObj("Keen Eye", "Keen Eye: Cloth", "", "/esoui/art/icons/ability_smith_002.dds", 
			{2, 9, 30}),
		tcc_CraftingPassiveObj("Hireling", "Outfitter Hireling", "", "/esoui/art/icons/ability_tradecraft_007.dds", 
			{3, 12, 32}),
		tcc_CraftingPassiveObj("Extraction", "Unraveling", "", "/esoui/art/icons/ability_tradecraft_005.dds", 
			{4, 22, 32}),
		tcc_CraftingPassiveObj("Research", "Stitching", "", "/esoui/art/icons/crafting_light_armor_component_004.dds", 
			{8, 18, 28, 45}),
		tcc_CraftingPassiveObj("Improvement", "Tannin Expertise", "", "/esoui/art/icons/ability_tradecraft_004.dds", 
			{10, 25, 40}),
	}
	tcc.Crafts["Leatherworking"].Passives = tcc.Crafts["Clothing"].Passives
	tcc.Crafts["Woodworking"].Passives = {
		tcc_CraftingPassiveObj("Proficiency", "Woodworking", "", "/esoui/art/icons/ability_tradecraft_009.dds", 
			{1, 5, 10, 15, 20, 25, 30, 35, 40, 50}),
		tcc_CraftingPassiveObj("Keen Eye", "Keen Eye: Wood", "", "/esoui/art/icons/ability_smith_002.dds", 
			{2, 9, 30}),
		tcc_CraftingPassiveObj("Hireling", "Lumberjack Hireling", "", "/esoui/art/icons/ability_tradecraft_007.dds", 
			{3, 12, 32}),
		tcc_CraftingPassiveObj("Extraction", "Wood Extraction", "", "/esoui/art/icons/ability_tradecraft_006.dds", 
			{4, 22, 32}),
		tcc_CraftingPassiveObj("Research", "Carpentry", "", "/esoui/art/icons/crafting_forester_plug_component_002.dds", 
			{8, 18, 28, 45}), -- 45 vs. 42
		tcc_CraftingPassiveObj("Improvement", "Resin Expertise", "", "/esoui/art/icons/ability_tradecraft_001.dds", 
			{10, 25, 40}),
	}
	tcc.Crafts["Enchanting"].Passives = {
		tcc_CraftingPassiveObj("Research", "Aspect Improvement", "", "/esoui/art/icons/ability_enchanter_002b.dds", 
			{1, 6, 16, 31}),
		tcc_CraftingPassiveObj("Proficiency", "Potency Improvement", "", "/esoui/art/icons/ability_enchanter_001b.dds", 
			{1, 5, 10, 15, 20, 25, 30, 35, 40, 50}),
		tcc_CraftingPassiveObj("Keen Eye", "Keen Eye: Rune Stones", "", "/esoui/art/icons/ability_smith_002.dds", 
			{2, 7, 14}),
		tcc_CraftingPassiveObj("Hireling", "Hireling", "", "/esoui/art/icons/ability_enchanter_008.dds", 
			{3, 12, 32}),
		tcc_CraftingPassiveObj("Extraction", "Runestone Extraction", "", "/esoui/art/icons/ability_enchanter_004.dds", 
			{4, 19, 29}),
	}

	-- skillTier, optimalItemLvl, createXp, deconstructXpWhite, deconstructXpGreen, deconstructXpBlue, deconstructXpPurple, 
	--   itemLinkWhite, itemLinkGreen, itemLinkBlue, itemLinkPurple
	tcc.Crafts["Blacksmithing"].CraftingXp = {
		[1] =  tcc_CraftingXpObj(1, "6", 127, 272, 347,     435, 	531, 	"|H1:item:43535:20:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",   "|H1:item:43535:21:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43535:22:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:23:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
		[2] =  tcc_CraftingXpObj(2, "16", 258, 553, 674,    752, 	918, 	"|H1:item:43535:20:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43535:21:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43535:22:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:23:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
		[3] =  tcc_CraftingXpObj(3, "26", 445, 955, 1065,   1189, 	1330, 	"|H1:item:43535:20:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43535:21:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43535:22:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:23:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
		[4] =  tcc_CraftingXpObj(4, "36", 646, 1384, 1548,  1743, 	1957, 	"|H1:item:43535:20:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43535:21:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43535:22:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:23:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
		[5] =  tcc_CraftingXpObj(5, "46", 951, 2040, 2293,  2588, 	2919, 	"|H1:item:43535:20:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43535:21:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43535:22:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:23:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
		[6] =  tcc_CraftingXpObj(6, "V1", 1116, 2391, 2713, 3039, 	3310, 	"|H1:item:43535:125:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:135:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:145:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:155:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
		[7] =  tcc_CraftingXpObj(7, "V4", 1362, 2919, 3310, 3764, 	4100, 	"|H1:item:43535:128:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:138:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:148:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:158:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
		[8] =  tcc_CraftingXpObj(8, "V7", 1612, 3455, 3928, 4466, 	4865, 	"|H1:item:43535:131:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:141:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:151:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:161:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
		[9] =  tcc_CraftingXpObj(9, "V9", 1833, 3928, 4466, 5078, 	5532, 	"|H1:item:43535:133:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:143:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:153:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:163:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
		[10] = tcc_CraftingXpObj(10, "V15", 0, 5773, 6564,  7462, 	8130, 	"|H1:item:43535:308:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:309:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43535:310:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", 	"|H1:item:43535:311:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"),
	}
	tcc.Crafts["Clothing"].CraftingXp = {
		[1] =  tcc_CraftingXpObj(1, "14", 268, 576, 670,     811, 	895, 	"|H1:item:43544:20:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",   "|H1:item:43544:21:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43544:22:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",   "|H1:item:43544:23:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"   ),
		[2] =  tcc_CraftingXpObj(2, "16", 301, 646, 782,     867, 	1053, 	"|H1:item:43544:20:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43544:21:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43544:22:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43544:23:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"  ),
		[3] =  tcc_CraftingXpObj(3, "26", 510, 1094, 1212,   1347, 	1500, 	"|H1:item:43544:20:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43544:21:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43544:22:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43544:23:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"  ),
		[4] =  tcc_CraftingXpObj(4, "36", 727, 1559, 1737,   1948, 	2180, 	"|H1:item:43544:20:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43544:21:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43544:22:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43544:23:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"  ),
		[5] =  tcc_CraftingXpObj(5, "46", 1059, 2269, 2543,  2860, 	3217, 	"|H1:item:43544:20:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43544:21:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43544:22:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43544:23:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"  ),
		[6] =  tcc_CraftingXpObj(6, "V1", 1235, 2648, 2974,  3346, 	3632, 	"|H1:item:43544:125:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:135:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43544:145:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:155:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h" ),
		[7] =  tcc_CraftingXpObj(7, "V4", 1501, 3217, 3632,  4108, 	4461, 	"|H1:item:43544:128:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:138:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43544:148:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:158:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h" ),
		[8] =  tcc_CraftingXpObj(8, "V8", 1917, 4108, 4647,  5256, 	5706, 	"|H1:item:43544:132:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:141:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43544:152:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:162:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h" ),
		[9] =  tcc_CraftingXpObj(9, "V10", 2168, 4647, 5256, 5946, 	6454, 	"|H1:item:43544:134:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:143:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43544:154:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:164:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h" ),
		[10] = tcc_CraftingXpObj(10, "V15", 0, 6195, 7006,   7925, 	8604, 	"|H1:item:43544:308:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:309:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43544:310:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43544:311:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h" ),
	}
	tcc.Crafts["Leatherworking"].CraftingXp = {
		[1] =  tcc_CraftingXpObj(1, "14", 268, 576, 670,     811, 	895, 	"|H1:item:43551:20:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",   "|H1:item:43551:21:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43551:22:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	 , "|H1:item:43551:23:6:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"),
		[2] =  tcc_CraftingXpObj(2, "16", 301, 646, 782,     867, 	1053, 	"|H1:item:43551:20:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43551:21:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43551:22:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h" , "|H1:item:43551:23:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"),
		[3] =  tcc_CraftingXpObj(3, "26", 510, 1094, 1212,   1347, 	1500, 	"|H1:item:43551:20:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43551:21:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43551:22:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h" , "|H1:item:43551:23:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"),
		[4] =  tcc_CraftingXpObj(4, "36", 727, 1559, 1737,   1948, 	2180, 	"|H1:item:43551:20:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43551:21:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43551:22:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h" , "|H1:item:43551:23:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"),
		[5] =  tcc_CraftingXpObj(5, "46", 1059, 2269, 2543,  2860, 	3217, 	"|H1:item:43551:20:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h",  "|H1:item:43551:21:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"	, "|H1:item:43551:22:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h" , "|H1:item:43551:23:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h"),
		[6] =  tcc_CraftingXpObj(6, "V1", 1235, 2648, 2974,  3346, 	3632, 	"|H1:item:43551:125:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43551:135:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:145:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:155:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h"),
		[7] =  tcc_CraftingXpObj(7, "V4", 1501, 3217, 3632,  4108, 	4461, 	"|H1:item:43551:128:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43551:138:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:148:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:158:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h"),
		[8] =  tcc_CraftingXpObj(8, "V8", 1917, 4108, 4647,  5256, 	5706, 	"|H1:item:43551:132:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43551:141:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:151:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:161:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h"),
		[9] =  tcc_CraftingXpObj(9, "V10", 2168, 4647, 5256, 5946, 	6454, 	"|H1:item:43551:134:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43551:143:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:153:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:163:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h"),
		[10] = tcc_CraftingXpObj(10, "V15", 0, 6195, 7006,   7925, 	8604, 	"|H1:item:43551:308:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:10000:0|h|h", "|H1:item:43551:309:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:310:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h", "|H1:item:43551:311:50:0:0:0:0:0:0:0:0:0:0:0:0:1:1:0:0:10000:0|h|h"),
	}
	tcc.Crafts["Woodworking"].CraftingXp = {
		[1] =  tcc_CraftingXpObj(1, "4", 214, 459, 548,     702, 	845, 	"|H1:item:43549:25:4:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",   "|H1:item:43549:26:4:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43549:27:4:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",   "|H1:item:43549:28:4:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"   ),
		[2] =  tcc_CraftingXpObj(2, "16", 477, 1023, 1207,  1311, 	1560, 	"|H1:item:43549:20:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43549:21:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43549:22:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43549:23:16:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"  ),
		[3] =  tcc_CraftingXpObj(3, "26", 751, 1609, 1755,  1921, 	2110, 	"|H1:item:43549:20:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43549:21:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43549:22:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43549:23:26:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"  ),
		[4] =  tcc_CraftingXpObj(4, "36", 1018, 2183, 2403, 2664, 	2949, 	"|H1:item:43549:20:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43549:21:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43549:22:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43549:23:36:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"  ),
		[5] =  tcc_CraftingXpObj(5, "46", 1427, 3059, 3397, 3787, 	4225, 	"|H1:item:43549:20:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43549:21:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"	, "|H1:item:43549:22:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",  "|H1:item:43549:23:46:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h"  ),
		[6] =  tcc_CraftingXpObj(6, "V1", 1646, 3528, 3927, 4383, 	4736, 	"|H1:item:43549:125:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:135:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:145:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:155:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h" ),
		[7] =  tcc_CraftingXpObj(7, "V4", 1971, 4225, 4736, 5317, 	5744, 	"|H1:item:43549:128:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:138:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:148:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:158:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h" ),
		[8] =  tcc_CraftingXpObj(8, "V7", 2297, 4922, 5526, 6204, 	6729, 	"|H1:item:43549:131:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:141:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:151:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:161:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h" ),
		[9] =  tcc_CraftingXpObj(9, "V9", 2578, 5526, 6204, 6966, 	7525, 	"|H1:item:43549:133:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:143:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:153:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:163:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h" ),
		[10] = tcc_CraftingXpObj(10, "V15", 0, 7821, 8781,  9859, 	10651, 	"|H1:item:43549:308:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:309:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:310:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "|H1:item:43549:311:50:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h" ),
	}
	-- skillTier, description, potencyAdditiveName, potencySubtractiveName, levelMin, levelMax, createXpWhite, deconstructXpWhite,
	-- createXpGreen, deconstructXpGreen, createXpBlue, deconstructXpBlue, createXpPurple, deconstructXpPurple, 
	-- potencyAdditiveLink, potencySubtractiveLink, itemLinkWhite, itemLinkGreen, itemLinkBlue, itemLinkPurple,
	-- potencyAdditiveTexture, potencySubtractiveTexture
	tcc.Crafts["Enchanting"].CraftingXp = {
		tcc_EnchantingXpObj(1,  "Trifling", "Jora", "Jode",     "1", "10",    164,  312,  329 , 625,  549,  1043,  986,  1874,  "|H1:item:45855:20:5:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45817:20:13:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:20:5:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:21:5:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:22:5:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:23:5:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_050.dds", "/esoui/art/icons/crafting_components_runestones_035.dds"),
		tcc_EnchantingXpObj(1,  "Inferior", "Porade", "Notade", "5", "15",    232,  441,  464 , 882,  774,  1472,  1390, 2645,  "|H1:item:45856:20:11:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45818:20:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:20:10:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:21:10:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:22:10:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:23:10:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_046.dds", "/esoui/art/icons/crafting_components_runestones_027.dds"),
		tcc_EnchantingXpObj(2,  "Petty", "Jera", "Ode",      "10", "20",   307,  584,  615 , 1172, 1027, 1957,  1845, 3516,  "|H1:item:45857:20:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45819:20:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:20:15:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:21:15:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:22:15:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:23:15:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_040.dds", "/esoui/art/icons/crafting_components_runestones_033.dds"),
		tcc_EnchantingXpObj(2,  "Slight", "Jejora", "Tade",   "15", "25",   403,  768,  807 , 1536, 1347, 2565,  2420, 4609,  "|H1:item:45806:20:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45820:20:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:20:20:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:21:20:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:22:20:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:23:20:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_041.dds", "/esoui/art/icons/crafting_components_runestones_028.dds"),
		tcc_EnchantingXpObj(3,  "Minor", "Odra", "Jayde",    "20", "30",   487,  928,  975 , 1857, 1628, 3101,  2925, 5572,  "|H1:item:45807:20:25:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45821:20:25:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:20:25:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:21:25:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:22:25:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:23:25:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_049.dds", "/esoui/art/icons/crafting_components_runestones_036.dds"),
		tcc_EnchantingXpObj(3,  "Lesser", "Pojora", "Edode",  "25", "35",   592,  1127, 1184, 2254, 1977, 3764,  3552, 6763,  "|H1:item:45808:20:24:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45822:20:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:20:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:21:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:22:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:23:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_048.dds", "/esoui/art/icons/crafting_components_runestones_026.dds"),
		tcc_EnchantingXpObj(4,  "Moderate", "Edora", "Pojode",  "30", "40",   692,  1318, 1385, 2636, 2312, 4402,  4154, 7910,  "|H1:item:45809:20:35:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45823:20:35:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:20:35:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:21:35:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:22:35:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:23:35:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_043.dds", "/esoui/art/icons/crafting_components_runestones_031.dds"),
		tcc_EnchantingXpObj(4,  "Average", "Jaera", "Rekude",  "35", "45",   814,  1552, 1629, 3104, 2720, 5183,  4887, 9313,  "|H1:item:45810:20:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45824:20:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:20:40:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:21:40:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:22:40:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:23:40:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_042.dds", "/esoui/art/icons/crafting_components_runestones_030.dds"),
		tcc_EnchantingXpObj(5,  "Strong", "Pora", "Hade",     "40", "50",   969,  1845, 1938, 3690, 3236, 6161,  5815, 11071, "|H1:item:45811:20:45:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45825:20:18:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:20:45:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:21:45:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:22:45:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:23:45:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_047.dds", "/esoui/art/icons/crafting_components_runestones_037.dds"),
		tcc_EnchantingXpObj(5,  "Major", "Denara", "Idode",  "V1", "V3",   1200, 2284, 2400, 4568, 4008, 7628,  7200, 13704, "|H1:item:45812:125:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45826:125:18:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:125:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:135:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:145:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:155:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_044.dds", "/esoui/art/icons/crafting_components_runestones_025.dds"),
		tcc_EnchantingXpObj(6,  "Greater", "Rera", "Pode",     "V3", "V5",   1281, 2437, 2562, 4875, 4278, 8141,  7686, 14625, "|H1:item:45813:127:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45827:127:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:127:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:137:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:147:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:157:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_045.dds", "/esoui/art/icons/crafting_components_runestones_032.dds"),
		tcc_EnchantingXpObj(7,  "Grand", "Derado", "Kedeko", "V5", "V7",   1362, 2591, 2724, 5182, 4549, 8653,  8172, 15546, "|H1:item:45814:129:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45828:129:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:129:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:139:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:149:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:159:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_051.dds", "/esoui/art/icons/crafting_components_runestones_052.dds"),
		tcc_EnchantingXpObj(8,  "Splendid", "Rekura", "Rede",   "V7", "V9",   1483, 2744, 2967, 5489, 4954, 9166,  8901, 16467, "|H1:item:45815:131:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45829:131:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:131:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:141:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:151:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:161:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_029.dds", "/esoui/art/icons/crafting_components_runestones_038.dds"),
		tcc_EnchantingXpObj(9,  "Monumental", "Kura", "Kude",     "V10", "V14", 1605, 3282, 3210, 6564, 5360, 10961, 9630, 19692, "|H1:item:45816:134:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:45830:134:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:272:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:273:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:274:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:275:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_034.dds", "/esoui/art/icons/crafting_components_runestones_039.dds"),
		tcc_EnchantingXpObj(10, "Superb", "Rejera", "Jehade", "V15", "V15", 0,    3654, 0,    7308, 0,    12204, 0,    21930, "|H1:item:64509:308:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:64508:308:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:308:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:309:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:310:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:311:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_053.dds", "/esoui/art/icons/crafting_components_runestones_055.dds"),
		tcc_EnchantingXpObj(10, "Truly Superb", "Repora", "Itade",  "V16", "V16", 0,    3808, 0,    7616, 0,    12718, 0,    22854, "|H1:item:68341:366:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:68340:366:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:366:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:367:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:368:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5365:369:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_054.dds", "/esoui/art/icons/crafting_components_runestones_056.dds"),
	}
	
	--/esoui/art/icons/crafting_components_runestones_010.dds??? ...012, 019
	-- tcc_EssenceRuneObj(name, translation, additiveEffect, subtractiveEffect, itemLink, itemTexture)
	tcc.EssenceRunes = {
		["Dekeipa"] = tcc_EssenceRuneObj("Dekeipa", "Frost", "(W) Frost Damage", "(J) Frost Resistance", "|H1:item:45839:20:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_017.dds"),
		["Deni"] = tcc_EssenceRuneObj("Deni", "Stamina", "(A) Increase Max Stamina", "(W) Magic Damage + Restore Stamina", "|H1:item:45833:20:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_016.dds"),
		["Denima"] = tcc_EssenceRuneObj("Denima", "Stamina Regen", "(J) Stamina Recovery", "(J) Reduce cost of Stamina abilities", "|H1:item:45836:20:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_015.dds"),
		["Deteri"] = tcc_EssenceRuneObj("Deteri", "Armor", "(A) Damage Shield 5 sec", "(W) Reduce target's Armor", "|H1:item:45842:20:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_014.dds"),
		["Haoko"] = tcc_EssenceRuneObj("Haoko", "Disease", "(W) Disease Damage", "(J) Disease Resistance", "|H1:item:45841:20:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_013.dds"),
		["Kaderi"] = tcc_EssenceRuneObj("Kaderi", "Shield", "(J) Increase Bash Damage", "(J) Reduce cost of Bash and Blocking", "|H1:item:45849:20:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_011.dds"),
		["Kuoko"] = tcc_EssenceRuneObj("Kuoko", "Poison", "(W) Poison Damage", "(J) Poison Resistance", "|H1:item:45837:20:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_024.dds"),
		["Makderi"] = tcc_EssenceRuneObj("Makderi", "Spell Harm", "(J) Spell Damage", "(J) Spell Resistance", "|H1:item:45848:20:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_009.dds"),
		["Makko"] = tcc_EssenceRuneObj("Makko", "Magicka", "(A) Increase Max Magicka", "(W) Magic Damage + Restore Magicka", "|H1:item:45832:20:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_023.dds"),
		["Makkoma"] = tcc_EssenceRuneObj("Makkoma", "Magicka Regen", "(J) Magicka Recovery", "(J) Reduce Magicka cost of spells", "|H1:item:45835:20:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_022.dds"),
		["Meip"] = tcc_EssenceRuneObj("Meip", "Shock", "(W) Shock Damage", "(J) Shock Resistance", "|H1:item:45840:20:24:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_021.dds"),
		["Oko"] = tcc_EssenceRuneObj("Oko", "Health", "(A) Increase Max Health", "(W) Magic Damage + Restore Health", "|H1:item:45831:20:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_020.dds"),
		["Okoma"] = tcc_EssenceRuneObj("Okoma", "Health Regen", "(J) Health Recovery", "(W) Unresistable Damage", "|H1:item:45834:20:18:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", ""),
		["Okori"] = tcc_EssenceRuneObj("Okori", "Power", "(W) Increase Weapon Damage 5 sec", "(W) Reduce target weapon damage 5 sec", "|H1:item:45843:20:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_008.dds"),
		["Oru"] = tcc_EssenceRuneObj("Oru", "Alchemist", "(J) Increase effect of restoration potions", "(J) Reduce cooldown of potions", "|H1:item:45846:20:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_007.dds"),
		["Rakeipa"] = tcc_EssenceRuneObj("Rakeipa", "Fire", "(W) Flame Damage", "(J) Flame Resistance", "|H1:item:45838:20:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_018.dds"),
		["Taderi"] = tcc_EssenceRuneObj("Taderi", "Physical Harm", "(J) Weapon Damage", "(J) Armor", "|H1:item:45847:20:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_006.dds"),
		["Hakeijo"] = tcc_EssenceRuneObj("Hakeijo", "(Prismatic)", "??? +Dmg to Daedra and Undead ???", "??? Health, Stamina, Magicka ???", "|H1:item:68342:20:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_058.dds"),
	}
end


--[[ Object Functions ]]--

function tcc_CraftObj(craftName, craftType, optimalItemType, skillIndex)
	return {
		CraftName = craftName,
		CraftType = craftType,
		OptimalItemType = optimalItemType,
		SkillIndex = skillIndex,
	}
end

function tcc_CraftingPassiveObj(nameGeneric, name, description, texture, rankAvailableAtSkillLevelArray)
	return {
		NameGeneric = nameGeneric,
		Name = name,
		Description = description,
		Texture = texture,
		RankAvailableAtSkillLevelArray = rankAvailableAtSkillLevelArray,
	}
end

function tcc_CraftingXpObj(skillTier, optimalItemLvl, createXp, deconstructXpWhite, deconstructXpGreen, deconstructXpBlue, deconstructXpPurple, itemLinkWhite, itemLinkGreen, itemLinkBlue, itemLinkPurple)
	return {
		SkillTier = skillTier,
		OptimalItemLvl = optimalItemLvl,
		CreateXp = createXp,
		DeconstructXpWhite = deconstructXpWhite,
		DeconstructXpGreen = deconstructXpGreen,
		DeconstructXpBlue = deconstructXpBlue,
		DeconstructXpPurple = deconstructXpPurple,
		ItemLinkWhite = itemLinkWhite,
		ItemLinkGreen = itemLinkGreen,
		ItemLinkBlue = itemLinkBlue,
		ItemLinkPurple = itemLinkPurple,
	}
end

function tcc_EnchantingXpObj(skillTier, description, potencyAdditiveName, potencySubtractiveName, levelMin, levelMax, createXpWhite, deconstructXpWhite, createXpGreen, deconstructXpGreen, createXpBlue, deconstructXpBlue, createXpPurple, deconstructXpPurple, potencyAdditiveLink, potencySubtractiveLink, itemLinkWhite, itemLinkGreen, itemLinkBlue, itemLinkPurple, potencyAdditiveTexture, potencySubtractiveTexture)
	return {
		SkillTier = skillTier,
		Description = description,
		PotencyAdditiveName = potencyAdditiveName,
		PotencySubtractiveName = potencySubtractiveName,
		LevelMin = levelMin,
		LevelMax = levelMax,
		CreateXpWhite = createXpWhite,
		DeconstructXpWhite = deconstructXpWhite,
		CreateXpGreen = createXpGreen,
		DeconstructXpGreen = deconstructXpGreen,
		CreateXpBlue = createXpBlue,
		DeconstructXpBlue = deconstructXpBlue,
		CreateXpPurple = createXpPurple,
		DeconstructXpPurple = deconstructXpPurple,
		PotencyAdditiveLink = potencyAdditiveLink,
		PotencySubtractiveLink = potencySubtractiveLink,
		ItemLinkWhite = itemLinkWhite,
		ItemLinkGreen = itemLinkGreen,
		ItemLinkBlue = itemLinkBlue,
		ItemLinkPurple = itemLinkPurple,
		PotencyAdditiveTexture = potencyAdditiveTexture,
		PotencySubtractiveTexture = potencySubtractiveTexture,
	}
end

function tcc_EssenceRuneObj(name, translation, additiveEffect, subtractiveEffect, itemLink, itemTexture)
	return {
		Name = name, 
		Translation = translation, 
		AdditiveEffect = additiveEffect, 
		SubtractiveEffect = subtractiveEffect, 
		ItemLink = itemLink,
		ItemTexture = itemTexture,
	}
end
