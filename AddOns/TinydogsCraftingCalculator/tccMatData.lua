-- TinydogsCraftingCalculator Item Data LUA File
-- Last Updated December 4, 2015
-- Created September 2015 by @tinydog - tinydog1234@hotmail.com

function tcc_InitItemData()
	-- Using an ordered list of quality names, so that I can reference the unordered list of quality objects in the correct order.
	tcc.ItemQualityNames = { "White", "Green", "Blue", "Purple", "Gold" }	
	tcc.ItemQuality = {
		-- tcc_IconObj(iconType, key, name, materialName, itemLink, normalTexture, mouseOverTexture, selectedTexture)
		["White"] = tcc_IconObj("Quality", "White", "Normal (White) Quality", nil, nil, "TinydogsCraftingCalculator/images/quality/white_up.dds", "TinydogsCraftingCalculator/images/quality/over.dds", "TinydogsCraftingCalculator/images/quality/white_down.dds"),
		["Green"] = tcc_IconObj("Quality", "Green", "Fine (Green) Quality", nil, nil, "TinydogsCraftingCalculator/images/quality/green_up.dds", "TinydogsCraftingCalculator/images/quality/over.dds", "TinydogsCraftingCalculator/images/quality/green_down.dds"),
		["Blue"] = tcc_IconObj("Quality", "Blue", "Superior (Blue) Quality", nil, nil, "TinydogsCraftingCalculator/images/quality/blue_up.dds", "TinydogsCraftingCalculator/images/quality/over.dds", "TinydogsCraftingCalculator/images/quality/blue_down.dds"),
		["Purple"] = tcc_IconObj("Quality", "Purple", "Epic (Purple) Quality", nil, nil, "TinydogsCraftingCalculator/images/quality/purple_up.dds", "TinydogsCraftingCalculator/images/quality/over.dds", "TinydogsCraftingCalculator/images/quality/purple_down.dds"),
		["Gold"] = tcc_IconObj("Quality", "Gold", "Legendary (Gold) Quality", nil, nil, "TinydogsCraftingCalculator/images/quality/gold_up.dds", "TinydogsCraftingCalculator/images/quality/over.dds", "TinydogsCraftingCalculator/images/quality/gold_down.dds"),
	}
	-- tcc_ItemQualityExtraProperties(qualityIconObj, index, colorCode, upgradeMatQtyPerImprovementPassiveRank)
	tcc.ItemQuality.White = tcc_ItemQualityExtraProperties(tcc.ItemQuality.White, 1, "FFFFFF", 		{ 0, 0, 0, 0 });
	tcc.ItemQuality.Green = tcc_ItemQualityExtraProperties(tcc.ItemQuality.Green, 2, "00FF00", 		{ 5, 4, 3, 2 });
	tcc.ItemQuality.Blue = tcc_ItemQualityExtraProperties(tcc.ItemQuality.Blue, 3, "6666FF", 		{ 7, 5, 4, 3 });
	tcc.ItemQuality.Purple = tcc_ItemQualityExtraProperties(tcc.ItemQuality.Purple, 4, "FF00FF", 	{ 10, 7, 5, 4 });
	tcc.ItemQuality.Gold = tcc_ItemQualityExtraProperties(tcc.ItemQuality.Gold, 5, "FFFF00", 		{ 20, 14, 10, 8 });
	
	tcc_InitQualityMats()
	
	tcc.TexturePaths = {
		-- % will be replaced with "up" (normal), "down" (rollover), "over" (selected), or "disabled"
		ArmorCategories = {
			["Heavy"] = "/esoui/art/inventory/inventory_tabicon_armor_%.dds",
			["Medium"] = "TinydogsCraftingCalculator/images/armor/medium_%.dds",
			["Light"] = "TinydogsCraftingCalculator/images/armor/light_%.dds",
		},
		BodyParts = {
			["Chest"] = "TinydogsCraftingCalculator/images/armor/chest_%.dds",
			["Shirt"] = "TinydogsCraftingCalculator/images/armor/chest_%.dds",
			["Feet"] = "TinydogsCraftingCalculator/images/armor/feet_%.dds",
			["Hands"] = "TinydogsCraftingCalculator/images/armor/hands_%.dds",
			["Head"] = "/esoui/art/inventory/inventory_tabicon_armor_%.dds",
			["Legs"] = "TinydogsCraftingCalculator/images/armor/legs_%.dds",
			["Shoulders"] = "TinydogsCraftingCalculator/images/armor/shoulders_%.dds",
			["Waist"] = "TinydogsCraftingCalculator/images/armor/belt_%.dds",
		},
		OtherItemTypes = {
			["Shield"] = "TinydogsCraftingCalculator/images/armor/shield_%.dds",
			["Axe"] = "TinydogsCraftingCalculator/images/weapon/axe_%.dds",
			["Mace"] = "TinydogsCraftingCalculator/images/weapon/mace_%.dds",
			["Sword"] = "TinydogsCraftingCalculator/images/weapon/sword_%.dds",
			["Dagger"] = "TinydogsCraftingCalculator/images/weapon/dagger_%.dds",
			["BattleAxe"] = "TinydogsCraftingCalculator/images/weapon/battleaxe_%.dds",
			["Maul"] = "TinydogsCraftingCalculator/images/weapon/mace_%.dds",
			["Greatsword"] = "TinydogsCraftingCalculator/images/weapon/greatsword_%.dds",
			["Bow"] = "TinydogsCraftingCalculator/images/weapon/bow_%.dds",
			["Fire"] = "TinydogsCraftingCalculator/images/weapon/fire_%.dds",
			["Ice"] = "TinydogsCraftingCalculator/images/weapon/ice_%.dds",
			["Lightning"] = "TinydogsCraftingCalculator/images/weapon/lightning_%.dds",
			["Resto"] = "TinydogsCraftingCalculator/images/weapon/restoration_%.dds",
		},
	}

	-- Item Materials
	tcc.ItemMaterials = {
		-- ["SkillTier"], ["MinimumLevel"],
		-- ["MaterialNameCloth"], ["MaterialNameLeather"], ["MaterialNameMetal"], ["MaterialNameWood"], 
		-- ["MaterialLinkCloth"], ["MaterialLinkLeather"], ["MaterialLinkMetal"], ["MaterialLinkWood"],
		-- ["MaterialTextureCloth"], ["MaterialTextureLeather"], ["MaterialTextureMetal"], ["MaterialTextureWood"], 
		[1] = tcc_ItemMaterialObj(1, "1", "Jute", "Rawhide", "Iron", "Maple", "|H1:item:811:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:794:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:5413:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:803:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_light_armor_standard_r_001.dds", "/esoui/art/icons/crafting_medium_armor_standard_f_001.dds", "/esoui/art/icons/crafting_ore_base_iron_r2.dds", "/esoui/art/icons/crafting_forester_weapon_component_006.dds"),
		[2] = tcc_ItemMaterialObj(2, "16", "Flax", "Hide", "Steel", "Oak", "|H1:item:4463:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:4447:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:4487:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:533:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_cloth_base_flax_r3.dds", "/esoui/art/icons/crafting_leather_base_boiled_leather_r3.dds", "/esoui/art/icons/crafting_ore_base_high_iron_r3.dds", "/esoui/art/icons/crafting_wood_base_oak_r3.dds"),
		[3] = tcc_ItemMaterialObj(3, "26", "Cotton", "Leather", "Orichalcum", "Beech", "|H1:item:23125:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:23099:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:23107:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:23121:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_cloth_base_cotton_r2.dds", "/esoui/art/icons/crafting_leather_base_boiled_leather_r2.dds", "/esoui/art/icons/crafting_ore_base_iron_r3.dds", "/esoui/art/icons/crafting_wood_base_beech_r3.dds"),
		[4] = tcc_ItemMaterialObj(4, "36", "Spidersilk", "Thick Leather", "Dwarven", "Hickory", "|H1:item:23126:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:23100:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:6000:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:23122:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_cloth_base_spidersilk_r2.dds", "/esoui/art/icons/crafting_leather_base_topgrain_r3.dds", "/esoui/art/icons/crafting_smith_plug_standard_r_001.dds", "/esoui/art/icons/crafting_wood_base_hickory_r3.dds"),
		[5] = tcc_ItemMaterialObj(5, "46", "Ebonthread", "Fell Hide", "Ebony", "Yew", "|H1:item:23127:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:23101:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:6001:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:23123:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_cloth_base_ebonthread_r2.dds", "/esoui/art/icons/crafting_leather_base_topgrain_r2.dds", "/esoui/art/icons/crafting_ore_base_ebony_r3.dds", "/esoui/art/icons/crafting_wood_base_yew_r3.dds"),
		[6] = tcc_ItemMaterialObj(6, "V1", "Kresh Fiber", "Topgrain Hide", "Calcinium", "Birch", "|H1:item:46131:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46135:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46127:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46139:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_cloth_famin.dds", "/esoui/art/icons/crafting_hide_fell.dds", "/esoui/art/icons/crafting_ingot_calcinium.dds", "/esoui/art/icons/crafting_wood_sanded_birch.dds"),
		[7] = tcc_ItemMaterialObj(7, "V4", "Ironthread", "Iron Hide", "Galatite", "Ash", "|H1:item:46132:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46136:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46128:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46140:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_cloth_ironthread.dds", "/esoui/art/icons/crafting_hide_iron.dds", "/esoui/art/icons/crafting_ingot_galatite.dds", "/esoui/art/icons/crafting_wood_sanded_ash.dds"),
		[8] = tcc_ItemMaterialObj(8, "V7", "Silverweave", "Superb Hide", "Quicksilver", "Mahogany", "|H1:item:46133:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46137:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46129:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46141:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_cloth_silverweave.dds", "/esoui/art/icons/crafting_hide_scaled.dds", "/esoui/art/icons/crafting_ingot_moonstone.dds", "/esoui/art/icons/crafting_wood_sanded_mahogany.dds"),
		[9] = tcc_ItemMaterialObj(9, "V9", "Void Cloth", "Shadowhide", "Voidstone", "Nightwood", "|H1:item:46134:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46138:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46130:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:46142:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_cloth_void.dds", "/esoui/art/icons/crafting_leather_base_leather_r2.dds", "/esoui/art/icons/crafting_ingot_voidstone.dds", "/esoui/art/icons/crafting_wood_sanded_nightwood.dds"),
		[10] = tcc_ItemMaterialObj(10, "V15", "Ancestor Silk", "Rubedo Leather", "Rubedite", "Ruby Ash", "|H1:item:64504:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:64506:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:64489:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "|H1:item:64502:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_cloth_base_harvestersilk.dds", "/esoui/art/icons/crafting_daedric_skin.dds", "/esoui/art/icons/crafting_colossus_iron.dds", "/esoui/art/icons/crafting_wood_ruddy_ash.dds"),
	}
	
	-- Item Levels
	tcc.ItemLevels = {
		-- ["ItemLevel"], ["SkillTier"], ["QtyModifierType"], ["BaseQtyCloth"], ["BaseQtyLeather"], ["BaseQtyMetal"], ["BaseQtyWood"]
		-- Numeric array index number corresponds to the Level Slider value.
		-- To look up a level by the actual level name, use tcc_ItemLevelByName().
		[1] = tcc_ItemLevelObj("1", 1, "QtyModifier", 5, 5, 2, 3),
		[2] = tcc_ItemLevelObj("4", 1, "QtyModifier", 6, 6, 3, 4),
		[3] = tcc_ItemLevelObj("6", 1, "QtyModifier", 7, 7, 4, 5),
		[4] = tcc_ItemLevelObj("8", 1, "QtyModifier", 8, 8, 5, 6),
		[5] = tcc_ItemLevelObj("10", 1, "QtyModifier", 9, 9, 6, 7),
		[6] = tcc_ItemLevelObj("12", 1, "QtyModifier", 10, 10, 7, 8),
		[7] = tcc_ItemLevelObj("14", 1, "QtyModifier", 11, 11, 8, 9),
		[8] = tcc_ItemLevelObj("16", 2, "QtyModifier", 6, 6, 3, 4),
		[9] = tcc_ItemLevelObj("18", 2, "QtyModifier", 7, 7, 4, 5),
		[10] = tcc_ItemLevelObj("20", 2, "QtyModifier", 8, 8, 5, 6),
		[11] = tcc_ItemLevelObj("22", 2, "QtyModifier", 9, 9, 6, 7),
		[12] = tcc_ItemLevelObj("24", 2, "QtyModifier", 10, 10, 7, 8),
		[13] = tcc_ItemLevelObj("26", 3, "QtyModifier", 7, 7, 4, 5),
		[14] = tcc_ItemLevelObj("28", 3, "QtyModifier", 8, 8, 5, 6),
		[15] = tcc_ItemLevelObj("30", 3, "QtyModifier", 9, 9, 6, 7),
		[16] = tcc_ItemLevelObj("32", 3, "QtyModifier", 10, 10, 7, 8),
		[17] = tcc_ItemLevelObj("34", 3, "QtyModifier", 11, 11, 8, 9),
		[18] = tcc_ItemLevelObj("36", 4, "QtyModifier", 8, 8, 5, 6),
		[19] = tcc_ItemLevelObj("38", 4, "QtyModifier", 9, 9, 6, 7),
		[20] = tcc_ItemLevelObj("40", 4, "QtyModifier", 10, 10, 7, 8),
		[21] = tcc_ItemLevelObj("42", 4, "QtyModifier", 11, 11, 8, 9),
		[22] = tcc_ItemLevelObj("44", 4, "QtyModifier", 12, 12, 9, 10),
		[23] = tcc_ItemLevelObj("46", 5, "QtyModifier", 9, 9, 6, 7),
		[24] = tcc_ItemLevelObj("48", 5, "QtyModifier", 10, 10, 7, 8),
		[25] = tcc_ItemLevelObj("50", 5, "QtyModifier", 11, 11, 8, 9),
		[26] = tcc_ItemLevelObj("V1", 6, "QtyModifier", 10, 10, 7, 8),
		[27] = tcc_ItemLevelObj("V2", 6, "QtyModifier", 11, 11, 8, 9),
		[28] = tcc_ItemLevelObj("V3", 6, "QtyModifier", 12, 12, 9, 10),
		[29] = tcc_ItemLevelObj("V4", 7, "QtyModifier", 11, 11, 8, 9),
		[30] = tcc_ItemLevelObj("V5", 7, "QtyModifier", 12, 12, 9, 10),
		[31] = tcc_ItemLevelObj("V6", 7, "QtyModifier", 13, 13, 10, 11),
		[32] = tcc_ItemLevelObj("V7", 8, "QtyModifier", 12, 12, 9, 10),
		[33] = tcc_ItemLevelObj("V8", 8, "QtyModifier", 13, 13, 10, 11),
		[34] = tcc_ItemLevelObj("V9", 9, "QtyModifier", 13, 13, 10, 11),
		[35] = tcc_ItemLevelObj("V10", 9, "QtyModifier", 14, 14, 11, 12),
		[36] = tcc_ItemLevelObj("V11", 9, "QtyModifier", 15, 15, 12, 13),
		[37] = tcc_ItemLevelObj("V12", 9, "QtyModifier", 16, 16, 13, 14),
		[38] = tcc_ItemLevelObj("V13", 9, "QtyModifier", 17, 17, 14, 15),
		[39] = tcc_ItemLevelObj("V14", 9, "QtyModifier", 18, 18, 15, 16),
		[40] = tcc_ItemLevelObj("V15", 10, "QtyModifierV15", 13, 13, 10, 12),
		[41] = tcc_ItemLevelObj("V16", 10, "QtyModifierV16", 130, 130, 100, 120),
	}

	-- Item Types
	tcc.ItemTypes = {
		-- ["Key"], "CraftName"], ["ItemCategory"], ["BodyPart"], ["TraitType"], ["ItemName"], ["MaterialCategory"], 
		-- ["QtyModifier"], ["QtyModifierV15"], ["QtyModifierV16"]
		["Light"] = {},
		["Medium"] = {},
		["Heavy"] = {},
		["Robe"] = tcc_ItemTypeObj(		"Robe", "Clothing", "Light", "Chest", "Armor", "Robe", "Cloth", 2, 2, 20),
		["Shirt"] = tcc_ItemTypeObj(	"Shirt", "Clothing", "Light", "Chest", "Armor", "Shirt", "Cloth", 2, 2, 20),
		["Shoes"] = tcc_ItemTypeObj(	"Shoes", "Clothing", "Light", "Feet", "Armor", "Shoes", "Cloth", 0, 0, 0),
		["Gloves"] = tcc_ItemTypeObj(	"Gloves", "Clothing", "Light", "Hands", "Armor", "Gloves", "Cloth", 0, 0, 0),
		["Hat"] = tcc_ItemTypeObj(		"Hat", "Clothing", "Light", "Head", "Armor", "Hat", "Cloth", 0, 0, 0),
		["Breeches"] = tcc_ItemTypeObj(	"Breeches", "Clothing", "Light", "Legs", "Armor", "Breeches", "Cloth", 1, 1, 10),
		["Epaulets"] = tcc_ItemTypeObj(	"Epaulets", "Clothing", "Light", "Shoulders", "Armor", "Epaulets", "Cloth", 0, 0, 0),
		["Sash"] = tcc_ItemTypeObj(		"Sash", "Clothing", "Light", "Waist", "Armor", "Sash", "Cloth", 0, 0, 0),
		["Jack"] = tcc_ItemTypeObj(		"Jack", "Clothing", "Medium", "Chest", "Armor", "Jack", "Leather", 2, 2, 20),
		["Boots"] = tcc_ItemTypeObj(	"Boots", "Clothing", "Medium", "Feet", "Armor", "Boots", "Leather", 0, 0, 0),
		["Bracers"] = tcc_ItemTypeObj(	"Bracers", "Clothing", "Medium", "Hands", "Armor", "Bracers", "Leather", 0, 0, 0),
		["Helmet"] = tcc_ItemTypeObj(	"Helmet", "Clothing", "Medium", "Head", "Armor", "Helmet", "Leather", 0, 0, 0),
		["Guards"] = tcc_ItemTypeObj(	"Guards", "Clothing", "Medium", "Legs", "Armor", "Guards", "Leather", 1, 1, 10),
		["ArmCops"] = tcc_ItemTypeObj(	"ArmCops", "Clothing", "Medium", "Shoulders", "Armor", "Arm Cops", "Leather", 0, 0, 0),
		["Belt"] = tcc_ItemTypeObj(		"Belt", "Clothing", "Medium", "Waist", "Armor", "Belt", "Leather", 0, 0, 0),
		["Cuirass"] = tcc_ItemTypeObj(	"Cuirass", "Blacksmithing", "Heavy", "Chest", "Armor", "Cuirass", "Metal", 5, 5, 50),
		["Sabatons"] = tcc_ItemTypeObj(	"Sabatons", "Blacksmithing", "Heavy", "Feet", "Armor", "Sabatons", "Metal", 3, 3, 30),
		["Gauntlets"] = tcc_ItemTypeObj("Gauntlets", "Blacksmithing", "Heavy", "Hands", "Armor", "Gauntlets", "Metal", 3, 3, 30),
		["Helm"] = tcc_ItemTypeObj(		"Helm", "Blacksmithing", "Heavy", "Head", "Armor", "Helm", "Metal", 3, 3, 30),
		["Greaves"] = tcc_ItemTypeObj(	"Greaves", "Blacksmithing", "Heavy", "Legs", "Armor", "Greaves", "Metal", 4, 4, 40),
		["Pauldrons"] = tcc_ItemTypeObj("Pauldrons", "Blacksmithing", "Heavy", "Shoulders", "Armor", "Pauldrons", "Metal", 3, 3, 30),
		["Girdle"] = tcc_ItemTypeObj(	"Girdle", "Blacksmithing", "Heavy", "Waist", "Armor", "Girdle", "Metal", 3, 3, 30),
		["Axe"] = tcc_ItemTypeObj(		"Axe", "Blacksmithing", "1H", "", "Weapon", "Axe", "Metal", 1, 1, 10),
		["Mace"] = tcc_ItemTypeObj(		"Mace", "Blacksmithing", "1H", "", "Weapon", "Mace", "Metal", 1, 1, 10),
		["Sword"] = tcc_ItemTypeObj(	"Sword", "Blacksmithing", "1H", "", "Weapon", "Sword", "Metal", 1, 1, 10),
		["Dagger"] = tcc_ItemTypeObj(	"Dagger", "Blacksmithing", "1H", "", "Weapon", "Dagger", "Metal", 0, 0, 0),
		["BattleAxe"] = tcc_ItemTypeObj("BattleAxe", "Blacksmithing", "2H", "", "Weapon", "Battle Axe", "Metal", 3, 4, 40),
		["Maul"] = tcc_ItemTypeObj(		"Maul", "Blacksmithing", "2H", "", "Weapon", "Maul", "Metal", 3, 4, 40),
		["Greatsword"] = tcc_ItemTypeObj("Greatsword", "Blacksmithing", "2H", "", "Weapon", "Greatsword", "Metal", 3, 4, 40),
		["Bow"] = tcc_ItemTypeObj(		"Bow", "Woodworking", "", "", "Weapon", "Bow", "Wood", 0, 0, 0),
		["Fire"] = tcc_ItemTypeObj(		"Fire", "Woodworking", "", "", "Weapon", "Fire Staff", "Wood", 0, 0, 0),
		["Ice"] = tcc_ItemTypeObj(		"Ice", "Woodworking", "", "", "Weapon", "Ice Staff", "Wood", 0, 0, 0),
		["Lightning"] = tcc_ItemTypeObj("Lightning", "Woodworking", "", "", "Weapon", "Lightning Staff", "Wood", 0, 0, 0),
		["Resto"] = tcc_ItemTypeObj(	"Resto", "Woodworking", "", "", "Weapon", "Restoration Staff", "Wood", 0, 0, 0),
		["Shield"] = tcc_ItemTypeObj(	"Shield", "Woodworking", "", "", "Armor", "Shield", "Wood", 3, 2, 20),
	}
	tcc.ItemTypes["Light"] = {
		["Chest"] = tcc.ItemTypes["Robe"],
		["Shirt"] = tcc.ItemTypes["Shirt"],
		["Feet"] = tcc.ItemTypes["Shoes"],
		["Hands"] = tcc.ItemTypes["Gloves"],
		["Head"] = tcc.ItemTypes["Hat"],
		["Legs"] = tcc.ItemTypes["Breeches"],
		["Shoulders"] = tcc.ItemTypes["Epaulets"],
		["Waist"] = tcc.ItemTypes["Sash"],
	}
	tcc.ItemTypes["Medium"] = {
		["Chest"] = tcc.ItemTypes["Jack"],
		["Feet"] = tcc.ItemTypes["Boots"],
		["Hands"] = tcc.ItemTypes["Bracers"],
		["Head"] = tcc.ItemTypes["Helmet"],
		["Legs"] = tcc.ItemTypes["Guards"],
		["Shoulders"] = tcc.ItemTypes["ArmCops"],
		["Waist"] = tcc.ItemTypes["Belt"],
	}
	tcc.ItemTypes["Heavy"] = {
		["Chest"] = tcc.ItemTypes["Cuirass"],
		["Feet"] = tcc.ItemTypes["Sabatons"],
		["Hands"] = tcc.ItemTypes["Gauntlets"],
		["Head"] = tcc.ItemTypes["Helm"],
		["Legs"] = tcc.ItemTypes["Greaves"],
		["Shoulders"] = tcc.ItemTypes["Pauldrons"],
		["Waist"] = tcc.ItemTypes["Girdle"],
	}

	-- tcc_IconObj(iconType, key, name, materialName, itemLink, normalTexture, [mouseOverTexture], [selectedTexture])
	tcc.ItemTraits = {
		["None"] = tcc_IconObj("Trait", "None", "None", nil, nil, "/esoui/art/hud/radialicon_cancel_up.dds", "/esoui/art/hud/radialicon_cancel_over.dds", "/esoui/art/hud/radialicon_cancel_over.dds"),
		["Armor"] = {
			tcc_IconObj("Trait", "Sturdy", 		"Sturdy", 		"Quartz", "|H1:item:4456:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_runecrafter_plug_component_002.dds"),
			tcc_IconObj("Trait", "Impenetrable", "Impenetrable", "Diamond", "|H1:item:23219:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_jewelry_base_diamond_r3.dds"),
			tcc_IconObj("Trait", "Reinforced", 	"Reinforced", 	"Sardonyx", "|H1:item:30221:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_enchantment_base_sardonyx_r2.dds"),
			tcc_IconObj("Trait", "WellFitted", 	"Well-Fitted", 	"Almandine", "|H1:item:23221:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_accessory_sp_names_002.dds"),
			tcc_IconObj("Trait", "Training", 	"Training", 	"Emerald", "|H1:item:4442:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_jewelry_base_emerald_r2.dds"),
			tcc_IconObj("Trait", "Infused", 	"Infused", 		"Bloodstone", "|H1:item:30219:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_enchantment_baxe_bloodstone_r2.dds"),
			tcc_IconObj("Trait", "Exploration", "Exploration", 	"Garnet", "|H1:item:23171:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_jewelry_base_garnet_r3.dds"),
			tcc_IconObj("Trait", "Divines", 	"Divines", 		"Sapphire", "|H1:item:23173:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_accessory_sp_names_001.dds"),
			tcc_IconObj("Trait", "NirnhonedFortified", 	"Nirnhoned",  "Fortified Nirncrux", "|H1:item:56862:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_potent_nirncrux_stone.dds"),
		},
		["Weapon"] = {
			tcc_IconObj("Trait", "Powered", 	"Powered", 	 "Chysolite", "|H1:item:23203:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_runecrafter_potion_008.dds"),
			tcc_IconObj("Trait", "Charged", 	"Charged", 	 "Amethyst", "|H1:item:23204:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_jewelry_base_amethyst_r3.dds"),
			tcc_IconObj("Trait", "Precise", 	"Precise", 	 "Ruby", "|H1:item:4486:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_jewelry_base_ruby_r3.dds"),
			tcc_IconObj("Trait", "Infused", 	"Infused", 	 "Jade", "|H1:item:810:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_enchantment_base_jade_r3.dds"),
			tcc_IconObj("Trait", "Defending", 	"Defending", "Turquoise", "|H1:item:813:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_jewelry_base_turquoise_r3.dds"),
			tcc_IconObj("Trait", "Training", 	"Training",  "Carnelian", "|H1:item:23165:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_runecrafter_armor_component_004.dds"),
			tcc_IconObj("Trait", "Sharpened", 	"Sharpened", "Fire Opal", "|H1:item:23149:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_enchantment_base_fire_opal_r3.dds"),
			tcc_IconObj("Trait", "Weighted", 	"Weighted",  "Citrine", "|H1:item:16291:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_smith_potion__sp_names_003.dds"),
			tcc_IconObj("Trait", "NirnhonedPotent", "Nirnhoned", "Potent Nirncrux", "|H1:item:56863:30:46:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_potent_nirncrux_dust.dds"),
		},
	}
	
	-- tcc_IconObj(iconType, key, name, materialName, itemLink, normalTexture, [mouseOverTexture], [selectedTexture])
	tcc.RacialStyles = {
		tcc_IconObj("Style", "Any", "Any", nil, nil, "/esoui/art/inventory/inventory_tabicon_all_up.dds", "/esoui/art/inventory/inventory_tabicon_all_over.dds", "/esoui/art/inventory/inventory_tabicon_all_down.dds"),
		tcc_IconObj("Style", "Breton", 		"Breton", 		"Molybdenum", "|H1:item:33251:30:1:0:0:0:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_metals_molybdenum.dds"),
		tcc_IconObj("Style", "Redguard", 	"Redguard", 	"Starmetal", "|H1:item:33258:30:1:0:0:0:0:0:0:0:0:0:0:0:0:2:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_medium_armor_sp_names_002.dds"),
		tcc_IconObj("Style", "Orc", 		"Orc", 			"Manganese", "|H1:item:33257:30:1:0:0:0:0:0:0:0:0:0:0:0:0:3:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_metals_manganese.dds"),
		tcc_IconObj("Style", "Dunmer", 		"Dunmer", 		"Obsidian", "|H1:item:33253:30:1:0:0:0:0:0:0:0:0:0:0:0:0:4:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_metals_graphite.dds"),
		tcc_IconObj("Style", "Nord", 		"Nord", 		"Corundum", "|H1:item:33256:30:1:0:0:0:0:0:0:0:0:0:0:0:0:5:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_metals_corundum.dds"),
		tcc_IconObj("Style", "Argonian", 	"Argonian", 	"Flint", "|H1:item:33150:30:1:0:0:0:0:0:0:0:0:0:0:0:0:6:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_smith_potion_standard_f_002.dds"),
		tcc_IconObj("Style", "Altmer", 		"Altmer", 		"Adamantite", "|H1:item:33252:30:1:0:0:0:0:0:0:0:0:0:0:0:0:7:0:0:0:0:0|h|h", "/esoui/art/icons/grafting_gems_adamantine.dds"),
		tcc_IconObj("Style", "Bosmer", 		"Bosmer", 		"Bone", "|H1:item:33194:30:1:0:0:0:0:0:0:0:0:0:0:0:0:8:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_gems_daedra_skull.dds"),
		tcc_IconObj("Style", "Khajiit", 	"Khajiit", 		"Moonstone", "|H1:item:33255:30:1:0:0:0:0:0:0:0:0:0:0:0:0:9:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_smith_plug_sp_names_001.dds"),
		tcc_IconObj("Style", "Dwemer", 		"Dwemer", 		"Dwemer Frame", "|H1:item:57587:30:1:0:0:0:0:0:0:0:0:0:0:0:0:14:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_dwemer_shiny_tube.dds"),
		tcc_IconObj("Style", "AncientElf", 	"Ancient Elf", 	"Palladium", "|H1:item:46152:30:1:0:0:0:0:0:0:0:0:0:0:0:0:15:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_ore_palladium.dds"),
		tcc_IconObj("Style", "Barbaric", 	"Barbaric", 	"Copper", "|H1:item:46149:30:1:0:0:0:0:0:0:0:0:0:0:0:0:17:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_smith_potion_standard_f_001.dds"),
		tcc_IconObj("Style", "Primal", 		"Primal", 		"Argentum", "|H1:item:46150:30:1:0:0:0:0:0:0:0:0:0:0:0:0:19:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_metals_argentum.dds"),
		tcc_IconObj("Style", "Daedric", 	"Daedric", 		"Daedra Heart", "|H1:item:46151:30:1:0:0:0:0:0:0:0:0:0:0:0:0:20:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_walking_dead_mort_heart.dds"),
		tcc_IconObj("Style", "AncientOrc", 	"Ancient Orc", 	"Cassiterite", "|H1:item:69555:0:1:0:0:0:0:0:0:0:0:0:0:0:0:22:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_smith_plug_standard_f_001.dds"),
		tcc_IconObj("Style", "Mercenary", 	"Mercenary", 	"Laurel", "|H1:item:64713:0:1:0:0:0:0:0:0:0:0:0:0:0:0:26:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_laurel.dds"),
		tcc_IconObj("Style", "Glass", 		"Glass", 		"Malachite", "|H1:item:64689:6:1:0:0:0:0:0:0:0:0:0:0:0:0:28:0:1:0:0:0|h|h", "/esoui/art/icons/crafting_ore_base_malachite_r2.dds"),
		tcc_IconObj("Style", "Xivkyn", 		"Xivkyn", 		"Charcoal of Remorse", "|H1:item:59922:30:1:0:0:0:0:0:0:0:0:0:0:0:0:29:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_smith_potion_008.dds"),
		tcc_IconObj("Style", "Akaviri", 	"Akaviri", 		"Goldscale", "|H1:item:64687:0:1:0:0:0:0:0:0:0:0:0:0:0:0:33:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_medium_armor_vendor_003.dds"),
		tcc_IconObj("Style", "Imperial", 	"Imperial", 	"Nickel", "|H1:item:33254:30:1:0:0:0:0:0:0:0:0:0:0:0:0:34:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_heavy_armor_sp_names_001.dds"),
	}
	
	-- tcc_MaterialSubComponentObj(componentMaterialKey, componentMaterialName, componentMaterialQuantity, componentMaterialLink, componentMaterialTexture)
	tcc.MaterialSubComponents = {
		["Dwemer"] = tcc_MaterialSubComponentObj("Dwemer", "Dwemer Scrap", 10, "|H1:item:57665:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_metals_dwarven_scrap.dds"),
		["AncientOrc"] = tcc_MaterialSubComponentObj("AncientOrc", "Cassiterite Sand", 10, "|H1:item:69556:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_ghost_inert_glow_dust.dds"),
		["Glass"] = tcc_MaterialSubComponentObj("Glass", "Malachite Shard", 10, "|H1:item:64690:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_ore_base_malachite_r1.dds"),
		["Akaviri"] = tcc_MaterialSubComponentObj("Akaviri", "Ancient Scale", 10, "|H1:item:64688:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_medium_armor_vendor_001.dds"),
	}

	-- tcc_ItemSetObj(longName, shortName)
	tcc.ItemSets = {
		tcc_ItemSetObj("None", 						"None"),
		tcc_ItemSetObj("Alessia's Bulwark", 		"Alessia's Bulwark"),
		tcc_ItemSetObj("Arena", 					"Arena"),
		tcc_ItemSetObj("Armor Master", 				"Armor Master"),
		tcc_ItemSetObj("Ashen Grip", 				"Ashen Grip"),
		tcc_ItemSetObj("Death's Wind", 				"Death's Wind"),
		tcc_ItemSetObj("Eyes of Mara", 				"Eyes of Mara"),
		tcc_ItemSetObj("Hist Bark", 				"Hist Bark"),
		tcc_ItemSetObj("Hunding's Rage", 			"Hunding's Rage"),
		tcc_ItemSetObj("Kagrenac's Hope", 			"Kagrenac's Hope"),
		tcc_ItemSetObj("Law of Julianos", 			"Law of Julianos"),
		tcc_ItemSetObj("Magnus' Gift", 				"Magnus' Gift"),
		tcc_ItemSetObj("Morkuldin", 				"Morkuldin"),
		tcc_ItemSetObj("Night Mother's Gaze", 		"Night Mother's Gaze"),
		tcc_ItemSetObj("Night's Silence", 			"Night's Silence"),
		tcc_ItemSetObj("Noble's Conquest", 			"Noble's Conquest"),
		tcc_ItemSetObj("Oblivion's Foe", 			"Oblivion's Foe"),
		tcc_ItemSetObj("Orgnum's Scales", 			"Orgnum's Scales"),
		tcc_ItemSetObj("Redistribution", 			"Redistribution"),
		tcc_ItemSetObj("Seducer", 					"Seducer"),
		tcc_ItemSetObj("Shalidor's Curse", 			"Shalidor's Curse"),
		tcc_ItemSetObj("Song of Lamae", 			"Song of Lamae"),
		tcc_ItemSetObj("Spectre's Eye", 			"Spectre's Eye"),
		tcc_ItemSetObj("Torug's Pact", 				"Torug's Pact"),
		tcc_ItemSetObj("Trial By Fire", 			"Trial By Fire"),
		tcc_ItemSetObj("Twice-Born Star", 			"Twice-Born Star"),
		tcc_ItemSetObj("Twilight's Embrace", 		"Twilight's Embrace"),
		tcc_ItemSetObj("Vampire's Kiss", 			"Vampire's Kiss"),
		tcc_ItemSetObj("Whitestrake's Retribution", "Whitestrake's"),
		tcc_ItemSetObj("Willow's Path", 			"Willow's Path"),
	}
end

-- Separate function so we can call it again, in case the user ranked up in quality improvement passive.
function tcc_InitQualityMats()
	--tcc_GetCurrentAbilityRank(skillType, skillIndex, abilityIndex)
	local smithImprovementMatQtyIndex = 1 + tcc_GetCurrentAbilityRank(SKILL_TYPE_TRADESKILL, TCC_SKILL_INDEX_BLACKSMITHING, 6)
	local clothImprovementMatQtyIndex = 1 + tcc_GetCurrentAbilityRank(SKILL_TYPE_TRADESKILL, TCC_SKILL_INDEX_CLOTHING, 6)
	local woodImprovementMatQtyIndex = 1 + tcc_GetCurrentAbilityRank(SKILL_TYPE_TRADESKILL, TCC_SKILL_INDEX_WOODWORKING, 6)
	
	-- tcc_MaterialObj(materialKey, materialName, craftName, materialQuantity, materialLink, materialTexture)
	tcc.QualityMats = {
		["Blacksmithing"] = {
			["Green"] = tcc_MaterialObj(	"Honing Stone", 	"Honing Stone", 	"Blacksmithing", tcc.ItemQuality.Green.UpgradeMatQtyPerImprovementPassiveRank[smithImprovementMatQtyIndex], "|H1:item:54170:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_ores_lazurite.dds"),
			["Blue"] = tcc_MaterialObj(		"Dwarven Oil", 		"Dwarven Oil", 		"Blacksmithing", tcc.ItemQuality.Blue.UpgradeMatQtyPerImprovementPassiveRank[smithImprovementMatQtyIndex], "|H1:item:54171:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_forester_weapon_vendor_component_002.dds"),
			["Purple"] = tcc_MaterialObj(	"Grain Solvent", 	"Grain Solvent", 	"Blacksmithing", tcc.ItemQuality.Purple.UpgradeMatQtyPerImprovementPassiveRank[smithImprovementMatQtyIndex], "|H1:item:54172:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_forester_potion_vendor_001.dds"),
			["Gold"] = tcc_MaterialObj(		"Tempering Alloy", 	"Tempering Alloy", 	"Blacksmithing", tcc.ItemQuality.Gold.UpgradeMatQtyPerImprovementPassiveRank[smithImprovementMatQtyIndex], "|H1:item:54173:34:11:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_smith_potion__sp_names_003.dds"),
		},                                  
		["Clothing"] = {                    
			["Green"] = tcc_MaterialObj(	"Hemming", 			"Hemming", 			"Clothing", tcc.ItemQuality.Green.UpgradeMatQtyPerImprovementPassiveRank[clothImprovementMatQtyIndex], "|H1:item:54174:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_light_armor_vendor_001.dds"),
			["Blue"] = tcc_MaterialObj(		"Embroidery", 		"Embroidery", 		"Clothing", tcc.ItemQuality.Blue.UpgradeMatQtyPerImprovementPassiveRank[clothImprovementMatQtyIndex], "|H1:item:54175:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_light_armor_vendor_component_002.dds"),
			["Purple"] = tcc_MaterialObj(	"Elegant Lining", 	"Elegant Lining", 	"Clothing", tcc.ItemQuality.Purple.UpgradeMatQtyPerImprovementPassiveRank[clothImprovementMatQtyIndex], "|H1:item:54176:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_runecrafter_potion_sp_name_001.dds"),
			["Gold"] = tcc_MaterialObj(		"Dreugh Wax", 		"Dreugh Wax", 		"Clothing", tcc.ItemQuality.Gold.UpgradeMatQtyPerImprovementPassiveRank[clothImprovementMatQtyIndex], "|H1:item:54177:34:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_outfitter_potion_014.dds"),
		},                                  
		["Leatherworking"] = {              
			["Green"] = tcc_MaterialObj(	"Hemming", 			"Hemming", 			"Clothing", tcc.ItemQuality.Green.UpgradeMatQtyPerImprovementPassiveRank[clothImprovementMatQtyIndex], "|H1:item:54174:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_light_armor_vendor_001.dds"),
			["Blue"] = tcc_MaterialObj(		"Embroidery", 		"Embroidery", 		"Clothing", tcc.ItemQuality.Blue.UpgradeMatQtyPerImprovementPassiveRank[clothImprovementMatQtyIndex], "|H1:item:54175:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_light_armor_vendor_component_002.dds"),
			["Purple"] = tcc_MaterialObj(	"Elegant Lining", 	"Elegant Lining", 	"Clothing", tcc.ItemQuality.Purple.UpgradeMatQtyPerImprovementPassiveRank[clothImprovementMatQtyIndex], "|H1:item:54176:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_runecrafter_potion_sp_name_001.dds"),
			["Gold"] = tcc_MaterialObj(		"Dreugh Wax", 		"Dreugh Wax", 		"Clothing", tcc.ItemQuality.Gold.UpgradeMatQtyPerImprovementPassiveRank[clothImprovementMatQtyIndex], "|H1:item:54177:34:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_outfitter_potion_014.dds"),
		},                                  
		["Woodworking"] = {                 
			["Green"] = tcc_MaterialObj(	"Pitch", 			"Pitch", 			"Woodworking", tcc.ItemQuality.Green.UpgradeMatQtyPerImprovementPassiveRank[woodImprovementMatQtyIndex], "|H1:item:54178:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_forester_weapon_vendor_component_002.dds"),
			["Blue"] = tcc_MaterialObj(		"Turpen", 			"Turpen", 			"Woodworking", tcc.ItemQuality.Blue.UpgradeMatQtyPerImprovementPassiveRank[woodImprovementMatQtyIndex], "|H1:item:54179:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_wood_turpen.dds"),
			["Purple"] = tcc_MaterialObj(	"Mastic", 			"Mastic", 			"Woodworking", tcc.ItemQuality.Purple.UpgradeMatQtyPerImprovementPassiveRank[woodImprovementMatQtyIndex], "|H1:item:54180:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_wood_mastic.dds"),
			["Gold"] = tcc_MaterialObj(		"Rosin", 			"Rosin", 			"Woodworking", tcc.ItemQuality.Gold.UpgradeMatQtyPerImprovementPassiveRank[woodImprovementMatQtyIndex], "|H1:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_wood_rosin.dds"),
		},                                  
		["Enchanting"] = {                  
			["White"] = tcc_MaterialObj(	"Ta", 				"Ta", 				"Enchanting", 1, "|H1:item:45850:20:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_003.dds"),
			["Green"] = tcc_MaterialObj(	"Jejota", 			"Jejota", 			"Enchanting", 1, "|H1:item:45851:21:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_005.dds"),
			["Blue"] = tcc_MaterialObj(		"Denata", 			"Denata", 			"Enchanting", 1, "|H1:item:45852:22:14:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_004.dds"),
			["Purple"] = tcc_MaterialObj(	"Rekuta", 			"Rekuta", 			"Enchanting", 1, "|H1:item:45853:23:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_002.dds"),
			["Gold"] = tcc_MaterialObj(		"Kuta", 			"Kuta", 			"Enchanting", 1, "|H1:item:45854:24:6:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h", "/esoui/art/icons/crafting_components_runestones_001.dds"),
		},
	}
end


--[[ Object Functions ]]--

function tcc_ItemMaterialObj(skillTier, minimumLevel, materialNameCloth, materialNameLeather, materialNameMetal, materialNameWood, materialLinkCloth, materialLinkLeather, materialLinkMetal, materialLinkWood, materialTextureCloth, materialTextureLeather, materialTextureMetal, materialTextureWood)
	return {
		["SkillTier"] = skillTier,
		["MinimumLevel"] = minimumLevel,
		["MaterialNameCloth"] = materialNameCloth,
		["MaterialNameLeather"] = materialNameLeather,
		["MaterialNameMetal"] = materialNameMetal,
		["MaterialNameWood"] = materialNameWood,
		["MaterialLinkCloth"] = materialLinkCloth,
		["MaterialLinkLeather"] = materialLinkLeather,
		["MaterialLinkMetal"] = materialLinkMetal,
		["MaterialLinkWood"] = materialLinkWood,
		["MaterialTextureCloth"] = materialTextureCloth,
		["MaterialTextureLeather"] = materialTextureLeather,
		["MaterialTextureMetal"] = materialTextureMetal,
		["MaterialTextureWood"] = materialTextureWood,
	}
end

function tcc_ItemLevelObj(itemLevel, skillTier, qtyModifierType, baseQtyCloth, baseQtyLeather, baseQtyMetal, baseQtyWood)
	return {
		["ItemLevel"] = itemLevel,
		["SkillTier"] = skillTier,
		["QtyModifierType"] = qtyModifierType,
		["BaseQtyCloth"] = baseQtyCloth,
		["BaseQtyLeather"] = baseQtyLeather,
		["BaseQtyMetal"] = baseQtyMetal,
		["BaseQtyWood"] = baseQtyWood,
	}
end

function tcc_ItemTypeObj(key, craftName, itemCategory, bodyPart, traitType, itemName, materialCategory, qtyModifier, qtyModifierV15, qtyModifierV16)
	return {
		["Key"] = key,
		["CraftName"] = craftName,
		["ItemCategory"] = itemCategory,
		["BodyPart"] = bodyPart,
		["TraitType"] = traitType,
		["ItemName"] = itemName,
		["MaterialCategory"] = materialCategory,
		["QtyModifier"] = qtyModifier,
		["QtyModifierV15"] = qtyModifierV15,
		["QtyModifierV16"] = qtyModifierV16,
	}
end

-- upgradeMatQtyPerImprovementPassiveRank is an array.
function tcc_ItemQualityExtraProperties(qualityIconObj, index, colorCode, upgradeMatQtyPerImprovementPassiveRank)
	qualityIconObj.Index = index
	qualityIconObj.ColorCode = colorCode
	qualityIconObj.UpgradeMatQtyPerImprovementPassiveRank = upgradeMatQtyPerImprovementPassiveRank
	return qualityIconObj
end

function tcc_IconObj(iconType, key, name, materialName, itemLink, normalTexture, mouseOverTexture, selectedTexture)
	return {
		IconType = iconType,
		Key = key, 
		Name = name,
		MaterialName = materialName,
		ItemLink = itemLink,
		NormalTexture = normalTexture,
		MouseOverTexture = mouseOverTexture,
		SelectedTexture = selectedTexture,
	}
end

function tcc_MaterialObj(materialKey, materialName, craftName, materialQuantity, materialLink, materialTexture)
	return {
		MaterialKey = materialKey,
		MaterialName = materialName, 
		CraftName = craftName, 
		MaterialQuantity = materialQuantity, 
		MaterialTotalQuantity = materialQuantity, 
		MaterialLink = materialLink, 
		MaterialTexture = materialTexture,
	}
end

function tcc_MaterialSubComponentObj(componentMaterialKey, componentMaterialName, componentMaterialQuantity, componentMaterialLink, componentMaterialTexture)
	return {
		ComponentMaterialKey = componentMaterialKey, 
		ComponentMaterialName = componentMaterialName,
		ComponentMaterialQuantity = componentMaterialQuantity, 
		ComponentMaterialLink = componentMaterialLink, 
		ComponentMaterialTexture = componentMaterialTexture,
	}
end

-- TODO: Flesh out the item set info.
function tcc_ItemSetObj(longName, shortName)
	return {
		LongName = longName,
		ShortName = shortName,
	}
end