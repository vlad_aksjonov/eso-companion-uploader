local localization = {
	FILTER_SUBCATEGORY_JEWELRY = "Àèæóòepèü",
	FILTER_SUBCATEGORY_GLYPHS = "Âìèñÿ",
	SUBFILTER_WEAPON_ENCHANTMENT_LABEL = "Çaùapoáaîèü opóæèü",
	SUBFILTER_WEAPON_TYPE_LABEL = "Òèï opóæèü",
	SUBFILTER_ARMOR_TYPE_LABEL = "Òèï àpoîè",
	SUBFILTER_ARMOR_ENCHANTMENT_LABEL = "Çaùapoáaîèü àpoîè",
	SUBFILTER_JEWELRY_TYPE_LABEL = "Òèï àèæóòepèè",
	SUBFILTER_JEWELRY_TRAIT_LABEL = "Ocoàeîîocòö àèæóòepèè",
	SUBFILTER_JEWELRY_ENCHANTMENT_LABEL = "Çaùapoáaîèe àèæóòepèè",
	SUBFILTER_MATERIAL_TYPE_LABEL = "Òèï íaòepèaìa",
	SUBFILTER_INGREDIENT_TYPE_LABEL = "Òèï èâpeäèeîòa",
	SUBFILTER_RUNE_TYPE_LABEL = "Òèï póîÿ",
	SUBFILTER_GLYPH_TYPE_LABEL = "Òèï âìèña",

	SUBFILTER_RECIPE_KNOWLEDGE_LABEL = "Çîaîèe peœeïòoá",
	SUBFILTER_RECIPE_KNOWLEDGE_UNKNOWN = "Îeèçáecòîÿe peœeïòÿ",
	SUBFILTER_RECIPE_KNOWLEDGE_KNOWN = "Èçáecòîÿe peœeïòÿ",

	SUBFILTER_MOTIF_KNOWLEDGE_LABEL = "Çîaîèe còèìeé",
	SUBFILTER_MOTIF_KNOWLEDGE_UNKNOWN = "Îeèçáecòîÿe còèìè",
	SUBFILTER_MOTIF_KNOWLEDGE_KNOWN = "Èçáecòîÿe còèìè",

	SUBFILTER_TRAIT_KNOWLEDGE_LABEL = "Ocoàeîîocòè",
	SUBFILTER_TRAIT_KNOWLEDGE_UNKNOWN = "Îeèçáecòîÿe ocoàeîîocòè",
	SUBFILTER_TRAIT_KNOWLEDGE_KNOWN = "Èçáecòîÿe ocoàeîîocòè",

	SUBFILTER_RUNE_KNOWLEDGE_LABEL = "Póîÿ",
	SUBFILTER_RUNE_KNOWLEDGE_UNKNOWN = "Îeèçáecòîÿe póîÿ",
	SUBFILTER_RUNE_KNOWLEDGE_KNOWN = "Èçáecòîÿe póîÿ",

	SUBFILTER_ITEM_SET_LABEL = "Îaàop",
	SUBFILTER_ITEM_SET_NORMAL = "Oäèîoùîÿé ïpeäíeò",
	SUBFILTER_ITEM_SET_HAS_SET = "Ïpeäíeò îaàopa",

	SUBFILTER_CRAFTING_LABEL = "Peíecìeîîoe",
	SUBFILTER_CRAFTING_IS_CRAFTED = "Cäeìaîîÿé ïpeäíeò",
	SUBFILTER_CRAFTING_IS_LOOT = "Îaéäeîîÿé ïpeäíeò",

	SUBFILTER_RECIPE_IMPROVEMENT_LABEL = "Ópoáeîö peœeïòoá",
	SUBFILTER_RECIPE_IMPROVEMENT_TOOLTIP = "Peœeïò äìü ópoáîeé <<1>> <<2>>",

	AUTO_SEARCH_TOGGLE_LABEL = "Áêìôùèòö aáòoïoècê",
	SEARCH_PREVIOUS_PAGE_LABEL = "Ïoêaçaòö ïpeä. còp.",
	SEARCH_SHOW_MORE_LABEL = "Ïoêaçaòö àoìöúe peçóìöòaòoá",
	RESET_ALL_FILTERS_LABEL = "Càpocèòö áce ñèìöòpÿ",
	RESET_FILTER_LABEL_TEMPLATE = "Càpocèòö ñèìöòp %s",

	CATEGORY_TITLE = "Êaòeâopèü",
	SUBCATEGORY_TITLE = "Ïoäêaòeâopèü",

	QUALITY_SELECTOR_TITLE = "Êaùecòáo:",

	TEXT_FILTER_TITLE = "Òeêcòoáÿé ñèìöòp:",
	TEXT_FILTER_TEXT = "Ñèìöòp ïo òeêcòó",

	UNIT_PRICE_FILTER_TITLE = "Ñèìöòp ïo œeîaí:",

	WARNING_SUBFILTER_LIMIT = "Îeìöçü oòoàpaòö àoìee ùeí ïo 8 ñèìöòpaí",

	SEARCH_LIBRARY_TOGGLE_LABEL = "Àèàìèoòeêa ïoècêa",
	SEARCH_LIBRARY_HISTORY_LABEL = "Ècòopèü",
	SEARCH_LIBRARY_FAVORITES_LABEL = "Èçàpaîîoe",
	SEARCH_LIBRARY_FAVORITE_BUTTON_ADD_TOOLTIP = "Äoàaáèòö á èçàpaîîoe",
	SEARCH_LIBRARY_FAVORITE_BUTTON_REMOVE_TOOLTIP = "Óäaìèòö èç èçàpaîîoâo",
	SEARCH_LIBRARY_EDIT_LABEL_BUTTON_TOOLTIP = "Ïepeèíeîoáaòö",
	SEARCH_LIBRARY_DELETE_LABEL_BUTTON_TOOLTIP = "Óäaìèòö èç ècòopèè",
	SEARCH_LIBRARY_MENU_OPEN_SETTINGS = "Oòêpÿòö îacòpoéêè aääoîa",
	SEARCH_LIBRARY_MENU_CLEAR_HISTORY = "Oùècòèòö ècòopèô",
	SEARCH_LIBRARY_MENU_CLEAR_FAVORITES = "Oùècòèòö èçàpaîîoe",
	SEARCH_LIBRARY_MENU_UNDO_ACTION = "Oòíeîèòö ïocìeäîee äeécòáèe",
	SEARCH_LIBRARY_MENU_UNLOCK_WINDOW = "Paçàìoêèpoáaòö oêîo",
	SEARCH_LIBRARY_MENU_LOCK_WINDOW = "Çaàìoêèpoáaòö oêîo",
	SEARCH_LIBRARY_MENU_RESET_WINDOW = "Càpocèòö ïoìoæeîèe oêîa",
	SEARCH_LIBRARY_MENU_CLOSE_WINDOW = "Çaêpÿòö oêîo",
	SEARCH_LIBRARY_SORT_HEADER_NAME = "Îaçáaîèe",
	SEARCH_LIBRARY_SORT_HEADER_SEARCHES = "Ïoècêè",

	TOOLTIP_LESS_THAN = "íeîöúe ",
	TOOLTIP_GREATER_THAN = "àoìöúe ",

	MAIL_AUGMENTATION_MESSAGE_BODY = "Áÿ ïpoäaìè <<2>> <<t:1>> <<3>> ça <<4>>.",
	MAIL_AUGMENTATION_INVOICE_COMMISSION = "Êoíèccèü",
	MAIL_AUGMENTATION_INVOICE_LISTING_FEE_REFUND = GetString(SI_TRADING_HOUSE_POSTING_LISTING_FEE) .. " (áoçápaò)",
	MAIL_AUGMENTATION_REQUEST_DATA = "Çaâpóçèòö äeòaìè",

	SETTINGS_REQUIRES_RELOADUI_WARNING = "Àóäeò ïpèíeîeîo òoìöêo ïocìe ïepeçaâpóçêè UI",
	SETTINGS_KEEP_FILTERS_ON_CLOSE_LABEL = "Çaïoíèîaòö ñèìöòpÿ íeæäó áèçèòaíè á íaâaçèî",
	SETTINGS_KEEP_FILTERS_ON_CLOSE_DESCRIPTION = "Çaïoíèîaeò áÿàpaîîÿe áaíè ñèìöòpÿ è ïpè ïocìeäóôûeí áèçèòe á íaâaçèî ïocìeäîèe ècïoìöçoáaîîÿe ñèìöòpÿ óæe àóäóò áêìôùeîÿ",
	SETTINGS_OLD_QUALITY_SELECTOR_BEHAVIOR_LABEL = "Ècïoìöçoáaòö còapÿé còèìö áÿàopa êaùecòáa",
	SETTINGS_OLD_QUALITY_SELECTOR_BEHAVIOR_DESCRIPTION = "Ecìè áêìôùeîo, ìeáÿé è ïpaáÿé êìèê ïoáÿúaôò è ïoîèæaôò êaùecòáo, a äáoéîoé êìèê èìè êìèê c úèñòoí ïoçáoìüeò áÿàpaòö îecêoìöêo çîaùeîèé",
	SETTINGS_DISPLAY_PER_UNIT_PRICE_LABEL = "Ïoêaçÿáaòö œeîó ça oäèî ïpeäíeò",
	SETTINGS_DISPLAY_PER_UNIT_PRICE_DESCRIPTION = "Êoâäa áêìôùeîo, ïoêaçÿáaeò œeîó ça eäèîèœó òoáapa, áíecòo còoèíocòè còaêa",
	SETTINGS_SORT_WITHOUT_SEARCH_LABEL = "Íeîüòö ïopüäoê àeç ïoècêa",
	SETTINGS_SORT_WITHOUT_SEARCH_DESCRIPTION = "Ïoçáoìüeò íeîüòö ïopüäoê copòèpoáêè àeç îeoàxoäèíocòè çaïócêa îoáoâo ïoècêa. Òeêóûèe peçóìöòaòÿ ïoècêa èçíeîüòcü òoìöêo ïocìe póùîoâo ïoècêa.",
	SETTINGS_KEEP_SORTORDER_ON_CLOSE_LABEL = "Çaïoíèîaòö copòèpoáó",
	SETTINGS_KEEP_SORTORDER_ON_CLOSE_DESCRIPTION = "Çaïoíèîaeò ïopüäoê copòèpoáêè ïpeäíeòoá íeæäó áèçèòaíè á íaâaçî áíecòo eâo càpoca.",
	SETTINGS_LIST_WITH_SINGLE_CLICK_LABEL = "Áÿcòaáìüòö oäîèí êìèêoí",
	SETTINGS_LIST_WITH_SINGLE_CLICK_DESCRIPTION = "Áÿcòaáìüòö ïpeäíeò îa ïpoäaæó ïo oäîoíó êìèêó íÿúè.",
	SETTINGS_SHOW_SEARCH_LIBRARY_TOOLTIPS_LABEL = "Ïoäcêaçêè á àèàìèoòeêe ïoècêa",
	SETTINGS_SHOW_SEARCH_LIBRARY_TOOLTIPS_DESCRIPTION = "Êoâäa áêìôùeîo, àóäeò áÿáoäèòcü äeòaìöîaü èîñopíaœèü äì êaæäoé çaïècè á àèàìèoòeêe ïoècêa.",
	SETTINGS_SHOW_TRADER_TOOLTIPS_LABEL = "Ïoäcêaçêa òopâoáœa",
	SETTINGS_SHOW_TRADER_TOOLTIPS_DESCRIPTION = "Ïoêaçÿáaeò îaîüòoâo áaúèíè âèìöäèüíè òopâoáœa, êoâäa îaáoäèòe îa îaçáaîèe âèìöäèè á cïècêe âèìöäèé",
	SETTINGS_AUTO_CLEAR_HISTORY_LABEL = "Aáòooùècòêa ècòopèè",
	SETTINGS_AUTO_CLEAR_HISTORY_DESCRIPTION = "Aáòoíaòèùecêè oùèûaeò ácô ècòopèô ïpè ïepáoí c îaùaìa èâpoáoé ceccèè áèçèòe ê òopâoáœó. Áÿíoæeòe oòíeîèòö óäaìeîèe á íeîô ècòopèè ïoècêa",
	SETTINGS_MAIL_AUGMENTATION_LABEL = "Ïoùòoáoe pacúèpeîèe",
	SETTINGS_MAIL_AUGMENTATION_DESCRIPTION = "Äoàaáìüeò àoìee äeòaìöîóô èîñopíaœèô o cäeìêe áo áxoäüûee ïècöío oò Âèìöäeécêoâo Íaâaçèîa, ecìè èîñopíaœèü äocòóïîa á ìoâe aêòèáîocòè âèìöäèè.",
	SETTINGS_MAIL_AUGMENTATION_INVOICE_LABEL = "Cùeò á ïècöíax",
	SETTINGS_MAIL_AUGMENTATION_INVOICE_DESCRIPTION = "Äoàaáìüeò äeòaìöîÿé cùeò á ïècöía, êoòopÿé coäepæèò èîñopíaœèô oào ácex ïocòóïìeîèüx.",
	SETTINGS_PURCHASE_NOTIFICATION_LABEL = "Oïoáeûeîèe o ïoêóïêe",
	SETTINGS_PURCHASE_NOTIFICATION_DESCRIPTION = "Ïoêaçÿáaeò cooàûeîèe á ùaòe ïocìe coáepúeîèü ïoêóïêè áaúeâo òoáapa á âèìöäeécêoí íaâaçèîe",
	SETTINGS_CANCEL_NOTIFICATION_LABEL = "Ïpeäóïpeæäeîèü oòçÿáa",
	SETTINGS_CANCEL_NOTIFICATION_DESCRIPTION = "Ïoêaçÿáaeò cooàûeîèe á ùaòe, ïocìe òoâo êaê áÿ oòoçáaìè ïpeäíeò èç ïpoäaæè á âèìöäeécêoí íaâaçèîe",
	SETTINGS_LISTED_NOTIFICATION_LABEL = "Ïpeäóïpeæäeîèe o paçíeûeîèè",
	SETTINGS_LISTED_NOTIFICATION_DESCRIPTION = "Ïoêaçÿáaeò cooàûeîèe á ùaòe, ïocìe òoâo êaê áÿ paçíecòèìè ïpeäíeò îa ïpoäaæó á âèìöäeécêoí íaâaçèîe",
	SETTINGS_PURCHASE_NOTIFICATION_DESCRIPTION = "Ïoêaçÿáaeò cooàûeîèe á ùaòe, ïocìe òoâo êaê áÿ êóïèìè ïpeäíeò á âèìöäeécêoí íaâaçèîe",
	SETTINGS_DISABLE_CUSTOM_SELL_TAB_FILTER_LABEL = "Oòêìôùèòö ñèìöòpÿ aääoîa",
	SETTINGS_DISABLE_CUSTOM_SELL_TAB_FILTER_DESCRIPTION = "Ïoêaçÿáaeò còaîäapòîÿe èâpoáÿe ñèìöòpÿ èîáeîòapü áíecòo ñèìöòpoá aääoîa AGS, ecìè áÿêìôùeîo.",
	SETTINGS_SKIP_GUILD_KIOSK_DIALOG_LABEL = "Ïpoïócòèòö äèaìoâ êèocêa",
	SETTINGS_SKIP_GUILD_KIOSK_DIALOG_DESCRIPTION = "Êoâäa áêìôùeîo, äèaìoâ âèìöäeécêoâo òopâoáœa (îe àaîêèpa) ïpoïócêaeòcü è íaâaçèî oòêpÿáaeòcü cpaçó aáòoíaòèùecêè. Áÿ cíoæeòe îaæaòö è óäepæèáaòö Shift ïpè îaùaìe äèaìoâa c âèìöäeécêèí òopâoáœeí, ùòoàÿ óáèäeòö còaîäapòîÿé äèaìoâ ïpè áêìôùeîèè ëòoé ñóîêœèè.",
	SETTINGS_SKIP_EMPTY_PAGES_LABEL = "Ïpoïócêaòö ïócòÿe còpaîèœÿ",
	SETTINGS_SKIP_EMPTY_PAGES_DESCRIPTION = "Êoâäa áêìôùeîo, còpaîèœÿ, îa êoòopÿx îeò peçóìöòaòoá ïoècêa, àóäóò aáòoíaòèùecêè ïpoïócêaòöcü è àóäeò coáepúeî ïepexoä ê cìeäóôûeé còpaîèœe. Ëòó îacòpoéêó íoæîo ïpoèâîopèpoáaòö, ecìè îaæaòö êîoïêó ctrl ïepeä áÿáoäoí peçóìöòaòoá.",

	CONTROLS_SUPPRESS_LOCAL_FILTERS = "Èâîopèpoáaòö ñèìöòpÿ",

	INVALID_STATE = "Oúèàêa.\nËòo àaâ èâpÿ è oî äoìæeî àÿòö ïoïpaáìeî á àìèæaéúee ápeíü.",

	LOCAL_FILTER_EXPLANATION_TOOLTIP = "Ëòoò ñèìöòp ìoêaìöîÿé è paàoòaeò òoìöêo äìü oòoàpaæaeíoé còpaîèœÿ",

	PURCHASE_NOTIFICATION = "Áÿ êóïèìè <<1>>x <<t:2>> ó <<3>> ça <<4>> á <<5>>",
	CANCEL_NOTIFICATION = "Áÿ oòoçáaìè èç ïpoäaæè <<1>>x <<t:2>> ça <<3>> á <<4>>",
	LISTED_NOTIFICATION = "Áÿ paçíecòèìè îa ïpoäaæó <<1>>x <<t:2>> ça <<3>> á <<4>>",
}
ZO_ShallowTableCopy(localization, AwesomeGuildStore.Localization)
