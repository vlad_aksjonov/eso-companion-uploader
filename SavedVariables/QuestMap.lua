QuestMapSettings =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Bswan"] = 
            {
                ["iconSet"] = "QuestMap",
                ["pinFilters"] = 
                {
                    ["Quest_completed"] = false,
                    ["Quest_uncompleted_pvp"] = false,
                    ["Quest_skill"] = false,
                    ["Quest_started"] = false,
                    ["Quest_uncompleted"] = false,
                    ["Quest_completed_pvp"] = false,
                    ["Quest_cadwell"] = false,
                    ["Quest_started_pvp"] = false,
                    ["Quest_hidden_pvp"] = false,
                    ["Quest_hidden"] = false,
                },
                ["version"] = 1,
                ["hiddenQuests"] = 
                {
                    [4953] = "Trouble at the Tree",
                    [4258] = "New in Town",
                    [4549] = "Back to Skywatch",
                    [4357] = "To Firsthold",
                    [4350] = "Faith in the Family",
                    [4767] = "One of the Undaunted",
                },
                ["displayClickMsg"] = true,
                ["pinLevel"] = 40,
                ["pinSize"] = 25,
                ["lastListArg"] = "completed",
            },
            ["Lb-Grocery"] = 
            {
                ["iconSet"] = "QuestMap",
                ["pinFilters"] = 
                {
                    ["Quest_completed"] = false,
                    ["Quest_uncompleted_pvp"] = false,
                    ["Quest_skill"] = false,
                    ["Quest_started"] = false,
                    ["Quest_uncompleted"] = true,
                    ["Quest_completed_pvp"] = false,
                    ["Quest_cadwell"] = false,
                    ["Quest_started_pvp"] = false,
                    ["Quest_hidden_pvp"] = false,
                    ["Quest_hidden"] = false,
                },
                ["version"] = 1,
                ["hiddenQuests"] = 
                {
                },
                ["displayClickMsg"] = true,
                ["pinLevel"] = 40,
                ["pinSize"] = 25,
                ["lastListArg"] = "uncompleted",
            },
            ["Purr-Purr'Dar"] = 
            {
                ["iconSet"] = "QuestMap",
                ["pinFilters"] = 
                {
                    ["Quest_completed"] = false,
                    ["Quest_uncompleted_pvp"] = false,
                    ["Quest_skill"] = false,
                    ["Quest_started"] = false,
                    ["Quest_uncompleted"] = true,
                    ["Quest_completed_pvp"] = false,
                    ["Quest_cadwell"] = false,
                    ["Quest_started_pvp"] = false,
                    ["Quest_hidden_pvp"] = false,
                    ["Quest_hidden"] = false,
                },
                ["version"] = 1,
                ["hiddenQuests"] = 
                {
                    [4974] = "Brackenleaf's Briars",
                },
                ["displayClickMsg"] = true,
                ["pinLevel"] = 40,
                ["pinSize"] = 25,
                ["lastListArg"] = "uncompleted",
            },
            ["Lb-Alchemy"] = 
            {
                ["iconSet"] = "QuestMap",
                ["pinFilters"] = 
                {
                    ["Quest_completed"] = false,
                    ["Quest_uncompleted_pvp"] = false,
                    ["Quest_skill"] = false,
                    ["Quest_started"] = false,
                    ["Quest_uncompleted"] = true,
                    ["Quest_completed_pvp"] = false,
                    ["Quest_cadwell"] = false,
                    ["Quest_started_pvp"] = false,
                    ["Quest_hidden_pvp"] = false,
                    ["Quest_hidden"] = false,
                },
                ["version"] = 1,
                ["hiddenQuests"] = 
                {
                },
                ["displayClickMsg"] = true,
                ["pinLevel"] = 40,
                ["pinSize"] = 25,
                ["lastListArg"] = "uncompleted",
            },
            ["Razum-Dar in Excile"] = 
            {
                ["iconSet"] = "QuestMap",
                ["pinFilters"] = 
                {
                    ["Quest_completed"] = false,
                    ["Quest_uncompleted_pvp"] = false,
                    ["Quest_skill"] = false,
                    ["Quest_started"] = false,
                    ["Quest_uncompleted"] = true,
                    ["Quest_completed_pvp"] = false,
                    ["Quest_cadwell"] = false,
                    ["Quest_started_pvp"] = false,
                    ["Quest_hidden_pvp"] = false,
                    ["Quest_hidden"] = false,
                },
                ["version"] = 1,
                ["hiddenQuests"] = 
                {
                },
                ["displayClickMsg"] = true,
                ["pinLevel"] = 40,
                ["pinSize"] = 25,
                ["lastListArg"] = "uncompleted",
            },
            ["Testimate"] = 
            {
                ["iconSet"] = "QuestMap",
                ["pinFilters"] = 
                {
                    ["Quest_completed"] = false,
                    ["Quest_uncompleted_pvp"] = false,
                    ["Quest_skill"] = false,
                    ["Quest_started"] = false,
                    ["Quest_uncompleted"] = false,
                    ["Quest_completed_pvp"] = false,
                    ["Quest_cadwell"] = false,
                    ["Quest_started_pvp"] = false,
                    ["Quest_hidden_pvp"] = false,
                    ["Quest_hidden"] = false,
                },
                ["version"] = 1,
                ["hiddenQuests"] = 
                {
                },
                ["displayClickMsg"] = true,
                ["pinLevel"] = 40,
                ["pinSize"] = 25,
                ["lastListArg"] = "uncompleted",
            },
            ["Lb-Enchanting"] = 
            {
                ["iconSet"] = "QuestMap",
                ["pinFilters"] = 
                {
                    ["Quest_completed"] = false,
                    ["Quest_uncompleted_pvp"] = false,
                    ["Quest_skill"] = false,
                    ["Quest_started"] = false,
                    ["Quest_uncompleted"] = true,
                    ["Quest_completed_pvp"] = false,
                    ["Quest_cadwell"] = false,
                    ["Quest_started_pvp"] = false,
                    ["Quest_hidden_pvp"] = false,
                    ["Quest_hidden"] = false,
                },
                ["version"] = 1,
                ["hiddenQuests"] = 
                {
                },
                ["displayClickMsg"] = true,
                ["pinLevel"] = 40,
                ["pinSize"] = 25,
                ["lastListArg"] = "uncompleted",
            },
        },
    },
}
