FyrMMSV =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["$AccountWide"] = 
            {
                ["MapTable"] = 
                {
                    ["banditden5_base_0"] = 
                    {
                        ["ZoneId"] = 465,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["innertanzelwil_base_0"] = 
                    {
                        ["ZoneId"] = 211,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["tormentedspireinstance_base_0"] = 
                    {
                        ["ZoneId"] = 9,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 368.9000000000,
                    },
                    ["imperialsewers_aldmeri1_base_0"] = 
                    {
                        ["ZoneId"] = 390,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 8975,
                        ["MapSize"] = 1,
                    },
                    ["dune_base_0"] = 
                    {
                        ["ZoneId"] = 179,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 5853,
                        ["MapSize"] = 875.1800000000,
                    },
                    ["rootsunder_base_0"] = 
                    {
                        ["ZoneId"] = 20,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 484.5700000000,
                    },
                    ["the_ebonheart_harborage_base_0"] = 
                    {
                        ["ZoneId"] = 9,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 161.9800000000,
                    },
                    ["gilvardelleabandoncave_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["greenshade_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 7196,
                        ["MapSize"] = 3916.1200000000,
                    },
                    ["vaultofhamanforgefire_base_0"] = 
                    {
                        ["ZoneId"] = 213,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 327.5100000000,
                    },
                    ["aldanobia_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["rajhinsvaultsmallroom_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 89.8000000000,
                    },
                    ["hallsoftorment1_base_0"] = 
                    {
                        ["ZoneId"] = 57,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 608.7100000000,
                    },
                    ["shornhelm_base_0"] = 
                    {
                        ["ZoneId"] = 5,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 1211,
                        ["MapSize"] = 629.6200000000,
                    },
                    ["narsis_base_0"] = 
                    {
                        ["ZoneId"] = 10,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3514,
                        ["MapSize"] = 636.1100000000,
                    },
                    ["hewsbane_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 9.8600000000,
                        ["subZoneId"] = 10009,
                        ["MapSize"] = 3330.0300000000,
                    },
                    ["foundryofwoe_base_0"] = 
                    {
                        ["ZoneId"] = 46,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["auridon_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3685,
                        ["MapSize"] = 5350.4000000000,
                    },
                    ["hiradirgecitadeltg6_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["rootsofsilvenar_base_0"] = 
                    {
                        ["ZoneId"] = 257,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 487.1000000000,
                    },
                    ["athimahmanson01_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                    },
                    ["catseyequay_base_0"] = 
                    {
                        ["ZoneId"] = 294,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["therefugeofdread_base_0"] = 
                    {
                        ["ZoneId"] = 190,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["islesoftorment_base_0"] = 
                    {
                        ["ZoneId"] = 315,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 829.9000000000,
                    },
                    ["ebonheart_base_0"] = 
                    {
                        ["ZoneId"] = 9,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2474,
                        ["MapSize"] = 1007.0500000000,
                    },
                    ["narilnagaia_base_0"] = 
                    {
                        ["ZoneId"] = 330,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 404.8300000000,
                    },
                    ["tormented_spire_base_0"] = 
                    {
                        ["ZoneId"] = 269,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 642.7200000000,
                    },
                    ["safehouse_base_0"] = 
                    {
                        ["ZoneId"] = 518,
                        ["ZoomLevel"] = 10,
                    },
                    ["khenarthisroost_base_0"] = 
                    {
                        ["ZoneId"] = 294,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 4788,
                        ["MapSize"] = 1,
                    },
                    ["sancretor5_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 220.7600000000,
                    },
                    ["maw_of_lorkaj_base_0"] = 
                    {
                        ["ZoneId"] = 179,
                        ["ZoomLevel"] = 10,
                    },
                    ["wailingprison1_base_0"] = 
                    {
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["wayrest_base_0"] = 
                    {
                        ["ZoneId"] = 4,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 4448,
                        ["MapSize"] = 927.4400000000,
                    },
                    ["gladeofthedivinevuldngrav_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["valleyofblades2_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 288.6700000000,
                    },
                    ["rajhinsvault_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 352.8600000000,
                    },
                    ["windhelm_base_0"] = 
                    {
                        ["ZoneId"] = 15,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3036,
                        ["MapSize"] = 713.1400000000,
                    },
                    ["weepingwindcave_base_0"] = 
                    {
                        ["ZoneId"] = 251,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 383.0500000000,
                    },
                    ["alikr_base_0"] = 
                    {
                        ["ZoneId"] = 17,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 5572,
                        ["MapSize"] = 4680.2800000000,
                    },
                    ["sancretor7_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 279.5200000000,
                    },
                    ["banditden8_base_0"] = 
                    {
                        ["ZoneId"] = 467,
                        ["ZoomLevel"] = 12,
                        ["MapSize"] = 1,
                    },
                    ["arenasclockworkint_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 9951,
                        ["MapSize"] = 325.1800000000,
                    },
                    ["themiddens_base_0"] = 
                    {
                        ["ZoneId"] = 304,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 453.1500000000,
                    },
                    ["innerseaarmature_base_0"] = 
                    {
                        ["ZoneId"] = 113,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 482.0200000000,
                    },
                    ["southpoint_base_0"] = 
                    {
                        ["ZoneId"] = 226,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["courtofcontempt_base_0"] = 
                    {
                        ["ZoneId"] = 154,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 4940,
                        ["MapSize"] = 151.1800000000,
                    },
                    ["fortvirakruin_base_0"] = 
                    {
                        ["ZoneId"] = 182,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 212.8600000000,
                    },
                    ["varoestatecaves01_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                    },
                    ["varoestatecellar_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                    },
                    ["shorsstone_base_0"] = 
                    {
                        ["ZoneId"] = 16,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2445,
                        ["MapSize"] = 752.4100000000,
                    },
                    ["malabaltor_base_0"] = 
                    {
                        ["ZoneId"] = 11,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2840,
                        ["MapSize"] = 4327.0800000000,
                    },
                    ["mzendeldt_base_0"] = 
                    {
                        ["ZoneId"] = 49,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["crimsoncove02_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["anvilcity_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 10191,
                    },
                    ["rawlkha_base_0"] = 
                    {
                        ["ZoneId"] = 179,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 5856,
                        ["MapSize"] = 559.0700000000,
                    },
                    ["crackedwoodcave_base_0"] = 
                    {
                        ["ZoneId"] = 272,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["hallsofsubmission_base_0"] = 
                    {
                        ["ZoneId"] = 51,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["crimsoncove_base_0"] = 
                    {
                        ["ZoneId"] = 26,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 572.0900000000,
                    },
                    ["wansalen_base_0"] = 
                    {
                        ["ZoneId"] = 194,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["mournhold_base_0"] = 
                    {
                        ["ZoneId"] = 10,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2092,
                        ["MapSize"] = 1105.2100000000,
                    },
                    ["gurzagsmine_base_0"] = 
                    {
                        ["ZoneId"] = 328,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 389.2500000000,
                    },
                    ["sancretor6_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 211.5700000000,
                    },
                    ["delsclaim_base_0"] = 
                    {
                        ["ZoneId"] = 192,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["firsthold_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 876.5200000000,
                    },
                    ["ateliertwicebornstar_base_0"] = 
                    {
                        ["ZoneId"] = 353,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 98.0200000000,
                    },
                    ["varoestatetunnels02_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                    },
                    ["southruins_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["barkbitecave_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 226.0800000000,
                    },
                    ["bahrahasgloom_secret3_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["rulanyilsfall_base_0"] = 
                    {
                        ["ZoneId"] = 25,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 504.1900000000,
                    },
                    ["belkarth_base_0"] = 
                    {
                        ["ZoneId"] = 353,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 7638,
                        ["MapSize"] = 658.6200000000,
                    },
                    ["ashmountain_base_0"] = 
                    {
                        ["ZoneId"] = 42,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 421.3700000000,
                    },
                    ["glenumbra_base_0"] = 
                    {
                        ["ZoneId"] = 2,
                        ["ZoomLevel"] = 9.9700000000,
                        ["subZoneId"] = 2321,
                        ["MapSize"] = 4963.7800000000,
                    },
                    ["buraniim_base_0"] = 
                    {
                        ["ZoneId"] = 297,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["ragnthar_base_0"] = 
                    {
                        ["ZoneId"] = 181,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 258.7800000000,
                    },
                    ["malabaltoroutlawrefuge_base_0"] = 
                    {
                        ["ZoneId"] = 449,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 258.5700000000,
                    },
                    ["sancretor8_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 218.1100000000,
                    },
                    ["eldenrootfightersguildup_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6633,
                        ["MapSize"] = 244.7800000000,
                    },
                    ["banditden4_room4_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["marbruk_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 5412,
                        ["MapSize"] = 579.9000000000,
                    },
                    ["lipsandtarn_base_0"] = 
                    {
                        ["ZoneId"] = 276,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["orkeyshollow_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 435.2100000000,
                    },
                    ["vulkhelguard_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 11,
                        ["subZoneId"] = 3685,
                        ["MapSize"] = 1064.5900000000,
                    },
                    ["sharktoothgrotto2_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["thevilemansefirstfloor_base_0"] = 
                    {
                        ["ZoneId"] = 179,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 596.4500000000,
                    },
                    ["eastmarch_base_0"] = 
                    {
                        ["ZoneId"] = 15,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3043,
                        ["MapSize"] = 5350.4000000000,
                    },
                    ["banditden8_room2_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["bergama_base_0"] = 
                    {
                        ["ZoneId"] = 17,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 7531,
                        ["MapSize"] = 714.5500000000,
                    },
                    ["undergroundsepulcher_cave_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["abahslanding_sulima_base_0"] = 
                    {
                        ["ZoneId"] = 520,
                        ["ZoomLevel"] = 10,
                    },
                    ["hiradirgecitadeltg3_s1_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["castleoftheworm4_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 936.5900000000,
                    },
                    ["coralheartchamber_base_0"] = 
                    {
                        ["ZoneId"] = 77,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 218.1900000000,
                    },
                    ["villageofthelost_base_0"] = 
                    {
                        ["ZoneId"] = 311,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1503.7100000000,
                    },
                    ["blisslower_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 111.6000000000,
                    },
                    ["hozzinsfolley_base_0"] = 
                    {
                        ["ZoneId"] = 109,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3758,
                        ["MapSize"] = 352.1100000000,
                    },
                    ["reliquaryofstars_base_0"] = 
                    {
                        ["ZoneId"] = 185,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 536.8900000000,
                    },
                    ["veiledkeepbase_0"] = 
                    {
                        ["ZoneId"] = 186,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["alcairecastle_base_0"] = 
                    {
                        ["ZoneId"] = 4,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 11,
                        ["MapSize"] = 582.6100000000,
                    },
                    ["cormountprison_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 81.0100000000,
                    },
                    ["the_aldmiri_harborage_map_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 152.5500000000,
                    },
                    ["mobarmine_base_0"] = 
                    {
                        ["ZoneId"] = 234,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 394.5200000000,
                    },
                    ["deathhollowhalls_base_0"] = 
                    {
                        ["ZoneId"] = 466,
                        ["ZoomLevel"] = 10,
                    },
                    ["sulimamansion_floor1_hidden_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["hubalajadpalaceint_02_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["eldenrootfightersguildown_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6637,
                        ["MapSize"] = 244.7800000000,
                    },
                    ["temple_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 16.7400000000,
                    },
                    ["hectahamegrottomain_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6258,
                        ["MapSize"] = 880.7400000000,
                    },
                    ["vulkwasten_base_0"] = 
                    {
                        ["ZoneId"] = 11,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2837,
                        ["MapSize"] = 715.8800000000,
                    },
                    ["karthdar_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 265.1800000000,
                    },
                    ["orrery_base_0"] = 
                    {
                        ["ZoneId"] = 225,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 333.8400000000,
                    },
                    ["sacredleapgrotto_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["sulimamansion_floor2_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["castleoftheworm3_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 240.7800000000,
                    },
                    ["bahrahasgloom_secret1_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["albeceansea_base_0"] = 
                    {
                        ["ZoneId"] = 309,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 209.1000000000,
                    },
                    ["coromount_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 284.8600000000,
                    },
                    ["imperialundergroundpart2_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 375.9400000000,
                    },
                    ["barrowtrench_base_0"] = 
                    {
                        ["ZoneId"] = 332,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 391.2000000000,
                    },
                    ["greymire_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["starvedplanes_base_0"] = 
                    {
                        ["ZoneId"] = 9,
                        ["ZoomLevel"] = 9.8600000000,
                        ["MapSize"] = 1,
                    },
                    ["daggerfall_base_0"] = 
                    {
                        ["ZoneId"] = 2,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2327,
                        ["MapSize"] = 1039.5000000000,
                    },
                    ["thevilemansesecondfloor_base_0"] = 
                    {
                        ["ZoneId"] = 179,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 5560,
                        ["MapSize"] = 594.4000000000,
                    },
                    ["kunasdelve_base_0"] = 
                    {
                        ["ZoneId"] = 248,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 417.4700000000,
                    },
                    ["baandaritradingpost_base_0"] = 
                    {
                        ["ZoneId"] = 11,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2841,
                        ["MapSize"] = 637.3700000000,
                    },
                    ["athimahcave01_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 10805,
                    },
                    ["eldenrootcrafting_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6706,
                        ["MapSize"] = 244.7600000000,
                    },
                    ["undergroundsepulcher_tunnel_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["knightsgrave01_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 10491,
                    },
                    ["blackvineruins_base_0"] = 
                    {
                        ["ZoneId"] = 258,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 394.6000000000,
                    },
                    ["imperialsewer_ebonheart3b_base_0"] = 
                    {
                        ["ZoneId"] = 390,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["softloamcavern_base_0"] = 
                    {
                        ["ZoneId"] = 115,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 463.3600000000,
                    },
                    ["dessicatedcave_base_0"] = 
                    {
                        ["ZoneId"] = 259,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 400.6000000000,
                    },
                    ["vilemansehouse01_base_0"] = 
                    {
                        ["ZoneId"] = 179,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 5559,
                        ["MapSize"] = 71.5000000000,
                    },
                    ["hubalajadpalaceint_02_h_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["fortamol_base_0"] = 
                    {
                        ["ZoneId"] = 15,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2250,
                        ["MapSize"] = 658.2900000000,
                    },
                    ["thehideaway_floor2_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["hubalajadpalaceint_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["secludedsewers_room3_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["wailingprison2_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["craglorn_dragonstar_base_0"] = 
                    {
                        ["ZoneId"] = 353,
                        ["ZoomLevel"] = 11,
                        ["subZoneId"] = 8631,
                        ["MapSize"] = 458.4600000000,
                    },
                    ["blackwoodsborderlands01_base_0"] = 
                    {
                        ["ZoneId"] = 538,
                        ["ZoomLevel"] = 10,
                    },
                    ["aldcroft_base_0"] = 
                    {
                        ["ZoneId"] = 2,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 666.2200000000,
                    },
                    ["bleakrock_base_0"] = 
                    {
                        ["ZoneId"] = 109,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2609,
                        ["MapSize"] = 1963.4000000000,
                    },
                    ["westelsweyrgate_base_0"] = 
                    {
                        ["ZoneId"] = 37,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3357,
                        ["MapSize"] = 600.1400000000,
                    },
                    ["eldenrootthroneroom_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 244.8400000000,
                    },
                    ["fardirsfolly_base_0"] = 
                    {
                        ["ZoneId"] = 249,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 338.7600000000,
                    },
                    ["blackwoodsborderlands02_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 10703,
                    },
                    ["koeglinvillage_base_0"] = 
                    {
                        ["ZoneId"] = 4,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 12,
                        ["MapSize"] = 551.2400000000,
                    },
                    ["secludedsewers_room1_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["imperialundergroundpart1_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 154.0700000000,
                    },
                    ["goldcoast_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 9.9600000000,
                        ["subZoneId"] = 10644,
                    },
                    ["woodhearth_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 5417,
                        ["MapSize"] = 1026.8000000000,
                    },
                    ["shademistenclave_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 463.6800000000,
                    },
                    ["abandonedmine_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["tomboftheapostates_base_0"] = 
                    {
                        ["ZoneId"] = 254,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 400.6600000000,
                    },
                    ["skywatch_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3650,
                        ["MapSize"] = 909.1600000000,
                    },
                    ["stormhold_base_0"] = 
                    {
                        ["ZoneId"] = 19,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 634.7700000000,
                    },
                    ["castleoftheworm2_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 427.1500000000,
                    },
                    ["reliquaryvaulttop_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 120.1700000000,
                    },
                    ["castleanvil01_base_0"] = 
                    {
                        ["ZoneId"] = 527,
                        ["ZoomLevel"] = 10,
                    },
                    ["skyshroudbarrow_base_0"] = 
                    {
                        ["ZoneId"] = 109,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 160.9000000000,
                    },
                    ["undergroundsepulcher_main_0"] = 
                    {
                        ["ZoneId"] = 463,
                        ["ZoomLevel"] = 10,
                    },
                    ["lastresortbarrow_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 555.4700000000,
                    },
                    ["vilemansehouse02_base_0"] = 
                    {
                        ["ZoneId"] = 179,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 9160,
                        ["MapSize"] = 71.5000000000,
                    },
                    ["brackenleaf_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["barkbitemine_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3947,
                        ["MapSize"] = 181.5700000000,
                    },
                    ["sulimamansion_floor2_hidden_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["sulimamansion_floor1_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["glitteringgrotto_room1_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["eyevea_base_0"] = 
                    {
                        ["ZoneId"] = 99,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 8621,
                        ["MapSize"] = 837.3100000000,
                    },
                    ["mehrunesspite_base_0"] = 
                    {
                        ["ZoneId"] = 195,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["orderenclave02_base_0"] = 
                    {
                        ["ZoneId"] = 529,
                        ["ZoomLevel"] = 10,
                    },
                    ["knightsgrave04_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                    },
                    ["ezduiin_base_0"] = 
                    {
                        ["ZoneId"] = 189,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["shaelruins_base_0"] = 
                    {
                        ["ZoneId"] = 256,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 372.7200000000,
                    },
                    ["wailingprison4_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["ilmyris_base_0"] = 
                    {
                        ["ZoneId"] = 307,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 467.6800000000,
                    },
                    ["entilasfolly_base_0"] = 
                    {
                        ["ZoneId"] = 193,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["housedrescrypts_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 373.7000000000,
                    },
                    ["mistral_base_0"] = 
                    {
                        ["ZoneId"] = 294,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 4762,
                        ["MapSize"] = 723.4200000000,
                    },
                    ["athimahmanson02_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 9.9500000000,
                    },
                    ["craglorn_base_0"] = 
                    {
                        ["ZoneId"] = 353,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 8403,
                        ["MapSize"] = 4681.7200000000,
                    },
                    ["sancretor4_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 242.9800000000,
                    },
                    ["falinesticave_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 327.9600000000,
                    },
                    ["ossuaryoftelacar_base_0"] = 
                    {
                        ["ZoneId"] = 230,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 450.6600000000,
                    },
                    ["laeloriaruins_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 347.4900000000,
                    },
                    ["wormrootdepths_base_0"] = 
                    {
                        ["ZoneId"] = 261,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 318.7600000000,
                    },
                    ["sancretor1_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 373.0400000000,
                    },
                    ["clawsstrike_base_0"] = 
                    {
                        ["ZoneId"] = 250,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 430.7000000000,
                    },
                    ["eldenrootservices_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 9.9500000000,
                        ["subZoneId"] = 6637,
                        ["MapSize"] = 292.6100000000,
                    },
                    ["sharktoothgrotto1_base_0"] = 
                    {
                        ["ZoneId"] = 392,
                        ["ZoomLevel"] = 10,
                    },
                    ["nesalas_base_0"] = 
                    {
                        ["ZoneId"] = 232,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["thevaultofexile_base_0"] = 
                    {
                        ["ZoneId"] = 187,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["hectahamegrottoritual_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6264,
                        ["MapSize"] = 880.7400000000,
                    },
                    ["abahslanding_velmont_base_0"] = 
                    {
                        ["ZoneId"] = 521,
                        ["ZoomLevel"] = 10,
                    },
                    ["theearthforge_base_0"] = 
                    {
                        ["ZoneId"] = 389,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 761.5000000000,
                    },
                    ["stonefallsoutlawrefuge_base_0"] = 
                    {
                        ["ZoneId"] = 457,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["hoarfrost_base_0"] = 
                    {
                        ["ZoneId"] = 5,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 118,
                        ["MapSize"] = 394.5300000000,
                    },
                    ["burrootkwamamine_base_0"] = 
                    {
                        ["ZoneId"] = 233,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 389.2800000000,
                    },
                    ["hubalajadpalace_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["toothmaulgully_base_0"] = 
                    {
                        ["ZoneId"] = 267,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 730.1800000000,
                    },
                    ["cheesemongershollow_base_0"] = 
                    {
                        ["ZoneId"] = 48,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["eastelsweyrgate_base_0"] = 
                    {
                        ["ZoneId"] = 37,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 1264,
                        ["MapSize"] = 806.6700000000,
                    },
                    ["varoestatecaves02_base_0"] = 
                    {
                        ["ZoneId"] = 523,
                        ["ZoomLevel"] = 10,
                    },
                    ["sancretor3_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 246.8800000000,
                    },
                    ["hectahamegrottovalenheart_base_0"] = 
                    {
                        ["ZoneId"] = 313,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 595.8300000000,
                    },
                    ["secludedsewers_base_0"] = 
                    {
                        ["ZoneId"] = 462,
                        ["ZoomLevel"] = 10,
                    },
                    ["reliquaryvaultbottom_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 272.9900000000,
                    },
                    ["templeofthemourningspring_base_0"] = 
                    {
                        ["ZoneId"] = 294,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["rage_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 327.2300000000,
                    },
                    ["imperialsewers_ebon2_base_0"] = 
                    {
                        ["ZoneId"] = 390,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 9832,
                        ["MapSize"] = 1,
                    },
                    ["wrothgar_base_0"] = 
                    {
                        ["ZoneId"] = 396,
                        ["ZoomLevel"] = 9.9600000000,
                        ["subZoneId"] = 8943,
                        ["MapSize"] = 4687.3200000000,
                    },
                    ["glitteringgrotto_room8_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["stirk_base_0"] = 
                    {
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 726.3500000000,
                    },
                    ["imperialsewer_ebonheart3m_base_0"] = 
                    {
                        ["ZoneId"] = 390,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["sentinel_base_0"] = 
                    {
                        ["ZoneId"] = 17,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2417,
                        ["MapSize"] = 1106.3900000000,
                    },
                    ["ondil_base_0"] = 
                    {
                        ["ZoneId"] = 191,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["bleakrockvillage_base_0"] = 
                    {
                        ["ZoneId"] = 109,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 594.0800000000,
                    },
                    ["hectahamegrottoarmory_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6258,
                        ["MapSize"] = 880.7400000000,
                    },
                    ["glitteringgrotto_base_0"] = 
                    {
                        ["ZoneId"] = 469,
                        ["ZoomLevel"] = 10,
                    },
                    ["haven_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 9.9300000000,
                        ["subZoneId"] = 7428,
                        ["MapSize"] = 943.1000000000,
                    },
                    ["sancretor2_base_0"] = 
                    {
                        ["ZoneId"] = 286,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 259.9700000000,
                    },
                    ["hircineswoods_base_0"] = 
                    {
                        ["ZoneId"] = 319,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["ava_whole_0"] = 
                    {
                        ["ZoneId"] = 37,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3560,
                        ["MapSize"] = 13065.3800000000,
                    },
                    ["fulstromhomestead_base_0"] = 
                    {
                        ["ZoneId"] = 15,
                        ["ZoomLevel"] = 10,
                    },
                    ["wailingprison6_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["tribunesfolly_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 10201,
                    },
                    ["aldanobitombdungeon_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["undergroundsepulcher_room1_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["jodeslight_base_0"] = 
                    {
                        ["ZoneId"] = 252,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 436.7200000000,
                    },
                    ["eldenhollow_base_0"] = 
                    {
                        ["ZoneId"] = 21,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["banditden4_room3_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["shatteredshoals_base_0"] = 
                    {
                        ["ZoneId"] = 294,
                        ["ZoomLevel"] = 9.9300000000,
                        ["MapSize"] = 1,
                    },
                    ["boneorchard_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["hiradirgecitadeltg3_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["shadowfen_base_0"] = 
                    {
                        ["ZoneId"] = 19,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2355,
                        ["MapSize"] = 4036.6500000000,
                    },
                    ["redfurtradingpost_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 5880,
                        ["MapSize"] = 475.4400000000,
                    },
                    ["eldenrootgroundfloor_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 9.9200000000,
                        ["subZoneId"] = 6706,
                        ["MapSize"] = 1013.6100000000,
                    },
                    ["stonefalls_base_0"] = 
                    {
                        ["ZoneId"] = 9,
                        ["ZoomLevel"] = 9.8200000000,
                        ["subZoneId"] = 2485,
                        ["MapSize"] = 4770.5500000000,
                    },
                    ["tombofanahbi_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 240.4100000000,
                    },
                    ["hiradirgecitadeltg3_s2_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["davonswatchcrypt_base_0"] = 
                    {
                        ["ZoneId"] = 75,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 305.6100000000,
                    },
                    ["marbrukoutlawsrefuge_base_0"] = 
                    {
                        ["ZoneId"] = 448,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 270.4000000000,
                    },
                    ["hazikslair_base_0"] = 
                    {
                        ["ZoneId"] = 294,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["glitteringgrotto_room2_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["imperialsewers_aldmeri3_base_0"] = 
                    {
                        ["ZoneId"] = 390,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["chateauravenousrodent_base_0"] = 
                    {
                        ["ZoneId"] = 59,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2545,
                        ["MapSize"] = 818.8100000000,
                    },
                    ["deshaan_base_0"] = 
                    {
                        ["ZoneId"] = 10,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3317,
                        ["MapSize"] = 5347.7300000000,
                    },
                    ["bewan_base_0"] = 
                    {
                        ["ZoneId"] = 196,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["caveoftrophies_base_0"] = 
                    {
                        ["ZoneId"] = 215,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 449.0200000000,
                    },
                    ["smugglerstunnel_base_0"] = 
                    {
                        ["ZoneId"] = 507,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["secludedsewers_room2_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["imperialsewers_ebon1_base_0"] = 
                    {
                        ["ZoneId"] = 390,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 9831,
                        ["MapSize"] = 1,
                    },
                    ["vindeathcave_base_0"] = 
                    {
                        ["ZoneId"] = 260,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 334.4000000000,
                    },
                    ["imperialsewer_ebonheart3_base_0"] = 
                    {
                        ["ZoneId"] = 390,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["zthenganaz_base_0"] = 
                    {
                        ["ZoneId"] = 407,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 475.5000000000,
                    },
                    ["charredridge_base_0"] = 
                    {
                        ["ZoneId"] = 445,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 681.7200000000,
                    },
                    ["lair_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 146.7100000000,
                    },
                    ["gladiatorsassembly_base_0"] = 
                    {
                        ["ZoneId"] = 353,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["grahtwood_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 9.9800000000,
                        ["subZoneId"] = 5878,
                        ["MapSize"] = 4624.1200000000,
                    },
                    ["crosswych_base_0"] = 
                    {
                        ["ZoneId"] = 2,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 4517,
                        ["MapSize"] = 828.1700000000,
                    },
                    ["despair_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 223.6900000000,
                    },
                    ["arenasmurkmireexterior_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 9739,
                        ["MapSize"] = 1,
                    },
                    ["imperialsewershub_base_0"] = 
                    {
                        ["ZoneId"] = 390,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["nimalten_base_0"] = 
                    {
                        ["ZoneId"] = 16,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3547,
                        ["MapSize"] = 940.4900000000,
                    },
                    ["dhalmora_base_0"] = 
                    {
                        ["ZoneId"] = 110,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2688,
                        ["MapSize"] = 402.8300000000,
                    },
                    ["arenaslobbyexterior_base_0"] = 
                    {
                        ["ZoneId"] = 396,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 9951,
                        ["MapSize"] = 469.4500000000,
                    },
                    ["smugglertunnel_base_0"] = 
                    {
                        ["ZoneId"] = 110,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 144.4000000000,
                    },
                    ["silatar_base_0"] = 
                    {
                        ["ZoneId"] = 303,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 948.6600000000,
                    },
                    ["eldenrootmagesguilddown_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6637,
                        ["MapSize"] = 244.7100000000,
                    },
                    ["grahtwoodoutlawrefuge_base_0"] = 
                    {
                        ["ZoneId"] = 447,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 204.4000000000,
                    },
                    ["banditden4_room2_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["dragonstararena01_base_0"] = 
                    {
                        ["ZoneId"] = 353,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["castleoftheworm1_base_0"] = 
                    {
                        ["ZoneId"] = 47,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 420.6100000000,
                    },
                    ["wailingprison7_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["orsinium_base_0"] = 
                    {
                        ["ZoneId"] = 396,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 9657,
                        ["MapSize"] = 893.9800000000,
                    },
                    ["balfoyen_base_0"] = 
                    {
                        ["ZoneId"] = 110,
                        ["ZoomLevel"] = 9.9500000000,
                        ["subZoneId"] = 3759,
                        ["MapSize"] = 1696.7100000000,
                    },
                    ["wailingprison5_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["coldharbour_base_0"] = 
                    {
                        ["ZoneId"] = 154,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 4719,
                        ["MapSize"] = 5621.5400000000,
                    },
                    ["bahrahasgloom_secret2_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["davonswatch_base_0"] = 
                    {
                        ["ZoneId"] = 9,
                        ["ZoomLevel"] = 9.9100000000,
                        ["subZoneId"] = 2471,
                        ["MapSize"] = 956.1000000000,
                    },
                    ["athimahmanson03_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                    },
                    ["stonefang_base_0"] = 
                    {
                        ["ZoneId"] = 9,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["secludedsewers_room4_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["imperialsewers_aldmeri2_base_0"] = 
                    {
                        ["ZoneId"] = 390,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 8976,
                        ["MapSize"] = 1,
                    },
                    ["hoarvorpit_base_0"] = 
                    {
                        ["ZoneId"] = 255,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 434.3300000000,
                    },
                    ["eldenrootmagesguild_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 7817,
                        ["MapSize"] = 244.7100000000,
                    },
                    ["thibautscairn_base_0"] = 
                    {
                        ["ZoneId"] = 247,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 355.9800000000,
                    },
                    ["gladeofthedivineasakala_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["harridanslair_base_0"] = 
                    {
                        ["ZoneId"] = 331,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 332.3000000000,
                    },
                    ["caracdena_base_0"] = 
                    {
                        ["ZoneId"] = 327,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 333.2100000000,
                    },
                    ["kvatchcity_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 10199,
                    },
                    ["chateaumasterbedroom_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2546,
                        ["MapSize"] = 163.3100000000,
                    },
                    ["arenasshiveringisles_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 9928,
                        ["MapSize"] = 628.4000000000,
                    },
                    ["iliathtempletunnels_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 297.8500000000,
                    },
                    ["banditden4_room1_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["hubalajadpalaceint_01_h_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 10,
                    },
                    ["fulstromcatacombs_base_0"] = 
                    {
                        ["ZoneId"] = 15,
                        ["ZoomLevel"] = 10,
                    },
                    ["thebanishedcells_base_0"] = 
                    {
                        ["ZoneId"] = 177,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 548.6100000000,
                    },
                    ["caveofbrokensails_base_0"] = 
                    {
                        ["ZoneId"] = 229,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 4046,
                        ["MapSize"] = 1,
                    },
                    ["riften_base_0"] = 
                    {
                        ["ZoneId"] = 16,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2433,
                        ["MapSize"] = 757.5800000000,
                    },
                    ["nereidtemple_base_0"] = 
                    {
                        ["ZoneId"] = 310,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 431.0400000000,
                    },
                    ["hollowlair_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 511.9400000000,
                    },
                    ["imperialprisondistrictdun_base_0"] = 
                    {
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["phaercatacombs_base_0"] = 
                    {
                        ["ZoneId"] = 184,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["valleyofblades1_base_0"] = 
                    {
                        ["ZoneId"] = 326,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 671.3900000000,
                    },
                    ["campgushnukbur_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 186.7300000000,
                    },
                    ["serpentsgrotto_base_0"] = 
                    {
                        ["ZoneId"] = 308,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 629.6300000000,
                    },
                    ["knightsgrave03_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                    },
                    ["reapersmarchoutlawrefuge_base_0"] = 
                    {
                        ["ZoneId"] = 450,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 266.3400000000,
                    },
                    ["thehideaway_base_0"] = 
                    {
                        ["ZoneId"] = 468,
                        ["ZoomLevel"] = 10,
                    },
                    ["undergroundsepulcher_room2_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["stormhaven_base_0"] = 
                    {
                        ["ZoneId"] = 4,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2367,
                        ["MapSize"] = 4681.6300000000,
                    },
                    ["thewormsretreat_base_0"] = 
                    {
                        ["ZoneId"] = 325,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["orderenclave03_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                    },
                    ["varoestatetunnels01_base_0"] = 
                    {
                        ["ZoneId"] = 519,
                        ["ZoomLevel"] = 10,
                    },
                    ["nisincave_base_0"] = 
                    {
                        ["ZoneId"] = 279,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["banditden4_base_0"] = 
                    {
                        ["ZoneId"] = 464,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["bangkorai_base_0"] = 
                    {
                        ["ZoneId"] = 14,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6104,
                        ["MapSize"] = 4344.6000000000,
                    },
                    ["abahslanding_base_0"] = 
                    {
                        ["ZoneId"] = 513,
                        ["ZoomLevel"] = 9.9100000000,
                        ["subZoneId"] = 10018,
                        ["MapSize"] = 1179.2500000000,
                    },
                    ["oldmerchantcaves_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 254.6600000000,
                    },
                    ["kragenmoor_base_0"] = 
                    {
                        ["ZoneId"] = 9,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 1927,
                        ["MapSize"] = 799.3700000000,
                    },
                    ["deadmansdrop_base_0"] = 
                    {
                        ["ZoneId"] = 253,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 427.1600000000,
                    },
                    ["therift_base_0"] = 
                    {
                        ["ZoneId"] = 16,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 3260,
                        ["MapSize"] = 5350.4300000000,
                    },
                    ["hollowcity_base_0"] = 
                    {
                        ["ZoneId"] = 154,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 4717,
                        ["MapSize"] = 658.6600000000,
                    },
                    ["rivenspire_base_0"] = 
                    {
                        ["ZoneId"] = 5,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2386,
                        ["MapSize"] = 4062.4700000000,
                    },
                    ["imperialcity_base_0"] = 
                    {
                        ["ZoneId"] = 335,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6959,
                        ["MapSize"] = 1,
                    },
                    ["bahrahasgloom_base_0"] = 
                    {
                        ["ZoneId"] = 514,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 555.2600000000,
                    },
                    ["havensewers_base_0"] = 
                    {
                        ["ZoneId"] = 231,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["undergroundsepulcher_room3_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["glitteringgrotto_room3_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                    },
                    ["dbsanctuary_base_0"] = 
                    {
                        ["ZoneId"] = 522,
                        ["ZoomLevel"] = 10,
                    },
                    ["reapersmarch_base_0"] = 
                    {
                        ["ZoneId"] = 179,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 5587,
                        ["MapSize"] = 4439.9500000000,
                    },
                    ["auridonoutlawrefuge_base_0"] = 
                    {
                        ["ZoneId"] = 446,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 206.7000000000,
                    },
                    ["hectahamegrottoarboretum_base_0"] = 
                    {
                        ["ZoneId"] = 18,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6258,
                        ["MapSize"] = 880.7400000000,
                    },
                    ["visionofthecompanions_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["theunderroot_base_0"] = 
                    {
                        ["ZoneId"] = 329,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 393.6000000000,
                    },
                    ["garlasagea_base_0"] = 
                    {
                        ["ZoneId"] = 521,
                        ["ZoomLevel"] = 10,
                    },
                    ["velynharbor_base_0"] = 
                    {
                        ["ZoneId"] = 11,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 2836,
                        ["MapSize"] = 992.9100000000,
                    },
                    ["db3jerallmountains_base_0"] = 
                    {
                        ["ZoneId"] = 537,
                        ["ZoomLevel"] = 10,
                    },
                    ["arenthia_base_0"] = 
                    {
                        ["ZoneId"] = 179,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 833.3800000000,
                    },
                    ["faltoniasmine_base_0"] = 
                    {
                        ["ZoneId"] = 180,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 7431,
                        ["MapSize"] = 211.6500000000,
                    },
                    ["eastmarchrefuge_base_0"] = 
                    {
                        ["ZoneId"] = 458,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 247.6000000000,
                    },
                    ["themangroves_base_0"] = 
                    {
                        ["ZoneId"] = 294,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["evermore_base_0"] = 
                    {
                        ["ZoneId"] = 14,
                        ["ZoomLevel"] = 10,
                        ["subZoneId"] = 6104,
                        ["MapSize"] = 749.7800000000,
                    },
                    ["gladeofthedivineshivering_base_0"] = 
                    {
                        ["ZoneId"] = 4294967296,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 1,
                    },
                    ["blisstop_base_0"] = 
                    {
                        ["ZoneId"] = 178,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 279.8400000000,
                    },
                    ["sheogorathstongue_base_0"] = 
                    {
                        ["ZoneId"] = 117,
                        ["ZoomLevel"] = 10,
                        ["MapSize"] = 494.2700000000,
                    },
                },
                ["DefaultZoomLevel"] = 10,
                ["CorrdinatesLocation"] = "Top",
                ["ZoneFrameAnchor"] = 
                {
                    [1] = 1,
                    [2] = "Fyr_MM_Menu",
                    [3] = 4,
                    [4] = 0,
                    [5] = -14,
                },
                ["ChunkSize"] = 200,
                ["MapStepping"] = 2,
                ["position"] = 
                {
                    ["relativePoint"] = 3,
                    ["point"] = 3,
                    ["offsetY"] = 440.4530944824,
                    ["offsetX"] = 1658.5992431641,
                },
                ["ShowGroupLabels"] = false,
                ["FastTravelEnabled"] = false,
                ["MemberPinColor"] = 
                {
                    ["b"] = 1,
                    ["r"] = 1,
                    ["a"] = 1,
                    ["g"] = 1,
                },
                ["PinTooltips"] = true,
                ["HidePvPPins"] = false,
                ["PinRefreshRate"] = 1200,
                ["UseOriginalAPI"] = true,
                ["PPTextures"] = "ESO UI Worldmap",
                ["ZoneFontHeight"] = 21,
                ["RotateMap"] = false,
                ["BorderTreasures"] = false,
                ["WorldMapRefresh"] = true,
                ["ShowClock"] = false,
                ["AfterCombatUnhideDelay"] = 5,
                ["LeaderDeadPinColor"] = 
                {
                    ["b"] = 1,
                    ["r"] = 1,
                    ["a"] = 1,
                    ["g"] = 1,
                },
                ["MenuDisabled"] = false,
                ["ShowFPS"] = false,
                ["PositionHeight"] = 19,
                ["WheelMap"] = false,
                ["HideZoneLabel"] = false,
                ["BorderWayshrine"] = false,
                ["ZoneNameColor"] = 
                {
                    ["b"] = 1,
                    ["r"] = 1,
                    ["a"] = 1,
                    ["g"] = 1,
                },
                ["InCombatAutoHide"] = false,
                ["MapWidth"] = 280,
                ["PPStyle"] = "Separate Player & Camera",
                ["LeaderPinSize"] = 32,
                ["hideCompass"] = false,
                ["LeaderPin"] = "Default",
                ["ZoneFrameLocationOption"] = "Default",
                ["ChunkDelay"] = 10,
                ["Heading"] = "CAMERA",
                ["PositionColor"] = 
                {
                    ["b"] = 0.3137254902,
                    ["r"] = 0.9960784314,
                    ["a"] = 1,
                    ["g"] = 0.9200000000,
                },
                ["BorderCrafting"] = false,
                ["UndiscoveredPOIPinColor"] = 
                {
                    ["b"] = 0.3000000000,
                    ["r"] = 0.7000000000,
                    ["a"] = 0.6000000000,
                    ["g"] = 0.7000000000,
                },
                ["MemberDeadPinColor"] = 
                {
                    ["b"] = 1,
                    ["r"] = 1,
                    ["a"] = 1,
                    ["g"] = 1,
                },
                ["ShowUnexploredPins"] = true,
                ["ShowBorder"] = true,
                ["MapHeight"] = 279.9999694824,
                ["ViewRefreshRate"] = 2500,
                ["HideZoomLevel"] = false,
                ["MemberPin"] = "Default",
                ["ShowPosition"] = true,
                ["ZoneFont"] = "Univers 57",
                ["LeaderPinColor"] = 
                {
                    ["b"] = 1,
                    ["r"] = 1,
                    ["a"] = 1,
                    ["g"] = 1,
                },
                ["StartupInfo"] = false,
                ["ZoneRefreshRate"] = 100,
                ["version"] = 5,
                ["MapAlpha"] = 100,
                ["MouseWheel"] = true,
                ["ViewRangeFiltering"] = false,
                ["PositionFont"] = "Univers 57",
                ["BorderPinsWaypoint"] = false,
                ["CoordinatesAnchor"] = 
                {
                    [1] = 1,
                    [2] = "Fyr_MM",
                    [3] = 1,
                    [4] = 0,
                    [5] = 0,
                },
                ["MapRefreshRate"] = 59,
                ["CustomPinViewRange"] = 250,
                ["BorderQuestGivers"] = false,
                ["BorderPins"] = false,
                ["MenuTexture"] = "Default",
                ["SpeedUnit"] = "ft/s",
                ["Siege"] = false,
                ["WheelTexture"] = "Moosetrax Normal Wheel",
                ["KeepNetworkRefreshRate"] = 2000,
                ["BorderPinsBank"] = false,
                ["PinScale"] = 74,
                ["ZoneFontStyle"] = "soft-shadow-thick",
                ["BorderPinsStables"] = false,
                ["ShowSpeed"] = false,
                ["ClampedToScreen"] = true,
                ["InCombatState"] = true,
                ["PositionFontStyle"] = "soft-shadow-thick",
                ["MemberPinSize"] = 31,
                ["LockPosition"] = false,
                ["ZoneNameContents"] = "Classic (Map only)",
                ["ZoomIncrement"] = 1,
            },
        },
    },
}
