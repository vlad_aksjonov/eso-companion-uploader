GuildSalesAssistantSavedVars =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["$AccountWide"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["version"] = 1,
                    ["lastScan"] = 
                    {
                        ["Aequilibration"] = 1465562616,
                        ["The Traveling Merchant"] = 1465562618,
                        ["Brave Cat Trade"] = 1465562720,
                        ["Team Gold"] = 1465562610,
                    },
                    ["newestItem"] = 
                    {
                        ["The Traveling Merchant"] = 1465562604,
                        ["Brave Cat Trade"] = 1465562634,
                        ["Team Gold"] = 1465562573,
                    },
                    ["SalesData"] = 
                    {
                        [51638] = 
                        {
                            ["1:0:4:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/quest_book_001.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:51638:5:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hCrafting Motifs 11: Ancient Elf|h",
                                        ["itemname"] = "Crafting Motifs 11: Ancient Elf",
                                        ["id"] = "506801976",
                                        ["guild"] = "Brave Cat Trade",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["timestamp"] = 1465531903,
                                        ["buyer"] = "@dvavoloska",
                                        ["quant"] = 1,
                                        ["price"] = 4000,
                                    },
                                },
                            },
                        },
                        [54177] = 
                        {
                            ["1:0:5:0:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_outfitter_potion_014.dds",
                                ["sales"] = 
                                {
                                    [4] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1462994877,
                                        ["guild"] = "Team Gold",
                                        ["id"] = "494489042",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Jviewegh",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1462892496,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "493934182",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Rienzi1844",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1462892497,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "493934188",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Rienzi1844",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1462892498,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "493934196",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Rienzi1844",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                            ["1:15:5:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_outfitter_potion_014.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54177:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hdreugh wax|h",
                                        ["itemname"] = "dreugh wax",
                                        ["timestamp"] = 1464119278,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500452906",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Mjk_cz",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hdreugh wax|h",
                                        ["itemname"] = "dreugh wax",
                                        ["timestamp"] = 1464209646,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "500894290",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@sir.socke",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hdreugh wax|h",
                                        ["itemname"] = "dreugh wax",
                                        ["timestamp"] = 1464239030,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500984632",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@JaybeMawfaka",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [4] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hdreugh wax|h",
                                        ["itemname"] = "dreugh wax",
                                        ["timestamp"] = 1464239033,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500984636",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@JaybeMawfaka",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [5] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hdreugh wax|h",
                                        ["itemname"] = "dreugh wax",
                                        ["timestamp"] = 1464239034,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500984640",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@JaybeMawfaka",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [6] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hdreugh wax|h",
                                        ["itemname"] = "dreugh wax",
                                        ["timestamp"] = 1464239036,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500984644",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@JaybeMawfaka",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [7] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hdreugh wax|h",
                                        ["itemname"] = "dreugh wax",
                                        ["timestamp"] = 1464239038,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500984648",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@JaybeMawfaka",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [8] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hdreugh wax|h",
                                        ["itemname"] = "dreugh wax",
                                        ["timestamp"] = 1464810429,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504143674",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Sirbreizh",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                    [9] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54177:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hdreugh wax|h",
                                        ["itemname"] = "dreugh wax",
                                        ["timestamp"] = 1465068363,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505226116",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Tokke87",
                                        ["price"] = 5500,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [45572] = 
                        {
                            ["1:10:3:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/quest_scroll_001.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:45572:4:48:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPyandonea Merlot Recipe|h",
                                        ["itemname"] = "Pyandonea Merlot Recipe",
                                        ["timestamp"] = 1465389508,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "506314928",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Merry-LeBone",
                                        ["price"] = 570,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [803] = 
                        {
                            ["1:4:1:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_forester_weapon_component_006.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:803:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hsanded maple^ns|h",
                                        ["itemname"] = "sanded maple^ns",
                                        ["timestamp"] = 1464472059,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "502282250",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Kirky_lad",
                                        ["price"] = 1000,
                                        ["quant"] = 200,
                                    },
                                },
                            },
                        },
                        [72900] = 
                        {
                            ["50:48:3:18"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/gear_thievesguildv2_medium_chest_a.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:72900:81:50:0:0:0:0:0:0:0:0:0:0:0:0:11:0:0:0:10000:0|hJack of Bahraha's Curse|h",
                                        ["itemname"] = "Jack of Bahraha's Curse",
                                        ["timestamp"] = 1464176289,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500658396",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Jboss03",
                                        ["price"] = 300,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [54181] = 
                        {
                            ["1:0:5:0:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_wood_rosin.dds",
                                ["sales"] = 
                                {
                                    [4] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463159698,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495249980",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@MrDark85",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:38:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463101306,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495022712",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@fred4",
                                        ["price"] = 4000,
                                        ["quant"] = 2,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:46:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463101319,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495022732",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@fred4",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:46:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463101321,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495022734",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@fred4",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                            ["1:15:5:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_wood_rosin.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464212437,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "500914904",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Amardon84",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464212438,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "500914908",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Amardon84",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464212440,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "500914918",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Amardon84",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [4] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464212441,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "500914926",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Amardon84",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [5] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464212442,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "500914930",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Amardon84",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [6] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464212444,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "500914944",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Amardon84",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [7] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464718809,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "503612946",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@SKYRYKER",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [8] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464718814,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "503613070",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@SKYRYKER",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [9] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464805457,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504095442",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@cookieofdarkness",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [10] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464805458,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504095446",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@cookieofdarkness",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [11] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1464821425,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "504232324",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Axmouth",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [12] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1465068617,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505227404",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Tamrin",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [13] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1465068618,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505227408",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Tamrin",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [14] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1465068619,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505227410",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Tamrin",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                    [15] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54181:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hrosin|h",
                                        ["itemname"] = "rosin",
                                        ["timestamp"] = 1465068621,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505227412",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Tamrin",
                                        ["price"] = 2000,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [71526] = 
                        {
                            ["1:13:4:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/quest_letter_002.dds",
                                ["sales"] = 
                                {
                                    [2] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:71526:5:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hCrafting Motifs 24, Chapter 4: Outlaw Bows|h",
                                        ["itemname"] = "Crafting Motifs 24, Chapter 4: Outlaw Bows",
                                        ["timestamp"] = 1464979173,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504822800",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Nelarth",
                                        ["price"] = 8500,
                                        ["quant"] = 1,
                                    },
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:71526:5:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hCrafting Motifs 24, Chapter 4: Outlaw Bows|h",
                                        ["itemname"] = "Crafting Motifs 24, Chapter 4: Outlaw Bows",
                                        ["timestamp"] = 1464720365,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "503638146",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@B3lgr4d3r",
                                        ["price"] = 8500,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [71527] = 
                        {
                            ["1:13:4:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/quest_letter_002.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:71527:5:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hCrafting Motifs 24, Chapter 5: Outlaw Chests|h",
                                        ["itemname"] = "Crafting Motifs 24, Chapter 5: Outlaw Chests",
                                        ["timestamp"] = 1464342640,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "501496786",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Aelthwyn",
                                        ["price"] = 16000,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [71532] = 
                        {
                            ["1:13:4:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/quest_letter_002.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:71532:5:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hCrafting Motifs 24, Chapter 10: Outlaw Maces|h",
                                        ["itemname"] = "Crafting Motifs 24, Chapter 10: Outlaw Maces",
                                        ["timestamp"] = 1464847391,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504278982",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Setesh45",
                                        ["price"] = 5800,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [33897] = 
                        {
                            ["40:0:2:0:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_bread_001.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:33897:3:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463343777,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496388182",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@ArcaneAlchemist",
                                        ["price"] = 500,
                                        ["quant"] = 71,
                                    },
                                },
                            },
                        },
                        [73962] = 
                        {
                            ["50:80:3:7"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/gear_abahswatch_2hhammer_a.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:73962:362:50:0:0:0:0:0:0:0:0:0:0:0:0:41:0:0:0:0:0|hLeki's Maul|h",
                                        ["itemname"] = "Leki's Maul",
                                        ["timestamp"] = 1465118420,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505393718",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Treysup3",
                                        ["price"] = 1300,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [64221] = 
                        {
                            ["1:0:5:0:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/quest_potion_001.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463213530,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495509618",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463213536,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495509638",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463220765,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495558470",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [4] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463220767,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495558482",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [5] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463220768,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495558502",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [6] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463301154,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496007318",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [7] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463347524,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496423670",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [8] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463347525,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496423672",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [9] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463347526,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496423680",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [10] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463347527,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496423686",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Stimulanz",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [11] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463436099,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496974790",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Missty777",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [12] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463436103,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496974818",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Missty777",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [13] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463436108,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496974876",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Missty777",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [14] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463436114,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "496974924",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Missty777",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [15] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463581685,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "497591476",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Mottly5257",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                            ["1:0:5:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/quest_potion_001.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1463687809,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "498164732",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Exstazik",
                                        ["price"] = 15000,
                                        ["quant"] = 5,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1463754023,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "498430782",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@ingirorhaun",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1463754024,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "498430786",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@ingirorhaun",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [4] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1463754032,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "498430834",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@ingirorhaun",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [5] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1463754034,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "498430842",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@ingirorhaun",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [6] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1463754035,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "498430854",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@ingirorhaun",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [7] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1463784166,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "498675802",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Roter-Schnee",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [8] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464017517,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "499915336",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@DIABLO12345678",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [9] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464017521,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "499915354",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@DIABLO12345678",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [10] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464209258,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500889804",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Peanutsun",
                                        ["price"] = 15000,
                                        ["quant"] = 5,
                                    },
                                    [11] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464387059,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "501796842",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@asgornla",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [12] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464387066,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "501796878",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@asgornla",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [13] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464440265,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "502028478",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@GoldRoger21",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [14] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464440268,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "502028506",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@GoldRoger21",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [15] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464440272,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "502028532",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@GoldRoger21",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [16] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464440283,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "502028666",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@GoldRoger21",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [17] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464551345,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "502764476",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@AleeynLockpick",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [18] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464551350,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "502764530",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@AleeynLockpick",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [19] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1464796967,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504016180",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@RealCorvoAttano",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [20] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465053875,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505138028",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@DuskDiamond",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [21] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465053877,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505138066",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@DuskDiamond",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [22] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465055753,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505148614",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@King_Kirby",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [23] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465055754,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505148620",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@King_Kirby",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [24] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465055756,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505148640",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@King_Kirby",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [25] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465154825,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505626526",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Nirvanis",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [26] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465293950,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "506016498",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Nirvanis",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [27] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465293952,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "506016506",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Nirvanis",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [28] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465336857,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "506202452",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Can3lason",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [29] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465417273,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "506447470",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Can3lason",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                    [30] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:64221:124:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPsijic Ambrosia|h",
                                        ["itemname"] = "Psijic Ambrosia",
                                        ["timestamp"] = 1465419802,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "506459888",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Can3lason",
                                        ["price"] = 3000,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [73100] = 
                        {
                            ["50:30:3:18"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/gear_artifactwormcultlight_feet_a.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:73100:362:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:10000:0|hShoes of the Worm|h",
                                        ["itemname"] = "Shoes of the Worm",
                                        ["timestamp"] = 1465256383,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505945462",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Wolkenpferdchen",
                                        ["price"] = 5000,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [54509] = 
                        {
                            ["32:0:4:22:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/gear_breton_ring_a.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54509:5:32:45871:5:32:0:0:0:0:0:0:0:0:0:1:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463148756,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495183278",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@QuinnQ",
                                        ["price"] = 150,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [76910] = 
                        {
                            ["1:0:1:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_style_item_dark_brotherhood_r2.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:76910:30:0:0:0:0:0:0:0:0:0:0:0:0:0:46:0:0:0:0:0|hTainted Blood|h",
                                        ["itemname"] = "Tainted Blood",
                                        ["timestamp"] = 1465069524,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505232422",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@JaybeMawfaka",
                                        ["price"] = 3400,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [23122] = 
                        {
                            ["1:4:1:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_wood_base_hickory_r3.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:23122:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hsanded hickory^ns|h",
                                        ["itemname"] = "sanded hickory^ns",
                                        ["timestamp"] = 1463620622,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "497850498",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Oman1985",
                                        ["price"] = 1700,
                                        ["quant"] = 200,
                                    },
                                },
                            },
                        },
                        [71536] = 
                        {
                            ["1:13:4:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/quest_letter_002.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:71536:5:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hCrafting Motifs 24, Chapter 14: Outlaw Swords|h",
                                        ["itemname"] = "Crafting Motifs 24, Chapter 14: Outlaw Swords",
                                        ["timestamp"] = 1464722623,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "503675364",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Cyberskater",
                                        ["price"] = 9100,
                                        ["quant"] = 1,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:71536:5:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hCrafting Motifs 24, Chapter 14: Outlaw Swords|h",
                                        ["itemname"] = "Crafting Motifs 24, Chapter 14: Outlaw Swords",
                                        ["timestamp"] = 1464810193,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504140686",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@FAQxxxYou2",
                                        ["price"] = 9100,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:71536:5:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hCrafting Motifs 24, Chapter 14: Outlaw Swords|h",
                                        ["itemname"] = "Crafting Motifs 24, Chapter 14: Outlaw Swords",
                                        ["timestamp"] = 1464902531,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504550534",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@taho666",
                                        ["price"] = 9100,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [6001] = 
                        {
                            ["1:0:1:0:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_ore_base_ebony_r3.dds",
                                ["sales"] = 
                                {
                                    [2] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:6001:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463403749,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "496705128",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@KanedaSyndrome",
                                        ["price"] = 2300,
                                        ["quant"] = 200,
                                    },
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:6001:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463403748,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "496705118",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@KanedaSyndrome",
                                        ["price"] = 2300,
                                        ["quant"] = 200,
                                    },
                                },
                            },
                        },
                        [69298] = 
                        {
                            ["50:82:4:7"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/gear_altmer_2hhammer_d.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:69298:363:50:5365:363:50:0:0:0:0:0:0:0:0:0:7:0:0:0:0:0|hMaul of Agility|h",
                                        ["itemname"] = "Maul of Agility",
                                        ["timestamp"] = 1465278323,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505976260",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@NskDen",
                                        ["price"] = 3800,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [6000] = 
                        {
                            ["1:0:1:0:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_smith_plug_standard_r_001.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:6000:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463350808,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "496449424",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Cabalerian",
                                        ["price"] = 1800,
                                        ["quant"] = 200,
                                    },
                                },
                            },
                        },
                        [46133] = 
                        {
                            ["1:4:1:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_cloth_silverweave.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:46133:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hsilverweave^ns|h",
                                        ["itemname"] = "silverweave^ns",
                                        ["timestamp"] = 1464437719,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "502010866",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@xaval_fofinho",
                                        ["price"] = 1000,
                                        ["quant"] = 200,
                                    },
                                },
                            },
                        },
                        [30357] = 
                        {
                            ["1:0:1:0"] = 
                            {
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:30357:175:50:0:0:0:0:0:0:0:0:0:0:0:0:36:0:0:0:0:0|hLockpick|h",
                                        ["itemname"] = "Lockpick",
                                        ["timestamp"] = 1465408650,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "506399662",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@dangers",
                                        ["price"] = 1000,
                                        ["quant"] = 200,
                                    },
                                },
                                ["itemIcon"] = "/esoui/art/icons/lockpick.dds",
                            },
                        },
                        [23126] = 
                        {
                            ["1:0:1:0:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_cloth_base_spidersilk_r2.dds",
                                ["sales"] = 
                                {
                                    [2] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:23126:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463573518,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "497540752",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@egorm",
                                        ["price"] = 1700,
                                        ["quant"] = 200,
                                    },
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:23126:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463515802,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "497331204",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@BeoWing",
                                        ["price"] = 1700,
                                        ["quant"] = 200,
                                    },
                                },
                            },
                        },
                        [72932] = 
                        {
                            ["50:48:3:13"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/gear_ancientyokudan_medium_legs_a.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:72932:81:50:0:0:0:0:0:0:0:0:0:0:0:0:44:0:0:0:10000:0|hGuards of Syvarra's Scales|h",
                                        ["itemname"] = "Guards of Syvarra's Scales",
                                        ["timestamp"] = 1464455886,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "502155426",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@kadimor",
                                        ["price"] = 300,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [54513] = 
                        {
                            ["45:0:4:22:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/gear_breton_neck_a.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54513:5:45:45871:5:45:0:0:0:0:0:0:0:0:0:7:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463148748,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495183260",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@QuinnQ",
                                        ["price"] = 150,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [71525] = 
                        {
                            ["1:13:4:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/quest_letter_002.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:71525:5:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hCrafting Motifs 24, Chapter 3: Outlaw Boots|h",
                                        ["itemname"] = "Crafting Motifs 24, Chapter 3: Outlaw Boots",
                                        ["timestamp"] = 1465237629,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505846282",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Cyberskater",
                                        ["price"] = 8500,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [533] = 
                        {
                            ["1:4:1:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_wood_base_oak_r3.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:533:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hsanded oak^ns|h",
                                        ["itemname"] = "sanded oak^ns",
                                        ["timestamp"] = 1464462864,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "502206022",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Cokris",
                                        ["price"] = 1400,
                                        ["quant"] = 200,
                                    },
                                },
                            },
                        },
                        [23099] = 
                        {
                            ["1:4:1:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_leather_base_boiled_leather_r3.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:23099:30:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hleather^ns|h",
                                        ["itemname"] = "leather^ns",
                                        ["timestamp"] = 1464226487,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "500966986",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Royal_Blood",
                                        ["price"] = 1400,
                                        ["quant"] = 200,
                                    },
                                },
                            },
                        },
                        [33909] = 
                        {
                            ["40:4:2:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_dessert_001.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:33909:3:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hSkyrim Jazbay Crostata|h",
                                        ["itemname"] = "Skyrim Jazbay Crostata",
                                        ["timestamp"] = 1465297468,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "506029208",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Wolfking37",
                                        ["price"] = 500,
                                        ["quant"] = 70,
                                    },
                                },
                            },
                        },
                        [54173] = 
                        {
                            ["1:0:5:0:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_smith_potion__sp_names_003.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1462892535,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "493934290",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Rienzi1844",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463131060,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495095338",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@PegasusRed1",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54173:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463131063,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495095352",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@PegasusRed1",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                    [4] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463164155,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495284536",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@lllDeStRoZalll",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                    [5] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                        ["timestamp"] = 1463164163,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "495284656",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@lllDeStRoZalll",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                            ["1:15:5:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_smith_potion__sp_names_003.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464013523,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "499891288",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@avvva",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464013525,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "499891304",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@avvva",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464021300,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "499940254",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Mumina",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [4] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464160566,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500588018",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Mulven_Morrko",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [5] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54173:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464211575,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500908838",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@PURPLE97",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [6] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54173:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464211577,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500908848",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@PURPLE97",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [7] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54173:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464211579,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500908858",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@PURPLE97",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [8] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54173:34:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464211663,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "500909420",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@PURPLE97",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [9] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464687917,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "503380450",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Domardal",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [10] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1464804453,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504085186",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Nina_Nord",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [11] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1465071965,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505248442",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Mishanya",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                    [12] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:54173:34:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|htempering alloy|h",
                                        ["itemname"] = "tempering alloy",
                                        ["timestamp"] = 1465071967,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "505248448",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Mishanya",
                                        ["price"] = 8300,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [45854] = 
                        {
                            ["21:2:5:0"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_components_runestones_001.dds",
                                ["sales"] = 
                                {
                                    [1] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:45854:24:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hKuta|h",
                                        ["itemname"] = "Kuta",
                                        ["timestamp"] = 1464773792,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "503844988",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Jimboo84",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                    [2] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:45854:24:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hKuta|h",
                                        ["itemname"] = "Kuta",
                                        ["timestamp"] = 1464806688,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "504108798",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Morostyle",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                    [3] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:45854:24:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hKuta|h",
                                        ["itemname"] = "Kuta",
                                        ["timestamp"] = 1464806689,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "504108822",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Morostyle",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                    [4] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:45854:24:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hKuta|h",
                                        ["itemname"] = "Kuta",
                                        ["timestamp"] = 1464806690,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "504108838",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Morostyle",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                    [5] = 
                                    {
                                        ["wasKiosk"] = false,
                                        ["itemLink"] = "|H0:item:45854:24:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hKuta|h",
                                        ["itemname"] = "Kuta",
                                        ["timestamp"] = 1464806692,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "504108868",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Morostyle",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                    [6] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:45854:24:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hKuta|h",
                                        ["itemname"] = "Kuta",
                                        ["timestamp"] = 1464898852,
                                        ["guild"] = "Brave Cat Trade",
                                        ["id"] = "504529444",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Forti-4",
                                        ["price"] = 8000,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                        [56863] = 
                        {
                            ["1:0:1:26"] = 
                            {
                                ["itemIcon"] = "/esoui/art/icons/crafting_potent_nirncrux_dust.dds",
                                ["sales"] = 
                                {
                                    [2] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:56863:30:6:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPotent Nirncrux|h",
                                        ["itemname"] = "Potent Nirncrux",
                                        ["timestamp"] = 1463987416,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "499765946",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Seelenblut",
                                        ["price"] = 23000,
                                        ["quant"] = 1,
                                    },
                                    [1] = 
                                    {
                                        ["wasKiosk"] = true,
                                        ["itemLink"] = "|H0:item:56863:30:6:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hPotent Nirncrux|h",
                                        ["itemname"] = "Potent Nirncrux",
                                        ["timestamp"] = 1463947995,
                                        ["guild"] = "The Traveling Merchant",
                                        ["id"] = "499642940",
                                        ["seller"] = "@VladislavAksjonov",
                                        ["buyer"] = "@Lizzyboof",
                                        ["price"] = 23000,
                                        ["quant"] = 1,
                                    },
                                },
                            },
                        },
                    },
                },
            },
        },
    },
}
