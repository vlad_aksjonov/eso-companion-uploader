CMLD_SavedVariables =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Bswan"] = 
            {
                ["version"] = 1,
                ["inventoryOffset"] = -100,
                ["enchantingInventoryList"] = true,
                ["woodworkingInventoryList"] = true,
                ["enchanting"] = true,
                ["showLevelsInInventoryLists"] = true,
                ["blacksmithingInventoryList"] = true,
                ["normalInventoryList"] = true,
                ["lootOffset"] = -20,
                ["clothingInventoryList"] = true,
                ["alchemy"] = true,
                ["levelSortHeader"] = true,
                ["font"] = 
                {
                    ["size"] = 14,
                    ["color"] = 
                    {
                        ["b"] = 1,
                        ["r"] = 1,
                        ["a"] = 1,
                        ["g"] = 1,
                    },
                    ["family"] = "Univers 55",
                    ["style"] = "none",
                },
            },
            ["Purr-Purr'Dar"] = 
            {
                ["version"] = 1,
                ["inventoryOffset"] = -100,
                ["enchantingInventoryList"] = true,
                ["woodworkingInventoryList"] = true,
                ["enchanting"] = true,
                ["showLevelsInInventoryLists"] = true,
                ["blacksmithingInventoryList"] = true,
                ["normalInventoryList"] = true,
                ["lootOffset"] = -20,
                ["clothingInventoryList"] = true,
                ["alchemy"] = true,
                ["levelSortHeader"] = true,
                ["font"] = 
                {
                    ["size"] = 14,
                    ["color"] = 
                    {
                        ["b"] = 1,
                        ["r"] = 1,
                        ["a"] = 1,
                        ["g"] = 1,
                    },
                    ["family"] = "Univers 55",
                    ["style"] = "none",
                },
            },
            ["Razum-Dar in Excile"] = 
            {
                ["version"] = 1,
                ["inventoryOffset"] = -100,
                ["enchantingInventoryList"] = true,
                ["woodworkingInventoryList"] = true,
                ["enchanting"] = true,
                ["showLevelsInInventoryLists"] = true,
                ["blacksmithingInventoryList"] = true,
                ["normalInventoryList"] = true,
                ["lootOffset"] = -20,
                ["clothingInventoryList"] = true,
                ["alchemy"] = true,
                ["levelSortHeader"] = true,
                ["font"] = 
                {
                    ["size"] = 14,
                    ["color"] = 
                    {
                        ["b"] = 1,
                        ["r"] = 1,
                        ["a"] = 1,
                        ["g"] = 1,
                    },
                    ["family"] = "Univers 55",
                    ["style"] = "none",
                },
            },
            ["Lb-Enchanting"] = 
            {
                ["version"] = 1,
                ["inventoryOffset"] = -100,
                ["enchantingInventoryList"] = true,
                ["woodworkingInventoryList"] = true,
                ["enchanting"] = true,
                ["showLevelsInInventoryLists"] = true,
                ["blacksmithingInventoryList"] = true,
                ["normalInventoryList"] = true,
                ["lootOffset"] = -20,
                ["clothingInventoryList"] = true,
                ["alchemy"] = true,
                ["levelSortHeader"] = true,
                ["font"] = 
                {
                    ["size"] = 14,
                    ["color"] = 
                    {
                        ["b"] = 1,
                        ["r"] = 1,
                        ["a"] = 1,
                        ["g"] = 1,
                    },
                    ["family"] = "Univers 55",
                    ["style"] = "none",
                },
            },
            ["Testimate"] = 
            {
                ["version"] = 1,
                ["inventoryOffset"] = -100,
                ["enchantingInventoryList"] = true,
                ["woodworkingInventoryList"] = true,
                ["enchanting"] = true,
                ["showLevelsInInventoryLists"] = true,
                ["blacksmithingInventoryList"] = true,
                ["normalInventoryList"] = true,
                ["lootOffset"] = -20,
                ["clothingInventoryList"] = true,
                ["alchemy"] = true,
                ["levelSortHeader"] = true,
                ["font"] = 
                {
                    ["size"] = 14,
                    ["color"] = 
                    {
                        ["b"] = 1,
                        ["r"] = 1,
                        ["a"] = 1,
                        ["g"] = 1,
                    },
                    ["family"] = "Univers 55",
                    ["style"] = "none",
                },
            },
            ["Lb-Grocery"] = 
            {
                ["version"] = 1,
                ["inventoryOffset"] = -100,
                ["enchantingInventoryList"] = true,
                ["woodworkingInventoryList"] = true,
                ["enchanting"] = true,
                ["showLevelsInInventoryLists"] = true,
                ["blacksmithingInventoryList"] = true,
                ["normalInventoryList"] = true,
                ["lootOffset"] = -20,
                ["clothingInventoryList"] = true,
                ["alchemy"] = true,
                ["levelSortHeader"] = true,
                ["font"] = 
                {
                    ["size"] = 14,
                    ["color"] = 
                    {
                        ["b"] = 1,
                        ["r"] = 1,
                        ["a"] = 1,
                        ["g"] = 1,
                    },
                    ["family"] = "Univers 55",
                    ["style"] = "none",
                },
            },
            ["Lb-Alchemy"] = 
            {
                ["version"] = 1,
                ["inventoryOffset"] = -100,
                ["enchantingInventoryList"] = true,
                ["woodworkingInventoryList"] = true,
                ["enchanting"] = true,
                ["showLevelsInInventoryLists"] = true,
                ["blacksmithingInventoryList"] = true,
                ["normalInventoryList"] = true,
                ["lootOffset"] = -20,
                ["clothingInventoryList"] = true,
                ["alchemy"] = true,
                ["levelSortHeader"] = true,
                ["font"] = 
                {
                    ["size"] = 14,
                    ["color"] = 
                    {
                        ["b"] = 1,
                        ["r"] = 1,
                        ["a"] = 1,
                        ["g"] = 1,
                    },
                    ["family"] = "Univers 55",
                    ["style"] = "none",
                },
            },
        },
    },
}
