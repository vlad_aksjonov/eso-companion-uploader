MXPVvars =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Testimate"] = 
            {
                ["OffsetX"] = -50,
                ["RateFactor"] = 1,
                ["UIMode"] = true,
                ["point"] = 12,
                ["relativePoint"] = 12,
                ["ScaleFactor"] = 1,
                ["XPMode"] = true,
                ["RPMode"] = false,
                ["AutoSwitch"] = true,
                ["OffsetY"] = -40,
                ["XPBarMode"] = true,
                ["version"] = 100,
                ["ConsoleMode"] = false,
                ["fadeXPGainDelay"] = 10000,
                ["MeterMode"] = true,
                ["CombatOnly"] = false,
                ["StartPaused"] = false,
                ["CombatUnpauses"] = false,
            },
            ["Bswan"] = 
            {
                ["OffsetX"] = 394.0275878906,
                ["RateFactor"] = 1,
                ["UIMode"] = true,
                ["point"] = 6,
                ["relativePoint"] = 6,
                ["ScaleFactor"] = 1,
                ["XPMode"] = true,
                ["RPMode"] = false,
                ["AutoSwitch"] = true,
                ["OffsetY"] = -6.6628417969,
                ["fadeXPGainDelay"] = 10000,
                ["XPBarMode"] = true,
                ["version"] = 100,
                ["ConsoleMode"] = false,
                ["MeterMode"] = true,
                ["CombatOnly"] = false,
                ["StartPaused"] = false,
                ["CombatUnpauses"] = false,
            },
        },
    },
}
