VotansImprovedLocations_Data =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["$AccountWide"] = 
            {
                ["showLevels"] = false,
                ["version"] = 1,
                ["sortBy"] = "Name",
                ["allAllianceOnTop"] = false,
            },
            ["Bswan"] = 
            {
                ["recent"] = 
                {
                    [1] = 29,
                    [2] = 28,
                    [3] = 14,
                    [4] = 27,
                    [5] = 15,
                },
                ["version"] = 1,
            },
            ["Purr-Purr'Dar"] = 
            {
                ["recent"] = 
                {
                    [1] = 7,
                },
                ["version"] = 1,
            },
            ["Razum-Dar in Excile"] = 
            {
                ["recent"] = 
                {
                    [2] = 28,
                    [1] = 11,
                },
                ["version"] = 1,
            },
            ["Lb-Enchanting"] = 
            {
                ["recent"] = 
                {
                    [1] = 15,
                },
                ["version"] = 1,
            },
            ["Testimate"] = 
            {
                ["recent"] = 
                {
                    [2] = 11,
                    [1] = 22,
                },
                ["version"] = 1,
            },
            ["Lb-Grocery"] = 
            {
                ["recent"] = 
                {
                    [1] = 11,
                },
                ["version"] = 1,
            },
            ["Lb-Alchemy"] = 
            {
                ["recent"] = 
                {
                    [1] = 28,
                },
                ["version"] = 1,
            },
        },
    },
}
