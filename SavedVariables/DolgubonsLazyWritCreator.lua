DolgubonsWritCrafterSavedVars =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Bswan"] = 
            {
                ["version"] = 19,
                ["Clothier"] = true,
                ["autoCraft"] = false,
                ["tutorial"] = false,
                ["Woodworker"] = true,
                ["Enchanter"] = true,
                ["Blacksmith"] = true,
                ["showWindow"] = true,
                ["delay"] = 100,
                ["shouldGrab"] = true,
                ["OffsetX"] = 923.3350219727,
                ["OffsetY"] = 0,
            },
            ["Purr-Purr'Dar"] = 
            {
                ["version"] = 19,
                ["Clothier"] = true,
                ["autoCraft"] = false,
                ["tutorial"] = false,
                ["Woodworker"] = true,
                ["Enchanter"] = true,
                ["Blacksmith"] = true,
                ["showWindow"] = true,
                ["delay"] = 100,
                ["shouldGrab"] = true,
                ["OffsetX"] = 470,
                ["OffsetY"] = 0,
            },
            ["Razum-Dar in Excile"] = 
            {
                ["version"] = 19,
                ["Clothier"] = true,
                ["autoCraft"] = false,
                ["tutorial"] = true,
                ["Woodworker"] = true,
                ["Enchanter"] = true,
                ["Blacksmith"] = true,
                ["showWindow"] = true,
                ["delay"] = 100,
                ["shouldGrab"] = true,
                ["OffsetX"] = 0,
                ["OffsetY"] = 0,
            },
            ["Lb-Enchanting"] = 
            {
                ["version"] = 19,
                ["Clothier"] = true,
                ["autoCraft"] = false,
                ["tutorial"] = true,
                ["Enchanter"] = true,
                ["Woodworker"] = true,
                ["delay"] = 100,
                ["shouldGrab"] = true,
                ["showWindow"] = true,
                ["Blacksmith"] = true,
            },
            ["Testimate"] = 
            {
                ["version"] = 19,
                ["Clothier"] = true,
                ["autoCraft"] = false,
                ["tutorial"] = true,
                ["Enchanter"] = true,
                ["Woodworker"] = true,
                ["delay"] = 100,
                ["shouldGrab"] = true,
                ["showWindow"] = true,
                ["Blacksmith"] = true,
            },
            ["Lb-Grocery"] = 
            {
                ["version"] = 19,
                ["Clothier"] = true,
                ["autoCraft"] = false,
                ["tutorial"] = true,
                ["Enchanter"] = true,
                ["Woodworker"] = true,
                ["delay"] = 100,
                ["shouldGrab"] = true,
                ["showWindow"] = true,
                ["Blacksmith"] = true,
            },
            ["Lb-Alchemy"] = 
            {
                ["version"] = 19,
                ["Clothier"] = true,
                ["autoCraft"] = false,
                ["tutorial"] = true,
                ["Enchanter"] = true,
                ["Woodworker"] = true,
                ["delay"] = 100,
                ["shouldGrab"] = true,
                ["showWindow"] = true,
                ["Blacksmith"] = true,
            },
        },
    },
}
