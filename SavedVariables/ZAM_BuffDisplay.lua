ZAM_BuffDisplayDB =
{
    ["attackShow"] = false,
    ["isLocked"] = true,
    ["barColor"] = 
    {
        ["b"] = 0.8980392218,
        ["r"] = 0.9294117689,
        ["a"] = 0,
        ["g"] = 1,
    },
    ["customTextColors"] = 
    {
        [0] = 
        {
            ["b"] = 1,
            ["r"] = 1,
            ["g"] = 1,
        },
        [1] = 
        {
            ["b"] = 0,
            ["r"] = 0,
            ["g"] = 1,
        },
        [2] = 
        {
            ["b"] = 0,
            ["r"] = 1,
            ["g"] = 0,
        },
    },
    ["font"] = "ESO Cartographer",
    ["barTex"] = "ESO Basic",
    ["menuHide"] = true,
    ["player"] = 
    {
        ["growUp"] = false,
        ["anchor"] = 
        {
            ["b"] = 9,
            ["y"] = 267.6954345703,
            ["a"] = 9,
            ["x"] = -304.6582031250,
        },
    },
    ["reticleover"] = 
    {
        ["growUp"] = false,
        ["anchor"] = 
        {
            ["b"] = 9,
            ["y"] = 267.8485717773,
            ["a"] = 9,
            ["x"] = -46.5142822266,
        },
    },
    ["defaultTextColor"] = true,
    ["displayBars"] = true,
    ["cmbtShow"] = false,
}
