Clock_Settings =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["$AccountWide"] = 
            {
                ["settings"] = 
                {
                    ["show_us"] = false,
                    ["show_date"] = false,
                    ["time"] = 
                    {
                        ["name"] = "noon",
                        ["daytime"] = 20955,
                        ["night"] = 7200,
                        ["start"] = 1398044126,
                    },
                    ["show_num"] = true,
                    ["active"] = true,
                    ["show_time"] = true,
                    ["moveable"] = true,
                    ["version"] = 0.7000000000,
                    ["show_bg"] = true,
                    ["show_rt"] = false,
                    ["show_fldate"] = false,
                    ["us_time"] = false,
                    ["auto_hide"] = true,
                    ["show_moon"] = true,
                    ["offset"] = 
                    {
                        ["y"] = 39.6936073303,
                        ["x"] = 1651.0634765625,
                    },
                    ["show_ldate"] = true,
                    ["sep_lr"] = true,
                    ["look"] = 
                    {
                        ["real"] = 
                        {
                            ["size"] = 24,
                            ["style"] = "thin_shadow",
                            ["font"] = "ESO Book Font",
                            ["color"] = 
                            {
                                ["b"] = 1,
                                ["g"] = 1,
                                ["a"] = 0.7500000000,
                                ["r"] = 1,
                            },
                        },
                        ["format"] = 
                        {
                            ["lore"] = "_DDD, _D. _MMM _YY _hh:_mm:_ss",
                            ["real"] = "_DDD, _D. _MMM _YY _hh:_mm:_ss",
                        },
                        ["lore"] = 
                        {
                            ["size"] = 30,
                            ["style"] = "thin_shadow",
                            ["font"] = "ESO Cartographer",
                            ["color"] = 
                            {
                                ["b"] = 0,
                                ["g"] = 0.6235294342,
                                ["a"] = 0.8891806602,
                                ["r"] = 0.7372549176,
                            },
                        },
                        ["bg"] = "Solid",
                    },
                    ["show_sec"] = false,
                    ["moon"] = 
                    {
                        ["new"] = 5,
                        ["full"] = 10,
                        ["way"] = 85,
                        ["name"] = "full",
                        ["start"] = 1425169441,
                    },
                    ["show_hz"] = false,
                },
            },
        },
    },
}
