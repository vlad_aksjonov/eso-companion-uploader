SkyS_SavedVariables =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Lb-Grocery"] = 
            {
                ["filters"] = 
                {
                    ["SkySMapPin_collected"] = false,
                    ["SkySMapPin_unknown"] = true,
                    ["SkySCompassPin_unknown"] = true,
                },
                ["pinTexture"] = 
                {
                    ["size"] = 38,
                    ["level"] = 40,
                    ["type"] = 1,
                },
                ["version"] = 4,
                ["compassMaxDistance"] = 0.0500000000,
            },
            ["Bswan"] = 
            {
                ["filters"] = 
                {
                    ["SkySMapPin_collected"] = false,
                    ["SkySMapPin_unknown"] = true,
                    ["SkySCompassPin_unknown"] = true,
                },
                ["pinTexture"] = 
                {
                    ["size"] = 38,
                    ["level"] = 40,
                    ["type"] = 1,
                },
                ["version"] = 4,
                ["compassMaxDistance"] = 0.0500000000,
            },
            ["Purr-Purr'Dar"] = 
            {
                ["filters"] = 
                {
                    ["SkySMapPin_collected"] = false,
                    ["SkySMapPin_unknown"] = false,
                    ["SkySCompassPin_unknown"] = true,
                },
                ["pinTexture"] = 
                {
                    ["size"] = 38,
                    ["level"] = 40,
                    ["type"] = 1,
                },
                ["version"] = 4,
                ["compassMaxDistance"] = 0.0500000000,
            },
            ["Razum-Dar in Excile"] = 
            {
                ["filters"] = 
                {
                    ["SkySMapPin_collected"] = false,
                    ["SkySMapPin_unknown"] = true,
                    ["SkySCompassPin_unknown"] = true,
                },
                ["pinTexture"] = 
                {
                    ["size"] = 38,
                    ["level"] = 40,
                    ["type"] = 1,
                },
                ["version"] = 4,
                ["compassMaxDistance"] = 0.0500000000,
            },
            ["Lb-Enchanting"] = 
            {
                ["filters"] = 
                {
                    ["SkySMapPin_collected"] = false,
                    ["SkySMapPin_unknown"] = true,
                    ["SkySCompassPin_unknown"] = true,
                },
                ["pinTexture"] = 
                {
                    ["size"] = 38,
                    ["level"] = 40,
                    ["type"] = 1,
                },
                ["version"] = 4,
                ["compassMaxDistance"] = 0.0500000000,
            },
            ["Testimate"] = 
            {
                ["filters"] = 
                {
                    ["SkySMapPin_collected"] = false,
                    ["SkySMapPin_unknown"] = true,
                    ["SkySCompassPin_unknown"] = true,
                },
                ["pinTexture"] = 
                {
                    ["size"] = 38,
                    ["level"] = 40,
                    ["type"] = 1,
                },
                ["version"] = 4,
                ["compassMaxDistance"] = 0.0500000000,
            },
            ["Purr-Purr'dar"] = 
            {
                ["filters"] = 
                {
                    ["SkySMapPin_collected"] = false,
                    ["SkySMapPin_unknown"] = false,
                    ["SkySCompassPin_unknown"] = true,
                },
                ["pinTexture"] = 
                {
                    ["size"] = 38,
                    ["level"] = 40,
                    ["type"] = 1,
                },
                ["version"] = 4,
                ["compassMaxDistance"] = 0.0500000000,
            },
            ["Lb-Alchemy"] = 
            {
                ["filters"] = 
                {
                    ["SkySMapPin_collected"] = false,
                    ["SkySMapPin_unknown"] = false,
                    ["SkySCompassPin_unknown"] = true,
                },
                ["pinTexture"] = 
                {
                    ["size"] = 38,
                    ["level"] = 40,
                    ["type"] = 1,
                },
                ["version"] = 4,
                ["compassMaxDistance"] = 0.0500000000,
            },
        },
    },
}
