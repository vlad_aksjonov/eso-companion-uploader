Undiscovered_Settings =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Purr-Purr'dar"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
                ["pinTexture"] = 
                {
                    ["size"] = 35,
                    ["level"] = 30,
                    ["type"] = 1,
                },
                ["export"] = 
                {
                    ["version"] = 1,
                },
            },
            ["Bswan"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
                ["pinTexture"] = 
                {
                    ["size"] = 35,
                    ["level"] = 30,
                    ["type"] = 1,
                },
                ["export"] = 
                {
                    ["version"] = 1,
                },
            },
            ["Lb-Grocery"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
                ["pinTexture"] = 
                {
                    ["size"] = 35,
                    ["level"] = 30,
                    ["type"] = 1,
                },
                ["export"] = 
                {
                    ["version"] = 1,
                },
            },
            ["Purr-Purr'Dar"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
                ["pinTexture"] = 
                {
                    ["size"] = 35,
                    ["level"] = 30,
                    ["type"] = 1,
                },
                ["export"] = 
                {
                    ["version"] = 1,
                },
            },
            ["Lb-Alchemy"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
                ["pinTexture"] = 
                {
                    ["size"] = 35,
                    ["level"] = 30,
                    ["type"] = 1,
                },
                ["export"] = 
                {
                    ["version"] = 1,
                },
            },
            ["Razum-Dar in Excile"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
                ["pinTexture"] = 
                {
                    ["size"] = 35,
                    ["level"] = 30,
                    ["type"] = 1,
                },
                ["export"] = 
                {
                    ["version"] = 1,
                },
            },
            ["Testimate"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
                ["pinTexture"] = 
                {
                    ["size"] = 35,
                    ["level"] = 30,
                    ["type"] = 1,
                },
                ["export"] = 
                {
                    ["version"] = 1,
                },
            },
            ["Lb-Enchanting"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
                ["pinTexture"] = 
                {
                    ["size"] = 35,
                    ["level"] = 30,
                    ["type"] = 1,
                },
                ["export"] = 
                {
                    ["version"] = 1,
                },
            },
        },
    },
}
LibMPM_Filters =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["LocalPlayer"] = 
            {
                ["filters"] = 
                {
                },
                ["version"] = 1,
            },
            ["Lb-Grocery"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
            },
            ["Purr-Purr'Dar"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = false,
                    ["UNDC_PinSet_Unknown"] = false,
                },
                ["version"] = 1,
            },
            ["Lb-Alchemy"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = false,
                    ["UNDC_PinSet_Unknown"] = false,
                },
                ["version"] = 1,
            },
            ["Lb-Enchanting"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
            },
            ["Bswan"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = false,
                    ["UNDC_PinSet_Unknown"] = false,
                },
                ["version"] = 1,
            },
            ["Purr-Purr'dar"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = false,
                    ["UNDC_PinSet_Unknown"] = false,
                },
                ["version"] = 1,
            },
            ["Razum-Dar in Excile"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = false,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
            },
            ["Testimate"] = 
            {
                ["filters"] = 
                {
                    ["UNDC_PinSet_Known"] = true,
                    ["UNDC_PinSet_Unknown"] = true,
                },
                ["version"] = 1,
            },
        },
    },
}
