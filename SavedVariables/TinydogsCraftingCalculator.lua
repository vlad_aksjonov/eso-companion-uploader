TinydogsCraftingCalculatorVars =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Bswan"] = 
            {
                ["TabSelected"] = "tccXpTabButton",
                ["CraftSelected"] = "Woodworking",
                ["OrderItemData"] = 
                {
                    [1] = 
                    {
                        ["ItemSet"] = "|cFFFFFF(none)|r",
                        ["ItemQty"] = 1,
                        ["ItemStyle"] = "|cFFFFFF(any)|r",
                        ["ItemLevel"] = "30",
                        ["ItemMaterials"] = 
                        {
                            ["QualityBlueMaterial"] = 
                            {
                                ["MaterialKey"] = "Embroidery",
                                ["MaterialQuantity"] = 3,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54175:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 3,
                                ["MaterialName"] = "Embroidery",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_component_002.dds",
                            },
                            ["QualityPurpleMaterial"] = 
                            {
                                ["MaterialKey"] = "Elegant Lining",
                                ["MaterialQuantity"] = 4,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54176:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 4,
                                ["MaterialName"] = "Elegant Lining",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_runecrafter_potion_sp_name_001.dds",
                            },
                            ["BaseMaterial"] = 
                            {
                                ["MaterialKey"] = "Cotton",
                                ["MaterialQuantity"] = 11,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:23125:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 11,
                                ["MaterialName"] = "Cotton",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_cloth_base_cotton_r2.dds",
                            },
                            ["QualityGreenMaterial"] = 
                            {
                                ["MaterialKey"] = "Hemming",
                                ["MaterialQuantity"] = 2,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54174:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 2,
                                ["MaterialName"] = "Hemming",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_001.dds",
                            },
                        },
                        ["ItemTrait"] = "|cFFFFFF(none)|r",
                        ["ItemName"] = "Light Shirt",
                        ["ItemText"] = "Level |cFFFF0030|r Light Shirt, |cFF00FFPurple|r quality",
                        ["ItemType"] = 
                        {
                            ["MaterialCategory"] = "Cloth",
                            ["TraitType"] = "Armor",
                            ["ItemCategory"] = "Light",
                            ["Key"] = "Shirt",
                            ["QtyModifier"] = 2,
                            ["QtyModifierV16"] = 20,
                            ["CraftName"] = "Clothing",
                            ["BodyPart"] = "Chest",
                            ["ItemName"] = "Shirt",
                            ["QtyModifierV15"] = 2,
                        },
                        ["ItemQuality"] = "|cFF00FFPurple|r",
                    },
                    [2] = 
                    {
                        ["ItemSet"] = "|cFFFFFF(none)|r",
                        ["ItemQty"] = 1,
                        ["ItemStyle"] = "|cFFFFFF(any)|r",
                        ["ItemLevel"] = "30",
                        ["ItemMaterials"] = 
                        {
                            ["QualityBlueMaterial"] = 
                            {
                                ["MaterialKey"] = "Embroidery",
                                ["MaterialQuantity"] = 3,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54175:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 3,
                                ["MaterialName"] = "Embroidery",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_component_002.dds",
                            },
                            ["QualityPurpleMaterial"] = 
                            {
                                ["MaterialKey"] = "Elegant Lining",
                                ["MaterialQuantity"] = 4,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54176:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 4,
                                ["MaterialName"] = "Elegant Lining",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_runecrafter_potion_sp_name_001.dds",
                            },
                            ["BaseMaterial"] = 
                            {
                                ["MaterialKey"] = "Cotton",
                                ["MaterialQuantity"] = 9,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:23125:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 9,
                                ["MaterialName"] = "Cotton",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_cloth_base_cotton_r2.dds",
                            },
                            ["QualityGreenMaterial"] = 
                            {
                                ["MaterialKey"] = "Hemming",
                                ["MaterialQuantity"] = 2,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54174:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 2,
                                ["MaterialName"] = "Hemming",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_001.dds",
                            },
                        },
                        ["ItemTrait"] = "|cFFFFFF(none)|r",
                        ["ItemName"] = "Light Shoes",
                        ["ItemText"] = "Level |cFFFF0030|r Light Shoes, |cFF00FFPurple|r quality",
                        ["ItemType"] = 
                        {
                            ["MaterialCategory"] = "Cloth",
                            ["TraitType"] = "Armor",
                            ["ItemCategory"] = "Light",
                            ["Key"] = "Shoes",
                            ["QtyModifier"] = 0,
                            ["QtyModifierV16"] = 0,
                            ["CraftName"] = "Clothing",
                            ["BodyPart"] = "Feet",
                            ["ItemName"] = "Shoes",
                            ["QtyModifierV15"] = 0,
                        },
                        ["ItemQuality"] = "|cFF00FFPurple|r",
                    },
                    [3] = 
                    {
                        ["ItemSet"] = "|cFFFFFF(none)|r",
                        ["ItemQty"] = 1,
                        ["ItemStyle"] = "|cFFFFFF(any)|r",
                        ["ItemLevel"] = "30",
                        ["ItemMaterials"] = 
                        {
                            ["QualityBlueMaterial"] = 
                            {
                                ["MaterialKey"] = "Embroidery",
                                ["MaterialQuantity"] = 3,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54175:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 3,
                                ["MaterialName"] = "Embroidery",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_component_002.dds",
                            },
                            ["QualityPurpleMaterial"] = 
                            {
                                ["MaterialKey"] = "Elegant Lining",
                                ["MaterialQuantity"] = 4,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54176:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 4,
                                ["MaterialName"] = "Elegant Lining",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_runecrafter_potion_sp_name_001.dds",
                            },
                            ["BaseMaterial"] = 
                            {
                                ["MaterialKey"] = "Cotton",
                                ["MaterialQuantity"] = 9,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:23125:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 9,
                                ["MaterialName"] = "Cotton",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_cloth_base_cotton_r2.dds",
                            },
                            ["QualityGreenMaterial"] = 
                            {
                                ["MaterialKey"] = "Hemming",
                                ["MaterialQuantity"] = 2,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54174:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 2,
                                ["MaterialName"] = "Hemming",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_001.dds",
                            },
                        },
                        ["ItemTrait"] = "|cFFFFFF(none)|r",
                        ["ItemName"] = "Light Gloves",
                        ["ItemText"] = "Level |cFFFF0030|r Light Gloves, |cFF00FFPurple|r quality",
                        ["ItemType"] = 
                        {
                            ["MaterialCategory"] = "Cloth",
                            ["TraitType"] = "Armor",
                            ["ItemCategory"] = "Light",
                            ["Key"] = "Gloves",
                            ["QtyModifier"] = 0,
                            ["QtyModifierV16"] = 0,
                            ["CraftName"] = "Clothing",
                            ["BodyPart"] = "Hands",
                            ["ItemName"] = "Gloves",
                            ["QtyModifierV15"] = 0,
                        },
                        ["ItemQuality"] = "|cFF00FFPurple|r",
                    },
                    [4] = 
                    {
                        ["ItemSet"] = "|cFFFFFF(none)|r",
                        ["ItemQty"] = 1,
                        ["ItemStyle"] = "|cFFFFFF(any)|r",
                        ["ItemLevel"] = "30",
                        ["ItemMaterials"] = 
                        {
                            ["QualityBlueMaterial"] = 
                            {
                                ["MaterialKey"] = "Embroidery",
                                ["MaterialQuantity"] = 3,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54175:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 3,
                                ["MaterialName"] = "Embroidery",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_component_002.dds",
                            },
                            ["QualityPurpleMaterial"] = 
                            {
                                ["MaterialKey"] = "Elegant Lining",
                                ["MaterialQuantity"] = 4,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54176:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 4,
                                ["MaterialName"] = "Elegant Lining",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_runecrafter_potion_sp_name_001.dds",
                            },
                            ["BaseMaterial"] = 
                            {
                                ["MaterialKey"] = "Cotton",
                                ["MaterialQuantity"] = 9,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:23125:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 9,
                                ["MaterialName"] = "Cotton",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_cloth_base_cotton_r2.dds",
                            },
                            ["QualityGreenMaterial"] = 
                            {
                                ["MaterialKey"] = "Hemming",
                                ["MaterialQuantity"] = 2,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54174:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 2,
                                ["MaterialName"] = "Hemming",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_001.dds",
                            },
                        },
                        ["ItemTrait"] = "|cFFFFFF(none)|r",
                        ["ItemName"] = "Light Hat",
                        ["ItemText"] = "Level |cFFFF0030|r Light Hat, |cFF00FFPurple|r quality",
                        ["ItemType"] = 
                        {
                            ["MaterialCategory"] = "Cloth",
                            ["TraitType"] = "Armor",
                            ["ItemCategory"] = "Light",
                            ["Key"] = "Hat",
                            ["QtyModifier"] = 0,
                            ["QtyModifierV16"] = 0,
                            ["CraftName"] = "Clothing",
                            ["BodyPart"] = "Head",
                            ["ItemName"] = "Hat",
                            ["QtyModifierV15"] = 0,
                        },
                        ["ItemQuality"] = "|cFF00FFPurple|r",
                    },
                    [5] = 
                    {
                        ["ItemSet"] = "|cFFFFFF(none)|r",
                        ["ItemQty"] = 1,
                        ["ItemStyle"] = "|cFFFFFF(any)|r",
                        ["ItemLevel"] = "30",
                        ["ItemMaterials"] = 
                        {
                            ["QualityBlueMaterial"] = 
                            {
                                ["MaterialKey"] = "Embroidery",
                                ["MaterialQuantity"] = 3,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54175:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 3,
                                ["MaterialName"] = "Embroidery",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_component_002.dds",
                            },
                            ["QualityPurpleMaterial"] = 
                            {
                                ["MaterialKey"] = "Elegant Lining",
                                ["MaterialQuantity"] = 4,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54176:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 4,
                                ["MaterialName"] = "Elegant Lining",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_runecrafter_potion_sp_name_001.dds",
                            },
                            ["BaseMaterial"] = 
                            {
                                ["MaterialKey"] = "Cotton",
                                ["MaterialQuantity"] = 10,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:23125:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 10,
                                ["MaterialName"] = "Cotton",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_cloth_base_cotton_r2.dds",
                            },
                            ["QualityGreenMaterial"] = 
                            {
                                ["MaterialKey"] = "Hemming",
                                ["MaterialQuantity"] = 2,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54174:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 2,
                                ["MaterialName"] = "Hemming",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_001.dds",
                            },
                        },
                        ["ItemTrait"] = "|cFFFFFF(none)|r",
                        ["ItemName"] = "Light Breeches",
                        ["ItemText"] = "Level |cFFFF0030|r Light Breeches, |cFF00FFPurple|r quality",
                        ["ItemType"] = 
                        {
                            ["MaterialCategory"] = "Cloth",
                            ["TraitType"] = "Armor",
                            ["ItemCategory"] = "Light",
                            ["Key"] = "Breeches",
                            ["QtyModifier"] = 1,
                            ["QtyModifierV16"] = 10,
                            ["CraftName"] = "Clothing",
                            ["BodyPart"] = "Legs",
                            ["ItemName"] = "Breeches",
                            ["QtyModifierV15"] = 1,
                        },
                        ["ItemQuality"] = "|cFF00FFPurple|r",
                    },
                    [6] = 
                    {
                        ["ItemSet"] = "|cFFFFFF(none)|r",
                        ["ItemQty"] = 1,
                        ["ItemStyle"] = "|cFFFFFF(any)|r",
                        ["ItemLevel"] = "30",
                        ["ItemMaterials"] = 
                        {
                            ["QualityBlueMaterial"] = 
                            {
                                ["MaterialKey"] = "Embroidery",
                                ["MaterialQuantity"] = 3,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54175:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 3,
                                ["MaterialName"] = "Embroidery",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_component_002.dds",
                            },
                            ["QualityPurpleMaterial"] = 
                            {
                                ["MaterialKey"] = "Elegant Lining",
                                ["MaterialQuantity"] = 4,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54176:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 4,
                                ["MaterialName"] = "Elegant Lining",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_runecrafter_potion_sp_name_001.dds",
                            },
                            ["BaseMaterial"] = 
                            {
                                ["MaterialKey"] = "Cotton",
                                ["MaterialQuantity"] = 9,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:23125:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 9,
                                ["MaterialName"] = "Cotton",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_cloth_base_cotton_r2.dds",
                            },
                            ["QualityGreenMaterial"] = 
                            {
                                ["MaterialKey"] = "Hemming",
                                ["MaterialQuantity"] = 2,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54174:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 2,
                                ["MaterialName"] = "Hemming",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_001.dds",
                            },
                        },
                        ["ItemTrait"] = "|cFFFFFF(none)|r",
                        ["ItemName"] = "Light Epaulets",
                        ["ItemText"] = "Level |cFFFF0030|r Light Epaulets, |cFF00FFPurple|r quality",
                        ["ItemType"] = 
                        {
                            ["MaterialCategory"] = "Cloth",
                            ["TraitType"] = "Armor",
                            ["ItemCategory"] = "Light",
                            ["Key"] = "Epaulets",
                            ["QtyModifier"] = 0,
                            ["QtyModifierV16"] = 0,
                            ["CraftName"] = "Clothing",
                            ["BodyPart"] = "Shoulders",
                            ["ItemName"] = "Epaulets",
                            ["QtyModifierV15"] = 0,
                        },
                        ["ItemQuality"] = "|cFF00FFPurple|r",
                    },
                    [7] = 
                    {
                        ["ItemSet"] = "|cFFFFFF(none)|r",
                        ["ItemQty"] = 1,
                        ["ItemStyle"] = "|cFFFFFF(any)|r",
                        ["ItemLevel"] = "30",
                        ["ItemMaterials"] = 
                        {
                            ["QualityBlueMaterial"] = 
                            {
                                ["MaterialKey"] = "Embroidery",
                                ["MaterialQuantity"] = 3,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54175:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 3,
                                ["MaterialName"] = "Embroidery",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_component_002.dds",
                            },
                            ["QualityPurpleMaterial"] = 
                            {
                                ["MaterialKey"] = "Elegant Lining",
                                ["MaterialQuantity"] = 4,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54176:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 4,
                                ["MaterialName"] = "Elegant Lining",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_runecrafter_potion_sp_name_001.dds",
                            },
                            ["BaseMaterial"] = 
                            {
                                ["MaterialKey"] = "Cotton",
                                ["MaterialQuantity"] = 9,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:23125:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 9,
                                ["MaterialName"] = "Cotton",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_cloth_base_cotton_r2.dds",
                            },
                            ["QualityGreenMaterial"] = 
                            {
                                ["MaterialKey"] = "Hemming",
                                ["MaterialQuantity"] = 2,
                                ["CraftName"] = "Clothing",
                                ["MaterialLink"] = "|H1:item:54174:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 2,
                                ["MaterialName"] = "Hemming",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_light_armor_vendor_001.dds",
                            },
                        },
                        ["ItemTrait"] = "|cFFFFFF(none)|r",
                        ["ItemName"] = "Light Sash",
                        ["ItemText"] = "Level |cFFFF0030|r Light Sash, |cFF00FFPurple|r quality",
                        ["ItemType"] = 
                        {
                            ["MaterialCategory"] = "Cloth",
                            ["TraitType"] = "Armor",
                            ["ItemCategory"] = "Light",
                            ["Key"] = "Sash",
                            ["QtyModifier"] = 0,
                            ["QtyModifierV16"] = 0,
                            ["CraftName"] = "Clothing",
                            ["BodyPart"] = "Waist",
                            ["ItemName"] = "Sash",
                            ["QtyModifierV15"] = 0,
                        },
                        ["ItemQuality"] = "|cFF00FFPurple|r",
                    },
                    [8] = 
                    {
                        ["ItemSet"] = "|cFFFFFF(none)|r",
                        ["ItemQty"] = 1,
                        ["ItemStyle"] = "|cFFFFFF(any)|r",
                        ["ItemLevel"] = "30",
                        ["ItemMaterials"] = 
                        {
                            ["QualityBlueMaterial"] = 
                            {
                                ["MaterialKey"] = "Turpen",
                                ["MaterialQuantity"] = 3,
                                ["CraftName"] = "Woodworking",
                                ["MaterialLink"] = "|H1:item:54179:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 3,
                                ["MaterialName"] = "Turpen",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_wood_turpen.dds",
                            },
                            ["QualityPurpleMaterial"] = 
                            {
                                ["MaterialKey"] = "Mastic",
                                ["MaterialQuantity"] = 4,
                                ["CraftName"] = "Woodworking",
                                ["MaterialLink"] = "|H1:item:54180:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 4,
                                ["MaterialName"] = "Mastic",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_wood_mastic.dds",
                            },
                            ["BaseMaterial"] = 
                            {
                                ["MaterialKey"] = "Beech",
                                ["MaterialQuantity"] = 7,
                                ["CraftName"] = "Woodworking",
                                ["MaterialLink"] = "|H1:item:23121:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 7,
                                ["MaterialName"] = "Beech",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_wood_base_beech_r3.dds",
                            },
                            ["QualityGreenMaterial"] = 
                            {
                                ["MaterialKey"] = "Pitch",
                                ["MaterialQuantity"] = 2,
                                ["CraftName"] = "Woodworking",
                                ["MaterialLink"] = "|H1:item:54178:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 2,
                                ["MaterialName"] = "Pitch",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_forester_weapon_vendor_component_002.dds",
                            },
                        },
                        ["ItemTrait"] = "|cFFFFFF(none)|r",
                        ["ItemName"] = "Lightning Staff",
                        ["ItemText"] = "Level |cFFFF0030|r Lightning Staff, |cFF00FFPurple|r quality",
                        ["ItemType"] = 
                        {
                            ["MaterialCategory"] = "Wood",
                            ["TraitType"] = "Weapon",
                            ["ItemCategory"] = "",
                            ["Key"] = "Lightning",
                            ["QtyModifier"] = 0,
                            ["QtyModifierV16"] = 0,
                            ["CraftName"] = "Woodworking",
                            ["BodyPart"] = "",
                            ["ItemName"] = "Lightning Staff",
                            ["QtyModifierV15"] = 0,
                        },
                        ["ItemQuality"] = "|cFF00FFPurple|r",
                    },
                    [9] = 
                    {
                        ["ItemSet"] = "|cFFFFFF(none)|r",
                        ["ItemQty"] = 1,
                        ["ItemStyle"] = "|cFFFFFF(any)|r",
                        ["ItemLevel"] = "30",
                        ["ItemMaterials"] = 
                        {
                            ["QualityBlueMaterial"] = 
                            {
                                ["MaterialKey"] = "Turpen",
                                ["MaterialQuantity"] = 3,
                                ["CraftName"] = "Woodworking",
                                ["MaterialLink"] = "|H1:item:54179:32:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 3,
                                ["MaterialName"] = "Turpen",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_wood_turpen.dds",
                            },
                            ["QualityPurpleMaterial"] = 
                            {
                                ["MaterialKey"] = "Mastic",
                                ["MaterialQuantity"] = 4,
                                ["CraftName"] = "Woodworking",
                                ["MaterialLink"] = "|H1:item:54180:33:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 4,
                                ["MaterialName"] = "Mastic",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_wood_mastic.dds",
                            },
                            ["BaseMaterial"] = 
                            {
                                ["MaterialKey"] = "Beech",
                                ["MaterialQuantity"] = 7,
                                ["CraftName"] = "Woodworking",
                                ["MaterialLink"] = "|H1:item:23121:30:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 7,
                                ["MaterialName"] = "Beech",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_wood_base_beech_r3.dds",
                            },
                            ["QualityGreenMaterial"] = 
                            {
                                ["MaterialKey"] = "Pitch",
                                ["MaterialQuantity"] = 2,
                                ["CraftName"] = "Woodworking",
                                ["MaterialLink"] = "|H1:item:54178:31:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                ["MaterialTotalQuantity"] = 2,
                                ["MaterialName"] = "Pitch",
                                ["MaterialTexture"] = "/esoui/art/icons/crafting_forester_weapon_vendor_component_002.dds",
                            },
                        },
                        ["ItemTrait"] = "|cFFFFFF(none)|r",
                        ["ItemName"] = "Restoration Staff",
                        ["ItemText"] = "Level |cFFFF0030|r Restoration Staff, |cFF00FFPurple|r quality",
                        ["ItemType"] = 
                        {
                            ["MaterialCategory"] = "Wood",
                            ["TraitType"] = "Weapon",
                            ["ItemCategory"] = "",
                            ["Key"] = "Resto",
                            ["QtyModifier"] = 0,
                            ["QtyModifierV16"] = 0,
                            ["CraftName"] = "Woodworking",
                            ["BodyPart"] = "",
                            ["ItemName"] = "Restoration Staff",
                            ["QtyModifierV15"] = 0,
                        },
                        ["ItemQuality"] = "|cFF00FFPurple|r",
                    },
                },
                ["version"] = 2,
                ["WindowPosition"] = 
                {
                    ["tccUI"] = 
                    {
                        ["y"] = 105.0955581665,
                        ["x"] = 251.0258483887,
                    },
                },
            },
            ["Testimate"] = 
            {
                ["TabSelected"] = "tccBuilderTabButton",
                ["CraftSelected"] = "Blacksmithing",
                ["version"] = 2,
            },
            ["$AccountWide"] = 
            {
                ["Scope"] = 
                {
                    ["ItemOrder"] = "Local",
                },
                ["EsoPlus"] = true,
                ["version"] = 2,
            },
        },
    },
}
