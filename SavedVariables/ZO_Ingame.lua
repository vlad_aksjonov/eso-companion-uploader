ZO_Ingame_SavedVariables =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Bswan"] = 
            {
                ["WorldMap"] = 
                {
                    [1] = 
                    {
                        ["keepSquare"] = true,
                        ["y"] = 0,
                        ["x"] = 0,
                        ["relPoint"] = 128,
                        ["width"] = 488,
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["point"] = 128,
                        ["height"] = 550,
                        ["mapSize"] = 2,
                    },
                    [2] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                                [144] = false,
                                [1] = true,
                                [156] = false,
                                [8] = true,
                                [145] = false,
                                [9] = true,
                            },
                            [2] = 
                            {
                                [1] = true,
                                [3] = true,
                                [5] = true,
                                [6] = true,
                                [7] = true,
                                [8] = true,
                                [9] = true,
                                [10] = true,
                                [11] = 1,
                                [156] = false,
                                [145] = false,
                                [12] = true,
                            },
                            [3] = 
                            {
                                [156] = false,
                                [145] = false,
                            },
                        },
                        ["mapSize"] = 1,
                    },
                    [3] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [1] = true,
                                [3] = false,
                                [4] = false,
                                [5] = false,
                                [7] = false,
                                [8] = false,
                                [11] = 1,
                                [156] = false,
                                [145] = false,
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [4] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                                [144] = false,
                                [1] = true,
                                [156] = false,
                                [9] = true,
                                [8] = true,
                                [145] = false,
                            },
                            [2] = 
                            {
                                [145] = false,
                                [2] = false,
                                [3] = false,
                                [156] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [5] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = true,
                                [1] = true,
                                [3] = false,
                                [156] = false,
                                [11] = 1,
                                [145] = false,
                            },
                            [3] = 
                            {
                                [156] = false,
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    ["userMode"] = 2,
                    ["version"] = 4,
                },
                ["DurabilityWarner"] = 
                {
                    ["version"] = 1,
                },
                ["PerformanceMeters"] = 
                {
                    ["point"] = 3,
                    ["y"] = -11.6116943359,
                    ["x"] = -20,
                    ["relPoint"] = 3,
                    ["version"] = 1,
                },
                ["SmithingCreation"] = 
                {
                    ["haveKnowledgeChecked"] = false,
                    ["version"] = 2,
                    ["useUniversalStyleItemChecked"] = false,
                    ["haveMaterialChecked"] = false,
                },
                ["Dyeing"] = 
                {
                    ["version"] = 1,
                    ["showLocked"] = false,
                    ["sortStyle"] = 1,
                },
                ["DeathRecap"] = 
                {
                    ["recapOn"] = true,
                    ["version"] = 1,
                },
                ["AutoComplete"] = 
                {
                    ["RecentInteractions"] = 
                    {
                        ["Count Drako"] = 1463687767,
                        ["Labrea"] = 1464115431,
                        ["Na'ada"] = 1465330003,
                        ["Aeristrae"] = 1465330003,
                        ["Ragewolfnl"] = 1464115431,
                        ["Fizzle My Drizzle"] = 1464114840,
                        ["Eldran Elderroot"] = 1465562689,
                        ["Kelago"] = 1464115431,
                        ["@dono122"] = 1463515235,
                        ["@Missty777"] = 1463436232,
                        ["Aika Asahari"] = 1465299413,
                        ["Gwirithiel Nellethiel"] = 1465562600,
                        ["Old'twi"] = 1464115431,
                        ["Nirephona Stormwatch"] = 1464113944,
                        ["Linanja"] = 1463225741,
                        ["@ZaaRauL"] = 1465233475,
                        ["Blondespell"] = 1463687767,
                        ["Adelyn"] = 1465145725,
                        ["@maxnum1"] = 1465562689,
                        ["Rhom'an Khiin"] = 1464115431,
                        ["@Tr1smegistos"] = 1464120737,
                        ["@Erondill"] = 1464980275,
                        ["Garfield-not so brave"] = 1465330003,
                        ["Nialith"] = 1465330003,
                        ["Richard Fortesque"] = 1463182422,
                        ["Khatnip Khemist"] = 1463687767,
                        ["Palomita Torres"] = 1464169390,
                        ["Purr-Purr'Dar"] = 1463421865,
                        ["Luturiel"] = 1463687767,
                        ["Droza Kmo"] = 1463687767,
                        ["@Linanja"] = 1463225741,
                        ["Sherpion"] = 1463687217,
                        ["Indra the Immortal"] = 1464206074,
                        [""] = 1464641834,
                        ["Mayorz"] = 1463687767,
                        ["Sophious"] = 1463687767,
                        ["@Bundaberg"] = 1464169390,
                        ["Fizzeehh"] = 1465330003,
                        ["Kathochen"] = 1464115431,
                        ["Sharlyn Claddzynge"] = 1465128159,
                        ["Raven Dreadclaw"] = 1463687767,
                        ["Rá-Thajiit"] = 1463685756,
                        ["@Xenowarrior92"] = 1465299414,
                        ["@cookieofdarkness"] = 1463507378,
                        ["@kadaur"] = 1465145725,
                    },
                    ["version"] = 3,
                },
                ["Provisioner"] = 
                {
                    ["haveIngredientsChecked"] = true,
                    ["haveSkillsChecked"] = true,
                    ["version"] = 1,
                },
                ["Chat"] = 
                {
                    ["version"] = 4,
                    ["containers"] = 
                    {
                        [1] = 
                        {
                            ["point"] = 1,
                            ["y"] = 743.0405883789,
                            ["x"] = -737.2026367188,
                            ["relPoint"] = 1,
                            ["height"] = 264.9796142578,
                            ["width"] = 465.2042236328,
                        },
                    },
                },
                ["GamepadProvisioner"] = 
                {
                    ["haveIngredientsChecked"] = false,
                    ["haveSkillsChecked"] = false,
                    ["version"] = 2,
                },
            },
            ["Purr-Purr'Dar"] = 
            {
                ["WorldMap"] = 
                {
                    [1] = 
                    {
                        ["keepSquare"] = true,
                        ["y"] = 0,
                        ["x"] = 0,
                        ["relPoint"] = 128,
                        ["width"] = 488,
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["point"] = 128,
                        ["height"] = 550,
                        ["mapSize"] = 2,
                    },
                    [2] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                                [8] = true,
                                [145] = false,
                                [132] = true,
                                [1] = true,
                                [142] = true,
                                [9] = true,
                            },
                            [2] = 
                            {
                                [145] = true,
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                    },
                    [3] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [145] = true,
                                [3] = false,
                                [4] = false,
                                [5] = false,
                                [11] = 1,
                                [7] = false,
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [4] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                                [145] = false,
                            },
                            [2] = 
                            {
                                [11] = 1,
                                [145] = true,
                                [2] = false,
                                [3] = false,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [5] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [11] = 1,
                                [3] = false,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    ["userMode"] = 2,
                    ["version"] = 4,
                },
                ["DurabilityWarner"] = 
                {
                    ["version"] = 1,
                },
                ["PerformanceMeters"] = 
                {
                    ["point"] = 3,
                    ["y"] = -20,
                    ["x"] = -20,
                    ["relPoint"] = 3,
                    ["version"] = 1,
                },
                ["SmithingCreation"] = 
                {
                    ["haveKnowledgeChecked"] = true,
                    ["version"] = 2,
                    ["useUniversalStyleItemChecked"] = false,
                    ["haveMaterialChecked"] = false,
                },
                ["Dyeing"] = 
                {
                    ["version"] = 1,
                    ["showLocked"] = false,
                    ["sortStyle"] = 1,
                },
                ["DeathRecap"] = 
                {
                    ["recapOn"] = true,
                    ["version"] = 1,
                },
                ["AutoComplete"] = 
                {
                    ["RecentInteractions"] = 
                    {
                        ["Ysanaura Sanciela"] = 1461362610,
                        ["@fortesque_eso"] = 1463176212,
                        ["Linanja"] = 1463176212,
                        ["Daxfuryan"] = 1461949914,
                        ["Adelyn"] = 1463397530,
                        ["Pzyko Chaoz"] = 1463333495,
                        ["@lewis211rr"] = 1461951463,
                        ["Gwirithiel Nellethiel"] = 1463397530,
                        ["Sharlyn Claddzynge"] = 1462740321,
                        ["@Linanja"] = 1463176212,
                        ["K Shara"] = 1462222723,
                        ["Richard Fortesque"] = 1463176212,
                        ["Bswan"] = 1463397530,
                        ["@ZaaRauL"] = 1462787764,
                        ["sanui"] = 1461952071,
                        ["@kadaur"] = 1463332904,
                    },
                    ["version"] = 3,
                },
                ["Provisioner"] = 
                {
                    ["haveIngredientsChecked"] = true,
                    ["haveSkillsChecked"] = true,
                    ["version"] = 1,
                },
                ["Chat"] = 
                {
                    ["version"] = 4,
                    ["containers"] = 
                    {
                        [1] = 
                        {
                            ["point"] = 6,
                            ["y"] = -82,
                            ["x"] = 0,
                            ["relPoint"] = 6,
                            ["height"] = 267,
                            ["width"] = 445,
                        },
                    },
                },
                ["GamepadProvisioner"] = 
                {
                    ["haveIngredientsChecked"] = false,
                    ["haveSkillsChecked"] = false,
                    ["version"] = 2,
                },
            },
            ["Testimate"] = 
            {
                ["WorldMap"] = 
                {
                    [1] = 
                    {
                        ["keepSquare"] = true,
                        ["y"] = 0,
                        ["x"] = 0,
                        ["relPoint"] = 128,
                        ["width"] = 488,
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["point"] = 128,
                        ["height"] = 550,
                        ["mapSize"] = 2,
                    },
                    [2] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                    },
                    [3] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [3] = false,
                                [4] = false,
                                [5] = false,
                                [11] = 1,
                                [7] = false,
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [4] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [11] = 1,
                                [2] = false,
                                [3] = false,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [5] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [11] = 1,
                                [3] = false,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    ["userMode"] = 2,
                    ["version"] = 4,
                },
                ["DurabilityWarner"] = 
                {
                    ["version"] = 1,
                },
                ["PerformanceMeters"] = 
                {
                    ["point"] = 6,
                    ["y"] = 20,
                    ["x"] = -20,
                    ["relPoint"] = 6,
                    ["version"] = 1,
                },
                ["SmithingCreation"] = 
                {
                    ["haveKnowledgeChecked"] = true,
                    ["version"] = 2,
                    ["useUniversalStyleItemChecked"] = false,
                    ["haveMaterialChecked"] = false,
                },
                ["Dyeing"] = 
                {
                    ["version"] = 1,
                    ["showLocked"] = true,
                    ["sortStyle"] = 1,
                },
                ["DeathRecap"] = 
                {
                    ["recapOn"] = true,
                    ["version"] = 1,
                },
                ["AutoComplete"] = 
                {
                    ["RecentInteractions"] = 
                    {
                        ["Adelyn"] = 1462317187,
                        ["@ZaaRauL"] = 1461782406,
                        ["Sharlyn Claddzynge"] = 1461782406,
                        ["@kadaur"] = 1462317187,
                    },
                    ["version"] = 3,
                },
                ["Provisioner"] = 
                {
                    ["haveIngredientsChecked"] = true,
                    ["haveSkillsChecked"] = true,
                    ["version"] = 1,
                },
                ["Chat"] = 
                {
                    ["version"] = 4,
                    ["containers"] = 
                    {
                        [1] = 
                        {
                            ["point"] = 6,
                            ["y"] = -82,
                            ["x"] = 0,
                            ["relPoint"] = 6,
                            ["height"] = 267,
                            ["width"] = 445,
                        },
                    },
                },
                ["GamepadProvisioner"] = 
                {
                    ["haveIngredientsChecked"] = false,
                    ["haveSkillsChecked"] = false,
                    ["version"] = 2,
                },
            },
            ["Lb-Alchemy"] = 
            {
                ["WorldMap"] = 
                {
                    [1] = 
                    {
                        ["keepSquare"] = true,
                        ["y"] = 0,
                        ["x"] = 0,
                        ["relPoint"] = 128,
                        ["width"] = 488,
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["point"] = 128,
                        ["height"] = 550,
                        ["mapSize"] = 2,
                    },
                    [2] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                                [9] = true,
                                [131] = false,
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                    },
                    [3] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [3] = false,
                                [4] = false,
                                [5] = false,
                                [11] = 1,
                                [7] = false,
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [4] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [3] = false,
                                [2] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [5] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [3] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    ["userMode"] = 2,
                    ["version"] = 4,
                },
                ["DurabilityWarner"] = 
                {
                    ["version"] = 1,
                },
                ["PerformanceMeters"] = 
                {
                    ["point"] = 6,
                    ["y"] = 20,
                    ["x"] = -20,
                    ["relPoint"] = 6,
                    ["version"] = 1,
                },
                ["SmithingCreation"] = 
                {
                    ["haveKnowledgeChecked"] = true,
                    ["version"] = 2,
                    ["useUniversalStyleItemChecked"] = false,
                    ["haveMaterialChecked"] = false,
                },
                ["Dyeing"] = 
                {
                    ["version"] = 1,
                    ["showLocked"] = true,
                    ["sortStyle"] = 1,
                },
                ["DeathRecap"] = 
                {
                    ["recapOn"] = true,
                    ["version"] = 1,
                },
                ["AutoComplete"] = 
                {
                    ["RecentInteractions"] = 
                    {
                        ["@ZaaRauL"] = 1461785289,
                        ["Sharlyn Claddzynge"] = 1461785289,
                    },
                    ["version"] = 3,
                },
                ["Provisioner"] = 
                {
                    ["haveIngredientsChecked"] = true,
                    ["haveSkillsChecked"] = true,
                    ["version"] = 1,
                },
                ["Chat"] = 
                {
                    ["version"] = 4,
                    ["containers"] = 
                    {
                        [1] = 
                        {
                            ["point"] = 6,
                            ["y"] = -82,
                            ["x"] = 0,
                            ["relPoint"] = 6,
                            ["height"] = 267,
                            ["width"] = 445,
                        },
                    },
                },
                ["GamepadProvisioner"] = 
                {
                    ["haveIngredientsChecked"] = false,
                    ["haveSkillsChecked"] = false,
                    ["version"] = 2,
                },
            },
            ["$AccountWide"] = 
            {
                ["GuildMotD"] = 
                {
                    ["Aequilibration"] = 0,
                    ["version"] = 1,
                    ["The Traveling Merchant"] = 3609353352,
                    ["The Lion Brotherhood"] = 2377982129,
                    ["Bandits Black Market"] = 1170474248,
                    ["Army of Fighters"] = 0,
                    ["First Kohorte of Asmodeus"] = 3365960957,
                    ["E-Shop Trade Boutique"] = 155162733,
                    ["Brave Cat Trade"] = 4128293818,
                    ["Team Gold"] = 779126561,
                },
            },
            ["Razum-Dar in Excile"] = 
            {
                ["WorldMap"] = 
                {
                    [1] = 
                    {
                        ["keepSquare"] = true,
                        ["y"] = 0,
                        ["x"] = 0,
                        ["relPoint"] = 128,
                        ["width"] = 488,
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["point"] = 128,
                        ["height"] = 550,
                        ["mapSize"] = 2,
                    },
                    [2] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                                [8] = true,
                                [1] = true,
                                [131] = false,
                                [144] = false,
                                [145] = false,
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                    },
                    [3] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [3] = false,
                                [4] = false,
                                [5] = false,
                                [11] = 1,
                                [7] = false,
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [4] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                                [144] = false,
                                [145] = false,
                                [131] = false,
                            },
                            [2] = 
                            {
                                [3] = false,
                                [2] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [5] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [3] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    ["userMode"] = 2,
                    ["version"] = 4,
                },
                ["DurabilityWarner"] = 
                {
                    ["version"] = 1,
                },
                ["PerformanceMeters"] = 
                {
                    ["point"] = 6,
                    ["y"] = 20,
                    ["x"] = -20,
                    ["relPoint"] = 6,
                    ["version"] = 1,
                },
                ["SmithingCreation"] = 
                {
                    ["haveKnowledgeChecked"] = true,
                    ["version"] = 2,
                    ["useUniversalStyleItemChecked"] = false,
                    ["haveMaterialChecked"] = false,
                },
                ["Dyeing"] = 
                {
                    ["version"] = 1,
                    ["showLocked"] = false,
                    ["sortStyle"] = 1,
                },
                ["DeathRecap"] = 
                {
                    ["recapOn"] = true,
                    ["version"] = 1,
                },
                ["AutoComplete"] = 
                {
                    ["RecentInteractions"] = 
                    {
                        ["@ZaaRauL"] = 1460575350,
                        ["Dwergkabouter"] = 1460798684,
                        ["Skapi Halbtroll"] = 1459967593,
                        ["Sharlyn Claddzynge"] = 1464025350,
                    },
                    ["version"] = 3,
                },
                ["Provisioner"] = 
                {
                    ["haveIngredientsChecked"] = true,
                    ["haveSkillsChecked"] = true,
                    ["version"] = 1,
                },
                ["Chat"] = 
                {
                    ["version"] = 4,
                    ["containers"] = 
                    {
                        [1] = 
                        {
                            ["point"] = 2,
                            ["y"] = 330.0151977539,
                            ["x"] = 0,
                            ["relPoint"] = 2,
                            ["height"] = 267,
                            ["width"] = 445,
                        },
                    },
                },
                ["GamepadProvisioner"] = 
                {
                    ["haveIngredientsChecked"] = false,
                    ["haveSkillsChecked"] = false,
                    ["version"] = 2,
                },
            },
            ["Lb-Enchanting"] = 
            {
                ["WorldMap"] = 
                {
                    [1] = 
                    {
                        ["keepSquare"] = true,
                        ["y"] = 0,
                        ["x"] = 0,
                        ["relPoint"] = 128,
                        ["width"] = 488,
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["point"] = 128,
                        ["height"] = 550,
                        ["mapSize"] = 2,
                    },
                    [2] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                    },
                    [3] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [3] = false,
                                [4] = false,
                                [5] = false,
                                [11] = 1,
                                [7] = false,
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [4] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [3] = false,
                                [2] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [5] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [3] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    ["userMode"] = 2,
                    ["version"] = 4,
                },
                ["DurabilityWarner"] = 
                {
                    ["version"] = 1,
                },
                ["PerformanceMeters"] = 
                {
                    ["point"] = 6,
                    ["y"] = 20,
                    ["x"] = -20,
                    ["relPoint"] = 6,
                    ["version"] = 1,
                },
                ["SmithingCreation"] = 
                {
                    ["haveKnowledgeChecked"] = true,
                    ["version"] = 2,
                    ["useUniversalStyleItemChecked"] = false,
                    ["haveMaterialChecked"] = false,
                },
                ["Dyeing"] = 
                {
                    ["version"] = 1,
                    ["showLocked"] = true,
                    ["sortStyle"] = 1,
                },
                ["DeathRecap"] = 
                {
                    ["recapOn"] = true,
                    ["version"] = 1,
                },
                ["AutoComplete"] = 
                {
                    ["RecentInteractions"] = 
                    {
                        ["@Bundaberg"] = 1463340241,
                        ["Palomita Torres"] = 1463340241,
                    },
                    ["version"] = 3,
                },
                ["Provisioner"] = 
                {
                    ["haveIngredientsChecked"] = true,
                    ["haveSkillsChecked"] = true,
                    ["version"] = 1,
                },
                ["Chat"] = 
                {
                    ["version"] = 4,
                    ["containers"] = 
                    {
                        [1] = 
                        {
                            ["point"] = 6,
                            ["y"] = -82,
                            ["x"] = 0,
                            ["relPoint"] = 6,
                            ["height"] = 267,
                            ["width"] = 445,
                        },
                    },
                },
                ["GamepadProvisioner"] = 
                {
                    ["haveIngredientsChecked"] = false,
                    ["haveSkillsChecked"] = false,
                    ["version"] = 2,
                },
            },
            ["Purr-Purr'dar"] = 
            {
                ["WorldMap"] = 
                {
                    [1] = 
                    {
                        ["keepSquare"] = true,
                        ["y"] = 0,
                        ["x"] = 0,
                        ["relPoint"] = 128,
                        ["width"] = 488,
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["point"] = 128,
                        ["height"] = 550,
                        ["mapSize"] = 2,
                    },
                    [2] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                                [144] = false,
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                    },
                    [3] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [3] = false,
                                [4] = false,
                                [5] = false,
                                [11] = 1,
                                [7] = false,
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [4] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [11] = 1,
                                [2] = false,
                                [3] = false,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [5] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [11] = 1,
                                [3] = false,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    ["userMode"] = 2,
                    ["version"] = 4,
                },
                ["DurabilityWarner"] = 
                {
                    ["version"] = 1,
                },
                ["PerformanceMeters"] = 
                {
                    ["point"] = 6,
                    ["y"] = 20,
                    ["x"] = -20,
                    ["relPoint"] = 6,
                    ["version"] = 1,
                },
                ["SmithingCreation"] = 
                {
                    ["haveKnowledgeChecked"] = true,
                    ["version"] = 2,
                    ["useUniversalStyleItemChecked"] = false,
                    ["haveMaterialChecked"] = false,
                },
                ["Dyeing"] = 
                {
                    ["version"] = 1,
                    ["showLocked"] = true,
                    ["sortStyle"] = 1,
                },
                ["DeathRecap"] = 
                {
                    ["recapOn"] = true,
                    ["version"] = 1,
                },
                ["AutoComplete"] = 
                {
                    ["RecentInteractions"] = 
                    {
                        ["Gwirithiel Nellethiel"] = 1460918814,
                    },
                    ["version"] = 3,
                },
                ["Provisioner"] = 
                {
                    ["haveIngredientsChecked"] = true,
                    ["haveSkillsChecked"] = true,
                    ["version"] = 1,
                },
                ["Chat"] = 
                {
                    ["version"] = 4,
                    ["containers"] = 
                    {
                        [1] = 
                        {
                            ["point"] = 6,
                            ["y"] = -82,
                            ["x"] = 0,
                            ["relPoint"] = 6,
                            ["height"] = 267,
                            ["width"] = 445,
                        },
                    },
                },
                ["GamepadProvisioner"] = 
                {
                    ["haveIngredientsChecked"] = false,
                    ["haveSkillsChecked"] = false,
                    ["version"] = 2,
                },
            },
            ["Lb-Grocery"] = 
            {
                ["WorldMap"] = 
                {
                    [1] = 
                    {
                        ["keepSquare"] = true,
                        ["y"] = 0,
                        ["x"] = 0,
                        ["relPoint"] = 128,
                        ["width"] = 488,
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["point"] = 128,
                        ["height"] = 550,
                        ["mapSize"] = 2,
                    },
                    [2] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                                [131] = false,
                            },
                            [2] = 
                            {
                                [1] = false,
                                [6] = false,
                                [11] = 1,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                    },
                    [3] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [3] = false,
                                [4] = false,
                                [5] = false,
                                [11] = 1,
                                [7] = false,
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [4] = 
                    {
                        ["filters"] = 
                        {
                            [1] = 
                            {
                            },
                            [2] = 
                            {
                                [11] = 1,
                                [2] = false,
                                [3] = false,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    [5] = 
                    {
                        ["filters"] = 
                        {
                            [2] = 
                            {
                                [8] = false,
                                [1] = false,
                                [11] = 1,
                                [3] = false,
                            },
                            [3] = 
                            {
                            },
                        },
                        ["mapSize"] = 1,
                        ["allowHistory"] = false,
                    },
                    ["userMode"] = 2,
                    ["version"] = 4,
                },
                ["DurabilityWarner"] = 
                {
                    ["version"] = 1,
                },
                ["PerformanceMeters"] = 
                {
                    ["point"] = 6,
                    ["y"] = 20,
                    ["x"] = -20,
                    ["relPoint"] = 6,
                    ["version"] = 1,
                },
                ["SmithingCreation"] = 
                {
                    ["haveKnowledgeChecked"] = true,
                    ["version"] = 2,
                    ["useUniversalStyleItemChecked"] = false,
                    ["haveMaterialChecked"] = false,
                },
                ["Dyeing"] = 
                {
                    ["version"] = 1,
                    ["showLocked"] = true,
                    ["sortStyle"] = 1,
                },
                ["DeathRecap"] = 
                {
                    ["recapOn"] = true,
                    ["version"] = 1,
                },
                ["AutoComplete"] = 
                {
                    ["RecentInteractions"] = 
                    {
                    },
                    ["version"] = 3,
                },
                ["Provisioner"] = 
                {
                    ["haveIngredientsChecked"] = true,
                    ["haveSkillsChecked"] = true,
                    ["version"] = 1,
                },
                ["Chat"] = 
                {
                    ["version"] = 4,
                    ["containers"] = 
                    {
                        [1] = 
                        {
                            ["point"] = 6,
                            ["y"] = -82,
                            ["x"] = 0,
                            ["relPoint"] = 6,
                            ["height"] = 267,
                            ["width"] = 445,
                        },
                    },
                },
                ["GamepadProvisioner"] = 
                {
                    ["haveIngredientsChecked"] = false,
                    ["haveSkillsChecked"] = false,
                    ["version"] = 2,
                },
            },
        },
    },
}
