LOST_TREASURE_SV =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Lb-Grocery"] = 
            {
                ["version"] = 3,
                ["pinTextureSize"] = 32,
                ["treasurePinTexture"] = 1,
                ["showSurveysCompass"] = true,
                ["treasureMarkMapMenuOption"] = 2,
                ["apiVersion"] = 100014,
                ["miniTreasureMap"] = 
                {
                    ["relativePoint"] = 3,
                    ["point"] = 3,
                    ["offsetY"] = 100,
                    ["offsetX"] = 100,
                },
                ["surveysMarkMapMenuOption"] = 2,
                ["surveysPinTexture"] = 1,
                ["showSurveys"] = true,
                ["showMiniTreasureMap"] = true,
                ["showTreasureCompass"] = true,
                ["showTreasure"] = true,
            },
            ["Bswan"] = 
            {
                ["apiVersion"] = 100014,
                ["pinTextureSize"] = 32,
                ["treasurePinTexture"] = 1,
                ["showSurveysCompass"] = true,
                ["showTreasure"] = true,
                ["showMiniTreasureMap"] = true,
                ["surveysMarkMapMenuOption"] = 2,
                ["miniTreasureMap"] = 
                {
                    ["point"] = 6,
                    ["offsetY"] = -341.5076904297,
                    ["offsetX"] = -0,
                    ["relativePoint"] = 6,
                    ["relativeTo"] = "GuiRoot",
                },
                ["treasureMarkMapMenuOption"] = 2,
                ["version"] = 3,
                ["surveysPinTexture"] = 1,
                ["showSurveys"] = true,
                ["showTreasureCompass"] = true,
                ["markerDeletionDelay"] = 10,
            },
            ["Purr-Purr'Dar"] = 
            {
                ["version"] = 3,
                ["pinTextureSize"] = 32,
                ["treasurePinTexture"] = 1,
                ["showSurveysCompass"] = true,
                ["treasureMarkMapMenuOption"] = 2,
                ["apiVersion"] = 100014,
                ["miniTreasureMap"] = 
                {
                    ["relativePoint"] = 3,
                    ["point"] = 3,
                    ["offsetY"] = 100,
                    ["offsetX"] = 100,
                },
                ["surveysMarkMapMenuOption"] = 2,
                ["surveysPinTexture"] = 1,
                ["showSurveys"] = true,
                ["showMiniTreasureMap"] = true,
                ["showTreasureCompass"] = true,
                ["showTreasure"] = true,
            },
            ["Razum-Dar in Excile"] = 
            {
                ["version"] = 3,
                ["pinTextureSize"] = 32,
                ["treasurePinTexture"] = 1,
                ["showSurveysCompass"] = true,
                ["treasureMarkMapMenuOption"] = 2,
                ["apiVersion"] = 100014,
                ["miniTreasureMap"] = 
                {
                    ["relativePoint"] = 3,
                    ["point"] = 3,
                    ["offsetY"] = 100,
                    ["offsetX"] = 100,
                },
                ["surveysMarkMapMenuOption"] = 2,
                ["surveysPinTexture"] = 1,
                ["showSurveys"] = true,
                ["showMiniTreasureMap"] = true,
                ["showTreasureCompass"] = true,
                ["showTreasure"] = true,
            },
            ["Lb-Enchanting"] = 
            {
                ["version"] = 3,
                ["pinTextureSize"] = 32,
                ["treasurePinTexture"] = 1,
                ["showSurveysCompass"] = true,
                ["treasureMarkMapMenuOption"] = 2,
                ["apiVersion"] = 100014,
                ["miniTreasureMap"] = 
                {
                    ["relativePoint"] = 3,
                    ["point"] = 3,
                    ["offsetY"] = 100,
                    ["offsetX"] = 100,
                },
                ["surveysMarkMapMenuOption"] = 2,
                ["surveysPinTexture"] = 1,
                ["showSurveys"] = true,
                ["showMiniTreasureMap"] = true,
                ["showTreasureCompass"] = true,
                ["showTreasure"] = true,
            },
            ["Testimate"] = 
            {
                ["apiVersion"] = 100014,
                ["pinTextureSize"] = 32,
                ["treasurePinTexture"] = 1,
                ["showSurveysCompass"] = true,
                ["showTreasure"] = true,
                ["showMiniTreasureMap"] = true,
                ["surveysMarkMapMenuOption"] = 2,
                ["miniTreasureMap"] = 
                {
                    ["relativePoint"] = 3,
                    ["point"] = 3,
                    ["offsetY"] = 100,
                    ["offsetX"] = 100,
                },
                ["treasureMarkMapMenuOption"] = 2,
                ["version"] = 3,
                ["surveysPinTexture"] = 1,
                ["showSurveys"] = true,
                ["showTreasureCompass"] = true,
                ["markerDeletionDelay"] = 10,
            },
            ["Purr-Purr'dar"] = 
            {
                ["version"] = 3,
                ["pinTextureSize"] = 32,
                ["treasurePinTexture"] = 1,
                ["showSurveysCompass"] = true,
                ["treasureMarkMapMenuOption"] = 2,
                ["apiVersion"] = 100014,
                ["miniTreasureMap"] = 
                {
                    ["relativePoint"] = 3,
                    ["point"] = 3,
                    ["offsetY"] = 100,
                    ["offsetX"] = 100,
                },
                ["surveysMarkMapMenuOption"] = 2,
                ["surveysPinTexture"] = 1,
                ["showSurveys"] = true,
                ["showMiniTreasureMap"] = true,
                ["showTreasureCompass"] = true,
                ["showTreasure"] = true,
            },
            ["Lb-Alchemy"] = 
            {
                ["version"] = 3,
                ["pinTextureSize"] = 32,
                ["treasurePinTexture"] = 1,
                ["showSurveysCompass"] = true,
                ["treasureMarkMapMenuOption"] = 2,
                ["apiVersion"] = 100014,
                ["miniTreasureMap"] = 
                {
                    ["relativePoint"] = 3,
                    ["point"] = 3,
                    ["offsetY"] = 100,
                    ["offsetX"] = 100,
                },
                ["surveysMarkMapMenuOption"] = 2,
                ["surveysPinTexture"] = 1,
                ["showSurveys"] = true,
                ["showMiniTreasureMap"] = true,
                ["showTreasureCompass"] = true,
                ["showTreasure"] = true,
            },
        },
    },
}
