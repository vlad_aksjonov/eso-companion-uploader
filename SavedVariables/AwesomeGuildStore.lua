AwesomeGuildStore_Data =
{
    ["@VladislavAksjonov"] = 
    {
        ["disableCustomSellTabFilter"] = false,
        ["version"] = 17,
        ["keepSortOrderOnClose"] = true,
        ["listingSortField"] = 1,
        ["mailAugmentationShowInvoice"] = false,
        ["oldQualitySelectorBehavior"] = false,
        ["listedNotification"] = false,
        ["listWithSingleClick"] = true,
        ["autoSearch"] = true,
        ["augementMails"] = true,
        ["sortOrder"] = true,
        ["cancelNotification"] = true,
        ["keepFiltersOnClose"] = true,
        ["sortWithoutSearch"] = false,
        ["listingSortOrder"] = true,
        ["searchLibrary"] = 
        {
            ["searches"] = 
            {
                ["3%1#3;4;19,2;26,-_-%100#1;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465164109,
                    ["state"] = "3%1#3;4;19,2;26,-_-%100#1;5",
                    ["searchCount"] = 26,
                    ["favorite"] = true,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#4;1;11,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153705,
                    ["state"] = "3%1#4;1;11,0",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Blacksmithing",
                },
                ["3%1#3;6;25,0%4#5;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1464878660,
                    ["state"] = "3%1#3;6;25,0%4#5;5",
                    ["searchCount"] = 41,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Motif",
                },
                ["3%1#4;6%4#5;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465164116,
                    ["state"] = "3%1#4;6%4#5;5",
                    ["searchCount"] = 42,
                    ["favorite"] = true,
                    ["label"] = "Materials > Provisioning",
                },
                ["3%1#2;3;6,128;7,4;8,0;21,0;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152085,
                    ["state"] = "3%1#2;3;6,128;7,4;8,0;21,0;23,0;24,0;25,0",
                    ["searchCount"] = 2,
                    ["favorite"] = false,
                    ["label"] = "Apparel > Medium Armor",
                },
                ["3%1#3;4;19,2;26,-_-%4#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465068627,
                    ["state"] = "3%1#3;4;19,2;26,-_-%4#2;5",
                    ["searchCount"] = 14,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#3;4;19,2;26,-_-%4#3;3"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465067927,
                    ["state"] = "3%1#3;4;19,2;26,-_-%4#3;3",
                    ["searchCount"] = 3,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#2;4;6,16;7,8;8,0;21,0;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152013,
                    ["state"] = "3%1#2;4;6,16;7,8;8,0;21,0;23,0;24,0;25,0",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Apparel > Light Armor",
                },
                ["3%1#1;3;4,0;1,0;2,0;21,2;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152429,
                    ["state"] = "3%1#1;3;4,0;1,0;2,0;21,2;23,0;24,0;25,0",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Weapon > Two-Handed",
                },
                ["3%1#3;4;19,2;26,-_-%4#3;3%100#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465067992,
                    ["state"] = "3%1#3;4;19,2;26,-_-%4#3;3%100#2;5",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#4;1;11,4"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465164126,
                    ["state"] = "3%1#4;1;11,4",
                    ["searchCount"] = 3,
                    ["favorite"] = false,
                    ["label"] = "Materials > Blacksmithing",
                },
                ["3%1#3;1%4#4;4"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465335485,
                    ["state"] = "3%1#3;1%4#4;4",
                    ["searchCount"] = 10,
                    ["favorite"] = false,
                    ["label"] = "Consumable > All",
                },
                ["3%1#1;2;3,2;1,0;2,0;21,2;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152402,
                    ["state"] = "3%1#1;2;3,2;1,0;2,0;21,2;23,0;24,0;25,0",
                    ["searchCount"] = 3,
                    ["favorite"] = false,
                    ["label"] = "Weapon > One-Handed",
                },
                ["3%1#0%100#3;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465164143,
                    ["state"] = "3%1#0%100#3;5",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "All",
                },
                ["3%1#4;2;12,4%3#1;140;160"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153523,
                    ["state"] = "3%1#4;2;12,4%3#1;140;160",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Clothing",
                },
                ["3%1#4;2;12,4%3#1;100;160"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153528,
                    ["state"] = "3%1#4;2;12,4%3#1;100;160",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Clothing",
                },
                ["3%1#3;7;20,2%4#5;5%100#1;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465143332,
                    ["state"] = "3%1#3;7;20,2%4#5;5%100#1;5",
                    ["searchCount"] = 3,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Motif",
                },
                ["3%1#4;5;15,0;22,0%100#2;5"] = 
                {
                    ["lastSearchTime"] = 1465496960,
                    ["history"] = true,
                    ["state"] = "3%1#4;5;15,0;22,0%100#2;5",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Enchanting",
                },
                ["3%1#3;7;20,2%4#4;4%100#1;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465143346,
                    ["state"] = "3%1#3;7;20,2%4#4;4%100#1;5",
                    ["searchCount"] = 4,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Motif",
                },
                ["3%1#1;2;3,16;1,0;2,0;21,0;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152198,
                    ["state"] = "3%1#1;2;3,16;1,0;2,0;21,0;23,0;24,0;25,0",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Weapon > One-Handed",
                },
                ["3%1#2;3;6,128;7,8;8,0;21,0;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152061,
                    ["state"] = "3%1#2;3;6,128;7,8;8,0;21,0;23,0;24,0;25,0",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Apparel > Medium Armor",
                },
                ["3%1#3;4;19,2;26,2_3%4#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465143317,
                    ["state"] = "3%1#3;4;19,2;26,2_3%4#2;5",
                    ["searchCount"] = 14,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#1;2;3,16;1,0;2,0;21,2;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152206,
                    ["state"] = "3%1#1;2;3,16;1,0;2,0;21,2;23,0;24,0;25,0",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Weapon > One-Handed",
                },
                ["3%1#3;4;19,2;26,-_-%4#3;5%100#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465068003,
                    ["state"] = "3%1#3;4;19,2;26,-_-%4#3;5%100#2;5",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#2;2;6,128;7,4;8,0;21,0;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152155,
                    ["state"] = "3%1#2;2;6,128;7,4;8,0;21,0;23,0;24,0;25,0",
                    ["searchCount"] = 2,
                    ["favorite"] = false,
                    ["label"] = "Apparel > Heavy Armor",
                },
                ["3%1#3;4;19,2;26,-_-%4#2;5%100#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465068560,
                    ["state"] = "3%1#3;4;19,2;26,-_-%4#2;5%100#2;5",
                    ["searchCount"] = 29,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#4;2;12,4"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153601,
                    ["state"] = "3%1#4;2;12,4",
                    ["searchCount"] = 6,
                    ["favorite"] = false,
                    ["label"] = "Materials > Clothing",
                },
                ["3%1#4;5;15,0;22,0%5#Rejora"] = 
                {
                    ["lastSearchTime"] = 1465497067,
                    ["history"] = true,
                    ["state"] = "3%1#4;5;15,0;22,0%5#Rejora",
                    ["searchCount"] = 2,
                    ["favorite"] = false,
                    ["label"] = "Materials > Enchanting > \"Rejora\"",
                },
                ["3%1#3;7;20,2%4#4;4%100#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465496946,
                    ["state"] = "3%1#3;7;20,2%4#4;4%100#2;5",
                    ["searchCount"] = 227,
                    ["favorite"] = true,
                    ["label"] = "Consumable > Motif",
                },
                ["3%1#4;3;13,4"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153702,
                    ["state"] = "3%1#4;3;13,4",
                    ["searchCount"] = 3,
                    ["favorite"] = false,
                    ["label"] = "Materials > Woodworking",
                },
                ["3%1#1;5;5,0;1,0;2,0;21,2;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152473,
                    ["state"] = "3%1#1;5;5,0;1,0;2,0;21,2;23,0;24,0;25,0",
                    ["searchCount"] = 3,
                    ["favorite"] = false,
                    ["label"] = "Weapon > Destruction Staff",
                },
                ["3%1#3;4;19,2;26,-_-%4#3;3%100#1;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465067851,
                    ["state"] = "3%1#3;4;19,2;26,-_-%4#3;3%100#1;5",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#3;7;20,0%4#5;5%100#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1464878687,
                    ["state"] = "3%1#3;7;20,0%4#5;5%100#2;5",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Motif",
                },
                ["3%1#0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465334648,
                    ["state"] = "3%1#0",
                    ["searchCount"] = 5,
                    ["favorite"] = false,
                    ["label"] = "All",
                },
                ["3%1#4;2;12,4%3#1;150;"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153507,
                    ["state"] = "3%1#4;2;12,4%3#1;150;",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Clothing",
                },
                ["3%1#4;2;12,4%3#1;150;160"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153513,
                    ["state"] = "3%1#4;2;12,4%3#1;150;160",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Clothing",
                },
                ["3%1#3;7;20,2%4#5;5%100#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465335024,
                    ["state"] = "3%1#3;7;20,2%4#5;5%100#2;5",
                    ["searchCount"] = 9,
                    ["favorite"] = true,
                    ["label"] = "Consumable > Motif",
                },
                ["3%1#4;4;14,8%5#nirn%100#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1464878480,
                    ["state"] = "3%1#4;4;14,8%5#nirn%100#2;5",
                    ["searchCount"] = 2,
                    ["favorite"] = false,
                    ["label"] = "Materials > Alchemy > \"nirn\"",
                },
                ["3%1#4;5;15,0;22,0%4#4;4%100#2;5"] = 
                {
                    ["lastSearchTime"] = 1465496957,
                    ["history"] = true,
                    ["state"] = "3%1#4;5;15,0;22,0%4#4;4%100#2;5",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Enchanting",
                },
                ["3%1#3;4;19,2;26,3_3%4#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465068691,
                    ["state"] = "3%1#3;4;19,2;26,3_3%4#2;5",
                    ["searchCount"] = 4,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#3;4;19,2;26,-_-%100#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465068576,
                    ["state"] = "3%1#3;4;19,2;26,-_-%100#2;5",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Recipe",
                },
                ["3%1#2;1;6,16;7,8;8,0;21,0;23,0;24,0;25,0%4#4;4%100#2;5"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465151995,
                    ["state"] = "3%1#2;1;6,16;7,8;8,0;21,0;23,0;24,0;25,0%4#4;4%100#2;5",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Apparel > All",
                },
                ["3%1#4;5;15,8;22,0%5#Rejera"] = 
                {
                    ["lastSearchTime"] = 1465497026,
                    ["history"] = true,
                    ["state"] = "3%1#4;5;15,8;22,0%5#Rejera",
                    ["searchCount"] = 3,
                    ["favorite"] = false,
                    ["label"] = "Materials > Enchanting > \"Rejera\"",
                },
                ["3%1#4;5;15,8;22,0"] = 
                {
                    ["lastSearchTime"] = 1465496978,
                    ["history"] = true,
                    ["state"] = "3%1#4;5;15,8;22,0",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Enchanting",
                },
                ["3%1#4;2;12,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153441,
                    ["state"] = "3%1#4;2;12,0",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Clothing",
                },
                ["3%1#2;1;6,16;7,8;8,0;21,0;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465152005,
                    ["state"] = "3%1#2;1;6,16;7,8;8,0;21,0;23,0;24,0;25,0",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Apparel > All",
                },
                ["3%1#3;5;25,0%4#4;4"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465335489,
                    ["state"] = "3%1#3;5;25,0%4#4;4",
                    ["searchCount"] = 3,
                    ["favorite"] = false,
                    ["label"] = "Consumable > Potion",
                },
                ["3%1#4;2;12,4%3#1;145;160"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153517,
                    ["state"] = "3%1#4;2;12,4%3#1;145;160",
                    ["searchCount"] = 1,
                    ["favorite"] = false,
                    ["label"] = "Materials > Clothing",
                },
                ["3%1#4;4;14,8%5#nirn"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1464878655,
                    ["state"] = "3%1#4;4;14,8%5#nirn",
                    ["searchCount"] = 7,
                    ["favorite"] = false,
                    ["label"] = "Materials > Alchemy > \"nirn\"",
                },
                ["3%1#1;6;1,0;2,0;21,2;23,0;24,0;25,0"] = 
                {
                    ["history"] = true,
                    ["lastSearchTime"] = 1465153401,
                    ["state"] = "3%1#1;6;1,0;2,0;21,2;23,0;24,0;25,0",
                    ["searchCount"] = 2,
                    ["favorite"] = false,
                    ["label"] = "Weapon > Restoration Staff",
                },
            },
            ["x"] = 980,
            ["autoClearHistory"] = false,
            ["showTooltips"] = true,
            ["width"] = 730,
            ["lastState"] = "3%1#4;5;15,0;22,0%5#Rejora",
            ["favoritesSortOrder"] = true,
            ["y"] = -5,
            ["locked"] = true,
            ["favoritesSortField"] = "searches",
            ["height"] = 185,
            ["isActive"] = true,
        },
        ["sortField"] = 2,
        ["displayPerUnitPrice"] = true,
        ["purchaseNotification"] = true,
        ["skipGuildKioskDialog"] = true,
        ["lastGuildName"] = "Brave Cat Trade",
        ["showTraderTooltip"] = true,
        ["skipEmptyPages"] = false,
    },
}
