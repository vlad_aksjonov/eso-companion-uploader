HarvensQuestJournal_SavedVariables =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Bswan"] = 
            {
                ["saveDaily"] = true,
                ["sortAscending"] = false,
                ["sort"] = 2,
                ["version"] = 1,
                ["saveRepeatable"] = true,
                ["showCompletedByCategory"] = true,
                ["completed"] = 
                {
                    [4] = 
                    {
                    },
                    [1] = 
                    {
                    },
                    [2] = 
                    {
                        ["Sacrament: Trader's Cove"] = 
                        {
                            [1] = "Sacrament: Trader's Cove",
                            [2] = 50,
                            [3] = "I received a contract from the Speaker.",
                            [4] = 1465231395,
                            [5] = 
                            {
                                [1] = 0,
                                [2] = 0,
                                [3] = "Dark Brotherhood Sanctuary",
                                [4] = 
                                {
                                    [2] = 
                                    {
                                        [2] = "Before I give you the name, I would like to propose a challenge. An opportunity to demonstrate your prowess. Indulge me and I will reward you. \n\nMy challenge is, of course, a secondary concern. As long as the target dies, the Sacrament is fulfilled.",
                                        [1] = "Yes, praise Sithis. The Black Sacrament has been performed and we have a new target. The Night Mother awaits her due.",
                                    },
                                    [1] = 
                                    {
                                        [2] = "I'm listening.",
                                        [1] = "Who do I need to kill?",
                                    },
                                },
                                [5] = "Speaker Terenus",
                                [6] = "Dark Brotherhood Sanctuary",
                            },
                            [7] = 3,
                            [8] = "Trader's Cove",
                            [9] = 
                            {
                                [1] = 
                                {
                                    [1] = "The Black Sacrament has been performed for Geon Alinie at the Trader's Cove.",
                                },
                                [2] = 
                                {
                                    [2] = 1465231417,
                                    [1] = "I have also been given several optional objectives which will enhance my rewards depending on how many I complete.",
                                },
                                [3] = 
                                {
                                    [2] = 1465232141,
                                    [1] = "The Speaker is waiting for my report at the Sanctuary.",
                                },
                            },
                            [10] = 
                            {
                                [1] = 0,
                                [2] = 0,
                                [3] = "Dark Brotherhood Sanctuary",
                                [5] = "Speaker Terenus",
                                [6] = "Dark Brotherhood Sanctuary",
                                [7] = "Exemplary work. You truly have a gift for death.\n\nReturn tomorrow. I'm certain we can find more ways to put your skills to the test.",
                            },
                            [11] = 
                            {
                                [1] = 
                                {
                                    [1] = "|H0:item:77574:48:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                },
                                [2] = 664,
                                [3] = 8233,
                            },
                            [12] = 1465232160,
                        },
                        ["Heist: Underground Sepulcher"] = 
                        {
                            [1] = "Heist: Underground Sepulcher",
                            [2] = 50,
                            [3] = "I was tipped off about treasures stashed in Underground Sepulcher. I don't have much time to secure the goods before they're moved, so I should act quickly on this lead.",
                            [4] = 1465230448,
                            [5] = 
                            {
                                [1] = 0,
                                [2] = 0,
                                [3] = "Thieves Den",
                                [4] = 
                                {
                                    [2] = 
                                    {
                                        [1] = "\"Word has it that there's a growing stockpile of valuables accumulating in the Underground Sepulcher. Clean 'em out, but get it done before anyone catches wise or we'll take a hit from the buyers.\"",
                                    },
                                    [1] = 
                                    {
                                        [1] = "<Take the listing.>",
                                    },
                                },
                                [5] = "Heist Board",
                                [6] = "Thieves Den",
                            },
                            [7] = 3,
                            [8] = "Underground Sepulcher",
                            [9] = 
                            {
                                [1] = 
                                {
                                    [1] = "I need to get to the site without drawing attention to myself. I should speak with Fa'ren-dar.",
                                },
                                [2] = 
                                {
                                    [2] = 1465230473,
                                    [1] = "The valuables are secured in the hideout beyond this entry. They are sure to be guarded. I need to maintain a low profile.",
                                },
                                [3] = 
                                {
                                    [2] = 1465230494,
                                    [1] = "The treasures I'm looking for should be in a secure chest somewhere within this hideout. I need to break in once they're located.",
                                },
                                [4] = 
                                {
                                    [2] = 1465230799,
                                    [1] = "I have the treasure! I just need to get it to Fa'ren-dar before he meets the buyers.",
                                },
                                [5] = 
                                {
                                    [2] = 1465230899,
                                    [1] = "Fa'ren-dar is waiting to lead me back to the Thieves Den from here. We should get going while the going is good.",
                                },
                                [6] = 
                                {
                                    [2] = 1465230914,
                                    [1] = "I turned over the goods to Fa'ren-dar. He'll be waiting with my share at the Thieves Den .",
                                },
                            },
                            [10] = 
                            {
                                [1] = 0,
                                [2] = 0,
                                [3] = "Thieves Den",
                                [5] = "Fa'ren-dar",
                                [6] = "Thieves Den",
                                [7] = "Our buyers were quite pleased with their \"inheritance\" and our fee reflects all your added care. I think you'll be quite pleased.\n\nYou've spent enough time in tombs today. Go out and live a little—I'll have something else lined up by tomorrow.",
                            },
                            [11] = 
                            {
                                [1] = 
                                {
                                    [2] = "|H0:item:74651:48:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                    [1] = "|H0:item:78002:48:50:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h|h",
                                },
                                [2] = 332,
                                [3] = 8233,
                            },
                            [12] = 1465230952,
                        },
                        ["Sacrament"] = 
                        {
                        },
                    },
                    [3] = 
                    {
                    },
                },
                ["quests"] = 
                {
                    [4] = 
                    {
                        [""] = 
                        {
                        },
                    },
                    [1] = 
                    {
                    },
                    [2] = 
                    {
                        ["Dark Revelations"] = 
                        {
                            [1] = "Dark Revelations",
                            [2] = 50,
                            [3] = "Count Carolus still owes us what he promised for taking on his contract—information concerning the Black Dragon. Astara wants to find out what the Count knows.",
                            [4] = 1465232746,
                            [5] = 
                            {
                                [1] = 0,
                                [2] = 0,
                                [3] = "Dark Brotherhood Sanctuary",
                                [4] = 
                                {
                                    [2] = 
                                    {
                                        [2] = "We're not sure, but that's where he requested to meet. He has a camp there which he's using for his own clandestine purposes.\n\nBefore you go, talk to Mirabelle. She has information concerning the Count that could prove useful to your mission.",
                                        [1] = "Mirabelle's outburst doesn't concern you, Assassin. Focus on the task at hand.\n\nCount Carolus still owes us for the Sacrament. He claimed to have information about the Black Dragon. Meet him at the ruins of Dasek Moor and find out what he knows.",
                                    },
                                    [1] = 
                                    {
                                        [2] = "I'll talk to Mirabelle before I head out.",
                                        [1] = "Why does he want to meet at the ruins?",
                                    },
                                },
                                [5] = "Astara Caerellius",
                                [6] = "Dark Brotherhood Sanctuary",
                            },
                            [7] = 3,
                            [8] = "The Gold Coast",
                            [9] = 
                            {
                                [1] = 
                                {
                                    [1] = "Before I head out to meet with Count Carolus, Astara asked me to talk to Mirabelle. She has information that might be of use to me when I meet with the Count.",
                                },
                            },
                            [13] = 0,
                        },
                        ["Crime Spree"] = 
                        {
                            [8] = "Hew's Bane",
                            [1] = "Crime Spree",
                            [2] = 50,
                            [3] = "Rumors are floating around that the Thieves Guild isn't a threat anymore. The Thieves Guild wants to quash the rumors with a series of conspicuous thefts.",
                            [4] = 1465231091,
                            [13] = 1,
                            [9] = 
                            {
                                [1] = 
                                {
                                    [1] = "I need to break into a safebox somewhere in Grahtwood to start the crime spree.",
                                },
                            },
                            [7] = 3,
                        },
                        ["Sacrament"] = 
                        {
                            [12] = 1465233358,
                        },
                    },
                    [3] = 
                    {
                        ["Wrothgar"] = 
                        {
                            ["Maelstrom Arena"] = 
                            {
                                [8] = "Wrothgar",
                                [1] = "Maelstrom Arena",
                                [2] = 50,
                                [3] = "I have been welcomed into the bizarre realm of the Daedric Demiprince Fa-Nuit-Hen. In order to test my mettle and demonstrate my skills, I've agreed to enter the Maelstrom Arena, where deadly challenges created by Fa-Nuit-Hen's Barons await me.",
                                [4] = 1465229144,
                                [13] = 1,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "I need to survive this challenge, The Drome of Toxic Shock.",
                                    },
                                },
                                [7] = 5,
                            },
                        },
                        ["Greenshade"] = 
                        {
                            ["The Unfilled Order"] = 
                            {
                                [8] = "Greenshade",
                                [1] = "The Unfilled Order",
                                [2] = 32,
                                [3] = "I found a dead adventurer in Barrow Trench. They entered the mine to collect rare ingredients for an alchemist in Woodhearth.",
                                [4] = 1465229144,
                                [13] = 0,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "Vadelen Indothan is somewhere in Woodhearth. I should check the tavern first.",
                                    },
                                },
                                [7] = 0,
                            },
                            ["To Velyn Harbor"] = 
                            {
                                [8] = "Greenshade",
                                [1] = "To Velyn Harbor",
                                [2] = 31,
                                [3] = "With Prince Naemon dead—again—and the new Silvenar revealed, Queen Ayrenn asked me to head to Velyn Harbor in Malabal Tor. The Aldmeri Dominion needs to prepare for the wedding between the Green Lady and the newly-discovered Silvenar.",
                                [4] = 1465229144,
                                [13] = 0,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "Queen Ayrenn described Velyn Harbor as a \"quiet little town,\" but nothing could be further from the truth. I should find someone in charge and discover what exactly is happening.",
                                    },
                                },
                                [7] = 0,
                            },
                        },
                        ["Coldharbour"] = 
                        {
                            ["The Army of Meridia"] = 
                            {
                                [8] = "Coldharbour",
                                [1] = "The Army of Meridia",
                                [2] = 48,
                                [3] = "I need to rebuild my army if we're going to have any chance of confronting Molag Bal and stopping the Planemeld. The mysterious Groundskeeper suggests I rescue as many people as I can and send them to the Hollow City.",
                                [4] = 1465229144,
                                [13] = 0,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "King Laloriaran Dynar is imprisoned to the west, beyond the Moonless Walk. Vanus Galerion is trapped somewhere to the east. I should also watch for any of the other missing Fighters Guild and Mages Guild members, or anyone else who can help me.",
                                    },
                                },
                                [7] = 0,
                            },
                        },
                        ["Cyrodiil"] = 
                        {
                            ["Seeds of Hope"] = 
                            {
                                [8] = "Cyrodiil",
                                [1] = "Seeds of Hope",
                                [2] = 50,
                                [3] = "The Bloody Hand Goblins have stolen Cropsford's seeds, needed for next season's crops.",
                                [4] = 1465229144,
                                [13] = 2,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "I recovered the seeds. I should bring them to Prefect Antias in Cropsford.",
                                    },
                                },
                                [7] = 0,
                            },
                            ["The Elder Scroll of Mnem"] = 
                            {
                                [8] = "Cyrodiil",
                                [1] = "The Elder Scroll of Mnem",
                                [2] = 50,
                                [3] = "The Elder Scroll of Mnem has been taken by enemy soldiers.",
                                [4] = 1465229144,
                                [13] = 1,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "I promised Grand Warlord Sorcalin I would see it returned to the Temple of Mnem.",
                                    },
                                },
                                [7] = 7,
                            },
                            ["Scout Alessia Mine"] = 
                            {
                                [8] = "Cyrodiil",
                                [1] = "Scout Alessia Mine",
                                [2] = 50,
                                [3] = "I accepted a mission to scout Alessia Mine for the Dominion.",
                                [4] = 1465229144,
                                [13] = 1,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "I should travel there as soon as I can.",
                                    },
                                },
                                [7] = 7,
                            },
                            ["Capture Castle Roebeck"] = 
                            {
                                [8] = "Cyrodiil",
                                [1] = "Capture Castle Roebeck",
                                [2] = 50,
                                [3] = "I accepted a mission to help re-capture Castle Roebeck for the Dominion.",
                                [4] = 1465229144,
                                [13] = 1,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "I should join with other warriors to launch an attack.",
                                    },
                                },
                                [7] = 7,
                            },
                            ["Kill Enemy Nightblades"] = 
                            {
                                [8] = "Cyrodiil",
                                [1] = "Kill Enemy Nightblades",
                                [2] = 50,
                                [3] = "Grand Warlord Sorcalin has authorized a reward for any Dominion soldier who slays 20 enemy Nightblades.",
                                [4] = 1465229144,
                                [13] = 2,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "I should seek out and kill enemy Nightblades.",
                                    },
                                },
                                [7] = 7,
                            },
                            ["Capture Blue Road Lumbermill"] = 
                            {
                                [8] = "Cyrodiil",
                                [1] = "Capture Blue Road Lumbermill",
                                [2] = 50,
                                [3] = "I accepted a mission to help capture Blue Road Lumbermill for the Dominion.",
                                [4] = 1465229144,
                                [13] = 1,
                                [9] = 
                                {
                                    [1] = 
                                    {
                                        [1] = "This is a dangerous task. I should go there with a group.",
                                    },
                                },
                                [7] = 7,
                            },
                        },
                    },
                },
                ["zonesFixed"] = true,
                ["linksConverted"] = true,
                ["layout"] = 
                {
                    ["titleFont"] = "ZoFontBookPaperTitle",
                    ["questA"] = 0.8000000000,
                    ["npcB"] = 0.0600000000,
                    ["bodyFont"] = "ZoFontBookPaper",
                    ["questB"] = 0.0500000000,
                    ["npcA"] = 0.8000000000,
                    ["fontG"] = 0.0980392247,
                    ["npcG"] = 0.3700000000,
                    ["skin"] = "Paper",
                    ["questR"] = 0.3400000000,
                    ["questG"] = 0.0500000000,
                    ["fontB"] = 0.0705882385,
                    ["fontA"] = 0.8000000000,
                    ["npcR"] = 0.0500000000,
                    ["fontR"] = 0.1686274558,
                    ["openAtTracked"] = false,
                },
                ["openAtTracked"] = false,
            },
            ["Testimate"] = 
            {
                ["saveDaily"] = true,
                ["sortAscending"] = true,
                ["sort"] = 0,
                ["version"] = 1,
                ["linksConverted"] = true,
                ["completed"] = 
                {
                    [4] = 
                    {
                    },
                    [1] = 
                    {
                    },
                    [2] = 
                    {
                    },
                    [3] = 
                    {
                    },
                },
                ["quests"] = 
                {
                    [4] = 
                    {
                    },
                    [1] = 
                    {
                    },
                    [2] = 
                    {
                    },
                    [3] = 
                    {
                    },
                },
                ["zonesFixed"] = true,
                ["layout"] = 
                {
                    ["titleFont"] = "ZoFontBookPaperTitle",
                    ["openAtTracked"] = false,
                    ["fontR"] = 0.1686274558,
                    ["bodyFont"] = "ZoFontBookPaper",
                    ["questA"] = 0.8000000000,
                    ["npcA"] = 0.8000000000,
                    ["fontG"] = 0.0980392247,
                    ["npcG"] = 0.3700000000,
                    ["fontB"] = 0.0705882385,
                    ["questR"] = 0.3400000000,
                    ["questG"] = 0.0500000000,
                    ["questB"] = 0.0500000000,
                    ["fontA"] = 0.8000000000,
                    ["npcR"] = 0.0500000000,
                    ["npcB"] = 0.0600000000,
                    ["skin"] = "Paper",
                },
                ["showCompletedByCategory"] = false,
                ["saveRepeatable"] = true,
            },
        },
    },
}
