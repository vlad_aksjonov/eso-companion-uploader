ShopkeeperSavedVars =
{
    ["Default"] = 
    {
        ["@VladislavAksjonov"] = 
        {
            ["Bswan"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["displaySalesDetails"] = false,
                    ["autoNext"] = false,
                    ["winTop"] = 105.7661132813,
                    ["ctrlDays"] = "Focus 2",
                    ["statsWinTop"] = 780.6016235352,
                    ["showMultiple"] = true,
                    ["focus2"] = 3,
                    ["defaultDays"] = "All",
                    ["blacklist"] = "Team Gold,  ",
                    ["ctrlShiftDays"] = "None",
                    ["showChatAlerts"] = false,
                    ["replaceInventoryValues"] = false,
                    ["feedbackWinLeft"] = 720,
                    ["openWithStore"] = true,
                    ["showCyroAlerts"] = true,
                    ["showUnitPrice"] = false,
                    ["scanFreq"] = 120,
                    ["version"] = 1,
                    ["guildWinLeft"] = 50.2052307129,
                    ["rankIndex"] = 8,
                    ["alertSoundName"] = "Book_Acquired",
                    ["pricingData"] = 
                    {
                        [45952] = 
                        {
                            ["1:0:2:0:0"] = 66,
                        },
                        [33602] = 
                        {
                            ["50:1:2:0:0"] = 10.1010101010,
                        },
                        [23107] = 
                        {
                            ["1:0:1:0:0"] = 7.5000000000,
                        },
                        [72900] = 
                        {
                            ["50:1:3:18:0"] = 300,
                        },
                        [4487] = 
                        {
                            ["1:0:1:0:0"] = 7.5000000000,
                        },
                        [43088] = 
                        {
                            ["50:1:2:0:0"] = 10.1010101010,
                        },
                        [28433] = 
                        {
                            ["40:0:2:0:0"] = 6.0240963855,
                        },
                        [23122] = 
                        {
                            ["1:0:1:0:0"] = 8.5000000000,
                        },
                        [533] = 
                        {
                            ["1:0:1:0:0"] = 7,
                        },
                        [43094] = 
                        {
                            ["50:1:2:0:0"] = 10,
                        },
                        [794] = 
                        {
                            ["1:0:1:0:0"] = 4.5000000000,
                        },
                        [28444] = 
                        {
                            ["50:1:2:0:0"] = 10,
                        },
                        [64221] = 
                        {
                            ["1:0:5:0:0"] = 3000,
                        },
                        [45854] = 
                        {
                            ["21:0:5:0:0"] = 8000,
                        },
                        [56863] = 
                        {
                            ["1:0:1:26:0"] = 23000,
                        },
                        [32160] = 
                        {
                            ["50:1:2:0:0"] = 10.1010101010,
                        },
                        [28513] = 
                        {
                            ["40:0:2:0:0"] = 10.9890109890,
                        },
                        [803] = 
                        {
                            ["1:0:1:0:0"] = 5,
                        },
                        [72932] = 
                        {
                            ["50:1:3:13:0"] = 300,
                        },
                        [5413] = 
                        {
                            ["1:0:1:0:0"] = 6,
                        },
                        [71526] = 
                        {
                            ["1:0:4:0:0"] = 8500,
                        },
                        [68199] = 
                        {
                            ["1:0:3:0:0"] = 480,
                        },
                        [23126] = 
                        {
                            ["1:0:1:0:0"] = 8.5000000000,
                        },
                        [33897] = 
                        {
                            ["40:0:2:0:0"] = 7.0422535211,
                        },
                        [33194] = 
                        {
                            ["1:0:1:0:0"] = 1,
                        },
                        [54177] = 
                        {
                            ["1:0:5:0:0"] = 5500,
                        },
                        [71532] = 
                        {
                            ["1:0:4:0:0"] = 5800,
                        },
                        [54509] = 
                        {
                            ["32:0:4:22:0"] = 150,
                        },
                        [76910] = 
                        {
                            ["1:0:1:0:0"] = 3400,
                        },
                        [33903] = 
                        {
                            ["40:0:2:0:0"] = 7.1428571429,
                        },
                        [6000] = 
                        {
                            ["1:0:1:0:0"] = 9,
                        },
                        [54513] = 
                        {
                            ["45:0:4:22:0"] = 150,
                        },
                        [28402] = 
                        {
                            ["50:1:2:0:0"] = 10.1010101010,
                        },
                        [54181] = 
                        {
                            ["1:0:5:0:0"] = 2000,
                        },
                        [33909] = 
                        {
                            ["40:0:2:0:0"] = 7.1428571429,
                        },
                        [46133] = 
                        {
                            ["1:0:1:0:0"] = 5,
                        },
                        [71536] = 
                        {
                            ["1:0:4:0:0"] = 9100,
                        },
                        [6001] = 
                        {
                            ["1:0:1:0:0"] = 11.5000000000,
                        },
                        [74729] = 
                        {
                            ["12:0:1:0:0"] = 100,
                            ["34:0:1:0:0"] = 200,
                            ["7:0:1:0:0"] = 100,
                        },
                        [28473] = 
                        {
                            ["40:0:2:0:0"] = 7.1428571429,
                        },
                        [54173] = 
                        {
                            ["1:0:5:0:0"] = 8300,
                        },
                        [23099] = 
                        {
                            ["1:0:1:0:0"] = 7,
                        },
                        [71527] = 
                        {
                            ["1:0:4:0:0"] = 16000,
                        },
                        [16426] = 
                        {
                            ["1:0:3:0:0"] = 170,
                        },
                        [71525] = 
                        {
                            ["1:0:4:0:0"] = 8500,
                        },
                        [30357] = 
                        {
                            ["1:0:1:0:0"] = 5,
                        },
                    },
                    ["rankIndexRoster"] = 1,
                    ["showGraph"] = true,
                    ["showPricing"] = true,
                    ["historyDepth"] = 30,
                    ["showCalc"] = true,
                    ["diplayGuildInfo"] = false,
                    ["windowFont"] = "ProseAntique",
                    ["showFullPrice"] = true,
                    ["winLeft"] = 50.2052307129,
                    ["showAnnounceAlerts"] = true,
                    ["viewBuyerSeller"] = "buyer",
                    ["viewSize"] = "full",
                    ["openWithMail"] = true,
                    ["focus1"] = 10,
                    ["offlineSales"] = true,
                    ["trimDecimals"] = false,
                    ["shiftDays"] = "Focus 1",
                    ["displayItemAnalysisButtons"] = false,
                    ["noSalesInfoDeal"] = 2,
                    ["viewGuildBuyerSeller"] = "seller",
                    ["displayListingMessage"] = false,
                    ["guildWinTop"] = 105.7661132813,
                    ["feedbackWinTop"] = 420,
                    ["statsWinLeft"] = 439.1606445313,
                    ["saucy"] = false,
                    ["trimOutliers"] = false,
                },
            },
            ["Purr-Purr'Dar"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["displaySalesDetails"] = false,
                    ["autoNext"] = false,
                    ["winTop"] = 168.3991851807,
                    ["ctrlDays"] = "Focus 2",
                    ["statsWinTop"] = 820,
                    ["showMultiple"] = true,
                    ["focus2"] = 3,
                    ["defaultDays"] = "All",
                    ["blacklist"] = "",
                    ["ctrlShiftDays"] = "None",
                    ["showChatAlerts"] = false,
                    ["replaceInventoryValues"] = false,
                    ["feedbackWinLeft"] = 720,
                    ["openWithStore"] = true,
                    ["showCyroAlerts"] = true,
                    ["showUnitPrice"] = false,
                    ["scanFreq"] = 120,
                    ["version"] = 1,
                    ["guildWinLeft"] = 54.2454223633,
                    ["rankIndex"] = 7,
                    ["viewBuyerSeller"] = "buyer",
                    ["pricingData"] = 
                    {
                        [71692] = 
                        {
                            ["1:0:4:0:0"] = 9000,
                        },
                        [64221] = 
                        {
                            ["1:0:5:0:0"] = 3000,
                        },
                        [16426] = 
                        {
                            ["1:0:3:0:0"] = 123,
                        },
                    },
                    ["rankIndexRoster"] = 1,
                    ["showGraph"] = true,
                    ["displayItemAnalysisButtons"] = false,
                    ["historyDepth"] = 30,
                    ["showPricing"] = true,
                    ["diplayGuildInfo"] = false,
                    ["showCalc"] = true,
                    ["windowFont"] = "ProseAntique",
                    ["winLeft"] = 54.2454223633,
                    ["showFullPrice"] = true,
                    ["showAnnounceAlerts"] = true,
                    ["viewSize"] = "full",
                    ["alertSoundName"] = "Book_Acquired",
                    ["focus1"] = 10,
                    ["offlineSales"] = true,
                    ["noSalesInfoDeal"] = 2,
                    ["shiftDays"] = "Focus 1",
                    ["trimDecimals"] = false,
                    ["openWithMail"] = true,
                    ["viewGuildBuyerSeller"] = "seller",
                    ["displayListingMessage"] = false,
                    ["guildWinTop"] = 168.3991851807,
                    ["feedbackWinTop"] = 420,
                    ["statsWinLeft"] = 720,
                    ["saucy"] = false,
                    ["trimOutliers"] = false,
                },
            },
            ["Testimate"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["displaySalesDetails"] = false,
                    ["autoNext"] = false,
                    ["winTop"] = 92.6332168579,
                    ["ctrlDays"] = "Focus 2",
                    ["statsWinTop"] = 820,
                    ["showMultiple"] = true,
                    ["focus2"] = 3,
                    ["defaultDays"] = "All",
                    ["blacklist"] = "",
                    ["ctrlShiftDays"] = "None",
                    ["showChatAlerts"] = false,
                    ["replaceInventoryValues"] = false,
                    ["feedbackWinLeft"] = 720,
                    ["openWithStore"] = true,
                    ["showCyroAlerts"] = true,
                    ["showUnitPrice"] = false,
                    ["scanFreq"] = 120,
                    ["version"] = 1,
                    ["guildWinLeft"] = 33.0306396484,
                    ["rankIndex"] = 1,
                    ["viewBuyerSeller"] = "buyer",
                    ["alertSoundName"] = "Book_Acquired",
                    ["showGraph"] = true,
                    ["rankIndexRoster"] = 1,
                    ["historyDepth"] = 30,
                    ["showPricing"] = true,
                    ["diplayGuildInfo"] = false,
                    ["showCalc"] = true,
                    ["windowFont"] = "ProseAntique",
                    ["winLeft"] = 33.0306396484,
                    ["showFullPrice"] = true,
                    ["showAnnounceAlerts"] = true,
                    ["viewSize"] = "full",
                    ["focus1"] = 10,
                    ["openWithMail"] = true,
                    ["offlineSales"] = true,
                    ["trimDecimals"] = false,
                    ["shiftDays"] = "Focus 1",
                    ["displayItemAnalysisButtons"] = false,
                    ["noSalesInfoDeal"] = 2,
                    ["displayListingMessage"] = false,
                    ["viewGuildBuyerSeller"] = "seller",
                    ["guildWinTop"] = 92.6332168579,
                    ["feedbackWinTop"] = 420,
                    ["statsWinLeft"] = 720,
                    ["saucy"] = false,
                    ["trimOutliers"] = false,
                },
            },
            ["Lb-Alchemy"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["displaySalesDetails"] = false,
                    ["autoNext"] = false,
                    ["winTop"] = 30,
                    ["ctrlDays"] = "Focus 2",
                    ["statsWinTop"] = 820,
                    ["showMultiple"] = true,
                    ["focus2"] = 3,
                    ["defaultDays"] = "All",
                    ["blacklist"] = "",
                    ["ctrlShiftDays"] = "None",
                    ["showChatAlerts"] = false,
                    ["replaceInventoryValues"] = false,
                    ["feedbackWinLeft"] = 720,
                    ["openWithStore"] = true,
                    ["showCyroAlerts"] = true,
                    ["showUnitPrice"] = false,
                    ["scanFreq"] = 120,
                    ["version"] = 1,
                    ["guildWinLeft"] = 30,
                    ["rankIndex"] = 1,
                    ["viewBuyerSeller"] = "buyer",
                    ["alertSoundName"] = "Book_Acquired",
                    ["showGraph"] = true,
                    ["rankIndexRoster"] = 1,
                    ["historyDepth"] = 30,
                    ["showPricing"] = true,
                    ["diplayGuildInfo"] = false,
                    ["showCalc"] = true,
                    ["windowFont"] = "ProseAntique",
                    ["winLeft"] = 30,
                    ["showFullPrice"] = true,
                    ["showAnnounceAlerts"] = true,
                    ["viewSize"] = "full",
                    ["focus1"] = 10,
                    ["openWithMail"] = true,
                    ["offlineSales"] = true,
                    ["trimDecimals"] = false,
                    ["shiftDays"] = "Focus 1",
                    ["displayItemAnalysisButtons"] = false,
                    ["noSalesInfoDeal"] = 2,
                    ["displayListingMessage"] = false,
                    ["viewGuildBuyerSeller"] = "seller",
                    ["guildWinTop"] = 30,
                    ["feedbackWinTop"] = 420,
                    ["statsWinLeft"] = 720,
                    ["saucy"] = false,
                    ["trimOutliers"] = false,
                },
            },
            ["$AccountWide"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["displaySalesDetails"] = false,
                    ["autoNext"] = false,
                    ["winTop"] = 30,
                    ["ctrlDays"] = "Focus 2",
                    ["statsWinTop"] = 820,
                    ["showMultiple"] = true,
                    ["focus2"] = 3,
                    ["defaultDays"] = "All",
                    ["blacklist"] = "",
                    ["ctrlShiftDays"] = "None",
                    ["showChatAlerts"] = false,
                    ["replaceInventoryValues"] = false,
                    ["allSettingsAccount"] = false,
                    ["feedbackWinLeft"] = 720,
                    ["openWithStore"] = true,
                    ["showCyroAlerts"] = true,
                    ["SSIndex"] = 
                    {
                    },
                    ["scanFreq"] = 120,
                    ["version"] = 1,
                    ["guildWinLeft"] = 30,
                    ["rankIndex"] = 1,
                    ["guildWinTop"] = 30,
                    ["viewBuyerSeller"] = "buyer",
                    ["rankIndexRoster"] = 1,
                    ["showGraph"] = true,
                    ["showPricing"] = true,
                    ["historyDepth"] = 30,
                    ["showCalc"] = true,
                    ["diplayGuildInfo"] = false,
                    ["windowFont"] = "ProseAntique",
                    ["showFullPrice"] = true,
                    ["winLeft"] = 30,
                    ["showAnnounceAlerts"] = true,
                    ["alertSoundName"] = "Book_Acquired",
                    ["viewSize"] = "full",
                    ["openWithMail"] = true,
                    ["trimDecimals"] = false,
                    ["offlineSales"] = true,
                    ["displayItemAnalysisButtons"] = false,
                    ["shiftDays"] = "Focus 1",
                    ["noSalesInfoDeal"] = 2,
                    ["viewGuildBuyerSeller"] = "seller",
                    ["displayListingMessage"] = false,
                    ["feedbackWinTop"] = 420,
                    ["focus1"] = 10,
                    ["trimOutliers"] = false,
                    ["statsWinLeft"] = 720,
                    ["saucy"] = false,
                    ["showUnitPrice"] = false,
                },
            },
            ["Razum-Dar in Excile"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["displaySalesDetails"] = false,
                    ["autoNext"] = false,
                    ["winTop"] = 250.2265014648,
                    ["ctrlDays"] = "Focus 2",
                    ["statsWinTop"] = 820,
                    ["showMultiple"] = true,
                    ["focus2"] = 3,
                    ["defaultDays"] = "All",
                    ["blacklist"] = "",
                    ["ctrlShiftDays"] = "None",
                    ["showChatAlerts"] = false,
                    ["replaceInventoryValues"] = false,
                    ["feedbackWinLeft"] = 720,
                    ["openWithStore"] = true,
                    ["showCyroAlerts"] = true,
                    ["showUnitPrice"] = false,
                    ["scanFreq"] = 120,
                    ["version"] = 1,
                    ["guildWinLeft"] = 47.1736450195,
                    ["rankIndex"] = 1,
                    ["viewBuyerSeller"] = "buyer",
                    ["alertSoundName"] = "Book_Acquired",
                    ["showGraph"] = true,
                    ["rankIndexRoster"] = 1,
                    ["historyDepth"] = 30,
                    ["showPricing"] = true,
                    ["diplayGuildInfo"] = false,
                    ["showCalc"] = true,
                    ["windowFont"] = "ProseAntique",
                    ["winLeft"] = 47.1736450195,
                    ["showFullPrice"] = true,
                    ["showAnnounceAlerts"] = true,
                    ["viewSize"] = "full",
                    ["focus1"] = 10,
                    ["openWithMail"] = true,
                    ["offlineSales"] = true,
                    ["trimDecimals"] = false,
                    ["shiftDays"] = "Focus 1",
                    ["displayItemAnalysisButtons"] = false,
                    ["noSalesInfoDeal"] = 2,
                    ["displayListingMessage"] = false,
                    ["viewGuildBuyerSeller"] = "seller",
                    ["guildWinTop"] = 250.2265014648,
                    ["feedbackWinTop"] = 420,
                    ["statsWinLeft"] = 720,
                    ["saucy"] = false,
                    ["trimOutliers"] = false,
                },
            },
            ["Lb-Enchanting"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["displaySalesDetails"] = false,
                    ["autoNext"] = false,
                    ["winTop"] = 30,
                    ["ctrlDays"] = "Focus 2",
                    ["statsWinTop"] = 820,
                    ["showMultiple"] = true,
                    ["focus2"] = 3,
                    ["defaultDays"] = "All",
                    ["blacklist"] = "",
                    ["ctrlShiftDays"] = "None",
                    ["showChatAlerts"] = false,
                    ["replaceInventoryValues"] = false,
                    ["feedbackWinLeft"] = 720,
                    ["openWithStore"] = true,
                    ["showCyroAlerts"] = true,
                    ["showUnitPrice"] = false,
                    ["scanFreq"] = 120,
                    ["version"] = 1,
                    ["guildWinLeft"] = 30,
                    ["rankIndex"] = 1,
                    ["viewBuyerSeller"] = "buyer",
                    ["alertSoundName"] = "Book_Acquired",
                    ["showGraph"] = true,
                    ["rankIndexRoster"] = 1,
                    ["historyDepth"] = 30,
                    ["showPricing"] = true,
                    ["diplayGuildInfo"] = false,
                    ["showCalc"] = true,
                    ["windowFont"] = "ProseAntique",
                    ["winLeft"] = 30,
                    ["showFullPrice"] = true,
                    ["showAnnounceAlerts"] = true,
                    ["viewSize"] = "full",
                    ["focus1"] = 10,
                    ["openWithMail"] = true,
                    ["offlineSales"] = true,
                    ["trimDecimals"] = false,
                    ["shiftDays"] = "Focus 1",
                    ["displayItemAnalysisButtons"] = false,
                    ["noSalesInfoDeal"] = 2,
                    ["displayListingMessage"] = false,
                    ["viewGuildBuyerSeller"] = "seller",
                    ["guildWinTop"] = 30,
                    ["feedbackWinTop"] = 420,
                    ["statsWinLeft"] = 720,
                    ["saucy"] = false,
                    ["trimOutliers"] = false,
                },
            },
            ["Purr-Purr'dar"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["displaySalesDetails"] = false,
                    ["autoNext"] = false,
                    ["winTop"] = 30,
                    ["ctrlDays"] = "Focus 2",
                    ["statsWinTop"] = 820,
                    ["showMultiple"] = true,
                    ["focus2"] = 3,
                    ["defaultDays"] = "All",
                    ["blacklist"] = "",
                    ["ctrlShiftDays"] = "None",
                    ["showChatAlerts"] = false,
                    ["replaceInventoryValues"] = false,
                    ["feedbackWinLeft"] = 720,
                    ["openWithStore"] = true,
                    ["showCyroAlerts"] = true,
                    ["showUnitPrice"] = false,
                    ["scanFreq"] = 120,
                    ["version"] = 1,
                    ["guildWinLeft"] = 30,
                    ["rankIndex"] = 1,
                    ["viewBuyerSeller"] = "buyer",
                    ["alertSoundName"] = "Book_Acquired",
                    ["showGraph"] = true,
                    ["rankIndexRoster"] = 1,
                    ["historyDepth"] = 30,
                    ["showPricing"] = true,
                    ["diplayGuildInfo"] = false,
                    ["showCalc"] = true,
                    ["windowFont"] = "ProseAntique",
                    ["winLeft"] = 30,
                    ["showFullPrice"] = true,
                    ["showAnnounceAlerts"] = true,
                    ["viewSize"] = "full",
                    ["focus1"] = 10,
                    ["openWithMail"] = true,
                    ["offlineSales"] = true,
                    ["trimDecimals"] = false,
                    ["shiftDays"] = "Focus 1",
                    ["displayItemAnalysisButtons"] = false,
                    ["noSalesInfoDeal"] = 2,
                    ["displayListingMessage"] = false,
                    ["viewGuildBuyerSeller"] = "seller",
                    ["guildWinTop"] = 30,
                    ["feedbackWinTop"] = 420,
                    ["statsWinLeft"] = 720,
                    ["saucy"] = false,
                    ["trimOutliers"] = false,
                },
            },
            ["Lb-Grocery"] = 
            {
                ["@VladislavAksjonov"] = 
                {
                    ["displaySalesDetails"] = false,
                    ["autoNext"] = false,
                    ["winTop"] = 30,
                    ["ctrlDays"] = "Focus 2",
                    ["statsWinTop"] = 820,
                    ["showMultiple"] = true,
                    ["focus2"] = 3,
                    ["defaultDays"] = "All",
                    ["blacklist"] = "",
                    ["ctrlShiftDays"] = "None",
                    ["showChatAlerts"] = false,
                    ["replaceInventoryValues"] = false,
                    ["feedbackWinLeft"] = 720,
                    ["openWithStore"] = true,
                    ["showCyroAlerts"] = true,
                    ["showUnitPrice"] = false,
                    ["scanFreq"] = 120,
                    ["version"] = 1,
                    ["guildWinLeft"] = 30,
                    ["rankIndex"] = 1,
                    ["viewBuyerSeller"] = "buyer",
                    ["alertSoundName"] = "Book_Acquired",
                    ["showGraph"] = true,
                    ["rankIndexRoster"] = 1,
                    ["historyDepth"] = 30,
                    ["showPricing"] = true,
                    ["diplayGuildInfo"] = false,
                    ["showCalc"] = true,
                    ["windowFont"] = "ProseAntique",
                    ["winLeft"] = 30,
                    ["showFullPrice"] = true,
                    ["showAnnounceAlerts"] = true,
                    ["viewSize"] = "full",
                    ["focus1"] = 10,
                    ["openWithMail"] = true,
                    ["offlineSales"] = true,
                    ["trimDecimals"] = false,
                    ["shiftDays"] = "Focus 1",
                    ["displayItemAnalysisButtons"] = false,
                    ["noSalesInfoDeal"] = 2,
                    ["displayListingMessage"] = false,
                    ["viewGuildBuyerSeller"] = "seller",
                    ["guildWinTop"] = 30,
                    ["feedbackWinTop"] = 420,
                    ["statsWinLeft"] = 720,
                    ["saucy"] = false,
                    ["trimOutliers"] = false,
                },
            },
        },
        ["MasterMerchant"] = 
        {
            ["$AccountWide"] = 
            {
                ["switchedToChampionRanks"] = true,
                ["historyDepth"] = 30,
                ["newestItem"] = 
                {
                    ["Bandits Black Market"] = 1462267438,
                    ["First Kohorte of Asmodeus"] = 1462264492,
                    ["The Traveling Merchant"] = 1465562604,
                    ["The Lion Brotherhood"] = 1462228717,
                    ["E-Shop Trade Boutique"] = 1462209505,
                    ["Brave Cat Trade"] = 1465562634,
                    ["Team Gold"] = 1465562573,
                },
                ["maxItemCount"] = 5000,
                ["minItemCount"] = 20,
                ["lastScan"] = 
                {
                    ["Aequilibration"] = 1465562616,
                    ["First Kohorte of Asmodeus"] = 1462269263,
                    ["The Traveling Merchant"] = 1465562618,
                    ["The Lion Brotherhood"] = 1462268778,
                    ["Army of Fighters"] = 1460398965,
                    ["Bandits Black Market"] = 1462269258,
                    ["E-Shop Trade Boutique"] = 1462269261,
                    ["Brave Cat Trade"] = 1465562720,
                    ["Team Gold"] = 1465562610,
                },
                ["version"] = 1,
                ["delayInit"] = false,
            },
        },
    },
}
